<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class frmLabTests : Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLabTests))
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.nbxTestFee = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblTestFee = New System.Windows.Forms.Label()
        Me.stbTestName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblTestName = New System.Windows.Forms.Label()
        Me.lblTestCode = New System.Windows.Forms.Label()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.stbNormalRange = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboLabsID = New System.Windows.Forms.ComboBox()
        Me.cboUnitMeasureID = New System.Windows.Forms.ComboBox()
        Me.cboSpecimenTypeID = New System.Windows.Forms.ComboBox()
        Me.fcbResultDataTypeID = New SyncSoft.Common.Win.Controls.FlatComboBox()
        Me.chkHidden = New System.Windows.Forms.CheckBox()
        Me.nbxUnitCost = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.stbBriefTestDescription = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.clbTubeTypes = New System.Windows.Forms.CheckedListBox()
        Me.nbxVATPercentage = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.chkRequiresResultsApproval = New System.Windows.Forms.CheckBox()
        Me.stbTestCode = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblLabsID = New System.Windows.Forms.Label()
        Me.lblNormalRange = New System.Windows.Forms.Label()
        Me.lblUnitMeasureID = New System.Windows.Forms.Label()
        Me.lblSpecimenTypeID = New System.Windows.Forms.Label()
        Me.pnlResultDataTypeID = New System.Windows.Forms.Panel()
        Me.lblResultDataTypeID = New System.Windows.Forms.Label()
        Me.tbcLabTests = New System.Windows.Forms.TabControl()
        Me.tpgGeneral = New System.Windows.Forms.TabPage()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.lblVATPercentage = New System.Windows.Forms.Label()
        Me.lblTubeType = New System.Windows.Forms.Label()
        Me.lblBriefTestDescription = New System.Windows.Forms.Label()
        Me.LabelUnitCost = New System.Windows.Forms.Label()
        Me.tbcLabTestsMORE = New System.Windows.Forms.TabControl()
        Me.tpgBillCustomFee = New System.Windows.Forms.TabPage()
        Me.dgvBillCustomFee = New System.Windows.Forms.DataGridView()
        Me.colBillCustomerName = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colAccountNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBillCustomFee = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBillCurrenciesID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.ColRequiresPayment = New SyncSoft.Common.Win.Controls.GridComboBoxColumn()
        Me.colBillCustomFeeSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.tpgInsuranceCustomFee = New System.Windows.Forms.TabPage()
        Me.dgvInsuranceCustomFee = New System.Windows.Forms.DataGridView()
        Me.colInsuranceName = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colInsuranceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInsuranceCustomFee = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInsuranceCurrenciesID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.ColInsuranceRequiresPayment = New SyncSoft.Common.Win.Controls.GridComboBoxColumn()
        Me.colInsuranceCustomFeeSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.tpgLabPossibleResults = New System.Windows.Forms.TabPage()
        Me.dgvLabPossibleResults = New System.Windows.Forms.DataGridView()
        Me.colPossibleResult = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPossibleResultSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.tpgPossibleConsumables = New System.Windows.Forms.TabPage()
        Me.dgvConsumables = New System.Windows.Forms.DataGridView()
        Me.tpgLabTestsEXT = New System.Windows.Forms.TabPage()
        Me.dgvLabTestsEXT = New System.Windows.Forms.DataGridView()
        Me.colSubTestCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSubTestName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNormalRange = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colUnitMeasureID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colResultDataTypeID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colHidden = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colAddPossibleLabResults = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.cmsLabTestEXT = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmsLabTestEXTCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmsLabTestEXTSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmsLabTestEXTPossibleLabResults = New System.Windows.Forms.ToolStripMenuItem()
        Me.colConsumableSelect = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.colConsumableNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableQuantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableUnitCost = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableUnitPrice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableNotes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColConsumableSaved = New SyncSoft.Common.Win.Controls.GridCheckBoxColumn()
        Me.pnlResultDataTypeID.SuspendLayout()
        Me.tbcLabTests.SuspendLayout()
        Me.tpgGeneral.SuspendLayout()
        Me.tbcLabTestsMORE.SuspendLayout()
        Me.tpgBillCustomFee.SuspendLayout()
        CType(Me.dgvBillCustomFee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgInsuranceCustomFee.SuspendLayout()
        CType(Me.dgvInsuranceCustomFee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgLabPossibleResults.SuspendLayout()
        CType(Me.dgvLabPossibleResults, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgPossibleConsumables.SuspendLayout()
        CType(Me.dgvConsumables, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgLabTestsEXT.SuspendLayout()
        CType(Me.dgvLabTestsEXT, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsLabTestEXT.SuspendLayout()
        Me.SuspendLayout()
        '
        'fbnClose
        '
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(750, 548)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 3
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'nbxTestFee
        '
        Me.nbxTestFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxTestFee.ControlCaption = "Test Fee"
        Me.nbxTestFee.DataFormat = SyncSoft.Common.Win.Controls.DisplayFormat.Standard
        Me.ebnSaveUpdate.SetDataMember(Me.nbxTestFee, "TestFee")
        Me.nbxTestFee.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxTestFee.DecimalPlaces = -1
        Me.nbxTestFee.Location = New System.Drawing.Point(141, 206)
        Me.nbxTestFee.MaxLength = 12
        Me.nbxTestFee.MaxValue = 0.0R
        Me.nbxTestFee.MinValue = 0.0R
        Me.nbxTestFee.MustEnterNumeric = True
        Me.nbxTestFee.Name = "nbxTestFee"
        Me.nbxTestFee.Size = New System.Drawing.Size(209, 20)
        Me.nbxTestFee.TabIndex = 18
        Me.nbxTestFee.Tag = "LabPrices"
        Me.nbxTestFee.Value = ""
        '
        'lblTestFee
        '
        Me.lblTestFee.Location = New System.Drawing.Point(14, 206)
        Me.lblTestFee.Name = "lblTestFee"
        Me.lblTestFee.Size = New System.Drawing.Size(121, 21)
        Me.lblTestFee.TabIndex = 17
        Me.lblTestFee.Text = "Test Fee"
        '
        'stbTestName
        '
        Me.stbTestName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTestName.CapitalizeFirstLetter = True
        Me.ebnSaveUpdate.SetDataMember(Me.stbTestName, "TestName")
        Me.stbTestName.EntryErrorMSG = ""
        Me.stbTestName.Location = New System.Drawing.Point(141, 30)
        Me.stbTestName.MaxLength = 100
        Me.stbTestName.Name = "stbTestName"
        Me.stbTestName.RegularExpression = ""
        Me.stbTestName.Size = New System.Drawing.Size(209, 20)
        Me.stbTestName.TabIndex = 4
        Me.stbTestName.Tag = "TestName"
        '
        'lblTestName
        '
        Me.lblTestName.Location = New System.Drawing.Point(14, 30)
        Me.lblTestName.Name = "lblTestName"
        Me.lblTestName.Size = New System.Drawing.Size(121, 21)
        Me.lblTestName.TabIndex = 3
        Me.lblTestName.Text = "Test Name"
        '
        'lblTestCode
        '
        Me.lblTestCode.Location = New System.Drawing.Point(14, 8)
        Me.lblTestCode.Name = "lblTestCode"
        Me.lblTestCode.Size = New System.Drawing.Size(121, 21)
        Me.lblTestCode.TabIndex = 0
        Me.lblTestCode.Text = "Test Code"
        '
        'btnSearch
        '
        Me.btnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSearch.Location = New System.Drawing.Point(11, 519)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(77, 23)
        Me.btnSearch.TabIndex = 1
        Me.btnSearch.Text = "S&earch"
        Me.btnSearch.UseVisualStyleBackColor = True
        Me.btnSearch.Visible = False
        '
        'fbnDelete
        '
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(750, 518)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 4
        Me.fbnDelete.Tag = "LabTests"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = False
        Me.fbnDelete.Visible = False
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(11, 548)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 2
        Me.ebnSaveUpdate.Tag = "LabTests"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = False
        '
        'stbNormalRange
        '
        Me.stbNormalRange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbNormalRange.CapitalizeFirstLetter = True
        Me.ebnSaveUpdate.SetDataMember(Me.stbNormalRange, "NormalRange")
        Me.stbNormalRange.EntryErrorMSG = ""
        Me.stbNormalRange.Location = New System.Drawing.Point(141, 99)
        Me.stbNormalRange.MaxLength = 800
        Me.stbNormalRange.Multiline = True
        Me.stbNormalRange.Name = "stbNormalRange"
        Me.stbNormalRange.RegularExpression = ""
        Me.stbNormalRange.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbNormalRange.Size = New System.Drawing.Size(209, 37)
        Me.stbNormalRange.TabIndex = 10
        Me.stbNormalRange.Tag = "LabTests"
        '
        'cboLabsID
        '
        Me.cboLabsID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboLabsID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ebnSaveUpdate.SetDataMember(Me.cboLabsID, "Lab,LabID")
        Me.cboLabsID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLabsID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboLabsID.FormattingEnabled = True
        Me.cboLabsID.Location = New System.Drawing.Point(141, 76)
        Me.cboLabsID.Name = "cboLabsID"
        Me.cboLabsID.Size = New System.Drawing.Size(209, 21)
        Me.cboLabsID.TabIndex = 8
        '
        'cboUnitMeasureID
        '
        Me.cboUnitMeasureID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboUnitMeasureID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ebnSaveUpdate.SetDataMember(Me.cboUnitMeasureID, "UnitMeasure,UnitMeasureID")
        Me.cboUnitMeasureID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnitMeasureID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboUnitMeasureID.FormattingEnabled = True
        Me.cboUnitMeasureID.Location = New System.Drawing.Point(141, 139)
        Me.cboUnitMeasureID.Name = "cboUnitMeasureID"
        Me.cboUnitMeasureID.Size = New System.Drawing.Size(209, 21)
        Me.cboUnitMeasureID.TabIndex = 12
        '
        'cboSpecimenTypeID
        '
        Me.cboSpecimenTypeID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboSpecimenTypeID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ebnSaveUpdate.SetDataMember(Me.cboSpecimenTypeID, "SpecimenType,SpecimenTypeID")
        Me.cboSpecimenTypeID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSpecimenTypeID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboSpecimenTypeID.FormattingEnabled = True
        Me.cboSpecimenTypeID.Location = New System.Drawing.Point(141, 53)
        Me.cboSpecimenTypeID.Name = "cboSpecimenTypeID"
        Me.cboSpecimenTypeID.Size = New System.Drawing.Size(209, 21)
        Me.cboSpecimenTypeID.TabIndex = 6
        '
        'fcbResultDataTypeID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.fcbResultDataTypeID, "ResultDataType,ResultDataTypeID")
        Me.fcbResultDataTypeID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.fcbResultDataTypeID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fcbResultDataTypeID.FormattingEnabled = True
        Me.fcbResultDataTypeID.Location = New System.Drawing.Point(135, 4)
        Me.fcbResultDataTypeID.Name = "fcbResultDataTypeID"
        Me.fcbResultDataTypeID.ReadOnly = True
        Me.fcbResultDataTypeID.Size = New System.Drawing.Size(209, 21)
        Me.fcbResultDataTypeID.TabIndex = 1
        '
        'chkHidden
        '
        Me.chkHidden.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkHidden, "Hidden")
        Me.chkHidden.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkHidden.Location = New System.Drawing.Point(17, 265)
        Me.chkHidden.Name = "chkHidden"
        Me.chkHidden.Size = New System.Drawing.Size(141, 20)
        Me.chkHidden.TabIndex = 25
        Me.chkHidden.Text = "Hidden"
        '
        'nbxUnitCost
        '
        Me.nbxUnitCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxUnitCost.ControlCaption = "Unit Cost"
        Me.nbxUnitCost.DataFormat = SyncSoft.Common.Win.Controls.DisplayFormat.Standard
        Me.ebnSaveUpdate.SetDataMember(Me.nbxUnitCost, "UnitCost")
        Me.nbxUnitCost.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxUnitCost.DecimalPlaces = -1
        Me.nbxUnitCost.Location = New System.Drawing.Point(141, 162)
        Me.nbxUnitCost.MaxLength = 12
        Me.nbxUnitCost.MaxValue = 0.0R
        Me.nbxUnitCost.MinValue = 0.0R
        Me.nbxUnitCost.MustEnterNumeric = True
        Me.nbxUnitCost.Name = "nbxUnitCost"
        Me.nbxUnitCost.Size = New System.Drawing.Size(209, 20)
        Me.nbxUnitCost.TabIndex = 14
        Me.nbxUnitCost.Tag = "LabPrices"
        Me.nbxUnitCost.Value = ""
        '
        'stbBriefTestDescription
        '
        Me.stbBriefTestDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBriefTestDescription.CapitalizeFirstLetter = True
        Me.ebnSaveUpdate.SetDataMember(Me.stbBriefTestDescription, "TestDescription")
        Me.stbBriefTestDescription.EntryErrorMSG = ""
        Me.stbBriefTestDescription.Location = New System.Drawing.Point(584, 59)
        Me.stbBriefTestDescription.MaxLength = 800
        Me.stbBriefTestDescription.Multiline = True
        Me.stbBriefTestDescription.Name = "stbBriefTestDescription"
        Me.stbBriefTestDescription.RegularExpression = ""
        Me.stbBriefTestDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbBriefTestDescription.Size = New System.Drawing.Size(209, 71)
        Me.stbBriefTestDescription.TabIndex = 23
        '
        'clbTubeTypes
        '
        Me.clbTubeTypes.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.ebnSaveUpdate.SetDataMember(Me.clbTubeTypes, "TubeType")
        Me.clbTubeTypes.FormattingEnabled = True
        Me.clbTubeTypes.Location = New System.Drawing.Point(584, 8)
        Me.clbTubeTypes.Name = "clbTubeTypes"
        Me.clbTubeTypes.Size = New System.Drawing.Size(209, 45)
        Me.clbTubeTypes.TabIndex = 21
        '
        'nbxVATPercentage
        '
        Me.nbxVATPercentage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxVATPercentage.ControlCaption = "VATPercentage"
        Me.nbxVATPercentage.DataFormat = SyncSoft.Common.Win.Controls.DisplayFormat.Standard
        Me.ebnSaveUpdate.SetDataMember(Me.nbxVATPercentage, "VATPercentage")
        Me.nbxVATPercentage.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxVATPercentage.DecimalPlaces = -1
        Me.nbxVATPercentage.Location = New System.Drawing.Point(141, 184)
        Me.nbxVATPercentage.MaxLength = 12
        Me.nbxVATPercentage.MaxValue = 0.0R
        Me.nbxVATPercentage.MinValue = 0.0R
        Me.nbxVATPercentage.MustEnterNumeric = True
        Me.nbxVATPercentage.Name = "nbxVATPercentage"
        Me.nbxVATPercentage.Size = New System.Drawing.Size(209, 20)
        Me.nbxVATPercentage.TabIndex = 16
        Me.nbxVATPercentage.Tag = "LabPrices"
        Me.nbxVATPercentage.Value = ""
        '
        'chkRequiresResultsApproval
        '
        Me.chkRequiresResultsApproval.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkRequiresResultsApproval, "RequiresResultsApproval")
        Me.chkRequiresResultsApproval.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkRequiresResultsApproval.Location = New System.Drawing.Point(460, 133)
        Me.chkRequiresResultsApproval.Name = "chkRequiresResultsApproval"
        Me.chkRequiresResultsApproval.Size = New System.Drawing.Size(199, 20)
        Me.chkRequiresResultsApproval.TabIndex = 24
        Me.chkRequiresResultsApproval.Text = "Requires Results Approval"
        '
        'stbTestCode
        '
        Me.stbTestCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTestCode.CapitalizeFirstLetter = True
        Me.ebnSaveUpdate.SetDataMember(Me.stbTestCode, "TestCode")
        Me.stbTestCode.Enabled = False
        Me.stbTestCode.EntryErrorMSG = ""
        Me.stbTestCode.Location = New System.Drawing.Point(141, 8)
        Me.stbTestCode.MaxLength = 100
        Me.stbTestCode.Name = "stbTestCode"
        Me.stbTestCode.RegularExpression = ""
        Me.stbTestCode.Size = New System.Drawing.Size(209, 20)
        Me.stbTestCode.TabIndex = 1
        Me.stbTestCode.Tag = "TestName"
        '
        'lblLabsID
        '
        Me.lblLabsID.Location = New System.Drawing.Point(14, 76)
        Me.lblLabsID.Name = "lblLabsID"
        Me.lblLabsID.Size = New System.Drawing.Size(121, 21)
        Me.lblLabsID.TabIndex = 7
        Me.lblLabsID.Text = "Lab"
        '
        'lblNormalRange
        '
        Me.lblNormalRange.Location = New System.Drawing.Point(14, 107)
        Me.lblNormalRange.Name = "lblNormalRange"
        Me.lblNormalRange.Size = New System.Drawing.Size(121, 21)
        Me.lblNormalRange.TabIndex = 9
        Me.lblNormalRange.Text = "Normal Range"
        '
        'lblUnitMeasureID
        '
        Me.lblUnitMeasureID.Location = New System.Drawing.Point(14, 139)
        Me.lblUnitMeasureID.Name = "lblUnitMeasureID"
        Me.lblUnitMeasureID.Size = New System.Drawing.Size(121, 21)
        Me.lblUnitMeasureID.TabIndex = 11
        Me.lblUnitMeasureID.Text = "Unit Measure"
        '
        'lblSpecimenTypeID
        '
        Me.lblSpecimenTypeID.Location = New System.Drawing.Point(14, 51)
        Me.lblSpecimenTypeID.Name = "lblSpecimenTypeID"
        Me.lblSpecimenTypeID.Size = New System.Drawing.Size(121, 21)
        Me.lblSpecimenTypeID.TabIndex = 5
        Me.lblSpecimenTypeID.Text = "Specimen Type"
        '
        'pnlResultDataTypeID
        '
        Me.pnlResultDataTypeID.Controls.Add(Me.fcbResultDataTypeID)
        Me.pnlResultDataTypeID.Controls.Add(Me.lblResultDataTypeID)
        Me.pnlResultDataTypeID.Location = New System.Drawing.Point(6, 229)
        Me.pnlResultDataTypeID.Name = "pnlResultDataTypeID"
        Me.pnlResultDataTypeID.Size = New System.Drawing.Size(355, 29)
        Me.pnlResultDataTypeID.TabIndex = 19
        '
        'lblResultDataTypeID
        '
        Me.lblResultDataTypeID.Location = New System.Drawing.Point(8, 4)
        Me.lblResultDataTypeID.Name = "lblResultDataTypeID"
        Me.lblResultDataTypeID.Size = New System.Drawing.Size(118, 21)
        Me.lblResultDataTypeID.TabIndex = 0
        Me.lblResultDataTypeID.Text = "Result Type of Data"
        '
        'tbcLabTests
        '
        Me.tbcLabTests.Controls.Add(Me.tpgGeneral)
        Me.tbcLabTests.Controls.Add(Me.tpgLabTestsEXT)
        Me.tbcLabTests.HotTrack = True
        Me.tbcLabTests.Location = New System.Drawing.Point(5, 12)
        Me.tbcLabTests.Name = "tbcLabTests"
        Me.tbcLabTests.SelectedIndex = 0
        Me.tbcLabTests.Size = New System.Drawing.Size(821, 500)
        Me.tbcLabTests.TabIndex = 0
        '
        'tpgGeneral
        '
        Me.tpgGeneral.Controls.Add(Me.stbTestCode)
        Me.tpgGeneral.Controls.Add(Me.btnLoad)
        Me.tpgGeneral.Controls.Add(Me.chkRequiresResultsApproval)
        Me.tpgGeneral.Controls.Add(Me.nbxVATPercentage)
        Me.tpgGeneral.Controls.Add(Me.lblVATPercentage)
        Me.tpgGeneral.Controls.Add(Me.lblTubeType)
        Me.tpgGeneral.Controls.Add(Me.clbTubeTypes)
        Me.tpgGeneral.Controls.Add(Me.stbBriefTestDescription)
        Me.tpgGeneral.Controls.Add(Me.lblBriefTestDescription)
        Me.tpgGeneral.Controls.Add(Me.nbxUnitCost)
        Me.tpgGeneral.Controls.Add(Me.LabelUnitCost)
        Me.tpgGeneral.Controls.Add(Me.chkHidden)
        Me.tpgGeneral.Controls.Add(Me.tbcLabTestsMORE)
        Me.tpgGeneral.Controls.Add(Me.pnlResultDataTypeID)
        Me.tpgGeneral.Controls.Add(Me.lblTestCode)
        Me.tpgGeneral.Controls.Add(Me.cboSpecimenTypeID)
        Me.tpgGeneral.Controls.Add(Me.lblTestName)
        Me.tpgGeneral.Controls.Add(Me.lblSpecimenTypeID)
        Me.tpgGeneral.Controls.Add(Me.stbTestName)
        Me.tpgGeneral.Controls.Add(Me.cboUnitMeasureID)
        Me.tpgGeneral.Controls.Add(Me.lblTestFee)
        Me.tpgGeneral.Controls.Add(Me.lblUnitMeasureID)
        Me.tpgGeneral.Controls.Add(Me.nbxTestFee)
        Me.tpgGeneral.Controls.Add(Me.stbNormalRange)
        Me.tpgGeneral.Controls.Add(Me.lblLabsID)
        Me.tpgGeneral.Controls.Add(Me.lblNormalRange)
        Me.tpgGeneral.Controls.Add(Me.cboLabsID)
        Me.tpgGeneral.Location = New System.Drawing.Point(4, 22)
        Me.tpgGeneral.Name = "tpgGeneral"
        Me.tpgGeneral.Size = New System.Drawing.Size(813, 474)
        Me.tpgGeneral.TabIndex = 3
        Me.tpgGeneral.Tag = "General"
        Me.tpgGeneral.Text = "General"
        Me.tpgGeneral.UseVisualStyleBackColor = True
        '
        'btnLoad
        '
        Me.btnLoad.AccessibleDescription = ""
        Me.btnLoad.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoad.Location = New System.Drawing.Point(353, 7)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(46, 24)
        Me.btnLoad.TabIndex = 2
        Me.btnLoad.Tag = ""
        Me.btnLoad.Text = "&Load"
        '
        'lblVATPercentage
        '
        Me.lblVATPercentage.Location = New System.Drawing.Point(14, 187)
        Me.lblVATPercentage.Name = "lblVATPercentage"
        Me.lblVATPercentage.Size = New System.Drawing.Size(121, 15)
        Me.lblVATPercentage.TabIndex = 15
        Me.lblVATPercentage.Text = "VATPercentage"
        '
        'lblTubeType
        '
        Me.lblTubeType.Location = New System.Drawing.Point(457, 4)
        Me.lblTubeType.Name = "lblTubeType"
        Me.lblTubeType.Size = New System.Drawing.Size(121, 20)
        Me.lblTubeType.TabIndex = 20
        Me.lblTubeType.Text = "Tube Type"
        Me.lblTubeType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBriefTestDescription
        '
        Me.lblBriefTestDescription.Location = New System.Drawing.Point(457, 62)
        Me.lblBriefTestDescription.Name = "lblBriefTestDescription"
        Me.lblBriefTestDescription.Size = New System.Drawing.Size(121, 49)
        Me.lblBriefTestDescription.TabIndex = 22
        Me.lblBriefTestDescription.Text = "Brief Test Description" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(Presented to Patient)"
        '
        'LabelUnitCost
        '
        Me.LabelUnitCost.Location = New System.Drawing.Point(14, 165)
        Me.LabelUnitCost.Name = "LabelUnitCost"
        Me.LabelUnitCost.Size = New System.Drawing.Size(121, 15)
        Me.LabelUnitCost.TabIndex = 13
        Me.LabelUnitCost.Text = "Unit Cost"
        '
        'tbcLabTestsMORE
        '
        Me.tbcLabTestsMORE.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbcLabTestsMORE.Controls.Add(Me.tpgBillCustomFee)
        Me.tbcLabTestsMORE.Controls.Add(Me.tpgInsuranceCustomFee)
        Me.tbcLabTestsMORE.Controls.Add(Me.tpgLabPossibleResults)
        Me.tbcLabTestsMORE.Controls.Add(Me.tpgPossibleConsumables)
        Me.tbcLabTestsMORE.HotTrack = True
        Me.tbcLabTestsMORE.Location = New System.Drawing.Point(6, 291)
        Me.tbcLabTestsMORE.Name = "tbcLabTestsMORE"
        Me.tbcLabTestsMORE.SelectedIndex = 0
        Me.tbcLabTestsMORE.Size = New System.Drawing.Size(804, 180)
        Me.tbcLabTestsMORE.TabIndex = 26
        '
        'tpgBillCustomFee
        '
        Me.tpgBillCustomFee.Controls.Add(Me.dgvBillCustomFee)
        Me.tpgBillCustomFee.Location = New System.Drawing.Point(4, 22)
        Me.tpgBillCustomFee.Name = "tpgBillCustomFee"
        Me.tpgBillCustomFee.Size = New System.Drawing.Size(796, 154)
        Me.tpgBillCustomFee.TabIndex = 2
        Me.tpgBillCustomFee.Tag = "BillCustomFee"
        Me.tpgBillCustomFee.Text = "Bill Custom Fee"
        Me.tpgBillCustomFee.UseVisualStyleBackColor = True
        '
        'dgvBillCustomFee
        '
        Me.dgvBillCustomFee.AllowUserToOrderColumns = True
        Me.dgvBillCustomFee.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBillCustomFee.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvBillCustomFee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colBillCustomerName, Me.colAccountNo, Me.colBillCustomFee, Me.colBillCurrenciesID, Me.ColRequiresPayment, Me.colBillCustomFeeSaved})
        Me.dgvBillCustomFee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvBillCustomFee.EnableHeadersVisualStyles = False
        Me.dgvBillCustomFee.GridColor = System.Drawing.Color.Khaki
        Me.dgvBillCustomFee.Location = New System.Drawing.Point(0, 0)
        Me.dgvBillCustomFee.Name = "dgvBillCustomFee"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBillCustomFee.RowHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvBillCustomFee.Size = New System.Drawing.Size(796, 154)
        Me.dgvBillCustomFee.TabIndex = 0
        Me.dgvBillCustomFee.Tag = "LabPrices"
        Me.dgvBillCustomFee.Text = "DataGridView1"
        '
        'colBillCustomerName
        '
        Me.colBillCustomerName.DataPropertyName = "AccountNo"
        Me.colBillCustomerName.DisplayStyleForCurrentCellOnly = True
        Me.colBillCustomerName.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colBillCustomerName.HeaderText = "To-Bill Account Name"
        Me.colBillCustomerName.Name = "colBillCustomerName"
        Me.colBillCustomerName.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colBillCustomerName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colBillCustomerName.Width = 250
        '
        'colAccountNo
        '
        Me.colAccountNo.DataPropertyName = "AccountNo"
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Info
        Me.colAccountNo.DefaultCellStyle = DataGridViewCellStyle2
        Me.colAccountNo.HeaderText = "Account No"
        Me.colAccountNo.Name = "colAccountNo"
        Me.colAccountNo.ReadOnly = True
        '
        'colBillCustomFee
        '
        Me.colBillCustomFee.DataPropertyName = "CustomFee"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.colBillCustomFee.DefaultCellStyle = DataGridViewCellStyle3
        Me.colBillCustomFee.HeaderText = "Custom Fee"
        Me.colBillCustomFee.MaxInputLength = 12
        Me.colBillCustomFee.Name = "colBillCustomFee"
        '
        'colBillCurrenciesID
        '
        Me.colBillCurrenciesID.DataPropertyName = "CurrenciesID"
        Me.colBillCurrenciesID.DisplayStyleForCurrentCellOnly = True
        Me.colBillCurrenciesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colBillCurrenciesID.HeaderText = "Currency"
        Me.colBillCurrenciesID.Name = "colBillCurrenciesID"
        Me.colBillCurrenciesID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'ColRequiresPayment
        '
        Me.ColRequiresPayment.ControlCaption = Nothing
        Me.ColRequiresPayment.DataPropertyName = "RequiresPayment"
        Me.ColRequiresPayment.DisplayStyleForCurrentCellOnly = True
        Me.ColRequiresPayment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ColRequiresPayment.HeaderText = "Requires Payment"
        Me.ColRequiresPayment.Name = "ColRequiresPayment"
        Me.ColRequiresPayment.SourceColumn = Nothing
        '
        'colBillCustomFeeSaved
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle4.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle4.NullValue = False
        Me.colBillCustomFeeSaved.DefaultCellStyle = DataGridViewCellStyle4
        Me.colBillCustomFeeSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colBillCustomFeeSaved.HeaderText = "Saved"
        Me.colBillCustomFeeSaved.Name = "colBillCustomFeeSaved"
        Me.colBillCustomFeeSaved.ReadOnly = True
        Me.colBillCustomFeeSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colBillCustomFeeSaved.Width = 50
        '
        'tpgInsuranceCustomFee
        '
        Me.tpgInsuranceCustomFee.Controls.Add(Me.dgvInsuranceCustomFee)
        Me.tpgInsuranceCustomFee.Location = New System.Drawing.Point(4, 22)
        Me.tpgInsuranceCustomFee.Name = "tpgInsuranceCustomFee"
        Me.tpgInsuranceCustomFee.Size = New System.Drawing.Size(796, 154)
        Me.tpgInsuranceCustomFee.TabIndex = 3
        Me.tpgInsuranceCustomFee.Tag = "InsuranceCustomFee"
        Me.tpgInsuranceCustomFee.Text = "Insurance Custom Fee"
        Me.tpgInsuranceCustomFee.UseVisualStyleBackColor = True
        '
        'dgvInsuranceCustomFee
        '
        Me.dgvInsuranceCustomFee.AllowUserToOrderColumns = True
        Me.dgvInsuranceCustomFee.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInsuranceCustomFee.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvInsuranceCustomFee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colInsuranceName, Me.colInsuranceNo, Me.colInsuranceCustomFee, Me.colInsuranceCurrenciesID, Me.ColInsuranceRequiresPayment, Me.colInsuranceCustomFeeSaved})
        Me.dgvInsuranceCustomFee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvInsuranceCustomFee.EnableHeadersVisualStyles = False
        Me.dgvInsuranceCustomFee.GridColor = System.Drawing.Color.Khaki
        Me.dgvInsuranceCustomFee.Location = New System.Drawing.Point(0, 0)
        Me.dgvInsuranceCustomFee.Name = "dgvInsuranceCustomFee"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInsuranceCustomFee.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvInsuranceCustomFee.Size = New System.Drawing.Size(796, 154)
        Me.dgvInsuranceCustomFee.TabIndex = 22
        Me.dgvInsuranceCustomFee.Tag = "LabPrices"
        Me.dgvInsuranceCustomFee.Text = "DataGridView1"
        '
        'colInsuranceName
        '
        Me.colInsuranceName.DataPropertyName = "InsuranceNo"
        Me.colInsuranceName.DisplayStyleForCurrentCellOnly = True
        Me.colInsuranceName.DropDownWidth = 200
        Me.colInsuranceName.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colInsuranceName.HeaderText = "Insurance Name"
        Me.colInsuranceName.Name = "colInsuranceName"
        Me.colInsuranceName.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colInsuranceName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colInsuranceName.Width = 300
        '
        'colInsuranceNo
        '
        Me.colInsuranceNo.DataPropertyName = "InsuranceNo"
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Info
        Me.colInsuranceNo.DefaultCellStyle = DataGridViewCellStyle7
        Me.colInsuranceNo.HeaderText = "Insurance No"
        Me.colInsuranceNo.Name = "colInsuranceNo"
        Me.colInsuranceNo.ReadOnly = True
        '
        'colInsuranceCustomFee
        '
        Me.colInsuranceCustomFee.DataPropertyName = "CustomFee"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.colInsuranceCustomFee.DefaultCellStyle = DataGridViewCellStyle8
        Me.colInsuranceCustomFee.HeaderText = "Custom Fee"
        Me.colInsuranceCustomFee.MaxInputLength = 12
        Me.colInsuranceCustomFee.Name = "colInsuranceCustomFee"
        '
        'colInsuranceCurrenciesID
        '
        Me.colInsuranceCurrenciesID.DataPropertyName = "CurrenciesID"
        Me.colInsuranceCurrenciesID.DisplayStyleForCurrentCellOnly = True
        Me.colInsuranceCurrenciesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colInsuranceCurrenciesID.HeaderText = "Currency"
        Me.colInsuranceCurrenciesID.Name = "colInsuranceCurrenciesID"
        Me.colInsuranceCurrenciesID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'ColInsuranceRequiresPayment
        '
        Me.ColInsuranceRequiresPayment.ControlCaption = Nothing
        Me.ColInsuranceRequiresPayment.DataPropertyName = "RequiresPayment"
        Me.ColInsuranceRequiresPayment.DisplayStyleForCurrentCellOnly = True
        Me.ColInsuranceRequiresPayment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ColInsuranceRequiresPayment.HeaderText = "Requires Payment"
        Me.ColInsuranceRequiresPayment.Name = "ColInsuranceRequiresPayment"
        Me.ColInsuranceRequiresPayment.SourceColumn = Nothing
        '
        'colInsuranceCustomFeeSaved
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle9.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle9.NullValue = False
        Me.colInsuranceCustomFeeSaved.DefaultCellStyle = DataGridViewCellStyle9
        Me.colInsuranceCustomFeeSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colInsuranceCustomFeeSaved.HeaderText = "Saved"
        Me.colInsuranceCustomFeeSaved.Name = "colInsuranceCustomFeeSaved"
        Me.colInsuranceCustomFeeSaved.ReadOnly = True
        Me.colInsuranceCustomFeeSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colInsuranceCustomFeeSaved.Width = 50
        '
        'tpgLabPossibleResults
        '
        Me.tpgLabPossibleResults.Controls.Add(Me.dgvLabPossibleResults)
        Me.tpgLabPossibleResults.Location = New System.Drawing.Point(4, 22)
        Me.tpgLabPossibleResults.Name = "tpgLabPossibleResults"
        Me.tpgLabPossibleResults.Size = New System.Drawing.Size(796, 154)
        Me.tpgLabPossibleResults.TabIndex = 1
        Me.tpgLabPossibleResults.Tag = "LabPossibleResults"
        Me.tpgLabPossibleResults.Text = "Lab Possible Results"
        Me.tpgLabPossibleResults.UseVisualStyleBackColor = True
        '
        'dgvLabPossibleResults
        '
        Me.dgvLabPossibleResults.AllowUserToOrderColumns = True
        Me.dgvLabPossibleResults.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLabPossibleResults.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgvLabPossibleResults.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colPossibleResult, Me.colPossibleResultSaved})
        Me.dgvLabPossibleResults.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLabPossibleResults.EnableHeadersVisualStyles = False
        Me.dgvLabPossibleResults.GridColor = System.Drawing.Color.Khaki
        Me.dgvLabPossibleResults.Location = New System.Drawing.Point(0, 0)
        Me.dgvLabPossibleResults.Name = "dgvLabPossibleResults"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLabPossibleResults.RowHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvLabPossibleResults.Size = New System.Drawing.Size(796, 154)
        Me.dgvLabPossibleResults.TabIndex = 0
        Me.dgvLabPossibleResults.Text = "DataGridView1"
        '
        'colPossibleResult
        '
        Me.colPossibleResult.DataPropertyName = "PossibleResult"
        Me.colPossibleResult.HeaderText = "Possible Result"
        Me.colPossibleResult.MaxInputLength = 200
        Me.colPossibleResult.Name = "colPossibleResult"
        Me.colPossibleResult.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colPossibleResult.Width = 250
        '
        'colPossibleResultSaved
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle12.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle12.NullValue = False
        Me.colPossibleResultSaved.DefaultCellStyle = DataGridViewCellStyle12
        Me.colPossibleResultSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colPossibleResultSaved.HeaderText = "Saved"
        Me.colPossibleResultSaved.Name = "colPossibleResultSaved"
        Me.colPossibleResultSaved.ReadOnly = True
        Me.colPossibleResultSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colPossibleResultSaved.Width = 50
        '
        'tpgPossibleConsumables
        '
        Me.tpgPossibleConsumables.Controls.Add(Me.dgvConsumables)
        Me.tpgPossibleConsumables.Location = New System.Drawing.Point(4, 22)
        Me.tpgPossibleConsumables.Name = "tpgPossibleConsumables"
        Me.tpgPossibleConsumables.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgPossibleConsumables.Size = New System.Drawing.Size(796, 154)
        Me.tpgPossibleConsumables.TabIndex = 4
        Me.tpgPossibleConsumables.Text = "Possible Consumables"
        Me.tpgPossibleConsumables.UseVisualStyleBackColor = True
        '
        'dgvConsumables
        '
        Me.dgvConsumables.AllowUserToOrderColumns = True
        Me.dgvConsumables.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvConsumables.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.dgvConsumables.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colConsumableSelect, Me.colConsumableNo, Me.colConsumableName, Me.colConsumableQuantity, Me.colConsumableUnitCost, Me.colConsumableUnitPrice, Me.colConsumableNotes, Me.ColConsumableSaved})
        Me.dgvConsumables.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvConsumables.EnableHeadersVisualStyles = False
        Me.dgvConsumables.GridColor = System.Drawing.Color.Khaki
        Me.dgvConsumables.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.dgvConsumables.Location = New System.Drawing.Point(3, 3)
        Me.dgvConsumables.Name = "dgvConsumables"
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle21.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvConsumables.RowHeadersDefaultCellStyle = DataGridViewCellStyle21
        Me.dgvConsumables.Size = New System.Drawing.Size(790, 148)
        Me.dgvConsumables.TabIndex = 42
        Me.dgvConsumables.Text = "DataGridView1"
        '
        'tpgLabTestsEXT
        '
        Me.tpgLabTestsEXT.Controls.Add(Me.dgvLabTestsEXT)
        Me.tpgLabTestsEXT.Location = New System.Drawing.Point(4, 22)
        Me.tpgLabTestsEXT.Name = "tpgLabTestsEXT"
        Me.tpgLabTestsEXT.Size = New System.Drawing.Size(813, 474)
        Me.tpgLabTestsEXT.TabIndex = 0
        Me.tpgLabTestsEXT.Tag = "LabTestsEXT"
        Me.tpgLabTestsEXT.Text = "Lab Tests EXTRA"
        Me.tpgLabTestsEXT.UseVisualStyleBackColor = True
        '
        'dgvLabTestsEXT
        '
        Me.dgvLabTestsEXT.AllowUserToOrderColumns = True
        Me.dgvLabTestsEXT.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle22.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLabTestsEXT.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle22
        Me.dgvLabTestsEXT.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colSubTestCode, Me.colSubTestName, Me.colNormalRange, Me.colUnitMeasureID, Me.colResultDataTypeID, Me.colHidden, Me.colSaved, Me.colAddPossibleLabResults})
        Me.dgvLabTestsEXT.ContextMenuStrip = Me.cmsLabTestEXT
        Me.dgvLabTestsEXT.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLabTestsEXT.EnableHeadersVisualStyles = False
        Me.dgvLabTestsEXT.GridColor = System.Drawing.Color.Khaki
        Me.dgvLabTestsEXT.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.dgvLabTestsEXT.Location = New System.Drawing.Point(0, 0)
        Me.dgvLabTestsEXT.Name = "dgvLabTestsEXT"
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle25.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLabTestsEXT.RowHeadersDefaultCellStyle = DataGridViewCellStyle25
        Me.dgvLabTestsEXT.Size = New System.Drawing.Size(813, 474)
        Me.dgvLabTestsEXT.TabIndex = 163
        Me.dgvLabTestsEXT.Text = "DataGridView1"
        '
        'colSubTestCode
        '
        Me.colSubTestCode.HeaderText = "Sub Test Code"
        Me.colSubTestCode.MaxInputLength = 20
        Me.colSubTestCode.Name = "colSubTestCode"
        Me.colSubTestCode.Width = 85
        '
        'colSubTestName
        '
        Me.colSubTestName.HeaderText = "Sub Test Name"
        Me.colSubTestName.MaxInputLength = 100
        Me.colSubTestName.Name = "colSubTestName"
        Me.colSubTestName.Width = 120
        '
        'colNormalRange
        '
        Me.colNormalRange.HeaderText = "Normal Range"
        Me.colNormalRange.MaxInputLength = 800
        Me.colNormalRange.Name = "colNormalRange"
        Me.colNormalRange.Width = 85
        '
        'colUnitMeasureID
        '
        Me.colUnitMeasureID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.colUnitMeasureID.DisplayStyleForCurrentCellOnly = True
        Me.colUnitMeasureID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colUnitMeasureID.HeaderText = "Unit Measure"
        Me.colUnitMeasureID.Name = "colUnitMeasureID"
        Me.colUnitMeasureID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colUnitMeasureID.Width = 85
        '
        'colResultDataTypeID
        '
        Me.colResultDataTypeID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.colResultDataTypeID.DisplayStyleForCurrentCellOnly = True
        Me.colResultDataTypeID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colResultDataTypeID.HeaderText = "Result Type of Data"
        Me.colResultDataTypeID.Name = "colResultDataTypeID"
        Me.colResultDataTypeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colResultDataTypeID.Width = 120
        '
        'colHidden
        '
        Me.colHidden.HeaderText = "Hidden"
        Me.colHidden.Name = "colHidden"
        Me.colHidden.Width = 50
        '
        'colSaved
        '
        Me.colSaved.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle23.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle23.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle23.NullValue = False
        Me.colSaved.DefaultCellStyle = DataGridViewCellStyle23
        Me.colSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colSaved.HeaderText = "Saved"
        Me.colSaved.Name = "colSaved"
        Me.colSaved.ReadOnly = True
        Me.colSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colSaved.Width = 50
        '
        'colAddPossibleLabResults
        '
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle24.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle24.ForeColor = System.Drawing.Color.Firebrick
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.DarkBlue
        Me.colAddPossibleLabResults.DefaultCellStyle = DataGridViewCellStyle24
        Me.colAddPossibleLabResults.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colAddPossibleLabResults.HeaderText = "Add Possible Lab Results"
        Me.colAddPossibleLabResults.Name = "colAddPossibleLabResults"
        Me.colAddPossibleLabResults.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colAddPossibleLabResults.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colAddPossibleLabResults.Text = "���"
        Me.colAddPossibleLabResults.UseColumnTextForButtonValue = True
        Me.colAddPossibleLabResults.Visible = False
        Me.colAddPossibleLabResults.Width = 135
        '
        'cmsLabTestEXT
        '
        Me.cmsLabTestEXT.AccessibleDescription = ""
        Me.cmsLabTestEXT.BackColor = System.Drawing.Color.GhostWhite
        Me.cmsLabTestEXT.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmsLabTestEXTCopy, Me.cmsLabTestEXTSelectAll, Me.cmsLabTestEXTPossibleLabResults})
        Me.cmsLabTestEXT.Name = "cmsSearch"
        Me.cmsLabTestEXT.Size = New System.Drawing.Size(180, 70)
        '
        'cmsLabTestEXTCopy
        '
        Me.cmsLabTestEXTCopy.Enabled = False
        Me.cmsLabTestEXTCopy.Image = CType(resources.GetObject("cmsLabTestEXTCopy.Image"), System.Drawing.Image)
        Me.cmsLabTestEXTCopy.Name = "cmsLabTestEXTCopy"
        Me.cmsLabTestEXTCopy.Size = New System.Drawing.Size(179, 22)
        Me.cmsLabTestEXTCopy.Text = "Copy"
        Me.cmsLabTestEXTCopy.ToolTipText = "To copy with column headings, use Ctrl+C key combination"
        '
        'cmsLabTestEXTSelectAll
        '
        Me.cmsLabTestEXTSelectAll.Enabled = False
        Me.cmsLabTestEXTSelectAll.Name = "cmsLabTestEXTSelectAll"
        Me.cmsLabTestEXTSelectAll.Size = New System.Drawing.Size(179, 22)
        Me.cmsLabTestEXTSelectAll.Text = "Select All"
        '
        'cmsLabTestEXTPossibleLabResults
        '
        Me.cmsLabTestEXTPossibleLabResults.Enabled = False
        Me.cmsLabTestEXTPossibleLabResults.Name = "cmsLabTestEXTPossibleLabResults"
        Me.cmsLabTestEXTPossibleLabResults.Size = New System.Drawing.Size(179, 22)
        Me.cmsLabTestEXTPossibleLabResults.Text = "Possible Lab Results"
        '
        'colConsumableSelect
        '
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.Firebrick
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.DarkBlue
        Me.colConsumableSelect.DefaultCellStyle = DataGridViewCellStyle15
        Me.colConsumableSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colConsumableSelect.HeaderText = "Select"
        Me.colConsumableSelect.Name = "colConsumableSelect"
        Me.colConsumableSelect.ReadOnly = True
        Me.colConsumableSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colConsumableSelect.Text = "���"
        Me.colConsumableSelect.UseColumnTextForButtonValue = True
        Me.colConsumableSelect.Width = 50
        '
        'colConsumableNo
        '
        Me.colConsumableNo.DataPropertyName = "ItemCode"
        Me.colConsumableNo.HeaderText = "Consumable No"
        Me.colConsumableNo.Name = "colConsumableNo"
        '
        'colConsumableName
        '
        Me.colConsumableName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colConsumableName.DataPropertyName = "ConsumableName"
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Info
        Me.colConsumableName.DefaultCellStyle = DataGridViewCellStyle16
        Me.colConsumableName.HeaderText = "Consumable Name"
        Me.colConsumableName.Name = "colConsumableName"
        Me.colConsumableName.ReadOnly = True
        Me.colConsumableName.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colConsumableQuantity
        '
        Me.colConsumableQuantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colConsumableQuantity.DataPropertyName = "Quantity"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle17.Format = "N0"
        DataGridViewCellStyle17.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle17.NullValue = Nothing
        Me.colConsumableQuantity.DefaultCellStyle = DataGridViewCellStyle17
        Me.colConsumableQuantity.HeaderText = "Quantity"
        Me.colConsumableQuantity.MaxInputLength = 12
        Me.colConsumableQuantity.Name = "colConsumableQuantity"
        '
        'colConsumableUnitCost
        '
        Me.colConsumableUnitCost.DataPropertyName = "UnitCost"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Info
        Me.colConsumableUnitCost.DefaultCellStyle = DataGridViewCellStyle18
        Me.colConsumableUnitCost.HeaderText = "Unit Cost"
        Me.colConsumableUnitCost.Name = "colConsumableUnitCost"
        Me.colConsumableUnitCost.ReadOnly = True
        '
        'colConsumableUnitPrice
        '
        Me.colConsumableUnitPrice.DataPropertyName = "UnitPrice"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Info
        Me.colConsumableUnitPrice.DefaultCellStyle = DataGridViewCellStyle19
        Me.colConsumableUnitPrice.HeaderText = "Unit Price"
        Me.colConsumableUnitPrice.Name = "colConsumableUnitPrice"
        Me.colConsumableUnitPrice.ReadOnly = True
        '
        'colConsumableNotes
        '
        Me.colConsumableNotes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colConsumableNotes.DataPropertyName = "Notes"
        Me.colConsumableNotes.HeaderText = "Notes"
        Me.colConsumableNotes.MaxInputLength = 40
        Me.colConsumableNotes.Name = "colConsumableNotes"
        '
        'ColConsumableSaved
        '
        Me.ColConsumableSaved.ControlCaption = Nothing
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle20.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle20.NullValue = False
        Me.ColConsumableSaved.DefaultCellStyle = DataGridViewCellStyle20
        Me.ColConsumableSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ColConsumableSaved.HeaderText = "Saved"
        Me.ColConsumableSaved.Name = "ColConsumableSaved"
        Me.ColConsumableSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ColConsumableSaved.SourceColumn = Nothing
        Me.ColConsumableSaved.Width = 50
        '
        'frmLabTests
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(831, 582)
        Me.Controls.Add(Me.tbcLabTests)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.fbnClose)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.KeyPreview = true
        Me.MaximizeBox = false
        Me.Name = "frmLabTests"
        Me.Text = "Lab Tests"
        Me.pnlResultDataTypeID.ResumeLayout(false)
        Me.tbcLabTests.ResumeLayout(false)
        Me.tpgGeneral.ResumeLayout(false)
        Me.tpgGeneral.PerformLayout
        Me.tbcLabTestsMORE.ResumeLayout(false)
        Me.tpgBillCustomFee.ResumeLayout(false)
        CType(Me.dgvBillCustomFee,System.ComponentModel.ISupportInitialize).EndInit
        Me.tpgInsuranceCustomFee.ResumeLayout(false)
        CType(Me.dgvInsuranceCustomFee,System.ComponentModel.ISupportInitialize).EndInit
        Me.tpgLabPossibleResults.ResumeLayout(false)
        CType(Me.dgvLabPossibleResults,System.ComponentModel.ISupportInitialize).EndInit
        Me.tpgPossibleConsumables.ResumeLayout(false)
        CType(Me.dgvConsumables,System.ComponentModel.ISupportInitialize).EndInit
        Me.tpgLabTestsEXT.ResumeLayout(false)
        CType(Me.dgvLabTestsEXT,System.ComponentModel.ISupportInitialize).EndInit
        Me.cmsLabTestEXT.ResumeLayout(false)
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents nbxTestFee As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblTestFee As System.Windows.Forms.Label
    Friend WithEvents stbTestName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblTestName As System.Windows.Forms.Label
    Friend WithEvents lblTestCode As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
    Friend WithEvents cboLabsID As System.Windows.Forms.ComboBox
    Friend WithEvents lblLabsID As System.Windows.Forms.Label
    Friend WithEvents stbNormalRange As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblNormalRange As System.Windows.Forms.Label
    Friend WithEvents cboUnitMeasureID As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnitMeasureID As System.Windows.Forms.Label
    Friend WithEvents cboSpecimenTypeID As System.Windows.Forms.ComboBox
    Friend WithEvents lblSpecimenTypeID As System.Windows.Forms.Label
    Friend WithEvents pnlResultDataTypeID As System.Windows.Forms.Panel
    Friend WithEvents fcbResultDataTypeID As SyncSoft.Common.Win.Controls.FlatComboBox
    Friend WithEvents lblResultDataTypeID As System.Windows.Forms.Label
    Friend WithEvents tbcLabTests As System.Windows.Forms.TabControl
    Friend WithEvents tpgGeneral As System.Windows.Forms.TabPage
    Friend WithEvents tpgLabTestsEXT As System.Windows.Forms.TabPage
    Friend WithEvents dgvLabTestsEXT As System.Windows.Forms.DataGridView
    Friend WithEvents tbcLabTestsMORE As System.Windows.Forms.TabControl
    Friend WithEvents tpgLabPossibleResults As System.Windows.Forms.TabPage
    Friend WithEvents dgvLabPossibleResults As System.Windows.Forms.DataGridView
    Friend WithEvents colPossibleResult As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPossibleResultSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents chkHidden As System.Windows.Forms.CheckBox
    Friend WithEvents tpgBillCustomFee As System.Windows.Forms.TabPage
    Friend WithEvents tpgInsuranceCustomFee As System.Windows.Forms.TabPage
    Friend WithEvents dgvBillCustomFee As System.Windows.Forms.DataGridView
    Friend WithEvents dgvInsuranceCustomFee As System.Windows.Forms.DataGridView
    Friend WithEvents LabelUnitCost As System.Windows.Forms.Label
    Friend WithEvents nbxUnitCost As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents stbBriefTestDescription As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblBriefTestDescription As System.Windows.Forms.Label
    Friend WithEvents lblTubeType As System.Windows.Forms.Label
    Friend WithEvents clbTubeTypes As System.Windows.Forms.CheckedListBox
    Friend WithEvents nbxVATPercentage As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblVATPercentage As Label
    Friend WithEvents tpgPossibleConsumables As System.Windows.Forms.TabPage
    Friend WithEvents dgvConsumables As System.Windows.Forms.DataGridView
    Friend WithEvents chkRequiresResultsApproval As System.Windows.Forms.CheckBox
    Friend WithEvents colBillCustomerName As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colAccountNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBillCustomFee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBillCurrenciesID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents ColRequiresPayment As SyncSoft.Common.Win.Controls.GridComboBoxColumn
    Friend WithEvents colBillCustomFeeSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colInsuranceName As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colInsuranceNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInsuranceCustomFee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInsuranceCurrenciesID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents ColInsuranceRequiresPayment As SyncSoft.Common.Win.Controls.GridComboBoxColumn
    Friend WithEvents colInsuranceCustomFeeSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents cmsLabTestEXT As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmsLabTestEXTCopy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmsLabTestEXTSelectAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmsLabTestEXTPossibleLabResults As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents colSubTestCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSubTestName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNormalRange As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colUnitMeasureID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colResultDataTypeID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colHidden As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colAddPossibleLabResults As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents stbTestCode As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents colConsumableSelect As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents colConsumableNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableQuantity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableUnitCost As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableUnitPrice As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableNotes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColConsumableSaved As SyncSoft.Common.Win.Controls.GridCheckBoxColumn
End Class
