﻿Option Strict On
Option Infer On

Imports SyncSoft.SQLDb
Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.SQL.Classes
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.Win.Controls
Imports SyncSoft.Common.SQL.Enumerations

Imports LookupData = SyncSoft.Lookup.SQL.LookupData
Imports LookupDataID = SyncSoft.SQLDb.Lookup.LookupDataID
Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects

Imports System.Collections.Generic
Imports SyncSoft.Common.Structures
Imports System.Drawing.Printing




Public Class frmOPDDoctorTrans
    Private printParagraphs As Collection
    Private WithEvents docDoctorPrintTransaction As New PrintDocument()
    ' The paragraphs.
    Private doctTransactionParagraphs As Collection
    Private pageNo As Integer
    Private printFontName As String = "Courier New"
    Private bodyBoldFont As New Font(printFontName, 10, FontStyle.Bold)
    Private bodyNormalFont As New Font(printFontName, 10)




    Private Sub frmOPDDoctorTrans_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Cursor = Cursors.WaitCursor()


            Me.dtpStartDateTime.MaxDate = Today.AddDays(1)
            Me.dtpEndDateTime.MaxDate = Today.AddDays(1)

            Me.dtpStartDateTime.Value = Today
            Me.dtpEndDateTime.Value = Now

            LoadLookupDataCombo(Me.cboDoctorSpecialityID, LookupObjects.DoctorSpecialty, False)

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub LoadStaff()

        Dim oStaff As New SyncSoft.SQLDb.Staff()
        Dim oStaffTitleID As New LookupDataID.StaffTitleID()
        Dim doctorSpecialtyID As String = StringValueMayBeEnteredIn(cboDoctorSpecialityID)

        Try
            Me.Cursor = Cursors.WaitCursor
            ' Load from Staff
            Dim staff As DataTable = oStaff.GetStaffByDoctorSpecialty(oStaffTitleID.Doctor, doctorSpecialtyID).Tables("Staff")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadComboData(Me.cboStaffNo, staff, "StaffFullName")
           '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


    Private Sub ClearControls()
        Me.stbAmount.Clear()
        Me.stbAmountWords.Clear()

        Me.stbTotalAmount.Clear()
        Me.stbTotalAmountWords.Clear()
        Me.dgvServices.Rows.Clear()
        Me.dgvProcedure.Rows.Clear()
        Me.dgvTheatreServices.Rows.Clear()
        Me.dgvIPDServices.Rows.Clear()
        Me.dgvIPDProcedure.Rows.Clear()
        Me.dgvIPDProcedure.Rows.Clear()
        Me.lblRecordsNo.Text = String.Empty

    End Sub

    Private Sub LoadData()

        Dim oItems As New Items()
        Dim oIPDItems As New IPDItems()
        Dim oStaff As New SyncSoft.SQLDb.Staff()

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim startDateTime As Date = DateTimeEnteredIn(Me.dtpStartDateTime, "Start Date")
            Dim endDateTime As Date = DateTimeEnteredIn(Me.dtpEndDateTime, "End Date")
            Dim staffNo As String = SubstringEnteredIn(Me.cboStaffNo, "Staff!")
            Dim itemName As String = StringMayBeEnteredIn(Me.stbItemName)

            Me.lblRecordsNo.Text = String.Empty
            If endDateTime < startDateTime Then Throw New ArgumentException("End Date Time can't be before Start Date Time!")

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''' Load OPD Data Tables
            Dim _OPDDoctorAttachedServices As DataTable = oItems.GetOPDDoctorAttachedServices(staffNo, startDateTime, endDateTime, itemName).Tables("Items")
            Dim _OPDDoctorAttachedProcedures As DataTable = oItems.GetOPDDoctorAttachedProcedures(staffNo, startDateTime, endDateTime, itemName).Tables("Items")
            Dim _OPDDoctorAttachedTheatreServices As DataTable = oItems.GetOPDDoctorAttachedTheatreServices(staffNo, startDateTime, endDateTime, itemName).Tables("Items")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''' Load IPD Data Tables
            Dim _IPDDoctorAttachedServices As DataTable = oIPDItems.GetIPDDoctorAttachedServices(staffNo, startDateTime, endDateTime, itemName).Tables("IPDItems")
            Dim _IPDDoctorAttachedProcedures As DataTable = oIPDItems.GetIPDDoctorAttachedProcedures(staffNo, startDateTime, endDateTime, itemName).Tables("IPDItems")
            Dim _IPDDoctorAttachedTheatreServices As DataTable = oIPDItems.GetIPDDoctorAttachedTheatreServices(staffNo, startDateTime, endDateTime, itemName).Tables("IPDItems")


            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''Load OPD Data Grids  '''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvServices, _OPDDoctorAttachedServices)
            LoadGridData(Me.dgvProcedure, _OPDDoctorAttachedProcedures)
            LoadGridData(dgvTheatreServices, _OPDDoctorAttachedTheatreServices)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''Load IPD Data Grids  '''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvIPDServices, _IPDDoctorAttachedServices)
            LoadGridData(Me.dgvIPDProcedure, _IPDDoctorAttachedProcedures)
            LoadGridData(dgvIPDTheatreServices, _IPDDoctorAttachedTheatreServices)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim serviceAmount As Decimal = CalculateGridAmount(dgvServices, colServiceAmount)
            Dim procedureAmount As Decimal = CalculateGridAmount(dgvProcedure, colProcedureAmount)
            Dim theatreAmount As Decimal = CalculateGridAmount(dgvTheatreServices, colTheatreAmount)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim _IPDServiceAmount As Decimal = CalculateGridAmount(dgvIPDServices, colIPDServiceAmount)
            Dim _IPDProcedureAmount As Decimal = CalculateGridAmount(dgvIPDProcedure, colIPDProcedureAmount)
            Dim _IPDTheatreAmount As Decimal = CalculateGridAmount(dgvIPDTheatreServices, colIPDTheatreAmount)
            
            Dim totalAmount As Decimal = serviceAmount + procedureAmount + theatreAmount + _IPDServiceAmount + _IPDProcedureAmount + _IPDTheatreAmount

            stbTotalAmount.Text = FormatNumber(totalAmount, AppData.DecimalPlaces)
            stbTotalAmountWords.Text = NumberToWords(totalAmount)

            Select Case Me.tbcSpecialistsReport.SelectedTab.Name
                Case tpgOPDTransactions.Name
                    OPDTabSelectedTabs()
                Case tpgIPDTransactions.Name
                    IPDTabSelectedTabs()
            End Select


        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub fbnClose_Click(sender As System.Object, e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub

    Private Sub fbnLoad_Click(sender As System.Object, e As System.EventArgs) Handles fbnLoad.Click
        Try

            Me.LoadData()

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub OPDTabSelectedTabs()
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim amount As Decimal
        Dim returnedRecord As Integer
        Dim tabLabel As String
        Select Case Me.tbcSpecialistsReport.SelectedTab.Name
            Case tpgOPDTransactions.Name
                Select Case tbcOPDTransactions.SelectedTab.Name
                    Case Me.tpgOPDServices.Name
                        returnedRecord = dgvServices.RowCount()
                        amount = CalculateGridAmount(dgvServices, colServiceAmount)
                        tabLabel = "OPD " + tpgOPDServices.Text
                        lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " returned"
                        fbnExport.Enabled = returnedRecord > 0
                        Me.stbAmount.Text = FormatNumber(amount, AppData.DecimalPlaces)
                        Me.stbAmountWords.Text = NumberToWords(amount)

                    Case Me.tpgOPDProcedure.Name
                        returnedRecord = dgvProcedure.RowCount()
                        amount = CalculateGridAmount(dgvProcedure, colProcedureAmount)
                        tabLabel = "OPD " + tpgOPDProcedure.Text
                        lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " returned"
                        fbnExport.Enabled = returnedRecord > 0
                        Me.stbAmount.Text = FormatNumber(amount, AppData.DecimalPlaces)
                        Me.stbAmountWords.Text = NumberToWords(amount)

                    Case Me.tpgOPDTheatreServices.Name
                        returnedRecord = dgvTheatreServices.RowCount()
                        amount = CalculateGridAmount(dgvTheatreServices, colTheatreAmount)
                        tabLabel = "OPD " + tpgOPDTheatreServices.Text
                        lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " returned"
                        fbnExport.Enabled = returnedRecord > 0
                        Me.stbAmount.Text = FormatNumber(amount, AppData.DecimalPlaces)
                        Me.stbAmountWords.Text = NumberToWords(amount)

                End Select

            Case tpgIPDTransactions.Name
                Select Case tbcIPDTransactions.SelectedTab.Name
                    Case Me.tpgIPDServices.Name
                        returnedRecord = dgvIPDServices.RowCount()
                        amount = CalculateGridAmount(dgvIPDServices, colIPDServiceAmount)
                        tabLabel = "IPD " + tpgIPDServices.Text
                        lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " returned"
                        fbnExport.Enabled = returnedRecord > 0
                        Me.stbAmount.Text = FormatNumber(amount, AppData.DecimalPlaces)
                        Me.stbAmountWords.Text = NumberToWords(amount)

                    Case Me.tpgIPDProcedures.Name
                        returnedRecord = dgvIPDProcedure.RowCount()
                        amount = CalculateGridAmount(dgvIPDProcedure, colIPDProcedureAmount)
                        tabLabel = "IPD " + tpgIPDProcedures.Text
                        lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " returned"
                        fbnExport.Enabled = returnedRecord > 0
                        Me.stbAmount.Text = FormatNumber(amount, AppData.DecimalPlaces)
                        Me.stbAmountWords.Text = NumberToWords(amount)

                    Case Me.tpgIPDTheatreServices.Name
                        returnedRecord = dgvIPDTheatreServices.RowCount()
                        amount = CalculateGridAmount(dgvIPDTheatreServices, colIPDTheatreAmount)
                        tabLabel = "IPD " + tpgIPDTheatreServices.Text
                        lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " returned"
                        fbnExport.Enabled = returnedRecord > 0
                        Me.stbAmount.Text = FormatNumber(amount, AppData.DecimalPlaces)
                        Me.stbAmountWords.Text = NumberToWords(amount)

                End Select
        End Select
        


    End Sub

    Private Sub IPDTabSelectedTabs()
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim amount As Decimal
        Dim returnedRecord As Integer
        Dim tabLabel As String
        Select Case Me.tbcSpecialistsReport.SelectedTab.Name
            

            Case tpgIPDTransactions.Name
                Select Case tbcIPDTransactions.SelectedTab.Name
                    Case Me.tpgIPDServices.Name
                        returnedRecord = dgvIPDServices.RowCount()
                        amount = CalculateGridAmount(dgvIPDServices, colIPDServiceAmount)
                        tabLabel = "IPD " + tpgIPDServices.Text
                        lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " returned"
                        fbnExport.Enabled = returnedRecord > 0
                        Me.stbAmount.Text = FormatNumber(amount, AppData.DecimalPlaces)
                        Me.stbAmountWords.Text = NumberToWords(amount)

                    Case Me.tpgIPDProcedures.Name
                        returnedRecord = dgvIPDProcedure.RowCount()
                        amount = CalculateGridAmount(dgvIPDProcedure, colIPDProcedureAmount)
                        tabLabel = "IPD " + tpgIPDProcedures.Text
                        lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " returned"
                        fbnExport.Enabled = returnedRecord > 0
                        Me.stbAmount.Text = FormatNumber(amount, AppData.DecimalPlaces)
                        Me.stbAmountWords.Text = NumberToWords(amount)

                    Case Me.tpgIPDTheatreServices.Name
                        returnedRecord = dgvIPDTheatreServices.RowCount()
                        amount = CalculateGridAmount(dgvIPDTheatreServices, colIPDTheatreAmount)
                        tabLabel = "IPD " + tpgIPDTheatreServices.Text
                        lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " returned"
                        fbnExport.Enabled = returnedRecord > 0
                        Me.stbAmount.Text = FormatNumber(amount, AppData.DecimalPlaces)
                        Me.stbAmountWords.Text = NumberToWords(amount)

                End Select
        End Select



    End Sub


    


    Private Sub cboDoctorSpecialityID_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboDoctorSpecialityID.SelectedIndexChanged
        Me.ClearControls()
        Me.LoadStaff()
    End Sub



    Private Sub tbcSpecialistsReport_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles tbcSpecialistsReport.SelectedIndexChanged
        Me.OPDTabSelectedTabs()

    End Sub

    Private Sub fbnExport_Click(sender As System.Object, e As System.EventArgs) Handles fbnExport.Click
        Select Case tbcSpecialistsReport.SelectedTab.Name

            Case Me.tpgOPDTransactions.Name
                Select Case tbcOPDTransactions.SelectedTab.Name
                    Case Me.tpgOPDServices.Name
                        ExportToExcel(dgvServices, "OPD " + tpgOPDServices.Text)
                    Case Me.tpgOPDProcedure.Name
                        ExportToExcel(dgvProcedure, "OPD " + tpgOPDProcedure.Text)
                    Case Me.tpgOPDTheatreServices.Name
                        ExportToExcel(dgvTheatreServices, "OPD " + tpgOPDTheatreServices.Text)
                End Select

            Case Me.tpgIPDTransactions.Name
                Select Case tbcIPDTransactions.SelectedTab.Name
                    Case Me.tpgIPDServices.Name
                        ExportToExcel(dgvIPDServices, "IPD " + tpgIPDServices.Text)
                    Case Me.tpgIPDProcedures.Name
                        ExportToExcel(dgvIPDProcedure, "IPD " + tpgIPDProcedures.Text)
                    Case Me.tpgIPDTheatreServices.Name
                        ExportToExcel(dgvIPDTheatreServices, "IPD " + tpgIPDTheatreServices.Text)
                End Select
        End Select

    End Sub

    Private Sub tbcOPDTransactions_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles tbcOPDTransactions.SelectedIndexChanged
        Me.OPDTabSelectedTabs()

    End Sub

    Private Sub tbcIPDTransactions_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles tbcIPDTransactions.SelectedIndexChanged
        Me.IPDTabSelectedTabs()
    End Sub


#Region " Transaction Printing "

    Private Function OPDStaffTransactionsList() As List(Of Tuple(Of String, String, String, String, Decimal, String))

        Try

            ' Create list of tuples with two items each.
            Dim lOPDStaffTransactionsDetails As New List(Of Tuple(Of String, String, String, String, Decimal, String))

            For rowNo As Integer = 0 To Me.dgvServices.RowCount - 1
                Dim cells As DataGridViewCellCollection = Me.dgvServices.Rows(rowNo).Cells
                Dim recordDateTime As String = FormatDate(DateEnteredIn(cells, Me.colServiceRecordDate, "Record Date Time!"))
                Dim patientNo As String = StringEnteredIn(cells, Me.colServicePatientNo, "Patient No!")
                Dim itemName As String = StringEnteredIn(cells, Me.colServiceItemName, "Item Name!")
                Dim amount As Decimal = DecimalEnteredIn(cells, Me.colServiceAmount, True, "Amount")
                Dim paystatus As String = StringEnteredIn(cells, Me.colServiceAmount, "Pay Status")
                lOPDStaffTransactionsDetails.Add(New Tuple(Of String, String, String, String, Decimal, String)(recordDateTime, patientNo, "Service", itemName, amount, paystatus))

            Next

            For rowNo As Integer = 0 To Me.dgvProcedure.RowCount - 1
                Dim cells As DataGridViewCellCollection = Me.dgvProcedure.Rows(rowNo).Cells
                Dim recordDateTime As String = FormatDate(DateEnteredIn(cells, Me.colProcedureRecordDate, "Record Date Time!"))
                Dim patientNo As String = StringEnteredIn(cells, Me.colProcedurePatientNo, "Patient No!")
                Dim itemName As String = StringEnteredIn(cells, Me.colProcedureItemName, "Item Name!")
                Dim amount As Decimal = DecimalEnteredIn(cells, Me.colProcedureAmount, True, "Amount")
                Dim paystatus As String = StringEnteredIn(cells, Me.colProcedureAmount, "Pay Status")
                lOPDStaffTransactionsDetails.Add(New Tuple(Of String, String, String, String, Decimal, String)(recordDateTime, patientNo, "Procedure", itemName, amount, paystatus))

            Next

            For rowNo As Integer = 0 To Me.dgvTheatreServices.RowCount - 1
                Dim cells As DataGridViewCellCollection = Me.dgvTheatreServices.Rows(rowNo).Cells
                Dim recordDateTime As String = FormatDate(DateEnteredIn(cells, Me.colTheatreRecordDate, "Record Date Time!"))
                Dim patientNo As String = StringEnteredIn(cells, Me.colTheatrePatientNo, "Patient No!")
                Dim itemName As String = StringEnteredIn(cells, Me.colTheatreItemName, "Item Name!")
                Dim amount As Decimal = DecimalEnteredIn(cells, Me.colTheatreAmount, True, "Amount")
                Dim paystatus As String = StringEnteredIn(cells, Me.colTheatreAmount, "Pay Status")
                lOPDStaffTransactionsDetails.Add(New Tuple(Of String, String, String, String, Decimal, String)(recordDateTime, patientNo, "Theatre", itemName, amount, paystatus))

            Next

            Return lOPDStaffTransactionsDetails

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Function IPDStaffTransactionsList() As List(Of Tuple(Of String, String, String, String, Decimal, String))

        Try

            ' Create list of tuples with two items each.
            Dim lIPDStaffTransactionsDetails As New List(Of Tuple(Of String, String, String, String, Decimal, String))

            For rowNo As Integer = 0 To Me.dgvIPDServices.RowCount - 1
                Dim cells As DataGridViewCellCollection = Me.dgvIPDServices.Rows(rowNo).Cells
                Dim recordDateTime As String = FormatDate(DateEnteredIn(cells, Me.colIPDServiceRecordDate, "Record Date Time!"))
                Dim patientNo As String = StringEnteredIn(cells, Me.colIPDServicePatientNo, "Patient No!")
                Dim itemName As String = StringEnteredIn(cells, Me.colIPDServiceItemName, "Item Name!")
                Dim amount As Decimal = DecimalEnteredIn(cells, Me.colIPDServiceAmount, True, "Amount")
                Dim paystatus As String = StringEnteredIn(cells, Me.colIPDServiceAmount, "Pay Status")
                lIPDStaffTransactionsDetails.Add(New Tuple(Of String, String, String, String, Decimal, String)(recordDateTime, patientNo, "Service", itemName, amount, paystatus))

            Next

            For rowNo As Integer = 0 To Me.dgvIPDProcedure.RowCount - 1
                Dim cells As DataGridViewCellCollection = Me.dgvIPDProcedure.Rows(rowNo).Cells
                Dim recordDateTime As String = FormatDate(DateEnteredIn(cells, Me.colIPDProcedureRecordDate, "Record Date Time!"))
                Dim patientNo As String = StringEnteredIn(cells, Me.colIPDProcedurePatientNo, "Patient No!")
                Dim itemName As String = StringEnteredIn(cells, Me.colIPDProcedureItemName, "Item Name!")
                Dim amount As Decimal = DecimalEnteredIn(cells, Me.colIPDProcedureAmount, True, "Amount")
                Dim paystatus As String = StringEnteredIn(cells, Me.colIPDProcedureAmount, "Pay Status")
                lIPDStaffTransactionsDetails.Add(New Tuple(Of String, String, String, String, Decimal, String)(recordDateTime, patientNo, "Procedure", itemName, amount, paystatus))

            Next

            For rowNo As Integer = 0 To Me.dgvIPDTheatreServices.RowCount - 1
                Dim cells As DataGridViewCellCollection = Me.dgvIPDTheatreServices.Rows(rowNo).Cells
                Dim recordDateTime As String = FormatDate(DateEnteredIn(cells, Me.colIPDTheatreRecordDate, "Record Date Time!"))
                Dim patientNo As String = StringEnteredIn(cells, Me.colIPDTheatrePatientNo, "Patient No!")
                Dim itemName As String = StringEnteredIn(cells, Me.colIPDTheatreItem, "Item Name!")
                Dim amount As Decimal = DecimalEnteredIn(cells, Me.colIPDTheatreAmount, True, "Amount")
                Dim paystatus As String = StringEnteredIn(cells, Me.colIPDTheatrePayStatus, "Pay Status")
                lIPDStaffTransactionsDetails.Add(New Tuple(Of String, String, String, String, Decimal, String)(recordDateTime, patientNo, "Theatre", itemName, amount, paystatus))

            Next

            Return lIPDStaffTransactionsDetails

        Catch ex As Exception
            Throw ex
        End Try

    End Function


    Private Sub PrintStaffTransactions()

        Dim dlgPrint As New PrintDialog()

        Try

            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' If Me.dgvOPDDoctorTrans.RowCount < 1 Then Throw New ArgumentException("Must have at least one entry on OPD Transactions!")

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.SetStaffTransactionPrintData()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            dlgPrint.Document = docDoctorPrintTransaction
            dlgPrint.Document.PrinterSettings.Collate = True
            If dlgPrint.ShowDialog = DialogResult.OK Then docDoctorPrintTransaction.Print()

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub docDoctorPrintTransaction_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles docDoctorPrintTransaction.PrintPage

        Try
            Dim titleFont As New Font(printFontName, 12, FontStyle.Bold)

            Dim xPos As Single = e.MarginBounds.Left
            Dim yPos As Single = e.MarginBounds.Top

            Dim lineHeight As Single = bodyNormalFont.GetHeight(e.Graphics)

            Dim title As String = AppData.ProductOwner.ToUpper() + " OPD Staff Transactions Voucher".ToUpper()

            Dim StaffName As String = SubstringLeft(cboStaffNo.Text)
            Dim staffNo As String = SubstringEnteredIn(Me.cboStaffNo, "Staff!")
            Dim startDate As String = FormatDate(DateMayBeEnteredIn(Me.dtpStartDateTime))
            Dim endDate As String = FormatDate(DateMayBeEnteredIn(Me.dtpEndDateTime))

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Increment the page number.
            pageNo += 1

            With e.Graphics

                'Dim widthTop As Single = .MeasureString("Received from width", titleFont).Width

                Dim widthTopFirst As Single = .MeasureString("W", titleFont).Width
                Dim widthTopSecond As Single = 9 * widthTopFirst
                Dim widthTopThird As Single = 19 * widthTopFirst
                Dim widthTopFourth As Single = 27 * widthTopFirst

                If pageNo < 2 Then

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    yPos = PrintPageHeader(e, bodyNormalFont, bodyBoldFont)
                    Dim oProductOwner As ProductOwner = GetProductOwnerInfo()
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    .DrawString(title, titleFont, Brushes.Black, xPos, yPos)
                    yPos += 3 * lineHeight


                    .DrawString("Staff Name: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(StaffName, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight


                    .DrawString("Start Date: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(startDate, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    .DrawString("End Date: ", bodyNormalFont, Brushes.Black, xPos + widthTopThird, yPos)
                    .DrawString(endDate, bodyBoldFont, Brushes.Black, xPos + widthTopFourth, yPos)
                    yPos += lineHeight

                    yPos += 2 * lineHeight



                End If

                Dim _StringFormat As New StringFormat()

                ' Draw the rest of the text left justified,
                ' wrap at words, and don't draw partial lines.

                With _StringFormat
                    .Alignment = StringAlignment.Near
                    .FormatFlags = StringFormatFlags.LineLimit
                    .Trimming = StringTrimming.Word
                End With

                Dim charactersFitted As Integer
                Dim linesFilled As Integer

                If doctTransactionParagraphs Is Nothing Then Return

                Do While doctTransactionParagraphs.Count > 0

                    ' Print the next paragraph.
                    Dim oPrintParagraps As PrintParagraps = DirectCast(doctTransactionParagraphs(1), PrintParagraps)
                    doctTransactionParagraphs.Remove(1)

                    ' Get the area available for this paragraph.
                    Dim printAreaRectangle As RectangleF = New RectangleF(e.MarginBounds.Left, yPos, e.MarginBounds.Width, e.MarginBounds.Bottom - yPos)

                    ' If the printing area rectangle's height < 1, make it 1.
                    If printAreaRectangle.Height < 1 Then printAreaRectangle.Height = 1

                    ' See how big the text will be and how many characters will fit.
                    Dim textSize As SizeF = .MeasureString(oPrintParagraps.Text, oPrintParagraps.TheFont, _
                        New SizeF(printAreaRectangle.Width, printAreaRectangle.Height), _StringFormat, charactersFitted, linesFilled)

                    ' See if any characters will fit.
                    If charactersFitted > 0 Then
                        ' Draw the text.
                        .DrawString(oPrintParagraps.Text, oPrintParagraps.TheFont, Brushes.Black, printAreaRectangle, _StringFormat)
                        ' Increase the location where we can start, add a little interparagraph spacing.
                        yPos += textSize.Height ' + oPrintParagraps.TheFont.GetHeight(e.Graphics))

                    End If

                    ' See if some of the paragraph didn't fit on the page.
                    If charactersFitted < oPrintParagraps.Text.Length Then
                        ' Some of the paragraph didn't fit, prepare to print the rest on the next page.
                        oPrintParagraps.Text = oPrintParagraps.Text.Substring(charactersFitted)
                        doctTransactionParagraphs.Add(oPrintParagraps, Before:=1)
                        Exit Do
                    End If
                Loop

                ' If we have more paragraphs, we have more pages.
                e.HasMorePages = (doctTransactionParagraphs.Count > 0)

            End With

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetStaffTransactionPrintData()
        Dim padItemNo As Integer = 4
        Dim padRecordDateTime As Integer = 18
        Dim padPatientNo As Integer = 13
        Dim padItemName As Integer = 30
        Dim padAmount As Integer = 10
        Dim padTotalAmount As Integer = 58

        Dim count As Integer
        Dim footerFont As New Font(printFontName, 9)

        pageNo = 0
        doctTransactionParagraphs = New Collection()

        Dim oVariousOptions As New VariousOptions()


        Try
            Dim tableHeader As New System.Text.StringBuilder(String.Empty)
            Dim tableData As New System.Text.StringBuilder(String.Empty)

            Dim startDateTime As Date = DateTimeEnteredIn(Me.dtpStartDateTime, "Start Date")
            Dim endDateTime As Date = DateTimeEnteredIn(Me.dtpEndDateTime, "End Date")
            Dim staffNo As String = SubstringEnteredIn(Me.cboStaffNo, "Staff!")
            tableHeader.Append("OPD Transactions")
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append("Record Date: ".PadRight(padRecordDateTime))
            tableHeader.Append("Patient No: ".PadRight(padPatientNo))
            tableHeader.Append("Item Name: ".PadRight(padItemName))
            tableHeader.Append("Amount: ".PadRight(padAmount))
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append(ControlChars.NewLine)
            doctTransactionParagraphs.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))

            'Dim tableData As New System.Text.StringBuilder(String.Empty)

            Dim _OPDStaffTransactions = From data In Me.OPDStaffTransactionsList

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            For Each item In OPDStaffTransactionsList()

                count += 1

                Dim itemNo As String = (count).ToString()
                Dim recordDateTime As String = item.Item1
                Dim patientNo As String = item.Item2
                Dim itemCategory As String = item.Item3
                Dim ItemName As String = item.Item4
                Dim amount As String = FormatNumber(item.Item5, AppData.DecimalPlaces)
                tableData.Append(recordDateTime.PadRight(padRecordDateTime))
                tableData.Append(patientNo.PadRight(padPatientNo))

                Dim wrappedItemName As List(Of String) = WrapText(ItemName, padItemName)
                If wrappedItemName.Count > 1 Then
                    For pos As Integer = 0 To wrappedItemName.Count - 1
                        tableData.Append(FixDataLength(wrappedItemName(pos).Trim(), padItemName))
                        If Not pos = wrappedItemName.Count - 1 Then
                            tableData.Append(ControlChars.NewLine)
                            tableData.Append(GetSpaces(padItemNo + padRecordDateTime + padPatientNo))
                        Else : tableData.Append(amount.PadRight(padAmount))
                        End If
                    Next
                Else
                    tableData.Append(FixDataLength(ItemName, padItemName))
                    tableData.Append(amount.PadLeft(padAmount))
                End If
                tableData.Append(ControlChars.NewLine)
            Next

            doctTransactionParagraphs.Add(New PrintParagraps(bodyNormalFont, tableData.ToString()))
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            tableData = New System.Text.StringBuilder(String.Empty)
            tableHeader.Append(ControlChars.NewLine)
            tableData.Append("IPD Transactions")
            tableHeader.Append(ControlChars.NewLine)
            tableData.Append(ControlChars.NewLine)
            doctTransactionParagraphs.Add(New PrintParagraps(bodyBoldFont, tableData.ToString()))
            tableData.Append(ControlChars.NewLine)
            tableData = New System.Text.StringBuilder(String.Empty)

            For Each item In IPDStaffTransactionsList()

                count += 1

                Dim itemNo As String = (count).ToString()
                Dim recordDateTime As String = item.Item1
                Dim patientNo As String = item.Item2
                Dim itemCategory As String = item.Item3
                Dim ItemName As String = item.Item4
                Dim amount As String = FormatNumber(item.Item5, AppData.DecimalPlaces)
                tableData.Append(recordDateTime.PadRight(padRecordDateTime))
                tableData.Append(patientNo.PadRight(padPatientNo))

                Dim wrappedItemName As List(Of String) = WrapText(ItemName, padItemName)
                If wrappedItemName.Count > 1 Then
                    For pos As Integer = 0 To wrappedItemName.Count - 1
                        tableData.Append(FixDataLength(wrappedItemName(pos).Trim(), padItemName))
                        If Not pos = wrappedItemName.Count - 1 Then
                            tableData.Append(ControlChars.NewLine)
                            tableData.Append(GetSpaces(padItemNo + padRecordDateTime + padPatientNo))
                        Else : tableData.Append(amount.PadRight(padAmount))
                        End If
                    Next
                Else
                    tableData.Append(FixDataLength(ItemName, padItemName))
                    tableData.Append(amount.PadRight(padAmount))
                End If
                tableData.Append(ControlChars.NewLine)
            Next
            doctTransactionParagraphs.Add(New PrintParagraps(bodyNormalFont, tableData.ToString()))
            Dim totalAmountData As New System.Text.StringBuilder(String.Empty)
            Dim totalAmount As Decimal = DecimalMayBeEnteredIn(Me.stbTotalAmount, True)
            totalAmountData.Append(ControlChars.NewLine)
            totalAmountData.Append("Total Amount: ")
            totalAmountData.Append(FormatNumber(totalAmount, AppData.DecimalPlaces).PadLeft(padTotalAmount))
            totalAmountData.Append(ControlChars.NewLine)
            doctTransactionParagraphs.Add(New PrintParagraps(bodyBoldFont, totalAmountData.ToString()))

            Dim totalAmountWords As New System.Text.StringBuilder(String.Empty)
            Dim amountWords As String = StringMayBeEnteredIn(Me.stbTotalAmountWords)
            totalAmountWords.Append("(" + amountWords.Trim() + " ONLY)")
            totalAmountWords.Append(ControlChars.NewLine)
            doctTransactionParagraphs.Add(New PrintParagraps(footerFont, totalAmountWords.ToString()))

            Dim CreatedSignData As New System.Text.StringBuilder(String.Empty)
            CreatedSignData.Append(ControlChars.NewLine)

            CreatedSignData.Append("Created By:       " + GetCharacters("."c, 20))
            CreatedSignData.Append(GetSpaces(4))
            CreatedSignData.Append("Date:  " + GetCharacters("."c, 20))
            CreatedSignData.Append(ControlChars.NewLine)
            doctTransactionParagraphs.Add(New PrintParagraps(footerFont, CreatedSignData.ToString()))

            Dim footerData As New System.Text.StringBuilder(String.Empty)
            footerData.Append(ControlChars.NewLine)
            footerData.Append("Printed by " + CurrentUser.FullName + " on " + FormatDate(Now) + " at " +
                              Now.ToString("hh:mm tt") + " from " + AppData.AppTitle)
            footerData.Append(ControlChars.NewLine)
            doctTransactionParagraphs.Add(New PrintParagraps(footerFont, footerData.ToString()))

        Catch ex As Exception
            Throw ex
        End Try

    End Sub



    Private Sub btnPrintPreview_Click(sender As System.Object, e As System.EventArgs) Handles btnPrintPreview.Click
        Try

            Me.Cursor = Cursors.WaitCursor

            ' Make a PrintDocument and attach it to the PrintPreview dialog.

            Dim dlgPrintPreview As New PrintPreviewDialog()

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Me.SetStaffTransactionPrintData()

            With dlgPrintPreview
                .Document = docDoctorPrintTransaction
                .Document.PrinterSettings.Collate = True
                .ShowIcon = False
                .WindowState = FormWindowState.Maximized
                .ShowDialog()
            End With


        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        Try

            Me.Cursor = Cursors.WaitCursor

            Me.PrintStaffTransactions()

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub


#End Region

    Private Sub cboStaffNo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboStaffNo.SelectedIndexChanged
        ClearControls()
    End Sub

   
End Class