
Option Strict On

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls
Imports SyncSoft.Common.SQL.Enumerations
Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects
Imports SyncSoft.SQLDb
Imports System.Collections.Generic
Imports SyncSoft.Common.SQL.Classes
Imports SyncSoft.SQLDb.Lookup
Imports SyncSoft.SQLDb.Lookup.LookupDataID

Public Class frmSmartBilling

#Region " Fields "
    Private oVariousOptions As New VariousOptions()
    Private patientNo As String
    Private copayTypeID As String
    Private genderID As String
    Private copayValue As Decimal
    Private oIntegrationAgent As New IntegrationAgents()
    Private smartAgentNo As String = oIntegrationAgent.SMART()
    Private oCopayTypeID As New CoPayTypeID()
    Private defaultVisitNo As String
    Private defaultVisitTypeID As String
    Private saveApproved As Boolean
    Dim oVisitTypeID As New LookupDataID.VisitTypeID()

#End Region


    Private Sub frmSmartBilling_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.Cursor = Cursors.WaitCursor()
            LoadLookupDataCombo(Me.cboVisitTypeID, LookupObjects.VisitType, True)
            If Not String.IsNullOrEmpty(defaultVisitNo) Then
                Me.stbAdmissionNo.Text = defaultVisitNo
                Me.ShowPatientDetails(defaultVisitTypeID, RevertText(defaultVisitNo))

            End If

            Me.SetCountToSmartItems()
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub frmSmartBilling_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub

    Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub

 

    Private Sub ShowPatientDetails(visitTypeID As String, ByVal visitNo As String)

        Dim oVisits As New Visits()
        Try

            Me.ClearControls()
            visitNo = RevertText(visitNo)
            Dim row As DataRow
            Dim admissionNo As String
            If visitTypeID.Equals(oVisitTypeID.InPatient) Then
                Dim oAdmissions As New Admissions()
                Dim admissions As DataTable = oAdmissions.GetVisitAdmissions(visitNo).Tables("Admissions")
                If admissions.Rows.Count < 1 Then Throw New ArgumentException("No admission found for the Visit No: " + visitNo)
                row = admissions.Rows(0)
                admissionNo = StringMayBeEnteredIn(row, "AdmissionNo")
                Me.stbAdmissionNo.Text = FormatText(admissionNo, "Admissions", "AdmissionNo")
                Me.stbAdmissionDateTime.Text = FormatDateTime(DateTimeMayBeEnteredIn(row, "AdmissionDateTime"))
                Me.stbAdmissionStatus.Text = StringMayBeEnteredIn(row, "AdmissionStatus")
                Dim oINTAdmissions As New INTAdmissions()
                Dim _INTAdmissions As DataTable = oINTAdmissions.GetINTAdmissions(oIntegrationAgent.SMART, admissionNo).Tables("INTAdmissions")
                Dim rowINTAdmissions As DataRow = _INTAdmissions.Rows(0)
                Dim provisionalMemberLimit As Decimal = DecimalEnteredIn(rowINTAdmissions, "MemberLimit", False)
                Me.stbProvisionalLimit.Text = FormatNumber(provisionalMemberLimit, AppData.DecimalPlaces)

            ElseIf visitTypeID.Equals(oVisitTypeID.OutPatient) Then
                Dim visits As DataTable = oVisits.GetVisits(RevertText(visitNo)).Tables("Visits")
                row = visits.Rows(0)
                Dim oINTVisits As New INTVisits()
                Dim _INTVisits As DataTable = oINTVisits.GetINTVisits(oIntegrationAgent.SMART, visitNo).Tables("INTVisits")
                Dim rowINTVisits As DataRow = _INTVisits.Rows(0)
                Dim provisionalMemberLimit As Decimal = DecimalEnteredIn(rowINTVisits, "MemberLimit", False)
                Me.stbProvisionalLimit.Text = FormatNumber(provisionalMemberLimit, AppData.DecimalPlaces)
            Else
                Dim visits As DataTable = oVisits.GetVisits(visitNo).Tables("Visits")
                row = visits.Rows(0)
            End If

            patientNo = StringEnteredIn(row, "PatientNo")


            Me.stbPatientNo.Text = FormatText(patientNo, "Patients", "PatientNo")
            Me.stbVisitNo.Text = FormatText(visitNo, "Visits", "VisitNo")
            Me.stbFullName.Text = StringEnteredIn(row, "FullName")
            Me.stbAge.Text = StringEnteredIn(row, "Age")
            Dim birthDate As Date = DateMayBeEnteredIn(row, "BirthDate")
            Me.lblAgeString.Text = GetAgeString(birthDate, True)
            Me.stbGender.Text = StringEnteredIn(row, "Gender")
            Me.genderID = StringEnteredIn(row, "GenderID")
            Me.copayTypeID = StringMayBeEnteredIn(row, "CoPayTypeID")
            Me.copayValue = DecimalMayBeEnteredIn(row, "CoPayValue", False)
            Me.stbCoPayType.Text = StringMayBeEnteredIn(row, "CoPayType")
            Me.stbBillNo.Text = FormatText(StringEnteredIn(row, "BillNo"), "BillCustomers", "AccountNo")
            Me.stbBillMode.Text = StringEnteredIn(row, "BillMode")
            Dim associatedBillCustomer As String = StringMayBeEnteredIn(row, "AssociatedBillCustomer")
            Dim billCustomerName As String = StringMayBeEnteredIn(row, "BillCustomerName")
            If Not String.IsNullOrEmpty(associatedBillCustomer) Then billCustomerName += " (" + associatedBillCustomer + ")"
            Me.stbBillCustomerName.Text = billCustomerName

            Me.LoadSmartBill(visitTypeID, visitNo)



            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch eX As Exception
            Me.ClearControls()
            ErrorMessage(eX)

        End Try

    End Sub


    Private Sub ClearControls()
        Me.stbPatientNo.Clear()
        Me.stbFullName.Clear()
        Me.stbAdmissionNo.Clear()
        Me.stbGender.Clear()
        Me.stbAge.Clear()
        Me.stbAdmissionStatus.Clear()
        Me.stbAdmissionDateTime.Clear()
        Me.stbBillNo.Clear()
        Me.stbBillCustomerName.Clear()
        Me.stbBillMode.Clear()
        Me.stbProvisionalLimit.Clear()
        Me.stbAdmissionStatus.Clear()
        Me.stbBillForItem.Clear()
        Me.stbBillWords.Clear()
        Me.stbCoPayType.Clear()
        Me.dgvSmartBills.Rows.Clear()

       

    End Sub

    Private Sub LoadSmartBill(visitTypeID As String, visitNo As String)

        Try
            Dim oINTExtraBillItems As New INTExtraBillItems()
            Dim _INTExtraBillItems As DataTable = oINTExtraBillItems.GetNotSyncedExtraBillItems(smartAgentNo, visitTypeID, visitNo).Tables("INTExtraBillItems")

            LoadGridData(dgvSmartBills, _INTExtraBillItems)
            FormatGridRow(dgvSmartBills)
            Me.CalculateTotalBill()
        Catch ex As Exception
            ErrorMessage(ex)
        End Try

    End Sub


    Private Sub CalculateTotalBill()

        Dim totalBill As Decimal
        Dim totalCopayValue As Decimal

        Me.stbBillForItem.Clear()
        Me.stbBillWords.Clear()


        For rowNo As Integer = 0 To Me.dgvSmartBills.RowCount - 1
            If CBool(Me.dgvSmartBills.Item(Me.colInclude.Name, rowNo).Value) = True Then
                Dim cells As DataGridViewCellCollection = Me.dgvSmartBills.Rows(rowNo).Cells
                Dim amount As Decimal = DecimalMayBeEnteredIn(cells, Me.colAmount)
                Dim cashAmount As Decimal = DecimalMayBeEnteredIn(cells, Me.colCashAmount)
                totalBill += amount
                totalCopayValue += cashAmount
            End If
        Next

        Me.stbBillForItem.Text = FormatNumber(totalBill, AppData.DecimalPlaces)
        If Me.copayTypeID = oCopayTypeID.Percent Then
            Me.nbxCoPayValue.Value = FormatNumber(totalCopayValue, AppData.DecimalPlaces)
        Else : Me.nbxCoPayValue.Value = FormatNumber(Me.copayValue, AppData.DecimalPlaces)
        End If
        Me.stbBillWords.Text = NumberToWords(totalBill)


    End Sub


    Private Sub dgvSmartBills_CellEndEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSmartBills.CellEndEdit
        If e.ColumnIndex.Equals(Me.colInclude.Index) Then
            Me.CalculateTotalBill()
        End If
    End Sub

    Private Sub ebnSaveUpdate_Click(sender As System.Object, e As System.EventArgs) Handles ebnSaveUpdate.Click
        Dim oSmartCardItems As SmartCardItems
        Dim oSmartCardMembers As New SmartCardMembers()
        Dim transactions As New List(Of TransactionList(Of DBConnect))
        Dim lINTExtraBillItems As New List(Of DBConnect)
        Dim lINTExtraBills As New List(Of DBConnect)
        Dim lExtraBillNo As New List(Of String)

        Try
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.Cursor = Cursors.Default
            Dim lSmartCardItems As New List(Of SmartCardItems)
            oSmartCardMembers = ProcessSmartCardData(patientNo)
            Dim coverAmount As Decimal = oSmartCardMembers.CoverAmount
            Dim expiryDate As Date = oSmartCardMembers.SchemeExpiryDate
            Dim totalBill As Decimal = DecimalMayBeEnteredIn(stbBillForItem)

            If oSmartCardMembers.CoverAmount < totalBill Then
                Throw New ArgumentException("The Cover Amount " + coverAmount.ToString + " can't be less than total bill " + totalBill.ToString)
            End If
            'If expiryDate < Today Then
            '    Throw New ArgumentException("The scheeme expiry date:  " + expiryDate.ToString + " cannot be less than today. Please contact Smart for assistance ")
            'End If
            Dim smardCardNo As String = RevertText(RevertText(oSmartCardMembers.MedicalCardNumber, "/"c))
            Dim visitNo As String = RevertText(RevertText(StringMayBeEnteredIn(stbVisitNo)))
            Dim visitTypeID As String = StringValueEnteredIn(cboVisitTypeID, "Visit Type ID")
            Dim totalCashAmount As Decimal

            For rowNo As Integer = 0 To Me.dgvSmartBills.RowCount - 1

                If CBool(Me.dgvSmartBills.Item(Me.colInclude.Name, rowNo).Value) = True Then

                    Dim cells As DataGridViewCellCollection = Me.dgvSmartBills.Rows(rowNo).Cells
                    Dim itemCode As String = StringEnteredIn(cells, Me.colItemCode, "Item Code!")
                    Dim itemName As String = StringEnteredIn(cells, Me.colItemName, "Item Name!")
                    Dim extraBillNo As String = StringEnteredIn(cells, Me.colExtraBillNo, "Extra Bill No!")
                    Dim itemCategoryID As String = StringEnteredIn(cells, Me.colItemCategoryID)
                    Dim quantity As Integer = IntegerEnteredIn(cells, Me.colQuantity)
                    Dim unitPrice As Decimal = DecimalEnteredIn(cells, Me.colUnitPrice, False)
                    Dim amount As Decimal = DecimalEnteredIn(cells, Me.colAmount, False)
                    Dim cashAmount As Decimal = DecimalEnteredIn(cells, Me.colCashAmount, False)
                    totalCashAmount += cashAmount
                    Using oINTExtraBillItems As New SyncSoft.SQLDb.INTExtraBillItems()
                        With oINTExtraBillItems
                            .AgentNo = smartAgentNo
                            .ExtraBillNo = extraBillNo
                            .ItemCode = itemCode
                            .ItemCategoryID = itemCategoryID
                        End With
                        lINTExtraBillItems.Add(oINTExtraBillItems)
                    End Using

                    With oSmartCardItems

                        .TransactionDate = FormatDate(Today, "yyyy-MM-dd")
                        .TransactionTime = GetTime(Now)
                        .ServiceProviderNr = oVariousOptions.SmartCardServiceProviderNo
                        .DiagnosisCode = (0).ToString()
                        .DiagnosisDescription = "Unknown Disease"
                        .EncounterType = GetEncounterType(itemCategoryID)
                        .CodeType = "Mcode"
                        .Code = itemCode
                        .CodeDescription = itemName
                        .itemCode = itemCode
                        .itemCategoryID = itemCategoryID
                        .Quantity = quantity.ToString()
                        .Amount = (quantity * unitPrice).ToString()

                    End With

                    lSmartCardItems.Add(oSmartCardItems)


                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                End If
            Next


            oSmartCardMembers.InvoiceNo = visitNo
            oSmartCardMembers.TotalBill = totalBill
            oSmartCardMembers.TotalServices = lSmartCardItems.Count()
            oSmartCardMembers.CopayType = copayTypeID
            oSmartCardMembers.CopayAmount = DecimalMayBeEnteredIn(Me.nbxCoPayValue)
            oSmartCardMembers.Gender = genderID

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            transactions.Add(New TransactionList(Of DBConnect)(lINTExtraBillItems, Action.Save))
            transactions.Add(New TransactionList(Of DBConnect)(lINTExtraBills, Action.Save))

            If UpdateSmartExchangeFiles(oSmartCardMembers, lSmartCardItems, visitNo, visitTypeID, False) Then
                DoTransactions(transactions)

            Else
                Throw New ArgumentException("Error processing smart card information. Please edit the Patient and try again")
                Return
            End If
            Me.saveApproved = False
            SetCountToSmartItems()
            Me.LoadSmartBill(visitNo, visitTypeID)
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub


    Public Function GetSaveApproved() As Boolean
        Return Me.saveApproved
    End Function

    Private Sub SetCountToSmartItems()
        Try
            Dim oINTExtraBillItems As New INTExtraBillItems()
            Dim _IPDcount As Integer = oINTExtraBillItems.GetCountNotSyncedExtraBillItems(oIntegrationAgent.SMART, oVisitTypeID.InPatient, String.Empty)
            Dim _OPDcount As Integer = oINTExtraBillItems.GetCountNotSyncedExtraBillItems(oIntegrationAgent.SMART, oVisitTypeID.OutPatient, String.Empty)
            lblPendingToSmartIPDItems.Text = "Pending To Smart IPD Item(s): " + _IPDcount.ToString
            btnLoad.Enabled = _IPDcount > 0
            lblPendingToSmartOPDItems.Text = "Pending To Smart OPD Item(s): " + _OPDcount.ToString
            btnLoadOPD.Enabled = _OPDcount > 0
        Catch ex As Exception
            ErrorMessage(ex)
        End Try
    End Sub

    Private Sub stbVisitNo_Leave(sender As Object, e As System.EventArgs) Handles stbVisitNo.Leave
        Me.ClearControls()
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim visitNo As String = StringMayBeEnteredIn(stbVisitNo)
            Dim visitTypeID As String = StringValueMayBeEnteredIn(cboVisitTypeID)
            If String.IsNullOrEmpty(visitNo) OrElse String.IsNullOrEmpty(visitTypeID) Then Return
            ShowPatientDetails(visitTypeID, visitNo)
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

   
    Private Sub btnLoad_Click(sender As System.Object, e As System.EventArgs) Handles btnLoad.Click
        Dim visitTypeID As String = oVisitTypeID.InPatient
        Me.cboVisitTypeID.SelectedValue = visitTypeID
        Dim fPendingToSmartExtraBillItems As New frmPendingToSmartExtraBillItems(visitTypeID, stbVisitNo)
        fPendingToSmartExtraBillItems.ShowDialog()
        Dim visitNo As String = RevertText(StringMayBeEnteredIn(stbVisitNo))
        If String.IsNullOrEmpty(visitNo) Then Return
        ShowPatientDetails(VisitTypeID, visitNo)

    End Sub

    Private Sub btnLoadOPD_Click(sender As System.Object, e As System.EventArgs) Handles btnLoadOPD.Click
        Dim visitTypeID As String = oVisitTypeID.OutPatient
        Me.cboVisitTypeID.SelectedValue = visitTypeID
        Dim fPendingToSmartExtraBillItems As New frmPendingToSmartExtraBillItems(visitTypeID, stbVisitNo)
        fPendingToSmartExtraBillItems.ShowDialog()
        Dim visitNo As String = RevertText(StringMayBeEnteredIn(stbVisitNo))
        If String.IsNullOrEmpty(visitNo) Then Return
        ShowPatientDetails(visitTypeID, visitNo)
    End Sub

    Private Sub cboVisitTypeID_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboVisitTypeID.SelectedIndexChanged
        Me.ClearControls()
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim visitNo As String = StringMayBeEnteredIn(stbVisitNo)
            Dim visitTypeID As String = StringValueMayBeEnteredIn(cboVisitTypeID)
            If String.IsNullOrEmpty(visitNo) OrElse String.IsNullOrEmpty(visitTypeID) Then Return
            ShowPatientDetails(visitTypeID, visitNo)
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub
End Class