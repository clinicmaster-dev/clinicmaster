
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmINTAgents : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmINTAgents))
        Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.stbAgentName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbClientID = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbToken = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbReferenceNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboConnectionModeID = New System.Windows.Forms.ComboBox()
        Me.cboDatabaseTypeID = New System.Windows.Forms.ComboBox()
        Me.stbDataSource = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbDBName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.nbxPort = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.stbDBUsername = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbPassword = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.chkEnabled = New System.Windows.Forms.CheckBox()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.lblAgentNo = New System.Windows.Forms.Label()
        Me.lblAgentName = New System.Windows.Forms.Label()
        Me.lblClientID = New System.Windows.Forms.Label()
        Me.lblToken = New System.Windows.Forms.Label()
        Me.lblReferenceNo = New System.Windows.Forms.Label()
        Me.lblConnectionModeID = New System.Windows.Forms.Label()
        Me.lblDatabaseTypeID = New System.Windows.Forms.Label()
        Me.lblDataSource = New System.Windows.Forms.Label()
        Me.lblDBName = New System.Windows.Forms.Label()
        Me.lblPort = New System.Windows.Forms.Label()
        Me.lblDBUsername = New System.Windows.Forms.Label()
        Me.lblPassword = New System.Windows.Forms.Label()
        Me.cboAgentNo = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'fbnSearch
        '
        Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnSearch.Location = New System.Drawing.Point(17, 318)
        Me.fbnSearch.Name = "fbnSearch"
        Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
        Me.fbnSearch.TabIndex = 0
        Me.fbnSearch.Text = "S&earch"
        Me.fbnSearch.UseVisualStyleBackColor = True
        Me.fbnSearch.Visible = False
        '
        'fbnDelete
        '
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(316, 318)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 1
        Me.fbnDelete.Tag = "INTAgents"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = False
        Me.fbnDelete.Visible = False
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(17, 345)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 2
        Me.ebnSaveUpdate.Tag = "INTAgents"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = False
        '
        'stbAgentName
        '
        Me.stbAgentName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAgentName.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbAgentName, "AgentName")
        Me.stbAgentName.EntryErrorMSG = ""
        Me.stbAgentName.Location = New System.Drawing.Point(218, 35)
        Me.stbAgentName.Name = "stbAgentName"
        Me.stbAgentName.RegularExpression = ""
        Me.stbAgentName.Size = New System.Drawing.Size(170, 20)
        Me.stbAgentName.TabIndex = 6
        '
        'stbClientID
        '
        Me.stbClientID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbClientID.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbClientID, "ClientID")
        Me.stbClientID.EntryErrorMSG = ""
        Me.stbClientID.Location = New System.Drawing.Point(218, 58)
        Me.stbClientID.Name = "stbClientID"
        Me.stbClientID.RegularExpression = ""
        Me.stbClientID.Size = New System.Drawing.Size(170, 20)
        Me.stbClientID.TabIndex = 8
        '
        'stbToken
        '
        Me.stbToken.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbToken.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbToken, "Token")
        Me.stbToken.EntryErrorMSG = ""
        Me.stbToken.Location = New System.Drawing.Point(218, 81)
        Me.stbToken.Name = "stbToken"
        Me.stbToken.RegularExpression = ""
        Me.stbToken.Size = New System.Drawing.Size(170, 20)
        Me.stbToken.TabIndex = 10
        '
        'stbReferenceNo
        '
        Me.stbReferenceNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbReferenceNo.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbReferenceNo, "ReferenceNo")
        Me.stbReferenceNo.EntryErrorMSG = ""
        Me.stbReferenceNo.Location = New System.Drawing.Point(218, 104)
        Me.stbReferenceNo.Name = "stbReferenceNo"
        Me.stbReferenceNo.RegularExpression = ""
        Me.stbReferenceNo.Size = New System.Drawing.Size(170, 20)
        Me.stbReferenceNo.TabIndex = 12
        '
        'cboConnectionModeID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboConnectionModeID, "ConnectionMode,ConnectionModeID")
        Me.cboConnectionModeID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboConnectionModeID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboConnectionModeID.Location = New System.Drawing.Point(218, 127)
        Me.cboConnectionModeID.Name = "cboConnectionModeID"
        Me.cboConnectionModeID.Size = New System.Drawing.Size(170, 21)
        Me.cboConnectionModeID.TabIndex = 14
        '
        'cboDatabaseTypeID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboDatabaseTypeID, "DatabaseType,DatabaseTypeID")
        Me.cboDatabaseTypeID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDatabaseTypeID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboDatabaseTypeID.Location = New System.Drawing.Point(218, 150)
        Me.cboDatabaseTypeID.Name = "cboDatabaseTypeID"
        Me.cboDatabaseTypeID.Size = New System.Drawing.Size(170, 21)
        Me.cboDatabaseTypeID.TabIndex = 16
        '
        'stbDataSource
        '
        Me.stbDataSource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbDataSource.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbDataSource, "DataSource")
        Me.stbDataSource.EntryErrorMSG = ""
        Me.stbDataSource.Location = New System.Drawing.Point(218, 173)
        Me.stbDataSource.Name = "stbDataSource"
        Me.stbDataSource.RegularExpression = ""
        Me.stbDataSource.Size = New System.Drawing.Size(170, 20)
        Me.stbDataSource.TabIndex = 18
        '
        'stbDBName
        '
        Me.stbDBName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbDBName.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbDBName, "DBName")
        Me.stbDBName.EntryErrorMSG = ""
        Me.stbDBName.Location = New System.Drawing.Point(218, 196)
        Me.stbDBName.Name = "stbDBName"
        Me.stbDBName.RegularExpression = ""
        Me.stbDBName.Size = New System.Drawing.Size(170, 20)
        Me.stbDBName.TabIndex = 20
        '
        'nbxPort
        '
        Me.nbxPort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxPort.ControlCaption = "Port"
        Me.ebnSaveUpdate.SetDataMember(Me.nbxPort, "Port")
        Me.nbxPort.DecimalPlaces = -1
        Me.nbxPort.Location = New System.Drawing.Point(218, 219)
        Me.nbxPort.MaxValue = 0.0R
        Me.nbxPort.MinValue = 0.0R
        Me.nbxPort.MustEnterNumeric = True
        Me.nbxPort.Name = "nbxPort"
        Me.nbxPort.Size = New System.Drawing.Size(170, 20)
        Me.nbxPort.TabIndex = 22
        Me.nbxPort.Value = ""
        '
        'stbDBUsername
        '
        Me.stbDBUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbDBUsername.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbDBUsername, "DBUsername")
        Me.stbDBUsername.EntryErrorMSG = ""
        Me.stbDBUsername.Location = New System.Drawing.Point(218, 242)
        Me.stbDBUsername.Name = "stbDBUsername"
        Me.stbDBUsername.RegularExpression = ""
        Me.stbDBUsername.Size = New System.Drawing.Size(170, 20)
        Me.stbDBUsername.TabIndex = 24
        '
        'stbPassword
        '
        Me.stbPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPassword.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbPassword, "Password")
        Me.stbPassword.EntryErrorMSG = ""
        Me.stbPassword.Location = New System.Drawing.Point(218, 265)
        Me.stbPassword.Name = "stbPassword"
        Me.stbPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(108)
        Me.stbPassword.RegularExpression = ""
        Me.stbPassword.Size = New System.Drawing.Size(170, 20)
        Me.stbPassword.TabIndex = 26
        '
        'chkEnabled
        '
        Me.chkEnabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkEnabled, "Enabled")
        Me.chkEnabled.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkEnabled.Location = New System.Drawing.Point(12, 288)
        Me.chkEnabled.Name = "chkEnabled"
        Me.chkEnabled.Size = New System.Drawing.Size(215, 20)
        Me.chkEnabled.TabIndex = 28
        Me.chkEnabled.Text = "Enabled"
        '
        'fbnClose
        '
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(316, 345)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 3
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'lblAgentNo
        '
        Me.lblAgentNo.Location = New System.Drawing.Point(12, 12)
        Me.lblAgentNo.Name = "lblAgentNo"
        Me.lblAgentNo.Size = New System.Drawing.Size(200, 20)
        Me.lblAgentNo.TabIndex = 5
        Me.lblAgentNo.Text = "Agent No"
        '
        'lblAgentName
        '
        Me.lblAgentName.Location = New System.Drawing.Point(12, 35)
        Me.lblAgentName.Name = "lblAgentName"
        Me.lblAgentName.Size = New System.Drawing.Size(200, 20)
        Me.lblAgentName.TabIndex = 7
        Me.lblAgentName.Text = "Agent Name"
        '
        'lblClientID
        '
        Me.lblClientID.Location = New System.Drawing.Point(12, 58)
        Me.lblClientID.Name = "lblClientID"
        Me.lblClientID.Size = New System.Drawing.Size(200, 20)
        Me.lblClientID.TabIndex = 9
        Me.lblClientID.Text = "Client ID"
        '
        'lblToken
        '
        Me.lblToken.Location = New System.Drawing.Point(12, 81)
        Me.lblToken.Name = "lblToken"
        Me.lblToken.Size = New System.Drawing.Size(200, 20)
        Me.lblToken.TabIndex = 11
        Me.lblToken.Text = "Token"
        '
        'lblReferenceNo
        '
        Me.lblReferenceNo.Location = New System.Drawing.Point(12, 104)
        Me.lblReferenceNo.Name = "lblReferenceNo"
        Me.lblReferenceNo.Size = New System.Drawing.Size(200, 20)
        Me.lblReferenceNo.TabIndex = 13
        Me.lblReferenceNo.Text = "Reference No"
        '
        'lblConnectionModeID
        '
        Me.lblConnectionModeID.Location = New System.Drawing.Point(12, 127)
        Me.lblConnectionModeID.Name = "lblConnectionModeID"
        Me.lblConnectionModeID.Size = New System.Drawing.Size(200, 20)
        Me.lblConnectionModeID.TabIndex = 15
        Me.lblConnectionModeID.Text = "Connection Mode ID"
        '
        'lblDatabaseTypeID
        '
        Me.lblDatabaseTypeID.Location = New System.Drawing.Point(12, 150)
        Me.lblDatabaseTypeID.Name = "lblDatabaseTypeID"
        Me.lblDatabaseTypeID.Size = New System.Drawing.Size(200, 20)
        Me.lblDatabaseTypeID.TabIndex = 17
        Me.lblDatabaseTypeID.Text = "DatabaseTypeID"
        '
        'lblDataSource
        '
        Me.lblDataSource.Location = New System.Drawing.Point(12, 173)
        Me.lblDataSource.Name = "lblDataSource"
        Me.lblDataSource.Size = New System.Drawing.Size(200, 20)
        Me.lblDataSource.TabIndex = 19
        Me.lblDataSource.Text = "Data Source"
        '
        'lblDBName
        '
        Me.lblDBName.Location = New System.Drawing.Point(12, 196)
        Me.lblDBName.Name = "lblDBName"
        Me.lblDBName.Size = New System.Drawing.Size(200, 20)
        Me.lblDBName.TabIndex = 21
        Me.lblDBName.Text = "DB Name"
        '
        'lblPort
        '
        Me.lblPort.Location = New System.Drawing.Point(12, 219)
        Me.lblPort.Name = "lblPort"
        Me.lblPort.Size = New System.Drawing.Size(200, 20)
        Me.lblPort.TabIndex = 23
        Me.lblPort.Text = "Port"
        '
        'lblDBUsername
        '
        Me.lblDBUsername.Location = New System.Drawing.Point(12, 242)
        Me.lblDBUsername.Name = "lblDBUsername"
        Me.lblDBUsername.Size = New System.Drawing.Size(200, 20)
        Me.lblDBUsername.TabIndex = 25
        Me.lblDBUsername.Text = "DB Username"
        '
        'lblPassword
        '
        Me.lblPassword.Location = New System.Drawing.Point(12, 265)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(200, 20)
        Me.lblPassword.TabIndex = 27
        Me.lblPassword.Text = "Password"
        '
        'cboAgentNo
        '
        Me.cboAgentNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAgentNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboAgentNo.Location = New System.Drawing.Point(218, 9)
        Me.cboAgentNo.Name = "cboAgentNo"
        Me.cboAgentNo.Size = New System.Drawing.Size(170, 21)
        Me.cboAgentNo.TabIndex = 29
        '
        'frmINTAgents
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(415, 395)
        Me.Controls.Add(Me.cboAgentNo)
        Me.Controls.Add(Me.fbnSearch)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.lblAgentNo)
        Me.Controls.Add(Me.stbAgentName)
        Me.Controls.Add(Me.lblAgentName)
        Me.Controls.Add(Me.stbClientID)
        Me.Controls.Add(Me.lblClientID)
        Me.Controls.Add(Me.stbToken)
        Me.Controls.Add(Me.lblToken)
        Me.Controls.Add(Me.stbReferenceNo)
        Me.Controls.Add(Me.lblReferenceNo)
        Me.Controls.Add(Me.cboConnectionModeID)
        Me.Controls.Add(Me.lblConnectionModeID)
        Me.Controls.Add(Me.cboDatabaseTypeID)
        Me.Controls.Add(Me.lblDatabaseTypeID)
        Me.Controls.Add(Me.stbDataSource)
        Me.Controls.Add(Me.lblDataSource)
        Me.Controls.Add(Me.stbDBName)
        Me.Controls.Add(Me.lblDBName)
        Me.Controls.Add(Me.nbxPort)
        Me.Controls.Add(Me.lblPort)
        Me.Controls.Add(Me.stbDBUsername)
        Me.Controls.Add(Me.lblDBUsername)
        Me.Controls.Add(Me.stbPassword)
        Me.Controls.Add(Me.lblPassword)
        Me.Controls.Add(Me.chkEnabled)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmINTAgents"
        Me.Text = "INTAgents"
        Me.ResumeLayout(False)
        Me.PerformLayout()

End Sub

Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents lblAgentNo As System.Windows.Forms.Label
Friend WithEvents stbAgentName As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblAgentName As System.Windows.Forms.Label
Friend WithEvents stbClientID As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblClientID As System.Windows.Forms.Label
Friend WithEvents stbToken As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblToken As System.Windows.Forms.Label
Friend WithEvents stbReferenceNo As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblReferenceNo As System.Windows.Forms.Label
Friend WithEvents cboConnectionModeID As System.Windows.Forms.ComboBox
Friend WithEvents lblConnectionModeID As System.Windows.Forms.Label
Friend WithEvents cboDatabaseTypeID As System.Windows.Forms.ComboBox
Friend WithEvents lblDatabaseTypeID As System.Windows.Forms.Label
Friend WithEvents stbDataSource As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblDataSource As System.Windows.Forms.Label
Friend WithEvents stbDBName As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblDBName As System.Windows.Forms.Label
Friend WithEvents nbxPort As SyncSoft.Common.Win.Controls.NumericBox
Friend WithEvents lblPort As System.Windows.Forms.Label
Friend WithEvents stbDBUsername As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblDBUsername As System.Windows.Forms.Label
Friend WithEvents stbPassword As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents chkEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents cboAgentNo As System.Windows.Forms.ComboBox

End Class