
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccountWithdrawRequests : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAccountWithdrawRequests))
        Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.cboAccountBillModesID = New System.Windows.Forms.ComboBox()
        Me.stbAccountName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.dtpRequestDate = New System.Windows.Forms.DateTimePicker()
        Me.nbxAmount = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.stbNotes = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboWithdrawRequestReasonID = New System.Windows.Forms.ComboBox()
        Me.cboWithdrawTypeID = New System.Windows.Forms.ComboBox()
        Me.dtpLastValidityDateTime = New System.Windows.Forms.DateTimePicker()
        Me.cboAccountBillNo = New System.Windows.Forms.ComboBox()
        Me.nbxAccountBalance = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.nbxOutstandingBalance = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.stbRequestNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblRequestNo = New System.Windows.Forms.Label()
        Me.lblAccountBillModesID = New System.Windows.Forms.Label()
        Me.lblAccountBillNo = New System.Windows.Forms.Label()
        Me.lblAccountName = New System.Windows.Forms.Label()
        Me.lblRequestDate = New System.Windows.Forms.Label()
        Me.lblAmount = New System.Windows.Forms.Label()
        Me.lblWithdrawRequestReasonID = New System.Windows.Forms.Label()
        Me.lblNotes = New System.Windows.Forms.Label()
        Me.lblAccountBalance = New System.Windows.Forms.Label()
        Me.lblOutstandingBalance = New System.Windows.Forms.Label()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.lblWithdrawTypeID = New System.Windows.Forms.Label()
        Me.lblLastValidityDateTime = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'fbnSearch
        '
        Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnSearch.Location = New System.Drawing.Point(17, 384)
        Me.fbnSearch.Name = "fbnSearch"
        Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
        Me.fbnSearch.TabIndex = 25
        Me.fbnSearch.Text = "S&earch"
        Me.fbnSearch.UseVisualStyleBackColor = True
        Me.fbnSearch.Visible = False
        '
        'fbnDelete
        '
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(316, 384)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 26
        Me.fbnDelete.Tag = "AccountWithdrawRequests"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = False
        Me.fbnDelete.Visible = False
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(17, 411)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 27
        Me.ebnSaveUpdate.Tag = "AccountWithdrawRequests"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = False
        '
        'cboAccountBillModesID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboAccountBillModesID, "AccountBillModes,AccountBillModesID")
        Me.cboAccountBillModesID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountBillModesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboAccountBillModesID.Location = New System.Drawing.Point(189, 35)
        Me.cboAccountBillModesID.Name = "cboAccountBillModesID"
        Me.cboAccountBillModesID.Size = New System.Drawing.Size(199, 21)
        Me.cboAccountBillModesID.TabIndex = 3
        '
        'stbAccountName
        '
        Me.stbAccountName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAccountName.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbAccountName, "AccountName")
        Me.stbAccountName.EntryErrorMSG = ""
        Me.stbAccountName.Location = New System.Drawing.Point(189, 82)
        Me.stbAccountName.Multiline = True
        Me.stbAccountName.Name = "stbAccountName"
        Me.stbAccountName.ReadOnly = True
        Me.stbAccountName.RegularExpression = ""
        Me.stbAccountName.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.stbAccountName.Size = New System.Drawing.Size(199, 47)
        Me.stbAccountName.TabIndex = 8
        '
        'dtpRequestDate
        '
        Me.dtpRequestDate.Checked = False
        Me.ebnSaveUpdate.SetDataMember(Me.dtpRequestDate, "RequestDate")
        Me.dtpRequestDate.Location = New System.Drawing.Point(189, 203)
        Me.dtpRequestDate.Name = "dtpRequestDate"
        Me.dtpRequestDate.ShowCheckBox = True
        Me.dtpRequestDate.Size = New System.Drawing.Size(199, 20)
        Me.dtpRequestDate.TabIndex = 16
        '
        'nbxAmount
        '
        Me.nbxAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxAmount.ControlCaption = "Amount"
        Me.ebnSaveUpdate.SetDataMember(Me.nbxAmount, "Amount")
        Me.nbxAmount.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxAmount.DecimalPlaces = 2
        Me.nbxAmount.DenyNegativeEntryValue = True
        Me.nbxAmount.Location = New System.Drawing.Point(189, 229)
        Me.nbxAmount.MaxValue = 0.0R
        Me.nbxAmount.MinValue = 0.0R
        Me.nbxAmount.Multiline = True
        Me.nbxAmount.MustEnterNumeric = True
        Me.nbxAmount.Name = "nbxAmount"
        Me.nbxAmount.Size = New System.Drawing.Size(199, 20)
        Me.nbxAmount.TabIndex = 18
        Me.nbxAmount.Value = ""
        '
        'stbNotes
        '
        Me.stbNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbNotes.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbNotes, "Notes")
        Me.stbNotes.EntryErrorMSG = ""
        Me.stbNotes.Location = New System.Drawing.Point(189, 301)
        Me.stbNotes.Multiline = True
        Me.stbNotes.Name = "stbNotes"
        Me.stbNotes.RegularExpression = ""
        Me.stbNotes.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.stbNotes.Size = New System.Drawing.Size(199, 78)
        Me.stbNotes.TabIndex = 24
        '
        'cboWithdrawRequestReasonID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboWithdrawRequestReasonID, "WithdrawRequestReason,WithdrawRequestReasonID")
        Me.cboWithdrawRequestReasonID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWithdrawRequestReasonID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboWithdrawRequestReasonID.Location = New System.Drawing.Point(189, 252)
        Me.cboWithdrawRequestReasonID.Name = "cboWithdrawRequestReasonID"
        Me.cboWithdrawRequestReasonID.Size = New System.Drawing.Size(199, 21)
        Me.cboWithdrawRequestReasonID.TabIndex = 20
        '
        'cboWithdrawTypeID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboWithdrawTypeID, "WithdrawType,WithdrawTypeID")
        Me.cboWithdrawTypeID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWithdrawTypeID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboWithdrawTypeID.Location = New System.Drawing.Point(189, 179)
        Me.cboWithdrawTypeID.Name = "cboWithdrawTypeID"
        Me.cboWithdrawTypeID.Size = New System.Drawing.Size(199, 21)
        Me.cboWithdrawTypeID.TabIndex = 14
        '
        'dtpLastValidityDateTime
        '
        Me.dtpLastValidityDateTime.Checked = False
        Me.dtpLastValidityDateTime.CustomFormat = "dd MMM yyyy hh:mm tt"
        Me.ebnSaveUpdate.SetDataMember(Me.dtpLastValidityDateTime, "LastValidityDateTime")
        Me.dtpLastValidityDateTime.Enabled = False
        Me.dtpLastValidityDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpLastValidityDateTime.Location = New System.Drawing.Point(189, 277)
        Me.dtpLastValidityDateTime.Name = "dtpLastValidityDateTime"
        Me.dtpLastValidityDateTime.ShowCheckBox = True
        Me.dtpLastValidityDateTime.Size = New System.Drawing.Size(199, 20)
        Me.dtpLastValidityDateTime.TabIndex = 22
        '
        'cboAccountBillNo
        '
        Me.cboAccountBillNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboAccountBillNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAccountBillNo.BackColor = System.Drawing.SystemColors.Window
        Me.ebnSaveUpdate.SetDataMember(Me.cboAccountBillNo, "AccountBillNo")
        Me.cboAccountBillNo.DropDownWidth = 256
        Me.cboAccountBillNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboAccountBillNo.FormattingEnabled = True
        Me.cboAccountBillNo.ItemHeight = 13
        Me.cboAccountBillNo.Location = New System.Drawing.Point(189, 59)
        Me.cboAccountBillNo.Name = "cboAccountBillNo"
        Me.cboAccountBillNo.Size = New System.Drawing.Size(199, 21)
        Me.cboAccountBillNo.TabIndex = 5
        '
        'nbxAccountBalance
        '
        Me.nbxAccountBalance.BackColor = System.Drawing.SystemColors.Info
        Me.nbxAccountBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxAccountBalance.ControlCaption = "AccountBalance"
        Me.nbxAccountBalance.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxAccountBalance.DecimalPlaces = -1
        Me.nbxAccountBalance.Location = New System.Drawing.Point(189, 132)
        Me.nbxAccountBalance.MaxValue = 0.0R
        Me.nbxAccountBalance.MinValue = 0.0R
        Me.nbxAccountBalance.Multiline = True
        Me.nbxAccountBalance.MustEnterNumeric = True
        Me.nbxAccountBalance.Name = "nbxAccountBalance"
        Me.nbxAccountBalance.ReadOnly = True
        Me.nbxAccountBalance.Size = New System.Drawing.Size(199, 20)
        Me.nbxAccountBalance.TabIndex = 10
        Me.nbxAccountBalance.Value = ""
        '
        'nbxOutstandingBalance
        '
        Me.nbxOutstandingBalance.BackColor = System.Drawing.SystemColors.Info
        Me.nbxOutstandingBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxOutstandingBalance.ControlCaption = "Outstanding Balance"
        Me.nbxOutstandingBalance.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxOutstandingBalance.DecimalPlaces = -1
        Me.nbxOutstandingBalance.Location = New System.Drawing.Point(189, 156)
        Me.nbxOutstandingBalance.MaxValue = 0.0R
        Me.nbxOutstandingBalance.MinValue = 0.0R
        Me.nbxOutstandingBalance.Multiline = True
        Me.nbxOutstandingBalance.MustEnterNumeric = True
        Me.nbxOutstandingBalance.Name = "nbxOutstandingBalance"
        Me.nbxOutstandingBalance.ReadOnly = True
        Me.nbxOutstandingBalance.Size = New System.Drawing.Size(199, 20)
        Me.nbxOutstandingBalance.TabIndex = 12
        Me.nbxOutstandingBalance.Value = ""
        '
        'fbnClose
        '
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(316, 411)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 28
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'stbRequestNo
        '
        Me.stbRequestNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbRequestNo.CapitalizeFirstLetter = False
        Me.stbRequestNo.EntryErrorMSG = ""
        Me.stbRequestNo.Location = New System.Drawing.Point(189, 12)
        Me.stbRequestNo.Name = "stbRequestNo"
        Me.stbRequestNo.RegularExpression = ""
        Me.stbRequestNo.Size = New System.Drawing.Size(199, 20)
        Me.stbRequestNo.TabIndex = 1
        '
        'lblRequestNo
        '
        Me.lblRequestNo.Location = New System.Drawing.Point(12, 12)
        Me.lblRequestNo.Name = "lblRequestNo"
        Me.lblRequestNo.Size = New System.Drawing.Size(155, 20)
        Me.lblRequestNo.TabIndex = 0
        Me.lblRequestNo.Text = "Request No"
        '
        'lblAccountBillModesID
        '
        Me.lblAccountBillModesID.Location = New System.Drawing.Point(12, 35)
        Me.lblAccountBillModesID.Name = "lblAccountBillModesID"
        Me.lblAccountBillModesID.Size = New System.Drawing.Size(155, 20)
        Me.lblAccountBillModesID.TabIndex = 2
        Me.lblAccountBillModesID.Text = "Account Bill Mode"
        '
        'lblAccountBillNo
        '
        Me.lblAccountBillNo.Location = New System.Drawing.Point(12, 59)
        Me.lblAccountBillNo.Name = "lblAccountBillNo"
        Me.lblAccountBillNo.Size = New System.Drawing.Size(155, 20)
        Me.lblAccountBillNo.TabIndex = 4
        Me.lblAccountBillNo.Text = "Account Bill No"
        '
        'lblAccountName
        '
        Me.lblAccountName.Location = New System.Drawing.Point(12, 82)
        Me.lblAccountName.Name = "lblAccountName"
        Me.lblAccountName.Size = New System.Drawing.Size(155, 20)
        Me.lblAccountName.TabIndex = 7
        Me.lblAccountName.Text = "Account Name"
        '
        'lblRequestDate
        '
        Me.lblRequestDate.Location = New System.Drawing.Point(12, 203)
        Me.lblRequestDate.Name = "lblRequestDate"
        Me.lblRequestDate.Size = New System.Drawing.Size(155, 20)
        Me.lblRequestDate.TabIndex = 15
        Me.lblRequestDate.Text = "Request Date"
        '
        'lblAmount
        '
        Me.lblAmount.Location = New System.Drawing.Point(12, 229)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(155, 20)
        Me.lblAmount.TabIndex = 17
        Me.lblAmount.Text = "Amount"
        '
        'lblWithdrawRequestReasonID
        '
        Me.lblWithdrawRequestReasonID.Location = New System.Drawing.Point(12, 253)
        Me.lblWithdrawRequestReasonID.Name = "lblWithdrawRequestReasonID"
        Me.lblWithdrawRequestReasonID.Size = New System.Drawing.Size(155, 20)
        Me.lblWithdrawRequestReasonID.TabIndex = 19
        Me.lblWithdrawRequestReasonID.Text = "Withdraw Request Reason"
        '
        'lblNotes
        '
        Me.lblNotes.Location = New System.Drawing.Point(12, 301)
        Me.lblNotes.Name = "lblNotes"
        Me.lblNotes.Size = New System.Drawing.Size(155, 20)
        Me.lblNotes.TabIndex = 23
        Me.lblNotes.Text = "Notes"
        '
        'lblAccountBalance
        '
        Me.lblAccountBalance.Location = New System.Drawing.Point(12, 132)
        Me.lblAccountBalance.Name = "lblAccountBalance"
        Me.lblAccountBalance.Size = New System.Drawing.Size(155, 20)
        Me.lblAccountBalance.TabIndex = 9
        Me.lblAccountBalance.Text = "Account Balance"
        '
        'lblOutstandingBalance
        '
        Me.lblOutstandingBalance.Location = New System.Drawing.Point(12, 156)
        Me.lblOutstandingBalance.Name = "lblOutstandingBalance"
        Me.lblOutstandingBalance.Size = New System.Drawing.Size(155, 20)
        Me.lblOutstandingBalance.TabIndex = 11
        Me.lblOutstandingBalance.Text = "Outstanding Balance"
        '
        'btnLoad
        '
        Me.btnLoad.AccessibleDescription = ""
        Me.btnLoad.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoad.Location = New System.Drawing.Point(392, 58)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(46, 24)
        Me.btnLoad.TabIndex = 6
        Me.btnLoad.Tag = ""
        Me.btnLoad.Text = "&Load"
        Me.btnLoad.Visible = False
        '
        'lblWithdrawTypeID
        '
        Me.lblWithdrawTypeID.Location = New System.Drawing.Point(12, 179)
        Me.lblWithdrawTypeID.Name = "lblWithdrawTypeID"
        Me.lblWithdrawTypeID.Size = New System.Drawing.Size(155, 20)
        Me.lblWithdrawTypeID.TabIndex = 13
        Me.lblWithdrawTypeID.Text = "Withdraw Type"
        '
        'lblLastValidityDateTime
        '
        Me.lblLastValidityDateTime.Location = New System.Drawing.Point(12, 277)
        Me.lblLastValidityDateTime.Name = "lblLastValidityDateTime"
        Me.lblLastValidityDateTime.Size = New System.Drawing.Size(140, 20)
        Me.lblLastValidityDateTime.TabIndex = 21
        Me.lblLastValidityDateTime.Text = "Last Validity Date Time "
        '
        'frmAccountWithdrawRequests
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(441, 452)
        Me.Controls.Add(Me.lblLastValidityDateTime)
        Me.Controls.Add(Me.dtpLastValidityDateTime)
        Me.Controls.Add(Me.cboWithdrawTypeID)
        Me.Controls.Add(Me.lblWithdrawTypeID)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.nbxOutstandingBalance)
        Me.Controls.Add(Me.lblOutstandingBalance)
        Me.Controls.Add(Me.nbxAccountBalance)
        Me.Controls.Add(Me.lblAccountBalance)
        Me.Controls.Add(Me.cboAccountBillNo)
        Me.Controls.Add(Me.cboWithdrawRequestReasonID)
        Me.Controls.Add(Me.fbnSearch)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.stbRequestNo)
        Me.Controls.Add(Me.lblRequestNo)
        Me.Controls.Add(Me.cboAccountBillModesID)
        Me.Controls.Add(Me.lblAccountBillModesID)
        Me.Controls.Add(Me.lblAccountBillNo)
        Me.Controls.Add(Me.stbAccountName)
        Me.Controls.Add(Me.lblAccountName)
        Me.Controls.Add(Me.dtpRequestDate)
        Me.Controls.Add(Me.lblRequestDate)
        Me.Controls.Add(Me.nbxAmount)
        Me.Controls.Add(Me.lblAmount)
        Me.Controls.Add(Me.lblWithdrawRequestReasonID)
        Me.Controls.Add(Me.stbNotes)
        Me.Controls.Add(Me.lblNotes)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmAccountWithdrawRequests"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "AccountWithdrawRequests"
        Me.Text = "Account Withdraw Requests"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents stbRequestNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblRequestNo As System.Windows.Forms.Label
    Friend WithEvents cboAccountBillModesID As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccountBillModesID As System.Windows.Forms.Label
    Friend WithEvents lblAccountBillNo As System.Windows.Forms.Label
    Friend WithEvents stbAccountName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAccountName As System.Windows.Forms.Label
    Friend WithEvents dtpRequestDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblRequestDate As System.Windows.Forms.Label
    Friend WithEvents nbxAmount As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents lblWithdrawRequestReasonID As System.Windows.Forms.Label
    Friend WithEvents stbNotes As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblNotes As System.Windows.Forms.Label
    Friend WithEvents cboWithdrawRequestReasonID As System.Windows.Forms.ComboBox
    Friend WithEvents cboAccountBillNo As System.Windows.Forms.ComboBox
    Friend WithEvents nbxAccountBalance As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblAccountBalance As System.Windows.Forms.Label
    Friend WithEvents nbxOutstandingBalance As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblOutstandingBalance As System.Windows.Forms.Label
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents cboWithdrawTypeID As System.Windows.Forms.ComboBox
    Friend WithEvents lblWithdrawTypeID As System.Windows.Forms.Label
    Friend WithEvents lblLastValidityDateTime As System.Windows.Forms.Label
    Friend WithEvents dtpLastValidityDateTime As System.Windows.Forms.DateTimePicker

End Class