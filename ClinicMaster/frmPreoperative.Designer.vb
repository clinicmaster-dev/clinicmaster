
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPreoperative : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPreoperative))
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle37 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle36 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle38 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle45 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle39 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle40 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle41 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle42 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle43 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle44 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.chkExplainationOfSurgery = New System.Windows.Forms.CheckBox()
        Me.chkBathed = New System.Windows.Forms.CheckBox()
        Me.chkKnownAllergy = New System.Windows.Forms.CheckBox()
        Me.chkCorrectIdentity = New System.Windows.Forms.CheckBox()
        Me.chkCorrectNotes = New System.Windows.Forms.CheckBox()
        Me.chkNPMLast6hrs = New System.Windows.Forms.CheckBox()
        Me.chkConsentSigned = New System.Windows.Forms.CheckBox()
        Me.chkDenturesRemoved = New System.Windows.Forms.CheckBox()
        Me.chkJewelleryRemoved = New System.Windows.Forms.CheckBox()
        Me.nbxBMI = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.stbBedNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbRoomNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblRoomNo = New System.Windows.Forms.Label()
        Me.lblBedNo = New System.Windows.Forms.Label()
        Me.stbWard = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblWard = New System.Windows.Forms.Label()
        Me.stbPackage = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblPackageName = New System.Windows.Forms.Label()
        Me.stbAdmissionStatus = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblAdmissionStatus = New System.Windows.Forms.Label()
        Me.stbTotalIPDDoctorRounds = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblTotalIPDDoctorRounds = New System.Windows.Forms.Label()
        Me.btnLoadRounds = New System.Windows.Forms.Button()
        Me.pnlAlerts = New System.Windows.Forms.Panel()
        Me.btnLoadList = New System.Windows.Forms.Button()
        Me.lblInWardAdmissions = New System.Windows.Forms.Label()
        Me.pnlCreateNewRound = New System.Windows.Forms.Panel()
        Me.chkCreateNewRound = New System.Windows.Forms.CheckBox()
        Me.cboRoundNo = New System.Windows.Forms.ComboBox()
        Me.stbAdmissionNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.btnFindRoundNo = New System.Windows.Forms.Button()
        Me.stbVisitNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblVisitNo = New System.Windows.Forms.Label()
        Me.stbAdmissionDateTime = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblAdmissionDateTime = New System.Windows.Forms.Label()
        Me.btnFindAdmissionNo = New System.Windows.Forms.Button()
        Me.lblAdmissionNo = New System.Windows.Forms.Label()
        Me.stbBillCustomerName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblBillCustomerName = New System.Windows.Forms.Label()
        Me.spbPhoto = New SyncSoft.Common.Win.Controls.SmartPictureBox()
        Me.stbPatientNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblPatientsNo = New System.Windows.Forms.Label()
        Me.stbBillMode = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblBillMode = New System.Windows.Forms.Label()
        Me.stbBillAccountNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbAge = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbGender = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblBillAccountNo = New System.Windows.Forms.Label()
        Me.lblAge = New System.Windows.Forms.Label()
        Me.lblGenderID = New System.Windows.Forms.Label()
        Me.stbFullName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblFullName = New System.Windows.Forms.Label()
        Me.pnlBill = New System.Windows.Forms.Panel()
        Me.lblBillWords = New System.Windows.Forms.Label()
        Me.stbBillForItem = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbBillWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblBillForItem = New System.Windows.Forms.Label()
        Me.lblRoundNo = New System.Windows.Forms.Label()
        Me.cboStaffNo = New System.Windows.Forms.ComboBox()
        Me.lblStaffNo = New System.Windows.Forms.Label()
        Me.stbRoundDateTime = New System.Windows.Forms.DateTimePicker()
        Me.lblRoundDateTime = New System.Windows.Forms.Label()
        Me.tbcDrRoles = New System.Windows.Forms.TabControl()
        Me.tpgGeneral = New System.Windows.Forms.TabPage()
        Me.tbcGeneral = New System.Windows.Forms.TabControl()
        Me.tpgClinicalFindings = New System.Windows.Forms.TabPage()
        Me.lblBMI = New System.Windows.Forms.Label()
        Me.grpTriage = New System.Windows.Forms.GroupBox()
        Me.nbxMUAC = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblMUAC = New System.Windows.Forms.Label()
        Me.nbxRespirationRate = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblRespirationRate = New System.Windows.Forms.Label()
        Me.stbBloodPressure = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.nbxWeight = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblBodySurfaceArea = New System.Windows.Forms.Label()
        Me.lblWeight = New System.Windows.Forms.Label()
        Me.nbxBodySurfaceArea = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.nbxTemperature = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblHeadCircum = New System.Windows.Forms.Label()
        Me.lblTemperature = New System.Windows.Forms.Label()
        Me.nbxHeadCircum = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.nbxHeight = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblBloodPressure = New System.Windows.Forms.Label()
        Me.lblHeight = New System.Windows.Forms.Label()
        Me.lblPulse = New System.Windows.Forms.Label()
        Me.nbxPulse = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.btnLoadTemplate = New System.Windows.Forms.Button()
        Me.lblClinicalNotes = New System.Windows.Forms.Label()
        Me.stbClinicalNotes = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.tpgDiagnosis = New System.Windows.Forms.TabPage()
        Me.dgvDiagnosis = New System.Windows.Forms.DataGridView()
        Me.ColDiagnosisSelect = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.colICDDiagnosisCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDiseaseCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColDiagnosedBy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDiseaseCategory = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNotes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDiagnosisSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.tpgProcedures = New System.Windows.Forms.TabPage()
        Me.dgvProcedures = New System.Windows.Forms.DataGridView()
        Me.colProcedureCode = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colICDProcedureCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colProcedureQuantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colProcedureUnitPrice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colProcedureNotes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colProcedureItemStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colProcedurePayStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colProceduresSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.pnlNavigateRounds = New System.Windows.Forms.Panel()
        Me.chkNavigateRounds = New System.Windows.Forms.CheckBox()
        Me.navRounds = New SyncSoft.Common.Win.Controls.DataNavigator()
        Me.pnlAlerts.SuspendLayout()
        Me.pnlCreateNewRound.SuspendLayout()
        CType(Me.spbPhoto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBill.SuspendLayout()
        Me.tbcDrRoles.SuspendLayout()
        Me.tpgGeneral.SuspendLayout()
        Me.tbcGeneral.SuspendLayout()
        Me.tpgClinicalFindings.SuspendLayout()
        Me.grpTriage.SuspendLayout()
        Me.tpgDiagnosis.SuspendLayout()
        CType(Me.dgvDiagnosis, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgProcedures.SuspendLayout()
        CType(Me.dgvProcedures, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlNavigateRounds.SuspendLayout()
        Me.SuspendLayout()
        '
        'fbnSearch
        '
        Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnSearch.Location = New System.Drawing.Point(5, 481)
        Me.fbnSearch.Name = "fbnSearch"
        Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
        Me.fbnSearch.TabIndex = 0
        Me.fbnSearch.Text = "S&earch"
        Me.fbnSearch.UseVisualStyleBackColor = True
        Me.fbnSearch.Visible = False
        '
        'fbnDelete
        '
        Me.fbnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(942, 475)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 1
        Me.fbnDelete.Tag = "Preoperative"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = False
        Me.fbnDelete.Visible = False
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(5, 508)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 2
        Me.ebnSaveUpdate.Tag = "Preoperative"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = False
        '
        'chkExplainationOfSurgery
        '
        Me.chkExplainationOfSurgery.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkExplainationOfSurgery, "ExplainationOfSurgery")
        Me.chkExplainationOfSurgery.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkExplainationOfSurgery.Location = New System.Drawing.Point(622, 8)
        Me.chkExplainationOfSurgery.Name = "chkExplainationOfSurgery"
        Me.chkExplainationOfSurgery.Size = New System.Drawing.Size(200, 20)
        Me.chkExplainationOfSurgery.TabIndex = 6
        Me.chkExplainationOfSurgery.Text = "Explaination O fSurgery Done"
        '
        'chkBathed
        '
        Me.chkBathed.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkBathed, "Bathed")
        Me.chkBathed.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkBathed.Location = New System.Drawing.Point(622, 31)
        Me.chkBathed.Name = "chkBathed"
        Me.chkBathed.Size = New System.Drawing.Size(200, 20)
        Me.chkBathed.TabIndex = 7
        Me.chkBathed.Text = "Bathed"
        '
        'chkKnownAllergy
        '
        Me.chkKnownAllergy.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkKnownAllergy, "KnownAllergy")
        Me.chkKnownAllergy.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkKnownAllergy.Location = New System.Drawing.Point(622, 54)
        Me.chkKnownAllergy.Name = "chkKnownAllergy"
        Me.chkKnownAllergy.Size = New System.Drawing.Size(200, 20)
        Me.chkKnownAllergy.TabIndex = 8
        Me.chkKnownAllergy.Text = "Known Allergy"
        '
        'chkCorrectIdentity
        '
        Me.chkCorrectIdentity.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkCorrectIdentity, "CorrectIdentity")
        Me.chkCorrectIdentity.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkCorrectIdentity.Location = New System.Drawing.Point(622, 77)
        Me.chkCorrectIdentity.Name = "chkCorrectIdentity"
        Me.chkCorrectIdentity.Size = New System.Drawing.Size(200, 20)
        Me.chkCorrectIdentity.TabIndex = 9
        Me.chkCorrectIdentity.Text = "Correct Identity/Name tag"
        '
        'chkCorrectNotes
        '
        Me.chkCorrectNotes.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkCorrectNotes, "CorrectNotes")
        Me.chkCorrectNotes.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkCorrectNotes.Location = New System.Drawing.Point(622, 100)
        Me.chkCorrectNotes.Name = "chkCorrectNotes"
        Me.chkCorrectNotes.Size = New System.Drawing.Size(200, 20)
        Me.chkCorrectNotes.TabIndex = 10
        Me.chkCorrectNotes.Text = "Correct Patient Notes"
        '
        'chkNPMLast6hrs
        '
        Me.chkNPMLast6hrs.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkNPMLast6hrs, "NPMLast6hrs")
        Me.chkNPMLast6hrs.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkNPMLast6hrs.Location = New System.Drawing.Point(622, 122)
        Me.chkNPMLast6hrs.Name = "chkNPMLast6hrs"
        Me.chkNPMLast6hrs.Size = New System.Drawing.Size(200, 20)
        Me.chkNPMLast6hrs.TabIndex = 11
        Me.chkNPMLast6hrs.Text = "NPM/NPO in the Last6 hours"
        '
        'chkConsentSigned
        '
        Me.chkConsentSigned.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkConsentSigned, "ConsentSigned")
        Me.chkConsentSigned.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkConsentSigned.Location = New System.Drawing.Point(622, 145)
        Me.chkConsentSigned.Name = "chkConsentSigned"
        Me.chkConsentSigned.Size = New System.Drawing.Size(200, 20)
        Me.chkConsentSigned.TabIndex = 12
        Me.chkConsentSigned.Text = "Consent Signed"
        '
        'chkDenturesRemoved
        '
        Me.chkDenturesRemoved.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkDenturesRemoved, "DenturesRemoved")
        Me.chkDenturesRemoved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkDenturesRemoved.Location = New System.Drawing.Point(622, 169)
        Me.chkDenturesRemoved.Name = "chkDenturesRemoved"
        Me.chkDenturesRemoved.Size = New System.Drawing.Size(200, 20)
        Me.chkDenturesRemoved.TabIndex = 13
        Me.chkDenturesRemoved.Text = "Dentures Removed"
        '
        'chkJewelleryRemoved
        '
        Me.chkJewelleryRemoved.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkJewelleryRemoved, "JewelleryRemoved")
        Me.chkJewelleryRemoved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkJewelleryRemoved.Location = New System.Drawing.Point(622, 192)
        Me.chkJewelleryRemoved.Name = "chkJewelleryRemoved"
        Me.chkJewelleryRemoved.Size = New System.Drawing.Size(200, 20)
        Me.chkJewelleryRemoved.TabIndex = 14
        Me.chkJewelleryRemoved.Text = "Jewellery Removed"
        '
        'nbxBMI
        '
        Me.nbxBMI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxBMI.ControlCaption = "BMI"
        Me.nbxBMI.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
        Me.nbxBMI.DecimalPlaces = 2
        Me.nbxBMI.Enabled = False
        Me.nbxBMI.Location = New System.Drawing.Point(145, 206)
        Me.nbxBMI.MaxLength = 12
        Me.nbxBMI.MaxValue = 0.0R
        Me.nbxBMI.MinValue = 0.0R
        Me.nbxBMI.MustEnterNumeric = True
        Me.nbxBMI.Name = "nbxBMI"
        Me.nbxBMI.Size = New System.Drawing.Size(145, 20)
        Me.nbxBMI.TabIndex = 30
        Me.nbxBMI.Value = ""
        '
        'fbnClose
        '
        Me.fbnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(942, 502)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 3
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'stbBedNo
        '
        Me.stbBedNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBedNo.CapitalizeFirstLetter = False
        Me.stbBedNo.Enabled = False
        Me.stbBedNo.EntryErrorMSG = ""
        Me.stbBedNo.Location = New System.Drawing.Point(810, 115)
        Me.stbBedNo.MaxLength = 60
        Me.stbBedNo.Name = "stbBedNo"
        Me.stbBedNo.RegularExpression = ""
        Me.stbBedNo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbBedNo.Size = New System.Drawing.Size(108, 20)
        Me.stbBedNo.TabIndex = 115
        '
        'stbRoomNo
        '
        Me.stbRoomNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbRoomNo.CapitalizeFirstLetter = False
        Me.stbRoomNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.stbRoomNo.Enabled = False
        Me.stbRoomNo.EntryErrorMSG = ""
        Me.stbRoomNo.Location = New System.Drawing.Point(810, 95)
        Me.stbRoomNo.MaxLength = 7
        Me.stbRoomNo.Name = "stbRoomNo"
        Me.stbRoomNo.RegularExpression = ""
        Me.stbRoomNo.Size = New System.Drawing.Size(108, 20)
        Me.stbRoomNo.TabIndex = 113
        '
        'lblRoomNo
        '
        Me.lblRoomNo.Location = New System.Drawing.Point(735, 94)
        Me.lblRoomNo.Name = "lblRoomNo"
        Me.lblRoomNo.Size = New System.Drawing.Size(71, 18)
        Me.lblRoomNo.TabIndex = 112
        Me.lblRoomNo.Text = "Room No"
        '
        'lblBedNo
        '
        Me.lblBedNo.Location = New System.Drawing.Point(735, 112)
        Me.lblBedNo.Name = "lblBedNo"
        Me.lblBedNo.Size = New System.Drawing.Size(71, 18)
        Me.lblBedNo.TabIndex = 114
        Me.lblBedNo.Text = "Bed No"
        '
        'stbWard
        '
        Me.stbWard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbWard.CapitalizeFirstLetter = False
        Me.stbWard.Enabled = False
        Me.stbWard.EntryErrorMSG = ""
        Me.stbWard.Location = New System.Drawing.Point(810, 74)
        Me.stbWard.MaxLength = 60
        Me.stbWard.Name = "stbWard"
        Me.stbWard.RegularExpression = ""
        Me.stbWard.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbWard.Size = New System.Drawing.Size(108, 20)
        Me.stbWard.TabIndex = 111
        '
        'lblWard
        '
        Me.lblWard.Location = New System.Drawing.Point(735, 73)
        Me.lblWard.Name = "lblWard"
        Me.lblWard.Size = New System.Drawing.Size(71, 18)
        Me.lblWard.TabIndex = 110
        Me.lblWard.Text = "Ward"
        '
        'stbPackage
        '
        Me.stbPackage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPackage.CapitalizeFirstLetter = False
        Me.stbPackage.Enabled = False
        Me.stbPackage.EntryErrorMSG = ""
        Me.stbPackage.Location = New System.Drawing.Point(810, 9)
        Me.stbPackage.MaxLength = 60
        Me.stbPackage.Name = "stbPackage"
        Me.stbPackage.RegularExpression = ""
        Me.stbPackage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbPackage.Size = New System.Drawing.Size(108, 20)
        Me.stbPackage.TabIndex = 105
        '
        'lblPackageName
        '
        Me.lblPackageName.Location = New System.Drawing.Point(738, 8)
        Me.lblPackageName.Name = "lblPackageName"
        Me.lblPackageName.Size = New System.Drawing.Size(68, 20)
        Me.lblPackageName.TabIndex = 104
        Me.lblPackageName.Text = "Package"
        '
        'stbAdmissionStatus
        '
        Me.stbAdmissionStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAdmissionStatus.CapitalizeFirstLetter = False
        Me.stbAdmissionStatus.Enabled = False
        Me.stbAdmissionStatus.EntryErrorMSG = ""
        Me.stbAdmissionStatus.Location = New System.Drawing.Point(810, 52)
        Me.stbAdmissionStatus.MaxLength = 60
        Me.stbAdmissionStatus.Name = "stbAdmissionStatus"
        Me.stbAdmissionStatus.RegularExpression = ""
        Me.stbAdmissionStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbAdmissionStatus.Size = New System.Drawing.Size(108, 20)
        Me.stbAdmissionStatus.TabIndex = 109
        '
        'lblAdmissionStatus
        '
        Me.lblAdmissionStatus.Location = New System.Drawing.Point(735, 50)
        Me.lblAdmissionStatus.Name = "lblAdmissionStatus"
        Me.lblAdmissionStatus.Size = New System.Drawing.Size(71, 20)
        Me.lblAdmissionStatus.TabIndex = 108
        Me.lblAdmissionStatus.Text = "Status"
        '
        'stbTotalIPDDoctorRounds
        '
        Me.stbTotalIPDDoctorRounds.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalIPDDoctorRounds.CapitalizeFirstLetter = False
        Me.stbTotalIPDDoctorRounds.Enabled = False
        Me.stbTotalIPDDoctorRounds.EntryErrorMSG = ""
        Me.stbTotalIPDDoctorRounds.Location = New System.Drawing.Point(810, 31)
        Me.stbTotalIPDDoctorRounds.MaxLength = 60
        Me.stbTotalIPDDoctorRounds.Name = "stbTotalIPDDoctorRounds"
        Me.stbTotalIPDDoctorRounds.RegularExpression = ""
        Me.stbTotalIPDDoctorRounds.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbTotalIPDDoctorRounds.Size = New System.Drawing.Size(108, 20)
        Me.stbTotalIPDDoctorRounds.TabIndex = 107
        '
        'lblTotalIPDDoctorRounds
        '
        Me.lblTotalIPDDoctorRounds.Location = New System.Drawing.Point(735, 29)
        Me.lblTotalIPDDoctorRounds.Name = "lblTotalIPDDoctorRounds"
        Me.lblTotalIPDDoctorRounds.Size = New System.Drawing.Size(71, 20)
        Me.lblTotalIPDDoctorRounds.TabIndex = 106
        Me.lblTotalIPDDoctorRounds.Text = "Total Rounds"
        '
        'btnLoadRounds
        '
        Me.btnLoadRounds.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnLoadRounds.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoadRounds.Location = New System.Drawing.Point(257, 67)
        Me.btnLoadRounds.Name = "btnLoadRounds"
        Me.btnLoadRounds.Size = New System.Drawing.Size(49, 24)
        Me.btnLoadRounds.TabIndex = 81
        Me.btnLoadRounds.Tag = ""
        Me.btnLoadRounds.Text = "&Load"
        '
        'pnlAlerts
        '
        Me.pnlAlerts.Controls.Add(Me.btnLoadList)
        Me.pnlAlerts.Controls.Add(Me.lblInWardAdmissions)
        Me.pnlAlerts.Location = New System.Drawing.Point(13, 152)
        Me.pnlAlerts.Name = "pnlAlerts"
        Me.pnlAlerts.Size = New System.Drawing.Size(289, 29)
        Me.pnlAlerts.TabIndex = 117
        '
        'btnLoadList
        '
        Me.btnLoadList.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnLoadList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoadList.Location = New System.Drawing.Point(199, 3)
        Me.btnLoadList.Name = "btnLoadList"
        Me.btnLoadList.Size = New System.Drawing.Size(72, 24)
        Me.btnLoadList.TabIndex = 1
        Me.btnLoadList.Tag = ""
        Me.btnLoadList.Text = "&Load List"
        '
        'lblInWardAdmissions
        '
        Me.lblInWardAdmissions.AccessibleDescription = ""
        Me.lblInWardAdmissions.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInWardAdmissions.ForeColor = System.Drawing.Color.Red
        Me.lblInWardAdmissions.Location = New System.Drawing.Point(8, 4)
        Me.lblInWardAdmissions.Name = "lblInWardAdmissions"
        Me.lblInWardAdmissions.Size = New System.Drawing.Size(179, 20)
        Me.lblInWardAdmissions.TabIndex = 0
        Me.lblInWardAdmissions.Text = "In Ward Admissions:"
        Me.lblInWardAdmissions.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'pnlCreateNewRound
        '
        Me.pnlCreateNewRound.Controls.Add(Me.chkCreateNewRound)
        Me.pnlCreateNewRound.Location = New System.Drawing.Point(2, 14)
        Me.pnlCreateNewRound.Name = "pnlCreateNewRound"
        Me.pnlCreateNewRound.Size = New System.Drawing.Size(169, 29)
        Me.pnlCreateNewRound.TabIndex = 74
        '
        'chkCreateNewRound
        '
        Me.chkCreateNewRound.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkCreateNewRound.Checked = True
        Me.chkCreateNewRound.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCreateNewRound.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkCreateNewRound.Location = New System.Drawing.Point(13, 3)
        Me.chkCreateNewRound.Name = "chkCreateNewRound"
        Me.chkCreateNewRound.Size = New System.Drawing.Size(137, 20)
        Me.chkCreateNewRound.TabIndex = 0
        Me.chkCreateNewRound.Text = "Create New Round"
        '
        'cboRoundNo
        '
        Me.cboRoundNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboRoundNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboRoundNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboRoundNo.FormattingEnabled = True
        Me.cboRoundNo.Location = New System.Drawing.Point(136, 69)
        Me.cboRoundNo.MaxLength = 20
        Me.cboRoundNo.Name = "cboRoundNo"
        Me.cboRoundNo.Size = New System.Drawing.Size(115, 21)
        Me.cboRoundNo.TabIndex = 80
        '
        'stbAdmissionNo
        '
        Me.stbAdmissionNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAdmissionNo.CapitalizeFirstLetter = False
        Me.stbAdmissionNo.EntryErrorMSG = ""
        Me.stbAdmissionNo.Location = New System.Drawing.Point(136, 46)
        Me.stbAdmissionNo.MaxLength = 20
        Me.stbAdmissionNo.Name = "stbAdmissionNo"
        Me.stbAdmissionNo.RegularExpression = ""
        Me.stbAdmissionNo.Size = New System.Drawing.Size(170, 20)
        Me.stbAdmissionNo.TabIndex = 77
        '
        'btnFindRoundNo
        '
        Me.btnFindRoundNo.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnFindRoundNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFindRoundNo.Image = CType(resources.GetObject("btnFindRoundNo.Image"), System.Drawing.Image)
        Me.btnFindRoundNo.Location = New System.Drawing.Point(103, 72)
        Me.btnFindRoundNo.Name = "btnFindRoundNo"
        Me.btnFindRoundNo.Size = New System.Drawing.Size(27, 21)
        Me.btnFindRoundNo.TabIndex = 79
        '
        'stbVisitNo
        '
        Me.stbVisitNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbVisitNo.CapitalizeFirstLetter = False
        Me.stbVisitNo.Enabled = False
        Me.stbVisitNo.EntryErrorMSG = ""
        Me.stbVisitNo.Location = New System.Drawing.Point(442, 94)
        Me.stbVisitNo.MaxLength = 20
        Me.stbVisitNo.Name = "stbVisitNo"
        Me.stbVisitNo.RegularExpression = ""
        Me.stbVisitNo.Size = New System.Drawing.Size(145, 20)
        Me.stbVisitNo.TabIndex = 95
        '
        'lblVisitNo
        '
        Me.lblVisitNo.Location = New System.Drawing.Point(308, 94)
        Me.lblVisitNo.Name = "lblVisitNo"
        Me.lblVisitNo.Size = New System.Drawing.Size(136, 20)
        Me.lblVisitNo.TabIndex = 94
        Me.lblVisitNo.Text = "Visit No"
        '
        'stbAdmissionDateTime
        '
        Me.stbAdmissionDateTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAdmissionDateTime.CapitalizeFirstLetter = False
        Me.stbAdmissionDateTime.Enabled = False
        Me.stbAdmissionDateTime.EntryErrorMSG = ""
        Me.stbAdmissionDateTime.Location = New System.Drawing.Point(442, 73)
        Me.stbAdmissionDateTime.MaxLength = 60
        Me.stbAdmissionDateTime.Name = "stbAdmissionDateTime"
        Me.stbAdmissionDateTime.RegularExpression = ""
        Me.stbAdmissionDateTime.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbAdmissionDateTime.Size = New System.Drawing.Size(145, 20)
        Me.stbAdmissionDateTime.TabIndex = 93
        '
        'lblAdmissionDateTime
        '
        Me.lblAdmissionDateTime.Location = New System.Drawing.Point(308, 72)
        Me.lblAdmissionDateTime.Name = "lblAdmissionDateTime"
        Me.lblAdmissionDateTime.Size = New System.Drawing.Size(136, 20)
        Me.lblAdmissionDateTime.TabIndex = 92
        Me.lblAdmissionDateTime.Text = "Admission Date Time"
        '
        'btnFindAdmissionNo
        '
        Me.btnFindAdmissionNo.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnFindAdmissionNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFindAdmissionNo.Image = CType(resources.GetObject("btnFindAdmissionNo.Image"), System.Drawing.Image)
        Me.btnFindAdmissionNo.Location = New System.Drawing.Point(103, 47)
        Me.btnFindAdmissionNo.Name = "btnFindAdmissionNo"
        Me.btnFindAdmissionNo.Size = New System.Drawing.Size(27, 21)
        Me.btnFindAdmissionNo.TabIndex = 76
        '
        'lblAdmissionNo
        '
        Me.lblAdmissionNo.Location = New System.Drawing.Point(12, 46)
        Me.lblAdmissionNo.Name = "lblAdmissionNo"
        Me.lblAdmissionNo.Size = New System.Drawing.Size(84, 20)
        Me.lblAdmissionNo.TabIndex = 75
        Me.lblAdmissionNo.Text = "Admission No"
        '
        'stbBillCustomerName
        '
        Me.stbBillCustomerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBillCustomerName.CapitalizeFirstLetter = False
        Me.stbBillCustomerName.EntryErrorMSG = ""
        Me.stbBillCustomerName.Location = New System.Drawing.Point(442, 116)
        Me.stbBillCustomerName.MaxLength = 41
        Me.stbBillCustomerName.Multiline = True
        Me.stbBillCustomerName.Name = "stbBillCustomerName"
        Me.stbBillCustomerName.ReadOnly = True
        Me.stbBillCustomerName.RegularExpression = ""
        Me.stbBillCustomerName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbBillCustomerName.Size = New System.Drawing.Size(145, 29)
        Me.stbBillCustomerName.TabIndex = 97
        '
        'lblBillCustomerName
        '
        Me.lblBillCustomerName.Location = New System.Drawing.Point(308, 119)
        Me.lblBillCustomerName.Name = "lblBillCustomerName"
        Me.lblBillCustomerName.Size = New System.Drawing.Size(136, 20)
        Me.lblBillCustomerName.TabIndex = 96
        Me.lblBillCustomerName.Text = "To-Bill Customer"
        '
        'spbPhoto
        '
        Me.spbPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.spbPhoto.Image = CType(resources.GetObject("spbPhoto.Image"), System.Drawing.Image)
        Me.spbPhoto.ImageSizeLimit = CType(200000, Long)
        Me.spbPhoto.InitialImage = CType(resources.GetObject("spbPhoto.InitialImage"), System.Drawing.Image)
        Me.spbPhoto.Location = New System.Drawing.Point(921, 9)
        Me.spbPhoto.Name = "spbPhoto"
        Me.spbPhoto.ReadOnly = True
        Me.spbPhoto.Size = New System.Drawing.Size(100, 89)
        Me.spbPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.spbPhoto.TabIndex = 123
        Me.spbPhoto.TabStop = False
        '
        'stbPatientNo
        '
        Me.stbPatientNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPatientNo.CapitalizeFirstLetter = False
        Me.stbPatientNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.stbPatientNo.Enabled = False
        Me.stbPatientNo.EntryErrorMSG = ""
        Me.stbPatientNo.Location = New System.Drawing.Point(418, 29)
        Me.stbPatientNo.MaxLength = 7
        Me.stbPatientNo.Name = "stbPatientNo"
        Me.stbPatientNo.RegularExpression = ""
        Me.stbPatientNo.Size = New System.Drawing.Size(170, 20)
        Me.stbPatientNo.TabIndex = 89
        '
        'lblPatientsNo
        '
        Me.lblPatientsNo.Location = New System.Drawing.Point(308, 29)
        Me.lblPatientsNo.Name = "lblPatientsNo"
        Me.lblPatientsNo.Size = New System.Drawing.Size(110, 20)
        Me.lblPatientsNo.TabIndex = 88
        Me.lblPatientsNo.Text = "Patient's No."
        '
        'stbBillMode
        '
        Me.stbBillMode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBillMode.CapitalizeFirstLetter = False
        Me.stbBillMode.Enabled = False
        Me.stbBillMode.EntryErrorMSG = ""
        Me.stbBillMode.Location = New System.Drawing.Point(656, 51)
        Me.stbBillMode.MaxLength = 60
        Me.stbBillMode.Name = "stbBillMode"
        Me.stbBillMode.RegularExpression = ""
        Me.stbBillMode.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbBillMode.Size = New System.Drawing.Size(76, 20)
        Me.stbBillMode.TabIndex = 103
        '
        'lblBillMode
        '
        Me.lblBillMode.Location = New System.Drawing.Point(598, 53)
        Me.lblBillMode.Name = "lblBillMode"
        Me.lblBillMode.Size = New System.Drawing.Size(55, 20)
        Me.lblBillMode.TabIndex = 102
        Me.lblBillMode.Text = "Bill Mode"
        '
        'stbBillAccountNo
        '
        Me.stbBillAccountNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBillAccountNo.CapitalizeFirstLetter = False
        Me.stbBillAccountNo.Enabled = False
        Me.stbBillAccountNo.EntryErrorMSG = ""
        Me.stbBillAccountNo.Location = New System.Drawing.Point(442, 52)
        Me.stbBillAccountNo.MaxLength = 60
        Me.stbBillAccountNo.Name = "stbBillAccountNo"
        Me.stbBillAccountNo.RegularExpression = ""
        Me.stbBillAccountNo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbBillAccountNo.Size = New System.Drawing.Size(145, 20)
        Me.stbBillAccountNo.TabIndex = 91
        '
        'stbAge
        '
        Me.stbAge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAge.CapitalizeFirstLetter = False
        Me.stbAge.Enabled = False
        Me.stbAge.EntryErrorMSG = ""
        Me.stbAge.Location = New System.Drawing.Point(656, 9)
        Me.stbAge.MaxLength = 60
        Me.stbAge.Name = "stbAge"
        Me.stbAge.RegularExpression = ""
        Me.stbAge.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbAge.Size = New System.Drawing.Size(76, 20)
        Me.stbAge.TabIndex = 99
        '
        'stbGender
        '
        Me.stbGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbGender.CapitalizeFirstLetter = False
        Me.stbGender.Enabled = False
        Me.stbGender.EntryErrorMSG = ""
        Me.stbGender.Location = New System.Drawing.Point(656, 30)
        Me.stbGender.MaxLength = 60
        Me.stbGender.Name = "stbGender"
        Me.stbGender.RegularExpression = ""
        Me.stbGender.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbGender.Size = New System.Drawing.Size(76, 20)
        Me.stbGender.TabIndex = 101
        '
        'lblBillAccountNo
        '
        Me.lblBillAccountNo.Location = New System.Drawing.Point(308, 52)
        Me.lblBillAccountNo.Name = "lblBillAccountNo"
        Me.lblBillAccountNo.Size = New System.Drawing.Size(136, 20)
        Me.lblBillAccountNo.TabIndex = 90
        Me.lblBillAccountNo.Text = "To-Bill Account No"
        '
        'lblAge
        '
        Me.lblAge.Location = New System.Drawing.Point(600, 11)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(49, 20)
        Me.lblAge.TabIndex = 98
        Me.lblAge.Text = "Age"
        '
        'lblGenderID
        '
        Me.lblGenderID.Location = New System.Drawing.Point(600, 31)
        Me.lblGenderID.Name = "lblGenderID"
        Me.lblGenderID.Size = New System.Drawing.Size(49, 20)
        Me.lblGenderID.TabIndex = 100
        Me.lblGenderID.Text = "Gender"
        '
        'stbFullName
        '
        Me.stbFullName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbFullName.CapitalizeFirstLetter = False
        Me.stbFullName.EntryErrorMSG = ""
        Me.stbFullName.Location = New System.Drawing.Point(418, 8)
        Me.stbFullName.MaxLength = 60
        Me.stbFullName.Name = "stbFullName"
        Me.stbFullName.ReadOnly = True
        Me.stbFullName.RegularExpression = ""
        Me.stbFullName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbFullName.Size = New System.Drawing.Size(170, 20)
        Me.stbFullName.TabIndex = 87
        '
        'lblFullName
        '
        Me.lblFullName.Location = New System.Drawing.Point(307, 8)
        Me.lblFullName.Name = "lblFullName"
        Me.lblFullName.Size = New System.Drawing.Size(111, 20)
        Me.lblFullName.TabIndex = 86
        Me.lblFullName.Text = "Patient's Name"
        '
        'pnlBill
        '
        Me.pnlBill.Controls.Add(Me.lblBillWords)
        Me.pnlBill.Controls.Add(Me.stbBillForItem)
        Me.pnlBill.Controls.Add(Me.stbBillWords)
        Me.pnlBill.Controls.Add(Me.lblBillForItem)
        Me.pnlBill.Location = New System.Drawing.Point(308, 153)
        Me.pnlBill.Name = "pnlBill"
        Me.pnlBill.Size = New System.Drawing.Size(707, 38)
        Me.pnlBill.TabIndex = 116
        Me.pnlBill.Visible = False
        '
        'lblBillWords
        '
        Me.lblBillWords.Location = New System.Drawing.Point(315, 8)
        Me.lblBillWords.Name = "lblBillWords"
        Me.lblBillWords.Size = New System.Drawing.Size(78, 18)
        Me.lblBillWords.TabIndex = 2
        Me.lblBillWords.Text = "Bill in Words"
        '
        'stbBillForItem
        '
        Me.stbBillForItem.BackColor = System.Drawing.SystemColors.Info
        Me.stbBillForItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBillForItem.CapitalizeFirstLetter = False
        Me.stbBillForItem.Enabled = False
        Me.stbBillForItem.EntryErrorMSG = ""
        Me.stbBillForItem.Location = New System.Drawing.Point(134, 5)
        Me.stbBillForItem.MaxLength = 20
        Me.stbBillForItem.Name = "stbBillForItem"
        Me.stbBillForItem.RegularExpression = ""
        Me.stbBillForItem.Size = New System.Drawing.Size(170, 20)
        Me.stbBillForItem.TabIndex = 1
        Me.stbBillForItem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'stbBillWords
        '
        Me.stbBillWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbBillWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBillWords.CapitalizeFirstLetter = False
        Me.stbBillWords.EntryErrorMSG = ""
        Me.stbBillWords.Location = New System.Drawing.Point(399, 4)
        Me.stbBillWords.MaxLength = 0
        Me.stbBillWords.Multiline = True
        Me.stbBillWords.Name = "stbBillWords"
        Me.stbBillWords.ReadOnly = True
        Me.stbBillWords.RegularExpression = ""
        Me.stbBillWords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbBillWords.Size = New System.Drawing.Size(187, 28)
        Me.stbBillWords.TabIndex = 3
        '
        'lblBillForItem
        '
        Me.lblBillForItem.Location = New System.Drawing.Point(13, 7)
        Me.lblBillForItem.Name = "lblBillForItem"
        Me.lblBillForItem.Size = New System.Drawing.Size(115, 18)
        Me.lblBillForItem.TabIndex = 0
        Me.lblBillForItem.Text = "Bill for"
        '
        'lblRoundNo
        '
        Me.lblRoundNo.Location = New System.Drawing.Point(12, 73)
        Me.lblRoundNo.Name = "lblRoundNo"
        Me.lblRoundNo.Size = New System.Drawing.Size(84, 20)
        Me.lblRoundNo.TabIndex = 78
        Me.lblRoundNo.Text = "Round No"
        '
        'cboStaffNo
        '
        Me.cboStaffNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboStaffNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboStaffNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStaffNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboStaffNo.Location = New System.Drawing.Point(136, 93)
        Me.cboStaffNo.Name = "cboStaffNo"
        Me.cboStaffNo.Size = New System.Drawing.Size(170, 21)
        Me.cboStaffNo.TabIndex = 83
        '
        'lblStaffNo
        '
        Me.lblStaffNo.Location = New System.Drawing.Point(12, 96)
        Me.lblStaffNo.Name = "lblStaffNo"
        Me.lblStaffNo.Size = New System.Drawing.Size(111, 20)
        Me.lblStaffNo.TabIndex = 82
        Me.lblStaffNo.Text = "Attending Doctor"
        '
        'stbRoundDateTime
        '
        Me.stbRoundDateTime.Checked = False
        Me.stbRoundDateTime.CustomFormat = "dd MMM yyyy hh:mm tt"
        Me.stbRoundDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.stbRoundDateTime.Location = New System.Drawing.Point(136, 116)
        Me.stbRoundDateTime.Name = "stbRoundDateTime"
        Me.stbRoundDateTime.ShowCheckBox = True
        Me.stbRoundDateTime.Size = New System.Drawing.Size(170, 20)
        Me.stbRoundDateTime.TabIndex = 85
        '
        'lblRoundDateTime
        '
        Me.lblRoundDateTime.Location = New System.Drawing.Point(12, 119)
        Me.lblRoundDateTime.Name = "lblRoundDateTime"
        Me.lblRoundDateTime.Size = New System.Drawing.Size(111, 20)
        Me.lblRoundDateTime.TabIndex = 84
        Me.lblRoundDateTime.Text = "Round Date Time"
        '
        'tbcDrRoles
        '
        Me.tbcDrRoles.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbcDrRoles.Controls.Add(Me.tpgGeneral)
        Me.tbcDrRoles.Controls.Add(Me.tpgDiagnosis)
        Me.tbcDrRoles.Controls.Add(Me.tpgProcedures)
        Me.tbcDrRoles.HotTrack = True
        Me.tbcDrRoles.Location = New System.Drawing.Point(1, 185)
        Me.tbcDrRoles.Name = "tbcDrRoles"
        Me.tbcDrRoles.SelectedIndex = 0
        Me.tbcDrRoles.Size = New System.Drawing.Size(1021, 290)
        Me.tbcDrRoles.TabIndex = 124
        '
        'tpgGeneral
        '
        Me.tpgGeneral.Controls.Add(Me.tbcGeneral)
        Me.tpgGeneral.Location = New System.Drawing.Point(4, 22)
        Me.tpgGeneral.Name = "tpgGeneral"
        Me.tpgGeneral.Size = New System.Drawing.Size(1013, 264)
        Me.tpgGeneral.TabIndex = 9
        Me.tpgGeneral.Tag = ""
        Me.tpgGeneral.Text = "General"
        Me.tpgGeneral.UseVisualStyleBackColor = True
        '
        'tbcGeneral
        '
        Me.tbcGeneral.Controls.Add(Me.tpgClinicalFindings)
        Me.tbcGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbcGeneral.HotTrack = True
        Me.tbcGeneral.Location = New System.Drawing.Point(0, 0)
        Me.tbcGeneral.Name = "tbcGeneral"
        Me.tbcGeneral.SelectedIndex = 0
        Me.tbcGeneral.Size = New System.Drawing.Size(1013, 264)
        Me.tbcGeneral.TabIndex = 0
        '
        'tpgClinicalFindings
        '
        Me.tpgClinicalFindings.Controls.Add(Me.lblBMI)
        Me.tpgClinicalFindings.Controls.Add(Me.grpTriage)
        Me.tpgClinicalFindings.Controls.Add(Me.btnLoadTemplate)
        Me.tpgClinicalFindings.Controls.Add(Me.lblClinicalNotes)
        Me.tpgClinicalFindings.Controls.Add(Me.stbClinicalNotes)
        Me.tpgClinicalFindings.Controls.Add(Me.chkNPMLast6hrs)
        Me.tpgClinicalFindings.Controls.Add(Me.chkConsentSigned)
        Me.tpgClinicalFindings.Controls.Add(Me.chkExplainationOfSurgery)
        Me.tpgClinicalFindings.Controls.Add(Me.chkCorrectNotes)
        Me.tpgClinicalFindings.Controls.Add(Me.chkCorrectIdentity)
        Me.tpgClinicalFindings.Controls.Add(Me.chkKnownAllergy)
        Me.tpgClinicalFindings.Controls.Add(Me.chkBathed)
        Me.tpgClinicalFindings.Controls.Add(Me.chkDenturesRemoved)
        Me.tpgClinicalFindings.Controls.Add(Me.chkJewelleryRemoved)
        Me.tpgClinicalFindings.Location = New System.Drawing.Point(4, 22)
        Me.tpgClinicalFindings.Name = "tpgClinicalFindings"
        Me.tpgClinicalFindings.Size = New System.Drawing.Size(1005, 238)
        Me.tpgClinicalFindings.TabIndex = 10
        Me.tpgClinicalFindings.Text = "Clinical Findings"
        Me.tpgClinicalFindings.UseVisualStyleBackColor = True
        '
        'lblBMI
        '
        Me.lblBMI.Location = New System.Drawing.Point(8, 213)
        Me.lblBMI.Name = "lblBMI"
        Me.lblBMI.Size = New System.Drawing.Size(134, 21)
        Me.lblBMI.TabIndex = 29
        Me.lblBMI.Text = "BMI (Kg/Mē)"
        '
        'grpTriage
        '
        Me.grpTriage.Controls.Add(Me.nbxBMI)
        Me.grpTriage.Controls.Add(Me.nbxMUAC)
        Me.grpTriage.Controls.Add(Me.lblMUAC)
        Me.grpTriage.Controls.Add(Me.nbxRespirationRate)
        Me.grpTriage.Controls.Add(Me.lblRespirationRate)
        Me.grpTriage.Controls.Add(Me.stbBloodPressure)
        Me.grpTriage.Controls.Add(Me.nbxWeight)
        Me.grpTriage.Controls.Add(Me.lblBodySurfaceArea)
        Me.grpTriage.Controls.Add(Me.lblWeight)
        Me.grpTriage.Controls.Add(Me.nbxBodySurfaceArea)
        Me.grpTriage.Controls.Add(Me.nbxTemperature)
        Me.grpTriage.Controls.Add(Me.lblHeadCircum)
        Me.grpTriage.Controls.Add(Me.lblTemperature)
        Me.grpTriage.Controls.Add(Me.nbxHeadCircum)
        Me.grpTriage.Controls.Add(Me.nbxHeight)
        Me.grpTriage.Controls.Add(Me.lblBloodPressure)
        Me.grpTriage.Controls.Add(Me.lblHeight)
        Me.grpTriage.Controls.Add(Me.lblPulse)
        Me.grpTriage.Controls.Add(Me.nbxPulse)
        Me.grpTriage.Location = New System.Drawing.Point(3, 5)
        Me.grpTriage.Name = "grpTriage"
        Me.grpTriage.Size = New System.Drawing.Size(303, 233)
        Me.grpTriage.TabIndex = 0
        Me.grpTriage.TabStop = False
        Me.grpTriage.Text = "Triage"
        '
        'nbxMUAC
        '
        Me.nbxMUAC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxMUAC.ControlCaption = "MUAC"
        Me.nbxMUAC.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
        Me.nbxMUAC.DecimalPlaces = 2
        Me.nbxMUAC.Location = New System.Drawing.Point(145, 55)
        Me.nbxMUAC.MaxLength = 6
        Me.nbxMUAC.MaxValue = 100.0R
        Me.nbxMUAC.MinValue = 1.0R
        Me.nbxMUAC.MustEnterNumeric = True
        Me.nbxMUAC.Name = "nbxMUAC"
        Me.nbxMUAC.Size = New System.Drawing.Size(145, 20)
        Me.nbxMUAC.TabIndex = 5
        Me.nbxMUAC.Value = ""
        '
        'lblMUAC
        '
        Me.lblMUAC.Location = New System.Drawing.Point(6, 57)
        Me.lblMUAC.Name = "lblMUAC"
        Me.lblMUAC.Size = New System.Drawing.Size(133, 20)
        Me.lblMUAC.TabIndex = 4
        Me.lblMUAC.Text = "MUAC"
        '
        'nbxRespirationRate
        '
        Me.nbxRespirationRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxRespirationRate.ControlCaption = "Respiration Rate"
        Me.nbxRespirationRate.DataType = SyncSoft.Common.Win.Controls.Number.[Short]
        Me.nbxRespirationRate.DecimalPlaces = -1
        Me.nbxRespirationRate.Location = New System.Drawing.Point(145, 183)
        Me.nbxRespirationRate.MaxLength = 3
        Me.nbxRespirationRate.MaxValue = 150.0R
        Me.nbxRespirationRate.MinValue = 10.0R
        Me.nbxRespirationRate.MustEnterNumeric = True
        Me.nbxRespirationRate.Name = "nbxRespirationRate"
        Me.nbxRespirationRate.Size = New System.Drawing.Size(145, 20)
        Me.nbxRespirationRate.TabIndex = 17
        Me.nbxRespirationRate.Value = ""
        '
        'lblRespirationRate
        '
        Me.lblRespirationRate.Location = New System.Drawing.Point(6, 183)
        Me.lblRespirationRate.Name = "lblRespirationRate"
        Me.lblRespirationRate.Size = New System.Drawing.Size(133, 21)
        Me.lblRespirationRate.TabIndex = 16
        Me.lblRespirationRate.Text = "Respiration Rate (B/min)"
        '
        'stbBloodPressure
        '
        Me.stbBloodPressure.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBloodPressure.CapitalizeFirstLetter = False
        Me.stbBloodPressure.EntryErrorMSG = "Must enter in the form 999/999"
        Me.stbBloodPressure.Location = New System.Drawing.Point(145, 119)
        Me.stbBloodPressure.MaxLength = 7
        Me.stbBloodPressure.Name = "stbBloodPressure"
        Me.stbBloodPressure.RegularExpression = "^[0-9]{1,3}/[0-9]{1,3}$"
        Me.stbBloodPressure.Size = New System.Drawing.Size(145, 20)
        Me.stbBloodPressure.TabIndex = 11
        '
        'nbxWeight
        '
        Me.nbxWeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxWeight.ControlCaption = "Weight"
        Me.nbxWeight.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
        Me.nbxWeight.DecimalPlaces = 2
        Me.nbxWeight.Location = New System.Drawing.Point(145, 12)
        Me.nbxWeight.MaxLength = 6
        Me.nbxWeight.MaxValue = 200.0R
        Me.nbxWeight.MinValue = 1.0R
        Me.nbxWeight.Name = "nbxWeight"
        Me.nbxWeight.Size = New System.Drawing.Size(145, 20)
        Me.nbxWeight.TabIndex = 1
        Me.nbxWeight.Value = ""
        '
        'lblBodySurfaceArea
        '
        Me.lblBodySurfaceArea.Location = New System.Drawing.Point(6, 161)
        Me.lblBodySurfaceArea.Name = "lblBodySurfaceArea"
        Me.lblBodySurfaceArea.Size = New System.Drawing.Size(133, 21)
        Me.lblBodySurfaceArea.TabIndex = 14
        Me.lblBodySurfaceArea.Text = "Body Surface Area (cm)"
        '
        'lblWeight
        '
        Me.lblWeight.Location = New System.Drawing.Point(6, 12)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(133, 21)
        Me.lblWeight.TabIndex = 0
        Me.lblWeight.Text = "Weight (Kg)"
        '
        'nbxBodySurfaceArea
        '
        Me.nbxBodySurfaceArea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxBodySurfaceArea.ControlCaption = "Body Surface Area"
        Me.nbxBodySurfaceArea.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
        Me.nbxBodySurfaceArea.DecimalPlaces = 2
        Me.nbxBodySurfaceArea.Location = New System.Drawing.Point(145, 161)
        Me.nbxBodySurfaceArea.MaxLength = 8
        Me.nbxBodySurfaceArea.MaxValue = 0.0R
        Me.nbxBodySurfaceArea.MinValue = 0.0R
        Me.nbxBodySurfaceArea.Name = "nbxBodySurfaceArea"
        Me.nbxBodySurfaceArea.Size = New System.Drawing.Size(145, 20)
        Me.nbxBodySurfaceArea.TabIndex = 15
        Me.nbxBodySurfaceArea.Value = ""
        '
        'nbxTemperature
        '
        Me.nbxTemperature.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxTemperature.ControlCaption = "Temperature"
        Me.nbxTemperature.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
        Me.nbxTemperature.DecimalPlaces = 2
        Me.nbxTemperature.Location = New System.Drawing.Point(145, 33)
        Me.nbxTemperature.MaxLength = 5
        Me.nbxTemperature.MaxValue = 45.0R
        Me.nbxTemperature.MinValue = 30.0R
        Me.nbxTemperature.Name = "nbxTemperature"
        Me.nbxTemperature.Size = New System.Drawing.Size(145, 20)
        Me.nbxTemperature.TabIndex = 3
        Me.nbxTemperature.Value = ""
        '
        'lblHeadCircum
        '
        Me.lblHeadCircum.Location = New System.Drawing.Point(6, 140)
        Me.lblHeadCircum.Name = "lblHeadCircum"
        Me.lblHeadCircum.Size = New System.Drawing.Size(133, 21)
        Me.lblHeadCircum.TabIndex = 12
        Me.lblHeadCircum.Text = "Head Circum (cm)"
        '
        'lblTemperature
        '
        Me.lblTemperature.Location = New System.Drawing.Point(6, 33)
        Me.lblTemperature.Name = "lblTemperature"
        Me.lblTemperature.Size = New System.Drawing.Size(133, 21)
        Me.lblTemperature.TabIndex = 2
        Me.lblTemperature.Text = "Temperature (Celc.)"
        '
        'nbxHeadCircum
        '
        Me.nbxHeadCircum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxHeadCircum.ControlCaption = "Head Circum"
        Me.nbxHeadCircum.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
        Me.nbxHeadCircum.DecimalPlaces = 2
        Me.nbxHeadCircum.Location = New System.Drawing.Point(145, 140)
        Me.nbxHeadCircum.MaxLength = 6
        Me.nbxHeadCircum.MaxValue = 100.0R
        Me.nbxHeadCircum.MinValue = 30.0R
        Me.nbxHeadCircum.MustEnterNumeric = True
        Me.nbxHeadCircum.Name = "nbxHeadCircum"
        Me.nbxHeadCircum.Size = New System.Drawing.Size(145, 20)
        Me.nbxHeadCircum.TabIndex = 13
        Me.nbxHeadCircum.Value = ""
        '
        'nbxHeight
        '
        Me.nbxHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxHeight.ControlCaption = "Height"
        Me.nbxHeight.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
        Me.nbxHeight.DecimalPlaces = 2
        Me.nbxHeight.Location = New System.Drawing.Point(145, 77)
        Me.nbxHeight.MaxLength = 6
        Me.nbxHeight.MaxValue = 250.0R
        Me.nbxHeight.MinValue = 50.0R
        Me.nbxHeight.Name = "nbxHeight"
        Me.nbxHeight.Size = New System.Drawing.Size(145, 20)
        Me.nbxHeight.TabIndex = 7
        Me.nbxHeight.Value = ""
        '
        'lblBloodPressure
        '
        Me.lblBloodPressure.Location = New System.Drawing.Point(6, 119)
        Me.lblBloodPressure.Name = "lblBloodPressure"
        Me.lblBloodPressure.Size = New System.Drawing.Size(133, 21)
        Me.lblBloodPressure.TabIndex = 10
        Me.lblBloodPressure.Text = "Blood Pressure (mmHg)"
        '
        'lblHeight
        '
        Me.lblHeight.Location = New System.Drawing.Point(6, 77)
        Me.lblHeight.Name = "lblHeight"
        Me.lblHeight.Size = New System.Drawing.Size(133, 21)
        Me.lblHeight.TabIndex = 6
        Me.lblHeight.Text = "Height (cm)"
        '
        'lblPulse
        '
        Me.lblPulse.Location = New System.Drawing.Point(6, 98)
        Me.lblPulse.Name = "lblPulse"
        Me.lblPulse.Size = New System.Drawing.Size(133, 21)
        Me.lblPulse.TabIndex = 8
        Me.lblPulse.Text = "Pulse (B/min)"
        '
        'nbxPulse
        '
        Me.nbxPulse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxPulse.ControlCaption = "Pulse"
        Me.nbxPulse.DataType = SyncSoft.Common.Win.Controls.Number.[Short]
        Me.nbxPulse.DecimalPlaces = -1
        Me.nbxPulse.Location = New System.Drawing.Point(145, 98)
        Me.nbxPulse.MaxLength = 3
        Me.nbxPulse.MaxValue = 250.0R
        Me.nbxPulse.MinValue = 50.0R
        Me.nbxPulse.Name = "nbxPulse"
        Me.nbxPulse.Size = New System.Drawing.Size(145, 20)
        Me.nbxPulse.TabIndex = 9
        Me.nbxPulse.Value = ""
        '
        'btnLoadTemplate
        '
        Me.btnLoadTemplate.BackColor = System.Drawing.SystemColors.Control
        Me.btnLoadTemplate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnLoadTemplate.Font = New System.Drawing.Font("Microsoft Sans Serif", 5.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLoadTemplate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnLoadTemplate.Image = CType(resources.GetObject("btnLoadTemplate.Image"), System.Drawing.Image)
        Me.btnLoadTemplate.Location = New System.Drawing.Point(386, 20)
        Me.btnLoadTemplate.Name = "btnLoadTemplate"
        Me.btnLoadTemplate.Size = New System.Drawing.Size(23, 19)
        Me.btnLoadTemplate.TabIndex = 2
        Me.btnLoadTemplate.UseVisualStyleBackColor = False
        Me.btnLoadTemplate.Visible = False
        '
        'lblClinicalNotes
        '
        Me.lblClinicalNotes.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblClinicalNotes.Location = New System.Drawing.Point(312, 19)
        Me.lblClinicalNotes.Name = "lblClinicalNotes"
        Me.lblClinicalNotes.Size = New System.Drawing.Size(88, 20)
        Me.lblClinicalNotes.TabIndex = 4
        Me.lblClinicalNotes.Text = "Clinical Notes"
        '
        'stbClinicalNotes
        '
        Me.stbClinicalNotes.AcceptsReturn = True
        Me.stbClinicalNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbClinicalNotes.CapitalizeFirstLetter = True
        Me.stbClinicalNotes.EntryErrorMSG = ""
        Me.stbClinicalNotes.Location = New System.Drawing.Point(315, 42)
        Me.stbClinicalNotes.MaxLength = 4000
        Me.stbClinicalNotes.Multiline = True
        Me.stbClinicalNotes.Name = "stbClinicalNotes"
        Me.stbClinicalNotes.RegularExpression = ""
        Me.stbClinicalNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbClinicalNotes.Size = New System.Drawing.Size(241, 102)
        Me.stbClinicalNotes.TabIndex = 5
        '
        'tpgDiagnosis
        '
        Me.tpgDiagnosis.Controls.Add(Me.dgvDiagnosis)
        Me.tpgDiagnosis.Location = New System.Drawing.Point(4, 22)
        Me.tpgDiagnosis.Name = "tpgDiagnosis"
        Me.tpgDiagnosis.Size = New System.Drawing.Size(1013, 271)
        Me.tpgDiagnosis.TabIndex = 10
        Me.tpgDiagnosis.Tag = "IPDDiagnosis"
        Me.tpgDiagnosis.Text = "Diagnosis"
        Me.tpgDiagnosis.UseVisualStyleBackColor = True
        '
        'dgvDiagnosis
        '
        Me.dgvDiagnosis.AllowUserToOrderColumns = True
        Me.dgvDiagnosis.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle31.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDiagnosis.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle31
        Me.dgvDiagnosis.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColDiagnosisSelect, Me.colICDDiagnosisCode, Me.colDiseaseCode, Me.ColDiagnosedBy, Me.colDiseaseCategory, Me.colNotes, Me.colDiagnosisSaved})
        Me.dgvDiagnosis.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDiagnosis.EnableHeadersVisualStyles = False
        Me.dgvDiagnosis.GridColor = System.Drawing.Color.Khaki
        Me.dgvDiagnosis.Location = New System.Drawing.Point(0, 0)
        Me.dgvDiagnosis.Name = "dgvDiagnosis"
        DataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle37.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDiagnosis.RowHeadersDefaultCellStyle = DataGridViewCellStyle37
        Me.dgvDiagnosis.Size = New System.Drawing.Size(1013, 271)
        Me.dgvDiagnosis.TabIndex = 1
        Me.dgvDiagnosis.Text = "DataGridView1"
        '
        'ColDiagnosisSelect
        '
        DataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle32.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle32.ForeColor = System.Drawing.Color.Firebrick
        DataGridViewCellStyle32.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.DarkBlue
        Me.ColDiagnosisSelect.DefaultCellStyle = DataGridViewCellStyle32
        Me.ColDiagnosisSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ColDiagnosisSelect.HeaderText = "Select"
        Me.ColDiagnosisSelect.Name = "ColDiagnosisSelect"
        Me.ColDiagnosisSelect.ReadOnly = True
        Me.ColDiagnosisSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ColDiagnosisSelect.Text = ""
        Me.ColDiagnosisSelect.UseColumnTextForButtonValue = True
        Me.ColDiagnosisSelect.Width = 50
        '
        'colICDDiagnosisCode
        '
        Me.colICDDiagnosisCode.HeaderText = "Code"
        Me.colICDDiagnosisCode.Name = "colICDDiagnosisCode"
        Me.colICDDiagnosisCode.Width = 80
        '
        'colDiseaseCode
        '
        DataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Info
        Me.colDiseaseCode.DefaultCellStyle = DataGridViewCellStyle33
        Me.colDiseaseCode.HeaderText = "Diagnosis"
        Me.colDiseaseCode.Name = "colDiseaseCode"
        Me.colDiseaseCode.ReadOnly = True
        Me.colDiseaseCode.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colDiseaseCode.Width = 300
        '
        'ColDiagnosedBy
        '
        DataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Info
        Me.ColDiagnosedBy.DefaultCellStyle = DataGridViewCellStyle34
        Me.ColDiagnosedBy.HeaderText = "Diagnosed By"
        Me.ColDiagnosedBy.Name = "ColDiagnosedBy"
        Me.ColDiagnosedBy.ReadOnly = True
        '
        'colDiseaseCategory
        '
        DataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Info
        Me.colDiseaseCategory.DefaultCellStyle = DataGridViewCellStyle35
        Me.colDiseaseCategory.HeaderText = "Category"
        Me.colDiseaseCategory.Name = "colDiseaseCategory"
        Me.colDiseaseCategory.ReadOnly = True
        Me.colDiseaseCategory.Width = 150
        '
        'colNotes
        '
        Me.colNotes.HeaderText = "Notes"
        Me.colNotes.MaxInputLength = 100
        Me.colNotes.Name = "colNotes"
        Me.colNotes.Width = 200
        '
        'colDiagnosisSaved
        '
        DataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle36.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle36.NullValue = False
        Me.colDiagnosisSaved.DefaultCellStyle = DataGridViewCellStyle36
        Me.colDiagnosisSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colDiagnosisSaved.HeaderText = "Saved"
        Me.colDiagnosisSaved.Name = "colDiagnosisSaved"
        Me.colDiagnosisSaved.ReadOnly = True
        Me.colDiagnosisSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colDiagnosisSaved.Width = 50
        '
        'tpgProcedures
        '
        Me.tpgProcedures.Controls.Add(Me.dgvProcedures)
        Me.tpgProcedures.Location = New System.Drawing.Point(4, 22)
        Me.tpgProcedures.Name = "tpgProcedures"
        Me.tpgProcedures.Size = New System.Drawing.Size(1013, 271)
        Me.tpgProcedures.TabIndex = 1
        Me.tpgProcedures.Tag = "DoctorProcedures"
        Me.tpgProcedures.Text = "Procedures"
        Me.tpgProcedures.UseVisualStyleBackColor = True
        '
        'dgvProcedures
        '
        Me.dgvProcedures.AllowUserToOrderColumns = True
        Me.dgvProcedures.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle38.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle38.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvProcedures.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle38
        Me.dgvProcedures.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colProcedureCode, Me.colICDProcedureCode, Me.colProcedureQuantity, Me.colProcedureUnitPrice, Me.colProcedureNotes, Me.colProcedureItemStatus, Me.colProcedurePayStatus, Me.colProceduresSaved})
        Me.dgvProcedures.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvProcedures.EnableHeadersVisualStyles = False
        Me.dgvProcedures.GridColor = System.Drawing.Color.Khaki
        Me.dgvProcedures.Location = New System.Drawing.Point(0, 0)
        Me.dgvProcedures.Name = "dgvProcedures"
        DataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle45.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle45.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle45.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle45.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle45.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle45.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvProcedures.RowHeadersDefaultCellStyle = DataGridViewCellStyle45
        Me.dgvProcedures.Size = New System.Drawing.Size(1013, 271)
        Me.dgvProcedures.TabIndex = 0
        Me.dgvProcedures.Text = "DataGridView1"
        '
        'colProcedureCode
        '
        Me.colProcedureCode.DisplayStyleForCurrentCellOnly = True
        Me.colProcedureCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colProcedureCode.HeaderText = "Procedure"
        Me.colProcedureCode.Name = "colProcedureCode"
        Me.colProcedureCode.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colProcedureCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colProcedureCode.Width = 280
        '
        'colICDProcedureCode
        '
        DataGridViewCellStyle39.BackColor = System.Drawing.SystemColors.Info
        Me.colICDProcedureCode.DefaultCellStyle = DataGridViewCellStyle39
        Me.colICDProcedureCode.HeaderText = "Code"
        Me.colICDProcedureCode.Name = "colICDProcedureCode"
        Me.colICDProcedureCode.ReadOnly = True
        Me.colICDProcedureCode.Width = 80
        '
        'colProcedureQuantity
        '
        DataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle40.Format = "N0"
        DataGridViewCellStyle40.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle40.NullValue = Nothing
        Me.colProcedureQuantity.DefaultCellStyle = DataGridViewCellStyle40
        Me.colProcedureQuantity.HeaderText = "Quantity"
        Me.colProcedureQuantity.Name = "colProcedureQuantity"
        Me.colProcedureQuantity.ReadOnly = True
        Me.colProcedureQuantity.Width = 60
        '
        'colProcedureUnitPrice
        '
        DataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle41.Format = "N2"
        DataGridViewCellStyle41.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle41.NullValue = Nothing
        Me.colProcedureUnitPrice.DefaultCellStyle = DataGridViewCellStyle41
        Me.colProcedureUnitPrice.HeaderText = "Unit Price"
        Me.colProcedureUnitPrice.Name = "colProcedureUnitPrice"
        Me.colProcedureUnitPrice.Width = 80
        '
        'colProcedureNotes
        '
        Me.colProcedureNotes.HeaderText = "Notes"
        Me.colProcedureNotes.MaxInputLength = 40
        Me.colProcedureNotes.Name = "colProcedureNotes"
        '
        'colProcedureItemStatus
        '
        DataGridViewCellStyle42.BackColor = System.Drawing.SystemColors.Info
        Me.colProcedureItemStatus.DefaultCellStyle = DataGridViewCellStyle42
        Me.colProcedureItemStatus.HeaderText = "Item Status"
        Me.colProcedureItemStatus.Name = "colProcedureItemStatus"
        Me.colProcedureItemStatus.ReadOnly = True
        Me.colProcedureItemStatus.Width = 80
        '
        'colProcedurePayStatus
        '
        DataGridViewCellStyle43.BackColor = System.Drawing.SystemColors.Info
        Me.colProcedurePayStatus.DefaultCellStyle = DataGridViewCellStyle43
        Me.colProcedurePayStatus.HeaderText = "Pay Status"
        Me.colProcedurePayStatus.Name = "colProcedurePayStatus"
        Me.colProcedurePayStatus.ReadOnly = True
        Me.colProcedurePayStatus.Width = 80
        '
        'colProceduresSaved
        '
        DataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle44.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle44.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle44.NullValue = False
        Me.colProceduresSaved.DefaultCellStyle = DataGridViewCellStyle44
        Me.colProceduresSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colProceduresSaved.HeaderText = "Saved"
        Me.colProceduresSaved.Name = "colProceduresSaved"
        Me.colProceduresSaved.ReadOnly = True
        Me.colProceduresSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colProceduresSaved.Width = 50
        '
        'pnlNavigateRounds
        '
        Me.pnlNavigateRounds.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.pnlNavigateRounds.Controls.Add(Me.chkNavigateRounds)
        Me.pnlNavigateRounds.Controls.Add(Me.navRounds)
        Me.pnlNavigateRounds.Location = New System.Drawing.Point(195, 495)
        Me.pnlNavigateRounds.Name = "pnlNavigateRounds"
        Me.pnlNavigateRounds.Size = New System.Drawing.Size(619, 35)
        Me.pnlNavigateRounds.TabIndex = 125
        '
        'chkNavigateRounds
        '
        Me.chkNavigateRounds.AccessibleDescription = ""
        Me.chkNavigateRounds.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkNavigateRounds.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkNavigateRounds.Location = New System.Drawing.Point(3, 7)
        Me.chkNavigateRounds.Name = "chkNavigateRounds"
        Me.chkNavigateRounds.Size = New System.Drawing.Size(181, 20)
        Me.chkNavigateRounds.TabIndex = 1
        Me.chkNavigateRounds.Text = "Navigate Admission Rounds"
        '
        'navRounds
        '
        Me.navRounds.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.navRounds.ColumnName = "RoundNo"
        Me.navRounds.DataSource = Nothing
        Me.navRounds.Location = New System.Drawing.Point(190, 1)
        Me.navRounds.Margin = New System.Windows.Forms.Padding(4)
        Me.navRounds.Name = "navRounds"
        Me.navRounds.NavAllEnabled = False
        Me.navRounds.NavLeftEnabled = False
        Me.navRounds.NavRightEnabled = False
        Me.navRounds.Size = New System.Drawing.Size(398, 32)
        Me.navRounds.TabIndex = 0
        '
        'frmPreoperative
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(1027, 544)
        Me.Controls.Add(Me.pnlNavigateRounds)
        Me.Controls.Add(Me.tbcDrRoles)
        Me.Controls.Add(Me.stbBedNo)
        Me.Controls.Add(Me.stbRoomNo)
        Me.Controls.Add(Me.lblRoomNo)
        Me.Controls.Add(Me.lblBedNo)
        Me.Controls.Add(Me.stbWard)
        Me.Controls.Add(Me.lblWard)
        Me.Controls.Add(Me.stbPackage)
        Me.Controls.Add(Me.lblPackageName)
        Me.Controls.Add(Me.stbAdmissionStatus)
        Me.Controls.Add(Me.lblAdmissionStatus)
        Me.Controls.Add(Me.stbTotalIPDDoctorRounds)
        Me.Controls.Add(Me.lblTotalIPDDoctorRounds)
        Me.Controls.Add(Me.btnLoadRounds)
        Me.Controls.Add(Me.pnlAlerts)
        Me.Controls.Add(Me.pnlCreateNewRound)
        Me.Controls.Add(Me.cboRoundNo)
        Me.Controls.Add(Me.stbAdmissionNo)
        Me.Controls.Add(Me.btnFindRoundNo)
        Me.Controls.Add(Me.stbVisitNo)
        Me.Controls.Add(Me.lblVisitNo)
        Me.Controls.Add(Me.stbAdmissionDateTime)
        Me.Controls.Add(Me.lblAdmissionDateTime)
        Me.Controls.Add(Me.btnFindAdmissionNo)
        Me.Controls.Add(Me.lblAdmissionNo)
        Me.Controls.Add(Me.stbBillCustomerName)
        Me.Controls.Add(Me.lblBillCustomerName)
        Me.Controls.Add(Me.spbPhoto)
        Me.Controls.Add(Me.stbPatientNo)
        Me.Controls.Add(Me.lblPatientsNo)
        Me.Controls.Add(Me.stbBillMode)
        Me.Controls.Add(Me.lblBillMode)
        Me.Controls.Add(Me.stbBillAccountNo)
        Me.Controls.Add(Me.stbAge)
        Me.Controls.Add(Me.stbGender)
        Me.Controls.Add(Me.lblBillAccountNo)
        Me.Controls.Add(Me.lblAge)
        Me.Controls.Add(Me.lblGenderID)
        Me.Controls.Add(Me.stbFullName)
        Me.Controls.Add(Me.lblFullName)
        Me.Controls.Add(Me.pnlBill)
        Me.Controls.Add(Me.lblRoundNo)
        Me.Controls.Add(Me.cboStaffNo)
        Me.Controls.Add(Me.lblStaffNo)
        Me.Controls.Add(Me.stbRoundDateTime)
        Me.Controls.Add(Me.lblRoundDateTime)
        Me.Controls.Add(Me.fbnSearch)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.fbnClose)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmPreoperative"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pre Operative Form"
        Me.pnlAlerts.ResumeLayout(False)
        Me.pnlCreateNewRound.ResumeLayout(False)
        CType(Me.spbPhoto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBill.ResumeLayout(False)
        Me.pnlBill.PerformLayout()
        Me.tbcDrRoles.ResumeLayout(False)
        Me.tpgGeneral.ResumeLayout(False)
        Me.tbcGeneral.ResumeLayout(False)
        Me.tpgClinicalFindings.ResumeLayout(False)
        Me.tpgClinicalFindings.PerformLayout()
        Me.grpTriage.ResumeLayout(False)
        Me.grpTriage.PerformLayout()
        Me.tpgDiagnosis.ResumeLayout(False)
        CType(Me.dgvDiagnosis, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgProcedures.ResumeLayout(False)
        CType(Me.dgvProcedures, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlNavigateRounds.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents chkExplainationOfSurgery As System.Windows.Forms.CheckBox
Friend WithEvents chkBathed As System.Windows.Forms.CheckBox
Friend WithEvents chkKnownAllergy As System.Windows.Forms.CheckBox
Friend WithEvents chkCorrectIdentity As System.Windows.Forms.CheckBox
Friend WithEvents chkCorrectNotes As System.Windows.Forms.CheckBox
Friend WithEvents chkNPMLast6hrs As System.Windows.Forms.CheckBox
Friend WithEvents chkConsentSigned As System.Windows.Forms.CheckBox
Friend WithEvents chkDenturesRemoved As System.Windows.Forms.CheckBox
    Friend WithEvents chkJewelleryRemoved As System.Windows.Forms.CheckBox
    Friend WithEvents stbBedNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents stbRoomNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblRoomNo As System.Windows.Forms.Label
    Friend WithEvents lblBedNo As System.Windows.Forms.Label
    Friend WithEvents stbWard As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblWard As System.Windows.Forms.Label
    Friend WithEvents stbPackage As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblPackageName As System.Windows.Forms.Label
    Friend WithEvents stbAdmissionStatus As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAdmissionStatus As System.Windows.Forms.Label
    Friend WithEvents stbTotalIPDDoctorRounds As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblTotalIPDDoctorRounds As System.Windows.Forms.Label
    Friend WithEvents btnLoadRounds As System.Windows.Forms.Button
    Friend WithEvents pnlAlerts As System.Windows.Forms.Panel
    Friend WithEvents btnLoadList As System.Windows.Forms.Button
    Friend WithEvents lblInWardAdmissions As System.Windows.Forms.Label
    Friend WithEvents pnlCreateNewRound As System.Windows.Forms.Panel
    Friend WithEvents chkCreateNewRound As System.Windows.Forms.CheckBox
    Friend WithEvents cboRoundNo As System.Windows.Forms.ComboBox
    Friend WithEvents stbAdmissionNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents btnFindRoundNo As System.Windows.Forms.Button
    Friend WithEvents stbVisitNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblVisitNo As System.Windows.Forms.Label
    Friend WithEvents stbAdmissionDateTime As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAdmissionDateTime As System.Windows.Forms.Label
    Friend WithEvents btnFindAdmissionNo As System.Windows.Forms.Button
    Friend WithEvents lblAdmissionNo As System.Windows.Forms.Label
    Friend WithEvents stbBillCustomerName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblBillCustomerName As System.Windows.Forms.Label
    Protected WithEvents spbPhoto As SyncSoft.Common.Win.Controls.SmartPictureBox
    Friend WithEvents stbPatientNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblPatientsNo As System.Windows.Forms.Label
    Friend WithEvents stbBillMode As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblBillMode As System.Windows.Forms.Label
    Friend WithEvents stbBillAccountNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents stbAge As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents stbGender As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblBillAccountNo As System.Windows.Forms.Label
    Friend WithEvents lblAge As System.Windows.Forms.Label
    Friend WithEvents lblGenderID As System.Windows.Forms.Label
    Friend WithEvents stbFullName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblFullName As System.Windows.Forms.Label
    Friend WithEvents pnlBill As System.Windows.Forms.Panel
    Friend WithEvents lblBillWords As System.Windows.Forms.Label
    Friend WithEvents stbBillForItem As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents stbBillWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblBillForItem As System.Windows.Forms.Label
    Friend WithEvents lblRoundNo As System.Windows.Forms.Label
    Friend WithEvents cboStaffNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblStaffNo As System.Windows.Forms.Label
    Friend WithEvents stbRoundDateTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblRoundDateTime As System.Windows.Forms.Label
    Friend WithEvents tbcDrRoles As System.Windows.Forms.TabControl
    Friend WithEvents tpgGeneral As System.Windows.Forms.TabPage
    Friend WithEvents tbcGeneral As System.Windows.Forms.TabControl
    Friend WithEvents tpgClinicalFindings As System.Windows.Forms.TabPage
    Friend WithEvents grpTriage As System.Windows.Forms.GroupBox
    Friend WithEvents nbxMUAC As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblMUAC As System.Windows.Forms.Label
    Friend WithEvents nbxRespirationRate As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblRespirationRate As System.Windows.Forms.Label
    Friend WithEvents stbBloodPressure As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents nbxWeight As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblBodySurfaceArea As System.Windows.Forms.Label
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents nbxBodySurfaceArea As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents nbxTemperature As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblHeadCircum As System.Windows.Forms.Label
    Friend WithEvents lblTemperature As System.Windows.Forms.Label
    Friend WithEvents nbxHeadCircum As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents nbxHeight As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblBloodPressure As System.Windows.Forms.Label
    Friend WithEvents lblHeight As System.Windows.Forms.Label
    Friend WithEvents lblPulse As System.Windows.Forms.Label
    Friend WithEvents nbxPulse As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents btnLoadTemplate As System.Windows.Forms.Button
    Friend WithEvents lblClinicalNotes As System.Windows.Forms.Label
    Friend WithEvents stbClinicalNotes As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents tpgDiagnosis As System.Windows.Forms.TabPage
    Friend WithEvents dgvDiagnosis As System.Windows.Forms.DataGridView
    Friend WithEvents ColDiagnosisSelect As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents colICDDiagnosisCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDiseaseCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColDiagnosedBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDiseaseCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNotes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDiagnosisSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents tpgProcedures As System.Windows.Forms.TabPage
    Friend WithEvents dgvProcedures As System.Windows.Forms.DataGridView
    Friend WithEvents colProcedureCode As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colICDProcedureCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colProcedureQuantity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colProcedureUnitPrice As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colProcedureNotes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colProcedureItemStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colProcedurePayStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colProceduresSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents lblBMI As System.Windows.Forms.Label
    Friend WithEvents nbxBMI As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents pnlNavigateRounds As System.Windows.Forms.Panel
    Friend WithEvents chkNavigateRounds As System.Windows.Forms.CheckBox
    Friend WithEvents navRounds As SyncSoft.Common.Win.Controls.DataNavigator

End Class