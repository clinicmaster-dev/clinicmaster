﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDashBoardUnbilledAdmissions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDashBoardUnbilledAdmissions))
        Me.fbClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.tbcItemOperations = New System.Windows.Forms.TabControl()
        Me.tpgUnbilledAdmissions = New System.Windows.Forms.TabPage()
        Me.dgvUnbilledAdmissions = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColAdmissions = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColBedNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColBed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColWard = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColVisitDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColAdmissionDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColDaysOnWard = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColAdmissionNotes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grpSetParameters = New System.Windows.Forms.GroupBox()
        Me.fbnReportOperations = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.fbnExport = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.pnlPeriod = New System.Windows.Forms.Panel()
        Me.dtpEndDateTime = New System.Windows.Forms.DateTimePicker()
        Me.lblStartDateTime = New System.Windows.Forms.Label()
        Me.dtpStartDateTime = New System.Windows.Forms.DateTimePicker()
        Me.lblEndDateTime = New System.Windows.Forms.Label()
        Me.lblRecordsNo = New System.Windows.Forms.Label()
        Me.tbcItemOperations.SuspendLayout()
        Me.tpgUnbilledAdmissions.SuspendLayout()
        CType(Me.dgvUnbilledAdmissions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpSetParameters.SuspendLayout()
        Me.pnlPeriod.SuspendLayout()
        Me.SuspendLayout()
        '
        'fbClose
        '
        Me.fbClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbClose.Location = New System.Drawing.Point(996, 533)
        Me.fbClose.Name = "fbClose"
        Me.fbClose.Size = New System.Drawing.Size(74, 22)
        Me.fbClose.TabIndex = 2
        Me.fbClose.Text = "&Close"
        Me.fbClose.UseVisualStyleBackColor = False
        '
        'tbcItemOperations
        '
        Me.tbcItemOperations.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbcItemOperations.Controls.Add(Me.tpgUnbilledAdmissions)
        Me.tbcItemOperations.HotTrack = True
        Me.tbcItemOperations.Location = New System.Drawing.Point(17, 107)
        Me.tbcItemOperations.Name = "tbcItemOperations"
        Me.tbcItemOperations.SelectedIndex = 0
        Me.tbcItemOperations.Size = New System.Drawing.Size(1053, 411)
        Me.tbcItemOperations.TabIndex = 1
        '
        'tpgUnbilledAdmissions
        '
        Me.tpgUnbilledAdmissions.Controls.Add(Me.dgvUnbilledAdmissions)
        Me.tpgUnbilledAdmissions.Location = New System.Drawing.Point(4, 22)
        Me.tpgUnbilledAdmissions.Name = "tpgUnbilledAdmissions"
        Me.tpgUnbilledAdmissions.Size = New System.Drawing.Size(1045, 385)
        Me.tpgUnbilledAdmissions.TabIndex = 19
        Me.tpgUnbilledAdmissions.Text = "Unbilled Admissions"
        Me.tpgUnbilledAdmissions.UseVisualStyleBackColor = True
        '
        'dgvUnbilledAdmissions
        '
        Me.dgvUnbilledAdmissions.AllowUserToAddRows = False
        Me.dgvUnbilledAdmissions.AllowUserToDeleteRows = False
        Me.dgvUnbilledAdmissions.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvUnbilledAdmissions.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvUnbilledAdmissions.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvUnbilledAdmissions.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvUnbilledAdmissions.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvUnbilledAdmissions.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvUnbilledAdmissions.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvUnbilledAdmissions.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn25, Me.DataGridViewTextBoxColumn24, Me.DataGridViewTextBoxColumn26, Me.ColAdmissions, Me.ColBedNo, Me.ColBed, Me.ColWard, Me.ColVisitDate, Me.ColAdmissionDate, Me.ColDaysOnWard, Me.ColAdmissionNotes})
        Me.dgvUnbilledAdmissions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvUnbilledAdmissions.EnableHeadersVisualStyles = False
        Me.dgvUnbilledAdmissions.GridColor = System.Drawing.Color.Khaki
        Me.dgvUnbilledAdmissions.Location = New System.Drawing.Point(0, 0)
        Me.dgvUnbilledAdmissions.Name = "dgvUnbilledAdmissions"
        Me.dgvUnbilledAdmissions.ReadOnly = True
        Me.dgvUnbilledAdmissions.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvUnbilledAdmissions.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvUnbilledAdmissions.RowHeadersVisible = False
        Me.dgvUnbilledAdmissions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvUnbilledAdmissions.Size = New System.Drawing.Size(1045, 385)
        Me.dgvUnbilledAdmissions.TabIndex = 0
        Me.dgvUnbilledAdmissions.Text = "DataGridView1"
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn25.DataPropertyName = "VisitNo"
        Me.DataGridViewTextBoxColumn25.HeaderText = "Visit No"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.ReadOnly = True
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn24.DataPropertyName = "FullName"
        Me.DataGridViewTextBoxColumn24.HeaderText = "Full Name"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = True
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn26.DataPropertyName = "Phone"
        Me.DataGridViewTextBoxColumn26.HeaderText = "Phone No"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        Me.DataGridViewTextBoxColumn26.ReadOnly = True
        '
        'ColAdmissions
        '
        Me.ColAdmissions.DataPropertyName = "AdmissionNo"
        Me.ColAdmissions.HeaderText = "Admission No"
        Me.ColAdmissions.Name = "ColAdmissions"
        Me.ColAdmissions.ReadOnly = True
        '
        'ColBedNo
        '
        Me.ColBedNo.DataPropertyName = "BedNo"
        Me.ColBedNo.HeaderText = "Bed No"
        Me.ColBedNo.Name = "ColBedNo"
        Me.ColBedNo.ReadOnly = True
        '
        'ColBed
        '
        Me.ColBed.DataPropertyName = "BedName"
        Me.ColBed.HeaderText = "Bed"
        Me.ColBed.Name = "ColBed"
        Me.ColBed.ReadOnly = True
        '
        'ColWard
        '
        Me.ColWard.DataPropertyName = "RoomName"
        Me.ColWard.HeaderText = "Ward"
        Me.ColWard.Name = "ColWard"
        Me.ColWard.ReadOnly = True
        '
        'ColVisitDate
        '
        Me.ColVisitDate.DataPropertyName = "VisitDate"
        Me.ColVisitDate.HeaderText = "Visit Date"
        Me.ColVisitDate.Name = "ColVisitDate"
        Me.ColVisitDate.ReadOnly = True
        '
        'ColAdmissionDate
        '
        Me.ColAdmissionDate.DataPropertyName = "AdmissionDate"
        Me.ColAdmissionDate.HeaderText = "Admission Date"
        Me.ColAdmissionDate.Name = "ColAdmissionDate"
        Me.ColAdmissionDate.ReadOnly = True
        '
        'ColDaysOnWard
        '
        Me.ColDaysOnWard.DataPropertyName = "DaysOnWard"
        Me.ColDaysOnWard.HeaderText = "Days On Ward"
        Me.ColDaysOnWard.Name = "ColDaysOnWard"
        Me.ColDaysOnWard.ReadOnly = True
        '
        'ColAdmissionNotes
        '
        Me.ColAdmissionNotes.DataPropertyName = "AdmissionNotes"
        Me.ColAdmissionNotes.HeaderText = "Admission Notes"
        Me.ColAdmissionNotes.Name = "ColAdmissionNotes"
        Me.ColAdmissionNotes.ReadOnly = True
        '
        'grpSetParameters
        '
        Me.grpSetParameters.Controls.Add(Me.fbnReportOperations)
        Me.grpSetParameters.Controls.Add(Me.fbnExport)
        Me.grpSetParameters.Controls.Add(Me.pnlPeriod)
        Me.grpSetParameters.Location = New System.Drawing.Point(12, 12)
        Me.grpSetParameters.Name = "grpSetParameters"
        Me.grpSetParameters.Size = New System.Drawing.Size(1054, 89)
        Me.grpSetParameters.TabIndex = 0
        Me.grpSetParameters.TabStop = False
        Me.grpSetParameters.Text = "Unbilled Admissions"
        '
        'fbnReportOperations
        '
        Me.fbnReportOperations.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnReportOperations.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnReportOperations.Location = New System.Drawing.Point(711, 25)
        Me.fbnReportOperations.Name = "fbnReportOperations"
        Me.fbnReportOperations.Size = New System.Drawing.Size(74, 22)
        Me.fbnReportOperations.TabIndex = 1
        Me.fbnReportOperations.Text = "&Load"
        '
        'fbnExport
        '
        Me.fbnExport.Enabled = False
        Me.fbnExport.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnExport.Location = New System.Drawing.Point(791, 25)
        Me.fbnExport.Name = "fbnExport"
        Me.fbnExport.Size = New System.Drawing.Size(74, 22)
        Me.fbnExport.TabIndex = 2
        Me.fbnExport.Text = "&Export"
        Me.fbnExport.UseVisualStyleBackColor = False
        '
        'pnlPeriod
        '
        Me.pnlPeriod.Controls.Add(Me.lblRecordsNo)
        Me.pnlPeriod.Controls.Add(Me.dtpEndDateTime)
        Me.pnlPeriod.Controls.Add(Me.lblStartDateTime)
        Me.pnlPeriod.Controls.Add(Me.dtpStartDateTime)
        Me.pnlPeriod.Controls.Add(Me.lblEndDateTime)
        Me.pnlPeriod.Location = New System.Drawing.Point(5, 15)
        Me.pnlPeriod.Name = "pnlPeriod"
        Me.pnlPeriod.Size = New System.Drawing.Size(1034, 68)
        Me.pnlPeriod.TabIndex = 0
        '
        'dtpEndDateTime
        '
        Me.dtpEndDateTime.CustomFormat = "dd MMM yyyy hh:mm tt"
        Me.dtpEndDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpEndDateTime.Location = New System.Drawing.Point(502, 12)
        Me.dtpEndDateTime.Name = "dtpEndDateTime"
        Me.dtpEndDateTime.ShowCheckBox = True
        Me.dtpEndDateTime.Size = New System.Drawing.Size(189, 20)
        Me.dtpEndDateTime.TabIndex = 3
        '
        'lblStartDateTime
        '
        Me.lblStartDateTime.Location = New System.Drawing.Point(10, 12)
        Me.lblStartDateTime.Name = "lblStartDateTime"
        Me.lblStartDateTime.Size = New System.Drawing.Size(137, 20)
        Me.lblStartDateTime.TabIndex = 0
        Me.lblStartDateTime.Text = "Start Record Date && Time"
        Me.lblStartDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStartDateTime
        '
        Me.dtpStartDateTime.CustomFormat = "dd MMM yyyy hh:mm tt"
        Me.dtpStartDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpStartDateTime.Location = New System.Drawing.Point(153, 12)
        Me.dtpStartDateTime.Name = "dtpStartDateTime"
        Me.dtpStartDateTime.ShowCheckBox = True
        Me.dtpStartDateTime.Size = New System.Drawing.Size(189, 20)
        Me.dtpStartDateTime.TabIndex = 1
        '
        'lblEndDateTime
        '
        Me.lblEndDateTime.Location = New System.Drawing.Point(357, 12)
        Me.lblEndDateTime.Name = "lblEndDateTime"
        Me.lblEndDateTime.Size = New System.Drawing.Size(139, 20)
        Me.lblEndDateTime.TabIndex = 2
        Me.lblEndDateTime.Text = "End Record Date && Time"
        Me.lblEndDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRecordsNo
        '
        Me.lblRecordsNo.ForeColor = System.Drawing.Color.Blue
        Me.lblRecordsNo.Location = New System.Drawing.Point(10, 35)
        Me.lblRecordsNo.Name = "lblRecordsNo"
        Me.lblRecordsNo.Size = New System.Drawing.Size(332, 23)
        Me.lblRecordsNo.TabIndex = 4
        '
        'frmDashBoardUnbilledAdmissions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1082, 558)
        Me.Controls.Add(Me.fbClose)
        Me.Controls.Add(Me.tbcItemOperations)
        Me.Controls.Add(Me.grpSetParameters)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmDashBoardUnbilledAdmissions"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Unbilled Admissions"
        Me.tbcItemOperations.ResumeLayout(False)
        Me.tpgUnbilledAdmissions.ResumeLayout(False)
        CType(Me.dgvUnbilledAdmissions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpSetParameters.ResumeLayout(False)
        Me.pnlPeriod.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents fbClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents tbcItemOperations As System.Windows.Forms.TabControl
    Friend WithEvents tpgUnbilledAdmissions As System.Windows.Forms.TabPage
    Friend WithEvents dgvUnbilledAdmissions As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColAdmissions As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColBedNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColBed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColWard As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColVisitDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColAdmissionDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColDaysOnWard As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColAdmissionNotes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grpSetParameters As System.Windows.Forms.GroupBox
    Friend WithEvents fbnReportOperations As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents fbnExport As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents pnlPeriod As System.Windows.Forms.Panel
    Friend WithEvents dtpEndDateTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDateTime As System.Windows.Forms.Label
    Friend WithEvents dtpStartDateTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEndDateTime As System.Windows.Forms.Label
    Friend WithEvents lblRecordsNo As System.Windows.Forms.Label
End Class
