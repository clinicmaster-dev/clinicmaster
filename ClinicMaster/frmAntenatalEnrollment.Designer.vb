
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAntenatalEnrollment : Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAntenatalEnrollment))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.btnSave = New SyncSoft.Common.Win.Controls.EditButton()
        Me.spbPhoto = New SyncSoft.Common.Win.Controls.SmartPictureBox()
        Me.txtFullName = New System.Windows.Forms.TextBox()
        Me.clbSocialHistory = New System.Windows.Forms.CheckedListBox()
        Me.stbSocialHistoryNotes = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.clbSurgicalHistory = New System.Windows.Forms.CheckedListBox()
        Me.stbSurgicalHistoryNotes = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.clbFamilyHistory = New System.Windows.Forms.CheckedListBox()
        Me.stbFamilyHistoryNotes = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.clbGynaecologicalHistory = New System.Windows.Forms.CheckedListBox()
        Me.stbGynHistoryNotes = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.clbMedicalHistory = New System.Windows.Forms.CheckedListBox()
        Me.dtpBloodTransfusionDate = New System.Windows.Forms.DateTimePicker()
        Me.stbMedicalHistoryNotes = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.chkLNMPDateReliable = New System.Windows.Forms.CheckBox()
        Me.cboCycleRegularID = New System.Windows.Forms.ComboBox()
        Me.cboHIVStatusID = New System.Windows.Forms.ComboBox()
        Me.cboBloodTransfusion = New System.Windows.Forms.ComboBox()
        Me.tbcAntenatalEnrollment = New System.Windows.Forms.TabControl()
        Me.tpgCurrentPregnancy = New System.Windows.Forms.TabPage()
        Me.tbcCurrentPregnancy = New System.Windows.Forms.TabControl()
        Me.tpgMenstruationHistory = New System.Windows.Forms.TabPage()
        Me.cboDonePregnancyScanID = New System.Windows.Forms.ComboBox()
        Me.lblDonePregnancyScan = New System.Windows.Forms.Label()
        Me.dtpLNMP = New System.Windows.Forms.DateTimePicker()
        Me.dtpEDD = New System.Windows.Forms.DateTimePicker()
        Me.dtpScanDate = New System.Windows.Forms.DateTimePicker()
        Me.lblScanDate = New System.Windows.Forms.Label()
        Me.lblCycleRegularID = New System.Windows.Forms.Label()
        Me.lblEDD = New System.Windows.Forms.Label()
        Me.lblLNMP = New System.Windows.Forms.Label()
        Me.tpgContraceptivesHistory = New System.Windows.Forms.TabPage()
        Me.dgvContraceptivesHistory = New System.Windows.Forms.DataGridView()
        Me.colContraceptiveID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colComplicationDetails = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colContraceptiveDateStarted = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colContraceptiveDiscontinued = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colContraceptiveRemovalReasons = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colContraceptiveNotes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colContraceptiveSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.tpgPreviousIllnesses = New System.Windows.Forms.TabPage()
        Me.grpMedicalHistory = New System.Windows.Forms.GroupBox()
        Me.lblBloodTransfusion = New System.Windows.Forms.Label()
        Me.lblMedicalHistory = New System.Windows.Forms.Label()
        Me.lblBloodTransfusionDate = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grpOBSGyn = New System.Windows.Forms.GroupBox()
        Me.lblGynHistoryNotes = New System.Windows.Forms.Label()
        Me.lblGynaecologicalHistory = New System.Windows.Forms.Label()
        Me.grpSurgicalHistory = New System.Windows.Forms.GroupBox()
        Me.lblSurgicalHistoryNotes = New System.Windows.Forms.Label()
        Me.lblSurgicalHistory = New System.Windows.Forms.Label()
        Me.tpgFamilySocialHistory = New System.Windows.Forms.TabPage()
        Me.grpFamilyHistory = New System.Windows.Forms.GroupBox()
        Me.lblFamilyHistory = New System.Windows.Forms.Label()
        Me.lblFamilyHistoryNotes = New System.Windows.Forms.Label()
        Me.grpSocialHistory = New System.Windows.Forms.GroupBox()
        Me.lblSocialHistoryNotes = New System.Windows.Forms.Label()
        Me.lblSocialHistory = New System.Windows.Forms.Label()
        Me.tpgObstetricHistory = New System.Windows.Forms.TabPage()
        Me.dgvOBHistory = New System.Windows.Forms.DataGridView()
        Me.colObstetricPregnancy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colObstetricYear = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colObstetricAbortionID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colObstetricAbortionPeriodID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colTypeOfDelivery = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colObstetricThirdStageID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colObstetricPeurPerinumID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colObstetricChildStatusID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colObstetricGenderID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colObstetricBirthWeight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colObstetricImmunised = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colObstetricHealthCondition = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colObstetricSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.tpgPatientAllergies = New System.Windows.Forms.TabPage()
        Me.dgvPatientAllergies = New System.Windows.Forms.DataGridView()
        Me.colAllergyNo = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colAllergyCategory = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colReaction = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPatientAllergiesSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.stbANCNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboPartnersHIVStatusID = New System.Windows.Forms.ComboBox()
        Me.stbPatientNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.nbxGravida = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.dtpEnrollmentDate = New System.Windows.Forms.DateTimePicker()
        Me.nbxPara = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.txtVisitDate = New System.Windows.Forms.TextBox()
        Me.txtAge = New System.Windows.Forms.TextBox()
        Me.stbJoinDate = New System.Windows.Forms.TextBox()
        Me.stbTotalVisits = New System.Windows.Forms.TextBox()
        Me.stbBloodGroup = New System.Windows.Forms.TextBox()
        Me.lblPatientNo = New System.Windows.Forms.Label()
        Me.lblPhoto = New System.Windows.Forms.Label()
        Me.lblVisitDate = New System.Windows.Forms.Label()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.lblAge = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblHIVStatus = New System.Windows.Forms.Label()
        Me.stbAdress = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.stbPhone = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.stbOccupation = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblOccupation = New System.Windows.Forms.Label()
        Me.stbMaritalStatus = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblMaritalStatus = New System.Windows.Forms.Label()
        Me.lblJoinDate = New System.Windows.Forms.Label()
        Me.lblNoOfVisits = New System.Windows.Forms.Label()
        Me.lblBloodGroup = New System.Windows.Forms.Label()
        Me.lblGravida = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblANCNo = New System.Windows.Forms.Label()
        Me.btnFindANCNo = New System.Windows.Forms.Button()
        Me.lblPartnerHIVStatus = New System.Windows.Forms.Label()
        Me.pnlStatusID = New System.Windows.Forms.Panel()
        Me.fcbStatusID = New SyncSoft.Common.Win.Controls.FlatComboBox()
        Me.lblStatusID = New System.Windows.Forms.Label()
        Me.pnlCreateNewRound = New System.Windows.Forms.Panel()
        Me.chkCreateNewEnrollment = New System.Windows.Forms.CheckBox()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.pnlNavigateANCEnrollments = New System.Windows.Forms.Panel()
        Me.chkNavigateEnrollments = New System.Windows.Forms.CheckBox()
        Me.navEnrollments = New SyncSoft.Common.Win.Controls.DataNavigator()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.stbGender = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblEnrollmentDate = New System.Windows.Forms.Label()
        CType(Me.spbPhoto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbcAntenatalEnrollment.SuspendLayout()
        Me.tpgCurrentPregnancy.SuspendLayout()
        Me.tbcCurrentPregnancy.SuspendLayout()
        Me.tpgMenstruationHistory.SuspendLayout()
        Me.tpgContraceptivesHistory.SuspendLayout()
        CType(Me.dgvContraceptivesHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgPreviousIllnesses.SuspendLayout()
        Me.grpMedicalHistory.SuspendLayout()
        Me.grpOBSGyn.SuspendLayout()
        Me.grpSurgicalHistory.SuspendLayout()
        Me.tpgFamilySocialHistory.SuspendLayout()
        Me.grpFamilyHistory.SuspendLayout()
        Me.grpSocialHistory.SuspendLayout()
        Me.tpgObstetricHistory.SuspendLayout()
        CType(Me.dgvOBHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgPatientAllergies.SuspendLayout()
        CType(Me.dgvPatientAllergies, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStatusID.SuspendLayout()
        Me.pnlCreateNewRound.SuspendLayout()
        Me.pnlNavigateANCEnrollments.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'fbnDelete
        '
        Me.fbnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(890, 480)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 42
        Me.fbnDelete.Tag = "AntenatalEnrollment"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = False
        Me.fbnDelete.Visible = False
        '
        'btnSave
        '
        Me.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.DataSource = Nothing
        Me.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Location = New System.Drawing.Point(22, 480)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(77, 23)
        Me.btnSave.TabIndex = 40
        Me.btnSave.Tag = "AntenatalEnrollment"
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'spbPhoto
        '
        Me.spbPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.btnSave.SetDataMember(Me.spbPhoto, "Photo")
        Me.spbPhoto.Image = CType(resources.GetObject("spbPhoto.Image"), System.Drawing.Image)
        Me.spbPhoto.ImageSizeLimit = CType(200000, Long)
        Me.spbPhoto.InitialImage = CType(resources.GetObject("spbPhoto.InitialImage"), System.Drawing.Image)
        Me.spbPhoto.Location = New System.Drawing.Point(920, 28)
        Me.spbPhoto.Name = "spbPhoto"
        Me.spbPhoto.ReadOnly = True
        Me.spbPhoto.Size = New System.Drawing.Size(108, 111)
        Me.spbPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.spbPhoto.TabIndex = 60
        Me.spbPhoto.TabStop = False
        '
        'txtFullName
        '
        Me.txtFullName.BackColor = System.Drawing.SystemColors.Control
        Me.txtFullName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.btnSave.SetDataMember(Me.txtFullName, "FullName")
        Me.txtFullName.Enabled = False
        Me.txtFullName.Location = New System.Drawing.Point(167, 73)
        Me.txtFullName.MaxLength = 41
        Me.txtFullName.Name = "txtFullName"
        Me.txtFullName.Size = New System.Drawing.Size(170, 20)
        Me.txtFullName.TabIndex = 9
        '
        'clbSocialHistory
        '
        Me.clbSocialHistory.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.btnSave.SetDataMember(Me.clbSocialHistory, "SocialHistory")
        Me.clbSocialHistory.FormattingEnabled = True
        Me.clbSocialHistory.Location = New System.Drawing.Point(119, 25)
        Me.clbSocialHistory.Name = "clbSocialHistory"
        Me.clbSocialHistory.Size = New System.Drawing.Size(170, 45)
        Me.clbSocialHistory.TabIndex = 1
        '
        'stbSocialHistoryNotes
        '
        Me.stbSocialHistoryNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbSocialHistoryNotes.CapitalizeFirstLetter = False
        Me.btnSave.SetDataMember(Me.stbSocialHistoryNotes, "SocialHistoryNotes")
        Me.stbSocialHistoryNotes.EntryErrorMSG = ""
        Me.stbSocialHistoryNotes.Location = New System.Drawing.Point(119, 101)
        Me.stbSocialHistoryNotes.Multiline = True
        Me.stbSocialHistoryNotes.Name = "stbSocialHistoryNotes"
        Me.stbSocialHistoryNotes.RegularExpression = ""
        Me.stbSocialHistoryNotes.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.stbSocialHistoryNotes.Size = New System.Drawing.Size(170, 44)
        Me.stbSocialHistoryNotes.TabIndex = 3
        '
        'clbSurgicalHistory
        '
        Me.clbSurgicalHistory.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.btnSave.SetDataMember(Me.clbSurgicalHistory, "SurgicalHistory")
        Me.clbSurgicalHistory.FormattingEnabled = True
        Me.clbSurgicalHistory.Location = New System.Drawing.Point(141, 23)
        Me.clbSurgicalHistory.Name = "clbSurgicalHistory"
        Me.clbSurgicalHistory.Size = New System.Drawing.Size(170, 60)
        Me.clbSurgicalHistory.TabIndex = 1
        '
        'stbSurgicalHistoryNotes
        '
        Me.stbSurgicalHistoryNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbSurgicalHistoryNotes.CapitalizeFirstLetter = False
        Me.btnSave.SetDataMember(Me.stbSurgicalHistoryNotes, "SurgicalHistoryNotes")
        Me.stbSurgicalHistoryNotes.EntryErrorMSG = ""
        Me.stbSurgicalHistoryNotes.Location = New System.Drawing.Point(141, 101)
        Me.stbSurgicalHistoryNotes.Multiline = True
        Me.stbSurgicalHistoryNotes.Name = "stbSurgicalHistoryNotes"
        Me.stbSurgicalHistoryNotes.RegularExpression = ""
        Me.stbSurgicalHistoryNotes.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.stbSurgicalHistoryNotes.Size = New System.Drawing.Size(170, 55)
        Me.stbSurgicalHistoryNotes.TabIndex = 3
        '
        'clbFamilyHistory
        '
        Me.clbFamilyHistory.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.btnSave.SetDataMember(Me.clbFamilyHistory, "FamilyHistory")
        Me.clbFamilyHistory.FormattingEnabled = True
        Me.clbFamilyHistory.Location = New System.Drawing.Point(112, 26)
        Me.clbFamilyHistory.Name = "clbFamilyHistory"
        Me.clbFamilyHistory.Size = New System.Drawing.Size(170, 75)
        Me.clbFamilyHistory.TabIndex = 1
        '
        'stbFamilyHistoryNotes
        '
        Me.stbFamilyHistoryNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbFamilyHistoryNotes.CapitalizeFirstLetter = False
        Me.btnSave.SetDataMember(Me.stbFamilyHistoryNotes, "FamilyHistoryNotes")
        Me.stbFamilyHistoryNotes.EntryErrorMSG = ""
        Me.stbFamilyHistoryNotes.Location = New System.Drawing.Point(112, 108)
        Me.stbFamilyHistoryNotes.Multiline = True
        Me.stbFamilyHistoryNotes.Name = "stbFamilyHistoryNotes"
        Me.stbFamilyHistoryNotes.RegularExpression = ""
        Me.stbFamilyHistoryNotes.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.stbFamilyHistoryNotes.Size = New System.Drawing.Size(170, 41)
        Me.stbFamilyHistoryNotes.TabIndex = 3
        '
        'clbGynaecologicalHistory
        '
        Me.clbGynaecologicalHistory.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.btnSave.SetDataMember(Me.clbGynaecologicalHistory, "GynaecologicalHistory")
        Me.clbGynaecologicalHistory.FormattingEnabled = True
        Me.clbGynaecologicalHistory.Location = New System.Drawing.Point(141, 26)
        Me.clbGynaecologicalHistory.Name = "clbGynaecologicalHistory"
        Me.clbGynaecologicalHistory.Size = New System.Drawing.Size(170, 60)
        Me.clbGynaecologicalHistory.TabIndex = 1
        '
        'stbGynHistoryNotes
        '
        Me.stbGynHistoryNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbGynHistoryNotes.CapitalizeFirstLetter = False
        Me.btnSave.SetDataMember(Me.stbGynHistoryNotes, "GynaecologicalHistoryNotes")
        Me.stbGynHistoryNotes.EntryErrorMSG = ""
        Me.stbGynHistoryNotes.Location = New System.Drawing.Point(141, 106)
        Me.stbGynHistoryNotes.Multiline = True
        Me.stbGynHistoryNotes.Name = "stbGynHistoryNotes"
        Me.stbGynHistoryNotes.RegularExpression = ""
        Me.stbGynHistoryNotes.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.stbGynHistoryNotes.Size = New System.Drawing.Size(170, 50)
        Me.stbGynHistoryNotes.TabIndex = 3
        '
        'clbMedicalHistory
        '
        Me.clbMedicalHistory.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.btnSave.SetDataMember(Me.clbMedicalHistory, "MedicalHistory")
        Me.clbMedicalHistory.FormattingEnabled = True
        Me.clbMedicalHistory.Location = New System.Drawing.Point(141, 17)
        Me.clbMedicalHistory.Name = "clbMedicalHistory"
        Me.clbMedicalHistory.Size = New System.Drawing.Size(170, 75)
        Me.clbMedicalHistory.TabIndex = 1
        '
        'dtpBloodTransfusionDate
        '
        Me.btnSave.SetDataMember(Me.dtpBloodTransfusionDate, "BloodTransfusionDate")
        Me.dtpBloodTransfusionDate.Location = New System.Drawing.Point(141, 122)
        Me.dtpBloodTransfusionDate.MaxDate = New Date(2079, 6, 6, 0, 0, 0, 0)
        Me.dtpBloodTransfusionDate.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpBloodTransfusionDate.Name = "dtpBloodTransfusionDate"
        Me.dtpBloodTransfusionDate.ShowCheckBox = True
        Me.dtpBloodTransfusionDate.Size = New System.Drawing.Size(170, 20)
        Me.dtpBloodTransfusionDate.TabIndex = 5
        Me.dtpBloodTransfusionDate.Value = New Date(2018, 5, 4, 0, 0, 0, 0)
        '
        'stbMedicalHistoryNotes
        '
        Me.stbMedicalHistoryNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbMedicalHistoryNotes.CapitalizeFirstLetter = False
        Me.btnSave.SetDataMember(Me.stbMedicalHistoryNotes, "MedicalHistoryNotes")
        Me.stbMedicalHistoryNotes.EntryErrorMSG = ""
        Me.stbMedicalHistoryNotes.Location = New System.Drawing.Point(141, 145)
        Me.stbMedicalHistoryNotes.Multiline = True
        Me.stbMedicalHistoryNotes.Name = "stbMedicalHistoryNotes"
        Me.stbMedicalHistoryNotes.RegularExpression = ""
        Me.stbMedicalHistoryNotes.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.stbMedicalHistoryNotes.Size = New System.Drawing.Size(170, 35)
        Me.stbMedicalHistoryNotes.TabIndex = 7
        '
        'chkLNMPDateReliable
        '
        Me.chkLNMPDateReliable.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.SetDataMember(Me.chkLNMPDateReliable, "LNMPDateReliable")
        Me.chkLNMPDateReliable.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkLNMPDateReliable.Location = New System.Drawing.Point(5, 35)
        Me.chkLNMPDateReliable.Name = "chkLNMPDateReliable"
        Me.chkLNMPDateReliable.Size = New System.Drawing.Size(151, 20)
        Me.chkLNMPDateReliable.TabIndex = 2
        Me.chkLNMPDateReliable.Text = "LNMP Date Reliable"
        '
        'cboCycleRegularID
        '
        Me.btnSave.SetDataMember(Me.cboCycleRegularID, "CycleRegular,CycleRegularID")
        Me.cboCycleRegularID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCycleRegularID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboCycleRegularID.Location = New System.Drawing.Point(140, 80)
        Me.cboCycleRegularID.Name = "cboCycleRegularID"
        Me.cboCycleRegularID.Size = New System.Drawing.Size(168, 21)
        Me.cboCycleRegularID.TabIndex = 6
        '
        'cboHIVStatusID
        '
        Me.btnSave.SetDataMember(Me.cboHIVStatusID, "HIVStatus,HIVStatusID")
        Me.cboHIVStatusID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHIVStatusID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboHIVStatusID.Location = New System.Drawing.Point(137, 3)
        Me.cboHIVStatusID.Name = "cboHIVStatusID"
        Me.cboHIVStatusID.Size = New System.Drawing.Size(170, 21)
        Me.cboHIVStatusID.TabIndex = 1
        '
        'cboBloodTransfusion
        '
        Me.btnSave.SetDataMember(Me.cboBloodTransfusion, "BloodTransfusion")
        Me.cboBloodTransfusion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBloodTransfusion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboBloodTransfusion.Location = New System.Drawing.Point(141, 98)
        Me.cboBloodTransfusion.Name = "cboBloodTransfusion"
        Me.cboBloodTransfusion.Size = New System.Drawing.Size(170, 21)
        Me.cboBloodTransfusion.TabIndex = 3
        '
        'tbcAntenatalEnrollment
        '
        Me.tbcAntenatalEnrollment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbcAntenatalEnrollment.Controls.Add(Me.tpgCurrentPregnancy)
        Me.tbcAntenatalEnrollment.Controls.Add(Me.tpgPreviousIllnesses)
        Me.tbcAntenatalEnrollment.Controls.Add(Me.tpgFamilySocialHistory)
        Me.tbcAntenatalEnrollment.Controls.Add(Me.tpgObstetricHistory)
        Me.tbcAntenatalEnrollment.Controls.Add(Me.tpgPatientAllergies)
        Me.btnSave.SetDataMember(Me.tbcAntenatalEnrollment, "PatientStatus,PatientStatusID")
        Me.tbcAntenatalEnrollment.Location = New System.Drawing.Point(16, 210)
        Me.tbcAntenatalEnrollment.Name = "tbcAntenatalEnrollment"
        Me.tbcAntenatalEnrollment.SelectedIndex = 0
        Me.tbcAntenatalEnrollment.Size = New System.Drawing.Size(1064, 243)
        Me.tbcAntenatalEnrollment.TabIndex = 0
        '
        'tpgCurrentPregnancy
        '
        Me.tpgCurrentPregnancy.Controls.Add(Me.tbcCurrentPregnancy)
        Me.tpgCurrentPregnancy.Location = New System.Drawing.Point(4, 22)
        Me.tpgCurrentPregnancy.Name = "tpgCurrentPregnancy"
        Me.tpgCurrentPregnancy.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgCurrentPregnancy.Size = New System.Drawing.Size(1056, 217)
        Me.tpgCurrentPregnancy.TabIndex = 0
        Me.tpgCurrentPregnancy.Text = "Current pregnancy "
        Me.tpgCurrentPregnancy.UseVisualStyleBackColor = True
        '
        'tbcCurrentPregnancy
        '
        Me.tbcCurrentPregnancy.Controls.Add(Me.tpgMenstruationHistory)
        Me.tbcCurrentPregnancy.Controls.Add(Me.tpgContraceptivesHistory)
        Me.tbcCurrentPregnancy.Location = New System.Drawing.Point(-1, 0)
        Me.tbcCurrentPregnancy.Name = "tbcCurrentPregnancy"
        Me.tbcCurrentPregnancy.SelectedIndex = 0
        Me.tbcCurrentPregnancy.Size = New System.Drawing.Size(1003, 217)
        Me.tbcCurrentPregnancy.TabIndex = 0
        '
        'tpgMenstruationHistory
        '
        Me.tpgMenstruationHistory.Controls.Add(Me.cboDonePregnancyScanID)
        Me.tpgMenstruationHistory.Controls.Add(Me.lblDonePregnancyScan)
        Me.tpgMenstruationHistory.Controls.Add(Me.dtpLNMP)
        Me.tpgMenstruationHistory.Controls.Add(Me.dtpEDD)
        Me.tpgMenstruationHistory.Controls.Add(Me.dtpScanDate)
        Me.tpgMenstruationHistory.Controls.Add(Me.lblScanDate)
        Me.tpgMenstruationHistory.Controls.Add(Me.cboCycleRegularID)
        Me.tpgMenstruationHistory.Controls.Add(Me.lblCycleRegularID)
        Me.tpgMenstruationHistory.Controls.Add(Me.chkLNMPDateReliable)
        Me.tpgMenstruationHistory.Controls.Add(Me.lblEDD)
        Me.tpgMenstruationHistory.Controls.Add(Me.lblLNMP)
        Me.tpgMenstruationHistory.Location = New System.Drawing.Point(4, 22)
        Me.tpgMenstruationHistory.Name = "tpgMenstruationHistory"
        Me.tpgMenstruationHistory.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgMenstruationHistory.Size = New System.Drawing.Size(995, 191)
        Me.tpgMenstruationHistory.TabIndex = 0
        Me.tpgMenstruationHistory.Text = "Menstruation History"
        Me.tpgMenstruationHistory.UseVisualStyleBackColor = True
        '
        'cboDonePregnancyScanID
        '
        Me.btnSave.SetDataMember(Me.cboDonePregnancyScanID, "DonePregnancyScan,DonePregnancyScanID")
        Me.cboDonePregnancyScanID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDonePregnancyScanID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboDonePregnancyScanID.Location = New System.Drawing.Point(140, 104)
        Me.cboDonePregnancyScanID.Name = "cboDonePregnancyScanID"
        Me.cboDonePregnancyScanID.Size = New System.Drawing.Size(168, 21)
        Me.cboDonePregnancyScanID.TabIndex = 8
        '
        'lblDonePregnancyScan
        '
        Me.lblDonePregnancyScan.Location = New System.Drawing.Point(5, 104)
        Me.lblDonePregnancyScan.Name = "lblDonePregnancyScan"
        Me.lblDonePregnancyScan.Size = New System.Drawing.Size(127, 20)
        Me.lblDonePregnancyScan.TabIndex = 7
        Me.lblDonePregnancyScan.Text = "Done Pregnancy Scan"
        '
        'dtpLNMP
        '
        Me.dtpLNMP.CustomFormat = "dd MMM yyyy"
        Me.btnSave.SetDataMember(Me.dtpLNMP, "LNMP")
        Me.dtpLNMP.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpLNMP.Location = New System.Drawing.Point(140, 9)
        Me.dtpLNMP.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpLNMP.Name = "dtpLNMP"
        Me.dtpLNMP.ShowCheckBox = True
        Me.dtpLNMP.Size = New System.Drawing.Size(168, 20)
        Me.dtpLNMP.TabIndex = 1
        '
        'dtpEDD
        '
        Me.dtpEDD.CustomFormat = "dd MMM yyyy"
        Me.btnSave.SetDataMember(Me.dtpEDD, "EDD")
        Me.dtpEDD.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpEDD.Location = New System.Drawing.Point(140, 57)
        Me.dtpEDD.MaxDate = New Date(2079, 6, 6, 0, 0, 0, 0)
        Me.dtpEDD.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpEDD.Name = "dtpEDD"
        Me.dtpEDD.ShowCheckBox = True
        Me.dtpEDD.Size = New System.Drawing.Size(168, 20)
        Me.dtpEDD.TabIndex = 4
        '
        'dtpScanDate
        '
        Me.dtpScanDate.CustomFormat = "dd MMM yyyy"
        Me.btnSave.SetDataMember(Me.dtpScanDate, "ScanDate")
        Me.dtpScanDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpScanDate.Location = New System.Drawing.Point(140, 128)
        Me.dtpScanDate.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpScanDate.Name = "dtpScanDate"
        Me.dtpScanDate.ShowCheckBox = True
        Me.dtpScanDate.Size = New System.Drawing.Size(168, 20)
        Me.dtpScanDate.TabIndex = 10
        '
        'lblScanDate
        '
        Me.lblScanDate.Location = New System.Drawing.Point(5, 128)
        Me.lblScanDate.Name = "lblScanDate"
        Me.lblScanDate.Size = New System.Drawing.Size(127, 20)
        Me.lblScanDate.TabIndex = 9
        Me.lblScanDate.Text = "Scan Date"
        '
        'lblCycleRegularID
        '
        Me.lblCycleRegularID.Location = New System.Drawing.Point(5, 80)
        Me.lblCycleRegularID.Name = "lblCycleRegularID"
        Me.lblCycleRegularID.Size = New System.Drawing.Size(127, 20)
        Me.lblCycleRegularID.TabIndex = 5
        Me.lblCycleRegularID.Text = "Cycle Regular"
        '
        'lblEDD
        '
        Me.lblEDD.Location = New System.Drawing.Point(5, 57)
        Me.lblEDD.Name = "lblEDD"
        Me.lblEDD.Size = New System.Drawing.Size(127, 20)
        Me.lblEDD.TabIndex = 3
        Me.lblEDD.Text = "EDD"
        '
        'lblLNMP
        '
        Me.lblLNMP.Location = New System.Drawing.Point(5, 10)
        Me.lblLNMP.Name = "lblLNMP"
        Me.lblLNMP.Size = New System.Drawing.Size(127, 20)
        Me.lblLNMP.TabIndex = 0
        Me.lblLNMP.Text = "LNMP"
        '
        'tpgContraceptivesHistory
        '
        Me.tpgContraceptivesHistory.AccessibleName = ""
        Me.tpgContraceptivesHistory.Controls.Add(Me.dgvContraceptivesHistory)
        Me.tpgContraceptivesHistory.Location = New System.Drawing.Point(4, 22)
        Me.tpgContraceptivesHistory.Name = "tpgContraceptivesHistory"
        Me.tpgContraceptivesHistory.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgContraceptivesHistory.Size = New System.Drawing.Size(995, 191)
        Me.tpgContraceptivesHistory.TabIndex = 1
        Me.tpgContraceptivesHistory.Text = "Contraceptives History"
        Me.tpgContraceptivesHistory.UseVisualStyleBackColor = True
        '
        'dgvContraceptivesHistory
        '
        Me.dgvContraceptivesHistory.AllowUserToOrderColumns = True
        Me.dgvContraceptivesHistory.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvContraceptivesHistory.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvContraceptivesHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colContraceptiveID, Me.colComplicationDetails, Me.colContraceptiveDateStarted, Me.colContraceptiveDiscontinued, Me.colContraceptiveRemovalReasons, Me.colContraceptiveNotes, Me.colContraceptiveSaved})
        Me.dgvContraceptivesHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvContraceptivesHistory.EnableHeadersVisualStyles = False
        Me.dgvContraceptivesHistory.GridColor = System.Drawing.Color.Khaki
        Me.dgvContraceptivesHistory.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.dgvContraceptivesHistory.Location = New System.Drawing.Point(3, 3)
        Me.dgvContraceptivesHistory.Name = "dgvContraceptivesHistory"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvContraceptivesHistory.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvContraceptivesHistory.Size = New System.Drawing.Size(989, 185)
        Me.dgvContraceptivesHistory.TabIndex = 0
        Me.dgvContraceptivesHistory.Text = "DataGridView1"
        '
        'colContraceptiveID
        '
        Me.colContraceptiveID.DataPropertyName = "ContraceptiveID"
        Me.colContraceptiveID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colContraceptiveID.HeaderText = "Contraceptive"
        Me.colContraceptiveID.Name = "colContraceptiveID"
        Me.colContraceptiveID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colContraceptiveID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colContraceptiveID.Width = 200
        '
        'colComplicationDetails
        '
        Me.colComplicationDetails.DataPropertyName = "ComplicationDetails"
        Me.colComplicationDetails.HeaderText = "Complication Details"
        Me.colComplicationDetails.Name = "colComplicationDetails"
        Me.colComplicationDetails.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colComplicationDetails.Width = 120
        '
        'colContraceptiveDateStarted
        '
        Me.colContraceptiveDateStarted.DataPropertyName = "DateStarted"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle2.NullValue = Nothing
        Me.colContraceptiveDateStarted.DefaultCellStyle = DataGridViewCellStyle2
        Me.colContraceptiveDateStarted.HeaderText = "Date Started"
        Me.colContraceptiveDateStarted.MaxInputLength = 12
        Me.colContraceptiveDateStarted.Name = "colContraceptiveDateStarted"
        '
        'colContraceptiveDiscontinued
        '
        Me.colContraceptiveDiscontinued.DataPropertyName = "DiscontinuedRemovedID"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        Me.colContraceptiveDiscontinued.DefaultCellStyle = DataGridViewCellStyle3
        Me.colContraceptiveDiscontinued.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colContraceptiveDiscontinued.HeaderText = "Discontinued"
        Me.colContraceptiveDiscontinued.Name = "colContraceptiveDiscontinued"
        Me.colContraceptiveDiscontinued.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colContraceptiveDiscontinued.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'colContraceptiveRemovalReasons
        '
        Me.colContraceptiveRemovalReasons.DataPropertyName = "RemovalReasonsID"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        Me.colContraceptiveRemovalReasons.DefaultCellStyle = DataGridViewCellStyle4
        Me.colContraceptiveRemovalReasons.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colContraceptiveRemovalReasons.HeaderText = "Reason For Removal"
        Me.colContraceptiveRemovalReasons.Name = "colContraceptiveRemovalReasons"
        Me.colContraceptiveRemovalReasons.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colContraceptiveRemovalReasons.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colContraceptiveRemovalReasons.Width = 150
        '
        'colContraceptiveNotes
        '
        Me.colContraceptiveNotes.DataPropertyName = "Notes"
        Me.colContraceptiveNotes.HeaderText = "Notes"
        Me.colContraceptiveNotes.MaxInputLength = 200
        Me.colContraceptiveNotes.Name = "colContraceptiveNotes"
        Me.colContraceptiveNotes.Width = 200
        '
        'colContraceptiveSaved
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle5.NullValue = False
        Me.colContraceptiveSaved.DefaultCellStyle = DataGridViewCellStyle5
        Me.colContraceptiveSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colContraceptiveSaved.HeaderText = "Saved"
        Me.colContraceptiveSaved.Name = "colContraceptiveSaved"
        Me.colContraceptiveSaved.ReadOnly = True
        Me.colContraceptiveSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colContraceptiveSaved.Width = 50
        '
        'tpgPreviousIllnesses
        '
        Me.tpgPreviousIllnesses.Controls.Add(Me.grpMedicalHistory)
        Me.tpgPreviousIllnesses.Controls.Add(Me.grpOBSGyn)
        Me.tpgPreviousIllnesses.Controls.Add(Me.grpSurgicalHistory)
        Me.tpgPreviousIllnesses.Location = New System.Drawing.Point(4, 22)
        Me.tpgPreviousIllnesses.Name = "tpgPreviousIllnesses"
        Me.tpgPreviousIllnesses.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgPreviousIllnesses.Size = New System.Drawing.Size(1056, 217)
        Me.tpgPreviousIllnesses.TabIndex = 2
        Me.tpgPreviousIllnesses.Text = "Previous Conditions"
        Me.tpgPreviousIllnesses.UseVisualStyleBackColor = True
        '
        'grpMedicalHistory
        '
        Me.grpMedicalHistory.Controls.Add(Me.cboBloodTransfusion)
        Me.grpMedicalHistory.Controls.Add(Me.lblBloodTransfusion)
        Me.grpMedicalHistory.Controls.Add(Me.clbMedicalHistory)
        Me.grpMedicalHistory.Controls.Add(Me.lblMedicalHistory)
        Me.grpMedicalHistory.Controls.Add(Me.dtpBloodTransfusionDate)
        Me.grpMedicalHistory.Controls.Add(Me.lblBloodTransfusionDate)
        Me.grpMedicalHistory.Controls.Add(Me.Label1)
        Me.grpMedicalHistory.Controls.Add(Me.stbMedicalHistoryNotes)
        Me.grpMedicalHistory.Location = New System.Drawing.Point(6, 19)
        Me.grpMedicalHistory.Name = "grpMedicalHistory"
        Me.grpMedicalHistory.Size = New System.Drawing.Size(328, 191)
        Me.grpMedicalHistory.TabIndex = 0
        Me.grpMedicalHistory.TabStop = False
        Me.grpMedicalHistory.Text = "MEDICAL HISTORY"
        '
        'lblBloodTransfusion
        '
        Me.lblBloodTransfusion.Location = New System.Drawing.Point(4, 101)
        Me.lblBloodTransfusion.Name = "lblBloodTransfusion"
        Me.lblBloodTransfusion.Size = New System.Drawing.Size(132, 20)
        Me.lblBloodTransfusion.TabIndex = 2
        Me.lblBloodTransfusion.Text = "Blood Transfusion"
        '
        'lblMedicalHistory
        '
        Me.lblMedicalHistory.Location = New System.Drawing.Point(5, 15)
        Me.lblMedicalHistory.Name = "lblMedicalHistory"
        Me.lblMedicalHistory.Size = New System.Drawing.Size(107, 20)
        Me.lblMedicalHistory.TabIndex = 0
        Me.lblMedicalHistory.Text = "Medical History"
        '
        'lblBloodTransfusionDate
        '
        Me.lblBloodTransfusionDate.Location = New System.Drawing.Point(5, 122)
        Me.lblBloodTransfusionDate.Name = "lblBloodTransfusionDate"
        Me.lblBloodTransfusionDate.Size = New System.Drawing.Size(133, 20)
        Me.lblBloodTransfusionDate.TabIndex = 4
        Me.lblBloodTransfusionDate.Text = "Blood transfusion Date"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(5, 145)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(133, 20)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Medical History Notes"
        '
        'grpOBSGyn
        '
        Me.grpOBSGyn.Controls.Add(Me.lblGynHistoryNotes)
        Me.grpOBSGyn.Controls.Add(Me.clbGynaecologicalHistory)
        Me.grpOBSGyn.Controls.Add(Me.stbGynHistoryNotes)
        Me.grpOBSGyn.Controls.Add(Me.lblGynaecologicalHistory)
        Me.grpOBSGyn.Location = New System.Drawing.Point(682, 19)
        Me.grpOBSGyn.Name = "grpOBSGyn"
        Me.grpOBSGyn.Size = New System.Drawing.Size(318, 191)
        Me.grpOBSGyn.TabIndex = 2
        Me.grpOBSGyn.TabStop = False
        Me.grpOBSGyn.Text = "OBS/GYN"
        '
        'lblGynHistoryNotes
        '
        Me.lblGynHistoryNotes.Location = New System.Drawing.Point(5, 108)
        Me.lblGynHistoryNotes.Name = "lblGynHistoryNotes"
        Me.lblGynHistoryNotes.Size = New System.Drawing.Size(133, 20)
        Me.lblGynHistoryNotes.TabIndex = 2
        Me.lblGynHistoryNotes.Text = "Gyn History Notes"
        '
        'lblGynaecologicalHistory
        '
        Me.lblGynaecologicalHistory.Location = New System.Drawing.Point(5, 29)
        Me.lblGynaecologicalHistory.Name = "lblGynaecologicalHistory"
        Me.lblGynaecologicalHistory.Size = New System.Drawing.Size(133, 20)
        Me.lblGynaecologicalHistory.TabIndex = 0
        Me.lblGynaecologicalHistory.Text = "Gynaecological History"
        '
        'grpSurgicalHistory
        '
        Me.grpSurgicalHistory.Controls.Add(Me.stbSurgicalHistoryNotes)
        Me.grpSurgicalHistory.Controls.Add(Me.lblSurgicalHistoryNotes)
        Me.grpSurgicalHistory.Controls.Add(Me.lblSurgicalHistory)
        Me.grpSurgicalHistory.Controls.Add(Me.clbSurgicalHistory)
        Me.grpSurgicalHistory.Location = New System.Drawing.Point(349, 19)
        Me.grpSurgicalHistory.Name = "grpSurgicalHistory"
        Me.grpSurgicalHistory.Size = New System.Drawing.Size(318, 191)
        Me.grpSurgicalHistory.TabIndex = 1
        Me.grpSurgicalHistory.TabStop = False
        Me.grpSurgicalHistory.Text = "SURGICAL HISTORY"
        '
        'lblSurgicalHistoryNotes
        '
        Me.lblSurgicalHistoryNotes.Location = New System.Drawing.Point(5, 103)
        Me.lblSurgicalHistoryNotes.Name = "lblSurgicalHistoryNotes"
        Me.lblSurgicalHistoryNotes.Size = New System.Drawing.Size(133, 20)
        Me.lblSurgicalHistoryNotes.TabIndex = 2
        Me.lblSurgicalHistoryNotes.Text = "Surgical History Notes"
        '
        'lblSurgicalHistory
        '
        Me.lblSurgicalHistory.Location = New System.Drawing.Point(5, 25)
        Me.lblSurgicalHistory.Name = "lblSurgicalHistory"
        Me.lblSurgicalHistory.Size = New System.Drawing.Size(133, 20)
        Me.lblSurgicalHistory.TabIndex = 0
        Me.lblSurgicalHistory.Text = "Surgical History"
        '
        'tpgFamilySocialHistory
        '
        Me.tpgFamilySocialHistory.Controls.Add(Me.grpFamilyHistory)
        Me.tpgFamilySocialHistory.Controls.Add(Me.grpSocialHistory)
        Me.tpgFamilySocialHistory.Location = New System.Drawing.Point(4, 22)
        Me.tpgFamilySocialHistory.Name = "tpgFamilySocialHistory"
        Me.tpgFamilySocialHistory.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgFamilySocialHistory.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tpgFamilySocialHistory.Size = New System.Drawing.Size(1056, 217)
        Me.tpgFamilySocialHistory.TabIndex = 1
        Me.tpgFamilySocialHistory.Text = "Family & Social"
        Me.tpgFamilySocialHistory.UseVisualStyleBackColor = True
        '
        'grpFamilyHistory
        '
        Me.grpFamilyHistory.Controls.Add(Me.clbFamilyHistory)
        Me.grpFamilyHistory.Controls.Add(Me.lblFamilyHistory)
        Me.grpFamilyHistory.Controls.Add(Me.stbFamilyHistoryNotes)
        Me.grpFamilyHistory.Controls.Add(Me.lblFamilyHistoryNotes)
        Me.grpFamilyHistory.Location = New System.Drawing.Point(5, 16)
        Me.grpFamilyHistory.Name = "grpFamilyHistory"
        Me.grpFamilyHistory.Size = New System.Drawing.Size(317, 164)
        Me.grpFamilyHistory.TabIndex = 0
        Me.grpFamilyHistory.TabStop = False
        Me.grpFamilyHistory.Text = "FAMILY HISTORY"
        '
        'lblFamilyHistory
        '
        Me.lblFamilyHistory.Location = New System.Drawing.Point(5, 26)
        Me.lblFamilyHistory.Name = "lblFamilyHistory"
        Me.lblFamilyHistory.Size = New System.Drawing.Size(134, 20)
        Me.lblFamilyHistory.TabIndex = 0
        Me.lblFamilyHistory.Text = "Family History"
        '
        'lblFamilyHistoryNotes
        '
        Me.lblFamilyHistoryNotes.Location = New System.Drawing.Point(5, 108)
        Me.lblFamilyHistoryNotes.Name = "lblFamilyHistoryNotes"
        Me.lblFamilyHistoryNotes.Size = New System.Drawing.Size(101, 20)
        Me.lblFamilyHistoryNotes.TabIndex = 2
        Me.lblFamilyHistoryNotes.Text = " Notes"
        '
        'grpSocialHistory
        '
        Me.grpSocialHistory.Controls.Add(Me.lblSocialHistoryNotes)
        Me.grpSocialHistory.Controls.Add(Me.clbSocialHistory)
        Me.grpSocialHistory.Controls.Add(Me.stbSocialHistoryNotes)
        Me.grpSocialHistory.Controls.Add(Me.lblSocialHistory)
        Me.grpSocialHistory.Location = New System.Drawing.Point(389, 16)
        Me.grpSocialHistory.Name = "grpSocialHistory"
        Me.grpSocialHistory.Size = New System.Drawing.Size(307, 164)
        Me.grpSocialHistory.TabIndex = 1
        Me.grpSocialHistory.TabStop = False
        Me.grpSocialHistory.Text = "SOCIAL HISTORY"
        '
        'lblSocialHistoryNotes
        '
        Me.lblSocialHistoryNotes.Location = New System.Drawing.Point(5, 103)
        Me.lblSocialHistoryNotes.Name = "lblSocialHistoryNotes"
        Me.lblSocialHistoryNotes.Size = New System.Drawing.Size(109, 20)
        Me.lblSocialHistoryNotes.TabIndex = 2
        Me.lblSocialHistoryNotes.Text = "Social History Notes"
        '
        'lblSocialHistory
        '
        Me.lblSocialHistory.Location = New System.Drawing.Point(6, 25)
        Me.lblSocialHistory.Name = "lblSocialHistory"
        Me.lblSocialHistory.Size = New System.Drawing.Size(101, 20)
        Me.lblSocialHistory.TabIndex = 0
        Me.lblSocialHistory.Text = "Social History"
        '
        'tpgObstetricHistory
        '
        Me.tpgObstetricHistory.Controls.Add(Me.dgvOBHistory)
        Me.tpgObstetricHistory.Location = New System.Drawing.Point(4, 22)
        Me.tpgObstetricHistory.Name = "tpgObstetricHistory"
        Me.tpgObstetricHistory.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgObstetricHistory.Size = New System.Drawing.Size(1056, 217)
        Me.tpgObstetricHistory.TabIndex = 3
        Me.tpgObstetricHistory.Text = "Obstetric"
        Me.tpgObstetricHistory.UseVisualStyleBackColor = True
        '
        'dgvOBHistory
        '
        Me.dgvOBHistory.AllowUserToOrderColumns = True
        Me.dgvOBHistory.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOBHistory.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvOBHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colObstetricPregnancy, Me.colObstetricYear, Me.colObstetricAbortionID, Me.colObstetricAbortionPeriodID, Me.colTypeOfDelivery, Me.colObstetricThirdStageID, Me.colObstetricPeurPerinumID, Me.colObstetricChildStatusID, Me.colObstetricGenderID, Me.colObstetricBirthWeight, Me.colObstetricImmunised, Me.colObstetricHealthCondition, Me.colObstetricSaved})
        Me.dgvOBHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOBHistory.EnableHeadersVisualStyles = False
        Me.dgvOBHistory.GridColor = System.Drawing.Color.Khaki
        Me.dgvOBHistory.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.dgvOBHistory.Location = New System.Drawing.Point(3, 3)
        Me.dgvOBHistory.Name = "dgvOBHistory"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOBHistory.RowHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvOBHistory.Size = New System.Drawing.Size(1050, 211)
        Me.dgvOBHistory.TabIndex = 0
        Me.dgvOBHistory.Text = "DataGridView1"
        '
        'colObstetricPregnancy
        '
        Me.colObstetricPregnancy.DataPropertyName = "Pregnancy"
        Me.colObstetricPregnancy.HeaderText = "Pregnancy"
        Me.colObstetricPregnancy.Name = "colObstetricPregnancy"
        Me.colObstetricPregnancy.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colObstetricPregnancy.Width = 60
        '
        'colObstetricYear
        '
        Me.colObstetricYear.DataPropertyName = "YearPregnant"
        Me.colObstetricYear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colObstetricYear.HeaderText = "Year"
        Me.colObstetricYear.Name = "colObstetricYear"
        Me.colObstetricYear.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colObstetricYear.Width = 60
        '
        'colObstetricAbortionID
        '
        Me.colObstetricAbortionID.DataPropertyName = "AbortionID"
        Me.colObstetricAbortionID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colObstetricAbortionID.HeaderText = "Abortion"
        Me.colObstetricAbortionID.Name = "colObstetricAbortionID"
        Me.colObstetricAbortionID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colObstetricAbortionID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colObstetricAbortionID.Width = 80
        '
        'colObstetricAbortionPeriodID
        '
        Me.colObstetricAbortionPeriodID.DataPropertyName = "AbortionPeriodID"
        Me.colObstetricAbortionPeriodID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colObstetricAbortionPeriodID.HeaderText = "Abortion Period"
        Me.colObstetricAbortionPeriodID.Name = "colObstetricAbortionPeriodID"
        '
        'colTypeOfDelivery
        '
        Me.colTypeOfDelivery.DataPropertyName = "TypeOfDeliveryID"
        Me.colTypeOfDelivery.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colTypeOfDelivery.HeaderText = "Type Of Delivery"
        Me.colTypeOfDelivery.Name = "colTypeOfDelivery"
        Me.colTypeOfDelivery.Width = 120
        '
        'colObstetricThirdStageID
        '
        Me.colObstetricThirdStageID.DataPropertyName = "ThirdStageID"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N0"
        Me.colObstetricThirdStageID.DefaultCellStyle = DataGridViewCellStyle8
        Me.colObstetricThirdStageID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colObstetricThirdStageID.HeaderText = "Third Stage"
        Me.colObstetricThirdStageID.Name = "colObstetricThirdStageID"
        Me.colObstetricThirdStageID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colObstetricThirdStageID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'colObstetricPeurPerinumID
        '
        Me.colObstetricPeurPerinumID.DataPropertyName = "PuerPeriumID"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        Me.colObstetricPeurPerinumID.DefaultCellStyle = DataGridViewCellStyle9
        Me.colObstetricPeurPerinumID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colObstetricPeurPerinumID.HeaderText = "Peur Perium"
        Me.colObstetricPeurPerinumID.Name = "colObstetricPeurPerinumID"
        Me.colObstetricPeurPerinumID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colObstetricPeurPerinumID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'colObstetricChildStatusID
        '
        Me.colObstetricChildStatusID.DataPropertyName = "ChildStatusID"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colObstetricChildStatusID.DefaultCellStyle = DataGridViewCellStyle10
        Me.colObstetricChildStatusID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.colObstetricChildStatusID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colObstetricChildStatusID.HeaderText = "Child Status"
        Me.colObstetricChildStatusID.Name = "colObstetricChildStatusID"
        Me.colObstetricChildStatusID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colObstetricChildStatusID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'colObstetricGenderID
        '
        Me.colObstetricGenderID.DataPropertyName = "GenderID"
        Me.colObstetricGenderID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colObstetricGenderID.HeaderText = "Gender"
        Me.colObstetricGenderID.Name = "colObstetricGenderID"
        Me.colObstetricGenderID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colObstetricGenderID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colObstetricGenderID.Width = 80
        '
        'colObstetricBirthWeight
        '
        Me.colObstetricBirthWeight.DataPropertyName = "BirthWeight"
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.colObstetricBirthWeight.DefaultCellStyle = DataGridViewCellStyle11
        Me.colObstetricBirthWeight.HeaderText = "Birth Weight"
        Me.colObstetricBirthWeight.Name = "colObstetricBirthWeight"
        '
        'colObstetricImmunised
        '
        Me.colObstetricImmunised.DataPropertyName = "ChildImmunised"
        Me.colObstetricImmunised.HeaderText = "Child Immunised"
        Me.colObstetricImmunised.Name = "colObstetricImmunised"
        Me.colObstetricImmunised.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colObstetricImmunised.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'colObstetricHealthCondition
        '
        Me.colObstetricHealthCondition.DataPropertyName = "HealthCondition"
        Me.colObstetricHealthCondition.HeaderText = "Child Health"
        Me.colObstetricHealthCondition.Name = "colObstetricHealthCondition"
        Me.colObstetricHealthCondition.Width = 150
        '
        'colObstetricSaved
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle12.NullValue = False
        Me.colObstetricSaved.DefaultCellStyle = DataGridViewCellStyle12
        Me.colObstetricSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colObstetricSaved.HeaderText = "Saved"
        Me.colObstetricSaved.Name = "colObstetricSaved"
        Me.colObstetricSaved.ReadOnly = True
        Me.colObstetricSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colObstetricSaved.Width = 50
        '
        'tpgPatientAllergies
        '
        Me.tpgPatientAllergies.Controls.Add(Me.dgvPatientAllergies)
        Me.tpgPatientAllergies.Location = New System.Drawing.Point(4, 22)
        Me.tpgPatientAllergies.Name = "tpgPatientAllergies"
        Me.tpgPatientAllergies.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgPatientAllergies.Size = New System.Drawing.Size(1056, 217)
        Me.tpgPatientAllergies.TabIndex = 4
        Me.tpgPatientAllergies.Text = "Allergies"
        Me.tpgPatientAllergies.UseVisualStyleBackColor = True
        '
        'dgvPatientAllergies
        '
        Me.dgvPatientAllergies.AllowUserToOrderColumns = True
        Me.dgvPatientAllergies.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPatientAllergies.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.dgvPatientAllergies.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colAllergyNo, Me.colAllergyCategory, Me.colReaction, Me.colPatientAllergiesSaved})
        Me.dgvPatientAllergies.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvPatientAllergies.EnableHeadersVisualStyles = False
        Me.dgvPatientAllergies.GridColor = System.Drawing.Color.Khaki
        Me.dgvPatientAllergies.Location = New System.Drawing.Point(3, 3)
        Me.dgvPatientAllergies.Name = "dgvPatientAllergies"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPatientAllergies.RowHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.dgvPatientAllergies.Size = New System.Drawing.Size(1050, 211)
        Me.dgvPatientAllergies.TabIndex = 0
        Me.dgvPatientAllergies.Text = "DataGridView1"
        '
        'colAllergyNo
        '
        Me.colAllergyNo.DataPropertyName = "AllergyNo"
        Me.colAllergyNo.DisplayStyleForCurrentCellOnly = True
        Me.colAllergyNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colAllergyNo.HeaderText = "Allergy"
        Me.colAllergyNo.Name = "colAllergyNo"
        Me.colAllergyNo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colAllergyNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colAllergyNo.Width = 250
        '
        'colAllergyCategory
        '
        Me.colAllergyCategory.DataPropertyName = "AllergyCategory"
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Info
        Me.colAllergyCategory.DefaultCellStyle = DataGridViewCellStyle15
        Me.colAllergyCategory.HeaderText = "Category"
        Me.colAllergyCategory.Name = "colAllergyCategory"
        Me.colAllergyCategory.ReadOnly = True
        Me.colAllergyCategory.Width = 80
        '
        'colReaction
        '
        Me.colReaction.DataPropertyName = "Reaction"
        DataGridViewCellStyle16.NullValue = Nothing
        Me.colReaction.DefaultCellStyle = DataGridViewCellStyle16
        Me.colReaction.HeaderText = "Reaction"
        Me.colReaction.MaxInputLength = 200
        Me.colReaction.Name = "colReaction"
        Me.colReaction.Width = 180
        '
        'colPatientAllergiesSaved
        '
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle17.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle17.NullValue = False
        Me.colPatientAllergiesSaved.DefaultCellStyle = DataGridViewCellStyle17
        Me.colPatientAllergiesSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colPatientAllergiesSaved.HeaderText = "Saved"
        Me.colPatientAllergiesSaved.Name = "colPatientAllergiesSaved"
        Me.colPatientAllergiesSaved.ReadOnly = True
        Me.colPatientAllergiesSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colPatientAllergiesSaved.Width = 50
        '
        'stbANCNo
        '
        Me.stbANCNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbANCNo.CapitalizeFirstLetter = False
        Me.btnSave.SetDataMember(Me.stbANCNo, "ANCNo")
        Me.stbANCNo.Enabled = False
        Me.stbANCNo.EntryErrorMSG = ""
        Me.stbANCNo.Location = New System.Drawing.Point(167, 52)
        Me.stbANCNo.Name = "stbANCNo"
        Me.stbANCNo.RegularExpression = ""
        Me.stbANCNo.Size = New System.Drawing.Size(170, 20)
        Me.stbANCNo.TabIndex = 7
        '
        'cboPartnersHIVStatusID
        '
        Me.btnSave.SetDataMember(Me.cboPartnersHIVStatusID, "PartnersHIVStatus,PartnersHIVStatusID")
        Me.cboPartnersHIVStatusID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPartnersHIVStatusID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboPartnersHIVStatusID.Location = New System.Drawing.Point(137, 3)
        Me.cboPartnersHIVStatusID.Name = "cboPartnersHIVStatusID"
        Me.cboPartnersHIVStatusID.Size = New System.Drawing.Size(170, 21)
        Me.cboPartnersHIVStatusID.TabIndex = 1
        '
        'stbPatientNo
        '
        Me.stbPatientNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPatientNo.CapitalizeFirstLetter = False
        Me.btnSave.SetDataMember(Me.stbPatientNo, "PatientNo")
        Me.stbPatientNo.EntryErrorMSG = ""
        Me.stbPatientNo.Location = New System.Drawing.Point(167, 30)
        Me.stbPatientNo.Name = "stbPatientNo"
        Me.stbPatientNo.RegularExpression = ""
        Me.stbPatientNo.Size = New System.Drawing.Size(113, 20)
        Me.stbPatientNo.TabIndex = 3
        '
        'nbxGravida
        '
        Me.nbxGravida.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxGravida.ControlCaption = "Gravida"
        Me.btnSave.SetDataMember(Me.nbxGravida, "Gravida")
        Me.nbxGravida.DecimalPlaces = -1
        Me.nbxGravida.DenyNegativeEntryValue = True
        Me.nbxGravida.Location = New System.Drawing.Point(167, 94)
        Me.nbxGravida.MaxValue = 100.0R
        Me.nbxGravida.MinValue = 0.0R
        Me.nbxGravida.MustEnterNumeric = True
        Me.nbxGravida.Name = "nbxGravida"
        Me.nbxGravida.Size = New System.Drawing.Size(170, 20)
        Me.nbxGravida.TabIndex = 11
        Me.nbxGravida.Value = ""
        '
        'dtpEnrollmentDate
        '
        Me.dtpEnrollmentDate.CustomFormat = "dd MMM yyyy"
        Me.btnSave.SetDataMember(Me.dtpEnrollmentDate, "EnrollmentDate")
        Me.dtpEnrollmentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpEnrollmentDate.Location = New System.Drawing.Point(498, 156)
        Me.dtpEnrollmentDate.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpEnrollmentDate.Name = "dtpEnrollmentDate"
        Me.dtpEnrollmentDate.ShowCheckBox = True
        Me.dtpEnrollmentDate.Size = New System.Drawing.Size(151, 20)
        Me.dtpEnrollmentDate.TabIndex = 27
        Me.dtpEnrollmentDate.Value = New Date(2018, 5, 10, 0, 0, 0, 0)
        '
        'nbxPara
        '
        Me.nbxPara.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxPara.ControlCaption = "Para"
        Me.btnSave.SetDataMember(Me.nbxPara, "Para")
        Me.nbxPara.DecimalPlaces = -1
        Me.nbxPara.DenyNegativeEntryValue = True
        Me.nbxPara.Location = New System.Drawing.Point(167, 115)
        Me.nbxPara.MaxValue = 100.0R
        Me.nbxPara.MinValue = 0.0R
        Me.nbxPara.MustEnterNumeric = True
        Me.nbxPara.Name = "nbxPara"
        Me.nbxPara.Size = New System.Drawing.Size(170, 20)
        Me.nbxPara.TabIndex = 13
        Me.nbxPara.Value = ""
        '
        'txtVisitDate
        '
        Me.txtVisitDate.BackColor = System.Drawing.SystemColors.Control
        Me.txtVisitDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtVisitDate.Enabled = False
        Me.txtVisitDate.Location = New System.Drawing.Point(781, 93)
        Me.txtVisitDate.MaxLength = 60
        Me.txtVisitDate.Name = "txtVisitDate"
        Me.txtVisitDate.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtVisitDate.Size = New System.Drawing.Size(130, 20)
        Me.txtVisitDate.TabIndex = 35
        '
        'txtAge
        '
        Me.txtAge.BackColor = System.Drawing.SystemColors.Control
        Me.txtAge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAge.Enabled = False
        Me.txtAge.Location = New System.Drawing.Point(781, 30)
        Me.txtAge.MaxLength = 60
        Me.txtAge.Name = "txtAge"
        Me.txtAge.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtAge.Size = New System.Drawing.Size(130, 20)
        Me.txtAge.TabIndex = 29
        '
        'stbJoinDate
        '
        Me.stbJoinDate.BackColor = System.Drawing.SystemColors.Control
        Me.stbJoinDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbJoinDate.Enabled = False
        Me.stbJoinDate.Location = New System.Drawing.Point(781, 72)
        Me.stbJoinDate.MaxLength = 60
        Me.stbJoinDate.Name = "stbJoinDate"
        Me.stbJoinDate.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbJoinDate.Size = New System.Drawing.Size(130, 20)
        Me.stbJoinDate.TabIndex = 33
        '
        'stbTotalVisits
        '
        Me.stbTotalVisits.BackColor = System.Drawing.SystemColors.Control
        Me.stbTotalVisits.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalVisits.Enabled = False
        Me.stbTotalVisits.Location = New System.Drawing.Point(781, 114)
        Me.stbTotalVisits.MaxLength = 60
        Me.stbTotalVisits.Name = "stbTotalVisits"
        Me.stbTotalVisits.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbTotalVisits.Size = New System.Drawing.Size(130, 20)
        Me.stbTotalVisits.TabIndex = 37
        '
        'stbBloodGroup
        '
        Me.stbBloodGroup.BackColor = System.Drawing.SystemColors.Control
        Me.stbBloodGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBloodGroup.Enabled = False
        Me.stbBloodGroup.Location = New System.Drawing.Point(498, 30)
        Me.stbBloodGroup.MaxLength = 60
        Me.stbBloodGroup.Name = "stbBloodGroup"
        Me.stbBloodGroup.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbBloodGroup.Size = New System.Drawing.Size(151, 20)
        Me.stbBloodGroup.TabIndex = 17
        '
        'lblPatientNo
        '
        Me.lblPatientNo.Location = New System.Drawing.Point(31, 32)
        Me.lblPatientNo.Name = "lblPatientNo"
        Me.lblPatientNo.Size = New System.Drawing.Size(97, 20)
        Me.lblPatientNo.TabIndex = 1
        Me.lblPatientNo.Text = "Patient No"
        '
        'lblPhoto
        '
        Me.lblPhoto.Location = New System.Drawing.Point(939, 142)
        Me.lblPhoto.Name = "lblPhoto"
        Me.lblPhoto.Size = New System.Drawing.Size(89, 20)
        Me.lblPhoto.TabIndex = 39
        Me.lblPhoto.Text = "Photo"
        Me.lblPhoto.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblVisitDate
        '
        Me.lblVisitDate.Location = New System.Drawing.Point(677, 93)
        Me.lblVisitDate.Name = "lblVisitDate"
        Me.lblVisitDate.Size = New System.Drawing.Size(77, 20)
        Me.lblVisitDate.TabIndex = 34
        Me.lblVisitDate.Text = "Last Visit Date"
        '
        'btnSearch
        '
        Me.btnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSearch.Image = CType(resources.GetObject("btnSearch.Image"), System.Drawing.Image)
        Me.btnSearch.Location = New System.Drawing.Point(134, 30)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(27, 21)
        Me.btnSearch.TabIndex = 2
        '
        'btnLoad
        '
        Me.btnLoad.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoad.Location = New System.Drawing.Point(286, 26)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(51, 24)
        Me.btnLoad.TabIndex = 4
        Me.btnLoad.Tag = ""
        Me.btnLoad.Text = "&Load"
        '
        'lblAge
        '
        Me.lblAge.Location = New System.Drawing.Point(677, 30)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(89, 20)
        Me.lblAge.TabIndex = 28
        Me.lblAge.Text = "Age"
        '
        'lblName
        '
        Me.lblName.Location = New System.Drawing.Point(29, 73)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(126, 20)
        Me.lblName.TabIndex = 8
        Me.lblName.Text = "Patient's Name"
        '
        'lblHIVStatus
        '
        Me.lblHIVStatus.Location = New System.Drawing.Point(1, 2)
        Me.lblHIVStatus.Name = "lblHIVStatus"
        Me.lblHIVStatus.Size = New System.Drawing.Size(97, 20)
        Me.lblHIVStatus.TabIndex = 0
        Me.lblHIVStatus.Text = "HIV Status"
        '
        'stbAdress
        '
        Me.stbAdress.BackColor = System.Drawing.SystemColors.Control
        Me.stbAdress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAdress.CapitalizeFirstLetter = True
        Me.stbAdress.Enabled = False
        Me.stbAdress.EntryErrorMSG = ""
        Me.stbAdress.Location = New System.Drawing.Point(498, 93)
        Me.stbAdress.MaxLength = 41
        Me.stbAdress.Multiline = True
        Me.stbAdress.Name = "stbAdress"
        Me.stbAdress.RegularExpression = ""
        Me.stbAdress.Size = New System.Drawing.Size(151, 41)
        Me.stbAdress.TabIndex = 23
        '
        'lblAddress
        '
        Me.lblAddress.Location = New System.Drawing.Point(357, 97)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(77, 20)
        Me.lblAddress.TabIndex = 22
        Me.lblAddress.Text = "Address"
        '
        'stbPhone
        '
        Me.stbPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPhone.CapitalizeFirstLetter = False
        Me.stbPhone.Enabled = False
        Me.stbPhone.EntryErrorMSG = ""
        Me.stbPhone.Location = New System.Drawing.Point(781, 51)
        Me.stbPhone.MaxLength = 30
        Me.stbPhone.Name = "stbPhone"
        Me.stbPhone.ReadOnly = True
        Me.stbPhone.RegularExpression = ""
        Me.stbPhone.Size = New System.Drawing.Size(130, 20)
        Me.stbPhone.TabIndex = 31
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(677, 51)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(77, 20)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "Telephone"
        '
        'stbOccupation
        '
        Me.stbOccupation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbOccupation.CapitalizeFirstLetter = False
        Me.stbOccupation.Enabled = False
        Me.stbOccupation.EntryErrorMSG = ""
        Me.stbOccupation.Location = New System.Drawing.Point(498, 51)
        Me.stbOccupation.Name = "stbOccupation"
        Me.stbOccupation.ReadOnly = True
        Me.stbOccupation.RegularExpression = ""
        Me.stbOccupation.Size = New System.Drawing.Size(151, 20)
        Me.stbOccupation.TabIndex = 19
        '
        'lblOccupation
        '
        Me.lblOccupation.Location = New System.Drawing.Point(357, 53)
        Me.lblOccupation.Name = "lblOccupation"
        Me.lblOccupation.Size = New System.Drawing.Size(77, 20)
        Me.lblOccupation.TabIndex = 18
        Me.lblOccupation.Text = "Occupation"
        '
        'stbMaritalStatus
        '
        Me.stbMaritalStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbMaritalStatus.CapitalizeFirstLetter = False
        Me.stbMaritalStatus.Enabled = False
        Me.stbMaritalStatus.EntryErrorMSG = ""
        Me.stbMaritalStatus.Location = New System.Drawing.Point(498, 72)
        Me.stbMaritalStatus.Name = "stbMaritalStatus"
        Me.stbMaritalStatus.ReadOnly = True
        Me.stbMaritalStatus.RegularExpression = ""
        Me.stbMaritalStatus.Size = New System.Drawing.Size(151, 20)
        Me.stbMaritalStatus.TabIndex = 21
        '
        'lblMaritalStatus
        '
        Me.lblMaritalStatus.Location = New System.Drawing.Point(357, 70)
        Me.lblMaritalStatus.Name = "lblMaritalStatus"
        Me.lblMaritalStatus.Size = New System.Drawing.Size(77, 20)
        Me.lblMaritalStatus.TabIndex = 20
        Me.lblMaritalStatus.Text = "Marital Status"
        '
        'lblJoinDate
        '
        Me.lblJoinDate.Location = New System.Drawing.Point(677, 74)
        Me.lblJoinDate.Name = "lblJoinDate"
        Me.lblJoinDate.Size = New System.Drawing.Size(77, 20)
        Me.lblJoinDate.TabIndex = 32
        Me.lblJoinDate.Text = "Join Date"
        '
        'lblNoOfVisits
        '
        Me.lblNoOfVisits.Location = New System.Drawing.Point(677, 116)
        Me.lblNoOfVisits.Name = "lblNoOfVisits"
        Me.lblNoOfVisits.Size = New System.Drawing.Size(77, 20)
        Me.lblNoOfVisits.TabIndex = 36
        Me.lblNoOfVisits.Text = "Total Visits"
        '
        'lblBloodGroup
        '
        Me.lblBloodGroup.Location = New System.Drawing.Point(357, 30)
        Me.lblBloodGroup.Name = "lblBloodGroup"
        Me.lblBloodGroup.Size = New System.Drawing.Size(71, 20)
        Me.lblBloodGroup.TabIndex = 16
        Me.lblBloodGroup.Text = "Blood Group"
        '
        'lblGravida
        '
        Me.lblGravida.Location = New System.Drawing.Point(31, 94)
        Me.lblGravida.Name = "lblGravida"
        Me.lblGravida.Size = New System.Drawing.Size(126, 20)
        Me.lblGravida.TabIndex = 10
        Me.lblGravida.Text = "Gravida"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(31, 115)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(126, 20)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Para"
        '
        'lblANCNo
        '
        Me.lblANCNo.Location = New System.Drawing.Point(31, 52)
        Me.lblANCNo.Name = "lblANCNo"
        Me.lblANCNo.Size = New System.Drawing.Size(97, 20)
        Me.lblANCNo.TabIndex = 5
        Me.lblANCNo.Text = "ANC No"
        '
        'btnFindANCNo
        '
        Me.btnFindANCNo.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnFindANCNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFindANCNo.Image = CType(resources.GetObject("btnFindANCNo.Image"), System.Drawing.Image)
        Me.btnFindANCNo.Location = New System.Drawing.Point(134, 52)
        Me.btnFindANCNo.Name = "btnFindANCNo"
        Me.btnFindANCNo.Size = New System.Drawing.Size(27, 21)
        Me.btnFindANCNo.TabIndex = 6
        '
        'lblPartnerHIVStatus
        '
        Me.lblPartnerHIVStatus.Location = New System.Drawing.Point(1, 5)
        Me.lblPartnerHIVStatus.Name = "lblPartnerHIVStatus"
        Me.lblPartnerHIVStatus.Size = New System.Drawing.Size(135, 20)
        Me.lblPartnerHIVStatus.TabIndex = 0
        Me.lblPartnerHIVStatus.Text = "Partner's HIV Status"
        '
        'pnlStatusID
        '
        Me.pnlStatusID.Controls.Add(Me.fcbStatusID)
        Me.pnlStatusID.Controls.Add(Me.lblStatusID)
        Me.pnlStatusID.Location = New System.Drawing.Point(655, 134)
        Me.pnlStatusID.Name = "pnlStatusID"
        Me.pnlStatusID.Size = New System.Drawing.Size(256, 27)
        Me.pnlStatusID.TabIndex = 38
        '
        'fcbStatusID
        '
        Me.fcbStatusID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.fcbStatusID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fcbStatusID.FormattingEnabled = True
        Me.fcbStatusID.Location = New System.Drawing.Point(125, 3)
        Me.fcbStatusID.Name = "fcbStatusID"
        Me.fcbStatusID.ReadOnly = True
        Me.fcbStatusID.Size = New System.Drawing.Size(127, 21)
        Me.fcbStatusID.TabIndex = 1
        '
        'lblStatusID
        '
        Me.lblStatusID.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblStatusID.Location = New System.Drawing.Point(21, 3)
        Me.lblStatusID.Name = "lblStatusID"
        Me.lblStatusID.Size = New System.Drawing.Size(95, 21)
        Me.lblStatusID.TabIndex = 0
        Me.lblStatusID.Text = "Status"
        '
        'pnlCreateNewRound
        '
        Me.pnlCreateNewRound.Controls.Add(Me.chkCreateNewEnrollment)
        Me.pnlCreateNewRound.Location = New System.Drawing.Point(32, 0)
        Me.pnlCreateNewRound.Name = "pnlCreateNewRound"
        Me.pnlCreateNewRound.Size = New System.Drawing.Size(169, 29)
        Me.pnlCreateNewRound.TabIndex = 0
        '
        'chkCreateNewEnrollment
        '
        Me.chkCreateNewEnrollment.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkCreateNewEnrollment.Checked = True
        Me.chkCreateNewEnrollment.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCreateNewEnrollment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkCreateNewEnrollment.Location = New System.Drawing.Point(-2, 5)
        Me.chkCreateNewEnrollment.Name = "chkCreateNewEnrollment"
        Me.chkCreateNewEnrollment.Size = New System.Drawing.Size(137, 20)
        Me.chkCreateNewEnrollment.TabIndex = 0
        Me.chkCreateNewEnrollment.Text = "Create New Enrollment"
        '
        'fbnClose
        '
        Me.fbnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.fbnClose.Location = New System.Drawing.Point(993, 479)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 43
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'pnlNavigateANCEnrollments
        '
        Me.pnlNavigateANCEnrollments.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.pnlNavigateANCEnrollments.Controls.Add(Me.chkNavigateEnrollments)
        Me.pnlNavigateANCEnrollments.Controls.Add(Me.navEnrollments)
        Me.pnlNavigateANCEnrollments.Location = New System.Drawing.Point(193, 471)
        Me.pnlNavigateANCEnrollments.Name = "pnlNavigateANCEnrollments"
        Me.pnlNavigateANCEnrollments.Size = New System.Drawing.Size(633, 39)
        Me.pnlNavigateANCEnrollments.TabIndex = 41
        Me.pnlNavigateANCEnrollments.Visible = False
        '
        'chkNavigateEnrollments
        '
        Me.chkNavigateEnrollments.AccessibleDescription = ""
        Me.chkNavigateEnrollments.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkNavigateEnrollments.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkNavigateEnrollments.Location = New System.Drawing.Point(8, 9)
        Me.chkNavigateEnrollments.Name = "chkNavigateEnrollments"
        Me.chkNavigateEnrollments.Size = New System.Drawing.Size(170, 20)
        Me.chkNavigateEnrollments.TabIndex = 0
        Me.chkNavigateEnrollments.Text = "Navigate ANC Enrollments"
        '
        'navEnrollments
        '
        Me.navEnrollments.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.navEnrollments.ColumnName = "ANCNo"
        Me.navEnrollments.DataSource = Nothing
        Me.navEnrollments.Location = New System.Drawing.Point(207, 4)
        Me.navEnrollments.Name = "navEnrollments"
        Me.navEnrollments.NavAllEnabled = False
        Me.navEnrollments.NavLeftEnabled = False
        Me.navEnrollments.NavRightEnabled = False
        Me.navEnrollments.Size = New System.Drawing.Size(413, 32)
        Me.navEnrollments.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cboPartnersHIVStatusID)
        Me.Panel1.Controls.Add(Me.lblPartnerHIVStatus)
        Me.Panel1.Location = New System.Drawing.Point(30, 162)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(314, 27)
        Me.Panel1.TabIndex = 15
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.cboHIVStatusID)
        Me.Panel2.Controls.Add(Me.lblHIVStatus)
        Me.Panel2.Location = New System.Drawing.Point(30, 136)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(314, 25)
        Me.Panel2.TabIndex = 14
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(357, 136)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 20)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "Gender"
        '
        'stbGender
        '
        Me.stbGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbGender.CapitalizeFirstLetter = False
        Me.stbGender.Enabled = False
        Me.stbGender.EntryErrorMSG = ""
        Me.stbGender.Location = New System.Drawing.Point(498, 135)
        Me.stbGender.Name = "stbGender"
        Me.stbGender.ReadOnly = True
        Me.stbGender.RegularExpression = ""
        Me.stbGender.Size = New System.Drawing.Size(151, 20)
        Me.stbGender.TabIndex = 25
        '
        'lblEnrollmentDate
        '
        Me.lblEnrollmentDate.Location = New System.Drawing.Point(357, 158)
        Me.lblEnrollmentDate.Name = "lblEnrollmentDate"
        Me.lblEnrollmentDate.Size = New System.Drawing.Size(105, 20)
        Me.lblEnrollmentDate.TabIndex = 26
        Me.lblEnrollmentDate.Text = "Enrollment Date"
        '
        'frmAntenatalEnrollment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1092, 517)
        Me.Controls.Add(Me.nbxPara)
        Me.Controls.Add(Me.dtpEnrollmentDate)
        Me.Controls.Add(Me.lblEnrollmentDate)
        Me.Controls.Add(Me.stbGender)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.pnlNavigateANCEnrollments)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.pnlCreateNewRound)
        Me.Controls.Add(Me.pnlStatusID)
        Me.Controls.Add(Me.btnFindANCNo)
        Me.Controls.Add(Me.stbANCNo)
        Me.Controls.Add(Me.lblANCNo)
        Me.Controls.Add(Me.nbxGravida)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblGravida)
        Me.Controls.Add(Me.stbBloodGroup)
        Me.Controls.Add(Me.lblBloodGroup)
        Me.Controls.Add(Me.stbTotalVisits)
        Me.Controls.Add(Me.lblNoOfVisits)
        Me.Controls.Add(Me.stbJoinDate)
        Me.Controls.Add(Me.lblJoinDate)
        Me.Controls.Add(Me.stbMaritalStatus)
        Me.Controls.Add(Me.lblMaritalStatus)
        Me.Controls.Add(Me.stbOccupation)
        Me.Controls.Add(Me.lblOccupation)
        Me.Controls.Add(Me.stbPhone)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.stbAdress)
        Me.Controls.Add(Me.lblAddress)
        Me.Controls.Add(Me.tbcAntenatalEnrollment)
        Me.Controls.Add(Me.txtAge)
        Me.Controls.Add(Me.lblAge)
        Me.Controls.Add(Me.txtFullName)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.txtVisitDate)
        Me.Controls.Add(Me.lblVisitDate)
        Me.Controls.Add(Me.spbPhoto)
        Me.Controls.Add(Me.lblPhoto)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.stbPatientNo)
        Me.Controls.Add(Me.lblPatientNo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.KeyPreview = true
        Me.MaximizeBox = false
        Me.Name = "frmAntenatalEnrollment"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "AntenatalEnrollment"
        Me.Text = "Antenatal Enrollment"
        CType(Me.spbPhoto,System.ComponentModel.ISupportInitialize).EndInit
        Me.tbcAntenatalEnrollment.ResumeLayout(false)
        Me.tpgCurrentPregnancy.ResumeLayout(false)
        Me.tbcCurrentPregnancy.ResumeLayout(false)
        Me.tpgMenstruationHistory.ResumeLayout(false)
        Me.tpgContraceptivesHistory.ResumeLayout(false)
        CType(Me.dgvContraceptivesHistory,System.ComponentModel.ISupportInitialize).EndInit
        Me.tpgPreviousIllnesses.ResumeLayout(false)
        Me.grpMedicalHistory.ResumeLayout(false)
        Me.grpMedicalHistory.PerformLayout
        Me.grpOBSGyn.ResumeLayout(false)
        Me.grpOBSGyn.PerformLayout
        Me.grpSurgicalHistory.ResumeLayout(false)
        Me.grpSurgicalHistory.PerformLayout
        Me.tpgFamilySocialHistory.ResumeLayout(false)
        Me.grpFamilyHistory.ResumeLayout(false)
        Me.grpFamilyHistory.PerformLayout
        Me.grpSocialHistory.ResumeLayout(false)
        Me.grpSocialHistory.PerformLayout
        Me.tpgObstetricHistory.ResumeLayout(false)
        CType(Me.dgvOBHistory,System.ComponentModel.ISupportInitialize).EndInit
        Me.tpgPatientAllergies.ResumeLayout(false)
        CType(Me.dgvPatientAllergies,System.ComponentModel.ISupportInitialize).EndInit
        Me.pnlStatusID.ResumeLayout(false)
        Me.pnlCreateNewRound.ResumeLayout(false)
        Me.pnlNavigateANCEnrollments.ResumeLayout(false)
        Me.Panel1.ResumeLayout(false)
        Me.Panel2.ResumeLayout(false)
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents btnSave As SyncSoft.Common.Win.Controls.EditButton
    Friend WithEvents stbPatientNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblPatientNo As System.Windows.Forms.Label
    Protected WithEvents spbPhoto As SyncSoft.Common.Win.Controls.SmartPictureBox
    Friend WithEvents lblPhoto As System.Windows.Forms.Label
    Friend WithEvents txtVisitDate As System.Windows.Forms.TextBox
    Friend WithEvents lblVisitDate As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents txtAge As System.Windows.Forms.TextBox
    Friend WithEvents lblAge As System.Windows.Forms.Label
    Friend WithEvents txtFullName As System.Windows.Forms.TextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents tbcAntenatalEnrollment As System.Windows.Forms.TabControl
    Friend WithEvents tpgCurrentPregnancy As System.Windows.Forms.TabPage
    Friend WithEvents tpgFamilySocialHistory As System.Windows.Forms.TabPage
    Friend WithEvents grpFamilyHistory As System.Windows.Forms.GroupBox
    Friend WithEvents clbFamilyHistory As System.Windows.Forms.CheckedListBox
    Friend WithEvents lblFamilyHistory As System.Windows.Forms.Label
    Friend WithEvents stbFamilyHistoryNotes As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblFamilyHistoryNotes As System.Windows.Forms.Label
    Friend WithEvents grpSocialHistory As System.Windows.Forms.GroupBox
    Friend WithEvents lblSocialHistoryNotes As System.Windows.Forms.Label
    Friend WithEvents clbSocialHistory As System.Windows.Forms.CheckedListBox
    Friend WithEvents stbSocialHistoryNotes As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblSocialHistory As System.Windows.Forms.Label
    Friend WithEvents tpgPreviousIllnesses As System.Windows.Forms.TabPage
    Friend WithEvents grpSurgicalHistory As System.Windows.Forms.GroupBox
    Friend WithEvents stbSurgicalHistoryNotes As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblSurgicalHistoryNotes As System.Windows.Forms.Label
    Friend WithEvents lblSurgicalHistory As System.Windows.Forms.Label
    Friend WithEvents clbSurgicalHistory As System.Windows.Forms.CheckedListBox
    Friend WithEvents lblHIVStatus As System.Windows.Forms.Label
    Friend WithEvents grpMedicalHistory As System.Windows.Forms.GroupBox
    Friend WithEvents clbMedicalHistory As System.Windows.Forms.CheckedListBox
    Friend WithEvents lblMedicalHistory As System.Windows.Forms.Label
    Friend WithEvents dtpBloodTransfusionDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblBloodTransfusionDate As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents stbMedicalHistoryNotes As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents grpOBSGyn As System.Windows.Forms.GroupBox
    Friend WithEvents lblGynHistoryNotes As System.Windows.Forms.Label
    Friend WithEvents clbGynaecologicalHistory As System.Windows.Forms.CheckedListBox
    Friend WithEvents stbGynHistoryNotes As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblGynaecologicalHistory As System.Windows.Forms.Label
    Friend WithEvents lblLNMP As System.Windows.Forms.Label
    Friend WithEvents chkLNMPDateReliable As System.Windows.Forms.CheckBox
    Friend WithEvents tbcCurrentPregnancy As System.Windows.Forms.TabControl
    Friend WithEvents tpgMenstruationHistory As System.Windows.Forms.TabPage
    Friend WithEvents cboCycleRegularID As System.Windows.Forms.ComboBox
    Friend WithEvents lblCycleRegularID As System.Windows.Forms.Label
    Friend WithEvents lblEDD As System.Windows.Forms.Label
    Friend WithEvents tpgContraceptivesHistory As System.Windows.Forms.TabPage
    Friend WithEvents dgvContraceptivesHistory As System.Windows.Forms.DataGridView
    Friend WithEvents stbAdress As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents stbPhone As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblScanDate As System.Windows.Forms.Label
    Friend WithEvents stbOccupation As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblOccupation As System.Windows.Forms.Label
    Friend WithEvents stbMaritalStatus As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblMaritalStatus As System.Windows.Forms.Label
    Friend WithEvents stbJoinDate As System.Windows.Forms.TextBox
    Friend WithEvents lblJoinDate As System.Windows.Forms.Label
    Friend WithEvents stbTotalVisits As System.Windows.Forms.TextBox
    Friend WithEvents lblNoOfVisits As System.Windows.Forms.Label
    Friend WithEvents tpgObstetricHistory As System.Windows.Forms.TabPage
    Friend WithEvents dgvOBHistory As System.Windows.Forms.DataGridView
    Friend WithEvents stbBloodGroup As System.Windows.Forms.TextBox
    Friend WithEvents lblBloodGroup As System.Windows.Forms.Label
    Friend WithEvents cboHIVStatusID As System.Windows.Forms.ComboBox
    Friend WithEvents lblGravida As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents nbxGravida As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents cboBloodTransfusion As System.Windows.Forms.ComboBox
    Friend WithEvents lblBloodTransfusion As System.Windows.Forms.Label
    Friend WithEvents tpgPatientAllergies As System.Windows.Forms.TabPage
    Friend WithEvents dgvPatientAllergies As System.Windows.Forms.DataGridView
    Friend WithEvents stbANCNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblANCNo As System.Windows.Forms.Label
    Friend WithEvents btnFindANCNo As System.Windows.Forms.Button
    Friend WithEvents cboPartnersHIVStatusID As System.Windows.Forms.ComboBox
    Friend WithEvents lblPartnerHIVStatus As System.Windows.Forms.Label
    Friend WithEvents pnlStatusID As System.Windows.Forms.Panel
    Friend WithEvents fcbStatusID As SyncSoft.Common.Win.Controls.FlatComboBox
    Friend WithEvents lblStatusID As System.Windows.Forms.Label
    Friend WithEvents dtpLNMP As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpEDD As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpScanDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboDonePregnancyScanID As System.Windows.Forms.ComboBox
    Friend WithEvents lblDonePregnancyScan As System.Windows.Forms.Label
    Friend WithEvents pnlCreateNewRound As System.Windows.Forms.Panel
    Friend WithEvents chkCreateNewEnrollment As System.Windows.Forms.CheckBox
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents pnlNavigateANCEnrollments As System.Windows.Forms.Panel
    Friend WithEvents chkNavigateEnrollments As System.Windows.Forms.CheckBox
    Friend WithEvents navEnrollments As SyncSoft.Common.Win.Controls.DataNavigator
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents stbGender As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblEnrollmentDate As System.Windows.Forms.Label
    Friend WithEvents dtpEnrollmentDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents colAllergyNo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colAllergyCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colReaction As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPatientAllergiesSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colContraceptiveID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colComplicationDetails As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colContraceptiveDateStarted As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colContraceptiveDiscontinued As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colContraceptiveRemovalReasons As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colContraceptiveNotes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colContraceptiveSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents nbxPara As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents colObstetricPregnancy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colObstetricYear As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colObstetricAbortionID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colObstetricAbortionPeriodID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colTypeOfDelivery As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colObstetricThirdStageID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colObstetricPeurPerinumID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colObstetricChildStatusID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colObstetricGenderID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colObstetricBirthWeight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colObstetricImmunised As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colObstetricHealthCondition As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colObstetricSaved As System.Windows.Forms.DataGridViewCheckBoxColumn

End Class