
Option Strict On

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls

Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects
Imports SyncSoft.SQLDb.Lookup.LookupDataID
Imports SyncSoft.Lookup.SQL
Imports SyncSoft.SQLDb
Imports SyncSoft.SQLDb.Lookup

Public Class frmAccountWithdrawApprovals

#Region " Fields "
    Private oRequestStatusID As New RequestStatusID()
    Private oBillModesID As New LookupDataID.BillModesID()
    Private oWithdrawTypeID As New LookupDataID.WithdrawTypeID()
    Private withdrawTypeID As String
    Private billModeID As String
#End Region


    Private Sub frmAccountWithdrawApprovals_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.Cursor = Cursors.WaitCursor()

            LoadAccountRequests()
            ExpireAccountWithdrawRequests()
            DeactivateIdleAccounts()
            Me.dtpApprovalDate.MaxDate = Today
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Public Sub LoadAccountRequests()
        Try
            Me.Cursor = Cursors.WaitCursor

            Dim lRequestStatusLookupData As New List(Of LookupData)
            Using oLookupData As New LookupData()
                With oLookupData
                    .DataID = oRequestStatusID.Approved
                    .DataDes = GetLookupDataDes(oRequestStatusID.Approved)
                    lRequestStatusLookupData.Add(oLookupData)
                End With
            End Using

            Using oLookupData As New LookupData()
                With oLookupData
                    .DataID = oRequestStatusID.Rejected
                    .DataDes = GetLookupDataDes(oRequestStatusID.Rejected)
                    lRequestStatusLookupData.Add(oLookupData)
                End With
            End Using

            With cboRequestStatusID
                .DataSource = lRequestStatusLookupData
                .DisplayMember = "DataDes"
                .ValueMember = "DataID"
            End With
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Public Sub LoadAccountRequest(requestNo As String)
        Try


            Me.Cursor = Cursors.WaitCursor
            Me.ClearControls()
            requestNo = RevertText(requestNo)
            If String.IsNullOrEmpty(requestNo) Then Return
            Dim oAccountWithdrawRequests As New AccountWithdrawRequests()
            Dim accountWithdrawRequests As DataTable = oAccountWithdrawRequests.GetAccountWithdrawRequests(requestNo).Tables("AccountWithdrawRequests")
            If accountWithdrawRequests.Rows.Count < 1 Then Return
            Dim row As DataRow = accountWithdrawRequests.Rows(0)
            Me.stbAccountBillModes.Text = StringEnteredIn(row, "AccountBillModes")
            Me.billModeID = StringEnteredIn(row, "AccountBillModesID")
            Dim accountBillNo As String = StringEnteredIn(row, "AccountBillNo")
            Me.withdrawTypeID = StringEnteredIn(row, "WithdrawTypeID")
            Me.stbAccountName.Text = StringEnteredIn(row, "AccountName")
            Me.nbxAmount.Text = FormatNumber(DecimalEnteredIn(row, "Amount", False), AppData.DecimalPlaces)
            Me.stbWithdrawType.Text = StringEnteredIn(row, "WithdrawType")
            Me.stbWithRequestReason.Text = StringEnteredIn(row, "WithdrawRequestReason")
            Dim requestDate As Date = DateEnteredIn(row, "RequestDate")
            Me.stbRequestDate.Text = FormatDate(requestDate)
            Me.stbLastValidityDateTime.Text = FormatDateTime(DateTimeEnteredIn(row, "LastValidityDateTime"))
            Me.stbWithdrawRequestNotes.Text = StringMayBeEnteredIn(row, "Notes")

            Me.LoadAccountDetails(billModeID, accountBillNo)
            Me.dtpApprovalDate.MinDate = requestDate
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub LoadAccountDetails(ByVal billModesID As String, ByVal accountNo As String)

        Dim accountName As String = String.Empty
        Dim outstandingBalance As Decimal
        Dim accountBalance As Decimal

        Dim oPatients As New SyncSoft.SQLDb.Patients()
        Dim oBillCustomers As New SyncSoft.SQLDb.BillCustomers()
        Dim oInsurances As New SyncSoft.SQLDb.Insurances()

        Try
            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbAccountName.Clear()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Select Case billModesID.ToUpper()

                Case oBillModesID.Cash.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim row As DataRow = oPatients.GetPatients(accountNo).Tables("Patients").Rows(0)

                    Me.stbAccountBillNo.Text = FormatText(accountNo, "Patients", "PatientNo")
                    accountName = StringMayBeEnteredIn(row, "FullName")
                    outstandingBalance = DecimalEnteredIn(row, "OutstandingBalance", True)
                    accountBalance = DecimalEnteredIn(row, "AccountBalance", True)
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Account.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim row As DataRow = oBillCustomers.GetBillCustomers(accountNo).Tables("BillCustomers").Rows(0)

                    Me.stbAccountBillNo.Text = FormatText(accountNo, "BillCustomers", "AccountNo").ToUpper()
                    accountName = StringMayBeEnteredIn(row, "BillCustomerName")
                    outstandingBalance = DecimalEnteredIn(row, "OutstandingBalance", True)
                    accountBalance = DecimalEnteredIn(row, "AccountBalance", True)

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    Me.stbAccountBillNo.Text = FormatText(accountNo, "BillCustomers", "AccountNo").ToUpper()
                    Dim billCustomerName = StringMayBeEnteredIn(row, "BillCustomerName")
                    Dim billCustomerTypeID As String = StringMayBeEnteredIn(row, "BillCustomerTypeID")

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Insurance.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim row As DataRow = oInsurances.GetInsurances(accountNo).Tables("Insurances").Rows(0)

                    Me.stbAccountBillNo.Text = FormatText(accountNo, "Insurances", "InsuranceNo")
                    accountName = StringMayBeEnteredIn(row, "InsuranceName")
                    outstandingBalance = DecimalEnteredIn(row, "OutstandingBalance", True)
                    accountBalance = DecimalEnteredIn(row, "AccountBalance", True)

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End Select

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbAccountName.Text = accountName
            Me.nbxAccountBalance.Text = FormatNumber(accountBalance, AppData.DecimalPlaces)
            Me.nbxOutstandingBalance.Text = FormatNumber(outstandingBalance, AppData.DecimalPlaces)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


    Private Sub ClearControls()
        Me.stbAccountBillModes.Clear()
        Me.nbxAmount.Clear()
        Me.stbAccountName.Clear()
        Me.stbWithRequestReason.Clear()
        Me.stbWithdrawRequestNotes.Clear()
    End Sub

    Private Sub frmAccountWithdrawApprovals_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub

    Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub

    Private Sub fbnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnDelete.Click

        Dim oAccountWithdrawApprovals As New SyncSoft.SQLDb.AccountWithdrawApprovals()

        Try
            Me.Cursor = Cursors.WaitCursor()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then Return

            oAccountWithdrawApprovals.RequestNo = RevertText(StringEnteredIn(Me.stbRequestNo, "Request No!"))

            DisplayMessage(oAccountWithdrawApprovals.Delete())
           
            ResetControlsIn(Me)
            Me.CallOnKeyEdit()

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub fbnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnSearch.Click

        Dim requestNo As String

        Dim oAccountWithdrawApprovals As New SyncSoft.SQLDb.AccountWithdrawApprovals()

        Try
            Me.Cursor = Cursors.WaitCursor()

            requestNo = RevertText(StringEnteredIn(Me.stbRequestNo, "Request No!"))

            Dim dataSource As DataTable = oAccountWithdrawApprovals.GetAccountWithdrawApprovals(requestNo).Tables("AccountWithdrawApprovals")
            Me.DisplayData(dataSource)
            Dim row As DataRow = dataSource.Rows(0)

            Dim requestStatusID As String = StringEnteredIn(row, "RequestStatusID")
            Dim requestStatus As String = StringEnteredIn(row, "RequestStatus")

            If requestStatusID.ToUpper().Equals(oRequestStatusID.Done.ToUpper()) Then
                DisplayMessage("The request with Request No: " + requestNo + " is already " + requestStatus + " and can't be edited!")
                fbnDelete.Enabled = False
                ebnSaveUpdate.Enabled = False
            End If
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub ebnSaveUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ebnSaveUpdate.Click

        Dim oAccountWithdrawApprovals As New SyncSoft.SQLDb.AccountWithdrawApprovals()

        Try

            Me.Cursor = Cursors.WaitCursor()

            Dim outStandingBalance As Decimal = nbxOutstandingBalance.GetDecimal(True)
            Dim accountBalance As Decimal = nbxAccountBalance.GetDecimal(True)
            Dim amount As Decimal = Me.nbxAmount.GetDecimal(False)
            Dim availableBalance As Decimal = accountBalance - outStandingBalance
            Dim accountName As String = StringEnteredIn(Me.stbAccountName, "Account Name!")
            Dim requestDate As Date = DateEnteredIn(Me.stbRequestDate, "Request Date!")
            Dim approvalDate As Date = DateEnteredIn(Me.dtpApprovalDate, "Approval Date!")
            Dim lastValidityDaTeTime As Date = DateTimeEnteredIn(stbLastValidityDateTime, "Last Validaty Date Time!")
            Dim requestStatusID As String = StringValueEnteredIn(Me.cboRequestStatusID, "Request Status!")
            Dim message As String = String.Empty


            If requestStatusID.ToUpper.Equals(oRequestStatusID.Approved.ToUpper()) Then

                If approvalDate > Today Then
                    message = "Approval date: " + FormatDate(approvalDate) + " cannot be greater than Today"
                ElseIf requestDate > approvalDate Then
                    message = "Request date: " + FormatDate(requestDate) + " cannot be greater than the approval Date: " + FormatDateTime(approvalDate)
                ElseIf requestDate > lastValidityDaTeTime Then
                    message = "Request date: " + FormatDate(requestDate) + " cannot be greater than the last Validity date: " + FormatDateTime(lastValidityDaTeTime)
                ElseIf lastValidityDaTeTime < Now Then
                    message = "The Last Validity Date: " + FormatDateTime(lastValidityDaTeTime) + " cannot be less than Now: " + FormatDateTime(Now)
                End If

                If withdrawTypeID.Equals(oWithdrawTypeID.ManualDebit.ToUpper()) Then
                    If outStandingBalance > accountBalance Then
                        message = "Account Name: " + accountName + " has outstanding balance: " + nbxOutstandingBalance.Text + " greater than to account balance. " + nbxAccountBalance.Text + " " +
                            "You cannot approve this request."
                    ElseIf availableBalance = 0 Then
                        message = "Account Name: " + accountName + " has outstanding balance: " + nbxOutstandingBalance.Text + " equal to account balance. " +
                            " You cannot make a withdraw request unless the outstanding balance is cleared"
                    End If
                End If
                If Not String.IsNullOrEmpty(message) Then
                    DisplayMessage(message)
                    Return
                End If
            End If
            With oAccountWithdrawApprovals

                .RequestNo = RevertText(StringEnteredIn(Me.stbRequestNo, "Request No!"))
                .AccountBillModes = StringEnteredIn(Me.stbAccountBillModes, "Account BillModes!")
                .AccountName = StringEnteredIn(Me.stbAccountName, "AccountName!")
                .ApprovalDate = DateEnteredIn(Me.dtpApprovalDate, "Approval Date!")
                .Notes = StringEnteredIn(Me.stbNotes, "Notes!")
                .RequestStatusID = requestStatusID
                .UserFullName = CurrentUser.FullName
                .LoginID = CurrentUser.LoginID

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ValidateEntriesIn(Me)
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End With

            Select Case Me.ebnSaveUpdate.ButtonText

                Case ButtonCaption.Save
                    oAccountWithdrawApprovals.Save()

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ResetControlsIn(Me)
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case ButtonCaption.Update
                    DisplayMessage(oAccountWithdrawApprovals.Update())
                    Me.CallOnKeyEdit()

            End Select

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

#Region " Edit Methods "

    Public Sub Edit()

        Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update
        Me.ebnSaveUpdate.Enabled = False
        Me.fbnDelete.Visible = True
        Me.fbnDelete.Enabled = False
        Me.fbnSearch.Visible = True

        ResetControlsIn(Me)

    End Sub

    Public Sub Save()

        Me.ebnSaveUpdate.ButtonText = ButtonCaption.Save
        Me.ebnSaveUpdate.Enabled = True
        Me.fbnDelete.Visible = False
        Me.fbnDelete.Enabled = True
        Me.fbnSearch.Visible = False

        ResetControlsIn(Me)

    End Sub

    Private Sub DisplayData(ByVal dataSource As DataTable)

        Try

            Me.ebnSaveUpdate.DataSource = dataSource
            Me.ebnSaveUpdate.LoadData(Me)

            Me.ebnSaveUpdate.Enabled = dataSource.Rows.Count > 0
            Me.fbnDelete.Enabled = dataSource.Rows.Count > 0

            Security.Apply(Me.ebnSaveUpdate, AccessRights.Update)
            Security.Apply(Me.fbnDelete, AccessRights.Delete)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub CallOnKeyEdit()
        If Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update Then
            Me.ebnSaveUpdate.Enabled = False
            Me.fbnDelete.Enabled = False
        End If
    End Sub

#End Region

    Private Sub btnLoad_Click(sender As System.Object, e As System.EventArgs) Handles btnLoad.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim frmToApproveAccountWithdrawRequests As New frmToApproveAccountWithdrawRequests(oRequestStatusID.Pending, Me.stbRequestNo)
            frmToApproveAccountWithdrawRequests.ShowDialog()

            Me.LoadAccountRequest(stbRequestNo.Text)
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub stbRequestNo_Leave(sender As Object, e As System.EventArgs) Handles stbRequestNo.Leave
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim requestNo As String = StringMayBeEnteredIn(stbRequestNo)
            If String.IsNullOrEmpty(requestNo) Then Return
            Me.LoadAccountRequest(requestNo)

        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub




    Private Sub stbRequestNo_TextChanged(sender As System.Object, e As System.EventArgs) Handles stbRequestNo.TextChanged
        Me.CallOnKeyEdit()
    End Sub
End Class