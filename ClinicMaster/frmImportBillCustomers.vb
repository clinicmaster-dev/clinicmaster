﻿
Option Strict On

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.Enumerations
Imports SyncSoft.Common.Win.Controls

Imports LookupData = SyncSoft.Lookup.SQL.LookupData
Imports LookupDataID = SyncSoft.SQLDb.Lookup.LookupDataID

Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects
Imports LookupCommDataID = SyncSoft.Common.Lookup.LookupCommDataID
Imports LookupCommObjects = SyncSoft.Common.Lookup.LookupCommObjects

Imports System.IO
Imports System.Text
Imports System.Collections.Generic

Public Class frmImportBillCustomers

#Region " Fields "
    Private _ErrorLog As New StringBuilder(String.Empty)
#End Region

    Private Sub frmImportBillCustomers_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub

    Private Sub stbFileName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stbFileName.TextChanged
        Me.ResetCTLS()
    End Sub

    Private Sub stbWorksheetName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stbWorksheetName.TextChanged
        Me.ResetCTLS()
    End Sub

    Private Sub ResetCTLS()
        Me.lblRecordsImported.Text = String.Empty
        Me.dgvImportedData.Rows.Clear()
        Me.fbnErrorLog.Enabled = False
        Me.lblSaveReport.Text = String.Empty
        Me.fbnExport.Visible = False
    End Sub

    Private Sub fbnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnBrowse.Click

        Dim openFileDLG As New OpenFileDialog()

        Try
            Me.Cursor = Cursors.WaitCursor

            With openFileDLG

                Try
                    .InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
                Catch ex As Exception
                    Exit Try
                End Try

                .Filter = "Microsoft Excel Files (*.xlsx)|*.xlsx|Excel Files (*.xls)|*.xls"
                If .ShowDialog = Windows.Forms.DialogResult.OK Then Me.stbFileName.Text = .FileName.ToString()

            End With

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub fbnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnImport.Click

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim path As String = StringEnteredIn(Me.stbFileName, "File Name!")
            Dim workSheetName As String = StringEnteredIn(Me.stbWorksheetName, "Work Sheet Name!")
            Dim range As String = ""
            Dim where As String = "where [Account No] is not null"

            Dim criterion As String = "select * from [" + workSheetName + "$" + range + "] " + where
            Dim importedData As DataTable = ImportFromExcel(path, criterion)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.ResetCTLS()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            For pos As Integer = 0 To importedData.Rows.Count - 1

                Dim row As DataRow = importedData.Rows(pos)

                With Me.dgvImportedData

                    .Rows.Add()

                    .Item(Me.colID.Name, pos).Value = IntegerEnteredIn(row, "ID")
                    .Item(Me.colAccountNo.Name, pos).Value = StringMayBeEnteredIn(row, "Account No")
                    .Item(Me.colBillCustomerName.Name, pos).Value = StringMayBeEnteredIn(row, "To-Bill Customer Name")
                    .Item(Me.colBillCustomerType.Name, pos).Value = StringMayBeEnteredIn(row, "Bill Customer Type")
                    .Item(Me.colInsuranceNo.Name, pos).Value = StringMayBeEnteredIn(row, "Insurance No")
                    .Item(Me.colContactPerson.Name, pos).Value = StringMayBeEnteredIn(row, "Contact Person")
                    .Item(Me.colAddress.Name, pos).Value = StringMayBeEnteredIn(row, "Address")
                    .Item(Me.colPhone.Name, pos).Value = StringMayBeEnteredIn(row, "Phone No")
                    .Item(Me.colCoPayType.Name, pos).Value = StringMayBeEnteredIn(row, "Co-Pay Type")
                    .Item(Me.colCoPayPercent.Name, pos).Value = SingleMayBeEnteredIn(row, "Co-Pay Percent")
                    .Item(Me.colCoPayValue.Name, pos).Value = DecimalMayBeEnteredIn(row, "Co-Pay Value")
                    .Item(Me.colCreditLimit.Name, pos).Value = DecimalMayBeEnteredIn(row, "Credit Limit")
                    .Item(Me.colAllowOnlyListedMember.Name, pos).Value = BooleanMayBeEnteredIn(row, "Allow Only Listed Member")
                    .Item(Me.colUseCustomFee.Name, pos).Value = BooleanMayBeEnteredIn(row, "Use Custom Fee")
                    .Item(Me.colSmartCardApplicable.Name, pos).Value = BooleanMayBeEnteredIn(row, "Smart Card Applicable")
                    .Item(Me.colCaptureMemberCardNo.Name, pos).Value = BooleanMayBeEnteredIn(row, "Capture Member Card No")
                    .Item(Me.colCaptureClaimReferenceNo.Name, pos).Value = BooleanMayBeEnteredIn(row, "Capture Claim Reference No")

                End With

            Next

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.lblRecordsImported.Text = importedData.Rows.Count.ToString() + " record(s) were imported for To-Bill Customers!"
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub fbnSaveAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnSaveAll.Click

        Dim savedRows As New List(Of Integer)

        Try
            Me.Cursor = Cursors.WaitCursor

            Me._ErrorLog.Remove(0, Me._ErrorLog.Length)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.dgvImportedData.RowCount < 1 Then Throw New ArgumentException("Must have at least one entry for To-Bill Customers!")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            For rowNo As Integer = 0 To Me.dgvImportedData.RowCount - 1

                Dim id As Integer
                Dim oStatusID As New LookupCommDataID.StatusID()

                Try

                    Me.Cursor = Cursors.WaitCursor

                    Using oBillCustomers As New SyncSoft.SQLDb.BillCustomers()

                        Dim cells As DataGridViewCellCollection = Me.dgvImportedData.Rows(rowNo).Cells

                        id = IntegerEnteredIn(cells, Me.colID)
                        Dim billCustomerType As String = StringEnteredIn(cells, Me.colBillCustomerType, "To-Bill Customer Type!")
                        Dim coPayType As String = StringEnteredIn(cells, Me.colCoPayType, "Co-Pay Type!")

                        With oBillCustomers

                            .AccountNo = RevertText(StringEnteredIn(cells, Me.colAccountNo))
                            .BillCustomerName = StringEnteredIn(cells, Me.colBillCustomerName, "To-Bill Customer Name!")
                            If String.IsNullOrEmpty(GetLookupDataID(LookupObjects.BillCustomerType, billCustomerType)) Then
                                Throw New ArgumentException("To-Bill Customer Type has wrong value!")
                            Else : .BillCustomerTypeID = GetLookupDataID(LookupObjects.BillCustomerType, billCustomerType)
                            End If
                            .InsuranceNo = RevertText(StringMayBeEnteredIn(cells, Me.colInsuranceNo))
                            .ContactPerson = StringMayBeEnteredIn(cells, Me.colContactPerson)
                            .Address = StringMayBeEnteredIn(cells, Me.colAddress)
                            .Phone = StringMayBeEnteredIn(cells, Me.colPhone)
                            .Fax = String.Empty
                            .Email = String.Empty
                            .Website = String.Empty
                            .LogoPhoto = Nothing
                            .MemberDeclaration = String.Empty
                            .DoctorDeclaration = String.Empty
                            If String.IsNullOrEmpty(GetLookupDataID(LookupObjects.CoPayType, coPayType)) Then
                                Throw New ArgumentException("Co-Pay Type has wrong value!")
                            Else : .CoPayTypeID = GetLookupDataID(LookupObjects.CoPayType, coPayType)
                            End If
                            .CoPayPercent = SingleMayBeEnteredIn(cells, Me.colCoPayPercent)
                            .CoPayValue = DecimalMayBeEnteredIn(cells, Me.colCoPayValue)
                            .CreditLimit = DecimalMayBeEnteredIn(cells, Me.colCreditLimit)
                            .AllowOnlyListedMember = BooleanMayBeEnteredIn(cells, Me.colAllowOnlyListedMember)
                            .UseCustomFee = BooleanMayBeEnteredIn(cells, Me.colUseCustomFee)
                            .SmartCardApplicable = BooleanMayBeEnteredIn(cells, Me.colSmartCardApplicable)
                            .CaptureMemberCardNo = BooleanMayBeEnteredIn(cells, Me.colCaptureMemberCardNo)
                            .CaptureClaimReferenceNo = BooleanMayBeEnteredIn(cells, Me.colCaptureClaimReferenceNo)
                            .Hidden = False
                            .AccountStatusID = oStatusID.Active
                            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            .Save()
                            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        End With

                    End Using

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    savedRows.Add(rowNo)
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Catch ex As Exception
                    Me._ErrorLog.Append("*** Imported record ID " + id.ToString() + ": " + ex.Message)
                    Me._ErrorLog.AppendLine()
                    Me._ErrorLog.AppendLine()

                Finally
                    Me.Cursor = Cursors.Default

                End Try

            Next

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me._ErrorLog.Length > 0 Then
                Me.fbnErrorLog.Enabled = True
                Me.fbnExport.Visible = True
            Else
                Me.fbnErrorLog.Enabled = False
                Me.fbnExport.Visible = False
            End If

            Dim saveMSG As String = "{" + savedRows.Count.ToString() + "} Records saved successfully and {" + (Me.dgvImportedData.RowCount - savedRows.Count).ToString() + "} failed."
            Me.lblSaveReport.Text = saveMSG
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            For pos As Integer = savedRows.Count - 1 To 0 Step -1
                Me.dgvImportedData.Rows.RemoveAt(savedRows.Item(pos))
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub fbnErrorLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnErrorLog.Click

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim importLogFile As String = My.Computer.FileSystem.SpecialDirectories.Temp + "\BillCustomers Import Log.txt"

            File.WriteAllText(importLogFile, Me._ErrorLog.ToString())

            If File.Exists(importLogFile) Then
                Process.Start(importLogFile)
            Else : DisplayMessage("No Import Error Message Logged!")
            End If

        Catch IOeX As IOException
            ErrorMessage(IOeX)

        Catch eX As Exception
            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub fbnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnExport.Click

        Try
            Me.Cursor = Cursors.WaitCursor

            ExportToExcel(Me.dgvImportedData, Me.stbWorksheetName.Text)

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

End Class