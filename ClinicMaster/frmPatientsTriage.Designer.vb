﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPatientsTriage
    Inherits System.Windows.Forms.Form

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal patientNo As String)
        MyClass.New()
        Me.PatientNo = patientNo
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub



    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPatientsTriage))
        Me.dgvTriage = New System.Windows.Forms.DataGridView()
        Me.colVisitNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colVisitDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colWeight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTemperature = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colHeight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMUAC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colHeartRate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBloodPressure = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colHeadCircum = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRespirationRate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOxygenSaturation = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNotes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.stbVisitDate = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblVisitDate = New System.Windows.Forms.Label()
        Me.stbJoinDate = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblJoinDate = New System.Windows.Forms.Label()
        Me.spbPhoto = New SyncSoft.Common.Win.Controls.SmartPictureBox()
        Me.stbPatientNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblHospitalPID = New System.Windows.Forms.Label()
        Me.stbAge = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbGender = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblAge = New System.Windows.Forms.Label()
        Me.lblGenderID = New System.Windows.Forms.Label()
        Me.stbFullName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblName = New System.Windows.Forms.Label()
        Me.pnlAlerts = New System.Windows.Forms.Panel()
        Me.stbPhone = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblPhone = New System.Windows.Forms.Label()
        Me.stbTotalVisits = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblTotalVisits = New System.Windows.Forms.Label()
        Me.stbLastVisitDate = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblLastVisitDate = New System.Windows.Forms.Label()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        CType(Me.dgvTriage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.spbPhoto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAlerts.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvTriage
        '
        Me.dgvTriage.AllowUserToAddRows = False
        Me.dgvTriage.AllowUserToDeleteRows = False
        Me.dgvTriage.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvTriage.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvTriage.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvTriage.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvTriage.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTriage.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvTriage.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTriage.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvTriage.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colVisitNo, Me.colVisitDate, Me.colWeight, Me.colTemperature, Me.colHeight, Me.colMUAC, Me.colHeartRate, Me.colBloodPressure, Me.colHeadCircum, Me.colRespirationRate, Me.colOxygenSaturation, Me.colNotes})
        Me.dgvTriage.EnableHeadersVisualStyles = False
        Me.dgvTriage.GridColor = System.Drawing.Color.Khaki
        Me.dgvTriage.Location = New System.Drawing.Point(5, 145)
        Me.dgvTriage.Name = "dgvTriage"
        Me.dgvTriage.ReadOnly = True
        Me.dgvTriage.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTriage.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvTriage.RowHeadersVisible = False
        Me.dgvTriage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvTriage.Size = New System.Drawing.Size(883, 184)
        Me.dgvTriage.TabIndex = 3
        Me.dgvTriage.Text = "DataGridView1"
        '
        'colVisitNo
        '
        Me.colVisitNo.DataPropertyName = "VisitNo"
        Me.colVisitNo.HeaderText = "Visit No"
        Me.colVisitNo.Name = "colVisitNo"
        Me.colVisitNo.ReadOnly = True
        '
        'colVisitDate
        '
        Me.colVisitDate.DataPropertyName = "VisitDate"
        Me.colVisitDate.HeaderText = "Visit Date"
        Me.colVisitDate.Name = "colVisitDate"
        Me.colVisitDate.ReadOnly = True
        '
        'colWeight
        '
        Me.colWeight.DataPropertyName = "Weight"
        Me.colWeight.HeaderText = "Weight"
        Me.colWeight.Name = "colWeight"
        Me.colWeight.ReadOnly = True
        Me.colWeight.Width = 50
        '
        'colTemperature
        '
        Me.colTemperature.DataPropertyName = "Temperature"
        Me.colTemperature.HeaderText = "Temperature"
        Me.colTemperature.Name = "colTemperature"
        Me.colTemperature.ReadOnly = True
        Me.colTemperature.Width = 70
        '
        'colHeight
        '
        Me.colHeight.DataPropertyName = "Height"
        Me.colHeight.HeaderText = "Height"
        Me.colHeight.Name = "colHeight"
        Me.colHeight.ReadOnly = True
        Me.colHeight.Width = 50
        '
        'colMUAC
        '
        Me.colMUAC.DataPropertyName = "MUAC"
        Me.colMUAC.HeaderText = "MUAC"
        Me.colMUAC.Name = "colMUAC"
        Me.colMUAC.ReadOnly = True
        '
        'colHeartRate
        '
        Me.colHeartRate.DataPropertyName = "HeartRate"
        Me.colHeartRate.HeaderText = "Heart Rate"
        Me.colHeartRate.Name = "colHeartRate"
        Me.colHeartRate.ReadOnly = True
        Me.colHeartRate.Width = 75
        '
        'colBloodPressure
        '
        Me.colBloodPressure.DataPropertyName = "BloodPressure"
        Me.colBloodPressure.HeaderText = "Blood Pressure"
        Me.colBloodPressure.Name = "colBloodPressure"
        Me.colBloodPressure.ReadOnly = True
        Me.colBloodPressure.Width = 85
        '
        'colHeadCircum
        '
        Me.colHeadCircum.DataPropertyName = "HeadCircum"
        Me.colHeadCircum.HeaderText = "Head Circum"
        Me.colHeadCircum.Name = "colHeadCircum"
        Me.colHeadCircum.ReadOnly = True
        Me.colHeadCircum.Width = 80
        '
        'colRespirationRate
        '
        Me.colRespirationRate.DataPropertyName = "RespirationRate"
        Me.colRespirationRate.HeaderText = "Respiration Rate"
        Me.colRespirationRate.Name = "colRespirationRate"
        Me.colRespirationRate.ReadOnly = True
        '
        'colOxygenSaturation
        '
        Me.colOxygenSaturation.DataPropertyName = "OxygenSaturation"
        Me.colOxygenSaturation.HeaderText = "Oxygen Saturation"
        Me.colOxygenSaturation.Name = "colOxygenSaturation"
        Me.colOxygenSaturation.ReadOnly = True
        Me.colOxygenSaturation.Visible = False
        '
        'colNotes
        '
        Me.colNotes.DataPropertyName = "Notes"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colNotes.DefaultCellStyle = DataGridViewCellStyle3
        Me.colNotes.HeaderText = "Notes"
        Me.colNotes.Name = "colNotes"
        Me.colNotes.ReadOnly = True
        Me.colNotes.Width = 150
        '
        'stbVisitDate
        '
        Me.stbVisitDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbVisitDate.CapitalizeFirstLetter = False
        Me.stbVisitDate.Enabled = False
        Me.stbVisitDate.EntryErrorMSG = ""
        Me.stbVisitDate.Location = New System.Drawing.Point(114, 87)
        Me.stbVisitDate.MaxLength = 60
        Me.stbVisitDate.Name = "stbVisitDate"
        Me.stbVisitDate.RegularExpression = ""
        Me.stbVisitDate.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbVisitDate.Size = New System.Drawing.Size(152, 20)
        Me.stbVisitDate.TabIndex = 55
        '
        'lblVisitDate
        '
        Me.lblVisitDate.Location = New System.Drawing.Point(3, 86)
        Me.lblVisitDate.Name = "lblVisitDate"
        Me.lblVisitDate.Size = New System.Drawing.Size(105, 20)
        Me.lblVisitDate.TabIndex = 54
        Me.lblVisitDate.Text = "Visit Date"
        '
        'stbJoinDate
        '
        Me.stbJoinDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbJoinDate.CapitalizeFirstLetter = False
        Me.stbJoinDate.Enabled = False
        Me.stbJoinDate.EntryErrorMSG = ""
        Me.stbJoinDate.Location = New System.Drawing.Point(114, 108)
        Me.stbJoinDate.MaxLength = 60
        Me.stbJoinDate.Name = "stbJoinDate"
        Me.stbJoinDate.RegularExpression = ""
        Me.stbJoinDate.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbJoinDate.Size = New System.Drawing.Size(152, 20)
        Me.stbJoinDate.TabIndex = 57
        '
        'lblJoinDate
        '
        Me.lblJoinDate.Location = New System.Drawing.Point(3, 107)
        Me.lblJoinDate.Name = "lblJoinDate"
        Me.lblJoinDate.Size = New System.Drawing.Size(105, 20)
        Me.lblJoinDate.TabIndex = 56
        Me.lblJoinDate.Text = "Join Date"
        '
        'spbPhoto
        '
        Me.spbPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.spbPhoto.Image = CType(resources.GetObject("spbPhoto.Image"), System.Drawing.Image)
        Me.spbPhoto.ImageSizeLimit = CType(200000, Long)
        Me.spbPhoto.InitialImage = CType(resources.GetObject("spbPhoto.InitialImage"), System.Drawing.Image)
        Me.spbPhoto.Location = New System.Drawing.Point(539, 3)
        Me.spbPhoto.Name = "spbPhoto"
        Me.spbPhoto.ReadOnly = True
        Me.spbPhoto.Size = New System.Drawing.Size(100, 100)
        Me.spbPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.spbPhoto.TabIndex = 58
        Me.spbPhoto.TabStop = False
        '
        'stbPatientNo
        '
        Me.stbPatientNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPatientNo.CapitalizeFirstLetter = False
        Me.stbPatientNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.stbPatientNo.Enabled = False
        Me.stbPatientNo.EntryErrorMSG = ""
        Me.stbPatientNo.Location = New System.Drawing.Point(114, 3)
        Me.stbPatientNo.MaxLength = 20
        Me.stbPatientNo.Name = "stbPatientNo"
        Me.stbPatientNo.RegularExpression = ""
        Me.stbPatientNo.Size = New System.Drawing.Size(152, 20)
        Me.stbPatientNo.TabIndex = 47
        '
        'lblHospitalPID
        '
        Me.lblHospitalPID.Location = New System.Drawing.Point(3, 3)
        Me.lblHospitalPID.Name = "lblHospitalPID"
        Me.lblHospitalPID.Size = New System.Drawing.Size(105, 20)
        Me.lblHospitalPID.TabIndex = 46
        Me.lblHospitalPID.Text = "Patient No"
        '
        'stbAge
        '
        Me.stbAge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAge.CapitalizeFirstLetter = False
        Me.stbAge.Enabled = False
        Me.stbAge.EntryErrorMSG = ""
        Me.stbAge.Location = New System.Drawing.Point(114, 45)
        Me.stbAge.MaxLength = 60
        Me.stbAge.Name = "stbAge"
        Me.stbAge.RegularExpression = ""
        Me.stbAge.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbAge.Size = New System.Drawing.Size(152, 20)
        Me.stbAge.TabIndex = 51
        '
        'stbGender
        '
        Me.stbGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbGender.CapitalizeFirstLetter = False
        Me.stbGender.Enabled = False
        Me.stbGender.EntryErrorMSG = ""
        Me.stbGender.Location = New System.Drawing.Point(114, 66)
        Me.stbGender.MaxLength = 60
        Me.stbGender.Name = "stbGender"
        Me.stbGender.RegularExpression = ""
        Me.stbGender.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbGender.Size = New System.Drawing.Size(152, 20)
        Me.stbGender.TabIndex = 53
        '
        'lblAge
        '
        Me.lblAge.Location = New System.Drawing.Point(3, 46)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(105, 20)
        Me.lblAge.TabIndex = 50
        Me.lblAge.Text = "Age"
        '
        'lblGenderID
        '
        Me.lblGenderID.Location = New System.Drawing.Point(3, 67)
        Me.lblGenderID.Name = "lblGenderID"
        Me.lblGenderID.Size = New System.Drawing.Size(105, 20)
        Me.lblGenderID.TabIndex = 52
        Me.lblGenderID.Text = "Gender"
        '
        'stbFullName
        '
        Me.stbFullName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbFullName.CapitalizeFirstLetter = False
        Me.stbFullName.Enabled = False
        Me.stbFullName.EntryErrorMSG = ""
        Me.stbFullName.Location = New System.Drawing.Point(114, 24)
        Me.stbFullName.MaxLength = 41
        Me.stbFullName.Name = "stbFullName"
        Me.stbFullName.RegularExpression = ""
        Me.stbFullName.Size = New System.Drawing.Size(152, 20)
        Me.stbFullName.TabIndex = 49
        '
        'lblName
        '
        Me.lblName.Location = New System.Drawing.Point(3, 26)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(105, 20)
        Me.lblName.TabIndex = 48
        Me.lblName.Text = "Patient's Name"
        '
        'pnlAlerts
        '
        Me.pnlAlerts.Controls.Add(Me.stbPhone)
        Me.pnlAlerts.Controls.Add(Me.lblPhone)
        Me.pnlAlerts.Controls.Add(Me.stbTotalVisits)
        Me.pnlAlerts.Controls.Add(Me.lblTotalVisits)
        Me.pnlAlerts.Controls.Add(Me.stbLastVisitDate)
        Me.pnlAlerts.Controls.Add(Me.lblLastVisitDate)
        Me.pnlAlerts.Controls.Add(Me.lblHospitalPID)
        Me.pnlAlerts.Controls.Add(Me.stbVisitDate)
        Me.pnlAlerts.Controls.Add(Me.lblName)
        Me.pnlAlerts.Controls.Add(Me.lblVisitDate)
        Me.pnlAlerts.Controls.Add(Me.stbFullName)
        Me.pnlAlerts.Controls.Add(Me.stbJoinDate)
        Me.pnlAlerts.Controls.Add(Me.lblGenderID)
        Me.pnlAlerts.Controls.Add(Me.lblJoinDate)
        Me.pnlAlerts.Controls.Add(Me.lblAge)
        Me.pnlAlerts.Controls.Add(Me.spbPhoto)
        Me.pnlAlerts.Controls.Add(Me.stbGender)
        Me.pnlAlerts.Controls.Add(Me.stbPatientNo)
        Me.pnlAlerts.Controls.Add(Me.stbAge)
        Me.pnlAlerts.Enabled = False
        Me.pnlAlerts.Location = New System.Drawing.Point(5, 4)
        Me.pnlAlerts.Name = "pnlAlerts"
        Me.pnlAlerts.Size = New System.Drawing.Size(643, 135)
        Me.pnlAlerts.TabIndex = 59
        '
        'stbPhone
        '
        Me.stbPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPhone.CapitalizeFirstLetter = False
        Me.stbPhone.Enabled = False
        Me.stbPhone.EntryErrorMSG = ""
        Me.stbPhone.Location = New System.Drawing.Point(384, 3)
        Me.stbPhone.MaxLength = 60
        Me.stbPhone.Name = "stbPhone"
        Me.stbPhone.RegularExpression = ""
        Me.stbPhone.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbPhone.Size = New System.Drawing.Size(149, 20)
        Me.stbPhone.TabIndex = 64
        '
        'lblPhone
        '
        Me.lblPhone.Location = New System.Drawing.Point(269, 5)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(109, 20)
        Me.lblPhone.TabIndex = 63
        Me.lblPhone.Text = "Phone"
        '
        'stbTotalVisits
        '
        Me.stbTotalVisits.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalVisits.CapitalizeFirstLetter = False
        Me.stbTotalVisits.Enabled = False
        Me.stbTotalVisits.EntryErrorMSG = ""
        Me.stbTotalVisits.Location = New System.Drawing.Point(384, 45)
        Me.stbTotalVisits.MaxLength = 60
        Me.stbTotalVisits.Name = "stbTotalVisits"
        Me.stbTotalVisits.RegularExpression = ""
        Me.stbTotalVisits.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbTotalVisits.Size = New System.Drawing.Size(149, 20)
        Me.stbTotalVisits.TabIndex = 68
        '
        'lblTotalVisits
        '
        Me.lblTotalVisits.Location = New System.Drawing.Point(269, 46)
        Me.lblTotalVisits.Name = "lblTotalVisits"
        Me.lblTotalVisits.Size = New System.Drawing.Size(109, 20)
        Me.lblTotalVisits.TabIndex = 67
        Me.lblTotalVisits.Text = "Total Visits"
        '
        'stbLastVisitDate
        '
        Me.stbLastVisitDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbLastVisitDate.CapitalizeFirstLetter = False
        Me.stbLastVisitDate.Enabled = False
        Me.stbLastVisitDate.EntryErrorMSG = ""
        Me.stbLastVisitDate.Location = New System.Drawing.Point(384, 24)
        Me.stbLastVisitDate.MaxLength = 20
        Me.stbLastVisitDate.Name = "stbLastVisitDate"
        Me.stbLastVisitDate.RegularExpression = ""
        Me.stbLastVisitDate.Size = New System.Drawing.Size(149, 20)
        Me.stbLastVisitDate.TabIndex = 66
        '
        'lblLastVisitDate
        '
        Me.lblLastVisitDate.Location = New System.Drawing.Point(269, 26)
        Me.lblLastVisitDate.Name = "lblLastVisitDate"
        Me.lblLastVisitDate.Size = New System.Drawing.Size(109, 20)
        Me.lblLastVisitDate.TabIndex = 65
        Me.lblLastVisitDate.Text = "Last Visit Date"
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRefresh.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefresh.Location = New System.Drawing.Point(14, 338)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(80, 24)
        Me.btnRefresh.TabIndex = 61
        Me.btnRefresh.Tag = ""
        Me.btnRefresh.Text = "&Refresh"
        '
        'fbnClose
        '
        Me.fbnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(800, 338)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 60
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'frmPatientsTriage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(887, 368)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.pnlAlerts)
        Me.Controls.Add(Me.dgvTriage)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPatientsTriage"
        Me.Text = "Patient's Triage"
        CType(Me.dgvTriage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.spbPhoto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAlerts.ResumeLayout(False)
        Me.pnlAlerts.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvTriage As System.Windows.Forms.DataGridView
    Friend WithEvents stbVisitDate As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblVisitDate As System.Windows.Forms.Label
    Friend WithEvents stbJoinDate As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblJoinDate As System.Windows.Forms.Label
    Friend WithEvents spbPhoto As SyncSoft.Common.Win.Controls.SmartPictureBox
    Friend WithEvents stbPatientNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblHospitalPID As System.Windows.Forms.Label
    Friend WithEvents stbAge As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents stbGender As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAge As System.Windows.Forms.Label
    Friend WithEvents lblGenderID As System.Windows.Forms.Label
    Friend WithEvents stbFullName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents pnlAlerts As System.Windows.Forms.Panel
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents stbPhone As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblPhone As System.Windows.Forms.Label
    Friend WithEvents stbTotalVisits As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblTotalVisits As System.Windows.Forms.Label
    Friend WithEvents stbLastVisitDate As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblLastVisitDate As System.Windows.Forms.Label
    Friend WithEvents colVisitNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colVisitDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colWeight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTemperature As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colHeight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colMUAC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colHeartRate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBloodPressure As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colHeadCircum As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRespirationRate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOxygenSaturation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNotes As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
