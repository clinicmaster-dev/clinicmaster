﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDueInvoices
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle36 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle37 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDueInvoices))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle45 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle38 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle39 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle40 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle41 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle42 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle43 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle44 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tbcExtraBillsInvoice = New System.Windows.Forms.TabControl()
        Me.tpgExtraBills = New System.Windows.Forms.TabPage()
        Me.dgvExtraBillsInvoice = New System.Windows.Forms.DataGridView()
        Me.cmsDueInvoices = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmsDueInvoicesCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmsDueInvoicesSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmsDueInvoicesExtraBillNoDetails = New System.Windows.Forms.ToolStripMenuItem()
        Me.tpgOPDInvoices = New System.Windows.Forms.TabPage()
        Me.dgvOPDBillsInvoice = New System.Windows.Forms.DataGridView()
        Me.colInvoiceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInvoiceDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDPatientNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVisitNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDGender = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDAge = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDMemberCardNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnPrintPreview = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.grpSetParameters = New System.Windows.Forms.GroupBox()
        Me.pnlPeriod = New System.Windows.Forms.Panel()
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker()
        Me.lblStartDate = New System.Windows.Forms.Label()
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker()
        Me.lblEndDate = New System.Windows.Forms.Label()
        Me.btnLoadPendingBillsPayment = New System.Windows.Forms.Button()
        Me.lblRecordsNo = New System.Windows.Forms.Label()
        Me.btnLoadPatients = New System.Windows.Forms.Button()
        Me.btnFindPatientNo = New System.Windows.Forms.Button()
        Me.cboBillModesID = New System.Windows.Forms.ComboBox()
        Me.lblBillModesID = New System.Windows.Forms.Label()
        Me.stbBillCustomerName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboBillAccountNo = New System.Windows.Forms.ComboBox()
        Me.lblBillCustomerName = New System.Windows.Forms.Label()
        Me.lblBillAccountNo = New System.Windows.Forms.Label()
        Me.pnlBill = New System.Windows.Forms.Panel()
        Me.stbBPAmountWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblBPAmountWords = New System.Windows.Forms.Label()
        Me.stbBPTotalBill = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblBillForItem = New System.Windows.Forms.Label()
        Me.stbCompanyName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboCompanyNo = New System.Windows.Forms.ComboBox()
        Me.lblCompanyName = New System.Windows.Forms.Label()
        Me.lblCompanyNo = New System.Windows.Forms.Label()
        Me.colExtraBillNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraBillDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPatientNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colVisitNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colGender = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAge = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMemberCardNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colVisitType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colVisitTypeID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.stbOutPatientBill = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblOutPatientBill = New System.Windows.Forms.Label()
        Me.stbInPatientBill = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblInPatientBill = New System.Windows.Forms.Label()
        Me.tbcExtraBillsInvoice.SuspendLayout()
        Me.tpgExtraBills.SuspendLayout()
        CType(Me.dgvExtraBillsInvoice, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsDueInvoices.SuspendLayout()
        Me.tpgOPDInvoices.SuspendLayout()
        CType(Me.dgvOPDBillsInvoice, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpSetParameters.SuspendLayout()
        Me.pnlPeriod.SuspendLayout()
        Me.pnlBill.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbcExtraBillsInvoice
        '
        Me.tbcExtraBillsInvoice.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbcExtraBillsInvoice.Controls.Add(Me.tpgExtraBills)
        Me.tbcExtraBillsInvoice.Controls.Add(Me.tpgOPDInvoices)
        Me.tbcExtraBillsInvoice.HotTrack = True
        Me.tbcExtraBillsInvoice.Location = New System.Drawing.Point(8, 185)
        Me.tbcExtraBillsInvoice.Name = "tbcExtraBillsInvoice"
        Me.tbcExtraBillsInvoice.SelectedIndex = 0
        Me.tbcExtraBillsInvoice.Size = New System.Drawing.Size(888, 277)
        Me.tbcExtraBillsInvoice.TabIndex = 44
        '
        'tpgExtraBills
        '
        Me.tpgExtraBills.Controls.Add(Me.dgvExtraBillsInvoice)
        Me.tpgExtraBills.Location = New System.Drawing.Point(4, 22)
        Me.tpgExtraBills.Name = "tpgExtraBills"
        Me.tpgExtraBills.Size = New System.Drawing.Size(880, 251)
        Me.tpgExtraBills.TabIndex = 6
        Me.tpgExtraBills.Tag = ""
        Me.tpgExtraBills.Text = "Extra Bills"
        Me.tpgExtraBills.UseVisualStyleBackColor = True
        '
        'dgvExtraBillsInvoice
        '
        Me.dgvExtraBillsInvoice.AllowUserToAddRows = False
        Me.dgvExtraBillsInvoice.AllowUserToDeleteRows = False
        Me.dgvExtraBillsInvoice.AllowUserToOrderColumns = True
        Me.dgvExtraBillsInvoice.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvExtraBillsInvoice.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvExtraBillsInvoice.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle25.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle25.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvExtraBillsInvoice.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle25
        Me.dgvExtraBillsInvoice.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colExtraBillNo, Me.colExtraBillDate, Me.colPatientNo, Me.colVisitNo, Me.colFullName, Me.colGender, Me.colAge, Me.colMemberCardNo, Me.colAmount, Me.colVisitType, Me.colVisitTypeID})
        Me.dgvExtraBillsInvoice.ContextMenuStrip = Me.cmsDueInvoices
        DataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle36.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle36.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvExtraBillsInvoice.DefaultCellStyle = DataGridViewCellStyle36
        Me.dgvExtraBillsInvoice.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvExtraBillsInvoice.EnableHeadersVisualStyles = False
        Me.dgvExtraBillsInvoice.GridColor = System.Drawing.Color.Khaki
        Me.dgvExtraBillsInvoice.Location = New System.Drawing.Point(0, 0)
        Me.dgvExtraBillsInvoice.Name = "dgvExtraBillsInvoice"
        Me.dgvExtraBillsInvoice.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle37.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle37.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvExtraBillsInvoice.RowHeadersDefaultCellStyle = DataGridViewCellStyle37
        Me.dgvExtraBillsInvoice.RowHeadersVisible = False
        Me.dgvExtraBillsInvoice.Size = New System.Drawing.Size(880, 251)
        Me.dgvExtraBillsInvoice.TabIndex = 0
        Me.dgvExtraBillsInvoice.Text = "DataGridView1"
        '
        'cmsDueInvoices
        '
        Me.cmsDueInvoices.AccessibleDescription = ""
        Me.cmsDueInvoices.BackColor = System.Drawing.Color.GhostWhite
        Me.cmsDueInvoices.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmsDueInvoicesCopy, Me.cmsDueInvoicesSelectAll, Me.cmsDueInvoicesExtraBillNoDetails})
        Me.cmsDueInvoices.Name = "cmsSearch"
        Me.cmsDueInvoices.Size = New System.Drawing.Size(177, 70)
        '
        'cmsDueInvoicesCopy
        '
        Me.cmsDueInvoicesCopy.Enabled = False
        Me.cmsDueInvoicesCopy.Image = CType(resources.GetObject("cmsDueInvoicesCopy.Image"), System.Drawing.Image)
        Me.cmsDueInvoicesCopy.Name = "cmsDueInvoicesCopy"
        Me.cmsDueInvoicesCopy.Size = New System.Drawing.Size(176, 22)
        Me.cmsDueInvoicesCopy.Text = "Copy"
        Me.cmsDueInvoicesCopy.ToolTipText = "To copy with column headings, use Ctrl+C key combination"
        '
        'cmsDueInvoicesSelectAll
        '
        Me.cmsDueInvoicesSelectAll.Enabled = False
        Me.cmsDueInvoicesSelectAll.Name = "cmsDueInvoicesSelectAll"
        Me.cmsDueInvoicesSelectAll.Size = New System.Drawing.Size(176, 22)
        Me.cmsDueInvoicesSelectAll.Text = "Select All"
        '
        'cmsDueInvoicesExtraBillNoDetails
        '
        Me.cmsDueInvoicesExtraBillNoDetails.Enabled = False
        Me.cmsDueInvoicesExtraBillNoDetails.Name = "cmsDueInvoicesExtraBillNoDetails"
        Me.cmsDueInvoicesExtraBillNoDetails.Size = New System.Drawing.Size(176, 22)
        Me.cmsDueInvoicesExtraBillNoDetails.Text = "Extra Bill No Details"
        '
        'tpgOPDInvoices
        '
        Me.tpgOPDInvoices.Controls.Add(Me.dgvOPDBillsInvoice)
        Me.tpgOPDInvoices.Location = New System.Drawing.Point(4, 22)
        Me.tpgOPDInvoices.Name = "tpgOPDInvoices"
        Me.tpgOPDInvoices.Size = New System.Drawing.Size(880, 210)
        Me.tpgOPDInvoices.TabIndex = 0
        Me.tpgOPDInvoices.Tag = ""
        Me.tpgOPDInvoices.Text = "OPD Invoices"
        Me.tpgOPDInvoices.UseVisualStyleBackColor = True
        '
        'dgvOPDBillsInvoice
        '
        Me.dgvOPDBillsInvoice.AllowUserToAddRows = False
        Me.dgvOPDBillsInvoice.AllowUserToDeleteRows = False
        Me.dgvOPDBillsInvoice.AllowUserToOrderColumns = True
        Me.dgvOPDBillsInvoice.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvOPDBillsInvoice.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvOPDBillsInvoice.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOPDBillsInvoice.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvOPDBillsInvoice.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colInvoiceNo, Me.colInvoiceDate, Me.colOPDPatientNo, Me.colOPDVisitNo, Me.colOPDFullName, Me.colOPDGender, Me.colOPDAge, Me.colOPDMemberCardNo, Me.colOPDAmount})
        Me.dgvOPDBillsInvoice.ContextMenuStrip = Me.cmsDueInvoices
        Me.dgvOPDBillsInvoice.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOPDBillsInvoice.EnableHeadersVisualStyles = False
        Me.dgvOPDBillsInvoice.GridColor = System.Drawing.Color.Khaki
        Me.dgvOPDBillsInvoice.Location = New System.Drawing.Point(0, 0)
        Me.dgvOPDBillsInvoice.Name = "dgvOPDBillsInvoice"
        Me.dgvOPDBillsInvoice.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle45.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle45.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle45.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle45.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle45.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle45.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle45.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOPDBillsInvoice.RowHeadersDefaultCellStyle = DataGridViewCellStyle45
        Me.dgvOPDBillsInvoice.RowHeadersVisible = False
        Me.dgvOPDBillsInvoice.Size = New System.Drawing.Size(880, 210)
        Me.dgvOPDBillsInvoice.TabIndex = 45
        Me.dgvOPDBillsInvoice.Text = "DataGridView1"
        '
        'colInvoiceNo
        '
        Me.colInvoiceNo.DataPropertyName = "InvoiceNo"
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Info
        Me.colInvoiceNo.DefaultCellStyle = DataGridViewCellStyle12
        Me.colInvoiceNo.HeaderText = "Invoice No"
        Me.colInvoiceNo.Name = "colInvoiceNo"
        Me.colInvoiceNo.ReadOnly = True
        Me.colInvoiceNo.Width = 80
        '
        'colInvoiceDate
        '
        Me.colInvoiceDate.DataPropertyName = "InvoiceDate"
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle13.Format = "D"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.colInvoiceDate.DefaultCellStyle = DataGridViewCellStyle13
        Me.colInvoiceDate.HeaderText = "Invoice Date"
        Me.colInvoiceDate.Name = "colInvoiceDate"
        Me.colInvoiceDate.ReadOnly = True
        Me.colInvoiceDate.Width = 80
        '
        'colOPDPatientNo
        '
        Me.colOPDPatientNo.DataPropertyName = "PatientNo"
        DataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDPatientNo.DefaultCellStyle = DataGridViewCellStyle38
        Me.colOPDPatientNo.HeaderText = "Patient No"
        Me.colOPDPatientNo.Name = "colOPDPatientNo"
        Me.colOPDPatientNo.ReadOnly = True
        '
        'colOPDVisitNo
        '
        Me.colOPDVisitNo.DataPropertyName = "VisitNo"
        DataGridViewCellStyle39.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVisitNo.DefaultCellStyle = DataGridViewCellStyle39
        Me.colOPDVisitNo.HeaderText = "Visit No"
        Me.colOPDVisitNo.Name = "colOPDVisitNo"
        Me.colOPDVisitNo.ReadOnly = True
        '
        'colOPDFullName
        '
        Me.colOPDFullName.DataPropertyName = "FullName"
        DataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDFullName.DefaultCellStyle = DataGridViewCellStyle40
        Me.colOPDFullName.HeaderText = "Full Name"
        Me.colOPDFullName.Name = "colOPDFullName"
        Me.colOPDFullName.ReadOnly = True
        Me.colOPDFullName.Width = 200
        '
        'colOPDGender
        '
        Me.colOPDGender.DataPropertyName = "Gender"
        DataGridViewCellStyle41.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDGender.DefaultCellStyle = DataGridViewCellStyle41
        Me.colOPDGender.HeaderText = "Gender"
        Me.colOPDGender.Name = "colOPDGender"
        '
        'colOPDAge
        '
        Me.colOPDAge.DataPropertyName = "Age"
        DataGridViewCellStyle42.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDAge.DefaultCellStyle = DataGridViewCellStyle42
        Me.colOPDAge.HeaderText = "Age"
        Me.colOPDAge.Name = "colOPDAge"
        Me.colOPDAge.ReadOnly = True
        '
        'colOPDMemberCardNo
        '
        Me.colOPDMemberCardNo.DataPropertyName = "MemberCardNo"
        DataGridViewCellStyle43.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDMemberCardNo.DefaultCellStyle = DataGridViewCellStyle43
        Me.colOPDMemberCardNo.HeaderText = "Member Card No"
        Me.colOPDMemberCardNo.Name = "colOPDMemberCardNo"
        Me.colOPDMemberCardNo.ReadOnly = True
        '
        'colOPDAmount
        '
        Me.colOPDAmount.DataPropertyName = "Amount"
        DataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle44.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle44.Format = "N2"
        DataGridViewCellStyle44.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle44.NullValue = Nothing
        Me.colOPDAmount.DefaultCellStyle = DataGridViewCellStyle44
        Me.colOPDAmount.HeaderText = "Amount"
        Me.colOPDAmount.Name = "colOPDAmount"
        Me.colOPDAmount.ReadOnly = True
        Me.colOPDAmount.Width = 80
        '
        'btnPrintPreview
        '
        Me.btnPrintPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintPreview.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrintPreview.Location = New System.Drawing.Point(84, 468)
        Me.btnPrintPreview.Name = "btnPrintPreview"
        Me.btnPrintPreview.Size = New System.Drawing.Size(83, 24)
        Me.btnPrintPreview.TabIndex = 50
        Me.btnPrintPreview.Text = "Print Pre&view"
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Location = New System.Drawing.Point(7, 468)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(73, 24)
        Me.btnPrint.TabIndex = 49
        Me.btnPrint.Text = "&Print"
        '
        'fbnClose
        '
        Me.fbnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(838, 468)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(50, 24)
        Me.fbnClose.TabIndex = 51
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'grpSetParameters
        '
        Me.grpSetParameters.Controls.Add(Me.pnlPeriod)
        Me.grpSetParameters.Controls.Add(Me.btnLoadPendingBillsPayment)
        Me.grpSetParameters.Controls.Add(Me.lblRecordsNo)
        Me.grpSetParameters.Location = New System.Drawing.Point(431, 2)
        Me.grpSetParameters.Name = "grpSetParameters"
        Me.grpSetParameters.Size = New System.Drawing.Size(465, 80)
        Me.grpSetParameters.TabIndex = 52
        Me.grpSetParameters.TabStop = False
        Me.grpSetParameters.Text = "Visit Period"
        '
        'pnlPeriod
        '
        Me.pnlPeriod.Controls.Add(Me.dtpEndDate)
        Me.pnlPeriod.Controls.Add(Me.lblStartDate)
        Me.pnlPeriod.Controls.Add(Me.dtpStartDate)
        Me.pnlPeriod.Controls.Add(Me.lblEndDate)
        Me.pnlPeriod.Location = New System.Drawing.Point(6, 16)
        Me.pnlPeriod.Name = "pnlPeriod"
        Me.pnlPeriod.Size = New System.Drawing.Size(283, 53)
        Me.pnlPeriod.TabIndex = 0
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Location = New System.Drawing.Point(71, 29)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(203, 20)
        Me.dtpEndDate.TabIndex = 3
        '
        'lblStartDate
        '
        Me.lblStartDate.Location = New System.Drawing.Point(10, 3)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(57, 20)
        Me.lblStartDate.TabIndex = 0
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Location = New System.Drawing.Point(71, 6)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(201, 20)
        Me.dtpStartDate.TabIndex = 1
        '
        'lblEndDate
        '
        Me.lblEndDate.Location = New System.Drawing.Point(10, 26)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(57, 20)
        Me.lblEndDate.TabIndex = 2
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnLoadPendingBillsPayment
        '
        Me.btnLoadPendingBillsPayment.AccessibleDescription = ""
        Me.btnLoadPendingBillsPayment.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnLoadPendingBillsPayment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoadPendingBillsPayment.Location = New System.Drawing.Point(303, 45)
        Me.btnLoadPendingBillsPayment.Name = "btnLoadPendingBillsPayment"
        Me.btnLoadPendingBillsPayment.Size = New System.Drawing.Size(78, 24)
        Me.btnLoadPendingBillsPayment.TabIndex = 2
        Me.btnLoadPendingBillsPayment.Tag = ""
        Me.btnLoadPendingBillsPayment.Text = "Load &Bill"
        '
        'lblRecordsNo
        '
        Me.lblRecordsNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblRecordsNo.ForeColor = System.Drawing.Color.Blue
        Me.lblRecordsNo.Location = New System.Drawing.Point(300, 16)
        Me.lblRecordsNo.Name = "lblRecordsNo"
        Me.lblRecordsNo.Size = New System.Drawing.Size(159, 22)
        Me.lblRecordsNo.TabIndex = 1
        Me.lblRecordsNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnLoadPatients
        '
        Me.btnLoadPatients.Enabled = False
        Me.btnLoadPatients.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnLoadPatients.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoadPatients.Location = New System.Drawing.Point(381, 25)
        Me.btnLoadPatients.Name = "btnLoadPatients"
        Me.btnLoadPatients.Size = New System.Drawing.Size(44, 24)
        Me.btnLoadPatients.TabIndex = 58
        Me.btnLoadPatients.Tag = ""
        Me.btnLoadPatients.Text = "&Load"
        '
        'btnFindPatientNo
        '
        Me.btnFindPatientNo.Enabled = False
        Me.btnFindPatientNo.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnFindPatientNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFindPatientNo.Image = CType(resources.GetObject("btnFindPatientNo.Image"), System.Drawing.Image)
        Me.btnFindPatientNo.Location = New System.Drawing.Point(159, 28)
        Me.btnFindPatientNo.Name = "btnFindPatientNo"
        Me.btnFindPatientNo.Size = New System.Drawing.Size(27, 21)
        Me.btnFindPatientNo.TabIndex = 56
        '
        'cboBillModesID
        '
        Me.cboBillModesID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboBillModesID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBillModesID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBillModesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboBillModesID.FormattingEnabled = True
        Me.cboBillModesID.ItemHeight = 13
        Me.cboBillModesID.Location = New System.Drawing.Point(192, 5)
        Me.cboBillModesID.Name = "cboBillModesID"
        Me.cboBillModesID.Size = New System.Drawing.Size(185, 21)
        Me.cboBillModesID.TabIndex = 54
        '
        'lblBillModesID
        '
        Me.lblBillModesID.Location = New System.Drawing.Point(12, 8)
        Me.lblBillModesID.Name = "lblBillModesID"
        Me.lblBillModesID.Size = New System.Drawing.Size(143, 18)
        Me.lblBillModesID.TabIndex = 53
        Me.lblBillModesID.Text = "Bill Mode"
        '
        'stbBillCustomerName
        '
        Me.stbBillCustomerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBillCustomerName.CapitalizeFirstLetter = False
        Me.stbBillCustomerName.EntryErrorMSG = ""
        Me.stbBillCustomerName.Location = New System.Drawing.Point(192, 54)
        Me.stbBillCustomerName.MaxLength = 41
        Me.stbBillCustomerName.Multiline = True
        Me.stbBillCustomerName.Name = "stbBillCustomerName"
        Me.stbBillCustomerName.ReadOnly = True
        Me.stbBillCustomerName.RegularExpression = ""
        Me.stbBillCustomerName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbBillCustomerName.Size = New System.Drawing.Size(185, 49)
        Me.stbBillCustomerName.TabIndex = 60
        '
        'cboBillAccountNo
        '
        Me.cboBillAccountNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboBillAccountNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBillAccountNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboBillAccountNo.DropDownWidth = 276
        Me.cboBillAccountNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboBillAccountNo.FormattingEnabled = True
        Me.cboBillAccountNo.ItemHeight = 13
        Me.cboBillAccountNo.Location = New System.Drawing.Point(192, 29)
        Me.cboBillAccountNo.Name = "cboBillAccountNo"
        Me.cboBillAccountNo.Size = New System.Drawing.Size(185, 21)
        Me.cboBillAccountNo.TabIndex = 57
        '
        'lblBillCustomerName
        '
        Me.lblBillCustomerName.Location = New System.Drawing.Point(12, 57)
        Me.lblBillCustomerName.Name = "lblBillCustomerName"
        Me.lblBillCustomerName.Size = New System.Drawing.Size(143, 18)
        Me.lblBillCustomerName.TabIndex = 59
        Me.lblBillCustomerName.Text = "To-Bill Customer Name"
        '
        'lblBillAccountNo
        '
        Me.lblBillAccountNo.Location = New System.Drawing.Point(12, 32)
        Me.lblBillAccountNo.Name = "lblBillAccountNo"
        Me.lblBillAccountNo.Size = New System.Drawing.Size(133, 18)
        Me.lblBillAccountNo.TabIndex = 55
        Me.lblBillAccountNo.Text = "To-Bill Account Number"
        '
        'pnlBill
        '
        Me.pnlBill.Controls.Add(Me.stbInPatientBill)
        Me.pnlBill.Controls.Add(Me.lblInPatientBill)
        Me.pnlBill.Controls.Add(Me.stbOutPatientBill)
        Me.pnlBill.Controls.Add(Me.lblOutPatientBill)
        Me.pnlBill.Controls.Add(Me.stbBPAmountWords)
        Me.pnlBill.Controls.Add(Me.lblBPAmountWords)
        Me.pnlBill.Controls.Add(Me.stbBPTotalBill)
        Me.pnlBill.Controls.Add(Me.lblBillForItem)
        Me.pnlBill.Location = New System.Drawing.Point(383, 84)
        Me.pnlBill.Name = "pnlBill"
        Me.pnlBill.Size = New System.Drawing.Size(513, 95)
        Me.pnlBill.TabIndex = 63
        '
        'stbBPAmountWords
        '
        Me.stbBPAmountWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbBPAmountWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBPAmountWords.CapitalizeFirstLetter = False
        Me.stbBPAmountWords.EntryErrorMSG = ""
        Me.stbBPAmountWords.Location = New System.Drawing.Point(126, 50)
        Me.stbBPAmountWords.MaxLength = 100
        Me.stbBPAmountWords.Multiline = True
        Me.stbBPAmountWords.Name = "stbBPAmountWords"
        Me.stbBPAmountWords.ReadOnly = True
        Me.stbBPAmountWords.RegularExpression = ""
        Me.stbBPAmountWords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbBPAmountWords.Size = New System.Drawing.Size(379, 43)
        Me.stbBPAmountWords.TabIndex = 16
        '
        'lblBPAmountWords
        '
        Me.lblBPAmountWords.Location = New System.Drawing.Point(10, 59)
        Me.lblBPAmountWords.Name = "lblBPAmountWords"
        Me.lblBPAmountWords.Size = New System.Drawing.Size(111, 21)
        Me.lblBPAmountWords.TabIndex = 15
        Me.lblBPAmountWords.Text = "Amount in Words"
        '
        'stbBPTotalBill
        '
        Me.stbBPTotalBill.BackColor = System.Drawing.SystemColors.Info
        Me.stbBPTotalBill.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBPTotalBill.CapitalizeFirstLetter = False
        Me.stbBPTotalBill.Enabled = False
        Me.stbBPTotalBill.EntryErrorMSG = ""
        Me.stbBPTotalBill.Location = New System.Drawing.Point(126, 27)
        Me.stbBPTotalBill.MaxLength = 20
        Me.stbBPTotalBill.Name = "stbBPTotalBill"
        Me.stbBPTotalBill.RegularExpression = ""
        Me.stbBPTotalBill.Size = New System.Drawing.Size(211, 20)
        Me.stbBPTotalBill.TabIndex = 13
        Me.stbBPTotalBill.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBillForItem
        '
        Me.lblBillForItem.Location = New System.Drawing.Point(14, 29)
        Me.lblBillForItem.Name = "lblBillForItem"
        Me.lblBillForItem.Size = New System.Drawing.Size(111, 18)
        Me.lblBillForItem.TabIndex = 12
        Me.lblBillForItem.Text = "Total Bill For"
        '
        'stbCompanyName
        '
        Me.stbCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbCompanyName.CapitalizeFirstLetter = True
        Me.stbCompanyName.Enabled = False
        Me.stbCompanyName.EntryErrorMSG = ""
        Me.stbCompanyName.Location = New System.Drawing.Point(192, 130)
        Me.stbCompanyName.MaxLength = 60
        Me.stbCompanyName.Multiline = True
        Me.stbCompanyName.Name = "stbCompanyName"
        Me.stbCompanyName.ReadOnly = True
        Me.stbCompanyName.RegularExpression = ""
        Me.stbCompanyName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbCompanyName.Size = New System.Drawing.Size(185, 34)
        Me.stbCompanyName.TabIndex = 67
        '
        'cboCompanyNo
        '
        Me.cboCompanyNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboCompanyNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboCompanyNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboCompanyNo.DropDownWidth = 256
        Me.cboCompanyNo.Enabled = False
        Me.cboCompanyNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboCompanyNo.FormattingEnabled = True
        Me.cboCompanyNo.ItemHeight = 13
        Me.cboCompanyNo.Location = New System.Drawing.Point(192, 106)
        Me.cboCompanyNo.Name = "cboCompanyNo"
        Me.cboCompanyNo.Size = New System.Drawing.Size(185, 21)
        Me.cboCompanyNo.TabIndex = 65
        '
        'lblCompanyName
        '
        Me.lblCompanyName.Enabled = False
        Me.lblCompanyName.Location = New System.Drawing.Point(12, 130)
        Me.lblCompanyName.Name = "lblCompanyName"
        Me.lblCompanyName.Size = New System.Drawing.Size(143, 18)
        Me.lblCompanyName.TabIndex = 66
        Me.lblCompanyName.Text = "To-Bill Company Name"
        '
        'lblCompanyNo
        '
        Me.lblCompanyNo.Enabled = False
        Me.lblCompanyNo.Location = New System.Drawing.Point(12, 106)
        Me.lblCompanyNo.Name = "lblCompanyNo"
        Me.lblCompanyNo.Size = New System.Drawing.Size(143, 18)
        Me.lblCompanyNo.TabIndex = 64
        Me.lblCompanyNo.Text = "To-Bill Company Number"
        '
        'colExtraBillNo
        '
        Me.colExtraBillNo.DataPropertyName = "ExtraBillNo"
        DataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Info
        Me.colExtraBillNo.DefaultCellStyle = DataGridViewCellStyle26
        Me.colExtraBillNo.HeaderText = "Extra Bill No"
        Me.colExtraBillNo.Name = "colExtraBillNo"
        Me.colExtraBillNo.ReadOnly = True
        Me.colExtraBillNo.Width = 80
        '
        'colExtraBillDate
        '
        Me.colExtraBillDate.DataPropertyName = "ExtraBillDate"
        DataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle27.Format = "D"
        DataGridViewCellStyle27.NullValue = Nothing
        Me.colExtraBillDate.DefaultCellStyle = DataGridViewCellStyle27
        Me.colExtraBillDate.HeaderText = "Bill Date"
        Me.colExtraBillDate.Name = "colExtraBillDate"
        Me.colExtraBillDate.ReadOnly = True
        Me.colExtraBillDate.Width = 80
        '
        'colPatientNo
        '
        Me.colPatientNo.DataPropertyName = "PatientNo"
        DataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Info
        Me.colPatientNo.DefaultCellStyle = DataGridViewCellStyle28
        Me.colPatientNo.HeaderText = "Patient No"
        Me.colPatientNo.Name = "colPatientNo"
        Me.colPatientNo.ReadOnly = True
        '
        'colVisitNo
        '
        Me.colVisitNo.DataPropertyName = "VisitNo"
        DataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Info
        Me.colVisitNo.DefaultCellStyle = DataGridViewCellStyle29
        Me.colVisitNo.HeaderText = "Visit No"
        Me.colVisitNo.Name = "colVisitNo"
        Me.colVisitNo.ReadOnly = True
        '
        'colFullName
        '
        Me.colFullName.DataPropertyName = "FullName"
        DataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Info
        Me.colFullName.DefaultCellStyle = DataGridViewCellStyle30
        Me.colFullName.HeaderText = "Full Name"
        Me.colFullName.Name = "colFullName"
        Me.colFullName.ReadOnly = True
        Me.colFullName.Width = 200
        '
        'colGender
        '
        Me.colGender.DataPropertyName = "Gender"
        DataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Info
        Me.colGender.DefaultCellStyle = DataGridViewCellStyle31
        Me.colGender.HeaderText = "Gender"
        Me.colGender.Name = "colGender"
        Me.colGender.ReadOnly = True
        '
        'colAge
        '
        Me.colAge.DataPropertyName = "Age"
        DataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Info
        Me.colAge.DefaultCellStyle = DataGridViewCellStyle32
        Me.colAge.HeaderText = "Age"
        Me.colAge.Name = "colAge"
        Me.colAge.ReadOnly = True
        '
        'colMemberCardNo
        '
        Me.colMemberCardNo.DataPropertyName = "MemberCardNo"
        DataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Info
        Me.colMemberCardNo.DefaultCellStyle = DataGridViewCellStyle33
        Me.colMemberCardNo.HeaderText = "Member Card No"
        Me.colMemberCardNo.Name = "colMemberCardNo"
        Me.colMemberCardNo.ReadOnly = True
        '
        'colAmount
        '
        Me.colAmount.DataPropertyName = "Amount"
        DataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle34.Format = "N2"
        DataGridViewCellStyle34.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle34.NullValue = Nothing
        Me.colAmount.DefaultCellStyle = DataGridViewCellStyle34
        Me.colAmount.HeaderText = "Amount"
        Me.colAmount.Name = "colAmount"
        Me.colAmount.ReadOnly = True
        Me.colAmount.Width = 80
        '
        'colVisitType
        '
        Me.colVisitType.DataPropertyName = "VisitType"
        DataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Info
        Me.colVisitType.DefaultCellStyle = DataGridViewCellStyle35
        Me.colVisitType.HeaderText = "Visit Type"
        Me.colVisitType.Name = "colVisitType"
        Me.colVisitType.ReadOnly = True
        '
        'colVisitTypeID
        '
        Me.colVisitTypeID.DataPropertyName = "VisitTypeID"
        Me.colVisitTypeID.HeaderText = "Visit Type ID"
        Me.colVisitTypeID.Name = "colVisitTypeID"
        Me.colVisitTypeID.ReadOnly = True
        Me.colVisitTypeID.Visible = False
        '
        'stbOutPatientBill
        '
        Me.stbOutPatientBill.BackColor = System.Drawing.SystemColors.Info
        Me.stbOutPatientBill.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbOutPatientBill.CapitalizeFirstLetter = False
        Me.stbOutPatientBill.Enabled = False
        Me.stbOutPatientBill.EntryErrorMSG = ""
        Me.stbOutPatientBill.Location = New System.Drawing.Point(126, 4)
        Me.stbOutPatientBill.MaxLength = 20
        Me.stbOutPatientBill.Name = "stbOutPatientBill"
        Me.stbOutPatientBill.RegularExpression = ""
        Me.stbOutPatientBill.Size = New System.Drawing.Size(127, 20)
        Me.stbOutPatientBill.TabIndex = 18
        Me.stbOutPatientBill.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOutPatientBill
        '
        Me.lblOutPatientBill.Location = New System.Drawing.Point(14, 6)
        Me.lblOutPatientBill.Name = "lblOutPatientBill"
        Me.lblOutPatientBill.Size = New System.Drawing.Size(111, 18)
        Me.lblOutPatientBill.TabIndex = 17
        Me.lblOutPatientBill.Text = "Out Patient Bill"
        '
        'stbInPatientBill
        '
        Me.stbInPatientBill.BackColor = System.Drawing.SystemColors.Info
        Me.stbInPatientBill.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbInPatientBill.CapitalizeFirstLetter = False
        Me.stbInPatientBill.Enabled = False
        Me.stbInPatientBill.EntryErrorMSG = ""
        Me.stbInPatientBill.Location = New System.Drawing.Point(366, 3)
        Me.stbInPatientBill.MaxLength = 20
        Me.stbInPatientBill.Name = "stbInPatientBill"
        Me.stbInPatientBill.RegularExpression = ""
        Me.stbInPatientBill.Size = New System.Drawing.Size(127, 20)
        Me.stbInPatientBill.TabIndex = 20
        Me.stbInPatientBill.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblInPatientBill
        '
        Me.lblInPatientBill.Location = New System.Drawing.Point(259, 6)
        Me.lblInPatientBill.Name = "lblInPatientBill"
        Me.lblInPatientBill.Size = New System.Drawing.Size(101, 18)
        Me.lblInPatientBill.TabIndex = 19
        Me.lblInPatientBill.Text = "In Patient Bill"
        '
        'frmDueInvoices
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(905, 499)
        Me.Controls.Add(Me.stbCompanyName)
        Me.Controls.Add(Me.cboCompanyNo)
        Me.Controls.Add(Me.lblCompanyName)
        Me.Controls.Add(Me.lblCompanyNo)
        Me.Controls.Add(Me.pnlBill)
        Me.Controls.Add(Me.btnLoadPatients)
        Me.Controls.Add(Me.btnFindPatientNo)
        Me.Controls.Add(Me.cboBillModesID)
        Me.Controls.Add(Me.lblBillModesID)
        Me.Controls.Add(Me.stbBillCustomerName)
        Me.Controls.Add(Me.cboBillAccountNo)
        Me.Controls.Add(Me.lblBillCustomerName)
        Me.Controls.Add(Me.lblBillAccountNo)
        Me.Controls.Add(Me.grpSetParameters)
        Me.Controls.Add(Me.btnPrintPreview)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.tbcExtraBillsInvoice)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmDueInvoices"
        Me.Text = "Due Invoices"
        Me.tbcExtraBillsInvoice.ResumeLayout(False)
        Me.tpgExtraBills.ResumeLayout(False)
        CType(Me.dgvExtraBillsInvoice, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsDueInvoices.ResumeLayout(False)
        Me.tpgOPDInvoices.ResumeLayout(False)
        CType(Me.dgvOPDBillsInvoice, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpSetParameters.ResumeLayout(False)
        Me.pnlPeriod.ResumeLayout(False)
        Me.pnlBill.ResumeLayout(False)
        Me.pnlBill.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbcExtraBillsInvoice As System.Windows.Forms.TabControl
    Friend WithEvents tpgExtraBills As System.Windows.Forms.TabPage
    Friend WithEvents dgvExtraBillsInvoice As System.Windows.Forms.DataGridView
    Friend WithEvents tpgOPDInvoices As System.Windows.Forms.TabPage
    Friend WithEvents btnPrintPreview As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents grpSetParameters As System.Windows.Forms.GroupBox
    Friend WithEvents pnlPeriod As System.Windows.Forms.Panel
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents btnLoadPendingBillsPayment As System.Windows.Forms.Button
    Friend WithEvents lblRecordsNo As System.Windows.Forms.Label
    Friend WithEvents btnLoadPatients As System.Windows.Forms.Button
    Friend WithEvents btnFindPatientNo As System.Windows.Forms.Button
    Friend WithEvents cboBillModesID As System.Windows.Forms.ComboBox
    Friend WithEvents lblBillModesID As System.Windows.Forms.Label
    Friend WithEvents stbBillCustomerName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents cboBillAccountNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblBillCustomerName As System.Windows.Forms.Label
    Friend WithEvents lblBillAccountNo As System.Windows.Forms.Label
    Friend WithEvents pnlBill As System.Windows.Forms.Panel
    Friend WithEvents stbBPAmountWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblBPAmountWords As System.Windows.Forms.Label
    Friend WithEvents stbBPTotalBill As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblBillForItem As System.Windows.Forms.Label
    Friend WithEvents dgvOPDBillsInvoice As System.Windows.Forms.DataGridView
    Friend WithEvents cmsDueInvoices As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmsDueInvoicesCopy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmsDueInvoicesSelectAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmsDueInvoicesExtraBillNoDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents colInvoiceNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInvoiceDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDPatientNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVisitNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDAge As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDMemberCardNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents stbCompanyName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents cboCompanyNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompanyName As System.Windows.Forms.Label
    Friend WithEvents lblCompanyNo As System.Windows.Forms.Label
    Friend WithEvents colExtraBillNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraBillDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPatientNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colVisitNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAge As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colMemberCardNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colVisitType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colVisitTypeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents stbInPatientBill As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblInPatientBill As System.Windows.Forms.Label
    Friend WithEvents stbOutPatientBill As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblOutPatientBill As System.Windows.Forms.Label
End Class
