
Option Strict On

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls

Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects
Imports SyncSoft.SQLDb
Imports SyncSoft.Common.SQL.Classes

Public Class frmCompanyMappings

#Region " Fields "
    Private oINTCompanyMappings As New SyncSoft.SQLDb.INTCompanyMappings()
#End Region

    Private Sub frmINTCompanyMappings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.Cursor = Cursors.WaitCursor()

            LoadAllAgents()
            LoadCompanies()
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub


    Private Sub LoadAllAgents()

        Dim oINTAgents As New SyncSoft.SQLDb.INTAgents()


        Try
            Me.Cursor = Cursors.WaitCursor

            ' Load from Agents

            Dim intAgents As DataTable = oINTAgents.GetINTAgents().Tables("INTAgents")

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With cboAgentNo
                .DataSource = intAgents
                .DisplayMember = "AgentName"
                .ValueMember = "AgentNo"
                .SelectedIndex = 0
            End With
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub LoadCompanies()
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim agentNo As String = StringValueMayBeEnteredIn(cboAgentNo)
            Dim oCompany As New SyncSoft.SQLDb.Companies()
            Dim oCompanyMappings As New SyncSoft.SQLDb.CompanyMappings()
            Me.dgvCompanyMappings.Rows.Clear()

            Me.Cursor = Cursors.WaitCursor

            If String.IsNullOrEmpty(agentNo) Then Return

            ' Load Companies
            Dim companies As DataTable = oCompany.GetCompanies().Tables("Companies")
            Dim companyNo As String = String.Empty
            Dim agentCompanyNo As String = String.Empty
            Dim agentCompanyName As String = String.Empty

            Dim rowCount As Integer = companies.Rows.Count()

            For row As Integer = 0 To rowCount - 1
               Me.dgvCompanyMappings.Rows.Add()
                Dim rowIndex As DataRow = companies.Rows(row)
                companyNo = StringEnteredIn(rowIndex, "CompanyNo")
                Dim companyMappings As DataTable = oCompanyMappings.GetCompanyMappings(agentNo, companyNo).Tables("CompanyMappings")
                If companyMappings.Rows.Count > 0 Then
                    Dim mrow As DataRow = companyMappings.Rows(0)
                    agentCompanyNo = StringMayBeEnteredIn(mrow, "AgentCompanyNo")
                    agentCompanyName = StringMayBeEnteredIn(mrow, "AgentCompanyName")
                    Me.dgvCompanyMappings.Item(Me.colSaved.Name, row).Value = True
                End If

                Me.dgvCompanyMappings.Item(Me.colInclude.Name, row).Value = True
                Me.dgvCompanyMappings.Item(Me.colCompanyNo.Name, row).Value = companyNo
                Me.dgvCompanyMappings.Item(Me.colCompanyName.Name, row).Value = StringEnteredIn(rowIndex, "CompanyName")
                Me.dgvCompanyMappings.Item(Me.colAgentCompanyNo.Name, row).Value = agentCompanyNo
                Me.dgvCompanyMappings.Item(Me.colAgentCompanyName.Name, row).Value = agentCompanyName
            Next

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub


    Private Function LoadINTCompanies() As DataTable




        Try
            Me.Cursor = Cursors.WaitCursor

            ' Load from Agents
            Dim agentNo As String = StringValueEnteredIn(cboAgentNo, "Agent No")
            Return oINTCompanyMappings.GetINTCompanyMappings(agentNo, String.Empty).Tables("INTCompanyMappings")
        Catch ex As Exception
            Throw ex
        Finally
            Me.Cursor = Cursors.Default
        End Try


    End Function


    Private Sub frmINTCompanyMappings_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub

    Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub


    Private Sub ebnSaveUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ebnSaveUpdate.Click

        Try
            Me.Cursor = Cursors.WaitCursor()

            Dim lCompanyMappings As New List(Of DBConnect)
            Dim transactions As New List(Of TransactionList(Of DBConnect))
            Dim oLookupData As New SyncSoft.Lookup.SQL.LookupData()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim nonSelected As Boolean = False
            For Each row As DataGridViewRow In Me.dgvCompanyMappings.Rows
                If row.IsNewRow Then Exit For
                If CBool(Me.dgvCompanyMappings.Item(Me.colInclude.Name, row.Index).Value) = True Then
                    nonSelected = False
                    Exit For
                End If
                nonSelected = True
            Next

            If Me.dgvCompanyMappings.RowCount < 1 OrElse nonSelected Then Throw New ArgumentException("Must include at least one Company!")
            Dim agentNo As String = StringValueEnteredIn(Me.cboAgentNo, "Agent No!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            For rowNo As Integer = 0 To Me.dgvCompanyMappings.Rows.Count - 1


                Try


                    Dim cells As DataGridViewCellCollection = Me.dgvCompanyMappings.Rows(rowNo).Cells

                    Dim include As Boolean = CBool(BooleanMayBeEnteredIn(cells, colInclude))

                    If include Then
                        Using oCompanyMappings As New SyncSoft.SQLDb.CompanyMappings()
                            Dim companyNo As String = StringEnteredIn(cells, Me.colCompanyNo, "CompanyNo!")
                            Dim agentCompanyNo As String = StringEnteredIn(cells, Me.colAgentCompanyNo, "Agent Company No!")


                            With oCompanyMappings
                                .AgentNo = agentNo
                                .CompanyNo = companyNo
                                .AgentCompanyNo = agentCompanyNo
                                .AgentNo = agentNo
                                .LoginID = CurrentUser.LoginID
                                .UserName = CurrentUser.FullName

                                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                ValidateEntriesIn(Me)
                                .Save()
                                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                            End With
                        End Using
                        Me.dgvCompanyMappings.Item(Me.colSaved.Name, rowNo).Value = True
                        DisplayMessage("Operation Successful")
                    End If

                Catch ex As Exception
                    ErrorMessage(ex)
                End Try

            Next

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            



        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try
    End Sub

#Region " Edit Methods "

    Public Sub Edit()

        Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update
        Me.ebnSaveUpdate.Enabled = False
        Me.fbnDelete.Visible = True
        Me.fbnDelete.Enabled = False
        Me.fbnSearch.Visible = True

        ResetControlsIn(Me)

    End Sub

    Public Sub Save()

        Me.ebnSaveUpdate.ButtonText = ButtonCaption.Save
        Me.ebnSaveUpdate.Enabled = True
        Me.fbnDelete.Visible = False
        Me.fbnDelete.Enabled = True
        Me.fbnSearch.Visible = False

        ResetControlsIn(Me)

    End Sub

    Private Sub DisplayData(ByVal dataSource As DataTable)

        Try

            Me.ebnSaveUpdate.DataSource = dataSource
            Me.ebnSaveUpdate.LoadData(Me)

            Me.ebnSaveUpdate.Enabled = dataSource.Rows.Count > 0
            Me.fbnDelete.Enabled = dataSource.Rows.Count > 0

            Security.Apply(Me.ebnSaveUpdate, AccessRights.Update)
            Security.Apply(Me.fbnDelete, AccessRights.Delete)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub CallOnKeyEdit()
        If Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update Then
            Me.ebnSaveUpdate.Enabled = False
            Me.fbnDelete.Enabled = False
        End If
    End Sub

#End Region

    Private Sub cboAgentNo_Leave(sender As Object, e As System.EventArgs) Handles cboAgentNo.Leave
        LoadCompanies()
    End Sub




    Private Sub dgvCompanyMappings_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCompanyMappings.CellClick
        Dim row As Integer = e.RowIndex
        If row < 0 Then Return
        Try

            If e.ColumnIndex.Equals(Me.colAgentCompanyNo.Index) Then
               
                Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("INT Companies", "Agent Company No", "Agent Company Name", Me.LoadINTCompanies(), "AgentCompanyFullName",
                                                                 "AgentCompanyNo", "AgentCompanyName", Me.dgvCompanyMappings, Me.colAgentCompanyNo, e.RowIndex)
                fSelectItem.ShowDialog()
                Try
                    Dim agentNo As String = StringEnteredIn(cboAgentNo, "Agent No")
                    Dim agentCompanyNo As String = StringMayBeEnteredIn(Me.dgvCompanyMappings.Rows(e.RowIndex).Cells, Me.colAgentCompanyNo)
                    Dim intCompanies As DataTable = oINTCompanyMappings.GetINTCompanyMappings(agentNo, agentCompanyNo).Tables("INTCompanyMappings")
                    If intCompanies.Rows.Count > 0 Then
                        Me.dgvCompanyMappings.Item(Me.colAgentCompanyName.Name, row).Value = StringEnteredIn(intCompanies.Rows(0), "AgentCompanyName")
                    Else
                        Me.dgvCompanyMappings.Item(Me.colAgentCompanyNo.Name, row).Value = String.Empty
                        Me.dgvCompanyMappings.Item(Me.colAgentCompanyName.Name, row).Value = String.Empty
                    End If
                Catch ex As Exception
                    ErrorMessage(ex)
                End Try
            End If
        Catch ex As Exception
            ErrorMessage(ex)
        End Try
    End Sub


    Private Sub dgvCompanyMappings_UserDeletingRow(sender As Object, e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvCompanyMappings.UserDeletingRow
        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oCompanyMappings As New SyncSoft.SQLDb.CompanyMappings()
            Dim toDeleteRowNo As Integer = e.Row.Index

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If CBool(Me.dgvCompanyMappings.Item(Me.colSaved.Name, toDeleteRowNo).Value).Equals(False) Then Return


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim agentNo As String = StringValueEnteredIn(Me.cboAgentNo, "Agent No!")
            Dim companyNo As String = CStr(Me.dgvCompanyMappings.Item(Me.colCompanyNo.Name, toDeleteRowNo).Value)
           

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oCompanyMappings
                .AgentNo = agentNo
                .CompanyNo = companyNo
                DisplayMessage(.Delete())
            End With

           
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub
End Class