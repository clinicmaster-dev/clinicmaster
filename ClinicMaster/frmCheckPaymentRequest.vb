﻿Option Strict On
Imports System.IO
Imports System.Xml
Imports System.Text
Imports System.Net
Imports SyncSoft.SQLDb
Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls
Imports SyncSoft.Common.Structures
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Classes
Imports SyncSoft.Common.SQL.Enumerations
Imports LookupDataID = SyncSoft.SQLDb.Lookup.LookupDataID
Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects
Imports LookupCommDataID = SyncSoft.Common.Lookup.LookupCommDataID
Imports LookupData = SyncSoft.Lookup.SQL.LookupData
Imports System.Drawing.Printing


Public Class frmCheckPaymentRequest
#Region " Fields "
    Private count As Integer
    Private defaultReferenceNo As String = String.Empty
    Private oIntegrationAgent As New LookupDataID.IntegrationAgents()
    Private currentCashVisitNo As String = String.Empty
    Private receiptNo As String = String.Empty
    Private BillCustomerName As String = String.Empty
    Private currentCashAllSaved As Boolean = True
    Private invoiceNoGenerated As String
    Private finalResponse As String
    Private canceled As Boolean
    Private PaymentReceived As Boolean
    Private items As DataTable
    Private oPayTypeID As New LookupDataID.PayTypeID()
    Private oVisitTypeID As New LookupDataID.VisitTypeID()
    Private oCopayTypeID As New LookupDataID.CoPayTypeID()
    Private payDate As Date = Now.Date()
    Private Const UpdateText As String = "&Update"
    Private Const EditText As String = "&Edit"
    Private CashThermalReceiptParagraphs As Collection
    Private WithEvents docCashThermalReceipt As New PrintDocument()
    Private pageNo As Integer
    Private printFontName As String = "Courier New"
    Private bodyBoldFont As New Font(printFontName, 10, FontStyle.Bold)
    Private bodyNormalFont As New Font(printFontName, 10)
    Private formInSaveMode As Boolean = True
#End Region


    Private Sub frmCheckPaymentRequest_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try

            If Not String.IsNullOrEmpty(defaultReferenceNo) Then
                Me.stbVisitNo.ReadOnly = True
                Me.stbVisitNo.Text = defaultReferenceNo
                Me.ShowCashPaymentHeaderData()
                Me.ProcessTabKey(True)
            Else : Me.stbVisitNo.ReadOnly = False
            End If
            Me.ApplySecurity()
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

#Region " Security Method "

    Private Sub ApplySecurity()

        Try

            If Me.btnEdit.Enabled Then Security.Apply(Me.btnEdit, AccessRights.Update)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

#End Region


    Private Sub dgvPaymentDetails_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPaymentDetails.CellEndEdit

        If e.ColumnIndex.Equals(Me.colQuantity.Index) OrElse
            e.ColumnIndex.Equals(Me.colUnitPrice.Index) OrElse
            e.ColumnIndex.Equals(Me.colDiscount.Index) Then
            Me.CalculateCashAmount(e.RowIndex)
            Me.CalculateCashTotalBill()

        ElseIf e.ColumnIndex.Equals(Me.colInclude.Index) Then
            Me.CalculateCashTotalBill()
        End If

    End Sub

    Private Sub CalculateCashAmount(ByVal rowNo As Integer)

        Try

            Dim cells As DataGridViewCellCollection = Me.dgvPaymentDetails.Rows(rowNo).Cells

            Dim cashAmount As Decimal = DecimalMayBeEnteredIn(cells, Me.colCashAmount)
            Dim discount As Decimal = DecimalMayBeEnteredIn(cells, Me.colDiscount)

            Me.dgvPaymentDetails.Item(Me.colAmount.Name, rowNo).Value = FormatNumber(cashAmount - discount)

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub CalculateCashTotalBill()

        Dim totalBill As Decimal

        Me.nbxAmountTendered.Clear()

        For rowNo As Integer = 0 To Me.dgvPaymentDetails.RowCount - 1
            If CBool(Me.dgvPaymentDetails.Item(Me.colInclude.Name, rowNo).Value) = True Then
                Dim cells As DataGridViewCellCollection = Me.dgvPaymentDetails.Rows(rowNo).Cells
                Dim amount As Decimal = DecimalMayBeEnteredIn(cells, Me.colAmount)
                totalBill += amount
            End If
        Next

        Me.nbxAmountTendered.Text = FormatNumber(totalBill, AppData.DecimalPlaces)
        Me.stbAmountWords.Text = NumberToWords(totalBill)


    End Sub

    Private Function CashRecordSaved(ByVal hideMessage As Boolean) As Boolean

        Try
            Dim message As String

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.dgvPaymentDetails.RowCount >= 1 Then
                If Me.dgvPaymentDetails.RowCount = 1 Then
                    message = "Please ensure that current payment detail is saved!"
                Else : message = "Please ensure that current payment details are saved!"
                End If
                If Not hideMessage Then DisplayMessage(message)
                 Me.BringToFront()
                If Me.WindowState = FormWindowState.Minimized Then Me.WindowState = FormWindowState.Normal
                Return False
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Return True

        Catch ex As Exception
            Return True

        End Try

    End Function

    Private Sub LoadCashItems(ByVal visitNo As String)

        Dim oPayStatusID As New LookupDataID.PayStatusID()
        Dim oBillModesID As New LookupDataID.BillModesID()
        Dim oServiceCodes As New LookupDataID.ServiceCodes()
        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItems As New SyncSoft.SQLDb.Items()

            Me.dgvPaymentDetails.Rows.Clear()

            Dim cashBillModeID As String = GetLookupDataDes(oBillModesID.Cash)
            Dim billMode As String = StringMayBeEnteredIn(Me.stbBillMode)

            If billMode.ToUpper().Equals(cashBillModeID.ToUpper()) Then

                If (Not formInSaveMode) Then
                    'fetch only items for a particular reference number and visit numbers
                    Dim ReferenceNo As String = RevertText(StringEnteredIn(stbReferenceNo, "Reference No"))
                    items = oItems.GetItemsByVisitAndReferenceNo(visitNo, ReferenceNo).Tables("Items")
                Else
                    items = oItems.GetItems(visitNo, String.Empty, String.Empty, oPayStatusID.NotPaid, cashBillModeID).Tables("Items")
                End If

            Else : items = oItems.GetNotPaidItemsCASH(visitNo).Tables("Items")
            End If

                If items Is Nothing OrElse items.Rows.Count < 1 Then
                    DisplayMessage("The Visit No: " + FormatText(visitNo, "Visits", "VisitNo") + " has no unpaid for items!")
                    Return
                End If

                For rowNo As Integer = 0 To items.Rows.Count - 1

                    Dim row As DataRow = items.Rows(rowNo)

                    With Me.dgvPaymentDetails

                        .Rows.Add()

                        .Item(Me.colInclude.Name, rowNo).Value = True
                        .Item(Me.colItemCode.Name, rowNo).Value = StringEnteredIn(row, "ItemCode")
                        .Item(Me.colItemName.Name, rowNo).Value = StringEnteredIn(row, "ItemName")
                        .Item(Me.colCategory.Name, rowNo).Value = StringMayBeEnteredIn(row, "ItemCategory")
                        .Item(Me.colQuantity.Name, rowNo).Value = IntegerMayBeEnteredIn(row, "Quantity")
                        .Item(Me.colUnitPrice.Name, rowNo).Value = FormatNumber(DecimalMayBeEnteredIn(row, "UnitPrice"))
                        .Item(Me.colDiscount.Name, rowNo).Value = FormatNumber(0)
                        .Item(Me.colAmount.Name, rowNo).Value = FormatNumber(DecimalMayBeEnteredIn(row, "Amount"))
                        .Item(Me.colCashAmount.Name, rowNo).Value = FormatNumber(DecimalMayBeEnteredIn(row, "Amount"))
                        .Item(Me.colItemStatus.Name, rowNo).Value = StringEnteredIn(row, "ItemStatus")
                        .Item(Me.colItemCategoryID.Name, rowNo).Value = StringMayBeEnteredIn(row, "ItemCategoryID")
                        .Item(Me.colItemStatusID.Name, rowNo).Value = StringMayBeEnteredIn(row, "ItemStatusID")
                    .Item(Me.colItemDetails.Name, rowNo).Value = StringMayBeEnteredIn(row, "ItemDetails")
                    .Item(Me.colInvoiceNo.Name, rowNo).Value = StringMayBeEnteredIn(row, "InvoiceNo")

                    End With

                    Me.CalculateCashAmount(rowNo)

                Next

                For Each row As DataGridViewRow In Me.dgvPaymentDetails.Rows
                    If row.IsNewRow Then Exit For

                    If Me.dgvPaymentDetails.Item(Me.colItemCode.Name, row.Index).Value.Equals(oServiceCodes.ServiceFee) Then
                        row.Cells(colInclude.Index).ReadOnly = True
                    row.Cells(colInclude.Index).Style.BackColor = Color.FromKnownColor(KnownColor.MistyRose)
                    row.Cells(colInvoiceNo.Index).Style.BackColor = Color.FromKnownColor(KnownColor.Info)
                Else
                    row.Cells(colInclude.Index).ReadOnly = False
                    row.Cells(colCashAmount.Index).ReadOnly = True
                    row.Cells(colInvoiceNo.Index).Style.BackColor = Color.FromKnownColor(KnownColor.Info)
                    If Not formInSaveMode Then
                        row.Cells(colInclude.Index).ReadOnly = True
                        row.Cells(colInclude.Index).Style.BackColor = Color.FromKnownColor(KnownColor.MistyRose)

                        row.Cells(colDiscount.Index).ReadOnly = True
                        row.Cells(colDiscount.Index).Style.BackColor = Color.FromKnownColor(KnownColor.Info)


                    End If

                End If
            Next

                Me.CalculateCashTotalBill()

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub checkPaymentMobileMoney(ByVal requestReference As String, ByVal Payphone As String)
        Try
            Dim oPayStatusID As New LookupDataID.PayStatusID()
            Dim oPaymentRequests As New SyncSoft.SQLDb.PaymentRequests()
            'Dim oPaymentRequestPayStatus As New SyncSoft.SQLDb.PaymentRequestPayStatus()
            Dim clientID As String
            Dim Uri As Uri
            Dim myUrl As String
            Dim contentType As String
            Dim method As String
            Dim finalResponse As String = Nothing
            Dim clientSecret As String = GetIntegrationAgentToken(oIntegrationAgent.Kwiksy)
            Dim terminalId As String = AppData.ProductOwner
            clientID = GetIntegrationAgentClientID(oIntegrationAgent.Kwiksy)
            Dim amountTendered As Decimal = DecimalEnteredIn(Me.nbxAmountTendered, True, "Amount Tendered!")
            Dim visitNo As String = RevertText(StringEnteredIn(stbVisitNo, "Visit No"))
            Dim telephone As String = StringEnteredIn(stbPhoneNo, "Phone No")
            Dim ReferenceNo As String = RevertText(StringEnteredIn(stbReferenceNo, "Reference No"))
            Dim comment As String = RevertText(StringEnteredIn(stbReferenceNo, "Reference No!"))

            Dim jsonString As String = "clientId=" & clientID & "&clientSecret=" & clientSecret & "&requestReference=" & requestReference & "&terminalId=" & terminalId & "&Payphone=" & Payphone

            myUrl = "http://app.kwiksy.com/api/checkkwiksypayment"
            contentType = "application/x-www-form-urlencoded"
            method = "POST"
            Uri = New Uri(myUrl)
            Dim jsonDataBytes As Byte() = Encoding.UTF8.GetBytes(jsonString)

            Dim req As WebRequest = WebRequest.Create(Uri)
            req.ContentType = contentType
            req.Method = method
            req.ContentLength = jsonDataBytes.Length

            Dim stream As Stream = req.GetRequestStream()
            stream.Write(jsonDataBytes, 0, jsonDataBytes.Length)
            stream.Close()
            Dim response As Stream = req.GetResponse().GetResponseStream()
            Dim reader As New StreamReader(response)
            Dim res As String = reader.ReadToEnd
            If res.Contains("PENDING") Then
                finalResponse = "Payment Pending, Ask Customer to Accept Payment....."

            ElseIf res.Contains("CANCELED") Then
                finalResponse = "Payment Canceled by Customer,Use Alternative....."
                canceled = True

            ElseIf res.Contains("COMPLETED") = True Then
                finalResponse = "Payment has been Successfully Processed....."
                PaymentReceived = True
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'With oPaymentRequestPayStatus
                With oPaymentRequests
                    .ReferenceNo = ReferenceNo
                    .VisitNo = visitNo
                    .Telephone = telephone
                    .Amount = amountTendered
                    .Comment = comment
                    .PayStatus = oPayStatusID.PaidFor
                    .LoginID = CurrentUser.LoginID

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                End With

                oPaymentRequests.Update()
                SaveCashPayment()
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            ElseIf res.Contains("02") Then
                finalResponse = "Not enough parameters supplied."
            ElseIf res.Contains("03") Then
                finalResponse = "Invalid client credentials."
            ElseIf res.Contains("06") Then
                finalResponse = "Service not active in the payers country."
            Else
                finalResponse = "Invalid Customer .............."
            End If

            txtresult.Text += finalResponse & Environment.NewLine
            txtresult.AppendText(Environment.NewLine)

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub
    Public Function IsConnectedToInternet() As Boolean
        If My.Computer.Network.IsAvailable Then
            Try
                Dim IPHost As IPHostEntry = Dns.GetHostEntry("www.google.com")
                Return True
            Catch
                Return False
            End Try
        Else
            Return False
        End If
    End Function

    Private Sub fbnClose_Click(sender As System.Object, e As System.EventArgs) Handles fbnClose.Click
        Try
            Me.Close()
            tmrCheckPayment.Stop()

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default
        End Try


    End Sub

    Private Sub fbnLoad_Click(sender As System.Object, e As System.EventArgs) Handles fbnLoad.Click
        Dim message As String
        Dim oVariousOptions As New VariousOptions()
        Dim oPayStatusID As New LookupDataID.PayStatusID()
        Dim oItemStatusID As New LookupDataID.ItemStatusID()
        Dim oPaymentRequests As New SyncSoft.SQLDb.PaymentRequests()
        Dim oIntegrationAgent As New LookupDataID.IntegrationAgents()


        Try

            Dim amountTendered As Decimal = DecimalEnteredIn(Me.nbxAmountTendered, True, "Amount Tendered!")
            Dim telephone As String = StringEnteredIn(stbPhoneNo, "Phone No")
            Dim comment As String = RevertText(StringEnteredIn(stbReferenceNo, "Reference No!"))
            invoiceNoGenerated = GetNextInvoiceNo()
            receiptNo = GetNextReceiptNo()
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim nonSelected As Boolean = False

            If amountTendered <= 0 Then Throw New ArgumentException("You can not make a payment of Zero (0) or less Amount.")

            For Each row As DataGridViewRow In Me.dgvPaymentDetails.Rows
                If row.IsNewRow Then Exit For
                If CBool(Me.dgvPaymentDetails.Item(Me.colInclude.Name, row.Index).Value) = True Then
                    nonSelected = False
                    Exit For
                End If
                nonSelected = True
            Next

            If Me.dgvPaymentDetails.RowCount < 1 OrElse nonSelected Then Throw New ArgumentException("Must include at least one entry on payment details!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If oVariousOptions.AllowAccessCashServices Then

                Dim hasPendingItems As Boolean = False
                For Each row As DataGridViewRow In Me.dgvPaymentDetails.Rows
                    If row.IsNewRow Then Exit For
                    If CBool(Me.dgvPaymentDetails.Item(Me.colInclude.Name, row.Index).Value) = True Then
                        Dim itemStatus As String = StringMayBeEnteredIn(row.Cells, Me.colItemStatus)
                        If itemStatus.ToUpper().Equals(GetLookupDataDes(oItemStatusID.Pending).ToUpper()) Then
                            hasPendingItems = True
                            Exit For
                        End If
                    End If
                    hasPendingItems = False
                Next

                If hasPendingItems Then
                    If oVariousOptions.AllowProcessingPendingItems Then
                        Message = "You have chosen to receipt pending service(s), changes at service point will not be allowed after this action. " +
                             ControlChars.NewLine + "Are you sure you want to save?"
                        If WarningMessage(Message) = Windows.Forms.DialogResult.No Then Return
                    Else : Message = "The system is set not to allow receipting pending service(s). Please contact your administrator!"
                        Throw New ArgumentException(Message)
                    End If

                End If

            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim hasNegativeAmount As Boolean = False

            For Each row As DataGridViewRow In Me.dgvPaymentDetails.Rows
                If row.IsNewRow Then Exit For
                If CBool(Me.dgvPaymentDetails.Item(Me.colInclude.Name, row.Index).Value) = True Then
                    Dim amount As Decimal = DecimalMayBeEnteredIn(row.Cells, Me.colAmount)
                    If amount < 0 Then
                        hasNegativeAmount = True
                        Exit For
                    End If
                End If
                hasNegativeAmount = False
            Next

            If hasNegativeAmount Then
                Dim count As Integer = 0
                For Each row As DataGridViewRow In Me.dgvPaymentDetails.Rows
                    If row.IsNewRow Then Exit For
                    If CBool(Me.dgvPaymentDetails.Item(Me.colInclude.Name, row.Index).Value) = True Then count += 1
                Next
                If count > 1 Then Throw New ArgumentException("Negative amount usually meant for a refund, cannot be receipted with other item(s)!")
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim hasDiscount As Boolean = False

            For Each row As DataGridViewRow In Me.dgvPaymentDetails.Rows
                If row.IsNewRow Then Exit For
                If CBool(Me.dgvPaymentDetails.Item(Me.colInclude.Name, row.Index).Value) = True Then
                    Dim discount As Decimal = DecimalMayBeEnteredIn(row.Cells, Me.colDiscount)
                    If discount > 0 Then
                        hasDiscount = True
                        Exit For
                    End If
                End If
                hasDiscount = False
            Next

            If hasDiscount Then
                Dim count As Integer = 0
                For Each row As DataGridViewRow In Me.dgvPaymentDetails.Rows
                    If row.IsNewRow Then Exit For
                    If CBool(Me.dgvPaymentDetails.Item(Me.colInclude.Name, row.Index).Value) = True Then count += 1
                Next
                If Not oVariousOptions.AllowDirectDiscountCashPayment AndAlso count > 0 Then
                    message = "The system is set not to allow a discount. Please contact your administrator!"
                    Throw New ArgumentException(message)

                ElseIf oVariousOptions.AllowDirectDiscountCashPayment AndAlso count > 0 Then
                    message = "You have chosen to give a discount that needs authorization. " +
                        ControlChars.NewLine + "Are you sure you want to save?"
                    If DeleteMessage(message) = Windows.Forms.DialogResult.No Then Return
                End If
            End If
            
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                

            With oPaymentRequests
                .ReferenceNo = RevertText(StringEnteredIn(stbReferenceNo, "Reference No!"))
                .VisitNo = RevertText(StringEnteredIn(stbVisitNo, "Visit No!"))
                .Telephone = telephone
                .Amount = amountTendered
                .Comment = comment
                .PayStatus = oPayStatusID.NotPaid
                .LoginID = CurrentUser.LoginID

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            End With
            oPaymentRequests.Save()
            'Then update the item reference number
            UpdateItemReferenceNo()

            'Execute the method to submit the payment request only if the form is in Save mode
            'Otherwise the form should only check for the payment Request status
            If formInSaveMode Then
                PayViaKwiksyMobileMoney(telephone, amountTendered, comment, comment)
            End If

            tmrCheckPayment.Start()
            tbcPayments.SelectTab(Me.tpgCheckStatus.Name)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
        End Try

    End Sub


    Private Sub tmrCheckPayment_Tick(sender As System.Object, e As System.EventArgs) Handles tmrCheckPayment.Tick
        Try
            Try
                If canceled.Equals(False) AndAlso PaymentReceived.Equals(False) Then
                    'FirstDayOfWeek check if internet is available
                    If IsConnectedToInternet() Then
                        checkPaymentMobileMoney(RevertText(stbReferenceNo.Text), stbPhoneNo.Text)
                    Else
                        txtresult.Text += "Your computer is not connected to internet. Please connect and try again." & Environment.NewLine
                        txtresult.AppendText(Environment.NewLine)
                    End If
                ElseIf canceled.Equals(True) Or PaymentReceived.Equals(True) Then
                        tmrCheckPayment.Stop()
                End If
            Catch ex As Exception
                ErrorMessage(ex)

            Finally
                Me.Cursor = Cursors.Default
            End Try

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default
        End Try


    End Sub

    Private Function GetInvoiceDetails(visitNo As String, copayTypeID As String) As List(Of DBConnect)

        Dim lInvoiceDetails As New List(Of DBConnect)
        Dim invoiceNo As String = RevertText(invoiceNoGenerated)
        Dim oObjectNames As New LookupDataID.AccessObjectNames()
        For rowNo As Integer = 0 To Me.dgvPaymentDetails.RowCount - 1

            Dim cells As DataGridViewCellCollection = Me.dgvPaymentDetails.Rows(rowNo).Cells
            Dim itemCode As String = StringEnteredIn(cells, Me.colItemCode, "item!")
            Dim itemCategoryID As String = StringEnteredIn(cells, Me.colItemCategoryID)
            Dim prevInvoiceNo As String = StringMayBeEnteredIn(cells, Me.colInvoiceNo)
            If String.IsNullOrEmpty(prevInvoiceNo) Then

                Using oInvoiceDetails As New SyncSoft.SQLDb.InvoiceDetails()



                    With oInvoiceDetails
                        .InvoiceNo = invoiceNo
                        .VisitNo = visitNo
                        .ItemCode = itemCode
                        .ItemCategoryID = itemCategoryID
                        .ObjectName = oObjectNames.Items()
                        .VisitTypeID = oVisitTypeID.OutPatient()
                        .Quantity = IntegerEnteredIn(cells, Me.colQuantity)
                        .UnitPrice = DecimalEnteredIn(cells, Me.colUnitPrice, False, "unit price!")
                        .Discount = 0
                        .Amount = DecimalEnteredIn(cells, Me.colAmount, False, "amount!")



                    End With


                    lInvoiceDetails.Add(oInvoiceDetails)
                End Using
            End If
        Next

        Return lInvoiceDetails
    End Function

    Private Sub ShowCashPatientDetails(ByVal visitNo As String)

        Dim oVisits As New SyncSoft.SQLDb.Visits()

        Try

            Dim visits As DataTable = oVisits.GetVisitPaymentData(visitNo).Tables("Visits")
            Dim row As DataRow = visits.Rows(0)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Change the value of reference number only if in save mode
            If formInSaveMode Then
                Dim reference As String = GetIntegrationReferenceNo(oIntegrationAgent.Kwiksy)
                Me.stbReferenceNo.Text = (reference + CStr(GetNextPayNo()))
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbVisitNo.Text = FormatText(visitNo, "Visits", "VisitNo")

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Me.stbFullName.Text = StringEnteredIn(row, "FullName")
            Me.stbPatientNo.Text = FormatText(StringEnteredIn(row, "PatientNo"), "Patients", "PatientNo")
            'Change the phone number only in save mode
            If formInSaveMode Then
                Me.stbPhoneNo.Text = StringMayBeEnteredIn(row, "Phone")
            End If

            Me.stbVisitDate.Text = FormatDate(DateEnteredIn(row, "VisitDate"))
            Me.stbBillMode.Text = StringEnteredIn(row, "BillMode")
            Me.stbCoPayType.Text = StringMayBeEnteredIn(row, "CoPayType")
            Me.nbxCoPayPercent.Value = SingleMayBeEnteredIn(row, "CoPayPercent").ToString()
            Me.nbxCoPayValue.Value = FormatNumber(DecimalMayBeEnteredIn(row, "CoPayValue"), AppData.DecimalPlaces)
            BillCustomerName = StringMayBeEnteredIn(row, "BillCustomerName")

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.ApplySecurity()

        Catch eX As Exception
            Throw eX

        End Try

    End Sub

    Private Sub UpdateItemReferenceNo()
        Dim lItems As New List(Of DBConnect)
        Dim transactions As New List(Of TransactionList(Of DBConnect))

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim visitNo As String = RevertText(StringEnteredIn(Me.stbVisitNo, "Visit No!"))
            Dim reference As String = RevertText(StringEnteredIn(stbReferenceNo, "Reference No"))

            For rowNo As Integer = 0 To Me.dgvPaymentDetails.RowCount - 1

                If CBool(Me.dgvPaymentDetails.Item(Me.colInclude.Name, rowNo).Value) = True Then

                    Dim cells As DataGridViewCellCollection = Me.dgvPaymentDetails.Rows(rowNo).Cells
                    Dim itemCode As String = StringEnteredIn(cells, Me.colItemCode, "item!")
                    Dim itemCategoryID As String = StringEnteredIn(cells, Me.colItemCategoryID)

                    Using oItems As New SyncSoft.SQLDb.ItemReferenceNo()
                        With oItems
                            .VisitNo = visitNo
                            .ItemCode = itemCode
                            .ItemCategoryID = itemCategoryID
                            .ReferenceNo = reference
                        End With
                        lItems.Add(oItems)
                    End Using
                End If
            Next
            transactions.Add(New TransactionList(Of DBConnect)(lItems, Action.Update))
            DoTransactions(transactions)
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub SaveCashPayment()

        Dim oPayments As New SyncSoft.SQLDb.Payments()
        Dim oVariousOptions As New VariousOptions()

        Dim oLookupData As New SyncSoft.Lookup.SQL.LookupData()
        Dim oPayModesID As New LookupDataID.PayModesID()
        Dim oCurrenciesID As New LookupDataID.CurrenciesID()
        Dim oAccountGroupID As New LookupDataID.AccountGroupID()
        Dim oPayStatusID As New LookupDataID.PayStatusID()
        Dim oBillModesID As New LookupDataID.BillModesID()
        Dim oAccountActionID As New LookupDataID.AccountActionID()
        Dim oEntryModeID As New LookupDataID.EntryModeID()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
        Dim oItemStatusID As New LookupDataID.ItemStatusID()
        Dim oExtraItemCodes As New LookupDataID.ExtraItemCodes()
        Dim oInvoices As New SyncSoft.SQLDb.Invoices()
        Dim oReceiptInvoiceDetails As New SyncSoft.SQLDb.ReceiptInvoiceDetails()

        Dim lPayments As New List(Of DBConnect)
        Dim linvoices As New List(Of DBConnect)
        Dim lInvoiceDetails As New List(Of DBConnect)
        Dim lAccountBalance As New List(Of DBConnect)
        Dim lSendBalance As New List(Of DBConnect)
        Dim lPaymentDetails As New List(Of DBConnect)
        Dim lItems As New List(Of DBConnect)
        Dim lItemsCASH As New List(Of DBConnect)
        Dim transactions As New List(Of TransactionList(Of DBConnect))

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim visitNo As String = RevertText(StringEnteredIn(Me.stbVisitNo, "Visit No!"))
            Dim patientNo As String = RevertText(StringMayBeEnteredIn(Me.stbPatientNo))

            Dim coPayType As String = StringMayBeEnteredIn(Me.stbCoPayType)
            Dim coPayPercent As Single = Me.nbxCoPayPercent.GetSingle()
            Dim copayTypeID As String = GetLookupDataID(LookupObjects.CoPayType, coPayType)

            Dim cashAccount As String = GetLookupDataDes(oBillModesID.Cash)
            Dim billMode As String = StringMayBeEnteredIn(Me.stbBillMode)


            Dim reference As String = RevertText(StringEnteredIn(stbReferenceNo, "Reference No"))
            Dim withholdingTax As Decimal = 0
            Dim grandDiscount As Decimal = 0
            Dim invoiceAmount As Decimal = 0
            Dim amountTendered As Decimal = DecimalEnteredIn(Me.nbxAmountTendered, True, "Amount Tendered!")
            Dim change As Decimal = 0
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oPayments


                .ReceiptNo = receiptNo
                If copayTypeID.ToUpper().Equals(oCopayTypeID.Percent().ToUpper()) OrElse copayTypeID.Equals(oCopayTypeID.Value().ToUpper()) Then
                    .PayTypeID = oPayTypeID.VisitBillCASH()
                ElseIf copayTypeID.ToUpper().Equals(oCopayTypeID.NA().ToUpper()) Then
                    .PayTypeID = oPayTypeID.VisitBill()
                End If
                .PayNo = visitNo
                .ClientFullName = StringEnteredIn(Me.stbFullName)
                .PayDate = payDate
                .PayModesID = oPayModesID.ElectronicFundTransfer
                .DocumentNo = reference
                .Amount = amountTendered
                .AmountWords = StringMayBeEnteredIn(Me.stbAmountWords)
                .Notes = "Paid Via Kwiksy On Reference No: " + reference
                .CurrenciesID = oCurrenciesID.UgandaShillings
                .WithholdingTax = withholdingTax
                .GrandDiscount = grandDiscount
                .AmountTendered = amountTendered
                .ExchangeRate = 1
                .Change = 0

                If change <= 0 Then
                    .CashAmount = amountTendered
                Else
                    .CashAmount = amountTendered - change
                End If

                .SendBalanceToAccount = False
                .UseAccountBalance = False
                .FilterNo = String.Empty
                .LoginID = CurrentUser.LoginID
            End With

            ValidateEntriesIn(Me.tpgToPayForItems)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            lPayments.Add(oPayments)




            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            For rowNo As Integer = 0 To Me.dgvPaymentDetails.RowCount - 1

                If CBool(Me.dgvPaymentDetails.Item(Me.colInclude.Name, rowNo).Value) = True Then

                    Dim cells As DataGridViewCellCollection = Me.dgvPaymentDetails.Rows(rowNo).Cells
                    Dim itemCode As String = StringEnteredIn(cells, Me.colItemCode, "item!")
                    Dim itemCategoryID As String = StringEnteredIn(cells, Me.colItemCategoryID)
                    Dim prevInvoiceNo As String = StringMayBeEnteredIn(cells, Me.colInvoiceNo)

                    Dim quantity As Integer = IntegerEnteredIn(cells, Me.colQuantity)

                    Using oPaymentDetails As New SyncSoft.SQLDb.PaymentDetails()
                        With oPaymentDetails
                            .ReceiptNo = receiptNo
                            .VisitNo = visitNo
                            .ItemCode = itemCode
                            If String.IsNullOrEmpty(prevInvoiceNo) Then
                                .InvoiceNo = invoiceNoGenerated
                            Else
                                .InvoiceNo = prevInvoiceNo
                            End If

                            .ItemCategoryID = itemCategoryID
                            .VisitTypeID = oVisitTypeID.OutPatient()
                            .Quantity = quantity
                            If itemCategoryID.ToUpper().Equals(oItemCategoryID.Extras.ToUpper()) AndAlso
                                 (itemCode.ToUpper().Equals(oExtraItemCodes.COPAYVALUE.ToUpper())) Then
                                .UnitPrice = Math.Abs(DecimalEnteredIn(cells, Me.colUnitPrice, True, "Unit Price!"))
                            Else : .UnitPrice = DecimalEnteredIn(cells, Me.colUnitPrice, False, "Unit Price!")
                            End If
                            .Discount = DecimalEnteredIn(cells, Me.colDiscount, True, "discount!")
                            If itemCategoryID.ToUpper().Equals(oItemCategoryID.Extras.ToUpper()) AndAlso
                                 (itemCode.ToUpper().Equals(oExtraItemCodes.COPAYVALUE.ToUpper())) Then
                                .Amount = DecimalEnteredIn(cells, Me.colAmount, True, "amount!")
                            Else : .Amount = DecimalEnteredIn(cells, Me.colAmount, False, "amount!")
                            End If
                            If String.IsNullOrEmpty(prevInvoiceNo) Then invoiceAmount += .Amount
                        End With

                        lPaymentDetails.Add(oPaymentDetails)

                    End Using
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    If billMode.ToUpper().Equals(cashAccount.ToUpper()) Then

                        Using oItems As New SyncSoft.SQLDb.Items()
                            With oItems
                                .VisitNo = visitNo
                                .ItemCode = itemCode
                                .ItemCategoryID = itemCategoryID
                                .LastUpdate = payDate
                                .PayStatusID = oPayStatusID.PaidFor
                                .LoginID = CurrentUser.LoginID
                                .ItemStatusID = String.Empty
                            End With
                            lItems.Add(oItems)
                        End Using

                    Else

                        Using oItemsCASH As New SyncSoft.SQLDb.ItemsCASH()
                            With oItemsCASH
                                .VisitNo = visitNo
                                .ItemCode = itemCode
                                .ItemCategoryID = itemCategoryID
                            End With
                            lItemsCASH.Add(oItemsCASH)
                        End Using

                    End If
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                End If
            Next

              '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Generate an Invoice for Cash Payments


            lInvoiceDetails = GetInvoiceDetails(visitNo, copayTypeID)

            If lInvoiceDetails.Count() > 0 Then



                With oInvoices

                    .InvoiceNo = invoiceNoGenerated
                    If copayTypeID.ToUpper().Equals(oCopayTypeID.Percent().ToUpper()) OrElse copayTypeID.Equals(oCopayTypeID.Value().ToUpper()) Then
                        .PayTypeID = oPayTypeID.VisitBillCASH()
                    ElseIf copayTypeID.ToUpper().Equals(oCopayTypeID.NA().ToUpper()) Then
                        .PayTypeID = oPayTypeID.VisitBill()
                    End If
                    .PayNo = visitNo
                    .InvoiceDate = payDate
                    .StartDate = payDate
                    .EndDate = payDate
                    .Amount = invoiceAmount
                    .AmountWords = NumberToWords(invoiceAmount)
                    .Locked = True
                    .EntryModeID = oEntryModeID.System()
                    .LoginID = CurrentUser.LoginID

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ValidateEntriesIn(Me)
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                End With

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                linvoices.Add(oInvoices)

                transactions.Add(New TransactionList(Of DBConnect)(linvoices, Action.Save))
                transactions.Add(New TransactionList(Of DBConnect)(lInvoiceDetails, Action.Save))
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ValidateEntriesIn(Me.tpgToPayForItems, ErrProvider)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            transactions.Add(New TransactionList(Of DBConnect)(lPayments, Action.Save))
            transactions.Add(New TransactionList(Of DBConnect)(lPaymentDetails, Action.Save))

            If billMode.ToUpper().Equals(cashAccount.ToUpper()) Then
                transactions.Add(New TransactionList(Of DBConnect)(lItems, Action.Update))
            Else : transactions.Add(New TransactionList(Of DBConnect)(lItemsCASH, Action.Update))
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim lservicePoints As New List(Of String)
            Dim oServicePoint As New LookupDataID.ServicePointID()
            Dim lWaitingPatient As New List(Of DBConnect)

            ''''make queue
            lWaitingPatient = GetQueuesList(visitNo, oServicePoint.Cashier(), lservicePoints)
            transactions.Add(New TransactionList(Of DBConnect)(lWaitingPatient, Action.Save))

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            DoTransactions(transactions)


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Not Me.chkPrintReceiptOnSaving.Checked Then
                Dim Message As String = "You have not checked Print Receipt On Saving. " + ControlChars.NewLine + "Would you want a receipt printed?"
                If WarningMessage(Message) = Windows.Forms.DialogResult.Yes Then Me.PrintCashReceipt(True)
            Else : Me.PrintCashReceipt(True)
            End If

            Dim allSelected As Boolean = True

            For Each row As DataGridViewRow In Me.dgvPaymentDetails.Rows
                If row.IsNewRow Then Exit For
                If CBool(Me.dgvPaymentDetails.Item(Me.colInclude.Name, row.Index).Value) = False Then
                    allSelected = False
                    Me.ShowCashPatientDetails(visitNo)
                    Me.LoadCashItems(visitNo)
                    Exit For
                End If
                allSelected = True
            Next

            If allSelected Then
                ResetControlsIn(Me.tpgToPayForItems)
                ResetControlsIn(Me.tpgCheckStatus)
                Me.dgvPaymentDetails.Rows.Clear()
                Me.txtresult.Clear()

            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            If ex.Message.Contains("The Receipt No:") Then
                SaveCashPayment()

            ElseIf ex.Message.Contains("The Invoice No:") OrElse ex.Message.EndsWith("already exists") Then
                SaveCashPayment()

            Else : ErrorMessage(ex)
            End If
        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub ShowCashPaymentHeaderData()
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim visitNo As String = RevertText(StringMayBeEnteredIn(Me.stbVisitNo))
            If String.IsNullOrEmpty(visitNo) Then Return
            Me.ShowCashPatientDetails(visitNo)
            Me.LoadCashItems(visitNo)
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub
    Private Sub GetPaymentRequests(ByVal referenceNo As String)

        Dim oPaymentRequests As New SyncSoft.SQLDb.PaymentRequests()
        Dim oPayStatusID As New LookupDataID.PayStatusID()

        Try

            Dim visits As DataTable = oPaymentRequests.GetPaymentRequests(referenceNo).Tables("PaymentRequests")
            Dim row As DataRow = visits.Rows(0)
            Dim PayStatus As String = StringEnteredIn(row, "PayStatus")
            Dim visitNo As String = FormatText(StringEnteredIn(row, "VisitNo"), "Visits", "VisitNo")

            If PayStatus.Equals(oPayStatusID.PaidFor) Then
                Throw New ArgumentException("Payment of Reference No : " + referenceNo + " is already processed!")
            End If

            Me.stbVisitNo.Text = visitNo
            Me.stbPhoneNo.Text = StringEnteredIn(row, "Telephone")
        Catch eX As Exception
            Throw eX

        End Try

    End Sub

    Private Sub btnLoadPendingCashPayment_Click(sender As System.Object, e As System.EventArgs) Handles btnLoadPendingCashPayment.Click
        Try

            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Not Me.CashRecordSaved(False) Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fPendingItems As New frmPendingItems(Me.stbVisitNo, AlertItemCategory.CashPayment)
            fPendingItems.ShowDialog(Me)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
           ShowCashPaymentHeaderData
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim amountTendered As Decimal = DecimalEnteredIn(Me.nbxAmountTendered, True, "Amount Tendered!")
            If amountTendered < 1 Then
                DisplayMessage("You can not make a payment of Zero (0) or less Amount.")
                ClearCashPaymentControls()
                Return

            End If

            
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub btnFindVisitNo_Click(sender As System.Object, e As System.EventArgs) Handles btnFindVisitNo.Click
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If Not Me.CashRecordSaved(False) Then Return

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim fFindVisitNo As New frmFindAutoNo(Me.stbVisitNo, AutoNumber.VisitNo)
        fFindVisitNo.ShowDialog(Me)

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ShowCashPaymentHeaderData()
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Sub

    Private Sub ClearCashPaymentControls()

        Me.stbFullName.Clear()
        Me.stbPatientNo.Clear()
        Me.stbVisitDate.Clear()
        Me.nbxAmountTendered.Clear()

        Me.stbBillMode.Clear()
        Me.stbCoPayType.Clear()
        Me.nbxCoPayPercent.Value = String.Empty
        Me.nbxCoPayValue.Value = String.Empty
        Me.stbAmountWords.Clear()
        Me.dgvPaymentDetails.Rows.Clear()
        Me.txtresult.Clear()
        If Not formInSaveMode Then
            Me.stbPhoneNo.Clear()
        End If

    End Sub

    Private Sub stbVisitNo_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stbVisitNo.Enter

        Try
            currentCashAllSaved = Me.CashRecordSaved(False)
            If Not currentCashAllSaved Then
                currentCashVisitNo = StringMayBeEnteredIn(Me.stbVisitNo)
                ProcessTabKey(True)
            Else : currentCashVisitNo = String.Empty
            End If

        Catch ex As Exception
            currentCashVisitNo = String.Empty
        End Try

    End Sub

    Private Sub stbVisitNo_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles stbVisitNo.Leave

        Try

            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Not Me.CashRecordSaved(False) AndAlso Not String.IsNullOrEmpty(currentCashVisitNo) Then
                Me.stbVisitNo.Text = currentCashVisitNo
                Return
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.ShowCashPaymentHeaderData()
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch eX As Exception
            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub stbVisitNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stbVisitNo.TextChanged
        Me.ClearCashPaymentControls()
    End Sub

#Region "Cashier THERMAL RECEIPT PRINTOUT"

    Private Sub PrintCashReceipt(ByVal receiptSaved As Boolean)

        Try

            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.dgvPaymentDetails.RowCount < 1 Then Throw New ArgumentException("Must include at least one entry on payment details!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim nonSelected As Boolean = False

            For Each row As DataGridViewRow In Me.dgvPaymentDetails.Rows
                If row.IsNewRow Then Exit For
                If CBool(Me.dgvPaymentDetails.Item(Me.colInclude.Name, row.Index).Value) = True Then
                    nonSelected = False
                    Exit For
                End If
                nonSelected = True
            Next

            If nonSelected Then Throw New ArgumentException("Must include at least one entry on payment details!")
            Me.PrintCashierThermalReceipt()

        Catch ex As Exception
            Throw ex

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Function GetPaymentDetailsItemTotalsList() As List(Of Tuple(Of String, Decimal))

        Try

            ' Create list of tuples with two items each.
            Dim paymentDetails As New List(Of Tuple(Of String, Decimal))

            For rowNo As Integer = 0 To Me.dgvPaymentDetails.RowCount - 1

                If CBool(Me.dgvPaymentDetails.Item(Me.colInclude.Name, rowNo).Value) = True Then

                    Dim cells As DataGridViewCellCollection = Me.dgvPaymentDetails.Rows(rowNo).Cells
                    Dim itemName As String = cells.Item(Me.colItemName.Name).Value.ToString()
                    Dim amount As Decimal = DecimalEnteredIn(cells, Me.colAmount, False, "amount!")

                    paymentDetails.Add(New Tuple(Of String, Decimal)(itemName, amount))

                End If
            Next

            Return paymentDetails

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Sub PrintCashierThermalReceipt()

        Dim dlgPrint As New PrintDialog()

        Try

            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.dgvPaymentDetails.RowCount < 1 Then Throw New ArgumentException("Must include at least one entry for Payments!")

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim nonSelected As Boolean = False

            For Each row As DataGridViewRow In Me.dgvPaymentDetails.Rows
                If row.IsNewRow Then Exit For
                If CBool(Me.dgvPaymentDetails.Item(Me.colInclude.Name, row.Index).Value) = True Then
                    nonSelected = False
                    Exit For
                End If
                nonSelected = True
            Next

            If nonSelected Then Throw New ArgumentException("Must include at least one entry for payments!")

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.SetCashierThermalReceiptPrintData()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            dlgPrint.Document = docCashThermalReceipt
            dlgPrint.Document.PrinterSettings.Collate = True
            If dlgPrint.ShowDialog = DialogResult.OK Then docCashThermalReceipt.Print()

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub docCashierThermalReceipt_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles docCashThermalReceipt.PrintPage

        Try

            Dim titleFont As New Font(printFontName, 12, FontStyle.Bold)

            Dim xPos As Single = CSng(e.MarginBounds.Left / 10)
            Dim yPos As Single = CSng(e.MarginBounds.Top / 8)

            Dim lineHeight As Single = bodyNormalFont.GetHeight(e.Graphics)

            Dim title As String = AppData.ProductOwner.ToUpper()


            Dim fullName As String = StringMayBeEnteredIn(Me.stbFullName)
            Dim patientNo As String = StringMayBeEnteredIn(Me.stbPatientNo)
            Dim VisitNo As String = StringMayBeEnteredIn(Me.stbVisitNo)
            Dim VisitDate As String = StringMayBeEnteredIn(Me.stbVisitDate)
            Dim billMode As String = StringMayBeEnteredIn(Me.stbBillMode)

            ' Increment the page number.
            pageNo += 1

            With e.Graphics

                Dim widthTopFirst As Single = .MeasureString("W", titleFont).Width
                Dim widthTopSecond As Single = 9 * widthTopFirst
                Dim widthTopThird As Single = 11 * widthTopFirst

                If pageNo < 2 Then

                    '.DrawString(title, titleFont, Brushes.Black, xPos, yPos)
                    'yPos += 3 * lineHeight
                    If (title.Length > 25) Then
                        .DrawString(title.Substring(0, 25), titleFont, Brushes.Black, xPos, yPos)
                        yPos += lineHeight
                        .DrawString(title.Substring(25), titleFont, Brushes.Black, xPos, yPos)
                        yPos += lineHeight
                        .DrawString("CASHIER RECEIPT".ToUpper(), titleFont, Brushes.Black, xPos, yPos)
                        yPos += 3 * lineHeight
                    Else
                        .DrawString(title, titleFont, Brushes.Black, xPos, yPos)
                        yPos += lineHeight
                        .DrawString("CASHIER RECEIPT".ToUpper(), titleFont, Brushes.Black, xPos, yPos)
                        yPos += 3 * lineHeight
                    End If

                    .DrawString("Received From: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    If (fullName.Length > 15) Then
                        .DrawString(fullName.Substring(0, 14), bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                        yPos += lineHeight
                        .DrawString(fullName.Substring(14), bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                        yPos += lineHeight
                    Else
                        .DrawString(fullName, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                        yPos += lineHeight
                    End If

                    .DrawString("Receipt No: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(receiptNo, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight

                    .DrawString("Patient No: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(patientNo, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight

                    .DrawString("Visit No: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(VisitNo, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight

                    .DrawString("Visit Date: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(VisitDate, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight

                    .DrawString("Bill Mode: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(billMode, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight

                    If (BillCustomerName.Length > 14) Then

                        .DrawString("Bill Customer: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                        .DrawString(BillCustomerName.Substring(0, 14), bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                        yPos += lineHeight
                        .DrawString(BillCustomerName.Substring(14), bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                        yPos += 2 * lineHeight

                    Else
                        .DrawString("Bill Customer: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                        .DrawString(BillCustomerName.Trim, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                        yPos += 2 * lineHeight
                    End If
                End If

                Dim _StringFormat As New StringFormat()

                ' Draw the rest of the text left justified,
                ' wrap at words, and don't draw partial lines.

                With _StringFormat
                    .Alignment = StringAlignment.Near
                    .FormatFlags = StringFormatFlags.LineLimit
                    .Trimming = StringTrimming.Word
                End With

                Dim charactersFitted As Integer
                Dim linesFilled As Integer

                If CashThermalReceiptParagraphs Is Nothing Then Return

                Do While CashThermalReceiptParagraphs.Count > 0

                    ' Print the next paragraph.
                    Dim oPrintParagraps As PrintParagraps = DirectCast(CashThermalReceiptParagraphs(1), PrintParagraps)
                    CashThermalReceiptParagraphs.Remove(1)

                    ' Get the area available for this paragraph.
                    Dim printAreaRectangle As RectangleF = New RectangleF(xPos, yPos, e.PageBounds.Width - xPos, e.MarginBounds.Bottom - yPos)

                    ' If the printing area rectangle's height < 1, make it 1.
                    If printAreaRectangle.Height < 1 Then printAreaRectangle.Height = 1

                    ' See how big the text will be and how many characters will fit.
                    Dim textSize As SizeF = .MeasureString(oPrintParagraps.Text, oPrintParagraps.TheFont,
                        New SizeF(printAreaRectangle.Width, printAreaRectangle.Height), _StringFormat, charactersFitted, linesFilled)

                    ' See if any characters will fit.
                    If charactersFitted > 0 Then
                        ' Draw the text.
                        .DrawString(oPrintParagraps.Text, oPrintParagraps.TheFont, Brushes.Black, printAreaRectangle, _StringFormat)
                        ' Increase the location where we can start, add a little interparagraph spacing.
                        yPos += textSize.Height ' + oPrintParagraps.TheFont.GetHeight(e.Graphics))

                    End If

                    ' See if some of the paragraph didn't fit on the page.
                    If charactersFitted < oPrintParagraps.Text.Length Then
                        ' Some of the paragraph didn't fit, prepare to print the rest on the next page.
                        oPrintParagraps.Text = oPrintParagraps.Text.Substring(charactersFitted)
                        CashThermalReceiptParagraphs.Add(oPrintParagraps, Before:=1)
                        Exit Do
                    End If
                Loop

                ' If we have more paragraphs, we have more pages.
                e.HasMorePages = (CashThermalReceiptParagraphs.Count > 0)

            End With

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetCashierThermalReceiptPrintData()

        Dim padItemNo As Integer = 4
        Dim padItemName As Integer = 16
        Dim padQuantity As Integer = 8
        Dim padIAmount As Integer = 10

        Dim padQty As Integer = 4
        Dim padUp As Integer = 9
        Dim padDc As Integer = 8
        Dim padTl As Integer = 13


        Dim footerFont As New Font(printFontName, 9)

        pageNo = 0
        CashThermalReceiptParagraphs = New Collection()

        Try

            Dim tableHeader As New System.Text.StringBuilder(String.Empty)
            tableHeader.Append(ControlChars.NewLine)
            '--------------------------------------------------------------------------------------------------
            tableHeader.Append("Description".PadRight(padItemName))
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append("Qty".PadRight(padQty))
            tableHeader.Append("Price".PadLeft(padUp))
            ' tableHeader.Append("Disc%".PadLeft(padDc))
            tableHeader.Append(" Total".PadLeft(padTl))
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append("-------------------------------".PadRight(padItemName))
            '--------------------------------------------------------------------------------------------------
            CashThermalReceiptParagraphs.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))

            Dim totalAmount As New System.Text.StringBuilder(String.Empty)
            Dim Amount As Double = 0.0
            Dim count As Integer
            Dim tableData As New System.Text.StringBuilder(String.Empty)

            For rowNo As Integer = 0 To Me.dgvPaymentDetails.RowCount - 1

                If CBool(Me.dgvPaymentDetails.Item(Me.colInclude.Name, rowNo).Value) = True Then

                    Dim cells As DataGridViewCellCollection = Me.dgvPaymentDetails.Rows(rowNo).Cells

                    count += 1

                    Dim itemNo As String = (count).ToString()
                    Dim ItemName As String = cells.Item(Me.colItemName.Name).Value.ToString()
                    Dim itemQuantity As String = cells.Item(Me.colQuantity.Name).Value.ToString()
                    Dim itemUnitPrice As String = cells.Item(Me.colUnitPrice.Name).Value.ToString()
                    'Dim itemDicount As String = cells.Item(Me.colDiscount.Name).Value.ToString()
                    Dim itemAmount As String = cells.Item(Me.colAmount.Name).Value.ToString()
                    'tableData.Append(itemNo.PadRight(padItemNo))
                    tableData.Append(ItemName.PadRight(padItemNo))
                    tableData.Append(ControlChars.NewLine)
                    tableData.Append(itemQuantity.PadRight(padQty))
                    tableData.Append(itemUnitPrice.PadLeft(padUp))
                    'tableData.Append(itemDicount.PadLeft(padDc))
                    tableData.Append(itemAmount.PadLeft(padTl))
                    tableData.Append(ControlChars.NewLine)

                End If
            Next

            tableData.Append(ControlChars.NewLine)
            CashThermalReceiptParagraphs.Add(New PrintParagraps(bodyNormalFont, tableData.ToString()))


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim paymentDetailsItemTotals = From data In Me.GetPaymentDetailsItemTotalsList Group data By ItemName = data.Item1
                                 Into Totals = Sum(data.Item2) Select ItemName, Totals

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            For Each item In paymentDetailsItemTotals
                Dim totals As Double = Convert.ToInt32(item.Totals)
                Amount += totals
            Next
            Dim receiptAmount As String = FormatNumber(Amount, AppData.DecimalPlaces)
            totalAmount.Append("Total Amount:  ".PadRight(padQty))
            totalAmount.Append(GetSpaces(3).PadRight(padQty))
            totalAmount.Append(receiptAmount.PadLeft(padTl))

            totalAmount.Append(ControlChars.NewLine)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            CashThermalReceiptParagraphs.Add(New PrintParagraps(bodyBoldFont, totalAmount.ToString()))
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim appreciationData As New System.Text.StringBuilder(String.Empty)
            appreciationData.Append(ControlChars.NewLine)
            appreciationData.Append("*** Thank you for choosing ***")
            appreciationData.Append(ControlChars.NewLine)
            appreciationData.Append("*** " + AppData.ProductOwner + " ***")
            appreciationData.Append(ControlChars.NewLine)
            CashThermalReceiptParagraphs.Add(New PrintParagraps(bodyBoldFont, appreciationData.ToString()))
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim footerData As New System.Text.StringBuilder(String.Empty)
            footerData.Append(ControlChars.NewLine)
            footerData.Append("Printed by " + CurrentUser.FullName)
            footerData.Append(ControlChars.NewLine)
            footerData.Append("On " + FormatDate(Now))
            footerData.Append(ControlChars.NewLine)
            footerData.Append("At " + Now.ToString("hh:mm tt") + " from " + AppData.AppTitle)
            footerData.Append(ControlChars.NewLine)
            CashThermalReceiptParagraphs.Add(New PrintParagraps(footerFont, footerData.ToString()))

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

#End Region

    Private Sub EnableEditUnitPrice(ByVal state As Boolean)

        Dim editCellStyle As New System.Windows.Forms.DataGridViewCellStyle(Me.colUnitPrice.DefaultCellStyle)

        If state Then
            editCellStyle.BackColor = System.Drawing.SystemColors.Window
            Me.btnEdit.Text = UpdateText
        Else
            editCellStyle.BackColor = System.Drawing.SystemColors.Info
            Me.btnEdit.Text = EditText
        End If

        Me.colUnitPrice.ReadOnly = Not state
        Me.EnableEditControls(Not state)
        Me.colUnitPrice.DefaultCellStyle = editCellStyle

    End Sub
    Private Sub PopulateFormInfo()
        Try
            If Not formInSaveMode Then
                Dim ReferenceNo As String = RevertText(StringEnteredIn(stbReferenceNo, "Reference No"))
                If Not String.IsNullOrEmpty(ReferenceNo) Then
                    GetPaymentRequests(ReferenceNo)
                    ShowCashPaymentHeaderData()
                End If
            End If

        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub EnableEditControls(ByVal state As Boolean)

        Dim oVariousOptions As New VariousOptions()
        Me.fbnLoad.Enabled = state


    End Sub


    Private Sub btnEdit_Click(sender As System.Object, e As System.EventArgs) Handles btnEdit.Click

        Dim oPayStatusID As New LookupDataID.PayStatusID()
        Dim oCoPayTypeID As New LookupDataID.CoPayTypeID()

        Try
            Me.Cursor = Cursors.WaitCursor

            If Me.btnEdit.Text = UpdateText Then

                Dim visitNo As String = RevertText(StringEnteredIn(Me.stbVisitNo, "Visit No!"))

                If Me.dgvPaymentDetails.RowCount < 1 Then Throw New ArgumentException("Must have at least one entry for payment details!")

                For Each row As DataGridViewRow In Me.dgvPaymentDetails.Rows

                    If row.IsNewRow Then Exit For

                    StringEnteredIn(row.Cells, Me.colItemCode, "item code!")
                    IntegerEnteredIn(row.Cells, Me.colQuantity, "quantity!")
                    DecimalEnteredIn(row.Cells, Me.colUnitPrice, False, "unit price!")
                    DecimalEnteredIn(row.Cells, Me.colDiscount, True, "discount!")
                    DecimalEnteredIn(row.Cells, Me.colAmount, False, "amount!")
                    StringEnteredIn(row.Cells, Me.colItemStatus, "item status!")
                Next

                Dim coPayType As String = StringMayBeEnteredIn(Me.stbCoPayType)
                Dim coPayPercent As Single = Me.nbxCoPayPercent.GetSingle()

                For rowNo As Integer = 0 To Me.dgvPaymentDetails.RowCount - 1

                    Dim lItems As New List(Of DBConnect)
                    Dim lItemsCASH As New List(Of DBConnect)
                    Dim transactions As New List(Of TransactionList(Of DBConnect))

                    Dim cells As DataGridViewCellCollection = Me.dgvPaymentDetails.Rows(rowNo).Cells
                    Dim itemCode As String = StringEnteredIn(cells, Me.colItemCode)
                    Dim quantity As Integer = IntegerEnteredIn(cells, Me.colQuantity)
                    Dim unitPrice As Decimal = DecimalEnteredIn(cells, Me.colUnitPrice, False)
                    Dim cashAmount As Decimal = CDec(quantity * unitPrice * coPayPercent) / 100
                    Dim itemCategoryID As String = StringEnteredIn(cells, Me.colItemCategoryID)

                    Try

                        Using oItems As New SyncSoft.SQLDb.Items()
                            With oItems
                                .VisitNo = visitNo
                                .ItemCode = itemCode
                                .Quantity = quantity
                                .UnitPrice = unitPrice
                                .ItemDetails = StringMayBeEnteredIn(cells, Me.colItemDetails)
                                .LastUpdate = DateEnteredIn(Me.stbVisitDate, "Visit Date!")
                                .ItemCategoryID = itemCategoryID
                                .ItemStatusID = StringEnteredIn(cells, Me.colItemStatusID)
                                .PayStatusID = oPayStatusID.NotPaid
                                .LoginID = CurrentUser.LoginID
                            End With
                            lItems.Add(oItems)
                        End Using

                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        transactions.Add(New TransactionList(Of DBConnect)(lItems, Action.Save))

                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        If coPayType.ToUpper().Equals(GetLookupDataDes(oCoPayTypeID.Percent).ToUpper()) Then
                            Using oItemsCASH As New SyncSoft.SQLDb.ItemsCASH()
                                With oItemsCASH
                                    .VisitNo = visitNo
                                    .ItemCode = itemCode
                                    .ItemCategoryID = itemCategoryID
                                    .CashAmount = cashAmount
                                    .CashPayStatusID = oPayStatusID.NotPaid
                                End With
                                lItemsCASH.Add(oItemsCASH)
                            End Using
                            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            transactions.Add(New TransactionList(Of DBConnect)(lItemsCASH, Action.Save))
                            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        End If

                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        DoTransactions(transactions)
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    Catch ex As Exception
                        ErrorMessage(ex)

                    End Try

                Next

                '''''''''''''''''''''''''''''''''''''''''''
                Me.LoadCashItems(visitNo)
                Me.EnableEditUnitPrice(False)
                '''''''''''''''''''''''''''''''''''''''''''

            Else : Me.EnableEditUnitPrice(True)
            End If

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub
#Region " Edit Methods "

    Public Sub Edit()
        Me.Text = "Check Mobile Money Payment"
        Me.fbnLoad.Text = "Check Payment"
        formInSaveMode = False
        Me.stbVisitNo.Enabled = False
        Me.btnLoadPendingCashPayment.Enabled = False
        Me.btnFindVisitNo.Enabled = False
        Me.stbReferenceNo.ReadOnly = False
        Me.stbPhoneNo.Enabled = False
        Me.stbPhoneNo.BackColor = System.Drawing.SystemColors.Control
        Me.btnEdit.Enabled = False

    End Sub

    Public Sub Save()
        Me.Text = "Make Mobile Money Payment"
        Me.fbnLoad.Text = "Submit Payment"
        formInSaveMode = True
        Me.stbVisitNo.Enabled = True
        Me.stbReferenceNo.ReadOnly = True
        Me.stbPhoneNo.BackColor = System.Drawing.SystemColors.Window
        Me.stbPhoneNo.Enabled = True
        Me.btnLoadPendingCashPayment.Enabled = True
        Me.btnFindVisitNo.Enabled = True
        Me.btnEdit.Enabled = False

    End Sub
#End Region

    Private Sub stbReferenceNo_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stbReferenceNo.Leave
        PopulateFormInfo()
    End Sub

    Private Sub stbPhoneNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stbPhoneNo.TextChanged

    End Sub

    Private Sub stbReferenceNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stbReferenceNo.TextChanged
        Try
            If String.IsNullOrEmpty(Me.stbReferenceNo.Text) OrElse String.IsNullOrWhiteSpace(Me.stbReferenceNo.Text) AndAlso formInSaveMode Then
                ClearCashPaymentControls()
            End If
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub
    Private Sub stbReferenceNo_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles stbReferenceNo.KeyPress
        If Not formInSaveMode Then
            If Asc(e.KeyChar) = 13 Then
                PopulateFormInfo()
                e.Handled = True
            End If
        End If
    End Sub
End Class