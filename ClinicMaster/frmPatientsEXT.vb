
Option Strict On

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls

Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects

Public Class frmPatientsEXT

#Region " Fields "
    Private defaultPatientNo As String = String.Empty
    Private birthDate As Date
#End Region

Private Sub frmPatientsEXT_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

	Try
		Me.Cursor = Cursors.WaitCursor()

            LoadLookupDataCombo(Me.cboAttachedToID, LookupObjects.AttachedTo, False)

            If Not String.IsNullOrEmpty(defaultPatientNo) Then
                Me.stbPatientNo.Text = FormatText(defaultPatientNo, "Patients", "PatientNo")
                Me.stbPatientNo.ReadOnly = True
                Me.ShowPatientDetails(defaultPatientNo)
                Me.ProcessTabKey(True)
            Else : Me.stbPatientNo.ReadOnly = False
            End If

	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

Private Sub frmPatientsEXT_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
	If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
End Sub

Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
	Me.Close()
End Sub

Private Sub fbnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnDelete.Click

Dim oPatientsEXT As New SyncSoft.SQLDb.PatientsEXT()

	Try
		Me.Cursor = Cursors.WaitCursor()

		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		If DeleteMessage() = Windows.Forms.DialogResult.No Then Return

		oPatientsEXT.PatientNo = StringEnteredIn(Me.stbPatientNo, "Patient No!")
		oPatientsEXT.AlternateNo = StringEnteredIn(Me.stbAlternateNo, "Alternate No!")

		DisplayMessage(oPatientsEXT.Delete())
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		ResetControlsIn(Me)
		Me.CallOnKeyEdit()

	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

    End Sub

    Private Sub SetNextPatientNo()

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim oPatients As New SyncSoft.SQLDb.Patients()
            Dim oAutoNumbers As New SyncSoft.Options.SQL.AutoNumbers()
            Dim patientNoPrefix As String = cboAttachedToID.Text + Today.Year.ToString().Substring(2)
            Dim autoNumbers As DataTable = oAutoNumbers.GetAutoNumbers("PatientsEXT", "AlternateNo").Tables("AutoNumbers")
            Dim row As DataRow = autoNumbers.Rows(0)
            Dim paddingLEN As Integer = IntegerEnteredIn(row, "PaddingLEN")
            Dim paddingCHAR As Char = CChar(StringEnteredIn(row, "PaddingCHAR"))
            Dim nextPatientNo As String = CStr(oPatients.GetNextPatientsEXTAlternateNoID).PadLeft(paddingLEN, paddingCHAR)
            Me.stbAlternateNo.Text = FormatText((patientNoPrefix + nextPatientNo).Trim(), "PatientsEXT", "AlternateNo")

        Catch eX As Exception
            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

Private Sub fbnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnSearch.Click

Dim patientNo As String
Dim alternateNo As String

Dim oPatientsEXT As New SyncSoft.SQLDb.PatientsEXT()

	Try
		Me.Cursor = Cursors.WaitCursor()

		patientNo = StringEnteredIn(Me.stbPatientNo, "Patient No!")
		alternateNo = StringEnteredIn(Me.stbAlternateNo, "Alternate No!")

            Dim dataSource As DataTable = oPatientsEXT.GetPatientsEXT(patientNo).Tables("PatientsEXT")
		Me.DisplayData(dataSource)

	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

    Private Sub ebnSaveUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ebnSaveUpdate.Click

        Dim oVariousOptions As New VariousOptions()
        Dim oPatientsEXT As New SyncSoft.SQLDb.PatientsEXT()

        Try
            Me.Cursor = Cursors.WaitCursor()

            With oPatientsEXT


                .PatientNo = RevertText(StringEnteredIn(Me.stbPatientNo, "Patient No!"))
                .AttachedToID = StringValueEnteredIn(Me.cboAttachedToID, "AttachedToID!")
                .AlternateNo = RevertText(StringEnteredIn(Me.stbAlternateNo, "Alternate No!"))
                .Notes = StringEnteredIn(Me.stbNotes, "Notes!")
                .LoginID = CurrentUser.LoginID

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ValidateEntriesIn(Me)
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End With

            Select Case Me.ebnSaveUpdate.ButtonText


                Case ButtonCaption.Save

                    oPatientsEXT.Save()

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    
                        Dim Message As String = "Would you like to open visits registration form now?"
                        If WarningMessage(Message) = Windows.Forms.DialogResult.Yes Then
                            Dim fVisits As New frmVisits(oPatientsEXT.PatientNo, ItemsKeyNo.PatientNo)
                            fVisits.Save()
                            fVisits.ShowDialog()
                        End If

                    ResetControlsIn(Me)
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case ButtonCaption.Update

                    DisplayMessage(oPatientsEXT.Update())
                    Me.CallOnKeyEdit()

            End Select

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

#Region " Edit Methods "

Public Sub Edit()

	Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update
	Me.ebnSaveUpdate.Enabled = False
	Me.fbnDelete.Visible = True
	Me.fbnDelete.Enabled = False
	Me.fbnSearch.Visible = True

	ResetControlsIn(Me)

End Sub

    Public Sub Save()
        Me.ebnSaveUpdate.ButtonText = ButtonCaption.Save
        Me.ebnSaveUpdate.Enabled = True
        Me.fbnDelete.Visible = False
        Me.fbnDelete.Enabled = True
        Me.fbnSearch.Visible = False
        Me.SetNextPatientNo()
        '' ResetControlsIn(Me)

    End Sub

Private Sub DisplayData(ByVal dataSource As DataTable)

Try

	Me.ebnSaveUpdate.DataSource = dataSource
	Me.ebnSaveUpdate.LoadData(Me)

	Me.ebnSaveUpdate.Enabled = dataSource.Rows.Count > 0
	Me.fbnDelete.Enabled = dataSource.Rows.Count > 0

	Security.Apply(Me.ebnSaveUpdate, AccessRights.Update)
	Security.Apply(Me.fbnDelete, AccessRights.Delete)

Catch ex As Exception
	Throw ex
End Try

End Sub

Private Sub CallOnKeyEdit()
If Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update Then
	Me.ebnSaveUpdate.Enabled = False
	Me.fbnDelete.Enabled = False
End If
End Sub

#End Region

    Private Sub cboAttachedToID_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboAttachedToID.SelectedIndexChanged
        SetNextPatientNo()
    End Sub

    Private Sub btnLoad_Click(sender As System.Object, e As System.EventArgs) Handles btnLoad.Click
        Try

            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fQuickSearch As New SyncSoft.SQL.Win.Forms.QuickSearch("Patients", Me.stbPatientNo)
            fQuickSearch.ShowDialog(Me)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim patientNo As String = RevertText(StringMayBeEnteredIn(Me.stbPatientNo))
            If Not String.IsNullOrEmpty(patientNo) Then Me.ShowPatientDetails(patientNo)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub stbPatientNo_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles stbPatientNo.Leave

        Try

            Dim patientNo As String = RevertText(StringMayBeEnteredIn(Me.stbPatientNo))
            ErrProvider.Clear()
            If String.IsNullOrEmpty(patientNo) Then Return
            Me.ShowPatientDetails(patientNo)

        Catch ex As Exception
            ErrorMessage(ex)
        End Try

    End Sub

    Private Sub ClearControls()

        Me.stbFullName.Clear()
        Me.stbAge.Clear()
        Me.lblAgeString.Text = String.Empty
        Me.stbGender.Clear()
        Me.stbJoinDate.Clear()
        Me.stbPhone.Clear()
        Me.stbLastVisitDate.Clear()
      End Sub

    Private Sub ShowPatientDetails(ByVal patientNo As String)

        Dim oPatients As New SyncSoft.SQLDb.Patients()
       
        Try

            Me.Cursor = Cursors.WaitCursor

            Me.ClearControls()

            Dim patients As DataTable = oPatients.GetPatients(patientNo).Tables("Patients")
            Dim row As DataRow = patients.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbPatientNo.Text = FormatText(patientNo, "Patients", "PatientNo")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbFullName.Text = StringEnteredIn(row, "FullName")
            Me.stbGender.Text = StringEnteredIn(row, "Gender")
            Me.stbAge.Text = StringEnteredIn(row, "Age")
            BirthDate = DateMayBeEnteredIn(row, "BirthDate")
            Me.lblAgeString.Text = GetAgeString(birthDate, True)
            Me.stbJoinDate.Text = FormatDate(DateEnteredIn(row, "JoinDate"))
            Me.stbPhone.Text = StringMayBeEnteredIn(row, "Phone")
            Me.stbLastVisitDate.Text = FormatDate(DateMayBeEnteredIn(row, "LastVisitDate"))

        Catch eX As Exception
            Me.ClearControls()
            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub stbPatientNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stbPatientNo.TextChanged
        If Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update Then Return
        Me.ClearControls()
    End Sub

End Class