
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAntenatalVisits : Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAntenatalVisits))
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.cboPallorID = New System.Windows.Forms.ComboBox()
        Me.cboJaundiceID = New System.Windows.Forms.ComboBox()
        Me.cboLynphadenopathyID = New System.Windows.Forms.ComboBox()
        Me.cboVaricoseID = New System.Windows.Forms.ComboBox()
        Me.cboOedemaID = New System.Windows.Forms.ComboBox()
        Me.cboHeartSoundID = New System.Windows.Forms.ComboBox()
        Me.cboAirEntryID = New System.Windows.Forms.ComboBox()
        Me.cboBreastID = New System.Windows.Forms.ComboBox()
        Me.cboLiverID = New System.Windows.Forms.ComboBox()
        Me.cboSpleenID = New System.Windows.Forms.ComboBox()
        Me.cboBowelSoundsID = New System.Windows.Forms.ComboBox()
        Me.cboScarID = New System.Windows.Forms.ComboBox()
        Me.cboPupilReactionID = New System.Windows.Forms.ComboBox()
        Me.cboReflexesID = New System.Windows.Forms.ComboBox()
        Me.cboOtherSTIID = New System.Windows.Forms.ComboBox()
        Me.stbSTIDetails = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboVulvaID = New System.Windows.Forms.ComboBox()
        Me.cboCervixID = New System.Windows.Forms.ComboBox()
        Me.cboAdnexaID = New System.Windows.Forms.ComboBox()
        Me.cboVaginaID = New System.Windows.Forms.ComboBox()
        Me.nbxAnenorrheaWeeks = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.stbFundalHeight = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboLieID = New System.Windows.Forms.ComboBox()
        Me.stbRelationPPOrBrim = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.nbxFoetalHeart = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.chkTTGiven = New System.Windows.Forms.CheckBox()
        Me.stbRemarks = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.dtpReturnDate = New System.Windows.Forms.DateTimePicker()
        Me.cboDoctorSpecialtyID = New System.Windows.Forms.ComboBox()
        Me.cboSkeletalDeformityID = New System.Windows.Forms.ComboBox()
        Me.cboPositionID = New System.Windows.Forms.ComboBox()
        Me.cboNurseID = New System.Windows.Forms.ComboBox()
        Me.cboNetUse = New System.Windows.Forms.ComboBox()
        Me.cboIPT = New System.Windows.Forms.ComboBox()
        Me.nbxDiagonalConjugate = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.nbxSacralCurve = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.nbxIschialSpine = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.nbxSubPublicAngle = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.nbxIschialTuberosities = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.cboConclusionID = New System.Windows.Forms.ComboBox()
        Me.stbRiskFactors = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbRecommendations = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboPresentationID = New System.Windows.Forms.ComboBox()
        Me.cboUterusID = New System.Windows.Forms.ComboBox()
        Me.cboDoctorID = New System.Windows.Forms.ComboBox()
        Me.lblVisitNo = New System.Windows.Forms.Label()
        Me.lblPallorID = New System.Windows.Forms.Label()
        Me.lblJaundiceID = New System.Windows.Forms.Label()
        Me.lblLynphadenopathyID = New System.Windows.Forms.Label()
        Me.lblVaricoseID = New System.Windows.Forms.Label()
        Me.lblOedemaID = New System.Windows.Forms.Label()
        Me.lblHeartSoundID = New System.Windows.Forms.Label()
        Me.lblAirEntryID = New System.Windows.Forms.Label()
        Me.lblBreastID = New System.Windows.Forms.Label()
        Me.lblLiverID = New System.Windows.Forms.Label()
        Me.lblSpleenID = New System.Windows.Forms.Label()
        Me.lblBowelSoundsID = New System.Windows.Forms.Label()
        Me.lblScarID = New System.Windows.Forms.Label()
        Me.lblPupilReactionID = New System.Windows.Forms.Label()
        Me.lblReflexesID = New System.Windows.Forms.Label()
        Me.lblOtherSTIID = New System.Windows.Forms.Label()
        Me.lblSTIDetails = New System.Windows.Forms.Label()
        Me.lblVulvaID = New System.Windows.Forms.Label()
        Me.lblCervixID = New System.Windows.Forms.Label()
        Me.lblAdnexaID = New System.Windows.Forms.Label()
        Me.lblVaginaID = New System.Windows.Forms.Label()
        Me.lblUterusID = New System.Windows.Forms.Label()
        Me.lblAnenorrheaWeeks = New System.Windows.Forms.Label()
        Me.lblFundalHeight = New System.Windows.Forms.Label()
        Me.lblPresentation = New System.Windows.Forms.Label()
        Me.lblLie = New System.Windows.Forms.Label()
        Me.lblRelationPPOrBrim = New System.Windows.Forms.Label()
        Me.lblFoetalHeart = New System.Windows.Forms.Label()
        Me.lblIPT = New System.Windows.Forms.Label()
        Me.lblNetUse = New System.Windows.Forms.Label()
        Me.lblRemarks = New System.Windows.Forms.Label()
        Me.lblReturnDate = New System.Windows.Forms.Label()
        Me.lblExaminerSpecialityID = New System.Windows.Forms.Label()
        Me.lblDoctorID = New System.Windows.Forms.Label()
        Me.grpAbdomen = New System.Windows.Forms.GroupBox()
        Me.grpCNS = New System.Windows.Forms.GroupBox()
        Me.grpChest = New System.Windows.Forms.GroupBox()
        Me.grpExamination = New System.Windows.Forms.GroupBox()
        Me.grpSTIs = New System.Windows.Forms.GroupBox()
        Me.grpGeneral = New System.Windows.Forms.GroupBox()
        Me.lblSkeletalDeformityID = New System.Windows.Forms.Label()
        Me.tbcPhysicalExamination = New System.Windows.Forms.TabControl()
        Me.tpgANCVitals = New System.Windows.Forms.TabPage()
        Me.nbxHeartRate = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblHeartRate = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.stbNotes = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.nbxBMI = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblBMI = New System.Windows.Forms.Label()
        Me.btnTriage = New System.Windows.Forms.Button()
        Me.stbBMIStatus = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblBMIStatus = New System.Windows.Forms.Label()
        Me.lblOxygenSaturation = New System.Windows.Forms.Label()
        Me.nbxOxygenSaturation = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.nbxRespirationRate = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblRespirationRate = New System.Windows.Forms.Label()
        Me.nbxMUAC = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblHeight = New System.Windows.Forms.Label()
        Me.lblMUAC = New System.Windows.Forms.Label()
        Me.nbxTemperature = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblTemperature = New System.Windows.Forms.Label()
        Me.lblWeight = New System.Windows.Forms.Label()
        Me.nbxPulse = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.nbxHeight = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblPulse = New System.Windows.Forms.Label()
        Me.nbxWeight = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblBloodPressure = New System.Windows.Forms.Label()
        Me.stbBloodPressure = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.tpgPhysicalExamination = New System.Windows.Forms.TabPage()
        Me.tpgPelvicExamination = New System.Windows.Forms.TabPage()
        Me.grpPrognosis = New System.Windows.Forms.GroupBox()
        Me.lblRiskFactors = New System.Windows.Forms.Label()
        Me.lblRecommendations = New System.Windows.Forms.Label()
        Me.grpAssessment = New System.Windows.Forms.GroupBox()
        Me.lblIschialSpine = New System.Windows.Forms.Label()
        Me.lblSubPublicAngle = New System.Windows.Forms.Label()
        Me.lblSacralCurve = New System.Windows.Forms.Label()
        Me.lblIschialTuberosities = New System.Windows.Forms.Label()
        Me.lblDiagonalConjugate = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tpgFoetalExamination = New System.Windows.Forms.TabPage()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tpgANCAllergies = New System.Windows.Forms.TabPage()
        Me.btnAddAllergy = New System.Windows.Forms.Button()
        Me.dgvPatientAllergies = New System.Windows.Forms.DataGridView()
        Me.colAllergyNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAllergyCategory = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colReaction = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPatientAllergiesSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.stbFullName = New System.Windows.Forms.TextBox()
        Me.lblName = New System.Windows.Forms.Label()
        Me.stbBloodGroup = New System.Windows.Forms.TextBox()
        Me.lblBloodGroup = New System.Windows.Forms.Label()
        Me.stbTotalVisits = New System.Windows.Forms.TextBox()
        Me.lblNoOfVisits = New System.Windows.Forms.Label()
        Me.stbPhone = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.stbAddress = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.txtVisitDate = New System.Windows.Forms.TextBox()
        Me.lblVisitDate = New System.Windows.Forms.Label()
        Me.stbHIVStatus = New System.Windows.Forms.TextBox()
        Me.stbEDD = New System.Windows.Forms.TextBox()
        Me.lblEDD = New System.Windows.Forms.Label()
        Me.stbANCJoinDate = New System.Windows.Forms.TextBox()
        Me.lblANCJoinDate = New System.Windows.Forms.Label()
        Me.pnlNavigateANCVisits = New System.Windows.Forms.Panel()
        Me.chkNavigateVisits = New System.Windows.Forms.CheckBox()
        Me.navVisits = New SyncSoft.Common.Win.Controls.DataNavigator()
        Me.stbPartnersHIVStatus = New System.Windows.Forms.TextBox()
        Me.lblHIVPartnerStatus = New System.Windows.Forms.Label()
        Me.spbPhoto = New SyncSoft.Common.Win.Controls.SmartPictureBox()
        Me.lblPhoto = New System.Windows.Forms.Label()
        Me.stbVisitNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbPatientNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblPatientNo = New System.Windows.Forms.Label()
        Me.btnFindByFingerprint = New System.Windows.Forms.Button()
        Me.lblNurse = New System.Windows.Forms.Label()
        Me.stbANCNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblANCNo = New System.Windows.Forms.Label()
        Me.btnLoadSeeANCVisits = New System.Windows.Forms.Button()
        Me.pnlCreateNewRound = New System.Windows.Forms.Panel()
        Me.chkCreateNewVisit = New System.Windows.Forms.CheckBox()
        Me.btnFindVisitNo = New System.Windows.Forms.Button()
        Me.btnClear = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.grpAbdomen.SuspendLayout()
        Me.grpCNS.SuspendLayout()
        Me.grpChest.SuspendLayout()
        Me.grpExamination.SuspendLayout()
        Me.grpSTIs.SuspendLayout()
        Me.grpGeneral.SuspendLayout()
        Me.tbcPhysicalExamination.SuspendLayout()
        Me.tpgANCVitals.SuspendLayout()
        Me.tpgPhysicalExamination.SuspendLayout()
        Me.tpgPelvicExamination.SuspendLayout()
        Me.grpPrognosis.SuspendLayout()
        Me.grpAssessment.SuspendLayout()
        Me.tpgFoetalExamination.SuspendLayout()
        Me.tpgANCAllergies.SuspendLayout()
        CType(Me.dgvPatientAllergies, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlNavigateANCVisits.SuspendLayout()
        CType(Me.spbPhoto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCreateNewRound.SuspendLayout()
        Me.SuspendLayout()
        '
        'fbnDelete
        '
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(922, 442)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 43
        Me.fbnDelete.Tag = "AntenatalProgress"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = False
        Me.fbnDelete.Visible = False
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(14, 451)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 40
        Me.ebnSaveUpdate.Tag = "AntenatalProgress"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = False
        '
        'cboPallorID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboPallorID, "Pallor,PallorID")
        Me.cboPallorID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPallorID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboPallorID.Location = New System.Drawing.Point(155, 19)
        Me.cboPallorID.Name = "cboPallorID"
        Me.cboPallorID.Size = New System.Drawing.Size(112, 21)
        Me.cboPallorID.TabIndex = 1
        '
        'cboJaundiceID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboJaundiceID, "Jaundice,JaundiceID")
        Me.cboJaundiceID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJaundiceID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboJaundiceID.Location = New System.Drawing.Point(155, 42)
        Me.cboJaundiceID.Name = "cboJaundiceID"
        Me.cboJaundiceID.Size = New System.Drawing.Size(112, 21)
        Me.cboJaundiceID.TabIndex = 3
        '
        'cboLynphadenopathyID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboLynphadenopathyID, "Lynphadenopathy,LynphadenopathyID")
        Me.cboLynphadenopathyID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLynphadenopathyID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboLynphadenopathyID.Location = New System.Drawing.Point(155, 65)
        Me.cboLynphadenopathyID.Name = "cboLynphadenopathyID"
        Me.cboLynphadenopathyID.Size = New System.Drawing.Size(112, 21)
        Me.cboLynphadenopathyID.TabIndex = 5
        '
        'cboVaricoseID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboVaricoseID, "Varicose,VaricoseID")
        Me.cboVaricoseID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVaricoseID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboVaricoseID.Location = New System.Drawing.Point(155, 88)
        Me.cboVaricoseID.Name = "cboVaricoseID"
        Me.cboVaricoseID.Size = New System.Drawing.Size(112, 21)
        Me.cboVaricoseID.TabIndex = 7
        '
        'cboOedemaID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboOedemaID, "Oedema,OedemaID")
        Me.cboOedemaID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOedemaID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboOedemaID.Location = New System.Drawing.Point(155, 111)
        Me.cboOedemaID.Name = "cboOedemaID"
        Me.cboOedemaID.Size = New System.Drawing.Size(112, 21)
        Me.cboOedemaID.TabIndex = 9
        '
        'cboHeartSoundID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboHeartSoundID, "HeartSound,HeartSoundID")
        Me.cboHeartSoundID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHeartSoundID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboHeartSoundID.Location = New System.Drawing.Point(141, 13)
        Me.cboHeartSoundID.Name = "cboHeartSoundID"
        Me.cboHeartSoundID.Size = New System.Drawing.Size(149, 21)
        Me.cboHeartSoundID.TabIndex = 1
        '
        'cboAirEntryID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboAirEntryID, "AirEntry,AirEntryID")
        Me.cboAirEntryID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAirEntryID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboAirEntryID.Location = New System.Drawing.Point(141, 36)
        Me.cboAirEntryID.Name = "cboAirEntryID"
        Me.cboAirEntryID.Size = New System.Drawing.Size(149, 21)
        Me.cboAirEntryID.TabIndex = 3
        '
        'cboBreastID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboBreastID, "Breast,BreastID")
        Me.cboBreastID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBreastID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboBreastID.Location = New System.Drawing.Point(141, 59)
        Me.cboBreastID.Name = "cboBreastID"
        Me.cboBreastID.Size = New System.Drawing.Size(149, 21)
        Me.cboBreastID.TabIndex = 5
        '
        'cboLiverID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboLiverID, "Liver,LiverID")
        Me.cboLiverID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLiverID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboLiverID.Location = New System.Drawing.Point(150, 12)
        Me.cboLiverID.Name = "cboLiverID"
        Me.cboLiverID.Size = New System.Drawing.Size(132, 21)
        Me.cboLiverID.TabIndex = 1
        '
        'cboSpleenID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboSpleenID, "Spleen,SpleenID")
        Me.cboSpleenID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSpleenID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboSpleenID.Location = New System.Drawing.Point(150, 34)
        Me.cboSpleenID.Name = "cboSpleenID"
        Me.cboSpleenID.Size = New System.Drawing.Size(132, 21)
        Me.cboSpleenID.TabIndex = 3
        '
        'cboBowelSoundsID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboBowelSoundsID, "BowelSounds,BowelSoundsID")
        Me.cboBowelSoundsID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBowelSoundsID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboBowelSoundsID.Location = New System.Drawing.Point(150, 57)
        Me.cboBowelSoundsID.Name = "cboBowelSoundsID"
        Me.cboBowelSoundsID.Size = New System.Drawing.Size(132, 21)
        Me.cboBowelSoundsID.TabIndex = 5
        '
        'cboScarID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboScarID, "Scar,ScarID")
        Me.cboScarID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboScarID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboScarID.Location = New System.Drawing.Point(150, 80)
        Me.cboScarID.Name = "cboScarID"
        Me.cboScarID.Size = New System.Drawing.Size(132, 21)
        Me.cboScarID.TabIndex = 7
        '
        'cboPupilReactionID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboPupilReactionID, "PupilReaction,PupilReactionID")
        Me.cboPupilReactionID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPupilReactionID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboPupilReactionID.Location = New System.Drawing.Point(150, 15)
        Me.cboPupilReactionID.Name = "cboPupilReactionID"
        Me.cboPupilReactionID.Size = New System.Drawing.Size(132, 21)
        Me.cboPupilReactionID.TabIndex = 1
        '
        'cboReflexesID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboReflexesID, "Reflexes,ReflexesID")
        Me.cboReflexesID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReflexesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboReflexesID.Location = New System.Drawing.Point(150, 38)
        Me.cboReflexesID.Name = "cboReflexesID"
        Me.cboReflexesID.Size = New System.Drawing.Size(132, 21)
        Me.cboReflexesID.TabIndex = 3
        '
        'cboOtherSTIID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboOtherSTIID, "OtherSTI,OtherSTIID")
        Me.cboOtherSTIID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherSTIID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboOtherSTIID.Location = New System.Drawing.Point(141, 24)
        Me.cboOtherSTIID.Name = "cboOtherSTIID"
        Me.cboOtherSTIID.Size = New System.Drawing.Size(149, 21)
        Me.cboOtherSTIID.TabIndex = 1
        '
        'stbSTIDetails
        '
        Me.stbSTIDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbSTIDetails.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbSTIDetails, "STIDetails")
        Me.stbSTIDetails.EntryErrorMSG = ""
        Me.stbSTIDetails.Location = New System.Drawing.Point(141, 47)
        Me.stbSTIDetails.Multiline = True
        Me.stbSTIDetails.Name = "stbSTIDetails"
        Me.stbSTIDetails.RegularExpression = ""
        Me.stbSTIDetails.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.stbSTIDetails.Size = New System.Drawing.Size(149, 25)
        Me.stbSTIDetails.TabIndex = 3
        '
        'cboVulvaID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboVulvaID, "Vulva,VulvaID")
        Me.cboVulvaID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVulvaID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboVulvaID.Location = New System.Drawing.Point(119, 26)
        Me.cboVulvaID.Name = "cboVulvaID"
        Me.cboVulvaID.Size = New System.Drawing.Size(170, 21)
        Me.cboVulvaID.TabIndex = 1
        '
        'cboCervixID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboCervixID, "Cervix,CervixID")
        Me.cboCervixID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCervixID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboCervixID.Location = New System.Drawing.Point(119, 49)
        Me.cboCervixID.Name = "cboCervixID"
        Me.cboCervixID.Size = New System.Drawing.Size(170, 21)
        Me.cboCervixID.TabIndex = 3
        '
        'cboAdnexaID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboAdnexaID, "Adnexa,AdnexaID")
        Me.cboAdnexaID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAdnexaID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboAdnexaID.Location = New System.Drawing.Point(119, 72)
        Me.cboAdnexaID.Name = "cboAdnexaID"
        Me.cboAdnexaID.Size = New System.Drawing.Size(170, 21)
        Me.cboAdnexaID.TabIndex = 5
        '
        'cboVaginaID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboVaginaID, "Vagina,VaginaID")
        Me.cboVaginaID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVaginaID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboVaginaID.Location = New System.Drawing.Point(119, 95)
        Me.cboVaginaID.Name = "cboVaginaID"
        Me.cboVaginaID.Size = New System.Drawing.Size(170, 21)
        Me.cboVaginaID.TabIndex = 7
        '
        'nbxAnenorrheaWeeks
        '
        Me.nbxAnenorrheaWeeks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxAnenorrheaWeeks.ControlCaption = " Anenorrhea Weeks"
        Me.ebnSaveUpdate.SetDataMember(Me.nbxAnenorrheaWeeks, "AnenorrheaWeeks")
        Me.nbxAnenorrheaWeeks.DecimalPlaces = -1
        Me.nbxAnenorrheaWeeks.Location = New System.Drawing.Point(162, 7)
        Me.nbxAnenorrheaWeeks.MaxValue = 42.0R
        Me.nbxAnenorrheaWeeks.MinValue = 0.0R
        Me.nbxAnenorrheaWeeks.MustEnterNumeric = True
        Me.nbxAnenorrheaWeeks.Name = "nbxAnenorrheaWeeks"
        Me.nbxAnenorrheaWeeks.Size = New System.Drawing.Size(170, 20)
        Me.nbxAnenorrheaWeeks.TabIndex = 1
        Me.nbxAnenorrheaWeeks.Value = ""
        '
        'stbFundalHeight
        '
        Me.stbFundalHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbFundalHeight.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbFundalHeight, "FundalHeight")
        Me.stbFundalHeight.EntryErrorMSG = ""
        Me.stbFundalHeight.Location = New System.Drawing.Point(162, 30)
        Me.stbFundalHeight.Name = "stbFundalHeight"
        Me.stbFundalHeight.RegularExpression = ""
        Me.stbFundalHeight.Size = New System.Drawing.Size(170, 20)
        Me.stbFundalHeight.TabIndex = 3
        '
        'cboLieID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboLieID, "Lie,LieID")
        Me.cboLieID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLieID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboLieID.Location = New System.Drawing.Point(162, 77)
        Me.cboLieID.Name = "cboLieID"
        Me.cboLieID.Size = New System.Drawing.Size(170, 21)
        Me.cboLieID.TabIndex = 7
        '
        'stbRelationPPOrBrim
        '
        Me.stbRelationPPOrBrim.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbRelationPPOrBrim.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbRelationPPOrBrim, "RelationPPOrBrim")
        Me.stbRelationPPOrBrim.EntryErrorMSG = ""
        Me.stbRelationPPOrBrim.Location = New System.Drawing.Point(162, 127)
        Me.stbRelationPPOrBrim.Name = "stbRelationPPOrBrim"
        Me.stbRelationPPOrBrim.RegularExpression = ""
        Me.stbRelationPPOrBrim.Size = New System.Drawing.Size(170, 20)
        Me.stbRelationPPOrBrim.TabIndex = 11
        '
        'nbxFoetalHeart
        '
        Me.nbxFoetalHeart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxFoetalHeart.ControlCaption = "Foetal Heart"
        Me.ebnSaveUpdate.SetDataMember(Me.nbxFoetalHeart, "FoetalHeart")
        Me.nbxFoetalHeart.DecimalPlaces = -1
        Me.nbxFoetalHeart.Location = New System.Drawing.Point(162, 150)
        Me.nbxFoetalHeart.MaxValue = 180.0R
        Me.nbxFoetalHeart.MinValue = 80.0R
        Me.nbxFoetalHeart.MustEnterNumeric = True
        Me.nbxFoetalHeart.Name = "nbxFoetalHeart"
        Me.nbxFoetalHeart.Size = New System.Drawing.Size(170, 20)
        Me.nbxFoetalHeart.TabIndex = 13
        Me.nbxFoetalHeart.Value = ""
        '
        'chkTTGiven
        '
        Me.chkTTGiven.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkTTGiven, "TTGiven")
        Me.chkTTGiven.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkTTGiven.Location = New System.Drawing.Point(332, 135)
        Me.chkTTGiven.Name = "chkTTGiven"
        Me.chkTTGiven.Size = New System.Drawing.Size(275, 20)
        Me.chkTTGiven.TabIndex = 27
        Me.chkTTGiven.Text = "TT Vaccine Given?"
        '
        'stbRemarks
        '
        Me.stbRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbRemarks.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbRemarks, "Remarks")
        Me.stbRemarks.EntryErrorMSG = ""
        Me.stbRemarks.Location = New System.Drawing.Point(546, 50)
        Me.stbRemarks.Multiline = True
        Me.stbRemarks.Name = "stbRemarks"
        Me.stbRemarks.RegularExpression = ""
        Me.stbRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.stbRemarks.Size = New System.Drawing.Size(218, 82)
        Me.stbRemarks.TabIndex = 19
        '
        'dtpReturnDate
        '
        Me.ebnSaveUpdate.SetDataMember(Me.dtpReturnDate, "ReturnDate")
        Me.dtpReturnDate.Location = New System.Drawing.Point(456, 113)
        Me.dtpReturnDate.MinDate = New Date(2018, 5, 10, 0, 0, 0, 0)
        Me.dtpReturnDate.Name = "dtpReturnDate"
        Me.dtpReturnDate.ShowCheckBox = True
        Me.dtpReturnDate.Size = New System.Drawing.Size(151, 20)
        Me.dtpReturnDate.TabIndex = 26
        Me.dtpReturnDate.Value = New Date(2020, 1, 12, 0, 0, 0, 0)
        '
        'cboDoctorSpecialtyID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboDoctorSpecialtyID, "DoctorSpecialty,DoctorSpecialtyID")
        Me.cboDoctorSpecialtyID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDoctorSpecialtyID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboDoctorSpecialtyID.Location = New System.Drawing.Point(147, 115)
        Me.cboDoctorSpecialtyID.Name = "cboDoctorSpecialtyID"
        Me.cboDoctorSpecialtyID.Size = New System.Drawing.Size(170, 21)
        Me.cboDoctorSpecialtyID.TabIndex = 12
        '
        'cboSkeletalDeformityID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboSkeletalDeformityID, "SkeletalDeformity,SkeletalDeformityID")
        Me.cboSkeletalDeformityID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSkeletalDeformityID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboSkeletalDeformityID.Location = New System.Drawing.Point(155, 136)
        Me.cboSkeletalDeformityID.Name = "cboSkeletalDeformityID"
        Me.cboSkeletalDeformityID.Size = New System.Drawing.Size(112, 21)
        Me.cboSkeletalDeformityID.TabIndex = 11
        '
        'cboPositionID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboPositionID, "Position,PositionID")
        Me.cboPositionID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPositionID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboPositionID.Location = New System.Drawing.Point(162, 101)
        Me.cboPositionID.Name = "cboPositionID"
        Me.cboPositionID.Size = New System.Drawing.Size(170, 21)
        Me.cboPositionID.TabIndex = 9
        '
        'cboNurseID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboNurseID, "NurseInCharge, NurseInChargeID")
        Me.cboNurseID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNurseID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboNurseID.Location = New System.Drawing.Point(147, 161)
        Me.cboNurseID.Name = "cboNurseID"
        Me.cboNurseID.Size = New System.Drawing.Size(171, 21)
        Me.cboNurseID.TabIndex = 16
        '
        'cboNetUse
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboNetUse, "NetUse, NetUseID")
        Me.cboNetUse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNetUse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboNetUse.Location = New System.Drawing.Point(546, 27)
        Me.cboNetUse.Name = "cboNetUse"
        Me.cboNetUse.Size = New System.Drawing.Size(218, 21)
        Me.cboNetUse.TabIndex = 17
        '
        'cboIPT
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboIPT, "IPT, IPTID")
        Me.cboIPT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIPT.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboIPT.Location = New System.Drawing.Point(546, 5)
        Me.cboIPT.Name = "cboIPT"
        Me.cboIPT.Size = New System.Drawing.Size(218, 21)
        Me.cboIPT.TabIndex = 15
        '
        'nbxDiagonalConjugate
        '
        Me.nbxDiagonalConjugate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxDiagonalConjugate.ControlCaption = "Diagonal Conjugate"
        Me.ebnSaveUpdate.SetDataMember(Me.nbxDiagonalConjugate, "DiagonalConjugate")
        Me.nbxDiagonalConjugate.DecimalPlaces = -1
        Me.nbxDiagonalConjugate.Location = New System.Drawing.Point(150, 17)
        Me.nbxDiagonalConjugate.MaxValue = 0.0R
        Me.nbxDiagonalConjugate.MinValue = 0.0R
        Me.nbxDiagonalConjugate.MustEnterNumeric = True
        Me.nbxDiagonalConjugate.Name = "nbxDiagonalConjugate"
        Me.nbxDiagonalConjugate.Size = New System.Drawing.Size(151, 20)
        Me.nbxDiagonalConjugate.TabIndex = 1
        Me.nbxDiagonalConjugate.Value = ""
        '
        'nbxSacralCurve
        '
        Me.nbxSacralCurve.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxSacralCurve.ControlCaption = "Sacral Curve"
        Me.ebnSaveUpdate.SetDataMember(Me.nbxSacralCurve, "SacralCurve")
        Me.nbxSacralCurve.DecimalPlaces = -1
        Me.nbxSacralCurve.Location = New System.Drawing.Point(150, 40)
        Me.nbxSacralCurve.MaxValue = 0.0R
        Me.nbxSacralCurve.MinValue = 0.0R
        Me.nbxSacralCurve.MustEnterNumeric = True
        Me.nbxSacralCurve.Name = "nbxSacralCurve"
        Me.nbxSacralCurve.Size = New System.Drawing.Size(151, 20)
        Me.nbxSacralCurve.TabIndex = 3
        Me.nbxSacralCurve.Value = ""
        '
        'nbxIschialSpine
        '
        Me.nbxIschialSpine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxIschialSpine.ControlCaption = "Ischial Spine"
        Me.ebnSaveUpdate.SetDataMember(Me.nbxIschialSpine, "IschialSpine")
        Me.nbxIschialSpine.DecimalPlaces = -1
        Me.nbxIschialSpine.Location = New System.Drawing.Point(150, 63)
        Me.nbxIschialSpine.MaxValue = 0.0R
        Me.nbxIschialSpine.MinValue = 0.0R
        Me.nbxIschialSpine.MustEnterNumeric = True
        Me.nbxIschialSpine.Name = "nbxIschialSpine"
        Me.nbxIschialSpine.Size = New System.Drawing.Size(151, 20)
        Me.nbxIschialSpine.TabIndex = 5
        Me.nbxIschialSpine.Value = ""
        '
        'nbxSubPublicAngle
        '
        Me.nbxSubPublicAngle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxSubPublicAngle.ControlCaption = "Sub Public Angle"
        Me.ebnSaveUpdate.SetDataMember(Me.nbxSubPublicAngle, "SubPublicAngle")
        Me.nbxSubPublicAngle.DecimalPlaces = -1
        Me.nbxSubPublicAngle.Location = New System.Drawing.Point(150, 87)
        Me.nbxSubPublicAngle.MaxValue = 0.0R
        Me.nbxSubPublicAngle.MinValue = 0.0R
        Me.nbxSubPublicAngle.MustEnterNumeric = True
        Me.nbxSubPublicAngle.Name = "nbxSubPublicAngle"
        Me.nbxSubPublicAngle.Size = New System.Drawing.Size(151, 20)
        Me.nbxSubPublicAngle.TabIndex = 7
        Me.nbxSubPublicAngle.Value = ""
        '
        'nbxIschialTuberosities
        '
        Me.nbxIschialTuberosities.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxIschialTuberosities.ControlCaption = "Ischial Tuberosities"
        Me.ebnSaveUpdate.SetDataMember(Me.nbxIschialTuberosities, "IschialTuberosities")
        Me.nbxIschialTuberosities.DecimalPlaces = -1
        Me.nbxIschialTuberosities.Location = New System.Drawing.Point(150, 110)
        Me.nbxIschialTuberosities.MaxValue = 0.0R
        Me.nbxIschialTuberosities.MinValue = 0.0R
        Me.nbxIschialTuberosities.MustEnterNumeric = True
        Me.nbxIschialTuberosities.Name = "nbxIschialTuberosities"
        Me.nbxIschialTuberosities.Size = New System.Drawing.Size(151, 20)
        Me.nbxIschialTuberosities.TabIndex = 9
        Me.nbxIschialTuberosities.Value = ""
        '
        'cboConclusionID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboConclusionID, "Conclusion,ConclusionID")
        Me.cboConclusionID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboConclusionID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboConclusionID.Location = New System.Drawing.Point(150, 133)
        Me.cboConclusionID.Name = "cboConclusionID"
        Me.cboConclusionID.Size = New System.Drawing.Size(151, 21)
        Me.cboConclusionID.TabIndex = 11
        '
        'stbRiskFactors
        '
        Me.stbRiskFactors.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbRiskFactors.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbRiskFactors, "RiskFactors")
        Me.stbRiskFactors.EntryErrorMSG = ""
        Me.stbRiskFactors.Location = New System.Drawing.Point(127, 21)
        Me.stbRiskFactors.Multiline = True
        Me.stbRiskFactors.Name = "stbRiskFactors"
        Me.stbRiskFactors.RegularExpression = ""
        Me.stbRiskFactors.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.stbRiskFactors.Size = New System.Drawing.Size(172, 62)
        Me.stbRiskFactors.TabIndex = 1
        '
        'stbRecommendations
        '
        Me.stbRecommendations.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbRecommendations.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbRecommendations, "Recommendations")
        Me.stbRecommendations.EntryErrorMSG = ""
        Me.stbRecommendations.Location = New System.Drawing.Point(127, 93)
        Me.stbRecommendations.Multiline = True
        Me.stbRecommendations.Name = "stbRecommendations"
        Me.stbRecommendations.RegularExpression = ""
        Me.stbRecommendations.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.stbRecommendations.Size = New System.Drawing.Size(172, 59)
        Me.stbRecommendations.TabIndex = 3
        '
        'cboPresentationID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboPresentationID, "Presentation,PresentationID")
        Me.cboPresentationID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPresentationID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboPresentationID.Location = New System.Drawing.Point(162, 53)
        Me.cboPresentationID.Name = "cboPresentationID"
        Me.cboPresentationID.Size = New System.Drawing.Size(170, 21)
        Me.cboPresentationID.TabIndex = 5
        '
        'cboUterusID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboUterusID, "Uterus,UterusID")
        Me.cboUterusID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUterusID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboUterusID.Location = New System.Drawing.Point(118, 116)
        Me.cboUterusID.Name = "cboUterusID"
        Me.cboUterusID.Size = New System.Drawing.Size(170, 21)
        Me.cboUterusID.TabIndex = 9
        '
        'cboDoctorID
        '
        Me.cboDoctorID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDoctorID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboDoctorID.Location = New System.Drawing.Point(147, 138)
        Me.cboDoctorID.Name = "cboDoctorID"
        Me.cboDoctorID.Size = New System.Drawing.Size(170, 21)
        Me.cboDoctorID.TabIndex = 14
        '
        'lblVisitNo
        '
        Me.lblVisitNo.Location = New System.Drawing.Point(9, 31)
        Me.lblVisitNo.Name = "lblVisitNo"
        Me.lblVisitNo.Size = New System.Drawing.Size(83, 20)
        Me.lblVisitNo.TabIndex = 1
        Me.lblVisitNo.Text = "Visit No"
        '
        'lblPallorID
        '
        Me.lblPallorID.Location = New System.Drawing.Point(6, 22)
        Me.lblPallorID.Name = "lblPallorID"
        Me.lblPallorID.Size = New System.Drawing.Size(124, 20)
        Me.lblPallorID.TabIndex = 0
        Me.lblPallorID.Text = "Pallor"
        '
        'lblJaundiceID
        '
        Me.lblJaundiceID.Location = New System.Drawing.Point(6, 45)
        Me.lblJaundiceID.Name = "lblJaundiceID"
        Me.lblJaundiceID.Size = New System.Drawing.Size(124, 20)
        Me.lblJaundiceID.TabIndex = 2
        Me.lblJaundiceID.Text = "Jaundice"
        '
        'lblLynphadenopathyID
        '
        Me.lblLynphadenopathyID.Location = New System.Drawing.Point(6, 68)
        Me.lblLynphadenopathyID.Name = "lblLynphadenopathyID"
        Me.lblLynphadenopathyID.Size = New System.Drawing.Size(124, 20)
        Me.lblLynphadenopathyID.TabIndex = 4
        Me.lblLynphadenopathyID.Text = "Lynphadenopathy"
        '
        'lblVaricoseID
        '
        Me.lblVaricoseID.Location = New System.Drawing.Point(6, 91)
        Me.lblVaricoseID.Name = "lblVaricoseID"
        Me.lblVaricoseID.Size = New System.Drawing.Size(124, 20)
        Me.lblVaricoseID.TabIndex = 6
        Me.lblVaricoseID.Text = "Varicose"
        '
        'lblOedemaID
        '
        Me.lblOedemaID.Location = New System.Drawing.Point(6, 114)
        Me.lblOedemaID.Name = "lblOedemaID"
        Me.lblOedemaID.Size = New System.Drawing.Size(124, 20)
        Me.lblOedemaID.TabIndex = 8
        Me.lblOedemaID.Text = "Oedema"
        '
        'lblHeartSoundID
        '
        Me.lblHeartSoundID.Location = New System.Drawing.Point(7, 16)
        Me.lblHeartSoundID.Name = "lblHeartSoundID"
        Me.lblHeartSoundID.Size = New System.Drawing.Size(124, 20)
        Me.lblHeartSoundID.TabIndex = 0
        Me.lblHeartSoundID.Text = "Heart Sound"
        '
        'lblAirEntryID
        '
        Me.lblAirEntryID.Location = New System.Drawing.Point(7, 39)
        Me.lblAirEntryID.Name = "lblAirEntryID"
        Me.lblAirEntryID.Size = New System.Drawing.Size(124, 20)
        Me.lblAirEntryID.TabIndex = 2
        Me.lblAirEntryID.Text = "Air Entry"
        '
        'lblBreastID
        '
        Me.lblBreastID.Location = New System.Drawing.Point(7, 62)
        Me.lblBreastID.Name = "lblBreastID"
        Me.lblBreastID.Size = New System.Drawing.Size(124, 20)
        Me.lblBreastID.TabIndex = 4
        Me.lblBreastID.Text = "Breast"
        '
        'lblLiverID
        '
        Me.lblLiverID.Location = New System.Drawing.Point(9, 15)
        Me.lblLiverID.Name = "lblLiverID"
        Me.lblLiverID.Size = New System.Drawing.Size(124, 20)
        Me.lblLiverID.TabIndex = 0
        Me.lblLiverID.Text = "Liver"
        '
        'lblSpleenID
        '
        Me.lblSpleenID.Location = New System.Drawing.Point(9, 37)
        Me.lblSpleenID.Name = "lblSpleenID"
        Me.lblSpleenID.Size = New System.Drawing.Size(124, 20)
        Me.lblSpleenID.TabIndex = 2
        Me.lblSpleenID.Text = "Spleen"
        '
        'lblBowelSoundsID
        '
        Me.lblBowelSoundsID.Location = New System.Drawing.Point(9, 60)
        Me.lblBowelSoundsID.Name = "lblBowelSoundsID"
        Me.lblBowelSoundsID.Size = New System.Drawing.Size(124, 20)
        Me.lblBowelSoundsID.TabIndex = 4
        Me.lblBowelSoundsID.Text = "Bowel Sounds"
        '
        'lblScarID
        '
        Me.lblScarID.Location = New System.Drawing.Point(10, 86)
        Me.lblScarID.Name = "lblScarID"
        Me.lblScarID.Size = New System.Drawing.Size(124, 20)
        Me.lblScarID.TabIndex = 6
        Me.lblScarID.Text = "Scar"
        '
        'lblPupilReactionID
        '
        Me.lblPupilReactionID.Location = New System.Drawing.Point(7, 15)
        Me.lblPupilReactionID.Name = "lblPupilReactionID"
        Me.lblPupilReactionID.Size = New System.Drawing.Size(124, 20)
        Me.lblPupilReactionID.TabIndex = 0
        Me.lblPupilReactionID.Text = "Pupil Reaction"
        '
        'lblReflexesID
        '
        Me.lblReflexesID.Location = New System.Drawing.Point(7, 38)
        Me.lblReflexesID.Name = "lblReflexesID"
        Me.lblReflexesID.Size = New System.Drawing.Size(124, 20)
        Me.lblReflexesID.TabIndex = 2
        Me.lblReflexesID.Text = "Reflexes"
        '
        'lblOtherSTIID
        '
        Me.lblOtherSTIID.Location = New System.Drawing.Point(6, 23)
        Me.lblOtherSTIID.Name = "lblOtherSTIID"
        Me.lblOtherSTIID.Size = New System.Drawing.Size(124, 20)
        Me.lblOtherSTIID.TabIndex = 0
        Me.lblOtherSTIID.Text = "Other STIs"
        '
        'lblSTIDetails
        '
        Me.lblSTIDetails.Location = New System.Drawing.Point(6, 46)
        Me.lblSTIDetails.Name = "lblSTIDetails"
        Me.lblSTIDetails.Size = New System.Drawing.Size(124, 20)
        Me.lblSTIDetails.TabIndex = 2
        Me.lblSTIDetails.Text = "STI Details"
        '
        'lblVulvaID
        '
        Me.lblVulvaID.Location = New System.Drawing.Point(11, 27)
        Me.lblVulvaID.Name = "lblVulvaID"
        Me.lblVulvaID.Size = New System.Drawing.Size(143, 20)
        Me.lblVulvaID.TabIndex = 0
        Me.lblVulvaID.Text = "Vulva"
        '
        'lblCervixID
        '
        Me.lblCervixID.Location = New System.Drawing.Point(11, 50)
        Me.lblCervixID.Name = "lblCervixID"
        Me.lblCervixID.Size = New System.Drawing.Size(143, 20)
        Me.lblCervixID.TabIndex = 2
        Me.lblCervixID.Text = "Cervix"
        '
        'lblAdnexaID
        '
        Me.lblAdnexaID.Location = New System.Drawing.Point(11, 73)
        Me.lblAdnexaID.Name = "lblAdnexaID"
        Me.lblAdnexaID.Size = New System.Drawing.Size(143, 20)
        Me.lblAdnexaID.TabIndex = 4
        Me.lblAdnexaID.Text = "Adnexa"
        '
        'lblVaginaID
        '
        Me.lblVaginaID.Location = New System.Drawing.Point(11, 96)
        Me.lblVaginaID.Name = "lblVaginaID"
        Me.lblVaginaID.Size = New System.Drawing.Size(143, 20)
        Me.lblVaginaID.TabIndex = 6
        Me.lblVaginaID.Text = "Vagina"
        '
        'lblUterusID
        '
        Me.lblUterusID.Location = New System.Drawing.Point(11, 119)
        Me.lblUterusID.Name = "lblUterusID"
        Me.lblUterusID.Size = New System.Drawing.Size(143, 20)
        Me.lblUterusID.TabIndex = 8
        Me.lblUterusID.Text = "Uterus"
        '
        'lblAnenorrheaWeeks
        '
        Me.lblAnenorrheaWeeks.Location = New System.Drawing.Point(9, 9)
        Me.lblAnenorrheaWeeks.Name = "lblAnenorrheaWeeks"
        Me.lblAnenorrheaWeeks.Size = New System.Drawing.Size(143, 20)
        Me.lblAnenorrheaWeeks.TabIndex = 0
        Me.lblAnenorrheaWeeks.Text = "Weeks of Anenorrhea "
        '
        'lblFundalHeight
        '
        Me.lblFundalHeight.Location = New System.Drawing.Point(9, 32)
        Me.lblFundalHeight.Name = "lblFundalHeight"
        Me.lblFundalHeight.Size = New System.Drawing.Size(143, 20)
        Me.lblFundalHeight.TabIndex = 2
        Me.lblFundalHeight.Text = "Fundal Height"
        '
        'lblPresentation
        '
        Me.lblPresentation.Location = New System.Drawing.Point(9, 55)
        Me.lblPresentation.Name = "lblPresentation"
        Me.lblPresentation.Size = New System.Drawing.Size(143, 20)
        Me.lblPresentation.TabIndex = 4
        Me.lblPresentation.Text = "Presentation"
        '
        'lblLie
        '
        Me.lblLie.Location = New System.Drawing.Point(9, 77)
        Me.lblLie.Name = "lblLie"
        Me.lblLie.Size = New System.Drawing.Size(143, 20)
        Me.lblLie.TabIndex = 6
        Me.lblLie.Text = "Lie"
        '
        'lblRelationPPOrBrim
        '
        Me.lblRelationPPOrBrim.Location = New System.Drawing.Point(9, 127)
        Me.lblRelationPPOrBrim.Name = "lblRelationPPOrBrim"
        Me.lblRelationPPOrBrim.Size = New System.Drawing.Size(143, 20)
        Me.lblRelationPPOrBrim.TabIndex = 10
        Me.lblRelationPPOrBrim.Text = "RelationPP Or Brim"
        '
        'lblFoetalHeart
        '
        Me.lblFoetalHeart.Location = New System.Drawing.Point(9, 150)
        Me.lblFoetalHeart.Name = "lblFoetalHeart"
        Me.lblFoetalHeart.Size = New System.Drawing.Size(143, 20)
        Me.lblFoetalHeart.TabIndex = 12
        Me.lblFoetalHeart.Text = "Foetal Heart"
        '
        'lblIPT
        '
        Me.lblIPT.Location = New System.Drawing.Point(385, 6)
        Me.lblIPT.Name = "lblIPT"
        Me.lblIPT.Size = New System.Drawing.Size(136, 20)
        Me.lblIPT.TabIndex = 14
        Me.lblIPT.Text = "IPT"
        '
        'lblNetUse
        '
        Me.lblNetUse.Location = New System.Drawing.Point(385, 28)
        Me.lblNetUse.Name = "lblNetUse"
        Me.lblNetUse.Size = New System.Drawing.Size(158, 20)
        Me.lblNetUse.TabIndex = 16
        Me.lblNetUse.Text = "Net Use (Mosquito Net)"
        '
        'lblRemarks
        '
        Me.lblRemarks.Location = New System.Drawing.Point(385, 52)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(158, 20)
        Me.lblRemarks.TabIndex = 18
        Me.lblRemarks.Text = "Remarks"
        '
        'lblReturnDate
        '
        Me.lblReturnDate.Location = New System.Drawing.Point(328, 118)
        Me.lblReturnDate.Name = "lblReturnDate"
        Me.lblReturnDate.Size = New System.Drawing.Size(105, 20)
        Me.lblReturnDate.TabIndex = 25
        Me.lblReturnDate.Text = "Return Date"
        '
        'lblExaminerSpecialityID
        '
        Me.lblExaminerSpecialityID.Location = New System.Drawing.Point(9, 115)
        Me.lblExaminerSpecialityID.Name = "lblExaminerSpecialityID"
        Me.lblExaminerSpecialityID.Size = New System.Drawing.Size(116, 20)
        Me.lblExaminerSpecialityID.TabIndex = 11
        Me.lblExaminerSpecialityID.Text = "Doctor Speciality"
        '
        'lblDoctorID
        '
        Me.lblDoctorID.Location = New System.Drawing.Point(9, 138)
        Me.lblDoctorID.Name = "lblDoctorID"
        Me.lblDoctorID.Size = New System.Drawing.Size(116, 20)
        Me.lblDoctorID.TabIndex = 13
        Me.lblDoctorID.Text = "Doctor"
        '
        'grpAbdomen
        '
        Me.grpAbdomen.Controls.Add(Me.cboBowelSoundsID)
        Me.grpAbdomen.Controls.Add(Me.lblBowelSoundsID)
        Me.grpAbdomen.Controls.Add(Me.lblSpleenID)
        Me.grpAbdomen.Controls.Add(Me.cboSpleenID)
        Me.grpAbdomen.Controls.Add(Me.lblLiverID)
        Me.grpAbdomen.Controls.Add(Me.cboLiverID)
        Me.grpAbdomen.Controls.Add(Me.lblScarID)
        Me.grpAbdomen.Controls.Add(Me.cboScarID)
        Me.grpAbdomen.Location = New System.Drawing.Point(305, 95)
        Me.grpAbdomen.Name = "grpAbdomen"
        Me.grpAbdomen.Size = New System.Drawing.Size(306, 107)
        Me.grpAbdomen.TabIndex = 2
        Me.grpAbdomen.TabStop = False
        Me.grpAbdomen.Text = "ABDOMEN"
        '
        'grpCNS
        '
        Me.grpCNS.Controls.Add(Me.cboReflexesID)
        Me.grpCNS.Controls.Add(Me.lblReflexesID)
        Me.grpCNS.Controls.Add(Me.lblPupilReactionID)
        Me.grpCNS.Controls.Add(Me.cboPupilReactionID)
        Me.grpCNS.Location = New System.Drawing.Point(653, 6)
        Me.grpCNS.Name = "grpCNS"
        Me.grpCNS.Size = New System.Drawing.Size(307, 66)
        Me.grpCNS.TabIndex = 3
        Me.grpCNS.TabStop = False
        Me.grpCNS.Text = "CNS"
        '
        'grpChest
        '
        Me.grpChest.Controls.Add(Me.cboBreastID)
        Me.grpChest.Controls.Add(Me.lblBreastID)
        Me.grpChest.Controls.Add(Me.lblAirEntryID)
        Me.grpChest.Controls.Add(Me.cboAirEntryID)
        Me.grpChest.Controls.Add(Me.lblHeartSoundID)
        Me.grpChest.Controls.Add(Me.cboHeartSoundID)
        Me.grpChest.Location = New System.Drawing.Point(305, 6)
        Me.grpChest.Name = "grpChest"
        Me.grpChest.Size = New System.Drawing.Size(306, 85)
        Me.grpChest.TabIndex = 1
        Me.grpChest.TabStop = False
        Me.grpChest.Text = "CHEST"
        '
        'grpExamination
        '
        Me.grpExamination.Controls.Add(Me.cboUterusID)
        Me.grpExamination.Controls.Add(Me.cboVaginaID)
        Me.grpExamination.Controls.Add(Me.cboAdnexaID)
        Me.grpExamination.Controls.Add(Me.lblUterusID)
        Me.grpExamination.Controls.Add(Me.cboVulvaID)
        Me.grpExamination.Controls.Add(Me.cboCervixID)
        Me.grpExamination.Controls.Add(Me.lblAdnexaID)
        Me.grpExamination.Controls.Add(Me.lblCervixID)
        Me.grpExamination.Controls.Add(Me.lblVulvaID)
        Me.grpExamination.Controls.Add(Me.lblVaginaID)
        Me.grpExamination.Location = New System.Drawing.Point(7, 3)
        Me.grpExamination.Name = "grpExamination"
        Me.grpExamination.Size = New System.Drawing.Size(294, 171)
        Me.grpExamination.TabIndex = 0
        Me.grpExamination.TabStop = False
        Me.grpExamination.Text = "EXAMINATION"
        '
        'grpSTIs
        '
        Me.grpSTIs.Controls.Add(Me.stbSTIDetails)
        Me.grpSTIs.Controls.Add(Me.lblSTIDetails)
        Me.grpSTIs.Controls.Add(Me.lblOtherSTIID)
        Me.grpSTIs.Controls.Add(Me.cboOtherSTIID)
        Me.grpSTIs.Location = New System.Drawing.Point(654, 75)
        Me.grpSTIs.Name = "grpSTIs"
        Me.grpSTIs.Size = New System.Drawing.Size(306, 88)
        Me.grpSTIs.TabIndex = 4
        Me.grpSTIs.TabStop = False
        Me.grpSTIs.Text = "STDs"
        '
        'grpGeneral
        '
        Me.grpGeneral.Controls.Add(Me.cboSkeletalDeformityID)
        Me.grpGeneral.Controls.Add(Me.lblSkeletalDeformityID)
        Me.grpGeneral.Controls.Add(Me.cboVaricoseID)
        Me.grpGeneral.Controls.Add(Me.lblOedemaID)
        Me.grpGeneral.Controls.Add(Me.cboOedemaID)
        Me.grpGeneral.Controls.Add(Me.lblVaricoseID)
        Me.grpGeneral.Controls.Add(Me.lblLynphadenopathyID)
        Me.grpGeneral.Controls.Add(Me.cboLynphadenopathyID)
        Me.grpGeneral.Controls.Add(Me.lblJaundiceID)
        Me.grpGeneral.Controls.Add(Me.cboJaundiceID)
        Me.grpGeneral.Controls.Add(Me.lblPallorID)
        Me.grpGeneral.Controls.Add(Me.cboPallorID)
        Me.grpGeneral.Location = New System.Drawing.Point(6, 6)
        Me.grpGeneral.Name = "grpGeneral"
        Me.grpGeneral.Size = New System.Drawing.Size(283, 198)
        Me.grpGeneral.TabIndex = 0
        Me.grpGeneral.TabStop = False
        Me.grpGeneral.Text = "GENERAL"
        '
        'lblSkeletalDeformityID
        '
        Me.lblSkeletalDeformityID.Location = New System.Drawing.Point(6, 139)
        Me.lblSkeletalDeformityID.Name = "lblSkeletalDeformityID"
        Me.lblSkeletalDeformityID.Size = New System.Drawing.Size(124, 20)
        Me.lblSkeletalDeformityID.TabIndex = 10
        Me.lblSkeletalDeformityID.Text = "Skeletal Deformity"
        '
        'tbcPhysicalExamination
        '
        Me.tbcPhysicalExamination.Controls.Add(Me.tpgANCVitals)
        Me.tbcPhysicalExamination.Controls.Add(Me.tpgPhysicalExamination)
        Me.tbcPhysicalExamination.Controls.Add(Me.tpgFoetalExamination)
        Me.tbcPhysicalExamination.Controls.Add(Me.tpgPelvicExamination)
        Me.tbcPhysicalExamination.Controls.Add(Me.tpgANCAllergies)
        Me.tbcPhysicalExamination.Location = New System.Drawing.Point(12, 199)
        Me.tbcPhysicalExamination.Name = "tbcPhysicalExamination"
        Me.tbcPhysicalExamination.SelectedIndex = 0
        Me.tbcPhysicalExamination.Size = New System.Drawing.Size(986, 242)
        Me.tbcPhysicalExamination.TabIndex = 0
        '
        'tpgANCVitals
        '
        Me.tpgANCVitals.Controls.Add(Me.nbxHeartRate)
        Me.tpgANCVitals.Controls.Add(Me.lblHeartRate)
        Me.tpgANCVitals.Controls.Add(Me.Label2)
        Me.tpgANCVitals.Controls.Add(Me.stbNotes)
        Me.tpgANCVitals.Controls.Add(Me.nbxBMI)
        Me.tpgANCVitals.Controls.Add(Me.lblBMI)
        Me.tpgANCVitals.Controls.Add(Me.btnTriage)
        Me.tpgANCVitals.Controls.Add(Me.stbBMIStatus)
        Me.tpgANCVitals.Controls.Add(Me.lblBMIStatus)
        Me.tpgANCVitals.Controls.Add(Me.lblOxygenSaturation)
        Me.tpgANCVitals.Controls.Add(Me.nbxOxygenSaturation)
        Me.tpgANCVitals.Controls.Add(Me.nbxRespirationRate)
        Me.tpgANCVitals.Controls.Add(Me.lblRespirationRate)
        Me.tpgANCVitals.Controls.Add(Me.nbxMUAC)
        Me.tpgANCVitals.Controls.Add(Me.lblHeight)
        Me.tpgANCVitals.Controls.Add(Me.lblMUAC)
        Me.tpgANCVitals.Controls.Add(Me.nbxTemperature)
        Me.tpgANCVitals.Controls.Add(Me.lblTemperature)
        Me.tpgANCVitals.Controls.Add(Me.lblWeight)
        Me.tpgANCVitals.Controls.Add(Me.nbxPulse)
        Me.tpgANCVitals.Controls.Add(Me.nbxHeight)
        Me.tpgANCVitals.Controls.Add(Me.lblPulse)
        Me.tpgANCVitals.Controls.Add(Me.nbxWeight)
        Me.tpgANCVitals.Controls.Add(Me.lblBloodPressure)
        Me.tpgANCVitals.Controls.Add(Me.stbBloodPressure)
        Me.tpgANCVitals.Location = New System.Drawing.Point(4, 22)
        Me.tpgANCVitals.Name = "tpgANCVitals"
        Me.tpgANCVitals.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgANCVitals.Size = New System.Drawing.Size(1025, 247)
        Me.tpgANCVitals.TabIndex = 3
        Me.tpgANCVitals.Text = "Triage"
        Me.tpgANCVitals.UseVisualStyleBackColor = True
        '
        'nbxHeartRate
        '
        Me.nbxHeartRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxHeartRate.ControlCaption = "Heart Rate"
        Me.nbxHeartRate.DataType = SyncSoft.Common.Win.Controls.Number.[Short]
        Me.nbxHeartRate.DecimalPlaces = -1
        Me.nbxHeartRate.Enabled = False
        Me.nbxHeartRate.Location = New System.Drawing.Point(401, 5)
        Me.nbxHeartRate.MaxLength = 3
        Me.nbxHeartRate.MaxValue = 250.0R
        Me.nbxHeartRate.MinValue = 0.0R
        Me.nbxHeartRate.MustEnterNumeric = True
        Me.nbxHeartRate.Name = "nbxHeartRate"
        Me.nbxHeartRate.Size = New System.Drawing.Size(157, 20)
        Me.nbxHeartRate.TabIndex = 19
        Me.nbxHeartRate.Value = ""
        '
        'lblHeartRate
        '
        Me.lblHeartRate.Location = New System.Drawing.Point(261, 5)
        Me.lblHeartRate.Name = "lblHeartRate"
        Me.lblHeartRate.Size = New System.Drawing.Size(134, 21)
        Me.lblHeartRate.TabIndex = 18
        Me.lblHeartRate.Text = "Heart Rate (B/min)"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(262, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(133, 21)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "Notes"
        '
        'stbNotes
        '
        Me.stbNotes.AcceptsReturn = True
        Me.stbNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbNotes.CapitalizeFirstLetter = True
        Me.stbNotes.EntryErrorMSG = ""
        Me.stbNotes.Location = New System.Drawing.Point(401, 48)
        Me.stbNotes.MaxLength = 2000
        Me.stbNotes.Multiline = True
        Me.stbNotes.Name = "stbNotes"
        Me.stbNotes.ReadOnly = True
        Me.stbNotes.RegularExpression = ""
        Me.stbNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbNotes.Size = New System.Drawing.Size(326, 151)
        Me.stbNotes.TabIndex = 23
        '
        'nbxBMI
        '
        Me.nbxBMI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxBMI.ControlCaption = "BMI"
        Me.nbxBMI.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
        Me.nbxBMI.DecimalPlaces = 2
        Me.nbxBMI.Enabled = False
        Me.nbxBMI.Location = New System.Drawing.Point(401, 26)
        Me.nbxBMI.MaxLength = 12
        Me.nbxBMI.MaxValue = 0.0R
        Me.nbxBMI.MinValue = 0.0R
        Me.nbxBMI.MustEnterNumeric = True
        Me.nbxBMI.Name = "nbxBMI"
        Me.nbxBMI.Size = New System.Drawing.Size(157, 20)
        Me.nbxBMI.TabIndex = 21
        Me.nbxBMI.Value = ""
        '
        'lblBMI
        '
        Me.lblBMI.Location = New System.Drawing.Point(261, 26)
        Me.lblBMI.Name = "lblBMI"
        Me.lblBMI.Size = New System.Drawing.Size(134, 21)
        Me.lblBMI.TabIndex = 20
        Me.lblBMI.Text = "BMI (Kg/M�)"
        '
        'btnTriage
        '
        Me.btnTriage.Enabled = False
        Me.btnTriage.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnTriage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTriage.Location = New System.Drawing.Point(733, 177)
        Me.btnTriage.Name = "btnTriage"
        Me.btnTriage.Size = New System.Drawing.Size(131, 23)
        Me.btnTriage.TabIndex = 24
        Me.btnTriage.Tag = "Triage"
        Me.btnTriage.Text = "Register Triage..."
        Me.btnTriage.UseVisualStyleBackColor = True
        '
        'stbBMIStatus
        '
        Me.stbBMIStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBMIStatus.CapitalizeFirstLetter = False
        Me.stbBMIStatus.Enabled = False
        Me.stbBMIStatus.EntryErrorMSG = ""
        Me.stbBMIStatus.Location = New System.Drawing.Point(146, 176)
        Me.stbBMIStatus.MaxLength = 7
        Me.stbBMIStatus.Name = "stbBMIStatus"
        Me.stbBMIStatus.RegularExpression = ""
        Me.stbBMIStatus.Size = New System.Drawing.Size(83, 20)
        Me.stbBMIStatus.TabIndex = 17
        '
        'lblBMIStatus
        '
        Me.lblBMIStatus.Location = New System.Drawing.Point(6, 175)
        Me.lblBMIStatus.Name = "lblBMIStatus"
        Me.lblBMIStatus.Size = New System.Drawing.Size(134, 21)
        Me.lblBMIStatus.TabIndex = 16
        Me.lblBMIStatus.Text = "BMI Status"
        '
        'lblOxygenSaturation
        '
        Me.lblOxygenSaturation.Location = New System.Drawing.Point(6, 155)
        Me.lblOxygenSaturation.Name = "lblOxygenSaturation"
        Me.lblOxygenSaturation.Size = New System.Drawing.Size(134, 21)
        Me.lblOxygenSaturation.TabIndex = 14
        Me.lblOxygenSaturation.Text = "Oxygen Saturation (%)"
        '
        'nbxOxygenSaturation
        '
        Me.nbxOxygenSaturation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxOxygenSaturation.ControlCaption = "Oxygen Saturation"
        Me.nbxOxygenSaturation.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
        Me.nbxOxygenSaturation.DecimalPlaces = 2
        Me.nbxOxygenSaturation.Enabled = False
        Me.nbxOxygenSaturation.Location = New System.Drawing.Point(146, 155)
        Me.nbxOxygenSaturation.MaxLength = 8
        Me.nbxOxygenSaturation.MaxValue = 100.0R
        Me.nbxOxygenSaturation.MinValue = 0.0R
        Me.nbxOxygenSaturation.Name = "nbxOxygenSaturation"
        Me.nbxOxygenSaturation.Size = New System.Drawing.Size(83, 20)
        Me.nbxOxygenSaturation.TabIndex = 15
        Me.nbxOxygenSaturation.Value = ""
        '
        'nbxRespirationRate
        '
        Me.nbxRespirationRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxRespirationRate.ControlCaption = "Respiration Rate"
        Me.nbxRespirationRate.DataType = SyncSoft.Common.Win.Controls.Number.[Short]
        Me.nbxRespirationRate.DecimalPlaces = -1
        Me.nbxRespirationRate.Enabled = False
        Me.nbxRespirationRate.Location = New System.Drawing.Point(146, 134)
        Me.nbxRespirationRate.MaxLength = 3
        Me.nbxRespirationRate.MaxValue = 150.0R
        Me.nbxRespirationRate.MinValue = 10.0R
        Me.nbxRespirationRate.MustEnterNumeric = True
        Me.nbxRespirationRate.Name = "nbxRespirationRate"
        Me.nbxRespirationRate.Size = New System.Drawing.Size(83, 20)
        Me.nbxRespirationRate.TabIndex = 13
        Me.nbxRespirationRate.Value = ""
        '
        'lblRespirationRate
        '
        Me.lblRespirationRate.Location = New System.Drawing.Point(6, 134)
        Me.lblRespirationRate.Name = "lblRespirationRate"
        Me.lblRespirationRate.Size = New System.Drawing.Size(134, 21)
        Me.lblRespirationRate.TabIndex = 12
        Me.lblRespirationRate.Text = "Respiration Rate (B/min)"
        '
        'nbxMUAC
        '
        Me.nbxMUAC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxMUAC.ControlCaption = "Pulse"
        Me.nbxMUAC.DataType = SyncSoft.Common.Win.Controls.Number.[Short]
        Me.nbxMUAC.DecimalPlaces = -1
        Me.nbxMUAC.Enabled = False
        Me.nbxMUAC.Location = New System.Drawing.Point(146, 70)
        Me.nbxMUAC.MaxLength = 3
        Me.nbxMUAC.MaxValue = 250.0R
        Me.nbxMUAC.MinValue = 50.0R
        Me.nbxMUAC.Name = "nbxMUAC"
        Me.nbxMUAC.Size = New System.Drawing.Size(83, 20)
        Me.nbxMUAC.TabIndex = 7
        Me.nbxMUAC.Value = ""
        '
        'lblHeight
        '
        Me.lblHeight.Location = New System.Drawing.Point(6, 49)
        Me.lblHeight.Name = "lblHeight"
        Me.lblHeight.Size = New System.Drawing.Size(134, 21)
        Me.lblHeight.TabIndex = 4
        Me.lblHeight.Text = "Height (cm)"
        '
        'lblMUAC
        '
        Me.lblMUAC.Location = New System.Drawing.Point(6, 70)
        Me.lblMUAC.Name = "lblMUAC"
        Me.lblMUAC.Size = New System.Drawing.Size(134, 21)
        Me.lblMUAC.TabIndex = 6
        Me.lblMUAC.Text = "MUAC"
        '
        'nbxTemperature
        '
        Me.nbxTemperature.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxTemperature.ControlCaption = "Temperature"
        Me.nbxTemperature.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
        Me.nbxTemperature.DecimalPlaces = 2
        Me.nbxTemperature.Enabled = False
        Me.nbxTemperature.Location = New System.Drawing.Point(146, 28)
        Me.nbxTemperature.MaxLength = 5
        Me.nbxTemperature.MaxValue = 45.0R
        Me.nbxTemperature.MinValue = 30.0R
        Me.nbxTemperature.Name = "nbxTemperature"
        Me.nbxTemperature.Size = New System.Drawing.Size(83, 20)
        Me.nbxTemperature.TabIndex = 3
        Me.nbxTemperature.Value = ""
        '
        'lblTemperature
        '
        Me.lblTemperature.Location = New System.Drawing.Point(6, 28)
        Me.lblTemperature.Name = "lblTemperature"
        Me.lblTemperature.Size = New System.Drawing.Size(134, 21)
        Me.lblTemperature.TabIndex = 2
        Me.lblTemperature.Text = "Temperature (Celc.)"
        '
        'lblWeight
        '
        Me.lblWeight.Location = New System.Drawing.Point(6, 7)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(134, 21)
        Me.lblWeight.TabIndex = 0
        Me.lblWeight.Text = "Weight (Kg)"
        '
        'nbxPulse
        '
        Me.nbxPulse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxPulse.ControlCaption = "Pulse"
        Me.nbxPulse.DataType = SyncSoft.Common.Win.Controls.Number.[Short]
        Me.nbxPulse.DecimalPlaces = -1
        Me.nbxPulse.Enabled = False
        Me.nbxPulse.Location = New System.Drawing.Point(146, 92)
        Me.nbxPulse.MaxLength = 3
        Me.nbxPulse.MaxValue = 250.0R
        Me.nbxPulse.MinValue = 50.0R
        Me.nbxPulse.Name = "nbxPulse"
        Me.nbxPulse.Size = New System.Drawing.Size(83, 20)
        Me.nbxPulse.TabIndex = 9
        Me.nbxPulse.Value = ""
        '
        'nbxHeight
        '
        Me.nbxHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxHeight.ControlCaption = "Height"
        Me.nbxHeight.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
        Me.nbxHeight.DecimalPlaces = 2
        Me.nbxHeight.Enabled = False
        Me.nbxHeight.Location = New System.Drawing.Point(146, 49)
        Me.nbxHeight.MaxLength = 6
        Me.nbxHeight.MaxValue = 250.0R
        Me.nbxHeight.MinValue = 50.0R
        Me.nbxHeight.Name = "nbxHeight"
        Me.nbxHeight.Size = New System.Drawing.Size(83, 20)
        Me.nbxHeight.TabIndex = 5
        Me.nbxHeight.Value = ""
        '
        'lblPulse
        '
        Me.lblPulse.Location = New System.Drawing.Point(6, 92)
        Me.lblPulse.Name = "lblPulse"
        Me.lblPulse.Size = New System.Drawing.Size(134, 21)
        Me.lblPulse.TabIndex = 8
        Me.lblPulse.Text = "Pulse (B/min)"
        '
        'nbxWeight
        '
        Me.nbxWeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxWeight.ControlCaption = "Weight"
        Me.nbxWeight.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
        Me.nbxWeight.DecimalPlaces = 2
        Me.nbxWeight.Enabled = False
        Me.nbxWeight.Location = New System.Drawing.Point(146, 7)
        Me.nbxWeight.MaxLength = 6
        Me.nbxWeight.MaxValue = 200.0R
        Me.nbxWeight.MinValue = 1.0R
        Me.nbxWeight.Name = "nbxWeight"
        Me.nbxWeight.Size = New System.Drawing.Size(83, 20)
        Me.nbxWeight.TabIndex = 1
        Me.nbxWeight.Value = ""
        '
        'lblBloodPressure
        '
        Me.lblBloodPressure.Location = New System.Drawing.Point(6, 113)
        Me.lblBloodPressure.Name = "lblBloodPressure"
        Me.lblBloodPressure.Size = New System.Drawing.Size(134, 21)
        Me.lblBloodPressure.TabIndex = 10
        Me.lblBloodPressure.Text = "Blood Pressure (mmHg)"
        '
        'stbBloodPressure
        '
        Me.stbBloodPressure.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBloodPressure.CapitalizeFirstLetter = False
        Me.stbBloodPressure.Enabled = False
        Me.stbBloodPressure.EntryErrorMSG = "Must enter in the form 999/999"
        Me.stbBloodPressure.Location = New System.Drawing.Point(146, 113)
        Me.stbBloodPressure.MaxLength = 7
        Me.stbBloodPressure.Name = "stbBloodPressure"
        Me.stbBloodPressure.RegularExpression = "^[0-9]{1,3}/[0-9]{1,3}$"
        Me.stbBloodPressure.Size = New System.Drawing.Size(83, 20)
        Me.stbBloodPressure.TabIndex = 11
        '
        'tpgPhysicalExamination
        '
        Me.tpgPhysicalExamination.Controls.Add(Me.grpGeneral)
        Me.tpgPhysicalExamination.Controls.Add(Me.grpSTIs)
        Me.tpgPhysicalExamination.Controls.Add(Me.grpAbdomen)
        Me.tpgPhysicalExamination.Controls.Add(Me.grpChest)
        Me.tpgPhysicalExamination.Controls.Add(Me.grpCNS)
        Me.tpgPhysicalExamination.Location = New System.Drawing.Point(4, 22)
        Me.tpgPhysicalExamination.Name = "tpgPhysicalExamination"
        Me.tpgPhysicalExamination.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgPhysicalExamination.Size = New System.Drawing.Size(978, 216)
        Me.tpgPhysicalExamination.TabIndex = 0
        Me.tpgPhysicalExamination.Text = "Physical Examination"
        Me.tpgPhysicalExamination.UseVisualStyleBackColor = True
        '
        'tpgPelvicExamination
        '
        Me.tpgPelvicExamination.Controls.Add(Me.grpPrognosis)
        Me.tpgPelvicExamination.Controls.Add(Me.grpAssessment)
        Me.tpgPelvicExamination.Controls.Add(Me.grpExamination)
        Me.tpgPelvicExamination.Location = New System.Drawing.Point(4, 22)
        Me.tpgPelvicExamination.Name = "tpgPelvicExamination"
        Me.tpgPelvicExamination.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgPelvicExamination.Size = New System.Drawing.Size(978, 216)
        Me.tpgPelvicExamination.TabIndex = 1
        Me.tpgPelvicExamination.Text = "Pelvic Examination"
        Me.tpgPelvicExamination.UseVisualStyleBackColor = True
        '
        'grpPrognosis
        '
        Me.grpPrognosis.Controls.Add(Me.stbRiskFactors)
        Me.grpPrognosis.Controls.Add(Me.lblRiskFactors)
        Me.grpPrognosis.Controls.Add(Me.stbRecommendations)
        Me.grpPrognosis.Controls.Add(Me.lblRecommendations)
        Me.grpPrognosis.Location = New System.Drawing.Point(629, 6)
        Me.grpPrognosis.Name = "grpPrognosis"
        Me.grpPrognosis.Size = New System.Drawing.Size(339, 168)
        Me.grpPrognosis.TabIndex = 2
        Me.grpPrognosis.TabStop = False
        Me.grpPrognosis.Text = "PROGNOSIS / PLAN"
        '
        'lblRiskFactors
        '
        Me.lblRiskFactors.Location = New System.Drawing.Point(8, 20)
        Me.lblRiskFactors.Name = "lblRiskFactors"
        Me.lblRiskFactors.Size = New System.Drawing.Size(135, 20)
        Me.lblRiskFactors.TabIndex = 0
        Me.lblRiskFactors.Text = "Risk Factors"
        '
        'lblRecommendations
        '
        Me.lblRecommendations.Location = New System.Drawing.Point(6, 95)
        Me.lblRecommendations.Name = "lblRecommendations"
        Me.lblRecommendations.Size = New System.Drawing.Size(98, 20)
        Me.lblRecommendations.TabIndex = 2
        Me.lblRecommendations.Text = "Recommendations"
        '
        'grpAssessment
        '
        Me.grpAssessment.Controls.Add(Me.nbxIschialSpine)
        Me.grpAssessment.Controls.Add(Me.lblIschialSpine)
        Me.grpAssessment.Controls.Add(Me.nbxSubPublicAngle)
        Me.grpAssessment.Controls.Add(Me.nbxSacralCurve)
        Me.grpAssessment.Controls.Add(Me.lblSubPublicAngle)
        Me.grpAssessment.Controls.Add(Me.lblSacralCurve)
        Me.grpAssessment.Controls.Add(Me.nbxIschialTuberosities)
        Me.grpAssessment.Controls.Add(Me.nbxDiagonalConjugate)
        Me.grpAssessment.Controls.Add(Me.lblIschialTuberosities)
        Me.grpAssessment.Controls.Add(Me.lblDiagonalConjugate)
        Me.grpAssessment.Controls.Add(Me.cboConclusionID)
        Me.grpAssessment.Controls.Add(Me.Label4)
        Me.grpAssessment.Location = New System.Drawing.Point(306, 6)
        Me.grpAssessment.Name = "grpAssessment"
        Me.grpAssessment.Size = New System.Drawing.Size(320, 168)
        Me.grpAssessment.TabIndex = 1
        Me.grpAssessment.TabStop = False
        Me.grpAssessment.Text = "ASSESSMENT"
        '
        'lblIschialSpine
        '
        Me.lblIschialSpine.Location = New System.Drawing.Point(18, 63)
        Me.lblIschialSpine.Name = "lblIschialSpine"
        Me.lblIschialSpine.Size = New System.Drawing.Size(107, 20)
        Me.lblIschialSpine.TabIndex = 4
        Me.lblIschialSpine.Text = "Ischial Spine"
        '
        'lblSubPublicAngle
        '
        Me.lblSubPublicAngle.Location = New System.Drawing.Point(18, 86)
        Me.lblSubPublicAngle.Name = "lblSubPublicAngle"
        Me.lblSubPublicAngle.Size = New System.Drawing.Size(135, 20)
        Me.lblSubPublicAngle.TabIndex = 6
        Me.lblSubPublicAngle.Text = "Sub Public Angle"
        '
        'lblSacralCurve
        '
        Me.lblSacralCurve.Location = New System.Drawing.Point(18, 41)
        Me.lblSacralCurve.Name = "lblSacralCurve"
        Me.lblSacralCurve.Size = New System.Drawing.Size(101, 20)
        Me.lblSacralCurve.TabIndex = 2
        Me.lblSacralCurve.Text = "Sacral Curve"
        '
        'lblIschialTuberosities
        '
        Me.lblIschialTuberosities.Location = New System.Drawing.Point(18, 109)
        Me.lblIschialTuberosities.Name = "lblIschialTuberosities"
        Me.lblIschialTuberosities.Size = New System.Drawing.Size(135, 20)
        Me.lblIschialTuberosities.TabIndex = 8
        Me.lblIschialTuberosities.Text = "Ischial Tuberosities"
        '
        'lblDiagonalConjugate
        '
        Me.lblDiagonalConjugate.Location = New System.Drawing.Point(18, 19)
        Me.lblDiagonalConjugate.Name = "lblDiagonalConjugate"
        Me.lblDiagonalConjugate.Size = New System.Drawing.Size(124, 20)
        Me.lblDiagonalConjugate.TabIndex = 0
        Me.lblDiagonalConjugate.Text = "Diagonal Conjugate"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(18, 132)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(135, 20)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Pelvic Conclusion"
        '
        'tpgFoetalExamination
        '
        Me.tpgFoetalExamination.Controls.Add(Me.cboPresentationID)
        Me.tpgFoetalExamination.Controls.Add(Me.cboNetUse)
        Me.tpgFoetalExamination.Controls.Add(Me.cboIPT)
        Me.tpgFoetalExamination.Controls.Add(Me.cboPositionID)
        Me.tpgFoetalExamination.Controls.Add(Me.Label1)
        Me.tpgFoetalExamination.Controls.Add(Me.stbFundalHeight)
        Me.tpgFoetalExamination.Controls.Add(Me.lblIPT)
        Me.tpgFoetalExamination.Controls.Add(Me.cboLieID)
        Me.tpgFoetalExamination.Controls.Add(Me.lblNetUse)
        Me.tpgFoetalExamination.Controls.Add(Me.lblLie)
        Me.tpgFoetalExamination.Controls.Add(Me.nbxAnenorrheaWeeks)
        Me.tpgFoetalExamination.Controls.Add(Me.stbRemarks)
        Me.tpgFoetalExamination.Controls.Add(Me.lblRemarks)
        Me.tpgFoetalExamination.Controls.Add(Me.stbRelationPPOrBrim)
        Me.tpgFoetalExamination.Controls.Add(Me.lblPresentation)
        Me.tpgFoetalExamination.Controls.Add(Me.lblRelationPPOrBrim)
        Me.tpgFoetalExamination.Controls.Add(Me.lblAnenorrheaWeeks)
        Me.tpgFoetalExamination.Controls.Add(Me.nbxFoetalHeart)
        Me.tpgFoetalExamination.Controls.Add(Me.lblFoetalHeart)
        Me.tpgFoetalExamination.Controls.Add(Me.lblFundalHeight)
        Me.tpgFoetalExamination.Location = New System.Drawing.Point(4, 22)
        Me.tpgFoetalExamination.Name = "tpgFoetalExamination"
        Me.tpgFoetalExamination.Size = New System.Drawing.Size(978, 216)
        Me.tpgFoetalExamination.TabIndex = 2
        Me.tpgFoetalExamination.Text = "Foetal Examination"
        Me.tpgFoetalExamination.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(9, 104)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(143, 20)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Position"
        '
        'tpgANCAllergies
        '
        Me.tpgANCAllergies.Controls.Add(Me.btnAddAllergy)
        Me.tpgANCAllergies.Controls.Add(Me.dgvPatientAllergies)
        Me.tpgANCAllergies.Location = New System.Drawing.Point(4, 22)
        Me.tpgANCAllergies.Name = "tpgANCAllergies"
        Me.tpgANCAllergies.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgANCAllergies.Size = New System.Drawing.Size(1007, 216)
        Me.tpgANCAllergies.TabIndex = 4
        Me.tpgANCAllergies.Text = "Allergies"
        Me.tpgANCAllergies.UseVisualStyleBackColor = True
        '
        'btnAddAllergy
        '
        Me.btnAddAllergy.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnAddAllergy.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddAllergy.Location = New System.Drawing.Point(609, 6)
        Me.btnAddAllergy.Name = "btnAddAllergy"
        Me.btnAddAllergy.Size = New System.Drawing.Size(123, 23)
        Me.btnAddAllergy.TabIndex = 1
        Me.btnAddAllergy.Tag = ""
        Me.btnAddAllergy.Text = "Add New Allergy"
        '
        'dgvPatientAllergies
        '
        Me.dgvPatientAllergies.AllowUserToOrderColumns = True
        Me.dgvPatientAllergies.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPatientAllergies.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPatientAllergies.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colAllergyNo, Me.colAllergyCategory, Me.colReaction, Me.colPatientAllergiesSaved})
        Me.dgvPatientAllergies.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvPatientAllergies.EnableHeadersVisualStyles = False
        Me.dgvPatientAllergies.GridColor = System.Drawing.Color.Khaki
        Me.dgvPatientAllergies.Location = New System.Drawing.Point(3, 3)
        Me.dgvPatientAllergies.Name = "dgvPatientAllergies"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPatientAllergies.RowHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvPatientAllergies.Size = New System.Drawing.Size(1001, 210)
        Me.dgvPatientAllergies.TabIndex = 0
        Me.dgvPatientAllergies.Text = "DataGridView1"
        '
        'colAllergyNo
        '
        Me.colAllergyNo.DataPropertyName = "AllergyName"
        Me.colAllergyNo.HeaderText = "Allergy"
        Me.colAllergyNo.Name = "colAllergyNo"
        Me.colAllergyNo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colAllergyNo.Width = 250
        '
        'colAllergyCategory
        '
        Me.colAllergyCategory.DataPropertyName = "AllergyCategory"
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Info
        Me.colAllergyCategory.DefaultCellStyle = DataGridViewCellStyle2
        Me.colAllergyCategory.HeaderText = "Category"
        Me.colAllergyCategory.Name = "colAllergyCategory"
        Me.colAllergyCategory.ReadOnly = True
        Me.colAllergyCategory.Width = 80
        '
        'colReaction
        '
        Me.colReaction.DataPropertyName = "Reaction"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.colReaction.DefaultCellStyle = DataGridViewCellStyle3
        Me.colReaction.HeaderText = "Reaction"
        Me.colReaction.MaxInputLength = 200
        Me.colReaction.Name = "colReaction"
        Me.colReaction.Width = 180
        '
        'colPatientAllergiesSaved
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle4.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle4.NullValue = False
        Me.colPatientAllergiesSaved.DefaultCellStyle = DataGridViewCellStyle4
        Me.colPatientAllergiesSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colPatientAllergiesSaved.HeaderText = "Saved"
        Me.colPatientAllergiesSaved.Name = "colPatientAllergiesSaved"
        Me.colPatientAllergiesSaved.ReadOnly = True
        Me.colPatientAllergiesSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colPatientAllergiesSaved.Width = 50
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(329, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 20)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "HIV Status"
        '
        'stbFullName
        '
        Me.stbFullName.BackColor = System.Drawing.SystemColors.Control
        Me.stbFullName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbFullName.Enabled = False
        Me.stbFullName.Location = New System.Drawing.Point(147, 72)
        Me.stbFullName.MaxLength = 41
        Me.stbFullName.Name = "stbFullName"
        Me.stbFullName.Size = New System.Drawing.Size(170, 20)
        Me.stbFullName.TabIndex = 8
        '
        'lblName
        '
        Me.lblName.Location = New System.Drawing.Point(9, 74)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(126, 20)
        Me.lblName.TabIndex = 7
        Me.lblName.Text = "Patient's Name"
        '
        'stbBloodGroup
        '
        Me.stbBloodGroup.BackColor = System.Drawing.SystemColors.Control
        Me.stbBloodGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBloodGroup.Enabled = False
        Me.stbBloodGroup.Location = New System.Drawing.Point(456, 49)
        Me.stbBloodGroup.MaxLength = 60
        Me.stbBloodGroup.Name = "stbBloodGroup"
        Me.stbBloodGroup.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbBloodGroup.Size = New System.Drawing.Size(151, 20)
        Me.stbBloodGroup.TabIndex = 20
        '
        'lblBloodGroup
        '
        Me.lblBloodGroup.Location = New System.Drawing.Point(328, 46)
        Me.lblBloodGroup.Name = "lblBloodGroup"
        Me.lblBloodGroup.Size = New System.Drawing.Size(71, 20)
        Me.lblBloodGroup.TabIndex = 19
        Me.lblBloodGroup.Text = "Blood Group"
        '
        'stbTotalVisits
        '
        Me.stbTotalVisits.BackColor = System.Drawing.SystemColors.Control
        Me.stbTotalVisits.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalVisits.Enabled = False
        Me.stbTotalVisits.Location = New System.Drawing.Point(736, 135)
        Me.stbTotalVisits.MaxLength = 60
        Me.stbTotalVisits.Name = "stbTotalVisits"
        Me.stbTotalVisits.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbTotalVisits.Size = New System.Drawing.Size(130, 20)
        Me.stbTotalVisits.TabIndex = 37
        '
        'lblNoOfVisits
        '
        Me.lblNoOfVisits.Location = New System.Drawing.Point(615, 136)
        Me.lblNoOfVisits.Name = "lblNoOfVisits"
        Me.lblNoOfVisits.Size = New System.Drawing.Size(116, 20)
        Me.lblNoOfVisits.TabIndex = 36
        Me.lblNoOfVisits.Text = "Total Visits"
        '
        'stbPhone
        '
        Me.stbPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPhone.CapitalizeFirstLetter = False
        Me.stbPhone.Enabled = False
        Me.stbPhone.EntryErrorMSG = ""
        Me.stbPhone.Location = New System.Drawing.Point(736, 30)
        Me.stbPhone.MaxLength = 30
        Me.stbPhone.Name = "stbPhone"
        Me.stbPhone.ReadOnly = True
        Me.stbPhone.RegularExpression = ""
        Me.stbPhone.Size = New System.Drawing.Size(130, 20)
        Me.stbPhone.TabIndex = 29
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(615, 32)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(77, 20)
        Me.Label6.TabIndex = 28
        Me.Label6.Text = "Phone No"
        '
        'stbAddress
        '
        Me.stbAddress.BackColor = System.Drawing.SystemColors.Control
        Me.stbAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAddress.CapitalizeFirstLetter = True
        Me.stbAddress.Enabled = False
        Me.stbAddress.EntryErrorMSG = ""
        Me.stbAddress.Location = New System.Drawing.Point(736, 51)
        Me.stbAddress.MaxLength = 41
        Me.stbAddress.Multiline = True
        Me.stbAddress.Name = "stbAddress"
        Me.stbAddress.RegularExpression = ""
        Me.stbAddress.Size = New System.Drawing.Size(130, 41)
        Me.stbAddress.TabIndex = 31
        '
        'lblAddress
        '
        Me.lblAddress.Location = New System.Drawing.Point(615, 62)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(77, 20)
        Me.lblAddress.TabIndex = 30
        Me.lblAddress.Text = "Address"
        '
        'txtVisitDate
        '
        Me.txtVisitDate.BackColor = System.Drawing.SystemColors.Control
        Me.txtVisitDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtVisitDate.Enabled = False
        Me.txtVisitDate.Location = New System.Drawing.Point(736, 114)
        Me.txtVisitDate.MaxLength = 60
        Me.txtVisitDate.Name = "txtVisitDate"
        Me.txtVisitDate.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtVisitDate.Size = New System.Drawing.Size(130, 20)
        Me.txtVisitDate.TabIndex = 35
        '
        'lblVisitDate
        '
        Me.lblVisitDate.Location = New System.Drawing.Point(614, 116)
        Me.lblVisitDate.Name = "lblVisitDate"
        Me.lblVisitDate.Size = New System.Drawing.Size(77, 20)
        Me.lblVisitDate.TabIndex = 34
        Me.lblVisitDate.Text = "Last Visit Date"
        '
        'stbHIVStatus
        '
        Me.stbHIVStatus.BackColor = System.Drawing.SystemColors.Control
        Me.stbHIVStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbHIVStatus.Enabled = False
        Me.stbHIVStatus.Location = New System.Drawing.Point(456, 70)
        Me.stbHIVStatus.MaxLength = 60
        Me.stbHIVStatus.Name = "stbHIVStatus"
        Me.stbHIVStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbHIVStatus.Size = New System.Drawing.Size(151, 20)
        Me.stbHIVStatus.TabIndex = 22
        '
        'stbEDD
        '
        Me.stbEDD.BackColor = System.Drawing.SystemColors.Control
        Me.stbEDD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbEDD.Enabled = False
        Me.stbEDD.Location = New System.Drawing.Point(456, 28)
        Me.stbEDD.MaxLength = 60
        Me.stbEDD.Name = "stbEDD"
        Me.stbEDD.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbEDD.Size = New System.Drawing.Size(151, 20)
        Me.stbEDD.TabIndex = 18
        '
        'lblEDD
        '
        Me.lblEDD.Location = New System.Drawing.Point(329, 28)
        Me.lblEDD.Name = "lblEDD"
        Me.lblEDD.Size = New System.Drawing.Size(71, 20)
        Me.lblEDD.TabIndex = 17
        Me.lblEDD.Text = "EDD"
        '
        'stbANCJoinDate
        '
        Me.stbANCJoinDate.BackColor = System.Drawing.SystemColors.Control
        Me.stbANCJoinDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbANCJoinDate.Enabled = False
        Me.stbANCJoinDate.Location = New System.Drawing.Point(736, 93)
        Me.stbANCJoinDate.MaxLength = 60
        Me.stbANCJoinDate.Name = "stbANCJoinDate"
        Me.stbANCJoinDate.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbANCJoinDate.Size = New System.Drawing.Size(130, 20)
        Me.stbANCJoinDate.TabIndex = 33
        '
        'lblANCJoinDate
        '
        Me.lblANCJoinDate.Location = New System.Drawing.Point(615, 95)
        Me.lblANCJoinDate.Name = "lblANCJoinDate"
        Me.lblANCJoinDate.Size = New System.Drawing.Size(109, 20)
        Me.lblANCJoinDate.TabIndex = 32
        Me.lblANCJoinDate.Text = " Join Date"
        '
        'pnlNavigateANCVisits
        '
        Me.pnlNavigateANCVisits.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.pnlNavigateANCVisits.Controls.Add(Me.chkNavigateVisits)
        Me.pnlNavigateANCVisits.Controls.Add(Me.navVisits)
        Me.pnlNavigateANCVisits.Location = New System.Drawing.Point(147, 447)
        Me.pnlNavigateANCVisits.Name = "pnlNavigateANCVisits"
        Me.pnlNavigateANCVisits.Size = New System.Drawing.Size(633, 39)
        Me.pnlNavigateANCVisits.TabIndex = 41
        Me.pnlNavigateANCVisits.Visible = False
        '
        'chkNavigateVisits
        '
        Me.chkNavigateVisits.AccessibleDescription = ""
        Me.chkNavigateVisits.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkNavigateVisits.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkNavigateVisits.Location = New System.Drawing.Point(8, 9)
        Me.chkNavigateVisits.Name = "chkNavigateVisits"
        Me.chkNavigateVisits.Size = New System.Drawing.Size(170, 20)
        Me.chkNavigateVisits.TabIndex = 0
        Me.chkNavigateVisits.Text = "Navigate ANC Patient Visits"
        '
        'navVisits
        '
        Me.navVisits.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.navVisits.ColumnName = "VisitNo"
        Me.navVisits.DataSource = Nothing
        Me.navVisits.Location = New System.Drawing.Point(207, 4)
        Me.navVisits.Name = "navVisits"
        Me.navVisits.NavAllEnabled = False
        Me.navVisits.NavLeftEnabled = False
        Me.navVisits.NavRightEnabled = False
        Me.navVisits.Size = New System.Drawing.Size(413, 32)
        Me.navVisits.TabIndex = 1
        '
        'stbPartnersHIVStatus
        '
        Me.stbPartnersHIVStatus.BackColor = System.Drawing.SystemColors.Control
        Me.stbPartnersHIVStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPartnersHIVStatus.Enabled = False
        Me.stbPartnersHIVStatus.Location = New System.Drawing.Point(456, 91)
        Me.stbPartnersHIVStatus.MaxLength = 60
        Me.stbPartnersHIVStatus.Name = "stbPartnersHIVStatus"
        Me.stbPartnersHIVStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbPartnersHIVStatus.Size = New System.Drawing.Size(151, 20)
        Me.stbPartnersHIVStatus.TabIndex = 24
        '
        'lblHIVPartnerStatus
        '
        Me.lblHIVPartnerStatus.Location = New System.Drawing.Point(328, 95)
        Me.lblHIVPartnerStatus.Name = "lblHIVPartnerStatus"
        Me.lblHIVPartnerStatus.Size = New System.Drawing.Size(106, 20)
        Me.lblHIVPartnerStatus.TabIndex = 23
        Me.lblHIVPartnerStatus.Text = "Partner's HIV Status"
        '
        'spbPhoto
        '
        Me.spbPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.spbPhoto.Image = CType(resources.GetObject("spbPhoto.Image"), System.Drawing.Image)
        Me.spbPhoto.ImageSizeLimit = CType(200000, Long)
        Me.spbPhoto.InitialImage = CType(resources.GetObject("spbPhoto.InitialImage"), System.Drawing.Image)
        Me.spbPhoto.Location = New System.Drawing.Point(885, 31)
        Me.spbPhoto.Name = "spbPhoto"
        Me.spbPhoto.ReadOnly = True
        Me.spbPhoto.Size = New System.Drawing.Size(108, 111)
        Me.spbPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.spbPhoto.TabIndex = 161
        Me.spbPhoto.TabStop = False
        '
        'lblPhoto
        '
        Me.lblPhoto.Location = New System.Drawing.Point(904, 145)
        Me.lblPhoto.Name = "lblPhoto"
        Me.lblPhoto.Size = New System.Drawing.Size(89, 20)
        Me.lblPhoto.TabIndex = 38
        Me.lblPhoto.Text = "Photo"
        Me.lblPhoto.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'stbVisitNo
        '
        Me.stbVisitNo.BackColor = System.Drawing.SystemColors.Window
        Me.stbVisitNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbVisitNo.CapitalizeFirstLetter = False
        Me.stbVisitNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.stbVisitNo.EntryErrorMSG = ""
        Me.stbVisitNo.Location = New System.Drawing.Point(147, 30)
        Me.stbVisitNo.MaxLength = 20
        Me.stbVisitNo.Name = "stbVisitNo"
        Me.stbVisitNo.RegularExpression = ""
        Me.stbVisitNo.Size = New System.Drawing.Size(115, 20)
        Me.stbVisitNo.TabIndex = 3
        '
        'stbPatientNo
        '
        Me.stbPatientNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPatientNo.CapitalizeFirstLetter = False
        Me.stbPatientNo.EntryErrorMSG = ""
        Me.stbPatientNo.Location = New System.Drawing.Point(147, 93)
        Me.stbPatientNo.Name = "stbPatientNo"
        Me.stbPatientNo.ReadOnly = True
        Me.stbPatientNo.RegularExpression = ""
        Me.stbPatientNo.Size = New System.Drawing.Size(170, 20)
        Me.stbPatientNo.TabIndex = 10
        '
        'lblPatientNo
        '
        Me.lblPatientNo.Location = New System.Drawing.Point(9, 93)
        Me.lblPatientNo.Name = "lblPatientNo"
        Me.lblPatientNo.Size = New System.Drawing.Size(97, 20)
        Me.lblPatientNo.TabIndex = 9
        Me.lblPatientNo.Text = "Patient No"
        '
        'btnFindByFingerprint
        '
        Me.btnFindByFingerprint.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnFindByFingerprint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFindByFingerprint.Location = New System.Drawing.Point(878, 166)
        Me.btnFindByFingerprint.Name = "btnFindByFingerprint"
        Me.btnFindByFingerprint.Size = New System.Drawing.Size(115, 23)
        Me.btnFindByFingerprint.TabIndex = 39
        Me.btnFindByFingerprint.Text = "Find By &Fingerprint"
        Me.btnFindByFingerprint.UseVisualStyleBackColor = True
        '
        'lblNurse
        '
        Me.lblNurse.Location = New System.Drawing.Point(9, 161)
        Me.lblNurse.Name = "lblNurse"
        Me.lblNurse.Size = New System.Drawing.Size(114, 20)
        Me.lblNurse.TabIndex = 15
        Me.lblNurse.Text = "Nurse in charge"
        '
        'stbANCNo
        '
        Me.stbANCNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbANCNo.CapitalizeFirstLetter = False
        Me.stbANCNo.EntryErrorMSG = ""
        Me.stbANCNo.Location = New System.Drawing.Point(147, 51)
        Me.stbANCNo.Name = "stbANCNo"
        Me.stbANCNo.RegularExpression = ""
        Me.stbANCNo.Size = New System.Drawing.Size(170, 20)
        Me.stbANCNo.TabIndex = 6
        '
        'lblANCNo
        '
        Me.lblANCNo.Location = New System.Drawing.Point(9, 54)
        Me.lblANCNo.Name = "lblANCNo"
        Me.lblANCNo.Size = New System.Drawing.Size(97, 20)
        Me.lblANCNo.TabIndex = 5
        Me.lblANCNo.Text = "ANC No"
        '
        'btnLoadSeeANCVisits
        '
        Me.btnLoadSeeANCVisits.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnLoadSeeANCVisits.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoadSeeANCVisits.Location = New System.Drawing.Point(269, 25)
        Me.btnLoadSeeANCVisits.Name = "btnLoadSeeANCVisits"
        Me.btnLoadSeeANCVisits.Size = New System.Drawing.Size(49, 24)
        Me.btnLoadSeeANCVisits.TabIndex = 4
        Me.btnLoadSeeANCVisits.Tag = ""
        Me.btnLoadSeeANCVisits.Text = "&Load"
        '
        'pnlCreateNewRound
        '
        Me.pnlCreateNewRound.Controls.Add(Me.chkCreateNewVisit)
        Me.pnlCreateNewRound.Location = New System.Drawing.Point(16, 0)
        Me.pnlCreateNewRound.Name = "pnlCreateNewRound"
        Me.pnlCreateNewRound.Size = New System.Drawing.Size(169, 29)
        Me.pnlCreateNewRound.TabIndex = 0
        '
        'chkCreateNewVisit
        '
        Me.chkCreateNewVisit.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkCreateNewVisit.Checked = True
        Me.chkCreateNewVisit.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCreateNewVisit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkCreateNewVisit.Location = New System.Drawing.Point(-2, 5)
        Me.chkCreateNewVisit.Name = "chkCreateNewVisit"
        Me.chkCreateNewVisit.Size = New System.Drawing.Size(137, 20)
        Me.chkCreateNewVisit.TabIndex = 0
        Me.chkCreateNewVisit.Text = "Create Visit"
        '
        'btnFindVisitNo
        '
        Me.btnFindVisitNo.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnFindVisitNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFindVisitNo.Image = CType(resources.GetObject("btnFindVisitNo.Image"), System.Drawing.Image)
        Me.btnFindVisitNo.Location = New System.Drawing.Point(114, 31)
        Me.btnFindVisitNo.Name = "btnFindVisitNo"
        Me.btnFindVisitNo.Size = New System.Drawing.Size(27, 21)
        Me.btnFindVisitNo.TabIndex = 2
        '
        'btnClear
        '
        Me.btnClear.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Location = New System.Drawing.Point(844, 474)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(72, 24)
        Me.btnClear.TabIndex = 42
        Me.btnClear.Tag = "AntenatalProgress"
        Me.btnClear.Text = "&Clear"
        Me.btnClear.UseVisualStyleBackColor = False
        Me.btnClear.Visible = False
        '
        'fbnClose
        '
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.fbnClose.Location = New System.Drawing.Point(922, 474)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 44
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'frmAntenatalVisits
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1004, 505)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnFindVisitNo)
        Me.Controls.Add(Me.pnlCreateNewRound)
        Me.Controls.Add(Me.stbANCNo)
        Me.Controls.Add(Me.lblANCNo)
        Me.Controls.Add(Me.btnLoadSeeANCVisits)
        Me.Controls.Add(Me.cboNurseID)
        Me.Controls.Add(Me.lblNurse)
        Me.Controls.Add(Me.btnFindByFingerprint)
        Me.Controls.Add(Me.stbPatientNo)
        Me.Controls.Add(Me.lblPatientNo)
        Me.Controls.Add(Me.stbVisitNo)
        Me.Controls.Add(Me.spbPhoto)
        Me.Controls.Add(Me.lblPhoto)
        Me.Controls.Add(Me.stbPartnersHIVStatus)
        Me.Controls.Add(Me.lblHIVPartnerStatus)
        Me.Controls.Add(Me.pnlNavigateANCVisits)
        Me.Controls.Add(Me.stbANCJoinDate)
        Me.Controls.Add(Me.lblANCJoinDate)
        Me.Controls.Add(Me.stbEDD)
        Me.Controls.Add(Me.lblEDD)
        Me.Controls.Add(Me.stbHIVStatus)
        Me.Controls.Add(Me.stbBloodGroup)
        Me.Controls.Add(Me.lblBloodGroup)
        Me.Controls.Add(Me.stbTotalVisits)
        Me.Controls.Add(Me.lblNoOfVisits)
        Me.Controls.Add(Me.stbPhone)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.stbAddress)
        Me.Controls.Add(Me.lblAddress)
        Me.Controls.Add(Me.txtVisitDate)
        Me.Controls.Add(Me.lblVisitDate)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.stbFullName)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.tbcPhysicalExamination)
        Me.Controls.Add(Me.chkTTGiven)
        Me.Controls.Add(Me.cboDoctorSpecialtyID)
        Me.Controls.Add(Me.lblExaminerSpecialityID)
        Me.Controls.Add(Me.cboDoctorID)
        Me.Controls.Add(Me.lblDoctorID)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.lblVisitNo)
        Me.Controls.Add(Me.lblReturnDate)
        Me.Controls.Add(Me.dtpReturnDate)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmAntenatalVisits"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Antenatal Visits"
        Me.grpAbdomen.ResumeLayout(False)
        Me.grpCNS.ResumeLayout(False)
        Me.grpChest.ResumeLayout(False)
        Me.grpExamination.ResumeLayout(False)
        Me.grpSTIs.ResumeLayout(False)
        Me.grpSTIs.PerformLayout()
        Me.grpGeneral.ResumeLayout(False)
        Me.tbcPhysicalExamination.ResumeLayout(False)
        Me.tpgANCVitals.ResumeLayout(False)
        Me.tpgANCVitals.PerformLayout()
        Me.tpgPhysicalExamination.ResumeLayout(False)
        Me.tpgPelvicExamination.ResumeLayout(False)
        Me.grpPrognosis.ResumeLayout(False)
        Me.grpPrognosis.PerformLayout()
        Me.grpAssessment.ResumeLayout(False)
        Me.grpAssessment.PerformLayout()
        Me.tpgFoetalExamination.ResumeLayout(False)
        Me.tpgFoetalExamination.PerformLayout()
        Me.tpgANCAllergies.ResumeLayout(False)
        CType(Me.dgvPatientAllergies, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlNavigateANCVisits.ResumeLayout(False)
        CType(Me.spbPhoto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCreateNewRound.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
    Friend WithEvents lblVisitNo As System.Windows.Forms.Label
    Friend WithEvents cboPallorID As System.Windows.Forms.ComboBox
    Friend WithEvents lblPallorID As System.Windows.Forms.Label
    Friend WithEvents cboJaundiceID As System.Windows.Forms.ComboBox
    Friend WithEvents lblJaundiceID As System.Windows.Forms.Label
    Friend WithEvents cboLynphadenopathyID As System.Windows.Forms.ComboBox
    Friend WithEvents lblLynphadenopathyID As System.Windows.Forms.Label
    Friend WithEvents cboVaricoseID As System.Windows.Forms.ComboBox
    Friend WithEvents lblVaricoseID As System.Windows.Forms.Label
    Friend WithEvents cboOedemaID As System.Windows.Forms.ComboBox
    Friend WithEvents lblOedemaID As System.Windows.Forms.Label
    Friend WithEvents cboHeartSoundID As System.Windows.Forms.ComboBox
    Friend WithEvents lblHeartSoundID As System.Windows.Forms.Label
    Friend WithEvents cboAirEntryID As System.Windows.Forms.ComboBox
    Friend WithEvents lblAirEntryID As System.Windows.Forms.Label
    Friend WithEvents cboBreastID As System.Windows.Forms.ComboBox
    Friend WithEvents lblBreastID As System.Windows.Forms.Label
    Friend WithEvents cboLiverID As System.Windows.Forms.ComboBox
    Friend WithEvents lblLiverID As System.Windows.Forms.Label
    Friend WithEvents cboSpleenID As System.Windows.Forms.ComboBox
    Friend WithEvents lblSpleenID As System.Windows.Forms.Label
    Friend WithEvents cboBowelSoundsID As System.Windows.Forms.ComboBox
    Friend WithEvents lblBowelSoundsID As System.Windows.Forms.Label
    Friend WithEvents cboScarID As System.Windows.Forms.ComboBox
    Friend WithEvents lblScarID As System.Windows.Forms.Label
    Friend WithEvents cboPupilReactionID As System.Windows.Forms.ComboBox
    Friend WithEvents lblPupilReactionID As System.Windows.Forms.Label
    Friend WithEvents cboReflexesID As System.Windows.Forms.ComboBox
    Friend WithEvents lblReflexesID As System.Windows.Forms.Label
    Friend WithEvents cboOtherSTIID As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherSTIID As System.Windows.Forms.Label
    Friend WithEvents stbSTIDetails As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblSTIDetails As System.Windows.Forms.Label
    Friend WithEvents cboVulvaID As System.Windows.Forms.ComboBox
    Friend WithEvents lblVulvaID As System.Windows.Forms.Label
    Friend WithEvents cboCervixID As System.Windows.Forms.ComboBox
    Friend WithEvents lblCervixID As System.Windows.Forms.Label
    Friend WithEvents cboAdnexaID As System.Windows.Forms.ComboBox
    Friend WithEvents lblAdnexaID As System.Windows.Forms.Label
    Friend WithEvents cboVaginaID As System.Windows.Forms.ComboBox
    Friend WithEvents lblVaginaID As System.Windows.Forms.Label
    Friend WithEvents lblUterusID As System.Windows.Forms.Label
    Friend WithEvents nbxAnenorrheaWeeks As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblAnenorrheaWeeks As System.Windows.Forms.Label
    Friend WithEvents stbFundalHeight As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblFundalHeight As System.Windows.Forms.Label
    Friend WithEvents lblPresentation As System.Windows.Forms.Label
    Friend WithEvents cboLieID As System.Windows.Forms.ComboBox
    Friend WithEvents lblLie As System.Windows.Forms.Label
    Friend WithEvents stbRelationPPOrBrim As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblRelationPPOrBrim As System.Windows.Forms.Label
    Friend WithEvents nbxFoetalHeart As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblFoetalHeart As System.Windows.Forms.Label
    Friend WithEvents chkTTGiven As System.Windows.Forms.CheckBox
    Friend WithEvents lblIPT As System.Windows.Forms.Label
    Friend WithEvents lblNetUse As System.Windows.Forms.Label
    Friend WithEvents stbRemarks As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents dtpReturnDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblReturnDate As System.Windows.Forms.Label
    Friend WithEvents cboDoctorSpecialtyID As System.Windows.Forms.ComboBox
    Friend WithEvents lblExaminerSpecialityID As System.Windows.Forms.Label
    Friend WithEvents cboDoctorID As System.Windows.Forms.ComboBox
    Friend WithEvents lblDoctorID As System.Windows.Forms.Label
    Friend WithEvents grpAbdomen As System.Windows.Forms.GroupBox
    Friend WithEvents grpCNS As System.Windows.Forms.GroupBox
    Friend WithEvents grpChest As System.Windows.Forms.GroupBox
    Friend WithEvents grpExamination As System.Windows.Forms.GroupBox
    Friend WithEvents grpSTIs As System.Windows.Forms.GroupBox
    Friend WithEvents grpGeneral As System.Windows.Forms.GroupBox
    Friend WithEvents cboSkeletalDeformityID As System.Windows.Forms.ComboBox
    Friend WithEvents lblSkeletalDeformityID As System.Windows.Forms.Label
    Friend WithEvents tbcPhysicalExamination As System.Windows.Forms.TabControl
    Friend WithEvents tpgPhysicalExamination As System.Windows.Forms.TabPage
    Friend WithEvents tpgPelvicExamination As System.Windows.Forms.TabPage
    Friend WithEvents tpgFoetalExamination As System.Windows.Forms.TabPage
    Friend WithEvents cboPositionID As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents stbFullName As System.Windows.Forms.TextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents stbBloodGroup As System.Windows.Forms.TextBox
    Friend WithEvents lblBloodGroup As System.Windows.Forms.Label
    Friend WithEvents stbTotalVisits As System.Windows.Forms.TextBox
    Friend WithEvents lblNoOfVisits As System.Windows.Forms.Label
    Friend WithEvents stbPhone As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents stbAddress As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents txtVisitDate As System.Windows.Forms.TextBox
    Friend WithEvents lblVisitDate As System.Windows.Forms.Label
    Friend WithEvents stbHIVStatus As System.Windows.Forms.TextBox
    Friend WithEvents stbEDD As System.Windows.Forms.TextBox
    Friend WithEvents lblEDD As System.Windows.Forms.Label
    Friend WithEvents stbANCJoinDate As System.Windows.Forms.TextBox
    Friend WithEvents lblANCJoinDate As System.Windows.Forms.Label
    Friend WithEvents tpgANCVitals As System.Windows.Forms.TabPage
    Friend WithEvents nbxMUAC As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblHeight As System.Windows.Forms.Label
    Friend WithEvents lblMUAC As System.Windows.Forms.Label
    Friend WithEvents nbxTemperature As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblTemperature As System.Windows.Forms.Label
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents nbxPulse As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents nbxHeight As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblPulse As System.Windows.Forms.Label
    Friend WithEvents nbxWeight As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblBloodPressure As System.Windows.Forms.Label
    Friend WithEvents stbBloodPressure As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents stbBMIStatus As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblBMIStatus As System.Windows.Forms.Label
    Friend WithEvents lblOxygenSaturation As System.Windows.Forms.Label
    Friend WithEvents nbxOxygenSaturation As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents nbxRespirationRate As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblRespirationRate As System.Windows.Forms.Label
    Friend WithEvents nbxHeartRate As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblHeartRate As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents stbNotes As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents nbxBMI As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblBMI As System.Windows.Forms.Label
    Friend WithEvents btnTriage As System.Windows.Forms.Button
    Friend WithEvents pnlNavigateANCVisits As System.Windows.Forms.Panel
    Friend WithEvents chkNavigateVisits As System.Windows.Forms.CheckBox
    Friend WithEvents navVisits As SyncSoft.Common.Win.Controls.DataNavigator
    Friend WithEvents stbPartnersHIVStatus As System.Windows.Forms.TextBox
    Friend WithEvents lblHIVPartnerStatus As System.Windows.Forms.Label
    Protected WithEvents spbPhoto As SyncSoft.Common.Win.Controls.SmartPictureBox
    Friend WithEvents lblPhoto As System.Windows.Forms.Label
    Friend WithEvents stbVisitNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents tpgANCAllergies As System.Windows.Forms.TabPage
    Friend WithEvents dgvPatientAllergies As System.Windows.Forms.DataGridView
    Friend WithEvents btnAddAllergy As System.Windows.Forms.Button
    Friend WithEvents colAllergyNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAllergyCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colReaction As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPatientAllergiesSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents stbPatientNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblPatientNo As System.Windows.Forms.Label
    Friend WithEvents btnFindByFingerprint As System.Windows.Forms.Button
    Friend WithEvents cboNetUse As System.Windows.Forms.ComboBox
    Friend WithEvents cboIPT As System.Windows.Forms.ComboBox
    Friend WithEvents cboNurseID As System.Windows.Forms.ComboBox
    Friend WithEvents lblNurse As System.Windows.Forms.Label
    Friend WithEvents stbANCNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblANCNo As System.Windows.Forms.Label
    Friend WithEvents btnLoadSeeANCVisits As System.Windows.Forms.Button
    Friend WithEvents grpPrognosis As System.Windows.Forms.GroupBox
    Friend WithEvents stbRiskFactors As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblRiskFactors As System.Windows.Forms.Label
    Friend WithEvents stbRecommendations As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblRecommendations As System.Windows.Forms.Label
    Friend WithEvents grpAssessment As System.Windows.Forms.GroupBox
    Friend WithEvents nbxIschialSpine As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblIschialSpine As System.Windows.Forms.Label
    Friend WithEvents nbxSubPublicAngle As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents nbxSacralCurve As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblSubPublicAngle As System.Windows.Forms.Label
    Friend WithEvents lblSacralCurve As System.Windows.Forms.Label
    Friend WithEvents nbxIschialTuberosities As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents nbxDiagonalConjugate As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblIschialTuberosities As System.Windows.Forms.Label
    Friend WithEvents lblDiagonalConjugate As System.Windows.Forms.Label
    Friend WithEvents cboConclusionID As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboPresentationID As System.Windows.Forms.ComboBox
    Friend WithEvents cboUterusID As System.Windows.Forms.ComboBox
    Friend WithEvents pnlCreateNewRound As System.Windows.Forms.Panel
    Friend WithEvents chkCreateNewVisit As System.Windows.Forms.CheckBox
    Friend WithEvents btnFindVisitNo As System.Windows.Forms.Button
    Friend WithEvents btnClear As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton

End Class