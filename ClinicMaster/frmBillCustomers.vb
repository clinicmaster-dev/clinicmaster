
Option Strict On

Imports SyncSoft.SQLDb
Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.SQL.Classes
Imports SyncSoft.Common.Win.Controls
Imports SyncSoft.Common.Enumerations
Imports SyncSoft.Common.SQL.Enumerations
Imports System.Collections.Generic
Imports LookupData = SyncSoft.Lookup.SQL.LookupData
Imports LookupDataID = SyncSoft.SQLDb.Lookup.LookupDataID
Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects
Imports LookupCommObjects = SyncSoft.Common.Lookup.LookupCommObjects
Imports LookupCommDataID = SyncSoft.Common.Lookup.LookupCommDataID

Public Class frmBillCustomers

#Region " Fields "

    Private billCustomers As DataTable
    Private services As DataTable
    Private drugs As DataTable
    Private labTests As DataTable
    Private radiologyExaminations As DataTable
    Private procedures As DataTable
    Private dentalServices As DataTable
    Private pathologyExaminations As DataTable
    Private opticalServices As DataTable
    Private extraChargeItems As DataTable

    Private beds As DataTable
    Private cardiology As DataTable
    Private consumables As DataTable

    Private _ServiceNameValue As String = String.Empty
    Private _DrugNameValue As String = String.Empty
    Private _TestNameValue As String = String.Empty
    Private _RadiologyNameValue As String = String.Empty
    Private _RadiologyNo As String = String.Empty
    Private _ProceduresNo As String = String.Empty
    Private _ProcedureNameValue As String = String.Empty
    Private _OpticalServicesNo As String = String.Empty
    Private _OpticalServiceValue As String = String.Empty
    Private _DentalServicesNo As String = String.Empty
    Private _DentalServiceValue As String = String.Empty
    Private _PathologyNo As String = String.Empty
    Private _PathologyValue As String = String.Empty
    Private _AccountNoValue As String = String.Empty
    Private _ExtraChargeItemsNo As String = String.Empty
    Private _ExtraChargeItemNameValue As String = String.Empty
    Private _DrugNo As String = String.Empty
    Private _TestNo As String = String.Empty
    Private _ServiceCode As String = String.Empty

    Private _BedNo As String = String.Empty
    Private _BedNameValue As String = String.Empty

    Private _CardiologyNo As String = String.Empty
    Private _CardiologyValue As String = String.Empty

    Private _ConsumableNo As String = String.Empty
    Private _ConsumableName As String = String.Empty

    Private _ICUServiceNo As String = String.Empty
    Private _ICUServiceName As String = String.Empty

    Private _EyeServicesNo As String = String.Empty
    Private _EyeServiceValue As String = String.Empty

    Private _MaternityServicesNo As String = String.Empty
    Private _MaternityServicesName As String = String.Empty

    Private _PackagesName As String = String.Empty
    Private _PackagesNo As String = String.Empty

    Private _TheatreServicesNo As String = String.Empty
    Private _TheatreServicesName As String = String.Empty

#End Region

    Private Sub frmBillCustomers_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.Cursor = Cursors.WaitCursor

            Me.LoadBillCustomersInsurance()

            LoadLookupDataCombo(Me.fcbBillCustomerTypeID, LookupObjects.BillCustomerType, False)
            LoadLookupDataCombo(Me.cboCoPayTypeID, LookupObjects.CoPayType, True)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.AccountStatus()
            Me.LoadBillCustomers()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub frmBillCustomers_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub

    Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub

    Private Sub cboAccountNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAccountNo.TextChanged
        Me.CallOnKeyEdit()
    End Sub

    Private Sub cboAccountNo_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAccountNo.Leave

        Try

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim accountNo As String = RevertText(SubstringRight(StringMayBeEnteredIn(Me.cboAccountNo))).ToUpper()
            Me.cboAccountNo.Text = FormatText(accountNo, "BillCustomers", "AccountNo")

            If Me.ebnSaveUpdate.ButtonText = ButtonCaption.Save Then Return

            For Each row As DataRow In billCustomers.Select("AccountNo = '" + accountNo + "'")
                Me.stbBillCustomerName.Text = StringMayBeEnteredIn(row, "BillCustomerName")
            Next
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
        End Try

    End Sub

    Private Sub btnLoadLogoPhoto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoadLogoPhoto.Click
        Me.spbLogoPhoto.LoadPhoto(Me.spbLogoPhoto.ImageSizeLimit)
    End Sub

    Private Sub btnClearLogoPhoto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearLogoPhoto.Click
        Me.spbLogoPhoto.DeletePhoto()
    End Sub

    Private Sub AccountStatus()

        Dim oLookupData As New LookupData()
        Dim oStatusID As New LookupCommDataID.StatusID()
        Dim statusLookupData As New DataTable()

        Try
            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            statusLookupData = oLookupData.GetLookupData(LookupCommObjects.Status).Tables("LookupData")
            If statusLookupData Is Nothing Then Return

            For Each row As DataRow In statusLookupData.Rows
                If oStatusID.Active.ToUpper().Equals(row.Item("DataID").ToString().ToUpper()) _
                OrElse oStatusID.Inactive.ToUpper().Equals(row.Item("DataID").ToString().ToUpper()) Then
                    Continue For
                Else : row.Delete()
                End If
            Next

            Me.cboAccountStatusID.DataSource = statusLookupData

            Me.cboAccountStatusID.DisplayMember = "DataDes"
            Me.cboAccountStatusID.ValueMember = "DataID"

            Me.cboAccountStatusID.SelectedValue = oStatusID.Active
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetCoPayDefault()

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim oCoPayTypeID As New LookupDataID.CoPayTypeID()
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.cboCoPayTypeID.SelectedValue = oCoPayTypeID.NA
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub LoadBillCustomers()

        Dim oBillCustomers As New SyncSoft.SQLDb.BillCustomers()
        Dim oSetupData As New SetupData()

        Try
            Me.Cursor = Cursors.WaitCursor

            ' Load from Bill Customers
            If Not InitOptions.LoadBillCustomersAtStart Then
                billCustomers = oBillCustomers.GetBillCustomers().Tables("BillCustomers")
                oSetupData.BillCustomers = billCustomers
            Else : billCustomers = oSetupData.BillCustomers
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.colBillCustomerName.Sorted = False
            LoadComboData(Me.colBillCustomerName, billCustomers, "AccountNo", "BillCustomerName")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub LoadBillCustomers(ByVal populate As Boolean)

        Try
            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If populate Then
                LoadComboData(Me.cboAccountNo, billCustomers, "BillCustomerFullName")
            Else : Me.cboAccountNo.Items.Clear()
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Function GetServices() As DataTable

        Dim oServices As New SyncSoft.SQLDb.Services()
        Dim oServiceCodes As New LookupDataID.ServiceCodes()

        Try
            Me.Cursor = Cursors.WaitCursor

            ' Load all from Services 

            services = oServices.GetServices().Tables("Services")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Return services
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)


        End Try

    End Function

    Private Function GetBeds() As DataTable

        Dim oBeds As New SyncSoft.SQLDb.Beds()

        Try
            Me.Cursor = Cursors.WaitCursor

            ' Load all from Beds 

            beds = oBeds.GetBeds().Tables("Beds")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Return beds
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)


        End Try

    End Function

    Private Function GetCardiology() As DataTable

        Dim oCardiologyExaminations As New SyncSoft.SQLDb.CardiologyExaminations()

        Try
            Me.Cursor = Cursors.WaitCursor

            ' Load all from Cardiology Examinations 

            cardiology = oCardiologyExaminations.GetCardiologyExaminations().Tables("CardiologyExaminations")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Return cardiology
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)


        End Try

    End Function

    Private Function GetDrugs() As DataTable

        Dim drugs As DataTable
        Dim oSetupData As New SetupData()
        Dim oDrugs As New SyncSoft.SQLDb.Drugs()

        Try

            ' Load from drugs

            If Not InitOptions.LoadDrugsAtStart Then
                drugs = oDrugs.GetDrugs().Tables("Drugs")
                oSetupData.Drugs = drugs
            Else : drugs = oSetupData.Drugs
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''
            Return drugs
            '''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)

        End Try

    End Function

    Private Function GetEyeName() As DataTable

        Dim eyeName As DataTable
        Dim oEyeService As New SyncSoft.SQLDb.EyeServices()

        Try

            eyeName = oEyeService.GetEyeServices().Tables("EyeServices")
        
            '''''''''''''''''''''''''''''''''''''''''''''''''
            Return eyeName
            '''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)

        End Try

    End Function

    Private Function GetICUService() As DataTable

        Dim icuName As DataTable
        Dim oICUService As New SyncSoft.SQLDb.ICUServices()

        Try

            icuName = oICUService.GetICUServices().Tables("ICUServices")

            '''''''''''''''''''''''''''''''''''''''''''''''''
            Return icuName
            '''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)

        End Try

    End Function

    Private Function GetMaternityService() As DataTable

        Dim maternityName As DataTable
        Dim oMaternityServices As New SyncSoft.SQLDb.MaternityServices()

        Try

            maternityName = oMaternityServices.GetMaternityServices().Tables("MaternityServices")

            '''''''''''''''''''''''''''''''''''''''''''''''''
            Return maternityName
            '''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)

        End Try

    End Function

    Private Function GetRadiologyService() As DataTable

        Dim examName As DataTable
        Dim oRadiologyExams As New SyncSoft.SQLDb.RadiologyExaminations()

        Try

            examName = oRadiologyExams.GetRadiologyExaminations().Tables("RadiologyExaminations")

            '''''''''''''''''''''''''''''''''''''''''''''''''
            Return examName
            '''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)

        End Try

    End Function

    Private Function GetTheatreServices() As DataTable

        Dim theatreName As DataTable
        Dim oTheatreServices As New SyncSoft.SQLDb.TheatreServices()

        Try

            theatreName = oTheatreServices.GetTheatreServices().Tables("TheatreServices")

            '''''''''''''''''''''''''''''''''''''''''''''''''
            Return theatreName
            '''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)

        End Try

    End Function

    Private Function GetPackages() As DataTable

        Dim packages As DataTable
        Dim oPackages As New SyncSoft.SQLDb.Packages()

        Try

            packages = oPackages.GetPackages().Tables("Packages")

            '''''''''''''''''''''''''''''''''''''''''''''''''
            Return packages
            '''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)

        End Try

    End Function


    Private Function GetExtraCharge() As DataTable

        Dim extraCharge As DataTable
        Dim oExtraChargeItems As New SyncSoft.SQLDb.ExtraChargeItems()

        Try

            extraCharge = oExtraChargeItems.GetExtraChargeItems().Tables("ExtraChargeItems")

            '''''''''''''''''''''''''''''''''''''''''''''''''
            Return extraCharge
            '''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)

        End Try

    End Function



    Private Function GetOpticalServices() As DataTable
        Dim optical As DataTable
        Dim oOpticalServices As New SyncSoft.SQLDb.OpticalServices()

        Try

            optical = oOpticalServices.GetOpticalServices().Tables("OpticalServices")

            '''''''''''''''''''''''''''''''''''''''''''''''''
            Return optical
            '''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)

        End Try
    End Function

    Private Function GetPathology() As DataTable

        Dim pathology As DataTable
        Dim oPathologyExaminations As New SyncSoft.SQLDb.PathologyExaminations()

        Try

            pathology = oPathologyExaminations.GetPathologyExaminations.Tables("PathologyExaminations")

            '''''''''''''''''''''''''''''''''''''''''''''''''
            Return pathology
            '''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)

        End Try

    End Function


    Private Function GetProcedures() As DataTable

        Dim procedures As DataTable
        Dim oProcedures As New SyncSoft.SQLDb.Procedures()

        Try

            procedures = oProcedures.GetProcedures().Tables("Procedures")

            '''''''''''''''''''''''''''''''''''''''''''''''''
            Return procedures
            '''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)

        End Try

    End Function

    Private Function GetConsumableItems() As DataTable

        Dim oConsumableItems As New SyncSoft.SQLDb.ConsumableItems()

        Try
            Me.Cursor = Cursors.WaitCursor

            ' Load all from consumables 

            consumables = oConsumableItems.GetConsumableItems().Tables("ConsumableItems")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Return consumables
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)


        End Try

    End Function

    Private Function GetLabTests() As DataTable

        Dim labTests As DataTable
        Dim oSetupData As New SetupData()
        Dim oLabTests As New SyncSoft.SQLDb.LabTests()

        Try

            ' Load from LabTests
            If Not InitOptions.LoadLabTestsAtStart Then
                labTests = oLabTests.GetLabTests().Tables("LabTests")
                oSetupData.LabTests = labTests
            Else : labTests = oSetupData.LabTests
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Return labTests
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)
        End Try

    End Function

    Private Function GetDentalServices() As DataTable

        Dim dentalService As DataTable
        Dim oDentalServices As New SyncSoft.SQLDb.DentalServices()

        Try

            dentalService = oDentalServices.GetDentalServices().Tables("DentalServices")

            '''''''''''''''''''''''''''''''''''''''''''''''''
            Return dentalService
            '''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw (ex)

        End Try

    End Function

    Private Sub SetNextAccountNo()

        Dim yearL2 As String = Today.Year.ToString().Substring(2)

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim oBillCustomers As New SyncSoft.SQLDb.BillCustomers()
            Dim oAutoNumbers As New SyncSoft.Options.SQL.AutoNumbers()

            Dim autoNumbers As DataTable = oAutoNumbers.GetAutoNumbers("BillCustomers", "AccountNo").Tables("AutoNumbers")
            Dim row As DataRow = autoNumbers.Rows(0)

            Dim paddingLEN As Integer = IntegerEnteredIn(row, "PaddingLEN")
            Dim paddingCHAR As Char = CChar(StringEnteredIn(row, "PaddingCHAR"))

            Dim nextAccountNo As String = CStr(oBillCustomers.GetNextAccountID).PadLeft(paddingLEN, paddingCHAR)
            Me.cboAccountNo.Text = FormatText(("A" + yearL2 + nextAccountNo).Trim(), "BillCustomers", "AccountNo")

        Catch eX As Exception
            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub LoadBillCustomersInsurance()

        Dim billCustomersInsurance As DataTable
        Dim oBillCustomersInsurance As New SyncSoft.SQLDb.BillCustomers()
        Dim oBillCustomerTypeID As New LookupDataID.BillCustomerTypeID()

        Try
            Me.Cursor = Cursors.WaitCursor

            ' Load from BillCustomersInsurance
            billCustomersInsurance = oBillCustomersInsurance.GetBillCustomersInsurance(oBillCustomerTypeID.Insurance).Tables("BillCustomers")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadComboData(Me.cboInsuranceNo, billCustomersInsurance, "BillCustomerFullName")
            Me.cboInsuranceNo.Items.Insert(0, "")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub ResetCoPayControls()

        Dim oCoPayTypeID As New LookupDataID.CoPayTypeID()
        Dim coPayTypeID As String = StringValueMayBeEnteredIn(Me.cboCoPayTypeID, "Co-Pay Type!")

        Select Case coPayTypeID

            Case oCoPayTypeID.NA

                Me.nbxCoPayPercent.Value = 0.ToString()
                Me.nbxCoPayPercent.Enabled = False
                Me.nbxCoPayValue.Value = 0.ToString()
                Me.nbxCoPayValue.Enabled = False

            Case oCoPayTypeID.Percent

                Me.nbxCoPayPercent.Value = String.Empty
                Me.nbxCoPayPercent.Enabled = True
                Me.nbxCoPayValue.Value = 0.0.ToString()
                Me.nbxCoPayValue.Enabled = False

            Case oCoPayTypeID.Value

                Me.nbxCoPayPercent.Value = 0.ToString()
                Me.nbxCoPayPercent.Enabled = False
                Me.nbxCoPayValue.Value = String.Empty
                Me.nbxCoPayValue.Enabled = True

            Case Else

                Me.nbxCoPayPercent.Value = String.Empty
                Me.nbxCoPayPercent.Enabled = True
                Me.nbxCoPayValue.Value = String.Empty
                Me.nbxCoPayValue.Enabled = True

        End Select

    End Sub

    Private Sub EnableStateControls(ByVal state As Boolean)

        Me.chkUseCustomFee.Checked = state
        Me.chkCaptureMemberCardNo.Checked = state
        Me.chkCaptureClaimReferenceNo.Checked = state

    End Sub

    Private Sub cboCoPayTypeID_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCoPayTypeID.SelectedIndexChanged
        Me.ResetCoPayControls()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnDelete.Click

        Dim oBillCustomers As New SyncSoft.SQLDb.BillCustomers()

        Try
            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then Return
            oBillCustomers.AccountNo = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            DisplayMessage(oBillCustomers.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ResetControlsIn(Me)
            ResetControlsIn(Me.tpgBillExcludedServices)
            ResetControlsIn(Me.tpgBillExcludedDrugs)
            ResetControlsIn(Me.tpgBillExcludedLabTests)
            ResetControlsIn(Me.tpgBillExcludedRadiology)
            ResetControlsIn(Me.tpgBillExcludedProcedures)
            ResetControlsIn(Me.tpgAssociatedBillCustomers)
            ResetControlsIn(Me.pnlCoPayTypeID)
            Me.CallOnKeyEdit()

        Catch eX As Exception
            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim oBillCustomers As New SyncSoft.SQLDb.BillCustomers()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No"))

            Me.Cursor = Cursors.WaitCursor

            Me.cboAccountNo.Text = FormatText(accountNo, "BillCustomers", "AccountNo")
            Dim dataSource As DataTable = oBillCustomers.GetBillCustomers(accountNo).Tables("BillCustomers")
            Me.DisplayData(dataSource)

            Me.LoadBillExcludedServices(accountNo)
            Me.LoadBillExcludedDrugs(accountNo)
            Me.LoadBillExcludedLabTests(accountNo)
            Me.LoadBillExcludedRadiology(accountNo)
            Me.LoadBillExcludedProcedures(accountNo)
            Me.LoadBillExcludedExtraChargeItems(accountNo)
            Me.LoadAssociatedBillCustomers(accountNo)
            Me.LoadBillExcludedDentalServices(accountNo)
            Me.LoadBillPathologyServices(accountNo)
            Me.LoadBillExcludedOpticalServices(accountNo)
            Me.LoadBillExcludedBeds(accountNo)
            Me.LoadBillExcludedCardiologyExaminations(accountNo)
            Me.LoadBillExcludedConsumableItems(accountNo)
            Me.LoadBillExcludedEyeExaminations(accountNo)
            Me.LoadBillExcludedICUServiceItems(accountNo)
            Me.LoadBillExcludedMaternityServicesItems(accountNo)
            Me.LoadBillExcludedPackagesItems(accountNo)
            Me.LoadBillExcludedTheatreServicesItems(accountNo)
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub ebnSaveUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ebnSaveUpdate.Click

        Dim records As Integer
        Dim transactions As New List(Of TransactionList(Of DBConnect))

        Try


            Me.Cursor = Cursors.WaitCursor
            Dim oBillCustomers As New SyncSoft.SQLDb.BillCustomers()
            Dim lBillCustomers As New List(Of DBConnect)

            With oBillCustomers

                .AccountNo = RevertText(StringEnteredIn(Me.cboAccountNo, "Account Number!")).ToUpper()
                .BillCustomerName = StringEnteredIn(Me.stbBillCustomerName, "To-Bill Customer Name!")
                .BillCustomerTypeID = StringValueEnteredIn(Me.fcbBillCustomerTypeID, "To-Bill Customer Type!")
                If Me.cboInsuranceNo.Text IsNot Nothing AndAlso Not String.IsNullOrEmpty(Me.cboInsuranceNo.Text) Then
                    .InsuranceNo = SubstringEnteredIn(Me.cboInsuranceNo, "To-Bill Customer Insurance!")
                Else : .InsuranceNo = String.Empty
                End If
                .ContactPerson = StringMayBeEnteredIn(Me.stbContactPerson)
                .Address = StringMayBeEnteredIn(Me.stbAddress)
                .Phone = StringMayBeEnteredIn(Me.stbPhone)
                .Fax = StringMayBeEnteredIn(Me.stbFax)
                .Email = StringMayBeEnteredIn(Me.stbEmail)
                .Website = StringMayBeEnteredIn(Me.stbWebsite)
                .LogoPhoto = BytesMayBeEnteredIn(Me.spbLogoPhoto)
                .MemberDeclaration = StringMayBeEnteredIn(Me.stbMemberDeclaration)
                .DoctorDeclaration = StringMayBeEnteredIn(Me.stbDoctorDeclaration)
                .CoPayTypeID = StringValueEnteredIn(Me.cboCoPayTypeID, "Co-Pay Type!")
                .CoPayPercent = Me.nbxCoPayPercent.GetSingle()
                .CoPayValue = Me.nbxCoPayValue.GetDecimal(False)
                .CreditLimit = Me.nbxCreditLimit.GetDecimal(False)
                .AllowOnlyListedMember = Me.chkAllowOnlyListedMember.Checked
                .UseCustomFee = Me.chkUseCustomFee.Checked
                .SmartCardApplicable = Me.chkSmartCardApplicable.Checked
                .CaptureMemberCardNo = Me.chkCaptureMemberCardNo.Checked
                .CaptureClaimReferenceNo = Me.chkCaptureClaimReferenceNo.Checked
                .Hidden = Me.chkHidden.Checked
                .AccountStatusID = StringValueEnteredIn(Me.cboAccountStatusID, "Account Status!")

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ValidateEntriesIn(Me)
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End With

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            lBillCustomers.Add(oBillCustomers)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            If oBillCustomers.Hidden.Equals(True) Then
                Dim message As String = "You have chosen to hide this To-Bill Customer and won�t be presented for selection. " +
                                        ControlChars.NewLine + "Are you sure you want to save?"
                If WarningMessage(message) = Windows.Forms.DialogResult.No Then Me.chkHidden.Focus() : Return
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Select Case Me.ebnSaveUpdate.ButtonText

                Case ButtonCaption.Save

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    transactions.Add(New TransactionList(Of DBConnect)(lBillCustomers, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedServicesList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedDrugsList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedLabTestsList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedRadiologyList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedProceduresList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(AssociatedBillCustomersList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedExtraChargeItems, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedDentalservicesList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedPathologyList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedOpticalServicesList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedBedsList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedCardiologyExaminationsList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedConsumableList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedEyeServicesList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedICUServicesList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedMaternityServicesList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedPackagesList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedTheatreServicesList, Action.Save))

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    records = DoTransactions(transactions)
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ResetControlsIn(Me)
                    ResetControlsIn(Me.tpgBillExcludedServices)
                    ResetControlsIn(Me.tpgBillExcludedDrugs)
                    ResetControlsIn(Me.tpgBillExcludedLabTests)
                    ResetControlsIn(Me.tpgBillExcludedRadiology)
                    ResetControlsIn(Me.tpgBillExcludedProcedures)
                    ResetControlsIn(Me.tpgAssociatedBillCustomers)
                    ResetControlsIn(Me.tpgBillExcludedExtraChargeItems)
                    ResetControlsIn(Me.tpgBillExcludedDentalServices)
                    ResetControlsIn(Me.tpgBillExcludedPathology)
                    ResetControlsIn(Me.tpgBillExcludedOpticalservices)
                    ResetControlsIn(Me.tpgToBillExcludedBeds)
                    ResetControlsIn(Me.tpgToBillExcludedConsumableItems)
                    ResetControlsIn(Me.tpgToBillExcludedCardiologyServices)
                    ResetControlsIn(Me.tpgToBillExcludedEyeServices)
                    ResetControlsIn(Me.tpgToBillExcludedMaternityServices)
                    ResetControlsIn(Me.tpgToBillExcludedICUServices)
                    ResetControlsIn(Me.tpgToBillExcludedPackages)
                    ResetControlsIn(Me.tpgToBillExcludedTheatreServices)
                    Me.ResetCoPayControls()

                    Me.SetNextAccountNo()
                    Me.EnableStateControls(True)
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case ButtonCaption.Update

                    transactions.Add(New TransactionList(Of DBConnect)(lBillCustomers, Action.Update, "BillCustomers"))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedServicesList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedDrugsList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedLabTestsList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedRadiologyList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedProceduresList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(AssociatedBillCustomersList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedExtraChargeItems, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedDentalservicesList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedPathologyList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedOpticalServicesList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedBedsList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedCardiologyExaminationsList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedConsumableList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedEyeServicesList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedICUServicesList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedMaternityServicesList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedPackagesList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(BillExcludedTheatreServicesList, Action.Save))
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    records = DoTransactions(transactions)
                    DisplayMessage(records.ToString() + " record(s) updated!")
                    Me.CallOnKeyEdit()
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End Select

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            For rowNo As Integer = 0 To Me.dgvBillExcludedServices.RowCount - 2
                Me.dgvBillExcludedServices.Item(Me.colBillExcludedServicesSaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvBillExcludedDrugs.RowCount - 2
                Me.dgvBillExcludedDrugs.Item(Me.colBillExcludedDrugsSaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvBillExcludedLabTests.RowCount - 2
                Me.dgvBillExcludedLabTests.Item(Me.colBillExcludedLabTestsSaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvBillExcludedRadiology.RowCount - 2
                Me.dgvBillExcludedRadiology.Item(Me.colBillExcludedRadiologySaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvBillExcludedProcedures.RowCount - 2
                Me.dgvBillExcludedProcedures.Item(Me.colBillExcludedProceduresSaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvAssociatedBillCustomers.RowCount - 2
                Me.dgvAssociatedBillCustomers.Item(Me.colAssociatedBillCustomersSaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvBillExcludedExtraChargeItems.RowCount - 2
                Me.dgvBillExcludedExtraChargeItems.Item(Me.colBillExcludedExtraChargeItemsSaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvBillExcludedDentalServices.RowCount - 2
                Me.dgvBillExcludedDentalServices.Item(Me.colBillExcludedDentalServicesSaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvBillExcludedPathology.RowCount - 2
                Me.dgvBillExcludedPathology.Item(Me.colBillExcludedPathologySaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvBillExcludedOpticalServices.RowCount - 2
                Me.dgvBillExcludedOpticalServices.Item(Me.colBillExcludedOtherOpticalSaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvBeds.RowCount - 2
                Me.dgvBeds.Item(Me.colBillExcludedBedsSaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvCardiologyServices.RowCount - 2
                Me.dgvCardiologyServices.Item(Me.colBillExcludedCardiologySaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvExcludedConsumables.RowCount - 2
                Me.dgvExcludedConsumables.Item(Me.colBillExcludedConsumableSaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvEyeServices.RowCount - 2
                Me.dgvEyeServices.Item(Me.colBillExcludedEyeServicesSaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvExcludedICUServices.RowCount - 2
                Me.dgvExcludedICUServices.Item(Me.colBillExcludedICUServiceSaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvExcludedMaternityServices.RowCount - 2
                Me.dgvExcludedMaternityServices.Item(Me.colBillExcludedMaternityServicesSaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvExcludedPackages.RowCount - 2
                Me.dgvExcludedPackages.Item(Me.colBillExcludedPackagesSaved.Name, rowNo).Value = True
            Next

            For rowNo As Integer = 0 To Me.dgvExcludedTheatreServices.RowCount - 2
                Me.dgvExcludedTheatreServices.Item(Me.colBillExcludedTheatreServicesSaved.Name, rowNo).Value = True
            Next
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch eX As Exception
            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Function BillExcludedServicesList() As List(Of DBConnect)

        Dim lBillExcludedServices As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvBillExcludedServices.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvBillExcludedServices.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.ColExTServiceCode, "Service Code!")
                        .ItemCategoryID = oItemCategoryID.Service

                    End With

                    lBillExcludedServices.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedServices

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function BillExcludedICUServicesList() As List(Of DBConnect)

        Dim lBillExcludedICUServices As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvExcludedICUServices.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvExcludedICUServices.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.colICUServiceNo, "ICU Service Code!")
                        .ItemCategoryID = oItemCategoryID.ICU

                    End With

                    lBillExcludedICUServices.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedICUServices

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function BillExcludedMaternityServicesList() As List(Of DBConnect)

        Dim lBillExcludedMaternityServices As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvExcludedMaternityServices.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvExcludedMaternityServices.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.colMaternityServicesNo, "Maternity Service Code!")
                        .ItemCategoryID = oItemCategoryID.Maternity

                    End With

                    lBillExcludedMaternityServices.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedMaternityServices

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function BillExcludedPackagesList() As List(Of DBConnect)

        Dim lBillExcludedPackages As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvExcludedPackages.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvExcludedPackages.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.colPackagesNo, "Package Code!")
                        .ItemCategoryID = oItemCategoryID.Packages

                    End With

                    lBillExcludedPackages.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedPackages

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function BillExcludedTheatreServicesList() As List(Of DBConnect)

        Dim lBillExcludedTheatreServices As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvExcludedTheatreServices.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvExcludedTheatreServices.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.colTheatreServicesNo, "Theatre Service No!")
                        .ItemCategoryID = oItemCategoryID.Theatre

                    End With

                    lBillExcludedTheatreServices.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedTheatreServices

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function BillExcludedBedsList() As List(Of DBConnect)

        Dim lBillExcludedBeds As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvBeds.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvBeds.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.colBedNo, "Bed No!")
                        .ItemCategoryID = oItemCategoryID.Admission

                    End With

                    lBillExcludedBeds.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedBeds

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function BillExcludedCardiologyExaminationsList() As List(Of DBConnect)

        Dim lBillExcludedCardiologyExaminations As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvCardiologyServices.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvCardiologyServices.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.ColCardiologyNo, "Cardiology No!")
                        .ItemCategoryID = oItemCategoryID.Cardiology

                    End With

                    lBillExcludedCardiologyExaminations.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedCardiologyExaminations

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function BillExcludedDrugsList() As List(Of DBConnect)

        Dim lBillExcludedDrugs As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvBillExcludedDrugs.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvBillExcludedDrugs.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.colEXTDrugNo, "Drug No!")
                        .ItemCategoryID = oItemCategoryID.Drug

                    End With

                    lBillExcludedDrugs.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedDrugs

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function BillExcludedConsumableList() As List(Of DBConnect)

        Dim lBillExcludedconsumableItems As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvExcludedConsumables.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvExcludedConsumables.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.colConsumableNo, "Consumable No!")
                        .ItemCategoryID = oItemCategoryID.Consumable

                    End With

                    lBillExcludedconsumableItems.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedconsumableItems

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function BillExcludedEyeServicesList() As List(Of DBConnect)

        Dim lBillExcludedEyeServices As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvEyeServices.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvEyeServices.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.ColEyeServicesNo, "Eye No!")
                        .ItemCategoryID = oItemCategoryID.Eye

                    End With

                    lBillExcludedEyeServices.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedEyeServices

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function BillExcludedLabTestsList() As List(Of DBConnect)

        Dim lBillExcludedLabTests As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvBillExcludedLabTests.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvBillExcludedLabTests.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.ColEXTTestNo, "Test Code!")
                        .ItemCategoryID = oItemCategoryID.Test

                    End With

                    lBillExcludedLabTests.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedLabTests

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function BillExcludedRadiologyList() As List(Of DBConnect)

        Dim lBillExcludedRadiology As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvBillExcludedRadiology.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvBillExcludedRadiology.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.colExamCode, "Exam Code!")
                        .ItemCategoryID = oItemCategoryID.Radiology

                    End With

                    lBillExcludedRadiology.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedRadiology

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function BillExcludedProceduresList() As List(Of DBConnect)

        Dim lBillExcludedProcedures As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvBillExcludedProcedures.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvBillExcludedProcedures.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.colProcedureCode, "Procedure Code!")
                        .ItemCategoryID = oItemCategoryID.Procedure

                    End With

                    lBillExcludedProcedures.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedProcedures

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function BillExcludedDentalservicesList() As List(Of DBConnect)

        Dim lBillExcludedDentalservicesList As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvBillExcludedDentalServices.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvBillExcludedDentalServices.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.colDentalCode, "Dental Services!")
                        .ItemCategoryID = oItemCategoryID.Dental

                    End With

                    lBillExcludedDentalservicesList.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedDentalservicesList

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function BillExcludedPathologyList() As List(Of DBConnect)

        Dim lBillExcludedPathologyList As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvBillExcludedPathology.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvBillExcludedPathology.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = SubstringRight(StringEnteredIn(cells, Me.colPathologyNo, "Pathology Exam Full Name!"))
                        .ItemCategoryID = oItemCategoryID.Pathology

                    End With

                    lBillExcludedPathologyList.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedPathologyList

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function BillExcludedOpticalServicesList() As List(Of DBConnect)

        Dim lBillExcludedOpticalServicesList As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvBillExcludedOpticalServices.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvBillExcludedOpticalServices.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.colOpticalCode, "Optical Code!")
                        .ItemCategoryID = oItemCategoryID.Optical

                    End With

                    lBillExcludedOpticalServicesList.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedOpticalServicesList

        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Private Function AssociatedBillCustomersList() As List(Of DBConnect)

        Dim lAssociatedBillCustomers As New List(Of DBConnect)

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvAssociatedBillCustomers.RowCount - 2

                Using oAssociatedBillCustomers As New SyncSoft.SQLDb.AssociatedBillCustomers()

                    Dim cells As DataGridViewCellCollection = Me.dgvAssociatedBillCustomers.Rows(rowNo).Cells

                    With oAssociatedBillCustomers
                        .AccountNo = accountNo
                        .AssociatedBillNo = StringEnteredIn(cells, Me.colBillCustomerName, "To-Bill Associated Account Name!")
                    End With

                    lAssociatedBillCustomers.Add(oAssociatedBillCustomers)

                End Using

            Next

            Return lAssociatedBillCustomers

        Catch ex As Exception
            Me.tbcBillCustomers.SelectTab(Me.tpgAssociatedBillCustomers.Name)
            Throw ex

        End Try

    End Function

    Private Function BillExcludedExtraChargeItems() As List(Of DBConnect)

        Dim lBillExcludedExtraChargeItems As New List(Of DBConnect)
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))

            For rowNo As Integer = 0 To Me.dgvBillExcludedExtraChargeItems.RowCount - 2

                Using oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

                    Dim cells As DataGridViewCellCollection = Me.dgvBillExcludedExtraChargeItems.Rows(rowNo).Cells

                    With oBillExcludedItems

                        .AccountNo = accountNo
                        .ItemCode = StringEnteredIn(cells, Me.colExtraChargeItemsCode, "Extra Charge!")
                        .ItemCategoryID = oItemCategoryID.Extras

                    End With

                    lBillExcludedExtraChargeItems.Add(oBillExcludedItems)

                End Using

            Next

            Return lBillExcludedExtraChargeItems

        Catch ex As Exception
            Throw ex

        End Try

    End Function

#Region " Services - Grid "

    Private Sub dgvBillExcludedServices_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvBillExcludedServices.CellBeginEdit

        If e.ColumnIndex <> Me.colServiceCode.Index OrElse Me.dgvBillExcludedServices.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvBillExcludedServices.CurrentCell.RowIndex
        _ServiceNameValue = StringMayBeEnteredIn(Me.dgvBillExcludedServices.Rows(selectedRow).Cells, Me.colServiceCode)

    End Sub

    Private Sub dgvBillExcludedServices_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedServices.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvBillExcludedServices.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.ColExTServiceCode.Index) Then

                If Me.dgvBillExcludedServices.Rows.Count > 1 Then Me.SetServicesEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub dgvBillExcludedServices_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvBillExcludedServices.UserDeletingRow

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvBillExcludedServices.Item(Me.colBillExcludedServicesSaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvBillExcludedServices.Item(Me.ColExTServiceCode.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Service
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub dgvBillExcludedServices_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBillExcludedServices.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub LoadBillExcludedServices(ByVal accountNo As String)

        Dim billExcludedServices As DataTable
        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvBillExcludedServices.Rows.Clear()

            ' Load items not yet paid for

            billExcludedServices = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Service).Tables("BillExcludedItems")

            If billExcludedServices Is Nothing OrElse billExcludedServices.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvBillExcludedServices, billExcludedServices)

            For Each row As DataGridViewRow In Me.dgvBillExcludedServices.Rows
                If row.IsNewRow Then Exit For
                Me.dgvBillExcludedServices.Item(Me.colBillExcludedServicesSaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub dgvBillExcludedServices_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedServices.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("Services", "Service Code", "Service", Me.GetServices(), "ServiceFullName",
                                                                     "ServiceCode", "ServiceName", Me.dgvBillExcludedServices, Me.ColExTServiceCode, e.RowIndex)

            Me._ServiceCode = StringMayBeEnteredIn(Me.dgvBillExcludedServices.Rows(e.RowIndex).Cells, Me.ColExTServiceCode)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColEXTServiceSelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvBillExcludedServices.Rows(e.RowIndex).IsNewRow Then

                Me.dgvBillExcludedServices.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetServicesEntries(e.RowIndex)
            ElseIf Me.ColEXTServiceSelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetServicesEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetServicesEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedServices.Rows(selectedRow).Cells, Me.ColExTServiceCode)
            Me.SetServicesEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetServicesEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvBillExcludedServices.Item(Me.colBillExcludedServicesSaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Service Code (" + Me._ServiceCode + ") can't be edited!")
                Me.dgvBillExcludedServices.Item(Me.ColExTServiceCode.Name, selectedRow).Value = Me._ServiceCode
                Me.dgvBillExcludedServices.Item(Me.ColExTServiceCode.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvBillExcludedServices.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedServices.Rows(rowNo).Cells, Me.ColExTServiceCode)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Service Code (" + enteredItem + ") already selected!")
                        Me.dgvBillExcludedServices.Rows.RemoveAt(selectedRow)
                        Me.dgvBillExcludedServices.Item(Me.ColExTServiceCode.Name, selectedRow).Value = Me._ServiceCode
                        Me.dgvBillExcludedServices.Item(Me.ColExTServiceCode.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredService(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredService(ByVal selectedRow As Integer)
        Try

            Dim oService As New SyncSoft.SQLDb.Services()
            Dim serviceCode As String = String.Empty

            If Me.dgvBillExcludedServices.Rows.Count > 1 Then serviceCode = SubstringRight(StringMayBeEnteredIn(Me.dgvBillExcludedServices.Rows(selectedRow).Cells, Me.ColExTServiceCode))

            If String.IsNullOrEmpty(serviceCode) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim services As DataTable = oService.GetServices(serviceCode).Tables("Services")
            If services Is Nothing OrElse String.IsNullOrEmpty(serviceCode) Then Return
            Dim row As DataRow = services.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim serviceName As String = StringEnteredIn(row, "ServiceName", "Service Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvBillExcludedServices
                .Item(Me.ColExTServiceCode.Name, selectedRow).Value = serviceCode.ToUpper()
                .Item(Me.colServiceCode.Name, selectedRow).Value = serviceName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvBillExcludedServices.Item(Me.ColExTServiceCode.Name, selectedRow).Value = Me._ServiceCode.ToUpper()
            Throw ex

        End Try

    End Sub


#End Region

#Region " Drugs - Grid "

    Private Sub dgvBillExcludedDrugs_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvBillExcludedDrugs.CellBeginEdit

        If e.ColumnIndex <> Me.colDrugName.Index OrElse Me.dgvBillExcludedDrugs.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvBillExcludedDrugs.CurrentCell.RowIndex
        _DrugNameValue = StringMayBeEnteredIn(Me.dgvBillExcludedDrugs.Rows(selectedRow).Cells, Me.colDrugName)


    End Sub

    Private Sub dgvBillExcludedDrugs_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvBillExcludedDrugs.UserDeletingRow

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvBillExcludedDrugs.Item(Me.colBillExcludedDrugsSaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvBillExcludedDrugs.Item(Me.colEXTDrugNo.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Drug
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub dgvBillExcludedDrugs_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBillExcludedDrugs.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub LoadBillExcludedDrugs(ByVal accountNo As String)

        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvBillExcludedDrugs.Rows.Clear()

            ' Load items not yet paid for

            Dim billExcludedDrugs As DataTable = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Drug).Tables("BillExcludedItems")

            If billExcludedDrugs Is Nothing OrElse billExcludedDrugs.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvBillExcludedDrugs, billExcludedDrugs)

            For Each row As DataGridViewRow In Me.dgvBillExcludedDrugs.Rows
                If row.IsNewRow Then Exit For
                Me.dgvBillExcludedDrugs.Item(Me.colBillExcludedDrugsSaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub dgvBillExcludedDrugs_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedDrugs.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("Drugs", "Drug No", "Drug", Me.GetDrugs(), "DrugFullName",
                                                                     "DrugNo", "DrugName", Me.dgvBillExcludedDrugs, Me.colEXTDrugNo, e.RowIndex)

            Me._DrugNo = StringMayBeEnteredIn(Me.dgvBillExcludedDrugs.Rows(e.RowIndex).Cells, Me.colEXTDrugNo)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.colSelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvBillExcludedDrugs.Rows(e.RowIndex).IsNewRow Then

                Me.dgvBillExcludedDrugs.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetDrugsEntries(e.RowIndex)
            ElseIf Me.colSelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetDrugsEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetDrugsEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = SubstringRight(StringMayBeEnteredIn(Me.dgvBillExcludedDrugs.Rows(selectedRow).Cells, Me.colEXTDrugNo))
            Me.SetDrugsEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetDrugsEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvBillExcludedDrugs.Item(Me.colBillExcludedDrugsSaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Drug No (" + Me._DrugNo + ") can't be edited!")
                Me.dgvBillExcludedDrugs.Item(Me.colEXTDrugNo.Name, selectedRow).Value = Me._DrugNo
                Me.dgvBillExcludedDrugs.Item(Me.colEXTDrugNo.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvBillExcludedDrugs.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedDrugs.Rows(rowNo).Cells, Me.colEXTDrugNo)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Drug No (" + enteredItem + ") already selected!")
                        Me.dgvBillExcludedDrugs.Rows.RemoveAt(selectedRow)
                        Me.dgvBillExcludedDrugs.Item(Me.colEXTDrugNo.Name, selectedRow).Value = Me._DrugNo
                        Me.dgvBillExcludedDrugs.Item(Me.colEXTDrugNo.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredDrug(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredDrug(ByVal selectedRow As Integer)
        Try

            Dim drugSelected As String = String.Empty
            Dim oDrugs As New SyncSoft.SQLDb.Drugs()
            Dim drugNo As String = String.Empty

            If Me.dgvBillExcludedDrugs.Rows.Count > 1 Then drugNo = SubstringRight(StringMayBeEnteredIn(Me.dgvBillExcludedDrugs.Rows(selectedRow).Cells, Me.colEXTDrugNo))

            If String.IsNullOrEmpty(drugNo) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim drugs As DataTable = oDrugs.GetDrugs(drugNo).Tables("Drugs")
            If drugs Is Nothing OrElse String.IsNullOrEmpty(drugNo) Then Return
            Dim row As DataRow = drugs.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim drugName As String = StringEnteredIn(row, "DrugName", "Drug Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvBillExcludedDrugs
                .Item(Me.colEXTDrugNo.Name, selectedRow).Value = drugNo.ToUpper()
                .Item(Me.colDrugName.Name, selectedRow).Value = drugName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvBillExcludedDrugs.Item(Me.colEXTDrugNo.Name, selectedRow).Value = Me._DrugNo.ToUpper()
            Throw ex

        End Try

    End Sub

    Private Sub dgvBillExcludedDrugs_CellEndEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedDrugs.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvBillExcludedDrugs.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.colEXTDrugNo.Index) Then

                If Me.dgvBillExcludedDrugs.Rows.Count > 1 Then Me.SetDrugsEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try
    End Sub

#End Region

#Region " LabTests - Grid "

    Private Sub dgvBillExcludedLabTests_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvBillExcludedLabTests.CellBeginEdit

        If e.ColumnIndex <> Me.colTestName.Index OrElse Me.dgvBillExcludedLabTests.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvBillExcludedLabTests.CurrentCell.RowIndex
        _TestNameValue = StringMayBeEnteredIn(Me.dgvBillExcludedLabTests.Rows(selectedRow).Cells, Me.colTestName)

    End Sub

    Private Sub dgvBillExcludedLabTests_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedLabTests.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvBillExcludedLabTests.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.ColEXTTestNo.Index) Then

                If Me.dgvBillExcludedLabTests.Rows.Count > 1 Then Me.SetLabTestEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub dgvBillExcludedLabTests_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvBillExcludedLabTests.UserDeletingRow

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvBillExcludedLabTests.Item(Me.colBillExcludedLabTestsSaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvBillExcludedLabTests.Item(Me.ColEXTTestNo.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Test
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub dgvBillExcludedLabTests_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBillExcludedLabTests.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub LoadBillExcludedLabTests(ByVal accountNo As String)

        Dim billExcludedLabTests As DataTable
        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvBillExcludedLabTests.Rows.Clear()

            ' Load items not yet paid for

            billExcludedLabTests = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Test).Tables("BillExcludedItems")

            If billExcludedLabTests Is Nothing OrElse billExcludedLabTests.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvBillExcludedLabTests, billExcludedLabTests)

            For Each row As DataGridViewRow In Me.dgvBillExcludedLabTests.Rows
                If row.IsNewRow Then Exit For
                Me.dgvBillExcludedLabTests.Item(Me.colBillExcludedLabTestsSaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub dgvBillExcludedLabTests_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedLabTests.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("LabTests", "Test Code", "Test", Me.GetLabTests(), "TestFullName",
                                                                     "TestCode", "TestName", Me.dgvBillExcludedLabTests, Me.ColEXTTestNo, e.RowIndex)

            Me._TestNo = StringMayBeEnteredIn(Me.dgvBillExcludedLabTests.Rows(e.RowIndex).Cells, Me.ColEXTTestNo)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColLabSelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvBillExcludedLabTests.Rows(e.RowIndex).IsNewRow Then

                Me.dgvBillExcludedLabTests.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetLabTestEntries(e.RowIndex)
            ElseIf Me.ColLabSelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetLabTestEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub SetLabTestEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = SubstringRight(StringMayBeEnteredIn(Me.dgvBillExcludedLabTests.Rows(selectedRow).Cells, Me.ColEXTTestNo))
            Me.SetLabTestEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetLabTestEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvBillExcludedLabTests.Item(Me.colBillExcludedLabTestsSaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Test No (" + Me._TestNo + ") can't be edited!")
                Me.dgvBillExcludedLabTests.Item(Me.ColEXTTestNo.Name, selectedRow).Value = Me._TestNo
                Me.dgvBillExcludedLabTests.Item(Me.ColEXTTestNo.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvBillExcludedLabTests.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedLabTests.Rows(rowNo).Cells, Me.ColEXTTestNo)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Test No (" + enteredItem + ") already selected!")
                        Me.dgvBillExcludedLabTests.Rows.RemoveAt(selectedRow)
                        Me.dgvBillExcludedLabTests.Item(Me.ColEXTTestNo.Name, selectedRow).Value = Me._TestNo
                        Me.dgvBillExcludedLabTests.Item(Me.ColEXTTestNo.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredLabTest(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredLabTest(ByVal selectedRow As Integer)
        Try

            Dim oLabTests As New SyncSoft.SQLDb.LabTests
            Dim labTest As String = String.Empty

            If Me.dgvBillExcludedLabTests.Rows.Count > 1 Then labTest = SubstringRight(StringMayBeEnteredIn(Me.dgvBillExcludedLabTests.Rows(selectedRow).Cells, Me.ColEXTTestNo))

            If String.IsNullOrEmpty(labTest) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim labTests As DataTable = oLabTests.GetLabTests(labTest).Tables("LabTests")
            If labTests Is Nothing OrElse String.IsNullOrEmpty(labTest) Then Return
            Dim row As DataRow = labTests.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim labTestName As String = StringEnteredIn(row, "TestName", "Test Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvBillExcludedLabTests
                .Item(Me.ColEXTTestNo.Name, selectedRow).Value = labTest.ToUpper()
                .Item(Me.colTestName.Name, selectedRow).Value = labTestName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvBillExcludedLabTests.Item(Me.ColEXTTestNo.Name, selectedRow).Value = Me._TestNo.ToUpper()
            Throw ex

        End Try

    End Sub

#End Region

#Region " Radiology - Grid "

    Private Sub dgvBillExcludedRadiology_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvBillExcludedRadiology.CellBeginEdit

        If e.ColumnIndex <> Me.colExamCode.Index OrElse Me.dgvBillExcludedRadiology.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvBillExcludedRadiology.CurrentCell.RowIndex
        _RadiologyNameValue = StringMayBeEnteredIn(Me.dgvBillExcludedRadiology.Rows(selectedRow).Cells, Me.colExamCode)


    End Sub

    Private Sub dgvBillExcludedRadiology_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedRadiology.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvBillExcludedRadiology.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.colExamCode.Index) Then

                If Me.dgvBillExcludedRadiology.Rows.Count > 1 Then Me.SetRadiologyEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try


    End Sub

    Private Sub dgvBillExcludedRadiology_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvBillExcludedRadiology.UserDeletingRow

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvBillExcludedRadiology.Item(Me.colBillExcludedRadiologySaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvBillExcludedRadiology.Item(Me.colExamCode.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Radiology
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub dgvBillExcludedRadiology_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBillExcludedRadiology.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub LoadBillExcludedRadiology(ByVal accountNo As String)

        Dim billExcludedRadiology As DataTable
        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvBillExcludedRadiology.Rows.Clear()

            ' Load items not yet paid for

            billExcludedRadiology = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Radiology).Tables("BillExcludedItems")

            If billExcludedRadiology Is Nothing OrElse billExcludedRadiology.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvBillExcludedRadiology, billExcludedRadiology)

            For Each row As DataGridViewRow In Me.dgvBillExcludedRadiology.Rows
                If row.IsNewRow Then Exit For
                Me.dgvBillExcludedRadiology.Item(Me.colBillExcludedRadiologySaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub dgvBillExcludedRadiology_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedRadiology.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("Radiology Examinations", "Exam Code", "Exam Name", Me.GetRadiologyService(), "ExamFullName",
                                                                     "ExamCode", "ExamName", Me.dgvBillExcludedRadiology, Me.colExamCode, e.RowIndex)

            Me._RadiologyNo = StringMayBeEnteredIn(Me.dgvBillExcludedRadiology.Rows(e.RowIndex).Cells, Me.colExamCode)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColEXTRadiologySelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvBillExcludedRadiology.Rows(e.RowIndex).IsNewRow Then

                Me.dgvBillExcludedRadiology.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetRadiologyEntries(e.RowIndex)
            ElseIf Me.ColEXTRadiologySelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetRadiologyEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetRadiologyEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedRadiology.Rows(selectedRow).Cells, Me.colExamCode)
            Me.SetRadiologyEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetRadiologyEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvBillExcludedRadiology.Item(Me.colBillExcludedRadiologySaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Exam Code (" + Me._RadiologyNo + ") can't be edited!")
                Me.dgvBillExcludedRadiology.Item(Me.colExamCode.Name, selectedRow).Value = Me._RadiologyNo
                Me.dgvBillExcludedRadiology.Item(Me.colExamCode.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvBillExcludedRadiology.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedRadiology.Rows(rowNo).Cells, Me.colExamCode)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Exam Code (" + enteredItem + ") already selected!")
                        Me.dgvBillExcludedRadiology.Rows.RemoveAt(selectedRow)
                        Me.dgvBillExcludedRadiology.Item(Me.colExamCode.Name, selectedRow).Value = Me._RadiologyNo
                        Me.dgvBillExcludedRadiology.Item(Me.colExamCode.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredRadiologyItems(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredRadiologyItems(ByVal selectedRow As Integer)
        Try

            Dim oRadiologyExaminations As New SyncSoft.SQLDb.RadiologyExaminations()
            Dim examNo As String = String.Empty

            If Me.dgvBillExcludedRadiology.Rows.Count > 1 Then examNo = SubstringRight(StringMayBeEnteredIn(Me.dgvBillExcludedRadiology.Rows(selectedRow).Cells, Me.colExamCode))

            If String.IsNullOrEmpty(examNo) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim RadiologyItems As DataTable = oRadiologyExaminations.GetRadiologyExaminations(examNo).Tables("RadiologyExaminations")
            If RadiologyItems Is Nothing OrElse String.IsNullOrEmpty(examNo) Then Return
            Dim row As DataRow = RadiologyItems.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim examName As String = StringEnteredIn(row, "ExamName", "Exam Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvBillExcludedRadiology
                .Item(Me.colExamCode.Name, selectedRow).Value = examNo.ToUpper()
                .Item(Me.colExamName.Name, selectedRow).Value = examName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvBillExcludedRadiology.Item(Me.colExamCode.Name, selectedRow).Value = Me._RadiologyNo.ToUpper()
            Throw ex

        End Try
    End Sub




#End Region

#Region " Procedures - Grid "

    Private Sub dgvBillExcludedProcedures_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvBillExcludedProcedures.CellBeginEdit

        If e.ColumnIndex <> Me.colProcedureCode.Index OrElse Me.dgvBillExcludedProcedures.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvBillExcludedProcedures.CurrentCell.RowIndex
        _ProcedureNameValue = StringMayBeEnteredIn(Me.dgvBillExcludedProcedures.Rows(selectedRow).Cells, Me.colProcedureCode)

    End Sub

    Private Sub dgvBillExcludedProcedures_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedProcedures.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvBillExcludedProcedures.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.colExamCode.Index) Then

                If Me.dgvBillExcludedProcedures.Rows.Count > 1 Then Me.SetProceduresEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try


    End Sub

    Private Sub dgvBillExcludedProcedures_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedProcedures.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("Procedures", "Procedure Code", "Procedure Name", Me.GetProcedures(), "ProcedureFullName",
                                                                     "ProcedureCode", "ProcedureName", Me.dgvBillExcludedProcedures, Me.colProcedureCode, e.RowIndex)

            Me._ProceduresNo = StringMayBeEnteredIn(Me.dgvBillExcludedProcedures.Rows(e.RowIndex).Cells, Me.colProcedureCode)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColEXTProceduresSelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvBillExcludedProcedures.Rows(e.RowIndex).IsNewRow Then

                Me.dgvBillExcludedProcedures.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetProceduresEntries(e.RowIndex)
            ElseIf Me.ColEXTProceduresSelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetProceduresEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetProceduresEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedProcedures.Rows(selectedRow).Cells, Me.colProcedureCode)
            Me.SetProceduresEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetProceduresEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvBillExcludedProcedures.Item(Me.colBillExcludedProceduresSaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Procedure Code (" + Me._ProceduresNo + ") can't be edited!")
                Me.dgvBillExcludedProcedures.Item(Me.colProcedureCode.Name, selectedRow).Value = Me._ProceduresNo
                Me.dgvBillExcludedProcedures.Item(Me.colProcedureCode.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvBillExcludedProcedures.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedProcedures.Rows(rowNo).Cells, Me.colProcedureCode)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Procedure Code (" + enteredItem + ") already selected!")
                        Me.dgvBillExcludedProcedures.Rows.RemoveAt(selectedRow)
                        Me.dgvBillExcludedProcedures.Item(Me.colProcedureCode.Name, selectedRow).Value = Me._ProceduresNo
                        Me.dgvBillExcludedProcedures.Item(Me.colProcedureCode.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredProceduresItems(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredProceduresItems(ByVal selectedRow As Integer)
        Try

            Dim oProcedures As New SyncSoft.SQLDb.Procedures()
            Dim procedureNo As String = String.Empty

            If Me.dgvBillExcludedProcedures.Rows.Count > 1 Then procedureNo = SubstringRight(StringMayBeEnteredIn(Me.dgvBillExcludedProcedures.Rows(selectedRow).Cells, Me.colProcedureCode))

            If String.IsNullOrEmpty(procedureNo) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim ProceduresItems As DataTable = oProcedures.GetProcedures(procedureNo).Tables("Procedures")
            If ProceduresItems Is Nothing OrElse String.IsNullOrEmpty(procedureNo) Then Return
            Dim row As DataRow = ProceduresItems.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim procedureName As String = StringEnteredIn(row, "ProcedureName", "Procedure Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvBillExcludedProcedures
                .Item(Me.colProcedureCode.Name, selectedRow).Value = procedureNo.ToUpper()
                .Item(Me.colProcedureName.Name, selectedRow).Value = procedureName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvBillExcludedProcedures.Item(Me.colProcedureCode.Name, selectedRow).Value = Me._ProceduresNo.ToUpper()
            Throw ex

        End Try
    End Sub

    Private Sub dgvBillExcludedProcedures_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvBillExcludedProcedures.UserDeletingRow

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvBillExcludedProcedures.Item(Me.colBillExcludedProceduresSaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvBillExcludedProcedures.Item(Me.colProcedureCode.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Procedure
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub dgvBillExcludedProcedures_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBillExcludedProcedures.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub LoadBillExcludedProcedures(ByVal accountNo As String)

        Dim billExcludedProcedures As DataTable
        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvBillExcludedProcedures.Rows.Clear()

            ' Load items not yet paid for

            billExcludedProcedures = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Procedure).Tables("BillExcludedItems")

            If billExcludedProcedures Is Nothing OrElse billExcludedProcedures.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvBillExcludedProcedures, billExcludedProcedures)

            For Each row As DataGridViewRow In Me.dgvBillExcludedProcedures.Rows
                If row.IsNewRow Then Exit For
                Me.dgvBillExcludedProcedures.Item(Me.colBillExcludedProceduresSaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

#End Region

#Region "Dental Services -Grid"

    Private Sub dgvBillExcludedDentalServices_CellBeginEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvBillExcludedDentalServices.CellBeginEdit
        If e.ColumnIndex <> Me.colDentalCode.Index OrElse Me.dgvBillExcludedDentalServices.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvBillExcludedDentalServices.CurrentCell.RowIndex
        _DentalServiceValue = StringMayBeEnteredIn(Me.dgvBillExcludedDentalServices.Rows(selectedRow).Cells, Me.colDentalCode)

    End Sub

    Private Sub dgvBillExcludedDentalServices_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedDentalServices.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvBillExcludedDentalServices.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.colDentalCode.Index) Then

                If Me.dgvBillExcludedDentalServices.Rows.Count > 1 Then Me.SetDentalServicesEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try


    End Sub

    Private Sub dgvBillExcludedDentalServices_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedDentalServices.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("Dental Services", "Dental Code", "Dental Name", Me.GetDentalServices(), "DentalFullName",
                                                                     "DentalCode", "DentalName", Me.dgvBillExcludedDentalServices, Me.colDentalCode, e.RowIndex)

            Me._DentalServicesNo = StringMayBeEnteredIn(Me.dgvBillExcludedDentalServices.Rows(e.RowIndex).Cells, Me.colDentalCode)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColEXTDentalServicesSelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvBillExcludedDentalServices.Rows(e.RowIndex).IsNewRow Then

                Me.dgvBillExcludedDentalServices.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetDentalServicesEntries(e.RowIndex)
            ElseIf Me.ColEXTDentalServicesSelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetDentalServicesEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetDentalServicesEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedDentalServices.Rows(selectedRow).Cells, Me.colDentalCode)
            Me.SetDentalServicesEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetDentalServicesEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvBillExcludedDentalServices.Item(Me.colBillExcludedDentalServicesSaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Dental Service (" + Me._DentalServicesNo + ") can't be edited!")
                Me.dgvBillExcludedDentalServices.Item(Me.colDentalCode.Name, selectedRow).Value = Me._DentalServicesNo
                Me.dgvBillExcludedDentalServices.Item(Me.colDentalCode.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvBillExcludedDentalServices.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedDentalServices.Rows(rowNo).Cells, Me.colDentalCode)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Dental Service  (" + enteredItem + ") already selected!")
                        Me.dgvBillExcludedDentalServices.Rows.RemoveAt(selectedRow)
                        Me.dgvBillExcludedDentalServices.Item(Me.colDentalCode.Name, selectedRow).Value = Me._DentalServicesNo
                        Me.dgvBillExcludedDentalServices.Item(Me.colDentalCode.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredDentalServicesItems(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredDentalServicesItems(ByVal selectedRow As Integer)
        Try

            Dim oDentalServicesExaminations As New SyncSoft.SQLDb.DentalServices()
            Dim dentalCode As String = String.Empty

            If Me.dgvBillExcludedDentalServices.Rows.Count > 1 Then dentalCode = SubstringRight(StringMayBeEnteredIn(Me.dgvBillExcludedDentalServices.Rows(selectedRow).Cells, Me.colDentalCode))

            If String.IsNullOrEmpty(dentalCode) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim DentalServicesItems As DataTable = oDentalServicesExaminations.GetDentalServices(dentalCode).Tables("DentalServices")
            If DentalServicesItems Is Nothing OrElse String.IsNullOrEmpty(dentalCode) Then Return
            Dim row As DataRow = DentalServicesItems.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim dentalName As String = StringEnteredIn(row, "DentalName", "Dental Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvBillExcludedDentalServices
                .Item(Me.colDentalCode.Name, selectedRow).Value = dentalCode.ToUpper()
                .Item(Me.colDentalName.Name, selectedRow).Value = dentalName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvBillExcludedDentalServices.Item(Me.colDentalCode.Name, selectedRow).Value = Me._DentalServicesNo.ToUpper()
            Throw ex

        End Try
    End Sub

    Private Sub dgvBillExcludedDentalServices_DataError(sender As Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBillExcludedDentalServices.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub dgvBillExcludedDentalServices_UserDeletingRow(sender As Object, e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvBillExcludedDentalServices.UserDeletingRow
        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvBillExcludedDentalServices.Item(Me.colBillExcludedDentalServicesSaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvBillExcludedDentalServices.Item(Me.colDentalCode.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Dental
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub LoadBillExcludedDentalServices(ByVal accountNo As String)

        Dim billExcludedDentalServices As DataTable
        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvBillExcludedDentalServices.Rows.Clear()
            billExcludedDentalServices = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Dental).Tables("BillExcludedItems")

            If billExcludedDentalServices Is Nothing OrElse billExcludedDentalServices.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvBillExcludedDentalServices, billExcludedDentalServices)

            For Each row As DataGridViewRow In Me.dgvBillExcludedDentalServices.Rows
                If row.IsNewRow Then Exit For
                Me.dgvBillExcludedDentalServices.Item(Me.colBillExcludedDentalServicesSaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub



#End Region

#Region "Pathology -Grid"

    Private Sub dgvBillExcludedPathology_CellBeginEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvBillExcludedPathology.CellBeginEdit
        If e.ColumnIndex <> Me.colPathologyNo.Index OrElse Me.dgvBillExcludedPathology.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvBillExcludedPathology.CurrentCell.RowIndex
        _PathologyValue = StringMayBeEnteredIn(Me.dgvBillExcludedPathology.Rows(selectedRow).Cells, Me.colPathologyNo)

    End Sub

    Private Sub dgvBillExcludedPathology_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedPathology.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvBillExcludedPathology.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.colPathologyNo.Index) Then

                If Me.dgvBillExcludedPathology.Rows.Count > 1 Then Me.SetPathologyEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try


    End Sub

    Private Sub dgvBillExcludedPathology_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedPathology.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("Pathology Examinations", "Exam Code", "Exam Name", Me.GetPathology(), "ExamFullName",
                                                                     "ExamCode", "ExamName", Me.dgvBillExcludedPathology, Me.colPathologyNo, e.RowIndex)

            Me._PathologyNo = StringMayBeEnteredIn(Me.dgvBillExcludedPathology.Rows(e.RowIndex).Cells, Me.colPathologyNo)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColEXTPathologySelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvBillExcludedPathology.Rows(e.RowIndex).IsNewRow Then

                Me.dgvBillExcludedPathology.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetPathologyEntries(e.RowIndex)
            ElseIf Me.ColEXTPathologySelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetPathologyEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetPathologyEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedPathology.Rows(selectedRow).Cells, Me.colPathologyNo)
            Me.SetPathologyEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetPathologyEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvBillExcludedPathology.Item(Me.colBillExcludedPathologySaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Pathology Code (" + Me._PathologyNo + ") can't be edited!")
                Me.dgvBillExcludedPathology.Item(Me.colPathologyNo.Name, selectedRow).Value = Me._PathologyNo
                Me.dgvBillExcludedPathology.Item(Me.colPathologyNo.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvBillExcludedPathology.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedPathology.Rows(rowNo).Cells, Me.colPathologyNo)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Pathology Code (" + enteredItem + ") already selected!")
                        Me.dgvBillExcludedPathology.Rows.RemoveAt(selectedRow)
                        Me.dgvBillExcludedPathology.Item(Me.colPathologyNo.Name, selectedRow).Value = Me._PathologyNo
                        Me.dgvBillExcludedPathology.Item(Me.colPathologyNo.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredPathologyItems(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredPathologyItems(ByVal selectedRow As Integer)
        Try

            Dim oPathologyExaminations As New SyncSoft.SQLDb.PathologyExaminations()
            Dim examNo As String = String.Empty

            If Me.dgvBillExcludedPathology.Rows.Count > 1 Then examNo = SubstringRight(StringMayBeEnteredIn(Me.dgvBillExcludedPathology.Rows(selectedRow).Cells, Me.colPathologyNo))

            If String.IsNullOrEmpty(examNo) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim PathologyItems As DataTable = oPathologyExaminations.GetPathologyExaminations(examNo).Tables("PathologyExaminations")
            If PathologyItems Is Nothing OrElse String.IsNullOrEmpty(examNo) Then Return
            Dim row As DataRow = PathologyItems.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim examName As String = StringEnteredIn(row, "ExamName", "Exam Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvBillExcludedPathology
                .Item(Me.colPathologyNo.Name, selectedRow).Value = examNo.ToUpper()
                .Item(Me.colPathologyExamName.Name, selectedRow).Value = examName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvBillExcludedPathology.Item(Me.colPathologyNo.Name, selectedRow).Value = Me._PathologyNo.ToUpper()
            Throw ex

        End Try
    End Sub

    Private Sub dgvBillExcludedPathology_UserDeletingRow(sender As System.Object, e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvBillExcludedPathology.UserDeletingRow
        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvBillExcludedPathology.Item(Me.colBillExcludedPathologySaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvBillExcludedPathology.Item(Me.colPathologyNo.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Pathology
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub dgvBillExcludedPathology_DataError(sender As Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBillExcludedPathology.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub LoadBillPathologyServices(ByVal accountNo As String)

        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvBillExcludedPathology.Rows.Clear()



            Dim billExcludedPathology As DataTable = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Pathology).Tables("BillExcludedItems")

            If billExcludedPathology Is Nothing OrElse billExcludedPathology.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvBillExcludedPathology, billExcludedPathology)

            For Each row As DataGridViewRow In Me.dgvBillExcludedPathology.Rows
                If row.IsNewRow Then Exit For
                Me.dgvBillExcludedPathology.Item(Me.colBillExcludedPathologySaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub


#End Region

#Region "Optical Services -Grid"
    Private Sub dgvBillExcludedOpticalServices_CellBeginEdit(sender As System.Object, e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvBillExcludedOpticalServices.CellBeginEdit
        If e.ColumnIndex <> Me.colOpticalCode.Index OrElse Me.dgvBillExcludedOpticalServices.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvBillExcludedOpticalServices.CurrentCell.RowIndex
        _OpticalServiceValue = StringMayBeEnteredIn(Me.dgvBillExcludedOpticalServices.Rows(selectedRow).Cells, Me.colOpticalCode)

    End Sub



    Private Sub dgvBillExcludedOpticalServices_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedOpticalServices.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvBillExcludedOpticalServices.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.colExamCode.Index) Then

                If Me.dgvBillExcludedOpticalServices.Rows.Count > 1 Then Me.SetOpticalServicesEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try


    End Sub

    Private Sub dgvBillExcludedOpticalServices_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedOpticalServices.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("Optical Services", "Optical Code", "Optical Name", Me.GetOpticalServices(), "OpticalFullName",
                                                                     "OpticalCode", "OpticalName", Me.dgvBillExcludedOpticalServices, Me.colOpticalCode, e.RowIndex)

            Me._OpticalServicesNo = StringMayBeEnteredIn(Me.dgvBillExcludedOpticalServices.Rows(e.RowIndex).Cells, Me.colOpticalCode)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColEXTOpticalServicesSelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvBillExcludedOpticalServices.Rows(e.RowIndex).IsNewRow Then

                Me.dgvBillExcludedOpticalServices.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetOpticalServicesEntries(e.RowIndex)
            ElseIf Me.ColEXTOpticalServicesSelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetOpticalServicesEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetOpticalServicesEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedOpticalServices.Rows(selectedRow).Cells, Me.colOpticalCode)
            Me.SetOpticalServicesEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetOpticalServicesEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvBillExcludedOpticalServices.Item(Me.colBillExcludedOtherOpticalSaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Optical Code (" + Me._OpticalServicesNo + ") can't be edited!")
                Me.dgvBillExcludedOpticalServices.Item(Me.colOpticalCode.Name, selectedRow).Value = Me._OpticalServicesNo
                Me.dgvBillExcludedOpticalServices.Item(Me.colOpticalCode.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvBillExcludedOpticalServices.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedOpticalServices.Rows(rowNo).Cells, Me.colOpticalCode)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Optical Code (" + enteredItem + ") already selected!")
                        Me.dgvBillExcludedOpticalServices.Rows.RemoveAt(selectedRow)
                        Me.dgvBillExcludedOpticalServices.Item(Me.colOpticalCode.Name, selectedRow).Value = Me._OpticalServicesNo
                        Me.dgvBillExcludedOpticalServices.Item(Me.colOpticalCode.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredOpticalServicesItems(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredOpticalServicesItems(ByVal selectedRow As Integer)
        Try

            Dim oOpticalServices As New SyncSoft.SQLDb.OpticalServices()
            Dim opticalName As String = String.Empty

            If Me.dgvBillExcludedOpticalServices.Rows.Count > 1 Then opticalName = SubstringRight(StringMayBeEnteredIn(Me.dgvBillExcludedOpticalServices.Rows(selectedRow).Cells, Me.colOpticalCode))

            If String.IsNullOrEmpty(opticalName) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim OpticalServicesItems As DataTable = oOpticalServices.GetOpticalServices(opticalName).Tables("OpticalServices")
            If OpticalServicesItems Is Nothing OrElse String.IsNullOrEmpty(opticalName) Then Return
            Dim row As DataRow = OpticalServicesItems.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim opticName As String = StringEnteredIn(row, "OpticalName", "Optical Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvBillExcludedOpticalServices
                .Item(Me.colOpticalCode.Name, selectedRow).Value = opticalName.ToUpper()
                .Item(Me.colOpticalName.Name, selectedRow).Value = opticName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvBillExcludedOpticalServices.Item(Me.colOpticalCode.Name, selectedRow).Value = Me._OpticalServicesNo.ToUpper()
            Throw ex

        End Try
    End Sub

    Private Sub dgvBillExcludedOpticalServices_DataError(sender As System.Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBillExcludedOpticalServices.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub dgvBillExcludedOpticalServices_UserDeletingRow(sender As Object, e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvBillExcludedOpticalServices.UserDeletingRow
        Try
            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvBillExcludedOpticalServices.Item(Me.colBillExcludedOtherOpticalSaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvBillExcludedOpticalServices.Item(Me.colOpticalCode.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Optical
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub LoadBillExcludedOpticalServices(ByVal accountNo As String)

        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvBillExcludedOpticalServices.Rows.Clear()



            Dim billExcludedOpticalServices As DataTable = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Optical).Tables("BillExcludedItems")

            If billExcludedOpticalServices Is Nothing OrElse billExcludedOpticalServices.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvBillExcludedOpticalServices, billExcludedOpticalServices)

            For Each row As DataGridViewRow In Me.dgvBillExcludedOpticalServices.Rows
                If row.IsNewRow Then Exit For
                Me.dgvBillExcludedOpticalServices.Item(Me.colBillExcludedOtherOpticalSaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub
#End Region

#Region " AssociatedBillCustomers - Grid "

    Private Sub dgvAssociatedBillCustomers_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvAssociatedBillCustomers.CellBeginEdit

        If e.ColumnIndex <> Me.colBillCustomerName.Index OrElse Me.dgvAssociatedBillCustomers.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvAssociatedBillCustomers.CurrentCell.RowIndex
        _AccountNoValue = StringMayBeEnteredIn(Me.dgvAssociatedBillCustomers.Rows(selectedRow).Cells, Me.colBillCustomerName)

    End Sub

    Private Sub dgvAssociatedBillCustomers_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAssociatedBillCustomers.CellEndEdit

        Try

            If Me.colBillCustomerName.Items.Count < 1 Then Return

            If e.ColumnIndex.Equals(Me.colBillCustomerName.Index) Then

                ' Ensure unique entry in the combo column

                If Me.dgvAssociatedBillCustomers.Rows.Count > 1 Then

                    Dim selectedRow As Integer = Me.dgvAssociatedBillCustomers.CurrentCell.RowIndex
                    Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvAssociatedBillCustomers.Rows(selectedRow).Cells, Me.colBillCustomerName)

                    If CBool(Me.dgvAssociatedBillCustomers.Item(Me.colAssociatedBillCustomersSaved.Name, selectedRow).Value).Equals(True) Then
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        Dim _BillCustomers As EnumerableRowCollection(Of DataRow) = billCustomers.AsEnumerable()
                        Dim billCustomerName As String = (From data In _BillCustomers _
                                            Where data.Field(Of String)("AccountNo").ToUpper().Equals(_AccountNoValue.ToUpper()) _
                                            Select data.Field(Of String)("BillCustomerName")).First()
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        DisplayMessage("Associated To-Bill No (" + billCustomerName + ") can't be edited!")
                        Me.dgvAssociatedBillCustomers.Item(Me.colBillCustomerName.Name, selectedRow).Value = _AccountNoValue
                        Me.dgvAssociatedBillCustomers.Item(Me.colBillCustomerName.Name, selectedRow).Selected = True
                        Return
                    End If

                    For rowNo As Integer = 0 To Me.dgvAssociatedBillCustomers.RowCount - 2
                        If Not rowNo.Equals(selectedRow) Then
                            Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvAssociatedBillCustomers.Rows(rowNo).Cells, Me.colBillCustomerName)
                            If enteredItem.Trim().ToUpper().Equals(selectedItem.Trim().ToUpper()) Then
                                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                Dim _BillCustomers As EnumerableRowCollection(Of DataRow) = billCustomers.AsEnumerable()
                                Dim enteredDisplay As String = (From data In _BillCustomers _
                                                    Where data.Field(Of String)("AccountNo").ToUpper().Equals(enteredItem.ToUpper()) _
                                                    Select data.Field(Of String)("BillCustomerName")).First()
                                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                DisplayMessage("Associated To-Bill No (" + enteredDisplay + ") already entered!")
                                Me.dgvAssociatedBillCustomers.Item(Me.colBillCustomerName.Name, selectedRow).Value = _AccountNoValue
                                Me.dgvAssociatedBillCustomers.Item(Me.colBillCustomerName.Name, selectedRow).Selected = True
                            End If
                        End If
                    Next

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    '''''''''''''' Populate other columns based upon what is entered in combo column '''''''''''''''''''''''''''''''''
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.DetailAssociatedBillCustomers()
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                End If

            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub DetailAssociatedBillCustomers()

        Dim selectedRow As Integer
        Dim selectedItem As String = String.Empty

        Try

            If Me.dgvAssociatedBillCustomers.Rows.Count > 1 Then
                selectedRow = Me.dgvAssociatedBillCustomers.CurrentCell.RowIndex
                selectedItem = StringMayBeEnteredIn(Me.dgvAssociatedBillCustomers.Rows(selectedRow).Cells, Me.colBillCustomerName)
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Not String.IsNullOrEmpty(selectedItem) Then
                Me.dgvAssociatedBillCustomers.Item(Me.colAssociatedBillNo.Name, selectedRow).Value = selectedItem
            Else : Me.dgvAssociatedBillCustomers.Item(Me.colAssociatedBillNo.Name, selectedRow).Value = String.Empty
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub dgvAssociatedBillCustomers_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvAssociatedBillCustomers.UserDeletingRow

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oAssociatedBillCustomers As New SyncSoft.SQLDb.AssociatedBillCustomers()
            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvAssociatedBillCustomers.Item(Me.colAssociatedBillCustomersSaved.Name, toDeleteRowNo).Value).Equals(False) Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim associatedBillNo As String = CStr(Me.dgvAssociatedBillCustomers.Item(Me.colBillCustomerName.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim message As String = "You do not have permission to delete this record!"
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage(message)
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oAssociatedBillCustomers
                .AccountNo = accountNo
                .AssociatedBillNo = associatedBillNo
            End With

            DisplayMessage(oAssociatedBillCustomers.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub dgvAssociatedBillCustomers_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvAssociatedBillCustomers.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub LoadAssociatedBillCustomers(ByVal accountNo As String)

        Dim oAssociatedBillCustomers As New SyncSoft.SQLDb.AssociatedBillCustomers()

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim associatedBillCustomers As DataTable = oAssociatedBillCustomers.GetAssociatedBillCustomers(accountNo).Tables("AssociatedBillCustomers")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvAssociatedBillCustomers, associatedBillCustomers)

            For Each row As DataGridViewRow In Me.dgvAssociatedBillCustomers.Rows
                If row.IsNewRow Then Exit For
                Me.dgvAssociatedBillCustomers.Item(Me.colAssociatedBillCustomersSaved.Name, row.Index).Value = True
            Next

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

#End Region

#Region "Extra Charge - Grid"


    Private Sub LoadBillExcludedExtraChargeItems(ByVal accountNo As String)

        Dim billExcludedExtraCharge As DataTable
        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvBillExcludedExtraChargeItems.Rows.Clear()

            ' Load items not yet paid for

            billExcludedExtraCharge = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Extras).Tables("BillExcludedItems")

            If billExcludedExtraCharge Is Nothing OrElse billExcludedExtraCharge.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvBillExcludedExtraChargeItems, billExcludedExtraCharge)

            For Each row As DataGridViewRow In Me.dgvBillExcludedExtraChargeItems.Rows
                If row.IsNewRow Then Exit For
                Me.dgvBillExcludedExtraChargeItems.Item(Me.colBillExcludedExtraChargeItemsSaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub dgvBillExcludedExtraChargeItems_CellBeginEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvBillExcludedExtraChargeItems.CellBeginEdit
        If e.ColumnIndex <> Me.colExtraChargeItemsCode.Index OrElse Me.dgvBillExcludedExtraChargeItems.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvBillExcludedExtraChargeItems.CurrentCell.RowIndex
        _ExtraChargeItemNameValue = StringMayBeEnteredIn(Me.dgvBillExcludedExtraChargeItems.Rows(selectedRow).Cells, Me.colExtraChargeItemsCode)
    End Sub



    Private Sub dgvBillExcludedExtraChargeItems_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedExtraChargeItems.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvBillExcludedExtraChargeItems.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.colExtraChargeItemsCode.Index) Then

                If Me.dgvBillExcludedExtraChargeItems.Rows.Count > 1 Then Me.SetExtraChargeItemsEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try


    End Sub

    Private Sub dgvBillExcludedExtraChargeItems_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBillExcludedExtraChargeItems.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("Extra Charge Items", "Extra Item Code", "Extra Item Name", Me.GetExtraCharge(), "ExtraItemFullName",
                                                                     "ExtraItemCode", "ExtraItemName", Me.dgvBillExcludedExtraChargeItems, Me.colExtraChargeItemsCode, e.RowIndex)

            Me._ExtraChargeItemsNo = StringMayBeEnteredIn(Me.dgvBillExcludedExtraChargeItems.Rows(e.RowIndex).Cells, Me.colExtraChargeItemsCode)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColEXTExtraChargeItemsSelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvBillExcludedExtraChargeItems.Rows(e.RowIndex).IsNewRow Then

                Me.dgvBillExcludedExtraChargeItems.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetExtraChargeItemsEntries(e.RowIndex)
            ElseIf Me.ColEXTExtraChargeItemsSelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetExtraChargeItemsEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetExtraChargeItemsEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedExtraChargeItems.Rows(selectedRow).Cells, Me.colExtraChargeItemsCode)
            Me.SetExtraChargeItemsEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetExtraChargeItemsEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvBillExcludedExtraChargeItems.Item(Me.colBillExcludedExtraChargeItemsSaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Extra Charge Code (" + Me._ExtraChargeItemsNo + ") can't be edited!")
                Me.dgvBillExcludedExtraChargeItems.Item(Me.colExtraChargeItemsCode.Name, selectedRow).Value = Me._ExtraChargeItemsNo
                Me.dgvBillExcludedExtraChargeItems.Item(Me.colExtraChargeItemsCode.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvBillExcludedExtraChargeItems.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvBillExcludedExtraChargeItems.Rows(rowNo).Cells, Me.colExtraChargeItemsCode)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Extra Charge Code (" + enteredItem + ") already selected!")
                        Me.dgvBillExcludedExtraChargeItems.Rows.RemoveAt(selectedRow)
                        Me.dgvBillExcludedExtraChargeItems.Item(Me.colExtraChargeItemsCode.Name, selectedRow).Value = Me._ExtraChargeItemsNo
                        Me.dgvBillExcludedExtraChargeItems.Item(Me.colExtraChargeItemsCode.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredExtraChargeItemsItems(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredExtraChargeItemsItems(ByVal selectedRow As Integer)
        Try

            Dim oExtraChargeItems As New SyncSoft.SQLDb.ExtraChargeItems()
            Dim extraItemCode As String = String.Empty

            If Me.dgvBillExcludedExtraChargeItems.Rows.Count > 1 Then extraItemCode = SubstringRight(StringMayBeEnteredIn(Me.dgvBillExcludedExtraChargeItems.Rows(selectedRow).Cells, Me.colExtraChargeItemsCode))

            If String.IsNullOrEmpty(extraItemCode) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim ExtraChargeItemsItems As DataTable = oExtraChargeItems.GetExtraChargeItems(extraItemCode).Tables("ExtraChargeItems")
            If ExtraChargeItemsItems Is Nothing OrElse String.IsNullOrEmpty(extraItemCode) Then Return
            Dim row As DataRow = ExtraChargeItemsItems.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim extraItemName As String = StringEnteredIn(row, "ExtraItemName", "Extra Item Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvBillExcludedExtraChargeItems
                .Item(Me.colExtraChargeItemsCode.Name, selectedRow).Value = extraItemCode.ToUpper()
                .Item(Me.colExtraChargeItems.Name, selectedRow).Value = extraItemName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvBillExcludedExtraChargeItems.Item(Me.colExtraChargeItemsCode.Name, selectedRow).Value = Me._ExtraChargeItemsNo.ToUpper()
            Throw ex

        End Try
    End Sub

    Private Sub dgvBillExcludedExtraChargeItems_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBillExcludedExtraChargeItems.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub dgvBillExcludedExtraChargeItems_UserDeletingRow(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvBillExcludedExtraChargeItems.UserDeletingRow
        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvBillExcludedExtraChargeItems.Item(Me.colBillExcludedExtraChargeItemsSaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvBillExcludedExtraChargeItems.Item(Me.colExtraChargeItemsCode.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Extras
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

#End Region

#Region " Beds - Grid "

    Private Sub dgvBeds_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvBeds.CellBeginEdit

        If e.ColumnIndex <> Me.colBedNo.Index OrElse Me.dgvBeds.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvBeds.CurrentCell.RowIndex
        _BedNameValue = StringMayBeEnteredIn(Me.dgvBeds.Rows(selectedRow).Cells, Me.colBedNo)

    End Sub

    Private Sub dgvBeds_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBeds.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvBeds.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.colBedNo.Index) Then

                If Me.dgvBeds.Rows.Count > 1 Then Me.SetBedsEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub dgvBeds_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvBeds.UserDeletingRow

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvBeds.Item(Me.colBillExcludedBedsSaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvBeds.Item(Me.colBedNo.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Admission
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub dgvBeds_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBeds.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub LoadBillExcludedBeds(ByVal accountNo As String)

        Dim billExcludedServices As DataTable
        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvBeds.Rows.Clear()

            ' Load items not yet paid for

            billExcludedServices = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Admission).Tables("BillExcludedItems")

            If billExcludedServices Is Nothing OrElse billExcludedServices.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvBeds, billExcludedServices)

            For Each row As DataGridViewRow In Me.dgvBeds.Rows
                If row.IsNewRow Then Exit For
                Me.dgvBeds.Item(Me.colBillExcludedBedsSaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub dgvBeds_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBeds.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("Beds", "Bed No", "Beds", Me.GetBeds, "BedFullName",
                                                                     "BedNo", "BedName", Me.dgvBeds, Me.colBedNo, e.RowIndex)

            Me._ServiceCode = StringMayBeEnteredIn(Me.dgvBeds.Rows(e.RowIndex).Cells, Me.colBedNo)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColEXTBedSelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvBeds.Rows(e.RowIndex).IsNewRow Then

                Me.dgvBeds.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetBedsEntries(e.RowIndex)
            ElseIf Me.ColEXTBedSelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetBedsEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetBedsEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvBeds.Rows(selectedRow).Cells, Me.colBedNo)
            Me.SetBedsEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetBedsEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvBeds.Item(Me.colBillExcludedBedsSaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Bed No (" + Me._BedNo + ") can't be edited!")
                Me.dgvBeds.Item(Me.colBedNo.Name, selectedRow).Value = Me._BedNo
                Me.dgvBeds.Item(Me.colBedNo.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvBeds.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvBeds.Rows(rowNo).Cells, Me.colBedNo)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Bed No (" + enteredItem + ") already selected!")
                        Me.dgvBeds.Rows.RemoveAt(selectedRow)
                        Me.dgvBeds.Item(Me.colBedNo.Name, selectedRow).Value = Me._BedNo
                        Me.dgvBeds.Item(Me.colBedNo.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredBeds(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredBeds(ByVal selectedRow As Integer)
        Try

            Dim oBeds As New SyncSoft.SQLDb.Beds()
            Dim bedNo As String = String.Empty

            If Me.dgvBeds.Rows.Count > 1 Then bedNo = SubstringRight(StringMayBeEnteredIn(Me.dgvBeds.Rows(selectedRow).Cells, Me.colBedNo))

            If String.IsNullOrEmpty(bedNo) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim beds As DataTable = oBeds.GetBeds(bedNo).Tables("Beds")
            If beds Is Nothing OrElse String.IsNullOrEmpty(bedNo) Then Return
            Dim row As DataRow = beds.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim bedName As String = StringEnteredIn(row, "BedName", "Bed Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvBeds
                .Item(Me.colBedNo.Name, selectedRow).Value = bedNo.ToUpper()
                .Item(Me.ColBedName.Name, selectedRow).Value = bedName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvBeds.Item(Me.colBedNo.Name, selectedRow).Value = Me._BedNo.ToUpper()
            Throw ex

        End Try

    End Sub


#End Region

#Region " Cardiology Services - Grid "

    Private Sub dgvCardiologyServices_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvCardiologyServices.CellBeginEdit

        If e.ColumnIndex <> Me.ColCardiologyNo.Index OrElse Me.dgvCardiologyServices.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvCardiologyServices.CurrentCell.RowIndex
        _CardiologyValue = StringMayBeEnteredIn(Me.dgvCardiologyServices.Rows(selectedRow).Cells, Me.ColCardiologyNo)

    End Sub

    Private Sub dgvCardiologyServices_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCardiologyServices.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvCardiologyServices.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.ColCardiologyNo.Index) Then

                If Me.dgvCardiologyServices.Rows.Count > 1 Then Me.SetCardiologyServicesEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub dgvCardiologyServices_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvCardiologyServices.UserDeletingRow

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvCardiologyServices.Item(Me.colBillExcludedCardiologySaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvCardiologyServices.Item(Me.ColCardiologyNo.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Cardiology
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub dgvCardiologyServices_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvCardiologyServices.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub LoadBillExcludedCardiologyExaminations(ByVal accountNo As String)

        Dim billExcludedServices As DataTable
        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvCardiologyServices.Rows.Clear()


            billExcludedServices = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Cardiology).Tables("BillExcludedItems")

            If billExcludedServices Is Nothing OrElse billExcludedServices.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvCardiologyServices, billExcludedServices)

            For Each row As DataGridViewRow In Me.dgvCardiologyServices.Rows
                If row.IsNewRow Then Exit For
                Me.dgvCardiologyServices.Item(Me.colBillExcludedCardiologySaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub dgvCardiologyServices_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCardiologyServices.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("CardiologyExaminations", "Exam Code", "Cardiology Examination", Me.GetCardiology(), "ExamFullName",
                                                                     "ExamCode", "ExamName", Me.dgvCardiologyServices, Me.ColCardiologyNo, e.RowIndex)

            Me._CardiologyNo = StringMayBeEnteredIn(Me.dgvCardiologyServices.Rows(e.RowIndex).Cells, Me.ColCardiologyNo)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColEXTCardiologySelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvCardiologyServices.Rows(e.RowIndex).IsNewRow Then

                Me.dgvCardiologyServices.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetCardiologyServicesEntries(e.RowIndex)
            ElseIf Me.ColEXTCardiologySelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetCardiologyServicesEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetCardiologyServicesEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvCardiologyServices.Rows(selectedRow).Cells, Me.ColCardiologyNo)
            Me.SetCardiologyServicesEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetCardiologyServicesEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvCardiologyServices.Item(Me.colBillExcludedCardiologySaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Exam Code (" + Me._BedNo + ") can't be edited!")
                Me.dgvCardiologyServices.Item(Me.ColCardiologyNo.Name, selectedRow).Value = Me._CardiologyNo
                Me.dgvCardiologyServices.Item(Me.ColCardiologyNo.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvCardiologyServices.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvCardiologyServices.Rows(rowNo).Cells, Me.ColCardiologyNo)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Exam Code (" + enteredItem + ") already selected!")
                        Me.dgvCardiologyServices.Rows.RemoveAt(selectedRow)
                        Me.dgvCardiologyServices.Item(Me.ColCardiologyNo.Name, selectedRow).Value = Me._CardiologyNo
                        Me.dgvCardiologyServices.Item(Me.ColCardiologyNo.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredCardiologyServices(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredCardiologyServices(ByVal selectedRow As Integer)
        Try

            Dim oCardiologyExaminations As New SyncSoft.SQLDb.CardiologyExaminations()
            Dim examCode As String = String.Empty

            If Me.dgvCardiologyServices.Rows.Count > 1 Then examCode = SubstringRight(StringMayBeEnteredIn(Me.dgvCardiologyServices.Rows(selectedRow).Cells, Me.ColCardiologyNo))

            If String.IsNullOrEmpty(examCode) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim cardiologyExaminations As DataTable = oCardiologyExaminations.GetCardiologyExaminations(examCode).Tables("CardiologyExaminations")
            If cardiologyExaminations Is Nothing OrElse String.IsNullOrEmpty(examCode) Then Return
            Dim row As DataRow = cardiologyExaminations.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim examName As String = StringEnteredIn(row, "ExamName", "Exam Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvCardiologyServices
                .Item(Me.ColCardiologyNo.Name, selectedRow).Value = examCode.ToUpper()
                .Item(Me.colCardiologyName.Name, selectedRow).Value = examName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvCardiologyServices.Item(Me.ColCardiologyNo.Name, selectedRow).Value = Me._CardiologyNo.ToUpper()
            Throw ex

        End Try

    End Sub


#End Region

#Region " Consumable Items - Grid "

    Private Sub dgvExcludedConsumables_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvExcludedConsumables.CellBeginEdit

        If e.ColumnIndex <> Me.ColCardiologyNo.Index OrElse Me.dgvExcludedConsumables.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvExcludedConsumables.CurrentCell.RowIndex
        _ConsumableName = StringMayBeEnteredIn(Me.dgvExcludedConsumables.Rows(selectedRow).Cells, Me.colConsumableNo)

    End Sub

    Private Sub dgvExcludedConsumables_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvExcludedConsumables.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvExcludedConsumables.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.colConsumableNo.Index) Then

                If Me.dgvExcludedConsumables.Rows.Count > 1 Then Me.SetConsumableItemsEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub dgvExcludedConsumables_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvExcludedConsumables.UserDeletingRow

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvExcludedConsumables.Item(Me.colBillExcludedConsumableSaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvExcludedConsumables.Item(Me.colConsumableNo.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Consumable
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub dgvExcludedConsumables_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvExcludedConsumables.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub LoadBillExcludedConsumableItems(ByVal accountNo As String)

        Dim billExcludedServices As DataTable
        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvExcludedConsumables.Rows.Clear()


            billExcludedServices = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Consumable).Tables("BillExcludedItems")

            If billExcludedServices Is Nothing OrElse billExcludedServices.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvExcludedConsumables, billExcludedServices)

            For Each row As DataGridViewRow In Me.dgvExcludedConsumables.Rows
                If row.IsNewRow Then Exit For
                Me.dgvExcludedConsumables.Item(Me.colBillExcludedConsumableSaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub dgvExcludedConsumables_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvExcludedConsumables.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("ConsumableItems", "Consumable No", "Consumable Items", Me.GetConsumableItems(), "ConsumableFullName",
                                                                     "ConsumableNo", "ConsumableName", Me.dgvExcludedConsumables, Me.colConsumableNo, e.RowIndex)

            Me._CardiologyNo = StringMayBeEnteredIn(Me.dgvExcludedConsumables.Rows(e.RowIndex).Cells, Me.colConsumableNo)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColEXTConsumableSelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvExcludedConsumables.Rows(e.RowIndex).IsNewRow Then

                Me.dgvExcludedConsumables.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetConsumableItemsEntries(e.RowIndex)
            ElseIf Me.ColEXTConsumableSelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetConsumableItemsEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetConsumableItemsEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvExcludedConsumables.Rows(selectedRow).Cells, Me.colConsumableNo)
            Me.SetConsumableItemsEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetConsumableItemsEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvExcludedConsumables.Item(Me.colBillExcludedConsumableSaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Consumable Code (" + Me._BedNo + ") can't be edited!")
                Me.dgvExcludedConsumables.Item(Me.colConsumableNo.Name, selectedRow).Value = Me._CardiologyNo
                Me.dgvExcludedConsumables.Item(Me.colConsumableNo.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvExcludedConsumables.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvExcludedConsumables.Rows(rowNo).Cells, Me.colConsumableNo)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Consumable No (" + enteredItem + ") already selected!")
                        Me.dgvExcludedConsumables.Rows.RemoveAt(selectedRow)
                        Me.dgvExcludedConsumables.Item(Me.colConsumableNo.Name, selectedRow).Value = Me._CardiologyNo
                        Me.dgvExcludedConsumables.Item(Me.colConsumableNo.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredConsumableItems(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredConsumableItems(ByVal selectedRow As Integer)
        Try

            Dim oConsumableItems As New SyncSoft.SQLDb.ConsumableItems()
            Dim consumableNo As String = String.Empty

            If Me.dgvExcludedConsumables.Rows.Count > 1 Then consumableNo = SubstringRight(StringMayBeEnteredIn(Me.dgvExcludedConsumables.Rows(selectedRow).Cells, Me.colConsumableNo))

            If String.IsNullOrEmpty(consumableNo) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim consumableItems As DataTable = oConsumableItems.GetConsumableItems(consumableNo).Tables("ConsumableItems")
            If consumableItems Is Nothing OrElse String.IsNullOrEmpty(consumableNo) Then Return
            Dim row As DataRow = consumableItems.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim consumableName As String = StringEnteredIn(row, "ConsumableName", "Consumable Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvExcludedConsumables
                .Item(Me.colConsumableNo.Name, selectedRow).Value = consumableNo.ToUpper()
                .Item(Me.colConsumableName.Name, selectedRow).Value = consumableName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvExcludedConsumables.Item(Me.colConsumableNo.Name, selectedRow).Value = Me._ConsumableNo.ToUpper()
            Throw ex

        End Try

    End Sub


#End Region

#Region " Eye Services - Grid "

    Private Sub dgvEyeServices_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvEyeServices.CellBeginEdit

        If e.ColumnIndex <> Me.ColEyeServicesNo.Index OrElse Me.dgvEyeServices.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvEyeServices.CurrentCell.RowIndex
        _EyeServiceValue = StringMayBeEnteredIn(Me.dgvEyeServices.Rows(selectedRow).Cells, Me.ColEyeServicesNo)

    End Sub

    Private Sub dgvEyeServices_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEyeServices.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvEyeServices.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.ColEyeServicesNo.Index) Then

                If Me.dgvEyeServices.Rows.Count > 1 Then Me.SetEyeServicesEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub dgvEyeServices_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvEyeServices.UserDeletingRow

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvEyeServices.Item(Me.colBillExcludedEyeServicesSaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvEyeServices.Item(Me.ColEyeServicesNo.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Eye
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub dgvEyeServices_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvEyeServices.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub LoadBillExcludedEyeExaminations(ByVal accountNo As String)

        Dim billExcludedServices As DataTable
        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvEyeServices.Rows.Clear()


            billExcludedServices = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Eye).Tables("BillExcludedItems")

            If billExcludedServices Is Nothing OrElse billExcludedServices.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvEyeServices, billExcludedServices)

            For Each row As DataGridViewRow In Me.dgvEyeServices.Rows
                If row.IsNewRow Then Exit For
                Me.dgvEyeServices.Item(Me.colBillExcludedEyeServicesSaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub dgvEyeServices_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEyeServices.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("EyeServices", "Eye Code", "Eye Service", Me.GetEyeName(), "EyeFullName",
                                                                     "EyeCode", "EyeName", Me.dgvEyeServices, Me.ColEyeServicesNo, e.RowIndex)

            Me._EyeServicesNo = StringMayBeEnteredIn(Me.dgvEyeServices.Rows(e.RowIndex).Cells, Me.ColEyeServicesNo)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColEXTEyeServicesSelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvEyeServices.Rows(e.RowIndex).IsNewRow Then

                Me.dgvEyeServices.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetEyeServicesEntries(e.RowIndex)
            ElseIf Me.ColEXTEyeServicesSelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetEyeServicesEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetEyeServicesEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvEyeServices.Rows(selectedRow).Cells, Me.ColEyeServicesNo)
            Me.SetEyeServicesEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetEyeServicesEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvEyeServices.Item(Me.colBillExcludedEyeServicesSaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Eye Code (" + Me._BedNo + ") can't be edited!")
                Me.dgvEyeServices.Item(Me.ColEyeServicesNo.Name, selectedRow).Value = Me._EyeServicesNo
                Me.dgvEyeServices.Item(Me.ColEyeServicesNo.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvEyeServices.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvEyeServices.Rows(rowNo).Cells, Me.ColEyeServicesNo)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Eye Code (" + enteredItem + ") already selected!")
                        Me.dgvEyeServices.Rows.RemoveAt(selectedRow)
                        Me.dgvEyeServices.Item(Me.ColEyeServicesNo.Name, selectedRow).Value = Me._EyeServicesNo
                        Me.dgvEyeServices.Item(Me.ColEyeServicesNo.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredEyeServices(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredEyeServices(ByVal selectedRow As Integer)
        Try

            Dim oEyeServices As New SyncSoft.SQLDb.EyeServices()
            Dim eyeNo As String = String.Empty

            If Me.dgvEyeServices.Rows.Count > 1 Then eyeNo = SubstringRight(StringMayBeEnteredIn(Me.dgvEyeServices.Rows(selectedRow).Cells, Me.ColEyeServicesNo))

            If String.IsNullOrEmpty(eyeNo) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim EyeServices As DataTable = oEyeServices.GetEyeServices(eyeNo).Tables("EyeServices")
            If EyeServices Is Nothing OrElse String.IsNullOrEmpty(eyeNo) Then Return
            Dim row As DataRow = EyeServices.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim eyeName As String = StringEnteredIn(row, "EyeName", "Eye Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvEyeServices
                .Item(Me.ColEyeServicesNo.Name, selectedRow).Value = eyeNo.ToUpper()
                .Item(Me.colEyeServiceName.Name, selectedRow).Value = eyeName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvEyeServices.Item(Me.ColEyeServicesNo.Name, selectedRow).Value = Me._CardiologyNo.ToUpper()
            Throw ex

        End Try

    End Sub


#End Region

#Region " ICUService Items - Grid "

    Private Sub dgvExcludedICUServices_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvExcludedICUServices.CellBeginEdit

        If e.ColumnIndex <> Me.ColCardiologyNo.Index OrElse Me.dgvExcludedICUServices.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvExcludedICUServices.CurrentCell.RowIndex
        _ICUServiceName = StringMayBeEnteredIn(Me.dgvExcludedICUServices.Rows(selectedRow).Cells, Me.colICUServiceNo)

    End Sub

    Private Sub dgvExcludedICUServices_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvExcludedICUServices.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvExcludedICUServices.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.colICUServiceNo.Index) Then

                If Me.dgvExcludedICUServices.Rows.Count > 1 Then Me.SetICUServiceItemsEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub dgvExcludedICUServices_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvExcludedICUServices.UserDeletingRow

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvExcludedICUServices.Item(Me.colBillExcludedICUServiceSaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvExcludedICUServices.Item(Me.colICUServiceNo.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.ICU
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub dgvExcludedICUServices_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvExcludedICUServices.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub LoadBillExcludedICUServiceItems(ByVal accountNo As String)

        Dim billExcludedServices As DataTable
        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvExcludedICUServices.Rows.Clear()


            billExcludedServices = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.ICU).Tables("BillExcludedItems")

            If billExcludedServices Is Nothing OrElse billExcludedServices.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvExcludedICUServices, billExcludedServices)

            For Each row As DataGridViewRow In Me.dgvExcludedICUServices.Rows
                If row.IsNewRow Then Exit For
                Me.dgvExcludedICUServices.Item(Me.colBillExcludedICUServiceSaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub dgvExcludedICUServices_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvExcludedICUServices.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("ICUServices", "ICU Code", "ICU Name", Me.GetICUService(), "ICUFullName",
                                                                     "ICUCode", "ICUName", Me.dgvExcludedICUServices, Me.colICUServiceNo, e.RowIndex)

            Me._ICUServiceNo = StringMayBeEnteredIn(Me.dgvExcludedICUServices.Rows(e.RowIndex).Cells, Me.colICUServiceNo)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColEXTICUServiceSelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvExcludedICUServices.Rows(e.RowIndex).IsNewRow Then

                Me.dgvExcludedICUServices.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetICUServiceItemsEntries(e.RowIndex)
            ElseIf Me.ColEXTICUServiceSelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetICUServiceItemsEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetICUServiceItemsEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvExcludedICUServices.Rows(selectedRow).Cells, Me.colICUServiceNo)
            Me.SetICUServiceItemsEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetICUServiceItemsEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvExcludedICUServices.Item(Me.colBillExcludedICUServiceSaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("ICUService No (" + Me._ICUServiceNo + ") can't be edited!")
                Me.dgvExcludedICUServices.Item(Me.colICUServiceNo.Name, selectedRow).Value = Me._ICUServiceNo
                Me.dgvExcludedICUServices.Item(Me.colICUServiceNo.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvExcludedICUServices.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvExcludedICUServices.Rows(rowNo).Cells, Me.colICUServiceNo)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("ICUService No (" + enteredItem + ") already selected!")
                        Me.dgvExcludedICUServices.Rows.RemoveAt(selectedRow)
                        Me.dgvExcludedICUServices.Item(Me.colICUServiceNo.Name, selectedRow).Value = Me._ICUServiceNo
                        Me.dgvExcludedICUServices.Item(Me.colICUServiceNo.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredICUServiceItems(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredICUServiceItems(ByVal selectedRow As Integer)
        Try

            Dim oICUServiceItems As New SyncSoft.SQLDb.ICUServices()
            Dim ICUServiceNo As String = String.Empty

            If Me.dgvExcludedICUServices.Rows.Count > 1 Then ICUServiceNo = SubstringRight(StringMayBeEnteredIn(Me.dgvExcludedICUServices.Rows(selectedRow).Cells, Me.colICUServiceNo))

            If String.IsNullOrEmpty(ICUServiceNo) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim ICUServiceItems As DataTable = oICUServiceItems.GetICUServices(ICUServiceNo).Tables("ICUServices")
            If ICUServiceItems Is Nothing OrElse String.IsNullOrEmpty(ICUServiceNo) Then Return
            Dim row As DataRow = ICUServiceItems.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim ICUServiceName As String = StringEnteredIn(row, "ICUName", "ICU Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvExcludedICUServices
                .Item(Me.colICUServiceNo.Name, selectedRow).Value = ICUServiceNo.ToUpper()
                .Item(Me.colICUServiceName.Name, selectedRow).Value = ICUServiceName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvExcludedICUServices.Item(Me.colICUServiceNo.Name, selectedRow).Value = Me._ICUServiceNo.ToUpper()
            Throw ex

        End Try

    End Sub


#End Region

#Region " MaternityServices Items - Grid "

    Private Sub dgvExcludedMaternityServices_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvExcludedMaternityServices.CellBeginEdit

        If e.ColumnIndex <> Me.ColCardiologyNo.Index OrElse Me.dgvExcludedMaternityServices.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvExcludedMaternityServices.CurrentCell.RowIndex
        _MaternityServicesName = StringMayBeEnteredIn(Me.dgvExcludedMaternityServices.Rows(selectedRow).Cells, Me.colMaternityServicesNo)

    End Sub

    Private Sub dgvExcludedMaternityServices_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvExcludedMaternityServices.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvExcludedMaternityServices.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.colMaternityServicesNo.Index) Then

                If Me.dgvExcludedMaternityServices.Rows.Count > 1 Then Me.SetMaternityServicesItemsEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub dgvExcludedMaternityServices_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvExcludedMaternityServices.UserDeletingRow

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvExcludedMaternityServices.Item(Me.colBillExcludedMaternityServicesSaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvExcludedMaternityServices.Item(Me.colMaternityServicesNo.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Maternity
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub dgvExcludedMaternityServices_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvExcludedMaternityServices.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub LoadBillExcludedMaternityServicesItems(ByVal accountNo As String)

        Dim billExcludedServices As DataTable
        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvExcludedMaternityServices.Rows.Clear()


            billExcludedServices = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Maternity).Tables("BillExcludedItems")

            If billExcludedServices Is Nothing OrElse billExcludedServices.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvExcludedMaternityServices, billExcludedServices)

            For Each row As DataGridViewRow In Me.dgvExcludedMaternityServices.Rows
                If row.IsNewRow Then Exit For
                Me.dgvExcludedMaternityServices.Item(Me.colBillExcludedMaternityServicesSaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub dgvExcludedMaternityServices_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvExcludedMaternityServices.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("Maternity Services", "Maternity Code", "Maternity Name", Me.GetMaternityService(), "MaternityServiceFullName",
                                                                     "MaternityCode", "MaternityName", Me.dgvExcludedMaternityServices, Me.colMaternityServicesNo, e.RowIndex)

            Me._CardiologyNo = StringMayBeEnteredIn(Me.dgvExcludedMaternityServices.Rows(e.RowIndex).Cells, Me.colMaternityServicesNo)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColEXTMaternityServicesSelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvExcludedMaternityServices.Rows(e.RowIndex).IsNewRow Then

                Me.dgvExcludedMaternityServices.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetMaternityServicesItemsEntries(e.RowIndex)
            ElseIf Me.ColEXTMaternityServicesSelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetMaternityServicesItemsEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetMaternityServicesItemsEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvExcludedMaternityServices.Rows(selectedRow).Cells, Me.colMaternityServicesNo)
            Me.SetMaternityServicesItemsEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetMaternityServicesItemsEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvExcludedMaternityServices.Item(Me.colBillExcludedMaternityServicesSaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Maternity Code (" + Me._MaternityServicesNo + ") can't be edited!")
                Me.dgvExcludedMaternityServices.Item(Me.colMaternityServicesNo.Name, selectedRow).Value = Me._MaternityServicesNo
                Me.dgvExcludedMaternityServices.Item(Me.colMaternityServicesNo.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvExcludedMaternityServices.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvExcludedMaternityServices.Rows(rowNo).Cells, Me.colMaternityServicesNo)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Maternity Code (" + enteredItem + ") already selected!")
                        Me.dgvExcludedMaternityServices.Rows.RemoveAt(selectedRow)
                        Me.dgvExcludedMaternityServices.Item(Me.colMaternityServicesNo.Name, selectedRow).Value = Me._MaternityServicesNo
                        Me.dgvExcludedMaternityServices.Item(Me.colMaternityServicesNo.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredMaternityServicesItems(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredMaternityServicesItems(ByVal selectedRow As Integer)
        Try

            Dim oMaternityServicesItems As New SyncSoft.SQLDb.MaternityServices()
            Dim MaternityServicesNo As String = String.Empty

            If Me.dgvExcludedMaternityServices.Rows.Count > 1 Then MaternityServicesNo = SubstringRight(StringMayBeEnteredIn(Me.dgvExcludedMaternityServices.Rows(selectedRow).Cells, Me.colMaternityServicesNo))

            If String.IsNullOrEmpty(MaternityServicesNo) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim MaternityServicesItems As DataTable = oMaternityServicesItems.GetMaternityServices(MaternityServicesNo).Tables("MaternityServices")
            If MaternityServicesItems Is Nothing OrElse String.IsNullOrEmpty(MaternityServicesNo) Then Return
            Dim row As DataRow = MaternityServicesItems.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim MaternityServicesName As String = StringEnteredIn(row, "MaternityName", "Maternity Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvExcludedMaternityServices
                .Item(Me.colMaternityServicesNo.Name, selectedRow).Value = MaternityServicesNo.ToUpper()
                .Item(Me.colMaternityServicesName.Name, selectedRow).Value = MaternityServicesName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvExcludedMaternityServices.Item(Me.colMaternityServicesNo.Name, selectedRow).Value = Me._MaternityServicesNo.ToUpper()
            Throw ex

        End Try

    End Sub


#End Region

#Region " Packages Items - Grid "

    Private Sub dgvExcludedPackages_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvExcludedPackages.CellBeginEdit

        If e.ColumnIndex <> Me.ColCardiologyNo.Index OrElse Me.dgvExcludedPackages.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvExcludedPackages.CurrentCell.RowIndex
        _PackagesName = StringMayBeEnteredIn(Me.dgvExcludedPackages.Rows(selectedRow).Cells, Me.colPackagesNo)

    End Sub

    Private Sub dgvExcludedPackages_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvExcludedPackages.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvExcludedPackages.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.colPackagesNo.Index) Then

                If Me.dgvExcludedPackages.Rows.Count > 1 Then Me.SetPackagesItemsEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub dgvExcludedPackages_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvExcludedPackages.UserDeletingRow

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvExcludedPackages.Item(Me.colBillExcludedPackagesSaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvExcludedPackages.Item(Me.colPackagesNo.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Packages
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub dgvExcludedPackages_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvExcludedPackages.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub LoadBillExcludedPackagesItems(ByVal accountNo As String)

        Dim billExcludedServices As DataTable
        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvExcludedPackages.Rows.Clear()


            billExcludedServices = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Packages).Tables("BillExcludedItems")

            If billExcludedServices Is Nothing OrElse billExcludedServices.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvExcludedPackages, billExcludedServices)

            For Each row As DataGridViewRow In Me.dgvExcludedPackages.Rows
                If row.IsNewRow Then Exit For
                Me.dgvExcludedPackages.Item(Me.colBillExcludedPackagesSaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub dgvExcludedPackages_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvExcludedPackages.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("Packages", "Package No", "Package Name", Me.GetPackages(), "PackageFullName",
                                                                     "PackageNo", "PackageName", Me.dgvExcludedPackages, Me.colPackagesNo, e.RowIndex)

            Me._CardiologyNo = StringMayBeEnteredIn(Me.dgvExcludedPackages.Rows(e.RowIndex).Cells, Me.colPackagesNo)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColEXTPackagesSelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvExcludedPackages.Rows(e.RowIndex).IsNewRow Then

                Me.dgvExcludedPackages.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetPackagesItemsEntries(e.RowIndex)
            ElseIf Me.ColEXTPackagesSelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetPackagesItemsEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetPackagesItemsEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvExcludedPackages.Rows(selectedRow).Cells, Me.colPackagesNo)
            Me.SetPackagesItemsEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetPackagesItemsEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvExcludedPackages.Item(Me.colBillExcludedPackagesSaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Package No (" + Me._BedNo + ") can't be edited!")
                Me.dgvExcludedPackages.Item(Me.colPackagesNo.Name, selectedRow).Value = Me._PackagesNo
                Me.dgvExcludedPackages.Item(Me.colPackagesNo.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvExcludedPackages.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvExcludedPackages.Rows(rowNo).Cells, Me.colPackagesNo)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Package No (" + enteredItem + ") already selected!")
                        Me.dgvExcludedPackages.Rows.RemoveAt(selectedRow)
                        Me.dgvExcludedPackages.Item(Me.colPackagesNo.Name, selectedRow).Value = Me._PackagesNo
                        Me.dgvExcludedPackages.Item(Me.colPackagesNo.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredPackagesItems(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredPackagesItems(ByVal selectedRow As Integer)
        Try

            Dim oPackagesItems As New SyncSoft.SQLDb.Packages()
            Dim PackagesNo As String = String.Empty

            If Me.dgvExcludedPackages.Rows.Count > 1 Then PackagesNo = SubstringRight(StringMayBeEnteredIn(Me.dgvExcludedPackages.Rows(selectedRow).Cells, Me.colPackagesNo))

            If String.IsNullOrEmpty(PackagesNo) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim PackagesItems As DataTable = oPackagesItems.GetPackages(PackagesNo).Tables("Packages")
            If PackagesItems Is Nothing OrElse String.IsNullOrEmpty(PackagesNo) Then Return
            Dim row As DataRow = PackagesItems.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim PackagesName As String = StringEnteredIn(row, "PackageName", "Package Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvExcludedPackages
                .Item(Me.colPackagesNo.Name, selectedRow).Value = PackagesNo.ToUpper()
                .Item(Me.colPackagesName.Name, selectedRow).Value = PackagesName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvExcludedPackages.Item(Me.colPackagesNo.Name, selectedRow).Value = Me._PackagesNo.ToUpper()
            Throw ex

        End Try

    End Sub


#End Region

#Region " TheatreServices Items - Grid "

    Private Sub dgvExcludedTheatreServices_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvExcludedTheatreServices.CellBeginEdit

        If e.ColumnIndex <> Me.ColCardiologyNo.Index OrElse Me.dgvExcludedTheatreServices.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvExcludedTheatreServices.CurrentCell.RowIndex
        _TheatreServicesName = StringMayBeEnteredIn(Me.dgvExcludedTheatreServices.Rows(selectedRow).Cells, Me.colTheatreServicesNo)

    End Sub

    Private Sub dgvExcludedTheatreServices_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvExcludedTheatreServices.CellEndEdit

        Try
            Dim selectedRow As Integer = Me.dgvExcludedTheatreServices.CurrentCell.RowIndex
            If e.ColumnIndex.Equals(Me.colTheatreServicesNo.Index) Then

                If Me.dgvExcludedTheatreServices.Rows.Count > 1 Then Me.SetTheatreServicesItemsEntries(selectedRow)


            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub dgvExcludedTheatreServices_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvExcludedTheatreServices.UserDeletingRow

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
            Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()

            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvExcludedTheatreServices.Item(Me.colBillExcludedTheatreServicesSaved.Name, toDeleteRowNo).Value) = False Then Return

            Dim accountNo As String = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No!"))
            Dim itemCode As String = CStr(Me.dgvExcludedTheatreServices.Item(Me.colTheatreServicesNo.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oBillExcludedItems
                .AccountNo = accountNo
                .ItemCode = itemCode
                .ItemCategoryID = oItemCategoryID.Theatre
            End With

            DisplayMessage(oBillExcludedItems.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub dgvExcludedTheatreServices_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvExcludedTheatreServices.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub LoadBillExcludedTheatreServicesItems(ByVal accountNo As String)

        Dim billExcludedServices As DataTable
        Dim oBillExcludedItems As New SyncSoft.SQLDb.BillExcludedItems()
        Dim oItemCategoryID As New LookupDataID.ItemCategoryID()

        Try

            Me.dgvExcludedTheatreServices.Rows.Clear()


            billExcludedServices = oBillExcludedItems.GetBillExcludedItems(accountNo, oItemCategoryID.Theatre).Tables("BillExcludedItems")

            If billExcludedServices Is Nothing OrElse billExcludedServices.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvExcludedTheatreServices, billExcludedServices)

            For Each row As DataGridViewRow In Me.dgvExcludedTheatreServices.Rows
                If row.IsNewRow Then Exit For
                Me.dgvExcludedTheatreServices.Item(Me.colBillExcludedTheatreServicesSaved.Name, row.Index).Value = True
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub dgvExcludedTheatreServices_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvExcludedTheatreServices.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fSelectItem As New SyncSoft.SQL.Win.Forms.SelectItem("Theatre Services", "Theatre Code", "Theatre Name", Me.GetTheatreServices(), "TheatreServicesFullName",
                                                                     "TheatreCode", "TheatreName", Me.dgvExcludedTheatreServices, Me.colTheatreServicesNo, e.RowIndex)

            Me._CardiologyNo = StringMayBeEnteredIn(Me.dgvExcludedTheatreServices.Rows(e.RowIndex).Cells, Me.colTheatreServicesNo)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColEXTTheatreServicesSelect.Index.Equals(e.ColumnIndex) AndAlso Me.dgvExcludedTheatreServices.Rows(e.RowIndex).IsNewRow Then

                Me.dgvExcludedTheatreServices.Rows.Add()

                fSelectItem.ShowDialog(Me)
                Me.SetTheatreServicesItemsEntries(e.RowIndex)
            ElseIf Me.ColEXTTheatreServicesSelect.Index.Equals(e.ColumnIndex) Then

                fSelectItem.ShowDialog(Me)
                Me.SetTheatreServicesItemsEntries(e.RowIndex)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetTheatreServicesItemsEntries(ByVal selectedRow As Integer)

        Try

            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvExcludedTheatreServices.Rows(selectedRow).Cells, Me.colTheatreServicesNo)
            Me.SetTheatreServicesItemsEntries(selectedRow, selectedItem)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetTheatreServicesItemsEntries(ByVal selectedRow As Integer, selectedItem As String)

        Try
            If CBool(Me.dgvExcludedTheatreServices.Item(Me.colBillExcludedTheatreServicesSaved.Name, selectedRow).Value).Equals(True) Then
                DisplayMessage("Theatre Service Code (" + Me._TheatreServicesNo + ") can't be edited!")
                Me.dgvExcludedTheatreServices.Item(Me.colTheatreServicesNo.Name, selectedRow).Value = Me._TheatreServicesNo
                Me.dgvExcludedTheatreServices.Item(Me.colTheatreServicesNo.Name, selectedRow).Selected = True
                Return
            End If

            For rowNo As Integer = 0 To Me.dgvExcludedTheatreServices.RowCount - 2
                If Not rowNo.Equals(selectedRow) Then
                    Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvExcludedTheatreServices.Rows(rowNo).Cells, Me.colTheatreServicesNo)
                    If enteredItem.ToUpper().Equals(selectedItem.ToUpper()) Then
                        DisplayMessage("Theatre Service Code (" + enteredItem + ") already selected!")
                        Me.dgvExcludedTheatreServices.Rows.RemoveAt(selectedRow)
                        Me.dgvExcludedTheatreServices.Item(Me.colTheatreServicesNo.Name, selectedRow).Value = Me._TheatreServicesNo
                        Me.dgvExcludedTheatreServices.Item(Me.colTheatreServicesNo.Name, selectedRow).Selected = True


                    End If
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Populate other columns based upon what is entered in combo column
            Me.DetailEnteredTheatreServicesItems(selectedRow)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub DetailEnteredTheatreServicesItems(ByVal selectedRow As Integer)
        Try

            Dim oTheatreServicesItems As New SyncSoft.SQLDb.TheatreServices()
            Dim TheatreServicesNo As String = String.Empty

            If Me.dgvExcludedTheatreServices.Rows.Count > 1 Then TheatreServicesNo = SubstringRight(StringMayBeEnteredIn(Me.dgvExcludedTheatreServices.Rows(selectedRow).Cells, Me.colTheatreServicesNo))

            If String.IsNullOrEmpty(TheatreServicesNo) Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim TheatreServicesItems As DataTable = oTheatreServicesItems.GetTheatreServices(TheatreServicesNo).Tables("TheatreServices")
            If TheatreServicesItems Is Nothing OrElse String.IsNullOrEmpty(TheatreServicesNo) Then Return
            Dim row As DataRow = TheatreServicesItems.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim TheatreServicesName As String = StringEnteredIn(row, "TheatreName", "Theatre Name!")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            With Me.dgvExcludedTheatreServices
                .Item(Me.colTheatreServicesNo.Name, selectedRow).Value = TheatreServicesNo.ToUpper()
                .Item(Me.colTheatreServicesName.Name, selectedRow).Value = TheatreServicesName

            End With


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.dgvExcludedTheatreServices.Item(Me.colTheatreServicesNo.Name, selectedRow).Value = Me._TheatreServicesNo.ToUpper()
            Throw ex

        End Try

    End Sub


#End Region


#Region " Edit Methods "

    Public Sub Edit()

        Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update
        Me.ebnSaveUpdate.Enabled = False
        Me.fbnDelete.Visible = True
        Me.fbnDelete.Enabled = False
        Me.btnSearch.Visible = True

        Me.cboAccountNo.Enabled = True
        Me.cboAccountNo.MaxLength = 64
        Me.LoadBillCustomers(True)
        Me.chkHidden.Enabled = True

        Me.pnlStatusID.Enabled = True

        ResetControlsIn(Me)
        ResetControlsIn(Me.tpgBillExcludedServices)
        ResetControlsIn(Me.tpgBillExcludedDrugs)
        ResetControlsIn(Me.tpgBillExcludedLabTests)
        ResetControlsIn(Me.tpgBillExcludedRadiology)
        ResetControlsIn(Me.tpgBillExcludedProcedures)
        ResetControlsIn(Me.tpgAssociatedBillCustomers)
        ResetControlsIn(Me.tpgBillExcludedExtraChargeItems)
        ResetControlsIn(Me.tpgBillExcludedOpticalservices)
        ResetControlsIn(Me.tpgBillExcludedPathology)
        ResetControlsIn(Me.tpgBillExcludedDentalServices)
        ResetControlsIn(Me.pnlCoPayTypeID)
        ResetControlsIn(Me.pnlStatusID)

        Me.ResetCoPayControls()
        Me.EnableStateControls(False)

    End Sub

    Public Sub Save()

        Me.ebnSaveUpdate.ButtonText = ButtonCaption.Save
        Me.ebnSaveUpdate.Enabled = True
        Me.fbnDelete.Visible = False
        Me.fbnDelete.Enabled = True
        Me.btnSearch.Visible = False

        Me.cboAccountNo.Enabled = Not InitOptions.AccountNoLocked
        Me.cboAccountNo.MaxLength = 20
        Me.LoadBillCustomers(False)
        Me.chkHidden.Enabled = False

        Me.pnlStatusID.Enabled = False

        ResetControlsIn(Me)
        ResetControlsIn(Me.tpgBillExcludedServices)
        ResetControlsIn(Me.tpgBillExcludedDrugs)
        ResetControlsIn(Me.tpgBillExcludedLabTests)
        ResetControlsIn(Me.tpgBillExcludedRadiology)
        ResetControlsIn(Me.tpgBillExcludedProcedures)
        ResetControlsIn(Me.tpgAssociatedBillCustomers)
        ResetControlsIn(Me.tpgBillExcludedExtraChargeItems)
        ResetControlsIn(Me.tpgBillExcludedOpticalservices)
        ResetControlsIn(Me.tpgBillExcludedPathology)
        ResetControlsIn(Me.tpgBillExcludedDentalServices)

        Me.SetNextAccountNo()
        Me.SetCoPayDefault()
        Me.ResetCoPayControls()
        Me.EnableStateControls(True)
        Me.AccountStatus()

    End Sub

    Private Sub DisplayData(ByVal dataSource As DataTable)

        Try

            Me.ebnSaveUpdate.DataSource = dataSource
            Me.ebnSaveUpdate.LoadData(Me)

            Me.ebnSaveUpdate.Enabled = dataSource.Rows.Count > 0
            Me.fbnDelete.Enabled = dataSource.Rows.Count > 0

            Security.Apply(Me.ebnSaveUpdate, AccessRights.Update)
            Security.Apply(Me.fbnDelete, AccessRights.Delete)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub CallOnKeyEdit()
        If Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update Then
            Me.ebnSaveUpdate.Enabled = False
            Me.fbnDelete.Enabled = False
        End If
    End Sub

#End Region

End Class