﻿
Option Strict On

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls
Imports SyncSoft.Common.SQL.Enumerations
Imports LookupDataID = SyncSoft.SQLDb.Lookup.LookupDataID
Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects
Imports SyncSoft.SQLDb.Lookup.LookupDataID
Imports System.Collections.Generic
Imports SyncSoft.Common.SQL.Classes
Imports SyncSoft.SQLDb
Imports System.Drawing.Printing

Public Class frmPatientsTriage

#Region "Fields"
    Private PatientNo As String = String.Empty
#End Region


    Private Sub frmPatientsTriage_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            If Not String.IsNullOrEmpty(PatientNo) Then
                Me.stbPatientNo.Text = FormatText(PatientNo, "Patients", "PatientNo")
                Me.ShowPatientDetails(PatientNo)
                Me.ProcessTabKey(True)
            End If
        Catch ex As Exception
            ErrorMessage(ex)
        End Try
    End Sub

    Private Sub fbnClose_Click(sender As System.Object, e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub

    Private Sub ShowPatientDetails(ByVal patientNo As String)

        Dim oPatients As New SyncSoft.SQLDb.Patients()
       
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim patients As DataTable = oPatients.GetPatients(patientNo).Tables("Patients")
            Dim row As DataRow = patients.Rows(0)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbPatientNo.Text = FormatText(patientNo, "Patients", "PatientNo")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbFullName.Text = StringEnteredIn(row, "FullName")
            Me.stbGender.Text = StringEnteredIn(row, "Gender")

            Me.stbAge.Text = StringEnteredIn(row, "Age")
            Dim birthDate As Date = DateMayBeEnteredIn(row, "BirthDate")

            Me.stbJoinDate.Text = FormatDate(DateEnteredIn(row, "JoinDate"))
            Me.stbPhone.Text = StringMayBeEnteredIn(row, "Phone")
            Me.stbLastVisitDate.Text = FormatDate(DateMayBeEnteredIn(row, "LastVisitDate"))
            Me.stbTotalVisits.Text = StringEnteredIn(row, "TotalVisits")
            Me.spbPhoto.Image = ImageMayBeEnteredIn(row, "Photo")

            Me.LoadPatientTriage(patientNo)

        Catch eX As Exception

            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub LoadPatientTriage(ByVal patientNo As String)

        Dim oTriage As New SyncSoft.SQLDb.Triage()

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim Triage As DataTable = oTriage.GetTriageByPatientNo(patientNo).Tables("Triage")
            If Triage Is Nothing OrElse Triage.Rows.Count < 1 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            For pos As Integer = 0 To Triage.Rows.Count - 1

                Dim row As DataRow = Triage.Rows(pos)

                With Me.dgvTriage
                    ' Ensure that you add a new row
                    .Rows.Add()
                    .Item(Me.colVisitNo.Name, pos).Value = StringEnteredIn(row, "VisitNo")
                    .Item(Me.colVisitDate.Name, pos).Value = StringMayBeEnteredIn(row, "VisitDate")
                    .Item(Me.colWeight.Name, pos).Value = StringMayBeEnteredIn(row, "Weight")
                    .Item(Me.colHeight.Name, pos).Value = StringMayBeEnteredIn(row, "Height")
                    .Item(Me.colTemperature.Name, pos).Value = StringMayBeEnteredIn(row, "Temperature")
                    .Item(Me.colMUAC.Name, pos).Value = StringMayBeEnteredIn(row, "MUAC")
                    .Item(Me.colBloodPressure.Name, pos).Value = StringMayBeEnteredIn(row, "BloodPressure")
                    .Item(Me.colHeadCircum.Name, pos).Value = StringMayBeEnteredIn(row, "HeadCircum")
                    .Item(Me.colOxygenSaturation.Name, pos).Value = StringMayBeEnteredIn(row, "OxygenSaturation")
                    .Item(Me.colNotes.Name, pos).Value = StringMayBeEnteredIn(row, "Notes")

                End With
            Next

        Catch eX As Exception

            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub
End Class