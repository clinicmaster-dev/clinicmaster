
Option Strict On

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls

Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects

Public Class frmPeriodicAntenatalVisits

#Region " Fields "

    Dim VisitNoControl As Control
    Dim ANCNoControl As Control
#End Region


    Private Sub frmPeriodicAntenatalVisits_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.Cursor = Cursors.WaitCursor()

            Me.Text = "Periodic Visits"

            Me.dtpStartDate.MinDate = CDate("01/01/1900")
            Me.dtpStartDate.MaxDate = Today
            Me.dtpEndDate.MaxDate = Today

            Me.ShowPeriodicAntenatalVisits(Today, Today)


            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub ShowPeriodicAntenatalVisits(ByVal startDate As Date, ByVal endDate As Date)

        Dim oAntenatalEnrollment As New SyncSoft.SQLDb.AntenatalEnrollment()

        Try
            Me.Cursor = Cursors.WaitCursor

            ' Load from Visits
            Dim visits As DataTable = oAntenatalEnrollment.GetPeriodicAntenatalEnrollment(startDate, endDate).Tables("AntenatalEnrollment")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvPeriodicAntenatalVisits, visits)

            Dim message As String = "No " + Me.Text + " record(s) found for period between " _
                                    + FormatDate(startDate) + " and " + FormatDate(endDate) + "!"

            If visits.Rows.Count < 1 Then DisplayMessage(message)
            Me.lblRecordsNo.Text = " Returned Record(s): " + visits.Rows.Count.ToString()
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


    Private Sub frmPeriodicAntenatalVisits_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub

    Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub


    Private Sub fbnLoad_Click(sender As System.Object, e As System.EventArgs) Handles fbnLoad.Click

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim startDate As Date = DateEnteredIn(Me.dtpStartDate, "Start Date")
            Dim endDate As Date = DateEnteredIn(Me.dtpEndDate, "End Date")

            If endDate < startDate Then Throw New ArgumentException("End Date can't be before Start Date!")
            Me.ShowPeriodicAntenatalVisits(startDate, endDate)

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub



    Private Sub dgvPeriodicAntenatalVisits_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPeriodicAntenatalVisits.CellDoubleClick
        Try

            Dim _VisitNo As String = Me.dgvPeriodicAntenatalVisits.Item(Me.colVisitNo.Name, e.RowIndex).Value.ToString()

            Dim _ANCNo As String = Me.dgvPeriodicAntenatalVisits.Item(Me.colANCNo.Name, e.RowIndex).Value.ToString()

            If TypeOf Me.VisitNoControl Is TextBox Then
                CType(Me.VisitNoControl, TextBox).Text = _VisitNo
                CType(Me.ANCNoControl, TextBox).Text = _ANCNo
                CType(Me.ANCNoControl, TextBox).Focus()

            ElseIf TypeOf Me.VisitNoControl Is SmartTextBox Then
                CType(Me.VisitNoControl, SmartTextBox).Text = _VisitNo
                CType(Me.ANCNoControl, TextBox).Text = _ANCNo
                CType(Me.ANCNoControl, SmartTextBox).Focus()

            ElseIf TypeOf Me.VisitNoControl Is ComboBox Then
                CType(Me.VisitNoControl, ComboBox).Text = _VisitNo
                CType(Me.ANCNoControl, ComboBox).Text = _ANCNo
                CType(Me.ANCNoControl, ComboBox).Focus()
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Me.Close()

        Catch ex As Exception
            Return
        End Try
    End Sub
End Class