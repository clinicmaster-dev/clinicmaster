
Option Strict On
Imports SyncSoft.SQLDb
Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.SQL.Classes
Imports SyncSoft.Common.Win.Controls
Imports SyncSoft.Common.Enumerations
Imports SyncSoft.Common.SQL.Enumerations

Imports System.Collections.Generic

Imports LookupData = SyncSoft.Lookup.SQL.LookupData
Imports LookupDataID = SyncSoft.SQLDb.Lookup.LookupDataID

Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects
Imports LookupCommObjects = SyncSoft.Common.Lookup.LookupCommObjects
Imports LookupCommDataID = SyncSoft.Common.Lookup.LookupCommDataID

Public Class frmAntenatalEnrollment

#Region " Fields "
    Private antenatalEnrollmentSaved As Boolean = True
    Private oBloodTransfusionID As New LookupDataID.YesNoID
    Private oAbortionID As New LookupDataID.YesNoID
    Dim oStatusID As New LookupCommDataID.StatusID()
    'Private oCurrentANCVisit As CurrentPatient

    Private allergies As DataTable
    Private _AllergyNoValue As String = String.Empty
#End Region

    Private Sub frmAntenatalEnrollment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.Cursor = Cursors.WaitCursor()
            Dim oYesNoID As New LookupDataID.YesNoID

            LoadLookupDataCombo(Me.cboCycleRegularID, LookupObjects.YesNo, False)
            LoadLookupDataCombo(Me.cboDonePregnancyScanID, LookupObjects.YesNo, False)
            LoadLookupDataCombo(Me.cboHIVStatusID, LookupObjects.HIVStatus, False)
            LoadLookupDataCombo(Me.cboPartnersHIVStatusID, LookupObjects.HIVStatus, False)
            LoadLookupDataCombo(Me.clbMedicalHistory, LookupObjects.MedicalHistory, False)
            LoadLookupDataCombo(Me.cboBloodTransfusion, LookupObjects.YesNo, False)
            LoadLookupDataCombo(Me.clbFamilyHistory, LookupObjects.FamilyHistory, False)
            LoadLookupDataCombo(Me.clbSocialHistory, LookupObjects.SocialHistory, False)
            LoadLookupDataCombo(Me.clbGynaecologicalHistory, LookupObjects.GynaecologicalHistory, False)
            LoadLookupDataCombo(Me.clbSurgicalHistory, LookupObjects.SurgicalHistory, False)

            LoadLookupDataCombo(Me.colObstetricAbortionID, LookupObjects.YesNo, False)
            LoadLookupDataCombo(Me.colObstetricAbortionPeriodID, LookupObjects.AbortionPeriod, False)
            LoadLookupDataCombo(Me.colObstetricThirdStageID, LookupObjects.ThirdStage, False)
            LoadLookupDataCombo(Me.colObstetricPeurPerinumID, LookupObjects.PeurPerinum, False)
            LoadGender()
            LoadLookupDataCombo(Me.colObstetricChildStatusID, LookupObjects.ChildStatus, False)
            LoadLookupDataCombo(Me.colTypeOfDelivery, LookupObjects.TypeOfDelivery, False)

            LoadLookupDataCombo(Me.colContraceptiveID, LookupObjects.FPMethods, False)
            LoadLookupDataCombo(Me.colContraceptiveDiscontinued, LookupObjects.YesNo, False)
            LoadLookupDataCombo(Me.colContraceptiveRemovalReasons, LookupObjects.ReasonsForRemoval, False)


            Me.cboDonePregnancyScanID.SelectedValue = oYesNoID.NA

            Me.LoadAllergies()
            Me.PatientStatus()

            Dim lYears As New List(Of LookupData)
            For year As Integer = Today.Year To 1960 Step -1
                Using oLookupData As New LookupData
                    With oLookupData
                        .DataDes = year.ToString()
                        .ObjectID = year
                    End With
                    lYears.Add(oLookupData)
                End Using

            Next
            Me.colObstetricYear.DataSource = lYears
            Me.colObstetricYear.DisplayMember = "DataDes"
            Me.colObstetricYear.ValueMember = "ObjectID"

            Me.fcbStatusID.Enabled = False
            Me.chkCreateNewEnrollment.Checked = True
            Me.btnFindANCNo.Enabled = False
            Me.stbANCNo.Enabled = False

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub LoadGender()
        Dim oLookupData As New LookupData()

        Try
            Me.Cursor = Cursors.WaitCursor

            ' Load from LookupData'
            Dim lookupData As DataTable = oLookupData.GetLookupData(LookupObjects.Gender).Tables("LookupData")

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With colObstetricGenderID
                .DataSource = lookupData
                .ValueMember = "DataID"
                .DisplayMember = "DataDes"
            End With

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


    Private Sub frmAntenatalEnrollment_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub

    Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub fbnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnDelete.Click

        Dim oAntenatalEnrollment As New SyncSoft.SQLDb.AntenatalEnrollment()

        Try
            Me.Cursor = Cursors.WaitCursor()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then Return

            oAntenatalEnrollment.ANCNo = RevertText(StringEnteredIn(Me.stbANCNo, "ANC No!"))

            DisplayMessage(oAntenatalEnrollment.Delete())
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            ResetControlsIn(Me)
            Me.CallOnKeyEdit()
            Me.navEnrollments.Clear()
            Me.chkNavigateEnrollments.Checked = False

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub nbxPara_Enter(sender As System.Object, e As System.EventArgs) Handles nbxPara.Enter

        Try

            Dim gravida As Integer = Me.nbxGravida.GetInteger()
            Me.nbxPara.MaxValue = gravida

        Catch ex As Exception
            Me.nbxPara.MaxValue = 0
        End Try

    End Sub

    Private Sub LoadANCDetails()

        Dim ANCNo As String

        Dim oAntenatalEnrollment As New SyncSoft.SQLDb.AntenatalEnrollment()

        Try
            Me.Cursor = Cursors.WaitCursor()
            Me.ClearControls()

            ANCNo = RevertText(StringMayBeEnteredIn(Me.stbANCNo))

            If Not String.IsNullOrEmpty(ANCNo) Then
                Dim dataSource As DataTable = oAntenatalEnrollment.GetAntenatalEnrollment(ANCNo).Tables("AntenatalEnrollment")

                Me.DisplayData(dataSource)

                Dim PatientNo As String = RevertText(StringMayBeEnteredIn(Me.stbPatientNo))

                Me.ShowPatientDetails(PatientNo)
                Me.LoadContraceptivesHistory(ANCNo)
                Me.Search(PatientNo)
            End If

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Public Sub Search(ByVal patientNo As String)

        Dim oPatients As New SyncSoft.SQLDb.Patients()
        Dim oVillages As New SyncSoft.SQLDb.Villages()
        Dim oVariousOptions As New VariousOptions()
        Dim oFingerprintDeviceID As New LookupCommDataID.FingerprintDeviceID()


        Try

            Me.Cursor = Cursors.WaitCursor()

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Me.stbPatientNo.Text = FormatText(patientNo, "Patients", "PatientNo")

            Me.LoadObstetric(patientNo)
            Me.LoadPatientAllergies(patientNo)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.tbcAntenatalEnrollment.SelectTab(Me.tpgContraceptivesHistory.Name)
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub


#Region " Edit Methods "

    Public Sub Edit()

        Me.btnSave.ButtonText = ButtonCaption.Update
        Me.btnSave.Enabled = False
        Me.fbnDelete.Visible = True
        Me.fbnDelete.Enabled = False
        Me.pnlNavigateANCEnrollments.Visible = True

        ResetControlsIn(Me)

        Me.stbPatientNo.Enabled = False
        Me.btnLoad.Enabled = False
        Me.stbANCNo.Enabled = True
        Me.btnSearch.Enabled = False


        Me.dtpLNMP.MaxDate = AppData.MaximumDate
        Me.dtpScanDate.MaxDate = AppData.MaximumDate
        Me.dtpEnrollmentDate.MaxDate = AppData.MaximumDate
        Me.dtpEDD.MinDate = AppData.MinimumDate
        Me.dtpBloodTransfusionDate.MaxDate = AppData.MaximumDate

        Me.btnFindANCNo.Enabled = True
    End Sub

    Public Sub Save()

        Me.btnSave.ButtonText = ButtonCaption.Save
        Me.btnSave.Enabled = True
        Me.fbnDelete.Visible = False
        Me.fbnDelete.Enabled = True
        Me.pnlNavigateANCEnrollments.Visible = False

        ResetControlsIn(Me)

        Me.stbPatientNo.Enabled = True
        Me.btnLoad.Enabled = True
        Me.stbANCNo.Enabled = False
        Me.btnSearch.Enabled = True


        Me.dtpLNMP.MaxDate = Today
        Me.dtpScanDate.MaxDate = Today
        Me.dtpEnrollmentDate.MaxDate = Today
        Me.dtpEDD.MinDate = Today
        Me.dtpBloodTransfusionDate.MaxDate = Today

        Me.btnFindANCNo.Enabled = False

    End Sub

    Private Sub DisplayData(ByVal dataSource As DataTable)

        Try

            Me.btnSave.DataSource = dataSource
            Me.btnSave.LoadData(Me)

            Me.btnSave.Enabled = dataSource.Rows.Count > 0
            Me.fbnDelete.Enabled = dataSource.Rows.Count > 0

            Security.Apply(Me.btnSave, AccessRights.Update)
            Security.Apply(Me.fbnDelete, AccessRights.Delete)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub CallOnKeyEdit()
        If Me.btnSave.ButtonText = ButtonCaption.Update Then
            Me.btnSave.Enabled = False
            Me.fbnDelete.Enabled = False
        End If
    End Sub

#End Region

#Region "LOAD OPTIONS"

    Private Sub ShowPatientDetails(ByVal patientNo As String)

        Dim oPatients As New SyncSoft.SQLDb.Patients()
        Dim oVisitCategoryID As New LookupDataID.VisitCategoryID()
        Dim oGenderID As New LookupDataID.GenderID()
        Dim gender As String = Nothing
        Try

            Me.Cursor = Cursors.WaitCursor

            Dim patients As DataTable = oPatients.GetPatients(patientNo).Tables("Patients")
            Dim row As DataRow = patients.Rows(0)

            If row.Item("Gender").ToString.Equals("Male") Then
                DisplayMessage("Enter Female Patient")
                ClearControls()
                Return
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbPatientNo.Text = FormatText(patientNo, "Patients", "PatientNo")
            Me.txtFullName.Text = StringEnteredIn(row, "FullName")
            Me.stbAdress.Text = StringMayBeEnteredIn(row, "Address")
            Me.stbPhone.Text = StringMayBeEnteredIn(row, "Phone")
            Me.stbBloodGroup.Text = StringMayBeEnteredIn(row, "BloodGroup")
            Me.txtVisitDate.Text = FormatDate(DateEnteredIn(row, "lastVisitDate"))
            Me.stbJoinDate.Text = FormatDate(DateEnteredIn(row, "joinDate"))
            Dim birthDate As Date = DateMayBeEnteredIn(row, "BirthDate")
            Me.txtAge.Text = GetAgeString(birthDate)
            Me.stbGender.Text = StringMayBeEnteredIn(row, "Gender")
            Me.stbMaritalStatus.Text = StringMayBeEnteredIn(row, "MaritalStatus")
            Me.stbOccupation.Text = StringMayBeEnteredIn(row, "Occupation")
            Me.stbTotalVisits.Text = StringEnteredIn(row, "TotalVisits")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Select Case Me.chkCreateNewEnrollment.Enabled

                Case chkCreateNewEnrollment.Checked = True
                    Me.SetNextANCNo(patientNo)
            End Select


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch eX As Exception
            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try


    End Sub

    Private Sub LoadContraceptivesHistory(ByVal ancNo As String)
        Dim oContraceptivesHistory As New SyncSoft.SQLDb.ContraceptivesHistory()
        Dim oVariousOptions As New VariousOptions()

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim contraceptivesHistory As DataTable = oContraceptivesHistory.GetContraceptivesHistory(ancNo).Tables("ContraceptivesHistory")

            If contraceptivesHistory Is Nothing OrElse contraceptivesHistory.Rows.Count < 1 Then Return


            LoadGridData(Me.dgvContraceptivesHistory, contraceptivesHistory)

            For rowNo As Integer = 0 To dgvContraceptivesHistory.Rows.Count - 2

                With Me.dgvContraceptivesHistory
                    .Item(Me.colContraceptiveSaved.Name, rowNo).Value = True
                End With

            Next


            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch eX As Exception

            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub LoadObstetric(ByVal patientNo As String)
        Dim oObstetric As New SyncSoft.SQLDb.Obstetric()
        Dim oVariousOptions As New VariousOptions()

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim obstetric As DataTable = oObstetric.GetObstetric(patientNo).Tables("Obstetric")

            If obstetric Is Nothing OrElse obstetric.Rows.Count < 1 Then Return


            LoadGridData(Me.dgvOBHistory, obstetric)

            For rowNo As Integer = 0 To dgvOBHistory.Rows.Count - 2

                With Me.dgvOBHistory
                    .Item(Me.colObstetricSaved.Name, rowNo).Value = True
                End With

            Next

            ' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch eX As Exception

            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub
    Private Sub LoadAllergies(ByVal patientNo As String)
        Dim oPatientAllergies As New SyncSoft.SQLDb.PatientAllergies()
        Dim oVariousOptions As New VariousOptions()

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim patientAllergies As DataTable = oPatientAllergies.GetPatientAllergies(patientNo).Tables("PatientAllergies")

            If patientAllergies Is Nothing OrElse patientAllergies.Rows.Count < 1 Then Return


            LoadGridData(Me.dgvPatientAllergies, patientAllergies)


            ' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Me.dgvPatientAllergies.Rows.Clear()
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch eX As Exception

            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub LoadAllergies()

        Dim oAllergies As New SyncSoft.SQLDb.Allergies()

        Try

            Me.Cursor = Cursors.WaitCursor

            ' Load from allergies
            allergies = oAllergies.GetAllergies().Tables("Allergies")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.colAllergyNo.Sorted = False
            LoadComboData(Me.colAllergyNo, allergies, "AllergyNo", "AllergyName")
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


    Private Sub PatientStatus()

        Dim oLookupData As New LookupData()
        Dim oGenderID As New LookupDataID.GenderID()
        Dim oStatusID As New LookupCommDataID.StatusID()

        Try
            Me.Cursor = Cursors.WaitCursor


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim statusLookupData As DataTable = oLookupData.GetLookupData(LookupCommObjects.Status).Tables("LookupData")
            If statusLookupData Is Nothing Then Return

            For Each row As DataRow In statusLookupData.Rows
                If oStatusID.Active.ToUpper().Equals(row.Item("DataID").ToString().ToUpper()) OrElse
                    oStatusID.Inactive.ToUpper().Equals(row.Item("DataID").ToString().ToUpper()) Then

                    Continue For
                Else : row.Delete()
                End If
            Next

            Me.fcbStatusID.DataSource = statusLookupData

            Me.fcbStatusID.DisplayMember = "DataDes"
            Me.fcbStatusID.ValueMember = "DataID"

            Me.fcbStatusID.SelectedValue = oStatusID.Active
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


    Private Sub SetNextANCNo(ByVal patientNo As String)

        Try

            Dim oAntenatalEnrollment As New SyncSoft.SQLDb.AntenatalEnrollment()
            Dim oVariousOption As New VariousOptions()
            Dim oAutoNumbers As New SyncSoft.Options.SQL.AutoNumbers()

            Dim autoNumbers As DataTable = oAutoNumbers.GetAutoNumbers("AntenatalEnrollment", "ANCNo").Tables("AutoNumbers")
            Dim row As DataRow = autoNumbers.Rows(0)

            Dim paddingLEN As Integer = IntegerEnteredIn(row, "PaddingLEN")
            Dim paddingCHAR As Char = CChar(StringEnteredIn(row, "PaddingCHAR"))

            Dim ANCNoPrefix As String = oVariousOption.ANCNoPrefix

            Dim ANCID As String = oAntenatalEnrollment.GetNextANCID(patientNo).ToString()
            ANCID = ANCID.PadLeft(paddingLEN, paddingCHAR)

            Me.stbANCNo.Text = FormatText(ANCNoPrefix + patientNo + ANCID.Trim(), "AntenatalEnrollment", "ANCNo")
        Catch eX As Exception
            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub
#End Region

#Region "SAVE METHODS"
    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Try
            Dim transactions As New List(Of TransactionList(Of DBConnect))
            Dim records As Integer

            Select Case Me.btnSave.ButtonText

                Case ButtonCaption.Save
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    transactions.Add(New TransactionList(Of DBConnect)(AntenatalEnrollmentList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(ObstetricHistoryList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(ContraceptivesHistoryList, Action.Save))
                    transactions.Add(New TransactionList(Of DBConnect)(PatientAllergiesList, Action.Save))

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    records = DoTransactions(transactions)

                    DisplayMessage(records.ToString() + " record(s) Saved!")

                    ClearControls()

                Case ButtonCaption.Update

                    transactions.Add(New TransactionList(Of DBConnect)(AntenatalEnrollmentList, Action.Update, "AntenatalEnrollment"))
                    transactions.Add(New TransactionList(Of DBConnect)(ObstetricHistoryList, Action.Update, "Obstetric"))
                    transactions.Add(New TransactionList(Of DBConnect)(ContraceptivesHistoryList, Action.Update, "ContraceptivesHistory"))
                    transactions.Add(New TransactionList(Of DBConnect)(PatientAllergiesList, Action.Save, "PatientAllergies"))

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    records = DoTransactions(transactions)

                    DisplayMessage(records.ToString() + " record(s) updated!")

                    For rowNo As Integer = 0 To dgvContraceptivesHistory.Rows.Count - 2

                        With Me.dgvContraceptivesHistory
                            .Item(Me.colContraceptiveSaved.Name, rowNo).Value = True
                        End With

                    Next

                    For rowNo As Integer = 0 To dgvOBHistory.Rows.Count - 2

                        With Me.dgvOBHistory
                            .Item(Me.colObstetricSaved.Name, rowNo).Value = True
                        End With

                    Next

                    For rowNo As Integer = 0 To dgvPatientAllergies.Rows.Count - 2

                        With Me.dgvPatientAllergies
                            .Item(Me.colPatientAllergiesSaved.Name, rowNo).Value = True
                        End With

                    Next

            End Select


        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Function AntenatalEnrollmentList() As List(Of DBConnect)

        Dim lAntenatalEnrollment As New List(Of DBConnect)

        Try

            Dim oAntenatalEnrollment As New SyncSoft.SQLDb.AntenatalEnrollment()
            Dim oYesNo As New LookupDataID.YesNoID


            With oAntenatalEnrollment
                .ANCNo = RevertText(StringEnteredIn(Me.stbANCNo, "ANC No!"))
                .PatientNo = RevertText(StringEnteredIn(Me.stbPatientNo, "Patient No!"))
                .HIVStatusID = StringValueEnteredIn(Me.cboHIVStatusID, "HIV Status!") 'kkkk
                .PartnersHIVStatusID = StringValueEnteredIn(Me.cboPartnersHIVStatusID, "Partner's HIV Status!")

                Dim gravida As Integer = Me.nbxGravida.GetInteger()
                .Gravida = gravida
                Dim para As Integer = Me.nbxPara.GetInteger()
                .Para = para
                If para > gravida Then
                    Me.nbxPara.Focus()
                    Throw New ArgumentException("Para can't be greater that Gravida")
                End If

                .EnrollmentDate = DateEnteredIn(Me.dtpEnrollmentDate, "Enrollment Date!")

                .LNMP = DateEnteredIn(Me.dtpLNMP, "LNMP!")

                .LNMPDateReliable = Me.chkLNMPDateReliable.Checked
                .CycleRegularID = StringValueEnteredIn(Me.cboCycleRegularID, "Cycle Regular!")
                .DonePregnancyScanID = StringValueEnteredIn(Me.cboDonePregnancyScanID, "Done Pregnancy Scan!")
                .EDD = DateEnteredIn(Me.dtpEDD, "EDD!")
                .PatientStatusID = StringValueEnteredIn(Me.fcbStatusID, "Status!")

                If .DonePregnancyScanID.ToUpper().Equals(oYesNo.Yes.ToUpper()) Then
                    .ScanDate = DateEnteredIn(Me.dtpScanDate, "Scan Date!")
                Else : .ScanDate = DateMayBeEnteredIn(Me.dtpScanDate)
                End If

                .MedicalHistory = StringToSplitSelectedIn(Me.clbMedicalHistory, LookupObjects.MedicalHistory)
                .MedicalHistoryNotes = StringMayBeEnteredIn(Me.stbMedicalHistoryNotes)

                If cboBloodTransfusion.SelectedIndex = -1 Then
                    Me.tbcAntenatalEnrollment.SelectTab(Me.tpgPreviousIllnesses.Name)
                    .BloodTransfusion = StringValueEnteredIn(Me.cboBloodTransfusion, "Blood Transfusion!")
                    Me.cboBloodTransfusion.Focus()
                Else
                    .BloodTransfusion = StringValueEnteredIn(Me.cboBloodTransfusion, "Blood Transfusion!")
                End If

                .BloodTransfusionDate = DateMayBeEnteredIn(Me.dtpBloodTransfusionDate)
                .SurgicalHistory = StringToSplitSelectedIn(Me.clbSurgicalHistory, LookupObjects.SurgicalHistory)
                .SurgicalHistoryNotes = StringMayBeEnteredIn(Me.stbSurgicalHistoryNotes)
                .GynaecologicalHistory = StringToSplitSelectedIn(Me.clbGynaecologicalHistory, LookupObjects.GynaecologicalHistory)
                .GynaecologicalHistoryNotes = StringMayBeEnteredIn(Me.stbGynHistoryNotes)

                .FamilyHistory = StringToSplitSelectedIn(Me.clbFamilyHistory, LookupObjects.FamilyHistory)
                .FamilyHistoryNotes = StringMayBeEnteredIn(Me.stbFamilyHistoryNotes)
                .SocialHistory = StringToSplitSelectedIn(Me.clbSocialHistory, LookupObjects.SocialHistory)
                .SocialHistoryNotes = StringMayBeEnteredIn(Me.stbSocialHistoryNotes)


                .LoginID = CurrentUser.LoginID

                Me.PatientStatus()
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ValidateEntriesIn(Me)
                ValidateEntriesIn(Me.tpgPreviousIllnesses)
                ValidateEntriesIn(Me.tpgMenstruationHistory)
                ValidateEntriesIn(Me.tpgFamilySocialHistory)
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            End With
            lAntenatalEnrollment.Add(oAntenatalEnrollment)

            Return lAntenatalEnrollment

        Catch ex As Exception
            Throw (ex)
        End Try

    End Function


    Private Function ContraceptivesHistoryList() As List(Of DBConnect)

        Dim lContraceptivesHistory As New List(Of DBConnect)

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim patientNo As String = RevertText(StringEnteredIn(Me.stbPatientNo, "Patient No!"))

            If Me.dgvContraceptivesHistory.RowCount < 1 Then Return Nothing

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            For rowNo As Integer = 0 To Me.dgvContraceptivesHistory.RowCount - 2

                Dim cells As DataGridViewCellCollection = Me.dgvContraceptivesHistory.Rows(rowNo).Cells

                Try

                    Using oContraceptives As New SyncSoft.SQLDb.ContraceptivesHistory
                        With oContraceptives

                            .PatientNo = patientNo
                            .ANCNo = RevertText(StringEnteredIn(Me.stbANCNo, "ANC No!"))
                            .ContraceptiveID = StringMayBeEnteredIn(cells, Me.colContraceptiveID)
                            '.ComplicationsID = StringMayBeEnteredIn(cells, Me.colComplications)
                            .ComplicationDetails = StringMayBeEnteredIn(cells, Me.colComplicationDetails)
                            .DateStarted = DateMayBeEnteredIn(cells, Me.colContraceptiveDateStarted)
                            .DiscontinuedRemovedID = StringMayBeEnteredIn(cells, Me.colContraceptiveDiscontinued)
                            .RemovalReasonsID = StringMayBeEnteredIn(cells, Me.colContraceptiveRemovalReasons)
                            .Notes = StringMayBeEnteredIn(cells, Me.colContraceptiveNotes)
                            .LoginID = CurrentUser.LoginID

                            ValidateEntriesIn(Me.tpgContraceptivesHistory)
                        End With

                        lContraceptivesHistory.Add(oContraceptives)

                    End Using


                Catch ex As Exception
                    Throw (ex)

                End Try

            Next

            Return lContraceptivesHistory

        Catch ex As Exception
            Me.tbcCurrentPregnancy.SelectTab(Me.tpgContraceptivesHistory.Name)
            Throw ex

        End Try

    End Function

    Private Function PatientAllergiesList() As List(Of DBConnect)

        Dim lPatientAllergies As New List(Of DBConnect)

        Try

            Dim patientNo As String = RevertText(StringEnteredIn(Me.stbPatientNo, "Patient No!"))

            If Me.dgvPatientAllergies.RowCount < 1 Then Return Nothing

            For rowNo As Integer = 0 To Me.dgvPatientAllergies.RowCount - 2

                Using oPatientAllergies As New SyncSoft.SQLDb.PatientAllergies()

                    Dim cells As DataGridViewCellCollection = Me.dgvPatientAllergies.Rows(rowNo).Cells

                    With oPatientAllergies

                        .PatientNo = patientNo
                        .AllergyNo = StringEnteredIn(cells, Me.colAllergyNo, "Allergy No!")
                        .Reaction = StringMayBeEnteredIn(cells, Me.colReaction)

                        ValidateEntriesIn(Me.tpgPatientAllergies)

                    End With

                    lPatientAllergies.Add(oPatientAllergies)

                End Using

            Next

            Return lPatientAllergies

        Catch ex As Exception
            Me.tbcAntenatalEnrollment.SelectTab(Me.tpgPatientAllergies.Name)
            Throw ex

        End Try

    End Function

    Private Function ObstetricHistoryList() As List(Of DBConnect)

        Dim lObstetricHistory As New List(Of DBConnect)

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim patientNo As String = RevertText(StringEnteredIn(Me.stbPatientNo, "Patient No!"))

            If Me.dgvOBHistory.RowCount < 1 Then Return Nothing
            Dim Message = "The number of Pregancies entered for Obstetric History is greater than Gravida !" +
                ControlChars.NewLine + "Are you sure you want to continue?"

            ' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            For rowNo As Integer = 0 To Me.dgvOBHistory.RowCount - 2

                Dim cells As DataGridViewCellCollection = Me.dgvOBHistory.Rows(rowNo).Cells
                Try

                    If dgvOBHistory.Rows(rowNo).IsNewRow Then Exit For

                    IntegerEnteredIn(cells, Me.colObstetricPregnancy, "Pregnancy")
                    IntegerEnteredIn(cells, Me.colObstetricYear, "Year")
                    StringEnteredIn(cells, Me.colObstetricAbortionID, "Abortion")
                    StringEnteredIn(cells, Me.colObstetricAbortionPeriodID, "Abortion Period")
                    StringEnteredIn(cells, Me.colTypeOfDelivery, "Type of Delivery")
                    StringEnteredIn(cells, Me.colObstetricThirdStageID, "Third Stage")
                    StringEnteredIn(cells, Me.colObstetricPeurPerinumID, "Peur Perium")
                    StringEnteredIn(cells, Me.colObstetricChildStatusID, "Child Status")
                    StringEnteredIn(cells, Me.colObstetricGenderID, "Gender")
                    DecimalEnteredIn(cells, Me.colObstetricBirthWeight, False, "Birth Weight")

                Catch ex As Exception
                    Me.tbcAntenatalEnrollment.SelectTab(Me.tpgObstetricHistory.Name)
                    Throw (ex)

                End Try

            Next

            If (dgvOBHistory.Rows.Count() - 1) > CInt(nbxGravida.Value) Then
                If WarningMessage(Message) = Windows.Forms.DialogResult.No Then Throw New ArgumentException("Action Cancelled!")
            End If

        For rowNo As Integer = 0 To Me.dgvOBHistory.RowCount - 2

            Dim cells As DataGridViewCellCollection = Me.dgvOBHistory.Rows(rowNo).Cells

            Try

                Using oObstetric As New SyncSoft.SQLDb.Obstetric

                    With oObstetric

                        .PatientNo = patientNo
                            .Pregnancy = IntegerEnteredIn(cells, Me.colObstetricPregnancy)
                            .YearPregnant = IntegerEnteredIn(cells, Me.colObstetricYear)
                        .AbortionID = StringEnteredIn(cells, Me.colObstetricAbortionID)
                        .AbortionPeriodID = StringEnteredIn(cells, Me.colObstetricAbortionPeriodID)
                        .TypeOfDeliveryID = StringMayBeEnteredIn(cells, Me.colTypeOfDelivery)
                        .ThirdStageID = StringMayBeEnteredIn(cells, Me.colObstetricThirdStageID)
                        .PuerPeriumID = StringMayBeEnteredIn(cells, Me.colObstetricPeurPerinumID)
                        .ChildStatusID = StringMayBeEnteredIn(cells, Me.colObstetricChildStatusID)
                        .GenderID = StringMayBeEnteredIn(cells, Me.colObstetricGenderID)
                            .BirthWeight = DecimalEnteredIn(cells, Me.colObstetricBirthWeight, False, "Birth Weight")

                        If String.IsNullOrEmpty(StringMayBeEnteredIn(cells, Me.colObstetricImmunised)) Then
                            .ChildImmunised = CBool(0)
                        Else
                            .ChildImmunised = CBool(1)
                        End If

                        .HealthCondition = StringMayBeEnteredIn(cells, Me.colObstetricHealthCondition)
                        .LoginID = CurrentUser.LoginID

                        ValidateEntriesIn(Me.tpgObstetricHistory)
                    End With

                        lObstetricHistory.Add(oObstetric)
                End Using

            Catch ex As Exception
                    Throw (ex)

            End Try

        Next

        Return lObstetricHistory

        Catch ex As Exception
            Me.tbcAntenatalEnrollment.SelectTab(Me.tpgObstetricHistory.Name)
            Throw ex

        End Try

    End Function

#End Region


#Region "CLEAR CONTROLS"

    Private Sub ClearControls()

        Me.txtFullName.Clear()
        Me.txtVisitDate.Clear()
        Me.txtAge.Clear()
        Me.stbAdress.Clear()
        Me.stbOccupation.Clear()
        stbGender.Clear()
        nbxPara.Clear()
        nbxGravida.Clear()
        LoadLookupDataCombo(Me.cboHIVStatusID, LookupObjects.HIVStatus, False)
        LoadLookupDataCombo(Me.cboPartnersHIVStatusID, LookupObjects.HIVStatus, False)
        Me.stbFamilyHistoryNotes.Clear()
        Me.stbBloodGroup.Clear()
        Me.stbGynHistoryNotes.Clear()
        Me.stbMaritalStatus.Clear()
        Me.stbPhone.Clear()
        Me.stbSocialHistoryNotes.Clear()
        Me.stbJoinDate.Clear()
        Me.stbTotalVisits.Clear()
        Me.clbFamilyHistory.ClearSelected()
        Me.clbGynaecologicalHistory.ClearSelected()
        Me.clbMedicalHistory.ClearSelected()
        Me.clbSocialHistory.ClearSelected()
        Me.clbSurgicalHistory.ClearSelected()
        Me.stbMedicalHistoryNotes.Clear()
        Me.dgvContraceptivesHistory.Rows.Clear()
        Me.dgvOBHistory.Rows.Clear()
        Me.dgvPatientAllergies.Rows.Clear()
        Me.ResetControls()


    End Sub

    Private Sub ResetControls()
        ResetControlsIn(Me.tpgCurrentPregnancy)
        ResetControlsIn(Me.tpgPreviousIllnesses)
        ResetControlsIn(Me.tpgMenstruationHistory)
        ResetControlsIn(Me.tpgFamilySocialHistory)
        ResetControlsIn(Me.tpgObstetricHistory)
        ResetControlsIn(Me.tpgPatientAllergies)

        ResetControlsIn(Me.grpMedicalHistory)
        ResetControlsIn(Me.grpSurgicalHistory)
        ResetControlsIn(Me.grpOBSGyn)
        ResetControlsIn(Me.grpFamilyHistory)
        ResetControlsIn(Me.grpSocialHistory)
        ResetControlsIn(Me.tpgMenstruationHistory)

    End Sub
#End Region

    Private Sub btnLoad_Click(sender As System.Object, e As System.EventArgs) Handles btnLoad.Click
        Try

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fQuickSearch As New SyncSoft.SQL.Win.Forms.QuickSearch("FemalePatients", Me.stbPatientNo)
            fQuickSearch.ShowDialog(Me)

            Dim patientNo As String = RevertText(StringMayBeEnteredIn(Me.stbPatientNo))
            If Not String.IsNullOrEmpty(patientNo) Then
                Me.ShowPatientDetails(patientNo)
                Me.Search(patientNo)
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub dgvContraceptives_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvContraceptivesHistory.CellClick
        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            Dim selectedRow As Integer = Me.dgvContraceptivesHistory.CurrentCell.RowIndex

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim dateStarted As Date = DateMayBeEnteredIn(Me.dgvContraceptivesHistory.Rows(e.RowIndex).Cells, Me.colContraceptiveDateStarted)

            Dim fSelectDateTime As New SyncSoft.SQL.Win.Forms.SelectDateTime(dateStarted, "Date Started", AppData.MinimumDate, Today,
                                                                     Me.dgvContraceptivesHistory, Me.colContraceptiveDateStarted, e.RowIndex)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.colContraceptiveDateStarted.Index.Equals(e.ColumnIndex) AndAlso Me.dgvContraceptivesHistory.Rows(e.RowIndex).IsNewRow Then

                Me.dgvContraceptivesHistory.Rows.Add()
                fSelectDateTime.ShowDialog(Me)

                Dim enteredDate As Date = DateMayBeEnteredIn(Me.dgvContraceptivesHistory.Rows(e.RowIndex).Cells, Me.colContraceptiveDateStarted)
                If enteredDate = AppData.NullDateValue Then Me.dgvContraceptivesHistory.Rows.RemoveAt(e.RowIndex)

            ElseIf Me.colContraceptiveDateStarted.Index.Equals(e.ColumnIndex) Then

                fSelectDateTime.ShowDialog(Me)

            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub


 

#Region " PatientAllergies - Grid "

    Private Sub dgvPatientAllergies_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvPatientAllergies.CellBeginEdit

        If e.ColumnIndex <> Me.colAllergyNo.Index OrElse Me.dgvPatientAllergies.Rows.Count <= 1 Then Return
        Dim selectedRow As Integer = Me.dgvPatientAllergies.CurrentCell.RowIndex
        _AllergyNoValue = StringMayBeEnteredIn(Me.dgvPatientAllergies.Rows(selectedRow).Cells, Me.colAllergyNo)

    End Sub

    Private Sub dgvPatientAllergies_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPatientAllergies.CellEndEdit

        Try

            If Me.colAllergyNo.Items.Count < 1 Then Return

            If e.ColumnIndex.Equals(Me.colAllergyNo.Index) Then

                ' Ensure unique entry in the combo column

                If Me.dgvPatientAllergies.Rows.Count > 1 Then

                    Dim selectedRow As Integer = Me.dgvPatientAllergies.CurrentCell.RowIndex
                    Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvPatientAllergies.Rows(selectedRow).Cells, Me.colAllergyNo)

                    If CBool(Me.dgvPatientAllergies.Item(Me.colPatientAllergiesSaved.Name, selectedRow).Value).Equals(True) Then
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        Dim _Allergies As EnumerableRowCollection(Of DataRow) = allergies.AsEnumerable()
                        Dim allergyDisplay As String = (From data In _Allergies _
                                            Where data.Field(Of String)("AllergyNo").ToUpper().Equals(_AllergyNoValue.ToUpper()) _
                                            Select data.Field(Of String)("AllergyName")).First()
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        DisplayMessage("Allergy (" + allergyDisplay + ") can't be edited!")
                        Me.dgvPatientAllergies.Item(Me.colAllergyNo.Name, selectedRow).Value = _AllergyNoValue
                        Me.dgvPatientAllergies.Item(Me.colAllergyNo.Name, selectedRow).Selected = True
                        Return
                    End If

                    For rowNo As Integer = 0 To Me.dgvPatientAllergies.RowCount - 2
                        If Not rowNo.Equals(selectedRow) Then
                            Dim enteredItem As String = StringMayBeEnteredIn(Me.dgvPatientAllergies.Rows(rowNo).Cells, Me.colAllergyNo)
                            If enteredItem.Equals(selectedItem) Then
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                Dim _Alergies As EnumerableRowCollection(Of DataRow) = allergies.AsEnumerable()
                                Dim enteredDisplay As String = (From data In _Alergies _
                                                    Where data.Field(Of String)("AllergyNo").ToUpper().Equals(enteredItem.ToUpper()) _
                                                    Select data.Field(Of String)("AllergyName")).First()
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                DisplayMessage("Allergy (" + enteredDisplay + ") already entered!")
                                Me.dgvPatientAllergies.Item(Me.colAllergyNo.Name, selectedRow).Value = _AllergyNoValue
                                Me.dgvPatientAllergies.Item(Me.colAllergyNo.Name, selectedRow).Selected = True
                            End If
                        End If
                    Next

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ''''''''''''''''''''''' Populate other columns based upon what is entered in combo column '''''''''''''''''''''''''''''
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    If allergies Is Nothing OrElse String.IsNullOrEmpty(selectedItem) Then Return

                    For Each row As DataRow In allergies.Select("AllergyNo = '" + selectedItem + "'")
                        Me.dgvPatientAllergies.Item(Me.colAllergyCategory.Name, selectedRow).Value = StringMayBeEnteredIn(row, "AllergyCategory")
                    Next

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                End If

            End If

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub dgvPatientAllergies_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvPatientAllergies.UserDeletingRow

        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oPatientAllergies As New SyncSoft.SQLDb.PatientAllergies()
            Dim toDeleteRowNo As Integer = e.Row.Index

            If CBool(Me.dgvPatientAllergies.Item(Me.colPatientAllergiesSaved.Name, toDeleteRowNo).Value).Equals(False) Then Return

            Dim patientNo As String = RevertText(StringEnteredIn(Me.stbPatientNo, "Patient No!"))
            Dim allergyNo As String = CStr(Me.dgvPatientAllergies.Item(Me.colAllergyNo.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim message As String = "You do not have permission to delete this record!"
            'If Me.btnDelete.Enabled = False Then
            '    DisplayMessage(message)
            '    e.Cancel = True
            '    Return
            'End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oPatientAllergies
                .PatientNo = patientNo
                .AllergyNo = allergyNo
            End With

            DisplayMessage(oPatientAllergies.Delete())

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub LoadPatientAllergies(ByVal patientNo As String)

        Dim oPatientAllergies As New SyncSoft.SQLDb.PatientAllergies()

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim patientAllergies As DataTable = oPatientAllergies.GetPatientAllergies(patientNo).Tables("PatientAllergies")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If patientAllergies Is Nothing OrElse patientAllergies.Rows.Count < 1 Then Return

            For Each row As DataRow In patientAllergies.Rows


                LoadGridData(Me.dgvPatientAllergies, patientAllergies)
            Next


            For rowNo As Integer = 0 To dgvPatientAllergies.Rows.Count - 2

                With Me.dgvPatientAllergies
                    .Item(Me.colPatientAllergiesSaved.Name, rowNo).Value = True
                End With

            Next

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

#End Region

    Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
        Try

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim fFindObject As New frmFindObject(ObjectName.PatientNo)

            If fFindObject.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
                Dim patientNo As String = RevertText(fFindObject.GetPatientNo())
                Me.stbPatientNo.Text = patientNo
                Me.ShowPatientDetails(patientNo)
                Me.LoadPatientAllergies(patientNo)
            End If

        Catch eX As Exception
            ErrorMessage(eX)
            Me.btnSearch.PerformClick()

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub cboBloodTransfusion_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboBloodTransfusion.SelectedIndexChanged
        Dim BloodTransfusionID As String = StringValueMayBeEnteredIn(cboBloodTransfusion, "Blood Transfusion")

        If String.IsNullOrEmpty(BloodTransfusionID) Then Return


        If BloodTransfusionID.Equals(oBloodTransfusionID.Yes) Then
            dtpBloodTransfusionDate.Enabled = True

        Else
            dtpBloodTransfusionDate.Enabled = False

        End If
    End Sub

    Private Sub btnFindANCNo_Click(sender As System.Object, e As System.EventArgs) Handles btnFindANCNo.Click
        Try
            ClearControls()
            Me.navEnrollments.Clear()
            Me.chkNavigateEnrollments.Checked = False
            Dim fFindANCNo As New frmFindAutoNo(Me.stbANCNo, AutoNumber.ANCNo)
            fFindANCNo.ShowDialog(Me)
            Me.stbANCNo.Focus()
            LoadANCDetails()
        Catch ex As Exception
            ErrorMessage(ex)
        End Try
    End Sub

    Private Sub dgvOBHistory_CellEndEdit(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOBHistory.CellEndEdit

        Dim oAbortion As New LookupDataID.YesNoID
        Dim oAbortionPeriodID As New LookupDataID.AbortionPeriodID
        Dim oTypeOfDeliveryID As New LookupDataID.TypeOfDeliveryID
        Dim oThirdStageID As New LookupDataID.ThirdStage
        Dim oPeurPerinumID As New LookupDataID.PeurPerinum
        Dim oGenderID As New LookupDataID.GenderID
        Dim oChildStatusID As New LookupDataID.ChildStatusID

        Dim selectedRow As Integer = Me.dgvOBHistory.CurrentCell.RowIndex

        Dim Abortion As String = StringMayBeEnteredIn(Me.dgvOBHistory.Rows(selectedRow).Cells, Me.colObstetricAbortionID)


        Try
            If e.ColumnIndex.Equals(Me.colObstetricBirthWeight.Index) Then

                If dgvOBHistory.Item(colObstetricBirthWeight.Index, selectedRow).Value Is Nothing Or dgvOBHistory.Item(colObstetricBirthWeight.Index, selectedRow).Value.ToString() = "" Then
                    Return
                Else
                    If IsNumeric(dgvOBHistory.Item(colObstetricBirthWeight.Index, selectedRow).Value) Then
                        Return
                    Else
                        DisplayMessage("Please enter numeric values for Birth Weight!")
                        dgvOBHistory.Item(colObstetricBirthWeight.Index, selectedRow).Value = 0
                        dgvOBHistory.CurrentCell = dgvOBHistory.Item(colObstetricBirthWeight.Index, selectedRow)
                    End If
                End If
            End If

            If e.ColumnIndex.Equals(Me.colObstetricPregnancy.Index) Then
                If IsNumeric(Me.dgvOBHistory.Item(colObstetricPregnancy.Name, selectedRow).Value) Then
                    If selectedRow = 0 Then
                        Me.dgvOBHistory.Item(colObstetricPregnancy.Name, selectedRow).Value = 1
                    Else
                        Me.dgvOBHistory.Item(colObstetricPregnancy.Name, selectedRow).Value = CInt(Me.dgvOBHistory.Item(colObstetricPregnancy.Name, (selectedRow - 1)).Value) + 1
                    End If

                Else
                    DisplayMessage("Please enter numeric values for Pregnancy!")
                    dgvOBHistory.Item(colObstetricPregnancy.Index, selectedRow).Value = ""
                    dgvOBHistory.CurrentCell = dgvOBHistory.Item(colObstetricPregnancy.Index, selectedRow)
                End If

            End If

            If e.ColumnIndex.Equals(Me.colObstetricAbortionID.Index) Then
                If Abortion.ToUpper.Equals(oAbortion.Yes.ToUpper) Then
                    Me.dgvOBHistory.Item(Me.colObstetricAbortionPeriodID.Name, selectedRow).Value = oAbortionPeriodID.Below12Weeks
                    Me.dgvOBHistory.Item(Me.colObstetricAbortionPeriodID.Name, selectedRow).ReadOnly = False
                    Me.dgvOBHistory.Item(Me.colObstetricAbortionPeriodID.Name, selectedRow).Style.BackColor = Color.White

                    Me.dgvOBHistory.Item(Me.colTypeOfDelivery.Name, selectedRow).Value = oTypeOfDeliveryID.NA
                    Me.dgvOBHistory.Item(Me.colTypeOfDelivery.Name, selectedRow).ReadOnly = True
                    Me.dgvOBHistory.Item(Me.colTypeOfDelivery.Name, selectedRow).Style.BackColor = Color.LightYellow

                    Me.dgvOBHistory.Item(Me.colObstetricPeurPerinumID.Name, selectedRow).Value = oPeurPerinumID.NA
                    Me.dgvOBHistory.Item(Me.colObstetricPeurPerinumID.Name, selectedRow).ReadOnly = True
                    Me.dgvOBHistory.Item(Me.colObstetricPeurPerinumID.Name, selectedRow).Style.BackColor = Color.LightYellow

                    Me.dgvOBHistory.Item(Me.colObstetricThirdStageID.Name, selectedRow).Value = oThirdStageID.NA
                    Me.dgvOBHistory.Item(Me.colObstetricThirdStageID.Name, selectedRow).ReadOnly = True
                    Me.dgvOBHistory.Item(Me.colObstetricThirdStageID.Name, selectedRow).Style.BackColor = Color.LightYellow

                    Me.dgvOBHistory.Item(Me.colObstetricGenderID.Name, selectedRow).Value = oGenderID.NA
                    Me.dgvOBHistory.Item(Me.colObstetricGenderID.Name, selectedRow).ReadOnly = True
                    Me.dgvOBHistory.Item(Me.colObstetricGenderID.Name, selectedRow).Style.BackColor = Color.LightYellow

                    Me.dgvOBHistory.Item(Me.colObstetricChildStatusID.Name, selectedRow).Value = oChildStatusID.NA
                    Me.dgvOBHistory.Item(Me.colObstetricChildStatusID.Name, selectedRow).ReadOnly = True
                    Me.dgvOBHistory.Item(Me.colObstetricChildStatusID.Name, selectedRow).Style.BackColor = Color.LightYellow

                    Me.dgvOBHistory.Item(Me.colObstetricBirthWeight.Name, selectedRow).Value = 0
                    Me.dgvOBHistory.Item(Me.colObstetricBirthWeight.Name, selectedRow).ReadOnly = True
                    Me.dgvOBHistory.Item(Me.colObstetricBirthWeight.Name, selectedRow).Style.BackColor = Color.LightYellow

                    Me.dgvOBHistory.Item(Me.colObstetricImmunised.Name, selectedRow).Value = False
                    Me.dgvOBHistory.Item(Me.colObstetricImmunised.Name, selectedRow).ReadOnly = True
                    Me.dgvOBHistory.Item(Me.colObstetricImmunised.Name, selectedRow).Style.BackColor = Color.LightYellow

                    Me.dgvOBHistory.Item(Me.colObstetricHealthCondition.Name, selectedRow).Value = "NA"
                    Me.dgvOBHistory.Item(Me.colObstetricHealthCondition.Name, selectedRow).ReadOnly = True
                    Me.dgvOBHistory.Item(Me.colObstetricHealthCondition.Name, selectedRow).Style.BackColor = Color.LightYellow

                Else

                    Me.dgvOBHistory.Item(Me.colObstetricAbortionPeriodID.Name, selectedRow).Value = oAbortionPeriodID.NA
                    Me.dgvOBHistory.Item(Me.colObstetricAbortionPeriodID.Name, selectedRow).ReadOnly = True
                    Me.dgvOBHistory.Item(Me.colObstetricAbortionPeriodID.Name, selectedRow).Style.BackColor = Color.LightYellow

                    Me.dgvOBHistory.Item(Me.colTypeOfDelivery.Name, selectedRow).ReadOnly = False
                    Me.dgvOBHistory.Item(Me.colTypeOfDelivery.Name, selectedRow).Style.BackColor = Color.White

                    Me.dgvOBHistory.Item(Me.colObstetricPeurPerinumID.Name, selectedRow).ReadOnly = False
                    Me.dgvOBHistory.Item(Me.colObstetricPeurPerinumID.Name, selectedRow).Style.BackColor = Color.White

                    Me.dgvOBHistory.Item(Me.colObstetricThirdStageID.Name, selectedRow).ReadOnly = False
                    Me.dgvOBHistory.Item(Me.colObstetricThirdStageID.Name, selectedRow).Style.BackColor = Color.White

                    Me.dgvOBHistory.Item(Me.colObstetricGenderID.Name, selectedRow).ReadOnly = False
                    Me.dgvOBHistory.Item(Me.colObstetricGenderID.Name, selectedRow).Style.BackColor = Color.White

                    Me.dgvOBHistory.Item(Me.colObstetricChildStatusID.Name, selectedRow).ReadOnly = False
                    Me.dgvOBHistory.Item(Me.colObstetricChildStatusID.Name, selectedRow).Style.BackColor = Color.White

                    Me.dgvOBHistory.Item(Me.colObstetricBirthWeight.Name, selectedRow).Value = 0
                    Me.dgvOBHistory.Item(Me.colObstetricBirthWeight.Name, selectedRow).ReadOnly = False
                    Me.dgvOBHistory.Item(Me.colObstetricBirthWeight.Name, selectedRow).Style.BackColor = Color.White

                    Me.dgvOBHistory.Item(Me.colObstetricImmunised.Name, selectedRow).ReadOnly = False
                    Me.dgvOBHistory.Item(Me.colObstetricImmunised.Name, selectedRow).Style.BackColor = Color.White

                    Me.dgvOBHistory.Item(Me.colObstetricHealthCondition.Name, selectedRow).ReadOnly = False
                    Me.dgvOBHistory.Item(Me.colObstetricHealthCondition.Name, selectedRow).Style.BackColor = Color.White
                End If
            End If
        Catch ex As Exception
            ErrorMessage(ex)
        End Try
    End Sub

    Private Sub dgvOBHistory_UserDeletedRow(sender As System.Object, e As System.Windows.Forms.DataGridViewRowEventArgs) Handles dgvOBHistory.UserDeletedRow
        For rowNo As Integer = 0 To dgvOBHistory.Rows.Count - 2
            If rowNo = 0 Then
                Me.dgvOBHistory.Item(colObstetricPregnancy.Name, rowNo).Value = 1
            Else
                Me.dgvOBHistory.Item(colObstetricPregnancy.Name, rowNo).Value = CInt(Me.dgvOBHistory.Item(colObstetricPregnancy.Name, (rowNo - 1)).Value) + 1
            End If
        Next
    End Sub

    Private Sub stbPatientNo_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles stbPatientNo.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub


    Private Sub stbPatientNo_TextChanged(sender As Object, e As System.EventArgs) Handles stbPatientNo.TextChanged
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim patientNo As String = StringMayBeEnteredIn(Me.stbPatientNo)
        If Not String.IsNullOrEmpty(patientNo) Then
            Me.stbPatientNo.Text = patientNo
            Return
        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Me.ClearControls()
        Me.ResetControls()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Sub


    Private Sub stbPatientNo_Leave(sender As System.Object, e As System.EventArgs) Handles stbPatientNo.Leave
        Try

            Me.Cursor = Cursors.WaitCursor
            Me.ClearControls()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim patientNo As String = RevertText(StringMayBeEnteredIn(Me.stbPatientNo))
            If Not String.IsNullOrEmpty(patientNo) Then
                Me.ShowPatientDetails(patientNo)
                Me.Search(patientNo)
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub stbANCNo_Leave(sender As System.Object, e As System.EventArgs) Handles stbANCNo.Leave
        Try
            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.ClearControls()

            If chkCreateNewEnrollment.Checked = False Then

                Dim ancNo As String = RevertText(StringMayBeEnteredIn(Me.stbANCNo))
                If Not String.IsNullOrEmpty(ancNo) Then Me.LoadANCDetails()

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub cboDonePregnancyScanID_SelectedValueChanged(sender As Object, e As System.EventArgs) Handles cboDonePregnancyScanID.SelectedValueChanged

        Try
            Dim oYesNoID As New LookupDataID.YesNoID

            Dim DonePregnancyScan As String = StringValueMayBeEnteredIn(Me.cboDonePregnancyScanID, "Done Pregnancy Scan!")
            If String.IsNullOrEmpty(DonePregnancyScan) Then Return

            If DonePregnancyScan.ToUpper().Equals(oYesNoID.Yes.ToUpper()) Then
                Me.dtpScanDate.Enabled = True
                Me.dtpScanDate.Value = Today
                Me.dtpScanDate.Checked = False
            Else
                Me.dtpScanDate.Enabled = False
                Me.dtpScanDate.Value = AppData.NullDateValue
                Me.dtpScanDate.Checked = True
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub SetReasonsForRemovalDefaults(ByVal selectedRow As Integer)

        Try
            Dim oReasonsForRemovalID As New LookupDataID.ReasonsForRemovalID
            Dim oYesNoID As New LookupDataID.YesNoID
            Dim selectedItem As String = StringMayBeEnteredIn(Me.dgvContraceptivesHistory.Rows(selectedRow).Cells, Me.colContraceptiveDiscontinued)

            If String.IsNullOrEmpty(selectedItem) Then Return

            If Not selectedItem.ToUpper().Equals(oYesNoID.Yes.ToUpper()) Then
                Me.dgvContraceptivesHistory.Item(Me.colContraceptiveRemovalReasons.Name, selectedRow).Value = oReasonsForRemovalID.NA
                Me.dgvContraceptivesHistory.Item(Me.colContraceptiveRemovalReasons.Name, selectedRow).Selected = True
            Else
                Me.dgvContraceptivesHistory.Item(Me.colContraceptiveRemovalReasons.Name, selectedRow).Value = Nothing
                Me.dgvContraceptivesHistory.Item(Me.colContraceptiveRemovalReasons.Name, selectedRow).Selected = True
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub dgvContraceptives_CellEndEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvContraceptivesHistory.CellEndEdit
        Dim oContraceptiveType As New LookupDataID.FPMethodsID
        Dim oDiscontinued As New LookupDataID.YesNoID
        Dim oReasonsForRemovalID As New LookupDataID.ReasonsForRemovalID

        Try
            If e.RowIndex < 0 Then Return

            Dim selectedRow As Integer = Me.dgvContraceptivesHistory.CurrentCell.RowIndex

            Dim ContraceptiveType As String = StringMayBeEnteredIn(Me.dgvContraceptivesHistory.Rows(selectedRow).Cells, Me.colContraceptiveID)

            If Me.colContraceptiveDiscontinued.Index.Equals(e.ColumnIndex) Then
                Me.SetReasonsForRemovalDefaults(selectedRow)
            End If

            If ContraceptiveType.ToUpper.Equals(oContraceptiveType.Abstinence.ToUpper) Then
                Me.dgvContraceptivesHistory.Item(Me.colComplicationDetails.Name, selectedRow).Value = "NA"
                Me.dgvContraceptivesHistory.Item(Me.colContraceptiveDateStarted.Name, selectedRow).Value = Nothing
                Me.dgvContraceptivesHistory.Item(Me.colContraceptiveDiscontinued.Name, selectedRow).Value = oDiscontinued.NA
                Me.dgvContraceptivesHistory.Item(Me.colContraceptiveRemovalReasons.Name, selectedRow).Value = oReasonsForRemovalID.NA
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub chkCreateNewEnrollment_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkCreateNewEnrollment.CheckedChanged
        Try
            ClearControls()
            If chkCreateNewEnrollment.Checked = False Then
                Me.Edit()
            Else
                Me.Save()
            End If
        Catch ex As Exception
            ErrorMessage(ex)
        End Try
    End Sub

    Private Sub dgvOBHistory_DataError(sender As Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvOBHistory.DataError
        ErrorMessage(e.Exception)
        e.Cancel = True
    End Sub

    Private Sub fbnClose_Click_1(sender As System.Object, e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub

#Region "Enrollments Navigate"

    Private Sub chkNavigateEnrollments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkNavigateEnrollments.Click
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If Not Me.AllSaved() Then
            Me.chkNavigateEnrollments.Checked = Not Me.chkNavigateEnrollments.Checked
            Return
        End If

        ' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If chkNavigateEnrollments.Checked = True Then
            Me.EnableNavigateEnrollments(Me.chkNavigateEnrollments.Checked)
        Else
            Me.navEnrollments.Clear()
        End If

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Sub

    Private Sub EnableNavigateEnrollments(ByVal state As Boolean)

        Dim startPosition As Integer = 0
        'Dim oVisits As New SyncSoft.SQLDb.Visits()
        Dim oAntenatalEnrollment As New SyncSoft.SQLDb.AntenatalEnrollment()


        Try

            Me.Cursor = Cursors.WaitCursor

            If state Then

                Dim patientNo As String = RevertText(StringEnteredIn(Me.stbPatientNo, "Patient No!"))
                Dim ancNo As String = RevertText(StringEnteredIn(Me.stbANCNo, "ANC No!"))

                Dim enrollments As DataTable = oAntenatalEnrollment.GetAntenatalEnrollmentByPatientNo(patientNo).Tables("AntenatalEnrollment")

                For pos As Integer = 0 To enrollments.Rows.Count - 1
                    If ancNo.ToUpper().Equals(enrollments.Rows(pos).Item("ANCNo").ToString().ToUpper()) Then
                        startPosition = pos + 1
                        Exit For
                    ElseIf pos = (enrollments.Rows.Count - 1) And startPosition = 0 Then
                        startPosition = pos + 1
                    Else : startPosition = 1
                    End If
                Next

                Me.navEnrollments.DataSource = enrollments
                Me.navEnrollments.Navigate(startPosition)

            Else : Me.navEnrollments.Clear()
            End If

        Catch eX As Exception
            Me.chkNavigateEnrollments.Checked = False
            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub OnCurrentValue(ByVal currentValue As Object) Handles navEnrollments.OnCurrentValue

        Try

            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim ancNo As String = RevertText(currentValue.ToString())
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If String.IsNullOrEmpty(ancNo) Then Return
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbANCNo.Text = FormatText(ancNo, "AntenatalEnrollment", "ANCNo")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.LoadANCDetails()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch eX As Exception
            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

#End Region

    Private Function AllSaved() As Boolean

        Try

            Dim oVariousOptions As New VariousOptions()
            Dim message As String
            Dim ancNo As String = StringMayBeEnteredIn(Me.stbANCNo)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If String.IsNullOrEmpty(ancNo) Then Return True
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            Dim oAntenatalEnrollment As New SyncSoft.SQLDb.AntenatalEnrollment()

            Dim AntenatalEnrollment As DataTable = oAntenatalEnrollment.GetAntenatalEnrollment(RevertText(ancNo)).Tables("AntenatalEnrollment")



            If AntenatalEnrollment Is Nothing Then

                message = "Please ensure that At least One Record is saved under Antenatal Enrollment!"
                DisplayMessage(message)

                Me.BringToFront()
                If Me.WindowState = FormWindowState.Minimized Then Me.WindowState = FormWindowState.Normal
                Return False
            End If

            '''''''''''''''''''''''''''''''
            Return True
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Return True

        End Try

    End Function

    Private Sub btnLoadEnrollment_Click(sender As System.Object, e As System.EventArgs)
        'Dim fPeriodicAntenatalEnrollments As New frmPeriodicAntenatalEnrollment(Me.stbANCNo)
        'fPeriodicAntenatalEnrollments.ShowDialog(Me)
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim _ANCNo As String = RevertText(StringMayBeEnteredIn(Me.stbANCNo))
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If String.IsNullOrEmpty(_ANCNo) Then Return

        Me.LoadANCDetails()
    End Sub

    Private Sub dgvOBHistory_EditingControlShowing(sender As System.Object, e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvOBHistory.EditingControlShowing
        Try
            Me.Cursor = Cursors.WaitCursor
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If TypeOf e.Control Is ComboBox Then
                Dim cbo As ComboBox = DirectCast(e.Control, ComboBox)
                cbo.DropDownHeight = 150
            End If
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub dgvOBHistory_UserDeletingRow(sender As Object, e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvOBHistory.UserDeletingRow
        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oObstetric As New Obstetric()
            Dim toDeleteRowNo As Integer = e.Row.Index

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If CBool(Me.dgvOBHistory.Item(Me.colObstetricSaved.Name, toDeleteRowNo).Value).Equals(False) Then Return
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim patientNo As String = RevertText(StringEnteredIn(Me.stbPatientNo, "Patient's No!"))
            Dim pregnancy = CInt(Me.dgvOBHistory.Item(Me.colObstetricPregnancy.Name, toDeleteRowNo).Value)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oObstetric
                .PatientNo = patientNo
                .Pregnancy = pregnancy
            End With

            DisplayMessage(oObstetric.Delete())

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub dgvContraceptivesHistory_UserDeletingRow(sender As Object, e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvContraceptivesHistory.UserDeletingRow
        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oContraceptivesHistory As New ContraceptivesHistory()
            Dim toDeleteRowNo As Integer = e.Row.Index

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If CBool(Me.dgvContraceptivesHistory.Item(Me.colContraceptiveSaved.Name, toDeleteRowNo).Value).Equals(False) Then Return
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim ancNo As String = RevertText(StringEnteredIn(Me.stbANCNo, "ANC No!"))
            Dim contraceptiveID = Me.dgvContraceptivesHistory.Item(Me.colContraceptiveID.Name, toDeleteRowNo).Value.ToString()

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Security.Apply(Me.fbnDelete, AccessRights.Delete)
            If Me.fbnDelete.Enabled = False Then
                DisplayMessage("You do not have permission to delete this record!")
                e.Cancel = True
                Return
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With oContraceptivesHistory
                .ANCNo = ancNo
                .ContraceptiveID = contraceptiveID
            End With

            DisplayMessage(oContraceptivesHistory.Delete())

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)
            e.Cancel = True

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

End Class