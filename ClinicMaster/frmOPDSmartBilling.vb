
Option Strict On

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls

Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects
Imports SyncSoft.SQLDb
Imports SyncSoft.SQLDb.Lookup.LookupDataID
Imports SyncSoft.Common.SQL.Classes
Imports SyncSoft.SQLDb.Lookup
Imports SyncSoft.Common.SQL.Enumerations

Public Class frmOPDSmartBilling

#Region " Fields "
    Private oVariousOptions As New VariousOptions()
    Private patientNo As String
    Private genderID As String
    Private copayTypeID As String
    Private copayValue As Decimal
    Private oIntegrationAgent As New IntegrationAgents()
    Private smartAgentNo As String = oIntegrationAgent.SMART()
    Private oCopayTypeID As New CoPayTypeID()
    Private saveApproved As Boolean
    Private defaultVisitNo As String
#End Region



    Private Sub frmOPDSmartBilling_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.Cursor = Cursors.WaitCursor()

            If Not String.IsNullOrEmpty(defaultVisitNo) Then
                Me.stbVisitNo.Text = defaultVisitNo
                Me.ShowPatientDetails(RevertText(defaultVisitNo))

            End If

            SetCountToSmartItems()

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub frmOPDSmartBilling_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub

    Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub

    Private Sub fbnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub fbnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


        Try
            Me.Cursor = Cursors.WaitCursor()

            'Dim dataSource As DataTable = oOPDSmartBilling.GetOPDSmartBilling().Tables("OPDSmartBilling")
            'Me.DisplayData(dataSource)

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub ebnSaveUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ebnSaveUpdate.Click

        Dim oSmartCardItems As SmartCardItems
        Dim oSmartCardMembers As New SmartCardMembers()
        Dim transactions As New List(Of TransactionList(Of DBConnect))
        Dim lINTItems As New List(Of DBConnect)
       
        Try
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.Cursor = Cursors.Default
            Dim lSmartCardItems As New List(Of SmartCardItems)
            oSmartCardMembers = ProcessSmartCardData(patientNo)
            Dim coverAmount As Decimal = oSmartCardMembers.CoverAmount
            Dim expiryDate As Date = oSmartCardMembers.SchemeExpiryDate
            Dim totalBill As Decimal = DecimalMayBeEnteredIn(stbAmount)
            Dim smartCardServiceProviderNo As String = oVariousOptions.SmartCardServiceProviderNo

            If oSmartCardMembers.CoverAmount < totalBill Then
                Throw New ArgumentException("The Cover Amount " + coverAmount.ToString + " can't be less than total bill " + totalBill.ToString)
            End If
            
            Dim smardCardNo As String = RevertText(RevertText(oSmartCardMembers.MedicalCardNumber, "/"c))
            Dim visitNo As String = RevertText(RevertText(StringMayBeEnteredIn(stbVisitNo)))
            Dim totalCashAmount As Decimal

            For rowNo As Integer = 0 To Me.dgvSmartBills.RowCount - 1

                If CBool(Me.dgvSmartBills.Item(Me.colInclude.Name, rowNo).Value) = True Then

                    Dim cells As DataGridViewCellCollection = Me.dgvSmartBills.Rows(rowNo).Cells
                    Dim itemCode As String = StringEnteredIn(cells, Me.colItemCode, "Item Code!")
                    Dim itemName As String = StringEnteredIn(cells, Me.colItemName, "Item Name!")
                    Dim itemCategoryID As String = StringEnteredIn(cells, Me.colItemCategoryID)
                    Dim quantity As Integer = IntegerEnteredIn(cells, Me.colQuantity)
                    Dim unitPrice As Decimal = DecimalEnteredIn(cells, Me.colUnitPrice, False)
                    Dim amount As Decimal = DecimalEnteredIn(cells, Me.colAmount, False)
                    Dim cashAmount As Decimal = DecimalEnteredIn(cells, Me.colCashAmount, False)
                    totalCashAmount += cashAmount
                    Using oINTItems As New SyncSoft.SQLDb.INTItems()
                        With oINTItems
                            .AgentNo = smartAgentNo
                            .VisitNo = visitNo
                            .ItemCode = itemCode
                            .ItemCategoryID = itemCategoryID

                        End With
                        lINTItems.Add(oINTItems)
                    End Using

                    With oSmartCardItems

                        .TransactionDate = FormatDate(Today, "yyyy-MM-dd")
                        .TransactionTime = GetTime(Now)
                        .ServiceProviderNr = smartCardServiceProviderNo
                        .DiagnosisCode = (0).ToString()
                        .DiagnosisDescription = "Unknown Disease"
                        .EncounterType = GetEncounterType(itemCategoryID)
                        .CodeType = "Mcode"
                        .Code = itemCode
                        .CodeDescription = itemName
                        .itemCode = itemCode
                        .itemCategoryID = itemCategoryID
                        .Quantity = quantity.ToString()
                        .Amount = (quantity * unitPrice).ToString()

                    End With

                    lSmartCardItems.Add(oSmartCardItems)


                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                End If
            Next


            oSmartCardMembers.InvoiceNo = visitNo
            oSmartCardMembers.TotalBill = totalBill
            oSmartCardMembers.TotalServices = lSmartCardItems.Count()
            oSmartCardMembers.CopayType = copayTypeID
            oSmartCardMembers.CopayAmount = DecimalMayBeEnteredIn(Me.stbCopayValue)
            oSmartCardMembers.Gender = genderID

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            transactions.Add(New TransactionList(Of DBConnect)(lINTItems, Action.Save))
            Dim oVisitType As New LookupDataID.VisitTypeID()
            If UpdateSmartExchangeFiles(oSmartCardMembers, lSmartCardItems, visitNo, oVisitType.OutPatient, False) Then
                DoTransactions(transactions)

            Else
                Throw New ArgumentException("Error processing smart card information. Please edit the Patient and try again")
                Return
            End If
            Me.saveApproved = False

            Me.LoadSmartBill(visitNo)
            SetCountToSmartItems()

        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

#Region " Edit Methods "


#End Region

    Private Sub ShowPatientDetails(visitNo As String)

        Dim oVisits As New SyncSoft.SQLDb.Visits()

        Try



            Dim visits As DataTable = oVisits.GetVisits(visitNo).Tables("Visits")
            Dim row As DataRow = visits.Rows(0)

            patientNo = StringEnteredIn(row, "PatientNo")

            Me.stbPatientNo.Text = FormatText(patientNo, "Patients", "PatientNo")
            Me.stbVisitNo.Text = FormatText(visitNo, "Visits", "VisitNo")
            Me.stbPatientName.Text = StringEnteredIn(row, "FullName")
            Me.stbVisitDate.Text = FormatDate(DateEnteredIn(row, "VisitDate"))
            Me.stbAge.Text = StringEnteredIn(row, "Age")
            Dim birthDate As Date = DateMayBeEnteredIn(row, "BirthDate")
            Me.lblAgeString.Text = GetAgeString(birthDate, True)
            ' Me.stbAdmissionStatus.Text = StringMayBeEnteredIn(row, "AdmissionStatus")
            Me.stbGender.Text = StringEnteredIn(row, "Gender")
            Me.genderID = StringEnteredIn(row, "GenderID")
            Me.copayTypeID = StringMayBeEnteredIn(row, "CoPayTypeID")
            Me.copayValue = DecimalMayBeEnteredIn(row, "CoPayValue", False)
            Me.stbCopayType.Text = StringMayBeEnteredIn(row, "CoPayType")
            Me.stbToBillNo.Text = FormatText(StringEnteredIn(row, "BillNo"), "BillCustomers", "AccountNo")
            Dim associatedBillCustomer As String = StringMayBeEnteredIn(row, "AssociatedBillCustomer")
            Dim billCustomerName As String = StringMayBeEnteredIn(row, "BillCustomerName")
            If Not String.IsNullOrEmpty(associatedBillCustomer) Then billCustomerName += " (" + associatedBillCustomer + ")"


            'billModesID = StringMayBeEnteredIn(row, "BillModesID")
            'associatedBillNo = StringMayBeEnteredIn(row, "AssociatedBillNo")
            Dim oINTVisits As New INTVisits()
            Dim _INTVisits As DataTable = oINTVisits.GetINTVisits(smartAgentNo, visitNo).Tables("INTVisits")

            Dim rowData As DataRow = _INTVisits.Rows(0)
            Dim provisionalMemberLimit As Decimal = DecimalEnteredIn(rowData, "MemberLimit", False)
            Me.stbProvisionalLimit.Text = FormatNumber(provisionalMemberLimit, AppData.DecimalPlaces)


            Me.LoadSmartBill(visitNo)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch eX As Exception
            ErrorMessage(eX)

        End Try

    End Sub

    Private Sub LoadSmartBill(visitNo As String)
        Try
            Dim oINTItems As New INTItems()
            Dim _INTItems As DataTable = oINTItems.GetNotSyncedItems(smartAgentNo, visitNo).Tables("INTItems")
            LoadGridData(dgvSmartBills, _INTItems)
            CalculateTotalBill()
        Catch ex As Exception
            ErrorMessage(ex)
        End Try
    End Sub

    Private Sub CalculateTotalBill()

        Dim totalBill As Decimal
        Dim totalCopayValue As Decimal

        Me.stbAmount.Clear()
        Me.stbAmountWords.Clear()


        For rowNo As Integer = 0 To Me.dgvSmartBills.RowCount - 1
            If CBool(Me.dgvSmartBills.Item(Me.colInclude.Name, rowNo).Value) = True Then
                Dim cells As DataGridViewCellCollection = Me.dgvSmartBills.Rows(rowNo).Cells
                Dim amount As Decimal = DecimalMayBeEnteredIn(cells, Me.colAmount)
                Dim cashAmount As Decimal = DecimalMayBeEnteredIn(cells, Me.colCashAmount)
                totalBill += amount
                totalCopayValue += cashAmount
            End If
        Next

        Me.stbAmount.Text = FormatNumber(totalBill, AppData.DecimalPlaces)
        If Me.copayTypeID = oCopayTypeID.Percent Then
            Me.stbCopayValue.Text = FormatNumber(totalCopayValue, AppData.DecimalPlaces)
        Else : stbCopayValue.Text = FormatNumber(Me.copayValue, AppData.DecimalPlaces)
        End If
        Me.stbAmountWords.Text = NumberToWords(totalBill)


    End Sub


    Private Sub dgvSmartBills_CellEndEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSmartBills.CellEndEdit
        If e.ColumnIndex.Equals(Me.colInclude.Index) Then
            Me.CalculateTotalBill()
        End If
    End Sub

    Private Sub stbVisitNo_Leave(sender As Object, e As System.EventArgs) Handles stbVisitNo.Leave
        Dim visitNo As String = RevertText(StringMayBeEnteredIn(Me.stbVisitNo))
        If String.IsNullOrEmpty(visitNo) Then Return
        Me.ShowPatientDetails(visitNo)
    End Sub

    Public Function GetSaveApproved() As Boolean
        Return saveApproved
    End Function


    Private Sub btnLoad_Click(sender As System.Object, e As System.EventArgs) Handles btnLoad.Click
        Dim fPendingToSmartItems As New frmPendingToSmartItems(stbVisitNo)
        fPendingToSmartItems.ShowDialog()
        Dim visitNo As String = RevertText(StringMayBeEnteredIn(stbVisitNo))
        If String.IsNullOrEmpty(visitNo) Then Return
        Me.ShowPatientDetails(visitNo)
    End Sub

    Private Sub SetCountToSmartItems()
        Try

            Dim oINTItems As New INTItems()
            Dim count As Integer = oINTItems.GetCountNotSyncedItems(oIntegrationAgent.SMART)
            lblPendingToSmartItems.Text = "Pending To Smart Item(s): " + count.ToString
            btnLoad.Enabled = count > 0
        Catch ex As Exception
            ErrorMessage(ex)
        End Try
    End Sub

End Class