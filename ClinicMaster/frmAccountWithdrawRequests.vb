
Option Strict On

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls

Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects
Imports SyncSoft.SQLDb.Lookup
Imports SyncSoft.SQLDb

Public Class frmAccountWithdrawRequests

#Region " Fields "
    Private oBillModesID As New LookupDataID.BillModesID()
    Private oRequestStatusID As New LookupDataID.RequestStatusID()
    Private oWithdrawTypeID As New LookupDataID.WithdrawTypeID()
    Private requestStatusID As String = oRequestStatusID.Pending()
    Private oVariousOptions As New VariousOptions()
#End Region

    Private Sub frmAccountWithdrawRequests_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.Cursor = Cursors.WaitCursor()

            LoadLookupDataCombo(Me.cboAccountBillModesID, LookupObjects.BillModes, False)
            LoadLookupDataCombo(Me.cboWithdrawRequestReasonID, LookupObjects.WithdrawReasons, False)
            LoadLookupDataCombo(Me.cboWithdrawTypeID, LookupObjects.WithdrawTypes, False)
            dtpRequestDate.MaxDate = Today

            ExpireAccountWithdrawRequests()
            DeactivateIdleAccounts()

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub LoadAccountDetails(ByVal billModesID As String)

        Dim oBillCustomers As New SyncSoft.SQLDb.BillCustomers()
        Dim oInsurances As New SyncSoft.SQLDb.Insurances()


        Dim oSetupData As New SetupData()

        Try
            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.ClearAccountControls()
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Select Case billModesID.ToUpper()

                Case oBillModesID.Cash.ToUpper()
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.lblAccountBillNo.Text = "Patient No"
                    Me.lblAccountName.Text = "Patient Name"
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Account.ToUpper()

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ' Load all from Bill Customers
                    Dim billCustomers As DataTable
                    If Not InitOptions.LoadBillCustomersAtStart Then
                        billCustomers = oBillCustomers.GetBillCustomers().Tables("BillCustomers")
                        oSetupData.BillCustomers = billCustomers
                    Else : billCustomers = oSetupData.BillCustomers
                    End If

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    LoadComboData(Me.cboAccountBillNo, billCustomers, "BillCustomerFullName")
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.lblAccountBillNo.Text = "Account No"
                    Me.lblAccountName.Text = "Account Name"
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Insurance.ToUpper()
                    Dim insurances As DataTable = oInsurances.GetInsurances().Tables("Insurances")
                    LoadComboData(Me.cboAccountBillNo, insurances, "InsuranceFullName")
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.lblAccountBillNo.Text = "Insurance No"
                    Me.lblAccountName.Text = "Insurance Name"
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End Select

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub LoadAccountDetails(ByVal billModesID As String, ByVal accountNo As String)

        Dim accountName As String = String.Empty
        Dim outstandingBalance As Decimal
        Dim accountBalance As Decimal

        Dim oPatients As New SyncSoft.SQLDb.Patients()
        Dim oBillCustomers As New SyncSoft.SQLDb.BillCustomers()
        Dim oInsurances As New SyncSoft.SQLDb.Insurances()

        Try
            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbAccountName.Clear()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Select Case billModesID.ToUpper()

                Case oBillModesID.Cash.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim row As DataRow = oPatients.GetPatients(accountNo).Tables("Patients").Rows(0)

                    Me.cboAccountBillNo.Text = FormatText(accountNo, "Patients", "PatientNo")
                    accountName = StringMayBeEnteredIn(row, "FullName")
                    outstandingBalance = DecimalEnteredIn(row, "OutstandingBalance", True)
                    accountBalance = DecimalEnteredIn(row, "AccountBalance", True)
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Account.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim row As DataRow = oBillCustomers.GetBillCustomers(accountNo).Tables("BillCustomers").Rows(0)

                    Me.cboAccountBillNo.Text = FormatText(accountNo, "BillCustomers", "AccountNo").ToUpper()
                    accountName = StringMayBeEnteredIn(row, "BillCustomerName")
                    outstandingBalance = DecimalEnteredIn(row, "OutstandingBalance", True)
                    accountBalance = DecimalEnteredIn(row, "AccountBalance", True)

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    Me.cboAccountBillNo.Text = FormatText(accountNo, "BillCustomers", "AccountNo").ToUpper()
                    Dim billCustomerName = StringMayBeEnteredIn(row, "BillCustomerName")
                    Dim billCustomerTypeID As String = StringMayBeEnteredIn(row, "BillCustomerTypeID")

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Insurance.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim row As DataRow = oInsurances.GetInsurances(accountNo).Tables("Insurances").Rows(0)

                    Me.cboAccountBillNo.Text = FormatText(accountNo, "Insurances", "InsuranceNo")
                    accountName = StringMayBeEnteredIn(row, "InsuranceName")
                    outstandingBalance = DecimalEnteredIn(row, "OutstandingBalance", True)
                    accountBalance = DecimalEnteredIn(row, "AccountBalance", True)

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End Select

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbAccountName.Text = accountName
            Me.nbxAccountBalance.Text = FormatNumber(accountBalance, AppData.DecimalPlaces)
            Me.nbxOutstandingBalance.Text = FormatNumber(outstandingBalance, AppData.DecimalPlaces)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetNextRequestID()

        Dim yearL2 As String = Today.Year.ToString().Substring(2)

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim oAccountWithdrawRequests As New SyncSoft.SQLDb.AccountWithdrawRequests()
            Dim oAutoNumbers As New SyncSoft.Options.SQL.AutoNumbers()

            Dim autoNumbers As DataTable = oAutoNumbers.GetAutoNumbers("AccountWithdrawRequests", "RequestNo").Tables("AutoNumbers")
            Dim row As DataRow = autoNumbers.Rows(0)

            Dim paddingLEN As Integer = IntegerEnteredIn(row, "PaddingLEN")
            Dim paddingCHAR As Char = CChar(StringEnteredIn(row, "PaddingCHAR"))
            Dim invoiceNo As String = yearL2 + oAccountWithdrawRequests.GetNextAccountWithdrawRequestID.ToString().PadLeft(paddingLEN, paddingCHAR)

            stbRequestNo.Text = FormatText(invoiceNo, "AccountWithdrawRequests", "RequestNo")

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub ClearAccountControls()
        Me.cboAccountBillNo.DataSource = Nothing
        Me.cboAccountBillNo.Items.Clear()
        Me.cboAccountBillNo.Text = String.Empty
    End Sub

    Private Sub frmAccountWithdrawRequests_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub

    Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub

    Private Sub fbnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnDelete.Click

        Dim oAccountWithdrawRequests As New SyncSoft.SQLDb.AccountWithdrawRequests()

        Try
            Me.Cursor = Cursors.WaitCursor()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then Return

            oAccountWithdrawRequests.RequestNo = RevertText(StringEnteredIn(Me.stbRequestNo, "Request No!"))

            DisplayMessage(oAccountWithdrawRequests.Delete())
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            ResetControlsIn(Me)
            Me.CallOnKeyEdit()

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub fbnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnSearch.Click

        Dim requestNo As String

        Dim oAccountWithdrawRequests As New SyncSoft.SQLDb.AccountWithdrawRequests()

        Try
            Me.Cursor = Cursors.WaitCursor()

            requestNo = RevertText(StringEnteredIn(Me.stbRequestNo, "Request No!"))

            Dim dataSource As DataTable = oAccountWithdrawRequests.GetAccountWithdrawRequests(requestNo).Tables("AccountWithdrawRequests")
            Me.DisplayData(dataSource)
            Dim row As DataRow = dataSource.Rows(0)
            Me.requestStatusID = StringEnteredIn(row, "RequestStatusID")
            Me.cboAccountBillNo.Text = StringEnteredIn(row, "AccountBillNo")
            Me.LoadAccountBalance()

            If Not requestStatusID.ToUpper().Equals(oRequestStatusID.Pending) Then
                DisplayMessage("The request with ID: " + requestNo + " is already " + GetLookupDataDes(requestStatusID) + " can't be edited!")
                Me.fbnDelete.Enabled = False
                Me.ebnSaveUpdate.Enabled = False
            End If
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub ebnSaveUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ebnSaveUpdate.Click

        Dim oAccountWithdrawRequests As New SyncSoft.SQLDb.AccountWithdrawRequests()

        Try
            Me.Cursor = Cursors.WaitCursor()

            Dim outStandingBalance As Decimal = nbxOutstandingBalance.GetDecimal(True)
            Dim accountBalance As Decimal = nbxAccountBalance.GetDecimal(True)
            Dim amount As Decimal = Me.nbxAmount.GetDecimal(False)
            Dim availableBalance As Decimal = accountBalance - outStandingBalance
            Dim accountName As String = StringEnteredIn(Me.stbAccountName, "Account Name!")
            Dim withdrawTypeID As String = StringValueEnteredIn(Me.cboWithdrawTypeID, "Withdraw Type ID!")
            Dim requestDate As Date = DateEnteredIn(Me.dtpRequestDate, "Request Date!")
            Dim lastValidityDaTeTime As Date = DateTimeEnteredIn(dtpLastValidityDateTime, "Last Validaty Date Time!")

            Dim message As String = String.Empty

            If withdrawTypeID.Equals(oWithdrawTypeID.ManualDebit.ToUpper()) Then


                If requestDate > Today Then
                    message = "Request date: " + FormatDate(requestDate) + " cannot be greater than Today"
                ElseIf requestDate > lastValidityDaTeTime Then
                    message = "Request date: " + FormatDate(requestDate) + " cannot be greater than the last validity date: " + FormatDateTime(lastValidityDaTeTime)
                ElseIf lastValidityDaTeTime < Now Then
                    message = "The Last Validity Date: " + FormatDateTime(lastValidityDaTeTime) + " cannot be less than Now: " + FormatDateTime(lastValidityDaTeTime)
                End If

                If accountBalance <= 0 Then
                    message = "Account Name: " + accountName + " has " + nbxAccountBalance.Text + " account balance You cannot make a withdraw request."
                ElseIf amount <= 0 Then
                    message = "The requested amount must be greater than 0."
                ElseIf outStandingBalance > accountBalance Then
                    message = "Account Name: " + accountName + " has outstanding balance: " + nbxOutstandingBalance.Text + " greater than account balance. " + nbxAccountBalance.Text + " " +
                        "You cannot make a withdraw request."
                ElseIf outStandingBalance = accountBalance Then
                    message = "Account Name: " + accountName + " has outstanding balance: " + nbxOutstandingBalance.Text + " equal to account balance. " +
                        " You cannot make a withdraw request unless the outstanding balance is cleared"
                ElseIf amount > availableBalance Then
                    message = "The amount requested " + FormatNumber(amount, AppData.DecimalPlaces) + " can't be greater than available balance: " + FormatNumber(availableBalance, AppData.DecimalPlaces)
                End If
            End If
            If Not String.IsNullOrEmpty(message) Then
                DisplayMessage(message)
                Return
            End If
            With oAccountWithdrawRequests

                .RequestNo = RevertText(StringEnteredIn(Me.stbRequestNo, "Request No!"))
                .AccountBillModesID = StringValueEnteredIn(Me.cboAccountBillModesID, "Account BillModes ID!")
                .AccountBillModes = StringEnteredIn(Me.cboAccountBillModesID, "Account BillModes ID!")
                .AccountBillNo = RevertText(StringEnteredIn(Me.cboAccountBillNo, "Account Bill No!"))
                .AccountName = accountName
                .RequestDate = DateEnteredIn(Me.dtpRequestDate, "Request Date!")
                .Amount = amount
                .WithdrawRequestReasonID = StringValueEnteredIn(Me.cboWithdrawRequestReasonID, "Withdraw Request Reason!")
                .WithdrawRequestReason = StringEnteredIn(Me.cboWithdrawRequestReasonID, "Withdraw Request Reason!")
                .WithdrawTypeID = StringValueEnteredIn(Me.cboWithdrawTypeID, "Withdraw Type ID!")
                .WithdrawType = StringEnteredIn(Me.cboWithdrawTypeID, "Withdraw Type ID!")
                .LastValidityDateTime = DateTimeEnteredIn(dtpLastValidityDateTime, "Last Validaty Date Time!")
                .Notes = StringEnteredIn(Me.stbNotes, "Notes!")
                .RequestStatusID = Me.requestStatusID
                .RequestStatus = GetLookupDataDes(.RequestStatusID)
                .UserFullName = CurrentUser.FullName
                .LoginID = CurrentUser.LoginID

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ValidateEntriesIn(Me)
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End With

            Select Case Me.ebnSaveUpdate.ButtonText

                Case ButtonCaption.Save

                    oAccountWithdrawRequests.Save()

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ResetControlsIn(Me)
                    Me.SetNextRequestID()

                    Me.dtpLastValidityDateTime.Value = Now.AddHours(oVariousOptions.WithdrawRequestExpiryHours)
                    
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case ButtonCaption.Update

                    DisplayMessage(oAccountWithdrawRequests.Update())
                    Me.CallOnKeyEdit()

            End Select

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub



#Region " Edit Methods "

    Public Sub Edit()

        Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update
        Me.ebnSaveUpdate.Enabled = False
        Me.fbnDelete.Visible = True
        Me.fbnDelete.Enabled = False
        Me.fbnSearch.Visible = True

        ResetControlsIn(Me)

    End Sub

    Public Sub Save()

        Me.ebnSaveUpdate.ButtonText = ButtonCaption.Save
        Me.ebnSaveUpdate.Enabled = True
        Me.fbnDelete.Visible = False
        Me.fbnDelete.Enabled = True
        Me.fbnSearch.Visible = False

        ResetControlsIn(Me)
        Me.SetNextRequestID()
        Me.dtpLastValidityDateTime.Value = Now.AddHours(oVariousOptions.WithdrawRequestExpiryHours)

    End Sub

    Private Sub DisplayData(ByVal dataSource As DataTable)

        Try

            Me.ebnSaveUpdate.DataSource = dataSource
            Me.ebnSaveUpdate.LoadData(Me)

            Me.ebnSaveUpdate.Enabled = dataSource.Rows.Count > 0
            Me.fbnDelete.Enabled = dataSource.Rows.Count > 0

            Security.Apply(Me.ebnSaveUpdate, AccessRights.Update)
            Security.Apply(Me.fbnDelete, AccessRights.Delete)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub CallOnKeyEdit()
        If Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update Then
            Me.ebnSaveUpdate.Enabled = False
            Me.fbnDelete.Enabled = False
        End If
    End Sub

#End Region

    Private Sub cboAccountBillModesID_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboAccountBillModesID.SelectedIndexChanged
        Try

            Me.Cursor = Cursors.WaitCursor
            Dim billModesID As String = StringValueMayBeEnteredIn(Me.cboAccountBillModesID, "Account Category!")
            If String.IsNullOrEmpty(billModesID) Then Return
            Me.btnLoad.Visible = billModesID.ToUpper().Equals(oBillModesID.Cash().ToUpper())
            Me.LoadAccountDetails(billModesID)

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub cboAccountBillNo_Leave(sender As Object, e As System.EventArgs) Handles cboAccountBillNo.Leave
        Try

            Me.LoadAccountBalance()

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub btnLoad_Click(sender As System.Object, e As System.EventArgs) Handles btnLoad.Click
        Try

            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fQuickSearch As New SyncSoft.SQL.Win.Forms.QuickSearch("Patients", Me.cboAccountBillNo)
            fQuickSearch.ShowDialog(Me)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim patientNo As String = RevertText(StringMayBeEnteredIn(Me.cboAccountBillNo))
            If Not String.IsNullOrEmpty(patientNo) Then Me.LoadAccountDetails(oBillModesID.Cash, patientNo)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub nbxAmount_Leave(sender As Object, e As System.EventArgs) Handles nbxAmount.Leave
        Try
            nbxAmount.Text = FormatNumber(DecimalMayBeEnteredIn(nbxAmount), AppData.DecimalPlaces)
        Catch ex As Exception
            ErrorMessage(ex)
        End Try

    End Sub

    Private Sub LoadAccountBalance()
        Try

            Dim billModesID As String = StringValueMayBeEnteredIn(Me.cboAccountBillModesID, "Account Category!")
            Dim accountNo As String = RevertText(SubstringRight(StringMayBeEnteredIn(Me.cboAccountBillNo)))

            If String.IsNullOrEmpty(accountNo) OrElse String.IsNullOrEmpty(billModesID) Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.LoadAccountDetails(billModesID, accountNo)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    

    Private Sub stbRequestNo_TextChanged(sender As System.Object, e As System.EventArgs) Handles stbRequestNo.TextChanged
        Me.CallOnKeyEdit()
    End Sub
End Class