
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDetailedAccountStatement : Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDetailedAccountStatement))
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle38 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle39 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle40 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle41 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle54 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle55 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle56 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle57 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle71 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle72 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle73 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle74 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle87 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle88 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle89 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle90 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle103 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle104 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle105 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle106 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle116 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle117 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle36 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle37 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle42 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle43 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle44 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle45 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle46 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle47 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle48 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle49 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle50 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle51 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle52 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle53 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle58 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle59 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle60 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle61 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle62 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle63 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle64 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle65 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle66 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle67 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle68 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle69 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle70 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle75 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle76 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle77 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle78 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle79 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle80 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle81 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle82 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle83 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle84 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle85 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle86 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle91 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle92 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle93 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle94 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle95 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle96 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle97 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle98 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle99 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle100 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle101 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle102 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle107 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle108 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle109 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle110 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle111 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle112 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle113 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle114 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle115 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.nbxAccountBalance = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.lblBillMode = New System.Windows.Forms.Label()
        Me.lblAccountBalance = New System.Windows.Forms.Label()
        Me.tbcAccountStatement = New System.Windows.Forms.TabControl()
        Me.tpgInvoices = New System.Windows.Forms.TabPage()
        Me.stbInvoiceAmountWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblInvoiceAmountWords = New System.Windows.Forms.Label()
        Me.stbInvoiceAmount = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblInvoiceAmount = New System.Windows.Forms.Label()
        Me.dgvInvoices = New System.Windows.Forms.DataGridView()
        Me.colInvoicePatientNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInvFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInvVisitNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInvInvoiceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInvBillNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInvInsuranceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInvoiceAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInvoiceDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmsEdit = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmsEditCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmsEditSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.tpgInvoiceAdjustments = New System.Windows.Forms.TabPage()
        Me.stbINVAAmountWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblINVAAmountWords = New System.Windows.Forms.Label()
        Me.stbInvoiceAdjustmentAmount = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblINVAAMouny = New System.Windows.Forms.Label()
        Me.dgvInvoiceAdjustments = New System.Windows.Forms.DataGridView()
        Me.colIVAPatientNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInvAFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIVAVisitNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIVAInvoiceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInvABillNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInvAInsuranceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIVAInvoiceDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAdjustmentNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAdjustmentDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIVAAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgExtraBills = New System.Windows.Forms.TabPage()
        Me.stbExtraBillAmountlWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblExtraBillAmountWords = New System.Windows.Forms.Label()
        Me.stbExtraBillAmount = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblExtraBillAmount = New System.Windows.Forms.Label()
        Me.dgvExtraBillItems = New System.Windows.Forms.DataGridView()
        Me.tpgOPDPayments = New System.Windows.Forms.TabPage()
        Me.stbTotalPaymentWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblPaymentWords = New System.Windows.Forms.Label()
        Me.stbTotalPayments = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblTotalPayments = New System.Windows.Forms.Label()
        Me.dgvCashReceipts = New System.Windows.Forms.DataGridView()
        Me.tpgBillFormPayments = New System.Windows.Forms.TabPage()
        Me.stbBillFormAmountWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblBillFormAmountWord = New System.Windows.Forms.Label()
        Me.stbBillFormAmount = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblBillFormAmount = New System.Windows.Forms.Label()
        Me.dgvBillFormPayments = New System.Windows.Forms.DataGridView()
        Me.tpgOPDRefunds = New System.Windows.Forms.TabPage()
        Me.stbTotalRefundWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblRefundWords = New System.Windows.Forms.Label()
        Me.stbTotalRefunds = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblTotalRefunds = New System.Windows.Forms.Label()
        Me.dgvRefunds = New System.Windows.Forms.DataGridView()
        Me.tpgExtraBillRefunds = New System.Windows.Forms.TabPage()
        Me.stbExtraBillRefundWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblExtraBillRefundAmpont = New System.Windows.Forms.Label()
        Me.stbExtraBillRefunds = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblExtraBillRefunds = New System.Windows.Forms.Label()
        Me.dgvExtraBillRefunds = New System.Windows.Forms.DataGridView()
        Me.tpgAccountTransactions = New System.Windows.Forms.TabPage()
        Me.stbNetAmountWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblNetAmountWords = New System.Windows.Forms.Label()
        Me.stbNetAmount = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblNetAmount = New System.Windows.Forms.Label()
        Me.stbTotalCredit = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblTotalCredit = New System.Windows.Forms.Label()
        Me.stbTotalDebit = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblTotalDebit = New System.Windows.Forms.Label()
        Me.stbPatientChequePaymentsWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblExpenditureTotalAmount = New System.Windows.Forms.Label()
        Me.lblExpenditureAmountWords = New System.Windows.Forms.Label()
        Me.stbPatientChequePayments = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.dgvAccountTrasaction = New System.Windows.Forms.DataGridView()
        Me.cboBillMode = New System.Windows.Forms.ComboBox()
        Me.fbnLoad = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.btnPrintPreview = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.cboAccountNo = New System.Windows.Forms.ComboBox()
        Me.lblAccountNo = New System.Windows.Forms.Label()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.stbBalance = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblBalance = New System.Windows.Forms.Label()
        Me.stbBalanceWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblBalanceWords = New System.Windows.Forms.Label()
        Me.fbnExport = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.lblRecordsNo = New System.Windows.Forms.Label()
        Me.cboCompanyNo = New System.Windows.Forms.ComboBox()
        Me.lblCompanyNo = New System.Windows.Forms.Label()
        Me.lblMainMemberNo = New System.Windows.Forms.Label()
        Me.stbMainMemberNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbAccountName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblAccountName = New System.Windows.Forms.Label()
        Me.stbCompanyName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblCompanyName = New System.Windows.Forms.Label()
        Me.dtpEndDateTime = New System.Windows.Forms.DateTimePicker()
        Me.lblStartDateTime = New System.Windows.Forms.Label()
        Me.dtpStartDateTime = New System.Windows.Forms.DateTimePicker()
        Me.lblEndDateTime = New System.Windows.Forms.Label()
        Me.stbMainMemberName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblMainMemberName = New System.Windows.Forms.Label()
        Me.stbTotalBill = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblTotalBillAmount = New System.Windows.Forms.Label()
        Me.stbTotalPaidAmount = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblTotalPaidAmount = New System.Windows.Forms.Label()
        Me.btnShowOutStandingBalance = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.stbOutStandingBalance = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.colBFPatientNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFBillNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFInsuranceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFVisitNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraBillNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colVisitType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraBillDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPatientNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPayVisitNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPayReceiptNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPayInvoiceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBillNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPayNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPayDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPayAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPayNotes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPayRecordDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPayRecordTime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFPPatientNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFPFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFPVisitNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFPReceiptNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFPExtraBillNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFPBillNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFPInsuranceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFPPayNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFPPayDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFPAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFPNotes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFPRecordDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBFPRecordTime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRefPatientNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRefFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRefReceiptNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRefRefundNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRefInvoiceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRefBillNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRefInsuranceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRefundDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRefAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRefNotes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRefundRecordDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRefTime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtRefPatientNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEXTRefFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEXTRefReceiptNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEXTRefExtraBillNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEXTRefRefundNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEXTRefBillNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEXTRefInsuranceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEXTRefRefundDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEXTRefAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEXTRefNotes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEXTRefRecordDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEXTRefRecordTime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTranNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTranDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.collAccAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCredit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDebit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBalance = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAccNotes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAccRecordDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAccTime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tbcAccountStatement.SuspendLayout()
        Me.tpgInvoices.SuspendLayout()
        CType(Me.dgvInvoices, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsEdit.SuspendLayout()
        Me.tpgInvoiceAdjustments.SuspendLayout()
        CType(Me.dgvInvoiceAdjustments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgExtraBills.SuspendLayout()
        CType(Me.dgvExtraBillItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgOPDPayments.SuspendLayout()
        CType(Me.dgvCashReceipts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgBillFormPayments.SuspendLayout()
        CType(Me.dgvBillFormPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgOPDRefunds.SuspendLayout()
        CType(Me.dgvRefunds, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgExtraBillRefunds.SuspendLayout()
        CType(Me.dgvExtraBillRefunds, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgAccountTransactions.SuspendLayout()
        CType(Me.dgvAccountTrasaction, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'nbxAccountBalance
        '
        Me.nbxAccountBalance.AllowDrop = True
        Me.nbxAccountBalance.BackColor = System.Drawing.SystemColors.Info
        Me.nbxAccountBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxAccountBalance.ControlCaption = "Account Balance"
        Me.nbxAccountBalance.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxAccountBalance.DecimalPlaces = -1
        Me.nbxAccountBalance.Location = New System.Drawing.Point(867, 7)
        Me.nbxAccountBalance.MaxValue = 0.0R
        Me.nbxAccountBalance.MinValue = 0.0R
        Me.nbxAccountBalance.MustEnterNumeric = True
        Me.nbxAccountBalance.Name = "nbxAccountBalance"
        Me.nbxAccountBalance.ReadOnly = True
        Me.nbxAccountBalance.Size = New System.Drawing.Size(179, 20)
        Me.nbxAccountBalance.TabIndex = 13
        Me.nbxAccountBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nbxAccountBalance.Value = ""
        '
        'fbnClose
        '
        Me.fbnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(970, 468)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 3
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'lblBillMode
        '
        Me.lblBillMode.Location = New System.Drawing.Point(17, 39)
        Me.lblBillMode.Name = "lblBillMode"
        Me.lblBillMode.Size = New System.Drawing.Size(113, 20)
        Me.lblBillMode.TabIndex = 0
        Me.lblBillMode.Text = "Account Category"
        '
        'lblAccountBalance
        '
        Me.lblAccountBalance.Location = New System.Drawing.Point(701, 8)
        Me.lblAccountBalance.Name = "lblAccountBalance"
        Me.lblAccountBalance.Size = New System.Drawing.Size(160, 20)
        Me.lblAccountBalance.TabIndex = 11
        Me.lblAccountBalance.Text = "Account Balance"
        '
        'tbcAccountStatement
        '
        Me.tbcAccountStatement.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbcAccountStatement.Controls.Add(Me.tpgInvoices)
        Me.tbcAccountStatement.Controls.Add(Me.tpgInvoiceAdjustments)
        Me.tbcAccountStatement.Controls.Add(Me.tpgExtraBills)
        Me.tbcAccountStatement.Controls.Add(Me.tpgOPDPayments)
        Me.tbcAccountStatement.Controls.Add(Me.tpgBillFormPayments)
        Me.tbcAccountStatement.Controls.Add(Me.tpgOPDRefunds)
        Me.tbcAccountStatement.Controls.Add(Me.tpgExtraBillRefunds)
        Me.tbcAccountStatement.Controls.Add(Me.tpgAccountTransactions)
        Me.tbcAccountStatement.HotTrack = True
        Me.tbcAccountStatement.Location = New System.Drawing.Point(12, 150)
        Me.tbcAccountStatement.Name = "tbcAccountStatement"
        Me.tbcAccountStatement.SelectedIndex = 0
        Me.tbcAccountStatement.Size = New System.Drawing.Size(1034, 284)
        Me.tbcAccountStatement.TabIndex = 0
        '
        'tpgInvoices
        '
        Me.tpgInvoices.Controls.Add(Me.stbInvoiceAmountWords)
        Me.tpgInvoices.Controls.Add(Me.lblInvoiceAmountWords)
        Me.tpgInvoices.Controls.Add(Me.stbInvoiceAmount)
        Me.tpgInvoices.Controls.Add(Me.lblInvoiceAmount)
        Me.tpgInvoices.Controls.Add(Me.dgvInvoices)
        Me.tpgInvoices.Location = New System.Drawing.Point(4, 22)
        Me.tpgInvoices.Name = "tpgInvoices"
        Me.tpgInvoices.Size = New System.Drawing.Size(1026, 258)
        Me.tpgInvoices.TabIndex = 10
        Me.tpgInvoices.Text = "Invoices"
        Me.tpgInvoices.UseVisualStyleBackColor = True
        '
        'stbInvoiceAmountWords
        '
        Me.stbInvoiceAmountWords.AllowDrop = True
        Me.stbInvoiceAmountWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbInvoiceAmountWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbInvoiceAmountWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbInvoiceAmountWords.CapitalizeFirstLetter = False
        Me.stbInvoiceAmountWords.EntryErrorMSG = ""
        Me.stbInvoiceAmountWords.Location = New System.Drawing.Point(483, 225)
        Me.stbInvoiceAmountWords.Multiline = True
        Me.stbInvoiceAmountWords.Name = "stbInvoiceAmountWords"
        Me.stbInvoiceAmountWords.ReadOnly = True
        Me.stbInvoiceAmountWords.RegularExpression = ""
        Me.stbInvoiceAmountWords.Size = New System.Drawing.Size(335, 30)
        Me.stbInvoiceAmountWords.TabIndex = 33
        '
        'lblInvoiceAmountWords
        '
        Me.lblInvoiceAmountWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblInvoiceAmountWords.Location = New System.Drawing.Point(336, 231)
        Me.lblInvoiceAmountWords.Name = "lblInvoiceAmountWords"
        Me.lblInvoiceAmountWords.Size = New System.Drawing.Size(131, 20)
        Me.lblInvoiceAmountWords.TabIndex = 32
        Me.lblInvoiceAmountWords.Text = "Net Amount In Words"
        '
        'stbInvoiceAmount
        '
        Me.stbInvoiceAmount.AllowDrop = True
        Me.stbInvoiceAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbInvoiceAmount.BackColor = System.Drawing.SystemColors.Info
        Me.stbInvoiceAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbInvoiceAmount.CapitalizeFirstLetter = False
        Me.stbInvoiceAmount.EntryErrorMSG = ""
        Me.stbInvoiceAmount.Location = New System.Drawing.Point(151, 231)
        Me.stbInvoiceAmount.Name = "stbInvoiceAmount"
        Me.stbInvoiceAmount.ReadOnly = True
        Me.stbInvoiceAmount.RegularExpression = ""
        Me.stbInvoiceAmount.Size = New System.Drawing.Size(174, 20)
        Me.stbInvoiceAmount.TabIndex = 12
        '
        'lblInvoiceAmount
        '
        Me.lblInvoiceAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblInvoiceAmount.Location = New System.Drawing.Point(9, 230)
        Me.lblInvoiceAmount.Name = "lblInvoiceAmount"
        Me.lblInvoiceAmount.Size = New System.Drawing.Size(136, 20)
        Me.lblInvoiceAmount.TabIndex = 11
        Me.lblInvoiceAmount.Text = "Total Amount"
        '
        'dgvInvoices
        '
        Me.dgvInvoices.AllowUserToAddRows = False
        Me.dgvInvoices.AllowUserToDeleteRows = False
        Me.dgvInvoices.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvInvoices.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvInvoices.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvInvoices.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvInvoices.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvInvoices.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvInvoices.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInvoices.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvInvoices.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colInvoicePatientNo, Me.colInvFullName, Me.colInvVisitNo, Me.colInvInvoiceNo, Me.colInvBillNo, Me.colInvInsuranceNo, Me.colInvoiceAmount, Me.colInvoiceDate})
        Me.dgvInvoices.ContextMenuStrip = Me.cmsEdit
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvInvoices.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgvInvoices.EnableHeadersVisualStyles = False
        Me.dgvInvoices.GridColor = System.Drawing.Color.Khaki
        Me.dgvInvoices.Location = New System.Drawing.Point(6, 0)
        Me.dgvInvoices.Name = "dgvInvoices"
        Me.dgvInvoices.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInvoices.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvInvoices.RowHeadersVisible = False
        Me.dgvInvoices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvInvoices.Size = New System.Drawing.Size(1020, 211)
        Me.dgvInvoices.TabIndex = 1
        Me.dgvInvoices.Text = "DataGridView1"
        '
        'colInvoicePatientNo
        '
        Me.colInvoicePatientNo.DataPropertyName = "PatientNo"
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Info
        Me.colInvoicePatientNo.DefaultCellStyle = DataGridViewCellStyle3
        Me.colInvoicePatientNo.HeaderText = "Patient No"
        Me.colInvoicePatientNo.Name = "colInvoicePatientNo"
        Me.colInvoicePatientNo.ReadOnly = True
        '
        'colInvFullName
        '
        Me.colInvFullName.DataPropertyName = "FullName"
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Info
        Me.colInvFullName.DefaultCellStyle = DataGridViewCellStyle4
        Me.colInvFullName.HeaderText = "Full Name"
        Me.colInvFullName.Name = "colInvFullName"
        Me.colInvFullName.ReadOnly = True
        Me.colInvFullName.Width = 250
        '
        'colInvVisitNo
        '
        Me.colInvVisitNo.DataPropertyName = "VisitNo"
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Info
        Me.colInvVisitNo.DefaultCellStyle = DataGridViewCellStyle5
        Me.colInvVisitNo.HeaderText = "Visit No"
        Me.colInvVisitNo.Name = "colInvVisitNo"
        Me.colInvVisitNo.ReadOnly = True
        '
        'colInvInvoiceNo
        '
        Me.colInvInvoiceNo.DataPropertyName = "InvoiceNo"
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Info
        Me.colInvInvoiceNo.DefaultCellStyle = DataGridViewCellStyle6
        Me.colInvInvoiceNo.HeaderText = "Invoice No"
        Me.colInvInvoiceNo.Name = "colInvInvoiceNo"
        Me.colInvInvoiceNo.ReadOnly = True
        '
        'colInvBillNo
        '
        Me.colInvBillNo.DataPropertyName = "BillNo"
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Info
        Me.colInvBillNo.DefaultCellStyle = DataGridViewCellStyle7
        Me.colInvBillNo.HeaderText = "Bill No"
        Me.colInvBillNo.Name = "colInvBillNo"
        Me.colInvBillNo.ReadOnly = True
        '
        'colInvInsuranceNo
        '
        Me.colInvInsuranceNo.DataPropertyName = "InsuranceNo"
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Info
        Me.colInvInsuranceNo.DefaultCellStyle = DataGridViewCellStyle8
        Me.colInvInsuranceNo.HeaderText = "Insurance No"
        Me.colInvInsuranceNo.Name = "colInvInsuranceNo"
        Me.colInvInsuranceNo.ReadOnly = True
        '
        'colInvoiceAmount
        '
        Me.colInvoiceAmount.DataPropertyName = "Amount"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Info
        Me.colInvoiceAmount.DefaultCellStyle = DataGridViewCellStyle9
        Me.colInvoiceAmount.HeaderText = "Amount"
        Me.colInvoiceAmount.Name = "colInvoiceAmount"
        Me.colInvoiceAmount.ReadOnly = True
        Me.colInvoiceAmount.Width = 80
        '
        'colInvoiceDate
        '
        Me.colInvoiceDate.DataPropertyName = "InvoiceDate"
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Info
        Me.colInvoiceDate.DefaultCellStyle = DataGridViewCellStyle10
        Me.colInvoiceDate.HeaderText = "Invoice Date"
        Me.colInvoiceDate.Name = "colInvoiceDate"
        Me.colInvoiceDate.ReadOnly = True
        '
        'cmsEdit
        '
        Me.cmsEdit.BackColor = System.Drawing.Color.GhostWhite
        Me.cmsEdit.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmsEditCopy, Me.cmsEditSelectAll})
        Me.cmsEdit.Name = "cmsSearch"
        Me.cmsEdit.Size = New System.Drawing.Size(123, 48)
        '
        'cmsEditCopy
        '
        Me.cmsEditCopy.Enabled = False
        Me.cmsEditCopy.Image = CType(resources.GetObject("cmsEditCopy.Image"), System.Drawing.Image)
        Me.cmsEditCopy.Name = "cmsEditCopy"
        Me.cmsEditCopy.Size = New System.Drawing.Size(122, 22)
        Me.cmsEditCopy.Text = "Copy"
        Me.cmsEditCopy.ToolTipText = "To copy with column headings, use Ctrl+C key combination"
        '
        'cmsEditSelectAll
        '
        Me.cmsEditSelectAll.Enabled = False
        Me.cmsEditSelectAll.Name = "cmsEditSelectAll"
        Me.cmsEditSelectAll.Size = New System.Drawing.Size(122, 22)
        Me.cmsEditSelectAll.Text = "Select All"
        '
        'tpgInvoiceAdjustments
        '
        Me.tpgInvoiceAdjustments.Controls.Add(Me.stbINVAAmountWords)
        Me.tpgInvoiceAdjustments.Controls.Add(Me.lblINVAAmountWords)
        Me.tpgInvoiceAdjustments.Controls.Add(Me.stbInvoiceAdjustmentAmount)
        Me.tpgInvoiceAdjustments.Controls.Add(Me.lblINVAAMouny)
        Me.tpgInvoiceAdjustments.Controls.Add(Me.dgvInvoiceAdjustments)
        Me.tpgInvoiceAdjustments.Location = New System.Drawing.Point(4, 22)
        Me.tpgInvoiceAdjustments.Name = "tpgInvoiceAdjustments"
        Me.tpgInvoiceAdjustments.Size = New System.Drawing.Size(1026, 258)
        Me.tpgInvoiceAdjustments.TabIndex = 12
        Me.tpgInvoiceAdjustments.Text = "Invoice Adjustments"
        Me.tpgInvoiceAdjustments.UseVisualStyleBackColor = True
        '
        'stbINVAAmountWords
        '
        Me.stbINVAAmountWords.AllowDrop = True
        Me.stbINVAAmountWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbINVAAmountWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbINVAAmountWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbINVAAmountWords.CapitalizeFirstLetter = False
        Me.stbINVAAmountWords.EntryErrorMSG = ""
        Me.stbINVAAmountWords.Location = New System.Drawing.Point(483, 225)
        Me.stbINVAAmountWords.Multiline = True
        Me.stbINVAAmountWords.Name = "stbINVAAmountWords"
        Me.stbINVAAmountWords.ReadOnly = True
        Me.stbINVAAmountWords.RegularExpression = ""
        Me.stbINVAAmountWords.Size = New System.Drawing.Size(335, 30)
        Me.stbINVAAmountWords.TabIndex = 37
        '
        'lblINVAAmountWords
        '
        Me.lblINVAAmountWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblINVAAmountWords.Location = New System.Drawing.Point(336, 231)
        Me.lblINVAAmountWords.Name = "lblINVAAmountWords"
        Me.lblINVAAmountWords.Size = New System.Drawing.Size(131, 20)
        Me.lblINVAAmountWords.TabIndex = 36
        Me.lblINVAAmountWords.Text = "Amount In Words"
        '
        'stbInvoiceAdjustmentAmount
        '
        Me.stbInvoiceAdjustmentAmount.AllowDrop = True
        Me.stbInvoiceAdjustmentAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbInvoiceAdjustmentAmount.BackColor = System.Drawing.SystemColors.Info
        Me.stbInvoiceAdjustmentAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbInvoiceAdjustmentAmount.CapitalizeFirstLetter = False
        Me.stbInvoiceAdjustmentAmount.EntryErrorMSG = ""
        Me.stbInvoiceAdjustmentAmount.Location = New System.Drawing.Point(151, 232)
        Me.stbInvoiceAdjustmentAmount.Name = "stbInvoiceAdjustmentAmount"
        Me.stbInvoiceAdjustmentAmount.ReadOnly = True
        Me.stbInvoiceAdjustmentAmount.RegularExpression = ""
        Me.stbInvoiceAdjustmentAmount.Size = New System.Drawing.Size(170, 20)
        Me.stbInvoiceAdjustmentAmount.TabIndex = 35
        '
        'lblINVAAMouny
        '
        Me.lblINVAAMouny.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblINVAAMouny.Location = New System.Drawing.Point(7, 232)
        Me.lblINVAAMouny.Name = "lblINVAAMouny"
        Me.lblINVAAMouny.Size = New System.Drawing.Size(136, 20)
        Me.lblINVAAMouny.TabIndex = 34
        Me.lblINVAAMouny.Text = "Total Amount"
        '
        'dgvInvoiceAdjustments
        '
        Me.dgvInvoiceAdjustments.AllowUserToAddRows = False
        Me.dgvInvoiceAdjustments.AllowUserToDeleteRows = False
        Me.dgvInvoiceAdjustments.AllowUserToOrderColumns = True
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle13.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvInvoiceAdjustments.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvInvoiceAdjustments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvInvoiceAdjustments.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvInvoiceAdjustments.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvInvoiceAdjustments.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvInvoiceAdjustments.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle14.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInvoiceAdjustments.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.dgvInvoiceAdjustments.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIVAPatientNo, Me.colInvAFullName, Me.colIVAVisitNo, Me.colIVAInvoiceNo, Me.colInvABillNo, Me.colInvAInsuranceNo, Me.colIVAInvoiceDate, Me.colAdjustmentNo, Me.colAdjustmentDate, Me.colIVAAmount})
        Me.dgvInvoiceAdjustments.ContextMenuStrip = Me.cmsEdit
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvInvoiceAdjustments.DefaultCellStyle = DataGridViewCellStyle25
        Me.dgvInvoiceAdjustments.EnableHeadersVisualStyles = False
        Me.dgvInvoiceAdjustments.GridColor = System.Drawing.Color.Khaki
        Me.dgvInvoiceAdjustments.Location = New System.Drawing.Point(2, 3)
        Me.dgvInvoiceAdjustments.Name = "dgvInvoiceAdjustments"
        Me.dgvInvoiceAdjustments.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle26.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle26.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInvoiceAdjustments.RowHeadersDefaultCellStyle = DataGridViewCellStyle26
        Me.dgvInvoiceAdjustments.RowHeadersVisible = False
        Me.dgvInvoiceAdjustments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvInvoiceAdjustments.Size = New System.Drawing.Size(1021, 213)
        Me.dgvInvoiceAdjustments.TabIndex = 2
        Me.dgvInvoiceAdjustments.Text = "DataGridView1"
        '
        'colIVAPatientNo
        '
        Me.colIVAPatientNo.DataPropertyName = "PatientNo"
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Info
        Me.colIVAPatientNo.DefaultCellStyle = DataGridViewCellStyle15
        Me.colIVAPatientNo.HeaderText = "Patient No"
        Me.colIVAPatientNo.Name = "colIVAPatientNo"
        Me.colIVAPatientNo.ReadOnly = True
        '
        'colInvAFullName
        '
        Me.colInvAFullName.DataPropertyName = "FullName"
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Info
        Me.colInvAFullName.DefaultCellStyle = DataGridViewCellStyle16
        Me.colInvAFullName.HeaderText = "Full Name"
        Me.colInvAFullName.Name = "colInvAFullName"
        Me.colInvAFullName.ReadOnly = True
        Me.colInvAFullName.Width = 200
        '
        'colIVAVisitNo
        '
        Me.colIVAVisitNo.DataPropertyName = "VisitNo"
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Info
        Me.colIVAVisitNo.DefaultCellStyle = DataGridViewCellStyle17
        Me.colIVAVisitNo.HeaderText = "Visit No"
        Me.colIVAVisitNo.Name = "colIVAVisitNo"
        Me.colIVAVisitNo.ReadOnly = True
        '
        'colIVAInvoiceNo
        '
        Me.colIVAInvoiceNo.DataPropertyName = "InvoiceNo"
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Info
        Me.colIVAInvoiceNo.DefaultCellStyle = DataGridViewCellStyle18
        Me.colIVAInvoiceNo.HeaderText = "Invoice No"
        Me.colIVAInvoiceNo.Name = "colIVAInvoiceNo"
        Me.colIVAInvoiceNo.ReadOnly = True
        '
        'colInvABillNo
        '
        Me.colInvABillNo.DataPropertyName = "BillNo"
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Info
        Me.colInvABillNo.DefaultCellStyle = DataGridViewCellStyle19
        Me.colInvABillNo.HeaderText = "Bill No"
        Me.colInvABillNo.Name = "colInvABillNo"
        Me.colInvABillNo.ReadOnly = True
        '
        'colInvAInsuranceNo
        '
        Me.colInvAInsuranceNo.DataPropertyName = "InsuranceNo"
        DataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Info
        Me.colInvAInsuranceNo.DefaultCellStyle = DataGridViewCellStyle20
        Me.colInvAInsuranceNo.HeaderText = "Insurance No"
        Me.colInvAInsuranceNo.Name = "colInvAInsuranceNo"
        Me.colInvAInsuranceNo.ReadOnly = True
        '
        'colIVAInvoiceDate
        '
        Me.colIVAInvoiceDate.DataPropertyName = "InvoiceDate"
        DataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Info
        Me.colIVAInvoiceDate.DefaultCellStyle = DataGridViewCellStyle21
        Me.colIVAInvoiceDate.HeaderText = "Invoice Date"
        Me.colIVAInvoiceDate.Name = "colIVAInvoiceDate"
        Me.colIVAInvoiceDate.ReadOnly = True
        '
        'colAdjustmentNo
        '
        Me.colAdjustmentNo.DataPropertyName = "AdjustmentNo"
        DataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Info
        Me.colAdjustmentNo.DefaultCellStyle = DataGridViewCellStyle22
        Me.colAdjustmentNo.HeaderText = "Adjustment No"
        Me.colAdjustmentNo.Name = "colAdjustmentNo"
        Me.colAdjustmentNo.ReadOnly = True
        '
        'colAdjustmentDate
        '
        Me.colAdjustmentDate.DataPropertyName = "AdjustmentDate"
        DataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Info
        Me.colAdjustmentDate.DefaultCellStyle = DataGridViewCellStyle23
        Me.colAdjustmentDate.HeaderText = "Adjustment Date"
        Me.colAdjustmentDate.Name = "colAdjustmentDate"
        Me.colAdjustmentDate.ReadOnly = True
        '
        'colIVAAmount
        '
        Me.colIVAAmount.DataPropertyName = "Amount"
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Info
        Me.colIVAAmount.DefaultCellStyle = DataGridViewCellStyle24
        Me.colIVAAmount.HeaderText = "Amount"
        Me.colIVAAmount.Name = "colIVAAmount"
        Me.colIVAAmount.ReadOnly = True
        Me.colIVAAmount.Width = 80
        '
        'tpgExtraBills
        '
        Me.tpgExtraBills.Controls.Add(Me.stbExtraBillAmountlWords)
        Me.tpgExtraBills.Controls.Add(Me.lblExtraBillAmountWords)
        Me.tpgExtraBills.Controls.Add(Me.stbExtraBillAmount)
        Me.tpgExtraBills.Controls.Add(Me.lblExtraBillAmount)
        Me.tpgExtraBills.Controls.Add(Me.dgvExtraBillItems)
        Me.tpgExtraBills.Location = New System.Drawing.Point(4, 22)
        Me.tpgExtraBills.Name = "tpgExtraBills"
        Me.tpgExtraBills.Size = New System.Drawing.Size(1026, 258)
        Me.tpgExtraBills.TabIndex = 11
        Me.tpgExtraBills.Text = "Bill Form"
        Me.tpgExtraBills.UseVisualStyleBackColor = True
        '
        'stbExtraBillAmountlWords
        '
        Me.stbExtraBillAmountlWords.AllowDrop = True
        Me.stbExtraBillAmountlWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbExtraBillAmountlWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbExtraBillAmountlWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbExtraBillAmountlWords.CapitalizeFirstLetter = False
        Me.stbExtraBillAmountlWords.EntryErrorMSG = ""
        Me.stbExtraBillAmountlWords.Location = New System.Drawing.Point(483, 222)
        Me.stbExtraBillAmountlWords.Multiline = True
        Me.stbExtraBillAmountlWords.Name = "stbExtraBillAmountlWords"
        Me.stbExtraBillAmountlWords.ReadOnly = True
        Me.stbExtraBillAmountlWords.RegularExpression = ""
        Me.stbExtraBillAmountlWords.Size = New System.Drawing.Size(335, 30)
        Me.stbExtraBillAmountlWords.TabIndex = 33
        '
        'lblExtraBillAmountWords
        '
        Me.lblExtraBillAmountWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblExtraBillAmountWords.Location = New System.Drawing.Point(336, 228)
        Me.lblExtraBillAmountWords.Name = "lblExtraBillAmountWords"
        Me.lblExtraBillAmountWords.Size = New System.Drawing.Size(131, 20)
        Me.lblExtraBillAmountWords.TabIndex = 32
        Me.lblExtraBillAmountWords.Text = "Amount In Words"
        '
        'stbExtraBillAmount
        '
        Me.stbExtraBillAmount.AllowDrop = True
        Me.stbExtraBillAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbExtraBillAmount.BackColor = System.Drawing.SystemColors.Info
        Me.stbExtraBillAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbExtraBillAmount.CapitalizeFirstLetter = False
        Me.stbExtraBillAmount.EntryErrorMSG = ""
        Me.stbExtraBillAmount.Location = New System.Drawing.Point(151, 229)
        Me.stbExtraBillAmount.Name = "stbExtraBillAmount"
        Me.stbExtraBillAmount.ReadOnly = True
        Me.stbExtraBillAmount.RegularExpression = ""
        Me.stbExtraBillAmount.Size = New System.Drawing.Size(174, 20)
        Me.stbExtraBillAmount.TabIndex = 14
        '
        'lblExtraBillAmount
        '
        Me.lblExtraBillAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblExtraBillAmount.Location = New System.Drawing.Point(5, 231)
        Me.lblExtraBillAmount.Name = "lblExtraBillAmount"
        Me.lblExtraBillAmount.Size = New System.Drawing.Size(143, 20)
        Me.lblExtraBillAmount.TabIndex = 13
        Me.lblExtraBillAmount.Text = "Total Amount"
        '
        'dgvExtraBillItems
        '
        Me.dgvExtraBillItems.AllowUserToAddRows = False
        Me.dgvExtraBillItems.AllowUserToDeleteRows = False
        Me.dgvExtraBillItems.AllowUserToOrderColumns = True
        DataGridViewCellStyle27.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle27.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvExtraBillItems.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle27
        Me.dgvExtraBillItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvExtraBillItems.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvExtraBillItems.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvExtraBillItems.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvExtraBillItems.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle28.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle28.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvExtraBillItems.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle28
        Me.dgvExtraBillItems.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colBFPatientNo, Me.colBFFullName, Me.colBFBillNo, Me.colBFInsuranceNo, Me.colBFVisitNo, Me.colExtraBillNo, Me.colVisitType, Me.colBFAmount, Me.colExtraBillDate})
        Me.dgvExtraBillItems.ContextMenuStrip = Me.cmsEdit
        DataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle38.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvExtraBillItems.DefaultCellStyle = DataGridViewCellStyle38
        Me.dgvExtraBillItems.EnableHeadersVisualStyles = False
        Me.dgvExtraBillItems.GridColor = System.Drawing.Color.Khaki
        Me.dgvExtraBillItems.Location = New System.Drawing.Point(0, 0)
        Me.dgvExtraBillItems.Name = "dgvExtraBillItems"
        Me.dgvExtraBillItems.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle39.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle39.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle39.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle39.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvExtraBillItems.RowHeadersDefaultCellStyle = DataGridViewCellStyle39
        Me.dgvExtraBillItems.RowHeadersVisible = False
        Me.dgvExtraBillItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvExtraBillItems.Size = New System.Drawing.Size(1026, 215)
        Me.dgvExtraBillItems.TabIndex = 1
        Me.dgvExtraBillItems.Text = "DataGridView1"
        '
        'tpgOPDPayments
        '
        Me.tpgOPDPayments.Controls.Add(Me.stbTotalPaymentWords)
        Me.tpgOPDPayments.Controls.Add(Me.lblPaymentWords)
        Me.tpgOPDPayments.Controls.Add(Me.stbTotalPayments)
        Me.tpgOPDPayments.Controls.Add(Me.lblTotalPayments)
        Me.tpgOPDPayments.Controls.Add(Me.dgvCashReceipts)
        Me.tpgOPDPayments.Location = New System.Drawing.Point(4, 22)
        Me.tpgOPDPayments.Name = "tpgOPDPayments"
        Me.tpgOPDPayments.Size = New System.Drawing.Size(1026, 258)
        Me.tpgOPDPayments.TabIndex = 8
        Me.tpgOPDPayments.Tag = ""
        Me.tpgOPDPayments.Text = "OPD Payments"
        Me.tpgOPDPayments.UseVisualStyleBackColor = True
        '
        'stbTotalPaymentWords
        '
        Me.stbTotalPaymentWords.AllowDrop = True
        Me.stbTotalPaymentWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbTotalPaymentWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbTotalPaymentWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalPaymentWords.CapitalizeFirstLetter = False
        Me.stbTotalPaymentWords.EntryErrorMSG = ""
        Me.stbTotalPaymentWords.Location = New System.Drawing.Point(483, 226)
        Me.stbTotalPaymentWords.Multiline = True
        Me.stbTotalPaymentWords.Name = "stbTotalPaymentWords"
        Me.stbTotalPaymentWords.ReadOnly = True
        Me.stbTotalPaymentWords.RegularExpression = ""
        Me.stbTotalPaymentWords.Size = New System.Drawing.Size(335, 30)
        Me.stbTotalPaymentWords.TabIndex = 33
        '
        'lblPaymentWords
        '
        Me.lblPaymentWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblPaymentWords.Location = New System.Drawing.Point(336, 232)
        Me.lblPaymentWords.Name = "lblPaymentWords"
        Me.lblPaymentWords.Size = New System.Drawing.Size(131, 20)
        Me.lblPaymentWords.TabIndex = 32
        Me.lblPaymentWords.Text = "Amount In Words"
        '
        'stbTotalPayments
        '
        Me.stbTotalPayments.AllowDrop = True
        Me.stbTotalPayments.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbTotalPayments.BackColor = System.Drawing.SystemColors.Info
        Me.stbTotalPayments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalPayments.CapitalizeFirstLetter = False
        Me.stbTotalPayments.EntryErrorMSG = ""
        Me.stbTotalPayments.Location = New System.Drawing.Point(151, 233)
        Me.stbTotalPayments.Name = "stbTotalPayments"
        Me.stbTotalPayments.ReadOnly = True
        Me.stbTotalPayments.RegularExpression = ""
        Me.stbTotalPayments.Size = New System.Drawing.Size(174, 20)
        Me.stbTotalPayments.TabIndex = 16
        '
        'lblTotalPayments
        '
        Me.lblTotalPayments.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalPayments.Location = New System.Drawing.Point(9, 234)
        Me.lblTotalPayments.Name = "lblTotalPayments"
        Me.lblTotalPayments.Size = New System.Drawing.Size(126, 20)
        Me.lblTotalPayments.TabIndex = 15
        Me.lblTotalPayments.Text = "Total Amount"
        '
        'dgvCashReceipts
        '
        Me.dgvCashReceipts.AllowUserToAddRows = False
        Me.dgvCashReceipts.AllowUserToDeleteRows = False
        Me.dgvCashReceipts.AllowUserToOrderColumns = True
        DataGridViewCellStyle40.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle40.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvCashReceipts.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle40
        Me.dgvCashReceipts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvCashReceipts.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvCashReceipts.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvCashReceipts.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvCashReceipts.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle41.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle41.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle41.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle41.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle41.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle41.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCashReceipts.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle41
        Me.dgvCashReceipts.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colPatientNo, Me.colFullName, Me.colPayVisitNo, Me.colPayReceiptNo, Me.colPayInvoiceNo, Me.colBillNo, Me.colPayNo, Me.colPayDate, Me.colPayAmount, Me.colPayNotes, Me.colPayRecordDate, Me.colPayRecordTime})
        Me.dgvCashReceipts.ContextMenuStrip = Me.cmsEdit
        DataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle54.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle54.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle54.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle54.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle54.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle54.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvCashReceipts.DefaultCellStyle = DataGridViewCellStyle54
        Me.dgvCashReceipts.EnableHeadersVisualStyles = False
        Me.dgvCashReceipts.GridColor = System.Drawing.Color.Khaki
        Me.dgvCashReceipts.Location = New System.Drawing.Point(0, 0)
        Me.dgvCashReceipts.Name = "dgvCashReceipts"
        Me.dgvCashReceipts.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle55.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle55.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle55.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle55.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle55.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle55.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle55.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCashReceipts.RowHeadersDefaultCellStyle = DataGridViewCellStyle55
        Me.dgvCashReceipts.RowHeadersVisible = False
        Me.dgvCashReceipts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvCashReceipts.Size = New System.Drawing.Size(1023, 223)
        Me.dgvCashReceipts.TabIndex = 0
        Me.dgvCashReceipts.Text = "DataGridView1"
        '
        'tpgBillFormPayments
        '
        Me.tpgBillFormPayments.Controls.Add(Me.stbBillFormAmountWords)
        Me.tpgBillFormPayments.Controls.Add(Me.lblBillFormAmountWord)
        Me.tpgBillFormPayments.Controls.Add(Me.stbBillFormAmount)
        Me.tpgBillFormPayments.Controls.Add(Me.lblBillFormAmount)
        Me.tpgBillFormPayments.Controls.Add(Me.dgvBillFormPayments)
        Me.tpgBillFormPayments.Location = New System.Drawing.Point(4, 22)
        Me.tpgBillFormPayments.Name = "tpgBillFormPayments"
        Me.tpgBillFormPayments.Size = New System.Drawing.Size(1026, 258)
        Me.tpgBillFormPayments.TabIndex = 13
        Me.tpgBillFormPayments.Text = "Bill Form Payment"
        Me.tpgBillFormPayments.UseVisualStyleBackColor = True
        '
        'stbBillFormAmountWords
        '
        Me.stbBillFormAmountWords.AllowDrop = True
        Me.stbBillFormAmountWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbBillFormAmountWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbBillFormAmountWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBillFormAmountWords.CapitalizeFirstLetter = False
        Me.stbBillFormAmountWords.EntryErrorMSG = ""
        Me.stbBillFormAmountWords.Location = New System.Drawing.Point(483, 227)
        Me.stbBillFormAmountWords.Multiline = True
        Me.stbBillFormAmountWords.Name = "stbBillFormAmountWords"
        Me.stbBillFormAmountWords.ReadOnly = True
        Me.stbBillFormAmountWords.RegularExpression = ""
        Me.stbBillFormAmountWords.Size = New System.Drawing.Size(335, 30)
        Me.stbBillFormAmountWords.TabIndex = 38
        '
        'lblBillFormAmountWord
        '
        Me.lblBillFormAmountWord.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblBillFormAmountWord.Location = New System.Drawing.Point(336, 233)
        Me.lblBillFormAmountWord.Name = "lblBillFormAmountWord"
        Me.lblBillFormAmountWord.Size = New System.Drawing.Size(131, 20)
        Me.lblBillFormAmountWord.TabIndex = 37
        Me.lblBillFormAmountWord.Text = "Amount In Words"
        '
        'stbBillFormAmount
        '
        Me.stbBillFormAmount.AllowDrop = True
        Me.stbBillFormAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbBillFormAmount.BackColor = System.Drawing.SystemColors.Info
        Me.stbBillFormAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBillFormAmount.CapitalizeFirstLetter = False
        Me.stbBillFormAmount.EntryErrorMSG = ""
        Me.stbBillFormAmount.Location = New System.Drawing.Point(151, 234)
        Me.stbBillFormAmount.Name = "stbBillFormAmount"
        Me.stbBillFormAmount.ReadOnly = True
        Me.stbBillFormAmount.RegularExpression = ""
        Me.stbBillFormAmount.Size = New System.Drawing.Size(174, 20)
        Me.stbBillFormAmount.TabIndex = 36
        '
        'lblBillFormAmount
        '
        Me.lblBillFormAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblBillFormAmount.Location = New System.Drawing.Point(9, 235)
        Me.lblBillFormAmount.Name = "lblBillFormAmount"
        Me.lblBillFormAmount.Size = New System.Drawing.Size(126, 20)
        Me.lblBillFormAmount.TabIndex = 35
        Me.lblBillFormAmount.Text = "Total Amount"
        '
        'dgvBillFormPayments
        '
        Me.dgvBillFormPayments.AllowUserToAddRows = False
        Me.dgvBillFormPayments.AllowUserToDeleteRows = False
        Me.dgvBillFormPayments.AllowUserToOrderColumns = True
        DataGridViewCellStyle56.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle56.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvBillFormPayments.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle56
        Me.dgvBillFormPayments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvBillFormPayments.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvBillFormPayments.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvBillFormPayments.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvBillFormPayments.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle57.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle57.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle57.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle57.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle57.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle57.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle57.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBillFormPayments.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle57
        Me.dgvBillFormPayments.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colBFPPatientNo, Me.colBFPFullName, Me.colBFPVisitNo, Me.colBFPReceiptNo, Me.colBFPExtraBillNo, Me.colBFPBillNo, Me.colBFPInsuranceNo, Me.colBFPPayNo, Me.colBFPPayDate, Me.colBFPAmount, Me.colBFPNotes, Me.colBFPRecordDate, Me.colBFPRecordTime})
        Me.dgvBillFormPayments.ContextMenuStrip = Me.cmsEdit
        DataGridViewCellStyle71.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle71.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle71.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle71.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle71.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle71.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle71.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvBillFormPayments.DefaultCellStyle = DataGridViewCellStyle71
        Me.dgvBillFormPayments.EnableHeadersVisualStyles = False
        Me.dgvBillFormPayments.GridColor = System.Drawing.Color.Khaki
        Me.dgvBillFormPayments.Location = New System.Drawing.Point(0, 1)
        Me.dgvBillFormPayments.Name = "dgvBillFormPayments"
        Me.dgvBillFormPayments.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle72.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle72.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle72.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle72.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle72.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle72.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle72.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBillFormPayments.RowHeadersDefaultCellStyle = DataGridViewCellStyle72
        Me.dgvBillFormPayments.RowHeadersVisible = False
        Me.dgvBillFormPayments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvBillFormPayments.Size = New System.Drawing.Size(1023, 223)
        Me.dgvBillFormPayments.TabIndex = 34
        Me.dgvBillFormPayments.Text = "DataGridView1"
        '
        'tpgOPDRefunds
        '
        Me.tpgOPDRefunds.Controls.Add(Me.stbTotalRefundWords)
        Me.tpgOPDRefunds.Controls.Add(Me.lblRefundWords)
        Me.tpgOPDRefunds.Controls.Add(Me.stbTotalRefunds)
        Me.tpgOPDRefunds.Controls.Add(Me.lblTotalRefunds)
        Me.tpgOPDRefunds.Controls.Add(Me.dgvRefunds)
        Me.tpgOPDRefunds.Location = New System.Drawing.Point(4, 22)
        Me.tpgOPDRefunds.Name = "tpgOPDRefunds"
        Me.tpgOPDRefunds.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgOPDRefunds.Size = New System.Drawing.Size(1026, 258)
        Me.tpgOPDRefunds.TabIndex = 9
        Me.tpgOPDRefunds.Text = "OPD Refunds"
        Me.tpgOPDRefunds.UseVisualStyleBackColor = True
        '
        'stbTotalRefundWords
        '
        Me.stbTotalRefundWords.AllowDrop = True
        Me.stbTotalRefundWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbTotalRefundWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbTotalRefundWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalRefundWords.CapitalizeFirstLetter = False
        Me.stbTotalRefundWords.EntryErrorMSG = ""
        Me.stbTotalRefundWords.Location = New System.Drawing.Point(483, 226)
        Me.stbTotalRefundWords.Multiline = True
        Me.stbTotalRefundWords.Name = "stbTotalRefundWords"
        Me.stbTotalRefundWords.ReadOnly = True
        Me.stbTotalRefundWords.RegularExpression = ""
        Me.stbTotalRefundWords.Size = New System.Drawing.Size(335, 30)
        Me.stbTotalRefundWords.TabIndex = 31
        '
        'lblRefundWords
        '
        Me.lblRefundWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblRefundWords.Location = New System.Drawing.Point(344, 232)
        Me.lblRefundWords.Name = "lblRefundWords"
        Me.lblRefundWords.Size = New System.Drawing.Size(133, 20)
        Me.lblRefundWords.TabIndex = 30
        Me.lblRefundWords.Text = "Amount In Words"
        '
        'stbTotalRefunds
        '
        Me.stbTotalRefunds.AllowDrop = True
        Me.stbTotalRefunds.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbTotalRefunds.BackColor = System.Drawing.SystemColors.Info
        Me.stbTotalRefunds.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalRefunds.CapitalizeFirstLetter = False
        Me.stbTotalRefunds.EntryErrorMSG = ""
        Me.stbTotalRefunds.Location = New System.Drawing.Point(151, 233)
        Me.stbTotalRefunds.Name = "stbTotalRefunds"
        Me.stbTotalRefunds.ReadOnly = True
        Me.stbTotalRefunds.RegularExpression = ""
        Me.stbTotalRefunds.Size = New System.Drawing.Size(174, 20)
        Me.stbTotalRefunds.TabIndex = 18
        '
        'lblTotalRefunds
        '
        Me.lblTotalRefunds.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalRefunds.Location = New System.Drawing.Point(5, 235)
        Me.lblTotalRefunds.Name = "lblTotalRefunds"
        Me.lblTotalRefunds.Size = New System.Drawing.Size(127, 20)
        Me.lblTotalRefunds.TabIndex = 17
        Me.lblTotalRefunds.Text = "Total Amount"
        '
        'dgvRefunds
        '
        Me.dgvRefunds.AllowUserToAddRows = False
        Me.dgvRefunds.AllowUserToDeleteRows = False
        Me.dgvRefunds.AllowUserToOrderColumns = True
        DataGridViewCellStyle73.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle73.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvRefunds.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle73
        Me.dgvRefunds.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvRefunds.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvRefunds.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvRefunds.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvRefunds.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle74.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle74.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle74.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle74.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle74.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle74.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle74.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRefunds.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle74
        Me.dgvRefunds.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colRefPatientNo, Me.colRefFullName, Me.colRefReceiptNo, Me.colRefRefundNo, Me.colRefInvoiceNo, Me.colRefBillNo, Me.colRefInsuranceNo, Me.colRefundDate, Me.colRefAmount, Me.colRefNotes, Me.colRefundRecordDate, Me.colRefTime})
        Me.dgvRefunds.ContextMenuStrip = Me.cmsEdit
        DataGridViewCellStyle87.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle87.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle87.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle87.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle87.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle87.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle87.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvRefunds.DefaultCellStyle = DataGridViewCellStyle87
        Me.dgvRefunds.EnableHeadersVisualStyles = False
        Me.dgvRefunds.GridColor = System.Drawing.Color.Khaki
        Me.dgvRefunds.Location = New System.Drawing.Point(3, 3)
        Me.dgvRefunds.Name = "dgvRefunds"
        Me.dgvRefunds.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle88.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle88.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle88.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle88.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle88.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle88.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle88.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRefunds.RowHeadersDefaultCellStyle = DataGridViewCellStyle88
        Me.dgvRefunds.RowHeadersVisible = False
        Me.dgvRefunds.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvRefunds.Size = New System.Drawing.Size(1017, 221)
        Me.dgvRefunds.TabIndex = 15
        Me.dgvRefunds.Text = "DataGridView1"
        '
        'tpgExtraBillRefunds
        '
        Me.tpgExtraBillRefunds.Controls.Add(Me.stbExtraBillRefundWords)
        Me.tpgExtraBillRefunds.Controls.Add(Me.lblExtraBillRefundAmpont)
        Me.tpgExtraBillRefunds.Controls.Add(Me.stbExtraBillRefunds)
        Me.tpgExtraBillRefunds.Controls.Add(Me.lblExtraBillRefunds)
        Me.tpgExtraBillRefunds.Controls.Add(Me.dgvExtraBillRefunds)
        Me.tpgExtraBillRefunds.Location = New System.Drawing.Point(4, 22)
        Me.tpgExtraBillRefunds.Name = "tpgExtraBillRefunds"
        Me.tpgExtraBillRefunds.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgExtraBillRefunds.Size = New System.Drawing.Size(1026, 258)
        Me.tpgExtraBillRefunds.TabIndex = 14
        Me.tpgExtraBillRefunds.Text = "Bill Form Refunds"
        Me.tpgExtraBillRefunds.UseVisualStyleBackColor = True
        '
        'stbExtraBillRefundWords
        '
        Me.stbExtraBillRefundWords.AllowDrop = True
        Me.stbExtraBillRefundWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbExtraBillRefundWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbExtraBillRefundWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbExtraBillRefundWords.CapitalizeFirstLetter = False
        Me.stbExtraBillRefundWords.EntryErrorMSG = ""
        Me.stbExtraBillRefundWords.Location = New System.Drawing.Point(483, 225)
        Me.stbExtraBillRefundWords.Multiline = True
        Me.stbExtraBillRefundWords.Name = "stbExtraBillRefundWords"
        Me.stbExtraBillRefundWords.ReadOnly = True
        Me.stbExtraBillRefundWords.RegularExpression = ""
        Me.stbExtraBillRefundWords.Size = New System.Drawing.Size(335, 30)
        Me.stbExtraBillRefundWords.TabIndex = 36
        '
        'lblExtraBillRefundAmpont
        '
        Me.lblExtraBillRefundAmpont.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblExtraBillRefundAmpont.Location = New System.Drawing.Point(344, 231)
        Me.lblExtraBillRefundAmpont.Name = "lblExtraBillRefundAmpont"
        Me.lblExtraBillRefundAmpont.Size = New System.Drawing.Size(133, 20)
        Me.lblExtraBillRefundAmpont.TabIndex = 35
        Me.lblExtraBillRefundAmpont.Text = "Amount In Words"
        '
        'stbExtraBillRefunds
        '
        Me.stbExtraBillRefunds.AllowDrop = True
        Me.stbExtraBillRefunds.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbExtraBillRefunds.BackColor = System.Drawing.SystemColors.Info
        Me.stbExtraBillRefunds.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbExtraBillRefunds.CapitalizeFirstLetter = False
        Me.stbExtraBillRefunds.EntryErrorMSG = ""
        Me.stbExtraBillRefunds.Location = New System.Drawing.Point(151, 232)
        Me.stbExtraBillRefunds.Name = "stbExtraBillRefunds"
        Me.stbExtraBillRefunds.ReadOnly = True
        Me.stbExtraBillRefunds.RegularExpression = ""
        Me.stbExtraBillRefunds.Size = New System.Drawing.Size(174, 20)
        Me.stbExtraBillRefunds.TabIndex = 34
        '
        'lblExtraBillRefunds
        '
        Me.lblExtraBillRefunds.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblExtraBillRefunds.Location = New System.Drawing.Point(7, 235)
        Me.lblExtraBillRefunds.Name = "lblExtraBillRefunds"
        Me.lblExtraBillRefunds.Size = New System.Drawing.Size(127, 20)
        Me.lblExtraBillRefunds.TabIndex = 33
        Me.lblExtraBillRefunds.Text = "Total Amount"
        '
        'dgvExtraBillRefunds
        '
        Me.dgvExtraBillRefunds.AllowUserToAddRows = False
        Me.dgvExtraBillRefunds.AllowUserToDeleteRows = False
        Me.dgvExtraBillRefunds.AllowUserToOrderColumns = True
        DataGridViewCellStyle89.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle89.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvExtraBillRefunds.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle89
        Me.dgvExtraBillRefunds.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvExtraBillRefunds.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvExtraBillRefunds.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvExtraBillRefunds.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvExtraBillRefunds.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle90.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle90.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle90.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle90.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle90.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle90.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle90.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvExtraBillRefunds.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle90
        Me.dgvExtraBillRefunds.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colExtRefPatientNo, Me.colEXTRefFullName, Me.colEXTRefReceiptNo, Me.colEXTRefExtraBillNo, Me.colEXTRefRefundNo, Me.colEXTRefBillNo, Me.colEXTRefInsuranceNo, Me.colEXTRefRefundDate, Me.colEXTRefAmount, Me.colEXTRefNotes, Me.colEXTRefRecordDate, Me.colEXTRefRecordTime})
        Me.dgvExtraBillRefunds.ContextMenuStrip = Me.cmsEdit
        DataGridViewCellStyle103.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle103.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle103.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle103.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle103.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle103.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle103.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvExtraBillRefunds.DefaultCellStyle = DataGridViewCellStyle103
        Me.dgvExtraBillRefunds.EnableHeadersVisualStyles = False
        Me.dgvExtraBillRefunds.GridColor = System.Drawing.Color.Khaki
        Me.dgvExtraBillRefunds.Location = New System.Drawing.Point(5, 3)
        Me.dgvExtraBillRefunds.Name = "dgvExtraBillRefunds"
        Me.dgvExtraBillRefunds.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle104.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle104.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle104.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle104.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle104.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle104.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle104.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle104.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvExtraBillRefunds.RowHeadersDefaultCellStyle = DataGridViewCellStyle104
        Me.dgvExtraBillRefunds.RowHeadersVisible = False
        Me.dgvExtraBillRefunds.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvExtraBillRefunds.Size = New System.Drawing.Size(1017, 221)
        Me.dgvExtraBillRefunds.TabIndex = 32
        Me.dgvExtraBillRefunds.Text = "DataGridView1"
        '
        'tpgAccountTransactions
        '
        Me.tpgAccountTransactions.Controls.Add(Me.stbNetAmountWords)
        Me.tpgAccountTransactions.Controls.Add(Me.lblNetAmountWords)
        Me.tpgAccountTransactions.Controls.Add(Me.stbNetAmount)
        Me.tpgAccountTransactions.Controls.Add(Me.lblNetAmount)
        Me.tpgAccountTransactions.Controls.Add(Me.stbTotalCredit)
        Me.tpgAccountTransactions.Controls.Add(Me.lblTotalCredit)
        Me.tpgAccountTransactions.Controls.Add(Me.stbTotalDebit)
        Me.tpgAccountTransactions.Controls.Add(Me.lblTotalDebit)
        Me.tpgAccountTransactions.Controls.Add(Me.stbPatientChequePaymentsWords)
        Me.tpgAccountTransactions.Controls.Add(Me.lblExpenditureTotalAmount)
        Me.tpgAccountTransactions.Controls.Add(Me.lblExpenditureAmountWords)
        Me.tpgAccountTransactions.Controls.Add(Me.stbPatientChequePayments)
        Me.tpgAccountTransactions.Controls.Add(Me.dgvAccountTrasaction)
        Me.tpgAccountTransactions.Location = New System.Drawing.Point(4, 22)
        Me.tpgAccountTransactions.Name = "tpgAccountTransactions"
        Me.tpgAccountTransactions.Size = New System.Drawing.Size(1026, 258)
        Me.tpgAccountTransactions.TabIndex = 7
        Me.tpgAccountTransactions.Tag = "Account TransactionsDeposits"
        Me.tpgAccountTransactions.Text = "Account Transactions"
        Me.tpgAccountTransactions.UseVisualStyleBackColor = True
        '
        'stbNetAmountWords
        '
        Me.stbNetAmountWords.AllowDrop = True
        Me.stbNetAmountWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbNetAmountWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbNetAmountWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbNetAmountWords.CapitalizeFirstLetter = False
        Me.stbNetAmountWords.EntryErrorMSG = ""
        Me.stbNetAmountWords.Location = New System.Drawing.Point(483, 222)
        Me.stbNetAmountWords.Multiline = True
        Me.stbNetAmountWords.Name = "stbNetAmountWords"
        Me.stbNetAmountWords.ReadOnly = True
        Me.stbNetAmountWords.RegularExpression = ""
        Me.stbNetAmountWords.Size = New System.Drawing.Size(335, 30)
        Me.stbNetAmountWords.TabIndex = 29
        '
        'lblNetAmountWords
        '
        Me.lblNetAmountWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblNetAmountWords.Location = New System.Drawing.Point(336, 228)
        Me.lblNetAmountWords.Name = "lblNetAmountWords"
        Me.lblNetAmountWords.Size = New System.Drawing.Size(131, 20)
        Me.lblNetAmountWords.TabIndex = 28
        Me.lblNetAmountWords.Text = "Net Amount In Words"
        '
        'stbNetAmount
        '
        Me.stbNetAmount.AllowDrop = True
        Me.stbNetAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbNetAmount.BackColor = System.Drawing.SystemColors.Info
        Me.stbNetAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbNetAmount.CapitalizeFirstLetter = False
        Me.stbNetAmount.EntryErrorMSG = ""
        Me.stbNetAmount.Location = New System.Drawing.Point(152, 230)
        Me.stbNetAmount.Name = "stbNetAmount"
        Me.stbNetAmount.ReadOnly = True
        Me.stbNetAmount.RegularExpression = ""
        Me.stbNetAmount.Size = New System.Drawing.Size(173, 20)
        Me.stbNetAmount.TabIndex = 27
        '
        'lblNetAmount
        '
        Me.lblNetAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblNetAmount.Location = New System.Drawing.Point(3, 232)
        Me.lblNetAmount.Name = "lblNetAmount"
        Me.lblNetAmount.Size = New System.Drawing.Size(111, 20)
        Me.lblNetAmount.TabIndex = 26
        Me.lblNetAmount.Text = "Net Amount"
        '
        'stbTotalCredit
        '
        Me.stbTotalCredit.AllowDrop = True
        Me.stbTotalCredit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbTotalCredit.BackColor = System.Drawing.SystemColors.Info
        Me.stbTotalCredit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalCredit.CapitalizeFirstLetter = False
        Me.stbTotalCredit.EntryErrorMSG = ""
        Me.stbTotalCredit.Location = New System.Drawing.Point(152, 182)
        Me.stbTotalCredit.Name = "stbTotalCredit"
        Me.stbTotalCredit.ReadOnly = True
        Me.stbTotalCredit.RegularExpression = ""
        Me.stbTotalCredit.Size = New System.Drawing.Size(173, 20)
        Me.stbTotalCredit.TabIndex = 25
        '
        'lblTotalCredit
        '
        Me.lblTotalCredit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalCredit.Location = New System.Drawing.Point(5, 182)
        Me.lblTotalCredit.Name = "lblTotalCredit"
        Me.lblTotalCredit.Size = New System.Drawing.Size(109, 20)
        Me.lblTotalCredit.TabIndex = 24
        Me.lblTotalCredit.Text = "Credit"
        '
        'stbTotalDebit
        '
        Me.stbTotalDebit.AllowDrop = True
        Me.stbTotalDebit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbTotalDebit.BackColor = System.Drawing.SystemColors.Info
        Me.stbTotalDebit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalDebit.CapitalizeFirstLetter = False
        Me.stbTotalDebit.EntryErrorMSG = ""
        Me.stbTotalDebit.Location = New System.Drawing.Point(152, 205)
        Me.stbTotalDebit.Name = "stbTotalDebit"
        Me.stbTotalDebit.ReadOnly = True
        Me.stbTotalDebit.RegularExpression = ""
        Me.stbTotalDebit.Size = New System.Drawing.Size(173, 20)
        Me.stbTotalDebit.TabIndex = 23
        '
        'lblTotalDebit
        '
        Me.lblTotalDebit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalDebit.Location = New System.Drawing.Point(5, 207)
        Me.lblTotalDebit.Name = "lblTotalDebit"
        Me.lblTotalDebit.Size = New System.Drawing.Size(109, 20)
        Me.lblTotalDebit.TabIndex = 22
        Me.lblTotalDebit.Text = "Debit"
        '
        'stbPatientChequePaymentsWords
        '
        Me.stbPatientChequePaymentsWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbPatientChequePaymentsWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbPatientChequePaymentsWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPatientChequePaymentsWords.CapitalizeFirstLetter = False
        Me.stbPatientChequePaymentsWords.EntryErrorMSG = ""
        Me.stbPatientChequePaymentsWords.Location = New System.Drawing.Point(419, 316)
        Me.stbPatientChequePaymentsWords.MaxLength = 100
        Me.stbPatientChequePaymentsWords.Multiline = True
        Me.stbPatientChequePaymentsWords.Name = "stbPatientChequePaymentsWords"
        Me.stbPatientChequePaymentsWords.ReadOnly = True
        Me.stbPatientChequePaymentsWords.RegularExpression = ""
        Me.stbPatientChequePaymentsWords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbPatientChequePaymentsWords.Size = New System.Drawing.Size(377, 39)
        Me.stbPatientChequePaymentsWords.TabIndex = 12
        '
        'lblExpenditureTotalAmount
        '
        Me.lblExpenditureTotalAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblExpenditureTotalAmount.Location = New System.Drawing.Point(7, 329)
        Me.lblExpenditureTotalAmount.Name = "lblExpenditureTotalAmount"
        Me.lblExpenditureTotalAmount.Size = New System.Drawing.Size(84, 20)
        Me.lblExpenditureTotalAmount.TabIndex = 9
        Me.lblExpenditureTotalAmount.Text = "Total Amount"
        '
        'lblExpenditureAmountWords
        '
        Me.lblExpenditureAmountWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblExpenditureAmountWords.Location = New System.Drawing.Point(287, 329)
        Me.lblExpenditureAmountWords.Name = "lblExpenditureAmountWords"
        Me.lblExpenditureAmountWords.Size = New System.Drawing.Size(126, 21)
        Me.lblExpenditureAmountWords.TabIndex = 11
        Me.lblExpenditureAmountWords.Text = "Amount in Words"
        '
        'stbPatientChequePayments
        '
        Me.stbPatientChequePayments.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbPatientChequePayments.BackColor = System.Drawing.SystemColors.Info
        Me.stbPatientChequePayments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPatientChequePayments.CapitalizeFirstLetter = False
        Me.stbPatientChequePayments.Enabled = False
        Me.stbPatientChequePayments.EntryErrorMSG = ""
        Me.stbPatientChequePayments.Location = New System.Drawing.Point(97, 327)
        Me.stbPatientChequePayments.MaxLength = 20
        Me.stbPatientChequePayments.Name = "stbPatientChequePayments"
        Me.stbPatientChequePayments.RegularExpression = ""
        Me.stbPatientChequePayments.Size = New System.Drawing.Size(184, 20)
        Me.stbPatientChequePayments.TabIndex = 10
        Me.stbPatientChequePayments.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgvAccountTrasaction
        '
        Me.dgvAccountTrasaction.AllowUserToAddRows = False
        Me.dgvAccountTrasaction.AllowUserToDeleteRows = False
        Me.dgvAccountTrasaction.AllowUserToOrderColumns = True
        DataGridViewCellStyle105.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle105.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvAccountTrasaction.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle105
        Me.dgvAccountTrasaction.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvAccountTrasaction.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvAccountTrasaction.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvAccountTrasaction.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvAccountTrasaction.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle106.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle106.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle106.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle106.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle106.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle106.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle106.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle106.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAccountTrasaction.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle106
        Me.dgvAccountTrasaction.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colTranNo, Me.colTranDate, Me.collAccAmount, Me.colCredit, Me.colDebit, Me.colBalance, Me.colAccNotes, Me.colAccRecordDate, Me.colAccTime})
        Me.dgvAccountTrasaction.ContextMenuStrip = Me.cmsEdit
        DataGridViewCellStyle116.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle116.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle116.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle116.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle116.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle116.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle116.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvAccountTrasaction.DefaultCellStyle = DataGridViewCellStyle116
        Me.dgvAccountTrasaction.EnableHeadersVisualStyles = False
        Me.dgvAccountTrasaction.GridColor = System.Drawing.Color.Khaki
        Me.dgvAccountTrasaction.Location = New System.Drawing.Point(0, 0)
        Me.dgvAccountTrasaction.Name = "dgvAccountTrasaction"
        Me.dgvAccountTrasaction.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle117.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle117.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle117.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle117.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle117.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle117.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle117.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle117.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAccountTrasaction.RowHeadersDefaultCellStyle = DataGridViewCellStyle117
        Me.dgvAccountTrasaction.RowHeadersVisible = False
        Me.dgvAccountTrasaction.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvAccountTrasaction.Size = New System.Drawing.Size(1026, 179)
        Me.dgvAccountTrasaction.TabIndex = 2
        Me.dgvAccountTrasaction.Text = "DataGridView1"
        '
        'cboBillMode
        '
        Me.cboBillMode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboBillMode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBillMode.DropDownWidth = 300
        Me.cboBillMode.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboBillMode.FormattingEnabled = True
        Me.cboBillMode.Location = New System.Drawing.Point(145, 31)
        Me.cboBillMode.MaxLength = 20
        Me.cboBillMode.Name = "cboBillMode"
        Me.cboBillMode.Size = New System.Drawing.Size(171, 21)
        Me.cboBillMode.TabIndex = 2
        '
        'fbnLoad
        '
        Me.fbnLoad.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnLoad.Location = New System.Drawing.Point(508, 122)
        Me.fbnLoad.Name = "fbnLoad"
        Me.fbnLoad.Size = New System.Drawing.Size(70, 22)
        Me.fbnLoad.TabIndex = 17
        Me.fbnLoad.Text = "&Load"
        '
        'btnPrintPreview
        '
        Me.btnPrintPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintPreview.Enabled = False
        Me.btnPrintPreview.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrintPreview.Location = New System.Drawing.Point(110, 468)
        Me.btnPrintPreview.Name = "btnPrintPreview"
        Me.btnPrintPreview.Size = New System.Drawing.Size(90, 24)
        Me.btnPrintPreview.TabIndex = 2
        Me.btnPrintPreview.Text = "Print Pre&view"
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Enabled = False
        Me.btnPrint.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Location = New System.Drawing.Point(14, 468)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(90, 24)
        Me.btnPrint.TabIndex = 1
        Me.btnPrint.Text = "&Print"
        '
        'cboAccountNo
        '
        Me.cboAccountNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboAccountNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAccountNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboAccountNo.DropDownWidth = 256
        Me.cboAccountNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboAccountNo.FormattingEnabled = True
        Me.cboAccountNo.ItemHeight = 13
        Me.cboAccountNo.Location = New System.Drawing.Point(145, 55)
        Me.cboAccountNo.Name = "cboAccountNo"
        Me.cboAccountNo.Size = New System.Drawing.Size(171, 21)
        Me.cboAccountNo.TabIndex = 21
        '
        'lblAccountNo
        '
        Me.lblAccountNo.Location = New System.Drawing.Point(17, 60)
        Me.lblAccountNo.Name = "lblAccountNo"
        Me.lblAccountNo.Size = New System.Drawing.Size(113, 18)
        Me.lblAccountNo.TabIndex = 20
        Me.lblAccountNo.Text = "Account No"
        '
        'btnLoad
        '
        Me.btnLoad.AccessibleDescription = ""
        Me.btnLoad.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoad.Location = New System.Drawing.Point(322, 55)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(46, 24)
        Me.btnLoad.TabIndex = 22
        Me.btnLoad.Tag = ""
        Me.btnLoad.Text = "&Load"
        Me.btnLoad.Visible = False
        '
        'stbBalance
        '
        Me.stbBalance.AllowDrop = True
        Me.stbBalance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbBalance.BackColor = System.Drawing.SystemColors.Info
        Me.stbBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBalance.CapitalizeFirstLetter = False
        Me.stbBalance.EntryErrorMSG = ""
        Me.stbBalance.Location = New System.Drawing.Point(167, 437)
        Me.stbBalance.Name = "stbBalance"
        Me.stbBalance.ReadOnly = True
        Me.stbBalance.RegularExpression = ""
        Me.stbBalance.Size = New System.Drawing.Size(174, 20)
        Me.stbBalance.TabIndex = 25
        '
        'lblBalance
        '
        Me.lblBalance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblBalance.Location = New System.Drawing.Point(21, 440)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(130, 20)
        Me.lblBalance.TabIndex = 24
        Me.lblBalance.Text = "Balance"
        '
        'stbBalanceWords
        '
        Me.stbBalanceWords.AllowDrop = True
        Me.stbBalanceWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbBalanceWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbBalanceWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBalanceWords.CapitalizeFirstLetter = False
        Me.stbBalanceWords.EntryErrorMSG = ""
        Me.stbBalanceWords.Location = New System.Drawing.Point(499, 434)
        Me.stbBalanceWords.Multiline = True
        Me.stbBalanceWords.Name = "stbBalanceWords"
        Me.stbBalanceWords.ReadOnly = True
        Me.stbBalanceWords.RegularExpression = ""
        Me.stbBalanceWords.Size = New System.Drawing.Size(335, 30)
        Me.stbBalanceWords.TabIndex = 33
        '
        'lblBalanceWords
        '
        Me.lblBalanceWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblBalanceWords.Location = New System.Drawing.Point(352, 437)
        Me.lblBalanceWords.Name = "lblBalanceWords"
        Me.lblBalanceWords.Size = New System.Drawing.Size(131, 20)
        Me.lblBalanceWords.TabIndex = 32
        Me.lblBalanceWords.Text = "Balance In Words"
        '
        'fbnExport
        '
        Me.fbnExport.Enabled = False
        Me.fbnExport.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnExport.Location = New System.Drawing.Point(584, 122)
        Me.fbnExport.Name = "fbnExport"
        Me.fbnExport.Size = New System.Drawing.Size(74, 22)
        Me.fbnExport.TabIndex = 39
        Me.fbnExport.Text = "&Export"
        Me.fbnExport.UseVisualStyleBackColor = False
        '
        'lblRecordsNo
        '
        Me.lblRecordsNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblRecordsNo.ForeColor = System.Drawing.Color.Blue
        Me.lblRecordsNo.Location = New System.Drawing.Point(664, 124)
        Me.lblRecordsNo.Name = "lblRecordsNo"
        Me.lblRecordsNo.Size = New System.Drawing.Size(185, 13)
        Me.lblRecordsNo.TabIndex = 37
        Me.lblRecordsNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboCompanyNo
        '
        Me.cboCompanyNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboCompanyNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboCompanyNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboCompanyNo.DropDownWidth = 256
        Me.cboCompanyNo.Enabled = False
        Me.cboCompanyNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboCompanyNo.FormattingEnabled = True
        Me.cboCompanyNo.ItemHeight = 13
        Me.cboCompanyNo.Location = New System.Drawing.Point(145, 79)
        Me.cboCompanyNo.Name = "cboCompanyNo"
        Me.cboCompanyNo.Size = New System.Drawing.Size(171, 21)
        Me.cboCompanyNo.TabIndex = 43
        '
        'lblCompanyNo
        '
        Me.lblCompanyNo.Location = New System.Drawing.Point(17, 84)
        Me.lblCompanyNo.Name = "lblCompanyNo"
        Me.lblCompanyNo.Size = New System.Drawing.Size(113, 18)
        Me.lblCompanyNo.TabIndex = 42
        Me.lblCompanyNo.Text = "Company No"
        '
        'lblMainMemberNo
        '
        Me.lblMainMemberNo.AccessibleDescription = ""
        Me.lblMainMemberNo.Location = New System.Drawing.Point(17, 108)
        Me.lblMainMemberNo.Name = "lblMainMemberNo"
        Me.lblMainMemberNo.Size = New System.Drawing.Size(113, 18)
        Me.lblMainMemberNo.TabIndex = 44
        Me.lblMainMemberNo.Text = "Main Member No"
        '
        'stbMainMemberNo
        '
        Me.stbMainMemberNo.AccessibleDescription = ""
        Me.stbMainMemberNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbMainMemberNo.CapitalizeFirstLetter = False
        Me.stbMainMemberNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.stbMainMemberNo.EntryErrorMSG = ""
        Me.stbMainMemberNo.Location = New System.Drawing.Point(145, 104)
        Me.stbMainMemberNo.MaxLength = 20
        Me.stbMainMemberNo.Name = "stbMainMemberNo"
        Me.stbMainMemberNo.ReadOnly = True
        Me.stbMainMemberNo.RegularExpression = ""
        Me.stbMainMemberNo.Size = New System.Drawing.Size(171, 20)
        Me.stbMainMemberNo.TabIndex = 45
        '
        'stbAccountName
        '
        Me.stbAccountName.BackColor = System.Drawing.SystemColors.Info
        Me.stbAccountName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAccountName.CapitalizeFirstLetter = True
        Me.stbAccountName.EntryErrorMSG = ""
        Me.stbAccountName.Location = New System.Drawing.Point(508, 30)
        Me.stbAccountName.MaxLength = 60
        Me.stbAccountName.Multiline = True
        Me.stbAccountName.Name = "stbAccountName"
        Me.stbAccountName.ReadOnly = True
        Me.stbAccountName.RegularExpression = ""
        Me.stbAccountName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbAccountName.Size = New System.Drawing.Size(187, 27)
        Me.stbAccountName.TabIndex = 47
        '
        'lblAccountName
        '
        Me.lblAccountName.Location = New System.Drawing.Point(380, 30)
        Me.lblAccountName.Name = "lblAccountName"
        Me.lblAccountName.Size = New System.Drawing.Size(118, 18)
        Me.lblAccountName.TabIndex = 46
        Me.lblAccountName.Text = "Account Name"
        '
        'stbCompanyName
        '
        Me.stbCompanyName.BackColor = System.Drawing.SystemColors.Info
        Me.stbCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbCompanyName.CapitalizeFirstLetter = True
        Me.stbCompanyName.Enabled = False
        Me.stbCompanyName.EntryErrorMSG = ""
        Me.stbCompanyName.Location = New System.Drawing.Point(508, 58)
        Me.stbCompanyName.MaxLength = 60
        Me.stbCompanyName.Multiline = True
        Me.stbCompanyName.Name = "stbCompanyName"
        Me.stbCompanyName.ReadOnly = True
        Me.stbCompanyName.RegularExpression = ""
        Me.stbCompanyName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbCompanyName.Size = New System.Drawing.Size(187, 27)
        Me.stbCompanyName.TabIndex = 49
        '
        'lblCompanyName
        '
        Me.lblCompanyName.Location = New System.Drawing.Point(380, 55)
        Me.lblCompanyName.Name = "lblCompanyName"
        Me.lblCompanyName.Size = New System.Drawing.Size(118, 18)
        Me.lblCompanyName.TabIndex = 48
        Me.lblCompanyName.Text = "Company Name"
        '
        'dtpEndDateTime
        '
        Me.dtpEndDateTime.CustomFormat = "dd MMM yyyy hh:mm tt"
        Me.dtpEndDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpEndDateTime.Location = New System.Drawing.Point(508, 7)
        Me.dtpEndDateTime.Name = "dtpEndDateTime"
        Me.dtpEndDateTime.ShowCheckBox = True
        Me.dtpEndDateTime.Size = New System.Drawing.Size(187, 20)
        Me.dtpEndDateTime.TabIndex = 53
        '
        'lblStartDateTime
        '
        Me.lblStartDateTime.Location = New System.Drawing.Point(17, 2)
        Me.lblStartDateTime.Name = "lblStartDateTime"
        Me.lblStartDateTime.Size = New System.Drawing.Size(108, 20)
        Me.lblStartDateTime.TabIndex = 50
        Me.lblStartDateTime.Text = "Start Date Time"
        Me.lblStartDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStartDateTime
        '
        Me.dtpStartDateTime.CustomFormat = "dd MMM yyyy hh:mm tt"
        Me.dtpStartDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpStartDateTime.Location = New System.Drawing.Point(144, 7)
        Me.dtpStartDateTime.Name = "dtpStartDateTime"
        Me.dtpStartDateTime.ShowCheckBox = True
        Me.dtpStartDateTime.Size = New System.Drawing.Size(172, 20)
        Me.dtpStartDateTime.TabIndex = 51
        '
        'lblEndDateTime
        '
        Me.lblEndDateTime.Location = New System.Drawing.Point(383, 7)
        Me.lblEndDateTime.Name = "lblEndDateTime"
        Me.lblEndDateTime.Size = New System.Drawing.Size(112, 20)
        Me.lblEndDateTime.TabIndex = 52
        Me.lblEndDateTime.Text = "End Date Time"
        Me.lblEndDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'stbMainMemberName
        '
        Me.stbMainMemberName.BackColor = System.Drawing.SystemColors.Info
        Me.stbMainMemberName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbMainMemberName.CapitalizeFirstLetter = True
        Me.stbMainMemberName.Enabled = False
        Me.stbMainMemberName.EntryErrorMSG = ""
        Me.stbMainMemberName.Location = New System.Drawing.Point(508, 86)
        Me.stbMainMemberName.MaxLength = 60
        Me.stbMainMemberName.Multiline = True
        Me.stbMainMemberName.Name = "stbMainMemberName"
        Me.stbMainMemberName.ReadOnly = True
        Me.stbMainMemberName.RegularExpression = ""
        Me.stbMainMemberName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbMainMemberName.Size = New System.Drawing.Size(187, 27)
        Me.stbMainMemberName.TabIndex = 55
        '
        'lblMainMemberName
        '
        Me.lblMainMemberName.Location = New System.Drawing.Point(380, 83)
        Me.lblMainMemberName.Name = "lblMainMemberName"
        Me.lblMainMemberName.Size = New System.Drawing.Size(118, 18)
        Me.lblMainMemberName.TabIndex = 54
        Me.lblMainMemberName.Text = "Main Member Name"
        '
        'stbTotalBill
        '
        Me.stbTotalBill.AllowDrop = True
        Me.stbTotalBill.BackColor = System.Drawing.SystemColors.Info
        Me.stbTotalBill.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalBill.ControlCaption = "Account Balance"
        Me.stbTotalBill.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.stbTotalBill.DecimalPlaces = -1
        Me.stbTotalBill.Location = New System.Drawing.Point(867, 30)
        Me.stbTotalBill.MaxValue = 0.0R
        Me.stbTotalBill.MinValue = 0.0R
        Me.stbTotalBill.MustEnterNumeric = True
        Me.stbTotalBill.Name = "stbTotalBill"
        Me.stbTotalBill.ReadOnly = True
        Me.stbTotalBill.Size = New System.Drawing.Size(179, 20)
        Me.stbTotalBill.TabIndex = 57
        Me.stbTotalBill.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.stbTotalBill.Value = ""
        '
        'lblTotalBillAmount
        '
        Me.lblTotalBillAmount.Location = New System.Drawing.Point(701, 31)
        Me.lblTotalBillAmount.Name = "lblTotalBillAmount"
        Me.lblTotalBillAmount.Size = New System.Drawing.Size(160, 20)
        Me.lblTotalBillAmount.TabIndex = 56
        Me.lblTotalBillAmount.Text = "Total Bill"
        '
        'stbTotalPaidAmount
        '
        Me.stbTotalPaidAmount.AllowDrop = True
        Me.stbTotalPaidAmount.BackColor = System.Drawing.SystemColors.Info
        Me.stbTotalPaidAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalPaidAmount.ControlCaption = "Account Balance"
        Me.stbTotalPaidAmount.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.stbTotalPaidAmount.DecimalPlaces = -1
        Me.stbTotalPaidAmount.Location = New System.Drawing.Point(867, 53)
        Me.stbTotalPaidAmount.MaxValue = 0.0R
        Me.stbTotalPaidAmount.MinValue = 0.0R
        Me.stbTotalPaidAmount.MustEnterNumeric = True
        Me.stbTotalPaidAmount.Name = "stbTotalPaidAmount"
        Me.stbTotalPaidAmount.ReadOnly = True
        Me.stbTotalPaidAmount.Size = New System.Drawing.Size(179, 20)
        Me.stbTotalPaidAmount.TabIndex = 59
        Me.stbTotalPaidAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.stbTotalPaidAmount.Value = ""
        '
        'lblTotalPaidAmount
        '
        Me.lblTotalPaidAmount.Location = New System.Drawing.Point(704, 51)
        Me.lblTotalPaidAmount.Name = "lblTotalPaidAmount"
        Me.lblTotalPaidAmount.Size = New System.Drawing.Size(160, 20)
        Me.lblTotalPaidAmount.TabIndex = 58
        Me.lblTotalPaidAmount.Text = "Total Paid Amount"
        '
        'btnShowOutStandingBalance
        '
        Me.btnShowOutStandingBalance.Enabled = False
        Me.btnShowOutStandingBalance.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnShowOutStandingBalance.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnShowOutStandingBalance.Location = New System.Drawing.Point(704, 74)
        Me.btnShowOutStandingBalance.Name = "btnShowOutStandingBalance"
        Me.btnShowOutStandingBalance.Size = New System.Drawing.Size(160, 22)
        Me.btnShowOutStandingBalance.TabIndex = 60
        Me.btnShowOutStandingBalance.Text = "Show Outstanding Balance"
        Me.btnShowOutStandingBalance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'stbOutStandingBalance
        '
        Me.stbOutStandingBalance.AccessibleDescription = ""
        Me.stbOutStandingBalance.BackColor = System.Drawing.SystemColors.Info
        Me.stbOutStandingBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbOutStandingBalance.CapitalizeFirstLetter = False
        Me.stbOutStandingBalance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.stbOutStandingBalance.EntryErrorMSG = ""
        Me.stbOutStandingBalance.Location = New System.Drawing.Point(867, 74)
        Me.stbOutStandingBalance.MaxLength = 20
        Me.stbOutStandingBalance.Name = "stbOutStandingBalance"
        Me.stbOutStandingBalance.ReadOnly = True
        Me.stbOutStandingBalance.RegularExpression = ""
        Me.stbOutStandingBalance.Size = New System.Drawing.Size(179, 20)
        Me.stbOutStandingBalance.TabIndex = 61
        Me.stbOutStandingBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'colBFPatientNo
        '
        Me.colBFPatientNo.DataPropertyName = "PatientNo"
        DataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Info
        Me.colBFPatientNo.DefaultCellStyle = DataGridViewCellStyle29
        Me.colBFPatientNo.HeaderText = "Patient No"
        Me.colBFPatientNo.Name = "colBFPatientNo"
        Me.colBFPatientNo.ReadOnly = True
        '
        'colBFFullName
        '
        Me.colBFFullName.DataPropertyName = "FullName"
        DataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Info
        Me.colBFFullName.DefaultCellStyle = DataGridViewCellStyle30
        Me.colBFFullName.HeaderText = "Full Name"
        Me.colBFFullName.Name = "colBFFullName"
        Me.colBFFullName.ReadOnly = True
        Me.colBFFullName.Width = 200
        '
        'colBFBillNo
        '
        Me.colBFBillNo.DataPropertyName = "BillNo"
        DataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Info
        Me.colBFBillNo.DefaultCellStyle = DataGridViewCellStyle31
        Me.colBFBillNo.HeaderText = "Bill No"
        Me.colBFBillNo.Name = "colBFBillNo"
        Me.colBFBillNo.ReadOnly = True
        '
        'colBFInsuranceNo
        '
        Me.colBFInsuranceNo.DataPropertyName = "InsuranceNo"
        DataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Info
        Me.colBFInsuranceNo.DefaultCellStyle = DataGridViewCellStyle32
        Me.colBFInsuranceNo.HeaderText = "Insurance No"
        Me.colBFInsuranceNo.Name = "colBFInsuranceNo"
        Me.colBFInsuranceNo.ReadOnly = True
        '
        'colBFVisitNo
        '
        Me.colBFVisitNo.DataPropertyName = "VisitNo"
        DataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Info
        Me.colBFVisitNo.DefaultCellStyle = DataGridViewCellStyle33
        Me.colBFVisitNo.HeaderText = "Visit No"
        Me.colBFVisitNo.Name = "colBFVisitNo"
        Me.colBFVisitNo.ReadOnly = True
        '
        'colExtraBillNo
        '
        Me.colExtraBillNo.DataPropertyName = "ExtraBillNo"
        DataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Info
        Me.colExtraBillNo.DefaultCellStyle = DataGridViewCellStyle34
        Me.colExtraBillNo.HeaderText = "Extra Bill No"
        Me.colExtraBillNo.Name = "colExtraBillNo"
        Me.colExtraBillNo.ReadOnly = True
        '
        'colVisitType
        '
        Me.colVisitType.DataPropertyName = "VisitType"
        DataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Info
        Me.colVisitType.DefaultCellStyle = DataGridViewCellStyle35
        Me.colVisitType.HeaderText = "Visit Type"
        Me.colVisitType.Name = "colVisitType"
        Me.colVisitType.ReadOnly = True
        '
        'colBFAmount
        '
        Me.colBFAmount.DataPropertyName = "Amount"
        DataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Info
        Me.colBFAmount.DefaultCellStyle = DataGridViewCellStyle36
        Me.colBFAmount.HeaderText = "Amount"
        Me.colBFAmount.Name = "colBFAmount"
        Me.colBFAmount.ReadOnly = True
        Me.colBFAmount.Width = 80
        '
        'colExtraBillDate
        '
        Me.colExtraBillDate.DataPropertyName = "ExtraBillDate"
        DataGridViewCellStyle37.BackColor = System.Drawing.SystemColors.Info
        Me.colExtraBillDate.DefaultCellStyle = DataGridViewCellStyle37
        Me.colExtraBillDate.HeaderText = "Extra Bill Date"
        Me.colExtraBillDate.Name = "colExtraBillDate"
        Me.colExtraBillDate.ReadOnly = True
        '
        'colPatientNo
        '
        Me.colPatientNo.DataPropertyName = "PatientNo"
        DataGridViewCellStyle42.BackColor = System.Drawing.SystemColors.Info
        Me.colPatientNo.DefaultCellStyle = DataGridViewCellStyle42
        Me.colPatientNo.HeaderText = "Patient No"
        Me.colPatientNo.Name = "colPatientNo"
        Me.colPatientNo.ReadOnly = True
        '
        'colFullName
        '
        Me.colFullName.DataPropertyName = "FullName"
        DataGridViewCellStyle43.BackColor = System.Drawing.SystemColors.Info
        Me.colFullName.DefaultCellStyle = DataGridViewCellStyle43
        Me.colFullName.HeaderText = "Ful lName"
        Me.colFullName.Name = "colFullName"
        Me.colFullName.ReadOnly = True
        Me.colFullName.Width = 200
        '
        'colPayVisitNo
        '
        Me.colPayVisitNo.DataPropertyName = "VisitNo"
        DataGridViewCellStyle44.BackColor = System.Drawing.SystemColors.Info
        Me.colPayVisitNo.DefaultCellStyle = DataGridViewCellStyle44
        Me.colPayVisitNo.HeaderText = "Visit No"
        Me.colPayVisitNo.Name = "colPayVisitNo"
        Me.colPayVisitNo.ReadOnly = True
        '
        'colPayReceiptNo
        '
        Me.colPayReceiptNo.DataPropertyName = "ReceiptNo"
        DataGridViewCellStyle45.BackColor = System.Drawing.SystemColors.Info
        Me.colPayReceiptNo.DefaultCellStyle = DataGridViewCellStyle45
        Me.colPayReceiptNo.HeaderText = "Receipt No"
        Me.colPayReceiptNo.Name = "colPayReceiptNo"
        Me.colPayReceiptNo.ReadOnly = True
        '
        'colPayInvoiceNo
        '
        Me.colPayInvoiceNo.DataPropertyName = "InvoiceNo"
        DataGridViewCellStyle46.BackColor = System.Drawing.SystemColors.Info
        Me.colPayInvoiceNo.DefaultCellStyle = DataGridViewCellStyle46
        Me.colPayInvoiceNo.HeaderText = "Invoice No"
        Me.colPayInvoiceNo.Name = "colPayInvoiceNo"
        Me.colPayInvoiceNo.ReadOnly = True
        '
        'colBillNo
        '
        Me.colBillNo.DataPropertyName = "BillNo"
        DataGridViewCellStyle47.BackColor = System.Drawing.SystemColors.Info
        Me.colBillNo.DefaultCellStyle = DataGridViewCellStyle47
        Me.colBillNo.HeaderText = "Bill No"
        Me.colBillNo.Name = "colBillNo"
        Me.colBillNo.ReadOnly = True
        '
        'colPayNo
        '
        Me.colPayNo.DataPropertyName = "PayNo"
        DataGridViewCellStyle48.BackColor = System.Drawing.SystemColors.Info
        Me.colPayNo.DefaultCellStyle = DataGridViewCellStyle48
        Me.colPayNo.HeaderText = "Pay No"
        Me.colPayNo.Name = "colPayNo"
        Me.colPayNo.ReadOnly = True
        '
        'colPayDate
        '
        Me.colPayDate.DataPropertyName = "PayDate"
        DataGridViewCellStyle49.BackColor = System.Drawing.SystemColors.Info
        Me.colPayDate.DefaultCellStyle = DataGridViewCellStyle49
        Me.colPayDate.HeaderText = "Pay Date"
        Me.colPayDate.Name = "colPayDate"
        Me.colPayDate.ReadOnly = True
        Me.colPayDate.Width = 120
        '
        'colPayAmount
        '
        Me.colPayAmount.DataPropertyName = "Amount"
        DataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle50.BackColor = System.Drawing.SystemColors.Info
        Me.colPayAmount.DefaultCellStyle = DataGridViewCellStyle50
        Me.colPayAmount.HeaderText = "Amount"
        Me.colPayAmount.Name = "colPayAmount"
        Me.colPayAmount.ReadOnly = True
        Me.colPayAmount.Width = 80
        '
        'colPayNotes
        '
        Me.colPayNotes.DataPropertyName = "Notes"
        DataGridViewCellStyle51.BackColor = System.Drawing.SystemColors.Info
        Me.colPayNotes.DefaultCellStyle = DataGridViewCellStyle51
        Me.colPayNotes.HeaderText = "Notes"
        Me.colPayNotes.Name = "colPayNotes"
        Me.colPayNotes.ReadOnly = True
        Me.colPayNotes.Width = 150
        '
        'colPayRecordDate
        '
        Me.colPayRecordDate.DataPropertyName = "RecordDate"
        DataGridViewCellStyle52.BackColor = System.Drawing.SystemColors.Info
        Me.colPayRecordDate.DefaultCellStyle = DataGridViewCellStyle52
        Me.colPayRecordDate.HeaderText = "Record Date"
        Me.colPayRecordDate.Name = "colPayRecordDate"
        Me.colPayRecordDate.ReadOnly = True
        '
        'colPayRecordTime
        '
        Me.colPayRecordTime.DataPropertyName = "RecordTime"
        DataGridViewCellStyle53.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle53.NullValue = Nothing
        Me.colPayRecordTime.DefaultCellStyle = DataGridViewCellStyle53
        Me.colPayRecordTime.HeaderText = "Record Time"
        Me.colPayRecordTime.Name = "colPayRecordTime"
        Me.colPayRecordTime.ReadOnly = True
        '
        'colBFPPatientNo
        '
        Me.colBFPPatientNo.DataPropertyName = "PatientNo"
        DataGridViewCellStyle58.BackColor = System.Drawing.SystemColors.Info
        Me.colBFPPatientNo.DefaultCellStyle = DataGridViewCellStyle58
        Me.colBFPPatientNo.HeaderText = "Patient No"
        Me.colBFPPatientNo.Name = "colBFPPatientNo"
        Me.colBFPPatientNo.ReadOnly = True
        '
        'colBFPFullName
        '
        Me.colBFPFullName.DataPropertyName = "FullName"
        DataGridViewCellStyle59.BackColor = System.Drawing.SystemColors.Info
        Me.colBFPFullName.DefaultCellStyle = DataGridViewCellStyle59
        Me.colBFPFullName.HeaderText = "Full Name"
        Me.colBFPFullName.Name = "colBFPFullName"
        Me.colBFPFullName.ReadOnly = True
        Me.colBFPFullName.Width = 200
        '
        'colBFPVisitNo
        '
        Me.colBFPVisitNo.DataPropertyName = "VisitNo"
        DataGridViewCellStyle60.BackColor = System.Drawing.SystemColors.Info
        Me.colBFPVisitNo.DefaultCellStyle = DataGridViewCellStyle60
        Me.colBFPVisitNo.HeaderText = "Visit No"
        Me.colBFPVisitNo.Name = "colBFPVisitNo"
        Me.colBFPVisitNo.ReadOnly = True
        '
        'colBFPReceiptNo
        '
        Me.colBFPReceiptNo.DataPropertyName = "ReceiptNo"
        DataGridViewCellStyle61.BackColor = System.Drawing.SystemColors.Info
        Me.colBFPReceiptNo.DefaultCellStyle = DataGridViewCellStyle61
        Me.colBFPReceiptNo.HeaderText = "Receipt No"
        Me.colBFPReceiptNo.Name = "colBFPReceiptNo"
        Me.colBFPReceiptNo.ReadOnly = True
        '
        'colBFPExtraBillNo
        '
        Me.colBFPExtraBillNo.DataPropertyName = "ExtraBillNo"
        DataGridViewCellStyle62.BackColor = System.Drawing.SystemColors.Info
        Me.colBFPExtraBillNo.DefaultCellStyle = DataGridViewCellStyle62
        Me.colBFPExtraBillNo.HeaderText = "Extra Bill No"
        Me.colBFPExtraBillNo.Name = "colBFPExtraBillNo"
        Me.colBFPExtraBillNo.ReadOnly = True
        '
        'colBFPBillNo
        '
        Me.colBFPBillNo.DataPropertyName = "BillNo"
        DataGridViewCellStyle63.BackColor = System.Drawing.SystemColors.Info
        Me.colBFPBillNo.DefaultCellStyle = DataGridViewCellStyle63
        Me.colBFPBillNo.HeaderText = "Bill No"
        Me.colBFPBillNo.Name = "colBFPBillNo"
        Me.colBFPBillNo.ReadOnly = True
        '
        'colBFPInsuranceNo
        '
        Me.colBFPInsuranceNo.DataPropertyName = "InsuranceNo"
        DataGridViewCellStyle64.BackColor = System.Drawing.SystemColors.Info
        Me.colBFPInsuranceNo.DefaultCellStyle = DataGridViewCellStyle64
        Me.colBFPInsuranceNo.HeaderText = "Insurance No"
        Me.colBFPInsuranceNo.Name = "colBFPInsuranceNo"
        Me.colBFPInsuranceNo.ReadOnly = True
        '
        'colBFPPayNo
        '
        Me.colBFPPayNo.DataPropertyName = "PayNo"
        DataGridViewCellStyle65.BackColor = System.Drawing.SystemColors.Info
        Me.colBFPPayNo.DefaultCellStyle = DataGridViewCellStyle65
        Me.colBFPPayNo.HeaderText = "Pay No"
        Me.colBFPPayNo.Name = "colBFPPayNo"
        Me.colBFPPayNo.ReadOnly = True
        '
        'colBFPPayDate
        '
        Me.colBFPPayDate.DataPropertyName = "PayDate"
        DataGridViewCellStyle66.BackColor = System.Drawing.SystemColors.Info
        Me.colBFPPayDate.DefaultCellStyle = DataGridViewCellStyle66
        Me.colBFPPayDate.HeaderText = "Pay Date"
        Me.colBFPPayDate.Name = "colBFPPayDate"
        Me.colBFPPayDate.Width = 120
        '
        'colBFPAmount
        '
        Me.colBFPAmount.DataPropertyName = "Amount"
        DataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle67.BackColor = System.Drawing.SystemColors.Info
        Me.colBFPAmount.DefaultCellStyle = DataGridViewCellStyle67
        Me.colBFPAmount.HeaderText = "Amount"
        Me.colBFPAmount.Name = "colBFPAmount"
        Me.colBFPAmount.Width = 80
        '
        'colBFPNotes
        '
        Me.colBFPNotes.DataPropertyName = "Notes"
        DataGridViewCellStyle68.BackColor = System.Drawing.SystemColors.Info
        Me.colBFPNotes.DefaultCellStyle = DataGridViewCellStyle68
        Me.colBFPNotes.HeaderText = "Notes"
        Me.colBFPNotes.Name = "colBFPNotes"
        Me.colBFPNotes.ReadOnly = True
        Me.colBFPNotes.Width = 200
        '
        'colBFPRecordDate
        '
        Me.colBFPRecordDate.DataPropertyName = "RecordDate"
        DataGridViewCellStyle69.BackColor = System.Drawing.SystemColors.Info
        Me.colBFPRecordDate.DefaultCellStyle = DataGridViewCellStyle69
        Me.colBFPRecordDate.HeaderText = "Record Date"
        Me.colBFPRecordDate.Name = "colBFPRecordDate"
        Me.colBFPRecordDate.ReadOnly = True
        '
        'colBFPRecordTime
        '
        Me.colBFPRecordTime.DataPropertyName = "RecordTime"
        DataGridViewCellStyle70.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle70.NullValue = Nothing
        Me.colBFPRecordTime.DefaultCellStyle = DataGridViewCellStyle70
        Me.colBFPRecordTime.HeaderText = "Record Time"
        Me.colBFPRecordTime.Name = "colBFPRecordTime"
        Me.colBFPRecordTime.ReadOnly = True
        Me.colBFPRecordTime.Width = 150
        '
        'colRefPatientNo
        '
        Me.colRefPatientNo.DataPropertyName = "PatientNo"
        DataGridViewCellStyle75.BackColor = System.Drawing.SystemColors.Info
        Me.colRefPatientNo.DefaultCellStyle = DataGridViewCellStyle75
        Me.colRefPatientNo.HeaderText = "Patient No"
        Me.colRefPatientNo.Name = "colRefPatientNo"
        Me.colRefPatientNo.ReadOnly = True
        '
        'colRefFullName
        '
        Me.colRefFullName.DataPropertyName = "FullName"
        DataGridViewCellStyle76.BackColor = System.Drawing.SystemColors.Info
        Me.colRefFullName.DefaultCellStyle = DataGridViewCellStyle76
        Me.colRefFullName.HeaderText = "Full Name"
        Me.colRefFullName.Name = "colRefFullName"
        Me.colRefFullName.ReadOnly = True
        Me.colRefFullName.Width = 200
        '
        'colRefReceiptNo
        '
        Me.colRefReceiptNo.DataPropertyName = "ReceiptNo"
        DataGridViewCellStyle77.BackColor = System.Drawing.SystemColors.Info
        Me.colRefReceiptNo.DefaultCellStyle = DataGridViewCellStyle77
        Me.colRefReceiptNo.HeaderText = "ReceiptNo"
        Me.colRefReceiptNo.Name = "colRefReceiptNo"
        Me.colRefReceiptNo.ReadOnly = True
        '
        'colRefRefundNo
        '
        Me.colRefRefundNo.DataPropertyName = "RefundNo"
        DataGridViewCellStyle78.BackColor = System.Drawing.SystemColors.Info
        Me.colRefRefundNo.DefaultCellStyle = DataGridViewCellStyle78
        Me.colRefRefundNo.HeaderText = "Refund No"
        Me.colRefRefundNo.Name = "colRefRefundNo"
        Me.colRefRefundNo.ReadOnly = True
        '
        'colRefInvoiceNo
        '
        Me.colRefInvoiceNo.DataPropertyName = "InvoiceNo"
        DataGridViewCellStyle79.BackColor = System.Drawing.SystemColors.Info
        Me.colRefInvoiceNo.DefaultCellStyle = DataGridViewCellStyle79
        Me.colRefInvoiceNo.HeaderText = "Invoice No"
        Me.colRefInvoiceNo.Name = "colRefInvoiceNo"
        Me.colRefInvoiceNo.ReadOnly = True
        '
        'colRefBillNo
        '
        Me.colRefBillNo.DataPropertyName = "BillNo"
        DataGridViewCellStyle80.BackColor = System.Drawing.SystemColors.Info
        Me.colRefBillNo.DefaultCellStyle = DataGridViewCellStyle80
        Me.colRefBillNo.HeaderText = "Bill No"
        Me.colRefBillNo.Name = "colRefBillNo"
        Me.colRefBillNo.ReadOnly = True
        '
        'colRefInsuranceNo
        '
        Me.colRefInsuranceNo.DataPropertyName = "InsuranceNo"
        DataGridViewCellStyle81.BackColor = System.Drawing.SystemColors.Info
        Me.colRefInsuranceNo.DefaultCellStyle = DataGridViewCellStyle81
        Me.colRefInsuranceNo.HeaderText = "Insurance No"
        Me.colRefInsuranceNo.Name = "colRefInsuranceNo"
        Me.colRefInsuranceNo.ReadOnly = True
        '
        'colRefundDate
        '
        Me.colRefundDate.DataPropertyName = "RefundDate"
        DataGridViewCellStyle82.BackColor = System.Drawing.SystemColors.Info
        Me.colRefundDate.DefaultCellStyle = DataGridViewCellStyle82
        Me.colRefundDate.HeaderText = "Refund Date"
        Me.colRefundDate.Name = "colRefundDate"
        Me.colRefundDate.ReadOnly = True
        '
        'colRefAmount
        '
        Me.colRefAmount.DataPropertyName = "Amount"
        DataGridViewCellStyle83.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle83.BackColor = System.Drawing.SystemColors.Info
        Me.colRefAmount.DefaultCellStyle = DataGridViewCellStyle83
        Me.colRefAmount.HeaderText = "Amount"
        Me.colRefAmount.Name = "colRefAmount"
        Me.colRefAmount.ReadOnly = True
        '
        'colRefNotes
        '
        Me.colRefNotes.DataPropertyName = "Notes"
        DataGridViewCellStyle84.BackColor = System.Drawing.SystemColors.Info
        Me.colRefNotes.DefaultCellStyle = DataGridViewCellStyle84
        Me.colRefNotes.HeaderText = "Notes"
        Me.colRefNotes.Name = "colRefNotes"
        Me.colRefNotes.ReadOnly = True
        Me.colRefNotes.Width = 200
        '
        'colRefundRecordDate
        '
        Me.colRefundRecordDate.DataPropertyName = "RecordDate"
        DataGridViewCellStyle85.BackColor = System.Drawing.SystemColors.Info
        Me.colRefundRecordDate.DefaultCellStyle = DataGridViewCellStyle85
        Me.colRefundRecordDate.HeaderText = "Record Date"
        Me.colRefundRecordDate.Name = "colRefundRecordDate"
        Me.colRefundRecordDate.ReadOnly = True
        '
        'colRefTime
        '
        Me.colRefTime.DataPropertyName = "RecordTime"
        DataGridViewCellStyle86.BackColor = System.Drawing.SystemColors.Info
        Me.colRefTime.DefaultCellStyle = DataGridViewCellStyle86
        Me.colRefTime.HeaderText = "Record Time"
        Me.colRefTime.Name = "colRefTime"
        Me.colRefTime.ReadOnly = True
        '
        'colExtRefPatientNo
        '
        Me.colExtRefPatientNo.DataPropertyName = "PatientNo"
        DataGridViewCellStyle91.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle91.BackColor = System.Drawing.SystemColors.Info
        Me.colExtRefPatientNo.DefaultCellStyle = DataGridViewCellStyle91
        Me.colExtRefPatientNo.HeaderText = "Patient No"
        Me.colExtRefPatientNo.Name = "colExtRefPatientNo"
        Me.colExtRefPatientNo.ReadOnly = True
        '
        'colEXTRefFullName
        '
        Me.colEXTRefFullName.DataPropertyName = "FullName"
        DataGridViewCellStyle92.BackColor = System.Drawing.SystemColors.Info
        Me.colEXTRefFullName.DefaultCellStyle = DataGridViewCellStyle92
        Me.colEXTRefFullName.HeaderText = "Full Name"
        Me.colEXTRefFullName.Name = "colEXTRefFullName"
        Me.colEXTRefFullName.ReadOnly = True
        Me.colEXTRefFullName.Width = 200
        '
        'colEXTRefReceiptNo
        '
        Me.colEXTRefReceiptNo.DataPropertyName = "ReceiptNo"
        DataGridViewCellStyle93.BackColor = System.Drawing.SystemColors.Info
        Me.colEXTRefReceiptNo.DefaultCellStyle = DataGridViewCellStyle93
        Me.colEXTRefReceiptNo.HeaderText = "ReceiptNo"
        Me.colEXTRefReceiptNo.Name = "colEXTRefReceiptNo"
        Me.colEXTRefReceiptNo.ReadOnly = True
        '
        'colEXTRefExtraBillNo
        '
        Me.colEXTRefExtraBillNo.DataPropertyName = "ExtraBillNo"
        DataGridViewCellStyle94.BackColor = System.Drawing.SystemColors.Info
        Me.colEXTRefExtraBillNo.DefaultCellStyle = DataGridViewCellStyle94
        Me.colEXTRefExtraBillNo.HeaderText = "Extra Bill No"
        Me.colEXTRefExtraBillNo.Name = "colEXTRefExtraBillNo"
        Me.colEXTRefExtraBillNo.ReadOnly = True
        '
        'colEXTRefRefundNo
        '
        Me.colEXTRefRefundNo.DataPropertyName = "RefundNo"
        DataGridViewCellStyle95.BackColor = System.Drawing.SystemColors.Info
        Me.colEXTRefRefundNo.DefaultCellStyle = DataGridViewCellStyle95
        Me.colEXTRefRefundNo.HeaderText = "Refund No"
        Me.colEXTRefRefundNo.Name = "colEXTRefRefundNo"
        Me.colEXTRefRefundNo.ReadOnly = True
        '
        'colEXTRefBillNo
        '
        Me.colEXTRefBillNo.DataPropertyName = "BillNo"
        DataGridViewCellStyle96.BackColor = System.Drawing.SystemColors.Info
        Me.colEXTRefBillNo.DefaultCellStyle = DataGridViewCellStyle96
        Me.colEXTRefBillNo.HeaderText = "Bill No"
        Me.colEXTRefBillNo.Name = "colEXTRefBillNo"
        Me.colEXTRefBillNo.ReadOnly = True
        '
        'colEXTRefInsuranceNo
        '
        Me.colEXTRefInsuranceNo.DataPropertyName = "InsuranceNo"
        DataGridViewCellStyle97.BackColor = System.Drawing.SystemColors.Info
        Me.colEXTRefInsuranceNo.DefaultCellStyle = DataGridViewCellStyle97
        Me.colEXTRefInsuranceNo.HeaderText = "Insurance No"
        Me.colEXTRefInsuranceNo.Name = "colEXTRefInsuranceNo"
        Me.colEXTRefInsuranceNo.ReadOnly = True
        '
        'colEXTRefRefundDate
        '
        Me.colEXTRefRefundDate.DataPropertyName = "RefundDate"
        DataGridViewCellStyle98.BackColor = System.Drawing.SystemColors.Info
        Me.colEXTRefRefundDate.DefaultCellStyle = DataGridViewCellStyle98
        Me.colEXTRefRefundDate.HeaderText = "Refund Date"
        Me.colEXTRefRefundDate.Name = "colEXTRefRefundDate"
        Me.colEXTRefRefundDate.ReadOnly = True
        '
        'colEXTRefAmount
        '
        Me.colEXTRefAmount.DataPropertyName = "Amount"
        DataGridViewCellStyle99.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle99.BackColor = System.Drawing.SystemColors.Info
        Me.colEXTRefAmount.DefaultCellStyle = DataGridViewCellStyle99
        Me.colEXTRefAmount.HeaderText = "Amount"
        Me.colEXTRefAmount.Name = "colEXTRefAmount"
        Me.colEXTRefAmount.ReadOnly = True
        '
        'colEXTRefNotes
        '
        Me.colEXTRefNotes.DataPropertyName = "Notes"
        DataGridViewCellStyle100.BackColor = System.Drawing.SystemColors.Info
        Me.colEXTRefNotes.DefaultCellStyle = DataGridViewCellStyle100
        Me.colEXTRefNotes.HeaderText = "Notes"
        Me.colEXTRefNotes.Name = "colEXTRefNotes"
        Me.colEXTRefNotes.ReadOnly = True
        Me.colEXTRefNotes.Width = 200
        '
        'colEXTRefRecordDate
        '
        Me.colEXTRefRecordDate.DataPropertyName = "RecordDate"
        DataGridViewCellStyle101.BackColor = System.Drawing.SystemColors.Info
        Me.colEXTRefRecordDate.DefaultCellStyle = DataGridViewCellStyle101
        Me.colEXTRefRecordDate.HeaderText = "Record Date"
        Me.colEXTRefRecordDate.Name = "colEXTRefRecordDate"
        Me.colEXTRefRecordDate.ReadOnly = True
        '
        'colEXTRefRecordTime
        '
        Me.colEXTRefRecordTime.DataPropertyName = "RecordTime"
        DataGridViewCellStyle102.BackColor = System.Drawing.SystemColors.Info
        Me.colEXTRefRecordTime.DefaultCellStyle = DataGridViewCellStyle102
        Me.colEXTRefRecordTime.HeaderText = "Record Time"
        Me.colEXTRefRecordTime.Name = "colEXTRefRecordTime"
        Me.colEXTRefRecordTime.ReadOnly = True
        '
        'colTranNo
        '
        Me.colTranNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colTranNo.DataPropertyName = "TranNo"
        DataGridViewCellStyle107.BackColor = System.Drawing.SystemColors.Info
        Me.colTranNo.DefaultCellStyle = DataGridViewCellStyle107
        Me.colTranNo.HeaderText = "Tran No"
        Me.colTranNo.Name = "colTranNo"
        Me.colTranNo.ReadOnly = True
        '
        'colTranDate
        '
        Me.colTranDate.DataPropertyName = "TranDate"
        DataGridViewCellStyle108.BackColor = System.Drawing.SystemColors.Info
        Me.colTranDate.DefaultCellStyle = DataGridViewCellStyle108
        Me.colTranDate.HeaderText = "Tran Date"
        Me.colTranDate.Name = "colTranDate"
        Me.colTranDate.ReadOnly = True
        '
        'collAccAmount
        '
        Me.collAccAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.collAccAmount.DataPropertyName = "Amount"
        DataGridViewCellStyle109.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle109.BackColor = System.Drawing.SystemColors.Info
        Me.collAccAmount.DefaultCellStyle = DataGridViewCellStyle109
        Me.collAccAmount.HeaderText = "Amount"
        Me.collAccAmount.Name = "collAccAmount"
        Me.collAccAmount.ReadOnly = True
        '
        'colCredit
        '
        Me.colCredit.DataPropertyName = "Credit"
        DataGridViewCellStyle110.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle110.BackColor = System.Drawing.SystemColors.Info
        Me.colCredit.DefaultCellStyle = DataGridViewCellStyle110
        Me.colCredit.HeaderText = "Credit"
        Me.colCredit.Name = "colCredit"
        Me.colCredit.ReadOnly = True
        '
        'colDebit
        '
        Me.colDebit.DataPropertyName = "Debit"
        DataGridViewCellStyle111.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle111.BackColor = System.Drawing.SystemColors.Info
        Me.colDebit.DefaultCellStyle = DataGridViewCellStyle111
        Me.colDebit.HeaderText = "Debit"
        Me.colDebit.Name = "colDebit"
        Me.colDebit.ReadOnly = True
        '
        'colBalance
        '
        Me.colBalance.DataPropertyName = "Balance"
        DataGridViewCellStyle112.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle112.BackColor = System.Drawing.SystemColors.Info
        Me.colBalance.DefaultCellStyle = DataGridViewCellStyle112
        Me.colBalance.HeaderText = "Balance"
        Me.colBalance.Name = "colBalance"
        Me.colBalance.ReadOnly = True
        '
        'colAccNotes
        '
        Me.colAccNotes.DataPropertyName = "Notes"
        DataGridViewCellStyle113.BackColor = System.Drawing.SystemColors.Info
        Me.colAccNotes.DefaultCellStyle = DataGridViewCellStyle113
        Me.colAccNotes.HeaderText = "Notes"
        Me.colAccNotes.Name = "colAccNotes"
        Me.colAccNotes.ReadOnly = True
        Me.colAccNotes.Width = 150
        '
        'colAccRecordDate
        '
        Me.colAccRecordDate.DataPropertyName = "RecordDate"
        DataGridViewCellStyle114.BackColor = System.Drawing.SystemColors.Info
        Me.colAccRecordDate.DefaultCellStyle = DataGridViewCellStyle114
        Me.colAccRecordDate.HeaderText = "Record Date"
        Me.colAccRecordDate.Name = "colAccRecordDate"
        Me.colAccRecordDate.ReadOnly = True
        Me.colAccRecordDate.Width = 80
        '
        'colAccTime
        '
        Me.colAccTime.DataPropertyName = "RecordTime"
        DataGridViewCellStyle115.BackColor = System.Drawing.SystemColors.Info
        Me.colAccTime.DefaultCellStyle = DataGridViewCellStyle115
        Me.colAccTime.HeaderText = "Time"
        Me.colAccTime.Name = "colAccTime"
        Me.colAccTime.ReadOnly = True
        Me.colAccTime.Width = 80
        '
        'frmDetailedAccountStatement
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(1058, 504)
        Me.Controls.Add(Me.stbOutStandingBalance)
        Me.Controls.Add(Me.btnShowOutStandingBalance)
        Me.Controls.Add(Me.stbTotalPaidAmount)
        Me.Controls.Add(Me.lblTotalPaidAmount)
        Me.Controls.Add(Me.stbTotalBill)
        Me.Controls.Add(Me.lblTotalBillAmount)
        Me.Controls.Add(Me.stbMainMemberName)
        Me.Controls.Add(Me.lblMainMemberName)
        Me.Controls.Add(Me.dtpEndDateTime)
        Me.Controls.Add(Me.lblStartDateTime)
        Me.Controls.Add(Me.dtpStartDateTime)
        Me.Controls.Add(Me.lblEndDateTime)
        Me.Controls.Add(Me.stbCompanyName)
        Me.Controls.Add(Me.lblCompanyName)
        Me.Controls.Add(Me.stbAccountName)
        Me.Controls.Add(Me.lblAccountName)
        Me.Controls.Add(Me.lblMainMemberNo)
        Me.Controls.Add(Me.stbMainMemberNo)
        Me.Controls.Add(Me.cboCompanyNo)
        Me.Controls.Add(Me.lblCompanyNo)
        Me.Controls.Add(Me.fbnExport)
        Me.Controls.Add(Me.lblRecordsNo)
        Me.Controls.Add(Me.stbBalanceWords)
        Me.Controls.Add(Me.lblBalanceWords)
        Me.Controls.Add(Me.stbBalance)
        Me.Controls.Add(Me.lblBalance)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.cboAccountNo)
        Me.Controls.Add(Me.lblAccountNo)
        Me.Controls.Add(Me.btnPrintPreview)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.fbnLoad)
        Me.Controls.Add(Me.cboBillMode)
        Me.Controls.Add(Me.tbcAccountStatement)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.lblBillMode)
        Me.Controls.Add(Me.nbxAccountBalance)
        Me.Controls.Add(Me.lblAccountBalance)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmDetailedAccountStatement"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Detailed Account Statement"
        Me.tbcAccountStatement.ResumeLayout(False)
        Me.tpgInvoices.ResumeLayout(False)
        Me.tpgInvoices.PerformLayout()
        CType(Me.dgvInvoices, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsEdit.ResumeLayout(False)
        Me.tpgInvoiceAdjustments.ResumeLayout(False)
        Me.tpgInvoiceAdjustments.PerformLayout()
        CType(Me.dgvInvoiceAdjustments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgExtraBills.ResumeLayout(False)
        Me.tpgExtraBills.PerformLayout()
        CType(Me.dgvExtraBillItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgOPDPayments.ResumeLayout(False)
        Me.tpgOPDPayments.PerformLayout()
        CType(Me.dgvCashReceipts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgBillFormPayments.ResumeLayout(False)
        Me.tpgBillFormPayments.PerformLayout()
        CType(Me.dgvBillFormPayments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgOPDRefunds.ResumeLayout(False)
        Me.tpgOPDRefunds.PerformLayout()
        CType(Me.dgvRefunds, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgExtraBillRefunds.ResumeLayout(False)
        Me.tpgExtraBillRefunds.PerformLayout()
        CType(Me.dgvExtraBillRefunds, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgAccountTransactions.ResumeLayout(False)
        Me.tpgAccountTransactions.PerformLayout()
        CType(Me.dgvAccountTrasaction, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents lblBillMode As System.Windows.Forms.Label
    Friend WithEvents nbxAccountBalance As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblAccountBalance As System.Windows.Forms.Label
    Friend WithEvents tbcAccountStatement As TabControl
    Friend WithEvents tpgOPDPayments As TabPage
    Friend WithEvents dgvCashReceipts As DataGridView
    Friend WithEvents tpgAccountTransactions As TabPage
    Friend WithEvents stbPatientChequePaymentsWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblExpenditureTotalAmount As Label
    Friend WithEvents lblExpenditureAmountWords As Label
    Friend WithEvents stbPatientChequePayments As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents dgvAccountTrasaction As DataGridView
    Friend WithEvents tpgOPDRefunds As TabPage
    Friend WithEvents dgvRefunds As DataGridView
    Friend WithEvents cboBillMode As ComboBox
    Friend WithEvents fbnLoad As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents btnPrintPreview As Button
    Friend WithEvents btnPrint As Button
    Friend WithEvents cboAccountNo As ComboBox
    Friend WithEvents lblAccountNo As Label
    Friend WithEvents btnLoad As Button
    Friend WithEvents tpgInvoices As System.Windows.Forms.TabPage
    Friend WithEvents dgvInvoices As System.Windows.Forms.DataGridView
    Friend WithEvents tpgExtraBills As System.Windows.Forms.TabPage
    Friend WithEvents dgvExtraBillItems As System.Windows.Forms.DataGridView
    Friend WithEvents stbInvoiceAmount As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblInvoiceAmount As System.Windows.Forms.Label
    Friend WithEvents stbExtraBillAmount As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblExtraBillAmount As System.Windows.Forms.Label
    Friend WithEvents stbTotalPayments As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblTotalPayments As System.Windows.Forms.Label
    Friend WithEvents stbTotalRefunds As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblTotalRefunds As System.Windows.Forms.Label
    Friend WithEvents stbNetAmount As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblNetAmount As System.Windows.Forms.Label
    Friend WithEvents stbTotalCredit As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblTotalCredit As System.Windows.Forms.Label
    Friend WithEvents stbTotalDebit As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblTotalDebit As System.Windows.Forms.Label
    Friend WithEvents stbBalance As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents stbNetAmountWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblNetAmountWords As System.Windows.Forms.Label
    Friend WithEvents stbInvoiceAmountWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblInvoiceAmountWords As System.Windows.Forms.Label
    Friend WithEvents stbExtraBillAmountlWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblExtraBillAmountWords As System.Windows.Forms.Label
    Friend WithEvents stbTotalPaymentWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblPaymentWords As System.Windows.Forms.Label
    Friend WithEvents stbTotalRefundWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblRefundWords As System.Windows.Forms.Label
    Friend WithEvents stbBalanceWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblBalanceWords As System.Windows.Forms.Label
    Friend WithEvents tpgInvoiceAdjustments As System.Windows.Forms.TabPage
    Friend WithEvents dgvInvoiceAdjustments As System.Windows.Forms.DataGridView
    Friend WithEvents stbINVAAmountWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblINVAAmountWords As System.Windows.Forms.Label
    Friend WithEvents stbInvoiceAdjustmentAmount As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblINVAAMouny As System.Windows.Forms.Label
    Friend WithEvents fbnExport As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents lblRecordsNo As System.Windows.Forms.Label
    Friend WithEvents cboCompanyNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompanyNo As System.Windows.Forms.Label
    Friend WithEvents lblMainMemberNo As System.Windows.Forms.Label
    Friend WithEvents stbMainMemberNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents stbAccountName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAccountName As System.Windows.Forms.Label
    Friend WithEvents stbCompanyName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblCompanyName As System.Windows.Forms.Label
    Friend WithEvents tpgBillFormPayments As System.Windows.Forms.TabPage
    Friend WithEvents stbBillFormAmountWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblBillFormAmountWord As System.Windows.Forms.Label
    Friend WithEvents stbBillFormAmount As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblBillFormAmount As System.Windows.Forms.Label
    Friend WithEvents dgvBillFormPayments As System.Windows.Forms.DataGridView
    Friend WithEvents dtpEndDateTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDateTime As System.Windows.Forms.Label
    Friend WithEvents dtpStartDateTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEndDateTime As System.Windows.Forms.Label
    Friend WithEvents tpgExtraBillRefunds As System.Windows.Forms.TabPage
    Friend WithEvents stbExtraBillRefundWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblExtraBillRefundAmpont As System.Windows.Forms.Label
    Friend WithEvents stbExtraBillRefunds As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblExtraBillRefunds As System.Windows.Forms.Label
    Friend WithEvents dgvExtraBillRefunds As System.Windows.Forms.DataGridView
    Friend WithEvents cmsEdit As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmsEditCopy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmsEditSelectAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents stbMainMemberName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblMainMemberName As System.Windows.Forms.Label
    Friend WithEvents stbTotalBill As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblTotalBillAmount As System.Windows.Forms.Label
    Friend WithEvents stbTotalPaidAmount As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblTotalPaidAmount As System.Windows.Forms.Label
    Friend WithEvents btnShowOutStandingBalance As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents stbOutStandingBalance As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents colInvoicePatientNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInvFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInvVisitNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInvInvoiceNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInvBillNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInvInsuranceNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInvoiceAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInvoiceDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIVAPatientNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInvAFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIVAVisitNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIVAInvoiceNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInvABillNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInvAInsuranceNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIVAInvoiceDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAdjustmentNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAdjustmentDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIVAAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFPatientNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFBillNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFInsuranceNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFVisitNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraBillNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colVisitType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraBillDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPatientNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPayVisitNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPayReceiptNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPayInvoiceNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBillNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPayNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPayDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPayAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPayNotes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPayRecordDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPayRecordTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFPPatientNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFPFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFPVisitNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFPReceiptNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFPExtraBillNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFPBillNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFPInsuranceNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFPPayNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFPPayDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFPAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFPNotes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFPRecordDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBFPRecordTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRefPatientNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRefFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRefReceiptNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRefRefundNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRefInvoiceNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRefBillNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRefInsuranceNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRefundDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRefAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRefNotes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRefundRecordDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRefTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtRefPatientNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEXTRefFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEXTRefReceiptNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEXTRefExtraBillNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEXTRefRefundNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEXTRefBillNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEXTRefInsuranceNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEXTRefRefundDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEXTRefAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEXTRefNotes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEXTRefRecordDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEXTRefRecordTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTranNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTranDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents collAccAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCredit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDebit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBalance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAccNotes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAccRecordDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAccTime As System.Windows.Forms.DataGridViewTextBoxColumn
End Class