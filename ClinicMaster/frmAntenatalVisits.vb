
Option Strict On

Imports SyncSoft.SQLDb
Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Common.Structures
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.SQL.Classes
Imports SyncSoft.Common.SQL.Enumerations
Imports SyncSoft.Common.Win.Forms.CrossMatch
Imports SyncSoft.Common.Win.Forms.DigitalPersona
Imports SyncSoft.Common.Utilities.Fingerprint.CrossMatch
Imports SyncSoft.Common.Utilities.Fingerprint.DigitalPersona
Imports LookupDataID = SyncSoft.SQLDb.Lookup.LookupDataID
Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects
Imports LookupCommDataID = SyncSoft.Common.Lookup.LookupCommDataID
Imports System.Drawing.Printing
Imports System.Collections.Generic
Imports SyncSoft.Common.Win.Controls

Public Class frmAntenatalVisits

#Region " Fields "
    Private allergies As DataTable
    Private patientAllergies As DataTable
    Private antenatalVisitsSaved As Boolean = True
    Private pelvicExaminationSaved As Boolean = True
    Private oCurrentVisit As CurrentVisit
    Private oExaminerSpecialityID As New LookupDataID.StaffTitleID

    Private oCrossMatchTemplate As New CrossMatchFingerTemplate()
    Private oDigitalPersonaTemplate As New DigitalPersonaFingerTemplate()
#End Region

#Region " Properties "

    Private ReadOnly Property NewTriageTest() As String
        Get
            Return "Register Triage..."
        End Get
    End Property

    Private ReadOnly Property EditTriageTest() As String
        Get
            Return "Edit Triage..."
        End Get
    End Property

    Private ReadOnly Property ScheduleAppointment() As String
        Get
            Return "Schedule Appointment"
        End Get
    End Property

    Private ReadOnly Property TodaysAppointment() As String
        Get
            Return "Today's Appointments"
        End Get
    End Property

    Private ReadOnly Property ANCVisitClosed() As String
        Get
            Return "Visit Closed"
        End Get
    End Property

    Private ReadOnly Property ANCCloseVisit() As String
        Get
            Return "Close Visit"
        End Get
    End Property

#End Region

    Private Sub frmAntenatalVisits_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.Cursor = Cursors.WaitCursor()

            LoadLookupDataCombo(Me.cboPallorID, LookupObjects.Pallor, False)
            LoadLookupDataCombo(Me.cboJaundiceID, LookupObjects.YesNo, False)
            LoadLookupDataCombo(Me.cboLynphadenopathyID, LookupObjects.YesNo, False)
            LoadLookupDataCombo(Me.cboVaricoseID, LookupObjects.Varicose, False)
            LoadLookupDataCombo(Me.cboOedemaID, LookupObjects.Oedema, False)
            LoadLookupDataCombo(Me.cboHeartSoundID, LookupObjects.Normality, False)
            LoadLookupDataCombo(Me.cboAirEntryID, LookupObjects.Normality, False)
            LoadLookupDataCombo(Me.cboBreastID, LookupObjects.Normality, False)
            LoadLookupDataCombo(Me.cboLiverID, LookupObjects.Normality, False)
            LoadLookupDataCombo(Me.cboSpleenID, LookupObjects.Normality, False)
            LoadLookupDataCombo(Me.cboBowelSoundsID, LookupObjects.Normality, False)
            LoadLookupDataCombo(Me.cboScarID, LookupObjects.YesNo, False)
            LoadLookupDataCombo(Me.cboPupilReactionID, LookupObjects.Normality, False)
            LoadLookupDataCombo(Me.cboReflexesID, LookupObjects.Normality, False)
            LoadLookupDataCombo(Me.cboOtherSTIID, LookupObjects.YesNo, False)
            LoadLookupDataCombo(Me.cboSkeletalDeformityID, LookupObjects.YesNo, False)
            LoadLookupDataCombo(Me.cboVulvaID, LookupObjects.Normality, False)
            LoadLookupDataCombo(Me.cboCervixID, LookupObjects.Normality, False)
            LoadLookupDataCombo(Me.cboAdnexaID, LookupObjects.Normality, False)
            LoadLookupDataCombo(Me.cboVaginaID, LookupObjects.Normality, False)
            LoadLookupDataCombo(Me.cboUterusID, LookupObjects.Normality, False)
            LoadLookupDataCombo(Me.cboConclusionID, LookupObjects.PelvicConclusion, False)
            LoadLookupDataCombo(Me.cboLieID, LookupObjects.Lie, False)
            LoadLookupDataCombo(Me.cboPositionID, LookupObjects.Position, False)
            LoadLookupDataCombo(Me.cboDoctorSpecialtyID, LookupObjects.DoctorSpecialty, False)
            LoadLookupDataCombo(Me.cboIPT, LookupObjects.YesNoUnknown, False)
            LoadLookupDataCombo(Me.cboNetUse, LookupObjects.YesNoUnknown, False)
            LoadLookupDataCombo(Me.cboPresentationID, LookupObjects.Presentation, False)

            Me.LoadNurse()
            Me.btnFindVisitNo.Visible = False
            Me.stbANCNo.Enabled = False
            Me.btnClear.Visible = False


        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub frmAntenatalVisits_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub

    Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub

    Private Sub fbnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnDelete.Click

        Dim oAntenatalVisits As New SyncSoft.SQLDb.AntenatalVisits()

        Try
            Me.Cursor = Cursors.WaitCursor()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If DeleteMessage() = Windows.Forms.DialogResult.No Then Return

            oAntenatalVisits.VisitNo = StringEnteredIn(Me.stbVisitNo, "Visit No!")

            DisplayMessage(oAntenatalVisits.Delete())
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            ResetControls()
            Me.CallOnKeyEdit()
            Me.navVisits.Clear()
            Me.chkNavigateVisits.Checked = False

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub fbnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim visitNo As String

        Dim oAntenatalVisits As New SyncSoft.SQLDb.AntenatalVisits()

        Try
            Me.Cursor = Cursors.WaitCursor()

            visitNo = RevertText(StringEnteredIn(Me.stbVisitNo, "Visit No!"))

            Dim dataSource As DataTable = oAntenatalVisits.GetAntenatalVisits(visitNo).Tables("AntenatalVisits")
            Me.DisplayData(dataSource)

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub ebnSaveUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ebnSaveUpdate.Click
        Try
            Dim transactions As New List(Of TransactionList(Of DBConnect))
            Dim records As Integer

            Me.Cursor = Cursors.WaitCursor()

            Select Case Me.ebnSaveUpdate.ButtonText

                Case ButtonCaption.Save

                    transactions.Add(New TransactionList(Of DBConnect)(AntenatalVisitList, Action.Save))
                    If Not IsNothing(PelvicExaminationList) Then
                        transactions.Add(New TransactionList(Of DBConnect)(PelvicExaminationList, Action.Save))
                    End If

                    ' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    records = DoTransactions(transactions)

                    DisplayMessage(records.ToString() + " record(s) Saved!")

                    ' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ResetControls()
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Case ButtonCaption.Update
                    transactions.Add(New TransactionList(Of DBConnect)(AntenatalVisitList, Action.Update, "AntenatalVisits"))

                    If Not IsNothing(PelvicExaminationList) Then
                        transactions.Add(New TransactionList(Of DBConnect)(PelvicExaminationList, Action.Update, "PelvicExamination"))
                    End If

                    records = DoTransactions(transactions)

                    DisplayMessage(records.ToString() + " record(s) updated!")

                    btnClear.Visible = True

            End Select

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()
        End Try

    End Sub

#Region " Edit Methods "

    Public Sub Edit()

        Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update
        Me.ebnSaveUpdate.Enabled = False

        Me.btnLoadSeeANCVisits.Enabled = False
        Me.btnFindVisitNo.Visible = True
        Me.fbnDelete.Visible = True
        Me.fbnDelete.Enabled = False
        Me.pnlNavigateANCVisits.Visible = True

        Me.dtpReturnDate.MinDate = AppData.MinimumDate

        ResetControlsIn(Me)

    End Sub

    Public Sub Save()

        Me.ebnSaveUpdate.ButtonText = ButtonCaption.Save
        Me.ebnSaveUpdate.Enabled = True

        Me.dtpReturnDate.MinDate = Today

        Me.fbnDelete.Visible = False
        Me.btnClear.Visible = False

        Me.btnLoadSeeANCVisits.Enabled = True
        Me.btnFindVisitNo.Visible = False

        Me.pnlNavigateANCVisits.Visible = False

        ResetControlsIn(Me)

    End Sub

    Private Sub DisplayData(ByVal dataSource As DataTable)

        Try

            Me.ebnSaveUpdate.DataSource = dataSource
            Me.ebnSaveUpdate.LoadData(Me)

            Me.ebnSaveUpdate.Enabled = dataSource.Rows.Count > 0
            Me.fbnDelete.Enabled = dataSource.Rows.Count > 0

            Security.Apply(Me.ebnSaveUpdate, AccessRights.Update)
            Security.Apply(Me.fbnDelete, AccessRights.Delete)

        Catch ex As Exception
            DisplayMessage(ex.Message)
        End Try

    End Sub

    Private Sub CallOnKeyEdit()
        If Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update Then
            Me.ebnSaveUpdate.Enabled = False
            Me.fbnDelete.Enabled = False
        End If
    End Sub

#End Region

#Region "LOAD OPTIONS"


    Private Sub btnLoadSeeANCVisits_Click_1(sender As System.Object, e As System.EventArgs) Handles btnLoadSeeANCVisits.Click
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim fPeriodicAntenatalEnrollments As New frmPeriodicAntenatalVisits(Me.stbVisitNo, Me.stbANCNo)
        fPeriodicAntenatalEnrollments.ShowDialog(Me)
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim _ANCNo As String = RevertText(StringMayBeEnteredIn(Me.stbANCNo))
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If String.IsNullOrEmpty(_ANCNo) Then Return

        Me.ShowPatientDetails(_ANCNo)

    End Sub

    Private Sub LoadAntenatalVisits(ByVal visitNo As String)
        Dim oAntenatalVisits As New SyncSoft.SQLDb.AntenatalVisits()

        Try

            Me.Cursor = Cursors.WaitCursor


            Dim antenatalVisits As DataTable = oAntenatalVisits.GetAntenatalVisits(visitNo).Tables("AntenatalVisits")

            Dim row4 As DataRow = antenatalVisits.Rows(0)
            If antenatalVisits Is Nothing OrElse antenatalVisits.Rows.Count < 1 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim ancNo As String = StringEnteredIn(row4, "ANCNo")
            stbANCNo.Text = ancNo
            ShowPatientDetails(ancNo)
            Me.cboPallorID.Text = StringEnteredIn(row4, "Pallor")
            Me.cboJaundiceID.Text = StringEnteredIn(row4, "Jaundice")
            Me.cboLynphadenopathyID.Text = StringEnteredIn(row4, "Lynphadenopathy")
            Me.cboVaricoseID.Text = StringEnteredIn(row4, "Varicose")
            Me.cboOedemaID.Text = StringEnteredIn(row4, "Oedema")
            Me.cboHeartSoundID.Text = StringEnteredIn(row4, "HeartSound")
            Me.cboAirEntryID.Text = StringEnteredIn(row4, "AirEntry")
            Me.cboBreastID.Text = StringEnteredIn(row4, "Breast")
            Me.cboOtherSTIID.Text = StringEnteredIn(row4, "OtherSTIs")
            Me.stbSTIDetails.Text = StringMayBeEnteredIn(row4, "STIDetails")
            Me.cboLiverID.Text = StringEnteredIn(row4, "Liver")
            Me.cboSpleenID.Text = StringEnteredIn(row4, "Spleen")
            Me.cboBowelSoundsID.Text = StringEnteredIn(row4, "BowelSounds")
            Me.cboScarID.Text = StringEnteredIn(row4, "Scar")
            Me.cboPupilReactionID.Text = StringEnteredIn(row4, "PupilReaction")
            Me.cboReflexesID.Text = StringEnteredIn(row4, "Reflexes")
            Me.cboSkeletalDeformityID.Text = StringEnteredIn(row4, "SkeletalDeformity")
            Me.nbxAnenorrheaWeeks.Value = StringMayBeEnteredIn(row4, "AnenorrheaWeeks")
            Me.stbFundalHeight.Text = StringMayBeEnteredIn(row4, "FundalHeight")
            Me.cboPresentationID.Text = StringEnteredIn(row4, "Presentation")
            Me.cboLieID.Text = StringEnteredIn(row4, "Lie")
            Me.cboPositionID.Text = StringEnteredIn(row4, "Position")
            Me.stbRelationPPOrBrim.Text = StringEnteredIn(row4, "RelationPPOrBrim")
            Me.nbxFoetalHeart.Value = StringMayBeEnteredIn(row4, "FoetalHeart")
            Me.chkTTGiven.Checked = BooleanMayBeEnteredIn(row4, "TTGiven")
            Me.cboIPT.Text = StringEnteredIn(row4, "IPT")
            Me.cboNetUse.Text = StringEnteredIn(row4, "NetUse")
            Me.stbRemarks.Text = StringMayBeEnteredIn(row4, "Remarks")
            Me.dtpReturnDate.Value = DateMayBeEnteredIn(row4, "ReturnDate")
            Me.dtpReturnDate.Checked = True

            Me.cboDoctorSpecialtyID.Text = StringEnteredIn(row4, "DoctorSpeciality")

            Me.cboDoctorID.Text = StringEnteredIn(row4, "Doctor")
            Me.cboNurseID.Text = StringEnteredIn(row4, "NurseInCharge")

        Catch ex As Exception
            DisplayMessage(ex.Message)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub LoadPelvicExamination(ByVal visitNo As String)
        Dim oPelvicExamination As New SyncSoft.SQLDb.PelvicExamination()

        Try

            Me.Cursor = Cursors.WaitCursor


            Dim pelvicExamination As DataTable = oPelvicExamination.GetPelvicExamination(visitNo).Tables("PelvicExamination")

            If pelvicExamination Is Nothing OrElse pelvicExamination.Rows.Count < 1 Then Return
            Dim row4 As DataRow = pelvicExamination.Rows(0)

            ' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.cboVulvaID.Text = StringMayBeEnteredIn(row4, "Vulva")
            Me.cboCervixID.Text = StringMayBeEnteredIn(row4, "Cervix")
            Me.cboAdnexaID.Text = StringMayBeEnteredIn(row4, "Adnexa")
            Me.cboVaginaID.Text = StringMayBeEnteredIn(row4, "Vagina")
            Me.cboUterusID.Text = StringMayBeEnteredIn(row4, "Uterus")
            Me.nbxDiagonalConjugate.Value = StringMayBeEnteredIn(row4, "DiagonalConjugate")
            Me.nbxSacralCurve.Value = StringMayBeEnteredIn(row4, "SacralCurve")
            Me.nbxIschialSpine.Value = StringMayBeEnteredIn(row4, "IschialSpine")
            Me.nbxSubPublicAngle.Value = StringMayBeEnteredIn(row4, "SubPublicAngle")
            Me.nbxIschialTuberosities.Value = StringMayBeEnteredIn(row4, "IschialTuberosities")
            Me.cboConclusionID.Text = StringMayBeEnteredIn(row4, "Conclusion")
            Me.stbRiskFactors.Text = StringMayBeEnteredIn(row4, "RiskFactors")
            Me.stbRecommendations.Text = StringMayBeEnteredIn(row4, "Recommendations")

        Catch ex As Exception
            DisplayMessage(ex.Message)

        End Try

    End Sub
    Private Sub ShowPatientDetails(ByVal _ANCNo As String)
        Dim oVariousOptions As New VariousOptions()
        Dim opatientallergies As New SyncSoft.SQLDb.PatientAllergies()
        Dim oAntenatalEnrollment As New SyncSoft.SQLDb.AntenatalEnrollment()
        Dim message As String = String.Empty

        If String.IsNullOrEmpty(stbVisitNo.Text) Then Return

        Dim _VisitNo = RevertText(StringEnteredIn(Me.stbVisitNo, "Visit No!"))

        Dim antenatalEnrollments As DataTable = oAntenatalEnrollment.GetAntenatalEnrollment(_ANCNo, _VisitNo).Tables("AntenatalEnrollment")

        Dim row As DataRow = antenatalEnrollments.Rows(0)

        Dim oItems As New SyncSoft.SQLDb.Items()
        Dim patientNo As String = RevertText(StringEnteredIn(row, "PatientNo"))
        Dim outstandingBalanceErrorMSG As String = "This patient has offered/done service(s) with pending payment. " +
                                                    ControlChars.NewLine + "Please advice accordingly!"
        Dim patientAllergyMsg As String = " This Patient has Allergy(s)" + ControlChars.NewLine + "Please take note"

        Try

            Dim allergyList As DataTable = opatientallergies.GetPatientAllergies(patientNo).Tables("PatientAllergies")

            Me.LoadPatientAllergies(patientNo)

            '#################### PATIENT DETAILS################################'

            Me.stbPatientNo.Text = FormatText(patientNo, "Patients", "PatientNo")
            Me.stbFullName.Text = StringEnteredIn(row, "FullName")
            Me.stbPhone.Text = StringMayBeEnteredIn(row, "Phone")
            Me.stbBloodGroup.Text = StringMayBeEnteredIn(row, "BloodGroup")
            Me.stbANCJoinDate.Text = FormatDate(DateEnteredIn(row, "JoinDate"))
            Dim birthDate As Date = DateMayBeEnteredIn(row, "BirthDate")
            Me.stbTotalVisits.Text = StringEnteredIn(row, "TotalVisits")
            Me.stbAddress.Text = StringMayBeEnteredIn(row, "Address")
            Me.spbPhoto.Image = ImageMayBeEnteredIn(row, "Photo")
            Me.txtVisitDate.Text = FormatDate(DateEnteredIn(row, "lastVisitDate"))
            Me.stbTotalVisits.Text = StringEnteredIn(row, "TotalVisits")
            Dim visitDate As Date = DateEnteredIn(row, "VisitDate")
            Me.btnTriage.Enabled = True

            '######################### Antenatal ENROLLMENT#############################################

            Me.stbEDD.Text = FormatDate(DateMayBeEnteredIn(row, "EDD"))
            Me.stbHIVStatus.Text = StringEnteredIn(row, "HIVStatus")
            Me.stbPartnersHIVStatus.Text = StringEnteredIn(row, "PartnersHIVStatus")

            ''################################## TRIAGE DETAILS #####################################

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim hasTriage As Boolean = BooleanMayBeEnteredIn(row, "HasTriage")
            If Not hasTriage Then
                Me.btnTriage.Text = Me.NewTriageTest
                Security.Apply(Me.btnTriage, AccessRights.Write)
            Else
                Me.btnTriage.Text = Me.EditTriageTest
                Security.Apply(Me.btnTriage, AccessRights.Edit)
            End If
            Me.nbxWeight.Value = StringMayBeEnteredIn(row, "Weight")
            Me.nbxTemperature.Value = StringMayBeEnteredIn(row, "Temperature")
            Me.nbxHeight.Value = StringMayBeEnteredIn(row, "Height")
            Me.stbBloodPressure.Text = StringMayBeEnteredIn(row, "BloodPressure")
            Me.nbxRespirationRate.Value = StringMayBeEnteredIn(row, "RespirationRate")
            Me.nbxOxygenSaturation.Value = StringMayBeEnteredIn(row, "OxygenSaturation")
            Me.nbxHeartRate.Value = StringMayBeEnteredIn(row, "HeartRate")
            Me.nbxBMI.Value = StringMayBeEnteredIn(row, "BMI")
            Me.stbNotes.Text = StringMayBeEnteredIn(row, "Notes")
            Me.nbxMUAC.Value = StringMayBeEnteredIn(row, "MUAC")
            Me.stbBMIStatus.Text = StringMayBeEnteredIn(row, "BMIStatus")

        Catch ex As Exception
            DisplayMessage(ex.Message)
        End Try


    End Sub

    Private Sub LoadStaff(ByVal doctorSpecialtyID As String)

        Dim oStaff As New SyncSoft.SQLDb.Staff()
        Dim oStaffTitleID As New LookupDataID.StaffTitleID()
        Dim oVariousOptions As New VariousOptions()

        Try
            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''
            Me.cboDoctorID.Items.Clear()
            '''''''''''''''''''''''''''''''''''''''''''

            ' Load all from Staff
            Dim staff As DataTable = oStaff.GetStaffByDoctorSpecialty(oStaffTitleID.Doctor, doctorSpecialtyID).Tables("Staff")

            If oVariousOptions.RestrictSelectionOfOnlyLoggedInDoctors Then
                staff = oStaff.GetLoggedInStaffByDoctorSpecialty(oStaffTitleID.Doctor, doctorSpecialtyID).Tables("Staff")
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadComboData(Me.cboDoctorID, staff, "StaffFullName")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub LoadNurse()

        Dim oStaff As New SyncSoft.SQLDb.Staff()
        Dim oStaffTitleID As New LookupDataID.StaffTitleID()

        Try
            Me.Cursor = Cursors.WaitCursor

            ' Load from Staff
            Dim staff As DataTable = oStaff.GetStaffByStaffTitle(oStaffTitleID.Nurse).Tables("Staff")

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadComboData(Me.cboNurseID, staff, "StaffFullName")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub LoadDoctorSpecialty(ByVal IsDoctor As Boolean)

        Try

            Me.Cursor = Cursors.WaitCursor

            If Not IsDoctor Then
                Me.cboDoctorSpecialtyID.DataSource = Nothing
                Me.cboDoctorSpecialtyID.SelectedIndex = -1
                Me.cboDoctorSpecialtyID.SelectedIndex = -1
            Else : LoadLookupDataCombo(Me.cboDoctorSpecialtyID, LookupObjects.DoctorSpecialty, False)
            End If

            Me.cboDoctorSpecialtyID.Enabled = IsDoctor

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub
#End Region

#Region "Allergies -Grid"

    Private Sub LoadAllergies()

        Dim oAllergies As New SyncSoft.SQLDb.Allergies()

        Try

            Me.Cursor = Cursors.WaitCursor

            ' Load from allergies
            allergies = oAllergies.GetAllergies().Tables("Allergies")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Me.colAllergyNo.Sorted = False
            'LoadComboData(Me.colAllergyNo, allergies, "AllergyNo", "AllergyName")
            ' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


    Private Sub ShowAllergyNo(ByVal allergyNo As String, ByVal pos As Integer)
        Dim oAllergies As New SyncSoft.SQLDb.Allergies()
        Try
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If patientAllergies Is Nothing OrElse String.IsNullOrEmpty(allergyNo) Then Return

            For Each row As DataRow In patientAllergies.Select("AllergyNo = '" + allergyNo + "'")
                ' Me.dgvPatientAllergies.Item(Me.colAllergyNo.Name, pos).Value = StringMayBeEnteredIn(row, "AllergyNo")
            Next

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            DisplayMessage(ex.Message)

        End Try

    End Sub


    Private Sub LoadPatientAllergies(ByVal patientNo As String)

        Dim oPatientAllergies As New SyncSoft.SQLDb.PatientAllergies()

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim patientAllergies As DataTable = oPatientAllergies.GetPatientAllergies(patientNo).Tables("PatientAllergies")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If patientAllergies Is Nothing OrElse patientAllergies.Rows.Count < 1 Then Return

            For Each row As DataRow In patientAllergies.Rows


                LoadGridData(Me.dgvPatientAllergies, patientAllergies)
            Next




            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub btnAddAllergy_Click(sender As System.Object, e As System.EventArgs) Handles btnAddAllergy.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim oVariousOptions As New VariousOptions()
            Dim patientNo As String = RevertText(StringEnteredIn(Me.stbPatientNo, " A Visit No, to Continue!"))
            If Not oVariousOptions.EnableSecondPatientForm Then
                Dim fPatientAllergies As New frmPatients(patientNo, True)
                fPatientAllergies.tbcPatients.SelectTab(fPatientAllergies.tpgPatientAllergies.Name)
                fPatientAllergies.Edit()
                fPatientAllergies.ShowDialog()
            Else
                Dim fPatientAllergies As New frmPatientsTwo(patientNo, True)
                fPatientAllergies.tbcPatients.SelectTab(fPatientAllergies.tpgPatientAllergies.Name)
                fPatientAllergies.Edit()
                fPatientAllergies.ShowDialog()
            End If


        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

#End Region

#Region " Fingerprint  "

    Private Sub btnFindByFingerprint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFindByFingerprint.Click

        Dim oVariousOptions As New VariousOptions()
        Dim oFingerprintDeviceID As New LookupCommDataID.FingerprintDeviceID()

        Try

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Not Me.AllSaved() Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim visitFingerprints As DataTable = GetVisitFingerprints()

            If oVariousOptions.FingerprintDevice.ToUpper().Equals(oFingerprintDeviceID.CrossMatch.ToUpper()) Then

                Dim fFingerprintCapture As New FingerprintCapture(CaptureType.Verify, visitFingerprints, "VisitNo")
                fFingerprintCapture.ShowDialog()

                Dim visitNo As String = Me.oCrossMatchTemplate.ID
                If Me.oCrossMatchTemplate.Fingerprint Is Nothing OrElse String.IsNullOrEmpty(visitNo) Then Return

                Me.ShowPatientDetails(visitNo)

            ElseIf oVariousOptions.FingerprintDevice.ToUpper().Equals(oFingerprintDeviceID.DigitalPersona.ToUpper()) Then

                Dim fVerification As New Verification(visitFingerprints, "VisitNo")
                fVerification.ShowDialog()

                ' If Not String.IsNullOrEmpty(Me.oDigitalPersonaTemplate.ID) Then Me.LoadVisitsData(Me.oDigitalPersonaTemplate.ID)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
        End Try

    End Sub

#End Region

#Region "CLEAR CONTROLS"
    Private Sub ClearControls()
        Me.stbANCNo.Clear()
        Me.stbPatientNo.Clear()
        Me.stbFullName.Clear()
        Me.stbANCJoinDate.Clear()
        Me.stbAddress.Clear()
        Me.stbTotalVisits.Clear()
        Me.stbPhone.Clear()
        Me.stbBloodGroup.Clear()
        Me.txtVisitDate.Clear()
        Me.stbEDD.Clear()
        Me.stbHIVStatus.Clear()
        Me.stbPartnersHIVStatus.Clear()
        Me.spbPhoto.Image = Nothing
        Me.nbxWeight.Clear()
        Me.nbxTemperature.Clear()
        Me.nbxHeight.Clear()
        Me.nbxMUAC.Clear()
        Me.nbxPulse.Clear()
        Me.stbBloodPressure.Clear()
        Me.nbxRespirationRate.Clear()
        Me.nbxOxygenSaturation.Clear()
        Me.nbxHeartRate.Clear()
        Me.nbxBMI.Clear()
        Me.stbBMIStatus.Clear()
        Me.stbNotes.Clear()

        ResetControls()

    End Sub

    Private Sub ResetControls()

        '''''''''''''''''''''''''''''''''''''''
        ResetControlsIn(Me.tpgANCVitals)
        ResetControlsIn(Me.tpgFoetalExamination)
        ResetControlsIn(Me.tpgPelvicExamination)
        ResetControlsIn(Me.tpgPhysicalExamination)
        ResetControlsIn(Me.tpgANCAllergies)
        ResetControlsIn(Me.grpAbdomen)
        ResetControlsIn(Me.grpAssessment)
        ResetControlsIn(Me.grpGeneral)
        ResetControlsIn(Me.grpChest)
        ResetControlsIn(Me.grpCNS)
        ResetControlsIn(Me.grpExamination)
        ResetControlsIn(Me.grpPrognosis)
        ResetControlsIn(Me.grpSTIs)

        '''''''''''''''''''''''''''''''''''''''

        '''''''''''''''''''''''''''''''''''''''

    End Sub

#End Region


#Region "SAVE OPTIONS"
    Private Function PelvicExaminationList() As List(Of DBConnect)
        Dim lPelvicExamination As New List(Of DBConnect)

        Try
            If String.IsNullOrEmpty(cboVulvaID.Text) AndAlso String.IsNullOrEmpty(cboCervixID.Text) AndAlso
                String.IsNullOrEmpty(cboAdnexaID.Text) AndAlso String.IsNullOrEmpty(cboVaginaID.Text) AndAlso String.IsNullOrEmpty(cboUterusID.Text) AndAlso
                String.IsNullOrEmpty(nbxDiagonalConjugate.Value) AndAlso String.IsNullOrEmpty(nbxSacralCurve.Value) AndAlso String.IsNullOrEmpty(nbxIschialSpine.Value) AndAlso
                String.IsNullOrEmpty(nbxSubPublicAngle.Value) AndAlso String.IsNullOrEmpty(nbxIschialTuberosities.Value) AndAlso String.IsNullOrEmpty(cboConclusionID.Text) AndAlso
                String.IsNullOrEmpty(stbRiskFactors.Text) AndAlso
                String.IsNullOrEmpty(stbRecommendations.Text) Then

                Return Nothing
            End If


            Dim oPelvicExamination As New SyncSoft.SQLDb.PelvicExamination()

            With oPelvicExamination

                .VisitNo = RevertText(StringEnteredIn(Me.stbVisitNo, "Visit No!"))
                .ANCNo = RevertText(StringEnteredIn(Me.stbANCNo, "ANC No!"))

                If cboVulvaID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPelvicExamination.Name)
                    .VulvaID = StringValueEnteredIn(Me.cboVulvaID, "Vulva!")
                    Me.cboVulvaID.Focus()
                Else
                    .VulvaID = StringValueEnteredIn(Me.cboVulvaID, "Vulva!")
                End If

                If cboCervixID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPelvicExamination.Name)
                    .CervixID = StringValueEnteredIn(Me.cboCervixID, "Cervix!")
                    Me.cboCervixID.Focus()
                Else
                    .CervixID = StringValueEnteredIn(Me.cboCervixID, "Cervix!")
                End If

                If cboAdnexaID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPelvicExamination.Name)
                    .AdnexaID = StringValueEnteredIn(Me.cboAdnexaID, "Adnexa!")
                    Me.cboAdnexaID.Focus()
                Else
                    .AdnexaID = StringValueEnteredIn(Me.cboAdnexaID, "Adnexa!")
                End If

                If cboVaginaID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPelvicExamination.Name)
                    .VaginaID = StringValueEnteredIn(Me.cboVaginaID, "Vagina!")
                    Me.cboVaginaID.Focus()
                Else
                    .VaginaID = StringValueEnteredIn(Me.cboVaginaID, "Vagina!")
                End If

                If cboUterusID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPelvicExamination.Name)
                    .UterusID = StringValueEnteredIn(Me.cboUterusID, "Uterus!")
                    Me.cboUterusID.Focus()
                Else
                    .UterusID = StringValueEnteredIn(Me.cboUterusID, "Uterus!")
                End If

                If String.IsNullOrEmpty(nbxDiagonalConjugate.Text) Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPelvicExamination.Name)
                    .DiagonalConjugate = IntegerEnteredIn(Me.nbxDiagonalConjugate)
                    Me.nbxDiagonalConjugate.Focus()
                Else
                    .DiagonalConjugate = IntegerEnteredIn(Me.nbxDiagonalConjugate)
                End If

                If String.IsNullOrEmpty(nbxSacralCurve.Text) Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPelvicExamination.Name)
                    .SacralCurve = IntegerEnteredIn(Me.nbxSacralCurve)
                    Me.nbxSacralCurve.Focus()
                Else
                    .SacralCurve = IntegerEnteredIn(Me.nbxSacralCurve)
                End If

                If String.IsNullOrEmpty(nbxIschialSpine.Text) Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPelvicExamination.Name)
                    .IschialSpine = IntegerEnteredIn(Me.nbxIschialSpine)
                    Me.nbxIschialSpine.Focus()
                Else
                    .IschialSpine = IntegerEnteredIn(Me.nbxIschialSpine)
                End If

                If String.IsNullOrEmpty(nbxSubPublicAngle.Text) Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPelvicExamination.Name)
                    .SubPublicAngle = IntegerEnteredIn(Me.nbxSubPublicAngle)
                    Me.nbxSubPublicAngle.Focus()
                Else
                    .SubPublicAngle = IntegerEnteredIn(Me.nbxSubPublicAngle)
                End If

                If String.IsNullOrEmpty(nbxIschialTuberosities.Text) Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPelvicExamination.Name)
                    .IschialTuberosities = IntegerEnteredIn(Me.nbxIschialTuberosities)
                    Me.nbxIschialTuberosities.Focus()
                Else
                    .IschialTuberosities = IntegerEnteredIn(Me.nbxIschialTuberosities)
                End If

                If cboConclusionID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPelvicExamination.Name)
                    .ConclusionID = StringValueEnteredIn(Me.cboConclusionID, "Conclusion!")
                    Me.cboConclusionID.Focus()
                Else
                    .ConclusionID = StringValueEnteredIn(Me.cboConclusionID, "Conclusion!")
                End If

                .RiskFactors = StringMayBeEnteredIn(Me.stbRiskFactors)
                .Recommendations = StringMayBeEnteredIn(Me.stbRecommendations)
                .LoginID = CurrentUser.LoginID

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ValidateEntriesIn(Me)
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End With

            lPelvicExamination.Add(oPelvicExamination)

            Return lPelvicExamination

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Function AntenatalVisitList() As List(Of DBConnect)

        Dim lAntenatalVisit As New List(Of DBConnect)
        Try
            Dim oAntenatalVisits As New SyncSoft.SQLDb.AntenatalVisits()


            With oAntenatalVisits
                .VisitNo = RevertText(StringEnteredIn(Me.stbVisitNo, "Visit No!"))
                .ANCNo = RevertText(StringEnteredIn(Me.stbANCNo, "ANC No!"))
                .DoctorSpecialityID = StringValueEnteredIn(Me.cboDoctorSpecialtyID, "Doctor Speciality!")
                .DoctorID = SubstringEnteredIn(Me.cboDoctorID, "Doctor!")

                .NurseInChargeID = SubstringEnteredIn(Me.cboNurseID, "Nurse!")
                .ReturnDate = DateEnteredIn(Me.dtpReturnDate, "Return Date!")



                If cboPallorID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPhysicalExamination.Name)
                    .PallorID = StringValueEnteredIn(Me.cboPallorID, "Pallor!")
                    Me.cboPallorID.Focus()
                Else
                    .PallorID = StringValueEnteredIn(Me.cboPallorID, "Pallor!")
                End If

                If cboJaundiceID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPhysicalExamination.Name)
                    .JaundiceID = StringValueEnteredIn(Me.cboJaundiceID, "Jaundice!")
                    Me.cboJaundiceID.Focus()
                Else
                    .JaundiceID = StringValueEnteredIn(Me.cboJaundiceID, "Jaundice!")
                End If

                .LynphadenopathyID = StringValueEnteredIn(Me.cboLynphadenopathyID, "Lynphadenopathy!")

                If cboVaricoseID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPhysicalExamination.Name)
                    .VaricoseID = StringValueEnteredIn(Me.cboVaricoseID, "Varicose!")
                    Me.cboVaricoseID.Focus()
                Else
                    .VaricoseID = StringValueEnteredIn(Me.cboVaricoseID, "Varicose!")
                End If

                If cboOedemaID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPhysicalExamination.Name)
                    .OedemaID = StringValueEnteredIn(Me.cboOedemaID, "Oedema!")
                    Me.cboOedemaID.Focus()
                Else
                    .OedemaID = StringValueEnteredIn(Me.cboOedemaID, "Oedema!")
                End If
                .SkeletalDeformityID = StringValueEnteredIn(Me.cboSkeletalDeformityID, "Skeletal Deformity!")

                If cboHeartSoundID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPhysicalExamination.Name)
                    .HeartSoundID = StringValueEnteredIn(Me.cboHeartSoundID, "Heart Sound!")
                    Me.cboHeartSoundID.Focus()
                Else
                    .HeartSoundID = StringValueEnteredIn(Me.cboHeartSoundID, "Heart Sound!")
                End If

                If cboAirEntryID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPhysicalExamination.Name)
                    .AirEntryID = StringValueEnteredIn(Me.cboAirEntryID, "Air Entry!")
                    Me.cboAirEntryID.Focus()
                Else
                    .AirEntryID = StringValueEnteredIn(Me.cboAirEntryID, "Air Entry!")
                End If

                If cboBreastID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPhysicalExamination.Name)
                    .BreastID = StringValueEnteredIn(Me.cboBreastID, "Breast!")
                    Me.cboBreastID.Focus()
                Else
                    .BreastID = StringValueEnteredIn(Me.cboBreastID, "Breast!")
                End If

                If cboLiverID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPhysicalExamination.Name)
                    .LiverID = StringValueEnteredIn(Me.cboLiverID, "Liver!")
                    Me.cboLiverID.Focus()
                Else
                    .LiverID = StringValueEnteredIn(Me.cboLiverID, "Liver!")
                End If

                If cboSpleenID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPhysicalExamination.Name)
                    .SpleenID = StringValueEnteredIn(Me.cboSpleenID, "Spleen!")
                    Me.cboSpleenID.Focus()
                Else
                    .SpleenID = StringValueEnteredIn(Me.cboSpleenID, "Spleen!")
                End If

                .BowelSoundsID = StringValueEnteredIn(Me.cboBowelSoundsID, "Bowel Sounds!")
                .ScarID = StringValueEnteredIn(Me.cboScarID, "Scar!")

                If cboPupilReactionID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPhysicalExamination.Name)
                    .PupilReactionID = StringValueEnteredIn(Me.cboPupilReactionID, "Pupil Reaction!")
                    Me.cboPupilReactionID.Focus()
                Else
                    .PupilReactionID = StringValueEnteredIn(Me.cboPupilReactionID, "Pupil Reaction!")
                End If

                If cboReflexesID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPhysicalExamination.Name)
                    .ReflexesID = StringValueEnteredIn(Me.cboReflexesID, "Reflexes!")
                    Me.cboReflexesID.Focus()
                Else
                    .ReflexesID = StringValueEnteredIn(Me.cboReflexesID, "Reflexes!")
                End If

                If cboOtherSTIID.SelectedIndex = -1 Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgPhysicalExamination.Name)
                    .OtherSTIID = StringValueEnteredIn(Me.cboOtherSTIID, "Other STIs!")
                    Me.cboOtherSTIID.Focus()
                Else
                    .OtherSTIID = StringValueEnteredIn(Me.cboOtherSTIID, "Other STIs!")
                End If

                .STIDetails = StringMayBeEnteredIn(Me.stbSTIDetails)


                If String.IsNullOrEmpty(nbxAnenorrheaWeeks.Value.ToString()) Then
                    Me.tbcPhysicalExamination.SelectTab(Me.tpgFoetalExamination.Name)
                    .AnenorrheaWeeks = Me.nbxAnenorrheaWeeks.GetInteger()
                    Me.nbxAnenorrheaWeeks.Focus()
                Else
                    .AnenorrheaWeeks = Me.nbxAnenorrheaWeeks.GetInteger()
                End If

                .FundalHeight = StringMayBeEnteredIn(Me.stbFundalHeight)
                .PresentationID = StringValueEnteredIn(Me.cboPresentationID)
                .LieID = StringValueEnteredIn(Me.cboLieID)
                .PositionID = StringValueEnteredIn(Me.cboPositionID, "Position!")


                .RelationPPOrBrim = StringMayBeEnteredIn(Me.stbRelationPPOrBrim)

                .FoetalHeart = Me.nbxFoetalHeart.GetInteger()

                .TTGiven = Me.chkTTGiven.Checked
                .IPTID = StringValueEnteredIn(Me.cboIPT, "IPT!")
                .NetUseID = StringValueEnteredIn(Me.cboNetUse, "Net Use (Mosquito Net)")
                .Remarks = StringMayBeEnteredIn(Me.stbRemarks)




                .LoginID = CurrentUser.LoginID

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ValidateEntriesIn(Me)
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            End With

            lAntenatalVisit.Add(oAntenatalVisits)

            Return lAntenatalVisit
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region


#Region " Visits Navigate "

    Private Sub EnableNavigateVisitsCTLS(ByVal state As Boolean)

        Dim startPosition As Integer = 0
        Dim oVisits As New SyncSoft.SQLDb.Visits()
        Dim oAntenatalVisits As New SyncSoft.SQLDb.AntenatalVisits()


        Try

            Me.Cursor = Cursors.WaitCursor

            If state Then

                Dim visitNo As String = RevertText(StringEnteredIn(Me.stbVisitNo, "Visit No!"))
                Dim ancNo As String = RevertText(StringEnteredIn(Me.stbANCNo, "ANC No!"))

                Dim visits As DataTable = oAntenatalVisits.GetAntenatalVisitsByANCNo(ancNo).Tables("AntenatalVisits")

                For pos As Integer = 0 To visits.Rows.Count - 1
                    If visitNo.ToUpper().Equals(visits.Rows(pos).Item("VisitNo").ToString().ToUpper()) Then
                        startPosition = pos + 1
                        Exit For
                    ElseIf pos = (visits.Rows.Count - 1) And startPosition = 0 Then
                        startPosition = pos + 1
                    Else : startPosition = 1
                    End If
                Next

                Me.navVisits.DataSource = visits
                Me.navVisits.Navigate(startPosition)

            Else : Me.navVisits.Clear()
            End If

        Catch eX As Exception
            Me.chkNavigateVisits.Checked = False
            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


#End Region


    Private Sub chkNavigateVisits_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkNavigateVisits.Click
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If Not Me.AllSaved() Then
            Me.chkNavigateVisits.Checked = Not Me.chkNavigateVisits.Checked
            Return
        End If

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If chkNavigateVisits.Checked = True Then
            Me.EnableNavigateVisitsCTLS(Me.chkNavigateVisits.Checked)
        Else
            Me.navVisits.Clear()
        End If

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Sub

    Private Sub OnCurrentValue(ByVal currentValue As Object) Handles navVisits.OnCurrentValue

        Try

            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim visitNo As String = RevertText(currentValue.ToString())
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If String.IsNullOrEmpty(visitNo) Then Return
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbVisitNo.Text = FormatText(visitNo, "Visits", "VisitNo")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.LoadAntenatalVisits(visitNo)
            Me.LoadPelvicExamination(visitNo)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch eX As Exception
            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


    Private Sub LoadRecords(ByVal visitNo As String)
        Try

            '''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Me.ShowPatientDetails(visitNo)
            Me.LoadAntenatalVisits(visitNo)
            '''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            DisplayMessage(ex.Message)

        End Try

    End Sub


    Private Sub ShowOtherDetails(ByVal visitNo As String)

        visitNo = RevertText(StringEnteredIn(Me.stbVisitNo, "Visit No!"))

        Dim oAntenatalVisits As New SyncSoft.SQLDb.AntenatalVisits()

        Try
            Me.Cursor = Cursors.WaitCursor()

            Dim dataSource As DataTable = oAntenatalVisits.GetAntenatalVisits(visitNo).Tables("AntenatalVisits")
            Me.DisplayData(dataSource)


        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub


    Private Function AllSaved() As Boolean

        Try

            Dim oVariousOptions As New VariousOptions()
            Dim message As String
            Dim visitNo As String = StringMayBeEnteredIn(Me.stbVisitNo)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If String.IsNullOrEmpty(visitNo) Then Return True
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            Dim oAntenatalVisits As New SyncSoft.SQLDb.AntenatalVisits()

            Dim AntenatalVisits As DataTable = oAntenatalVisits.GetAntenatalVisits(RevertText(visitNo)).Tables("AntenatalVisits")



            If AntenatalVisits Is Nothing Then

                message = "Please ensure that At least One Record is saved under Antenatal progress!"
                DisplayMessage(message)

                Me.BringToFront()
                If Me.WindowState = FormWindowState.Minimized Then Me.WindowState = FormWindowState.Normal
                Return False
            End If

            '''''''''''''''''''''''''''''''
            Return True
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Return True

        End Try

    End Function

    Private Sub btnTriage_Click(sender As System.Object, e As System.EventArgs) Handles btnTriage.Click
        Try
            Me.Cursor = Cursors.WaitCursor

            Dim visitNo As String = RevertText(StringEnteredIn(Me.stbVisitNo, "Visit No!"))
            Dim ancNo As String = RevertText(StringEnteredIn(Me.stbANCNo, "ANC No!"))
            Dim fTriage As New frmTriage(visitNo, True)

            If Me.btnTriage.Text.Equals(Me.EditTriageTest) Then
                fTriage.Edit()
            Else : fTriage.Save()
            End If

            fTriage.ShowDialog()
            Me.ShowPatientDetails(ancNo)

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub chkCreateNewVisit_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkCreateNewVisit.CheckedChanged
        Try
            If chkCreateNewVisit.Checked = False Then
                ResetControls()
                Me.Edit()

            End If
            If chkCreateNewVisit.Checked = True Then
                ResetControls()
                Me.Save()

            End If
        Catch ex As Exception
            DisplayMessage(ex.Message)
        End Try
    End Sub

    Private Sub btnFindVisitNo_Click(sender As System.Object, e As System.EventArgs) Handles btnFindVisitNo.Click
        Try
            Me.Cursor = Cursors.WaitCursor

            Dim fFindANCNo As New frmFindAutoNo(Me.stbVisitNo, AutoNumber.AntenatalVisitNo)
            fFindANCNo.ShowDialog(Me)
            Me.stbVisitNo.Focus()

            Dim visitNo As String = RevertText(StringMayBeEnteredIn(Me.stbVisitNo))
            If String.IsNullOrEmpty(visitNo) Then Return

            LoadAntenatalVisits(visitNo)
            LoadPelvicExamination(visitNo)
            btnClear.Visible = False

        Catch ex As Exception
            DisplayMessage(ex.Message)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClear_Click(sender As System.Object, e As System.EventArgs) Handles btnClear.Click
        ResetControls()
    End Sub


    Private Sub stbVisitNo_TextChanged(sender As Object, e As System.EventArgs) Handles stbVisitNo.TextChanged
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim visitNo As String = StringMayBeEnteredIn(Me.stbVisitNo)
        If Not String.IsNullOrEmpty(visitNo) Then
            Me.stbVisitNo.Text = visitNo
            Return
        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Me.ClearControls()
        Me.ResetControls()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Sub

    Private Sub stbVisitNo_leave(sender As System.Object, e As System.EventArgs) Handles stbVisitNo.Leave
        Try
            Me.Cursor = Cursors.WaitCursor
            ClearControls()


            Dim visitNo As String = RevertText(StringMayBeEnteredIn(Me.stbVisitNo))
            ErrProvider.Clear()
            If String.IsNullOrEmpty(visitNo) Then Return

            If Me.ebnSaveUpdate.ButtonText = ButtonCaption.Save Then
                If Not String.IsNullOrEmpty(visitNo) Then
                    Dim oAntenatalEnrollment As New AntenatalEnrollment()
                    Dim antenatalDetails As DataTable = oAntenatalEnrollment.GetAntenatalEnrollmentByVisitNo(visitNo).Tables("AntenatalEnrollment")
                    If antenatalDetails Is Nothing OrElse antenatalDetails.Rows.Count < 1 Then Return
                    Dim ancNo As String = antenatalDetails.Rows(0).Item("ANCNo").ToString()
                    Me.stbANCNo.Text = RevertText(ancNo)
                    Me.ShowPatientDetails(ancNo)
                End If
            ElseIf Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update Then
                'load antenatal visit
                LoadAntenatalVisits(visitNo)
                LoadPelvicExamination(visitNo)
                btnClear.Visible = False
                Me.ebnSaveUpdate.Enabled = True
                Me.fbnDelete.Enabled = True
            End If

        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub cboDoctorSpecialtyID_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboDoctorSpecialtyID.SelectedIndexChanged
        Try

            Dim doctorSpecialtyID As String = StringValueMayBeEnteredIn(Me.cboDoctorSpecialtyID, "Doctor Specialty!")
            ErrProvider.Clear()
            If String.IsNullOrEmpty(doctorSpecialtyID) Then Return

            Me.LoadStaff(doctorSpecialtyID)
        Catch ex As Exception
            ErrorMessage(ex)

        End Try
    End Sub

End Class