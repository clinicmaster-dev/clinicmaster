<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInsuranceClaimSummaries
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInsuranceClaimSummaries))
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.tbcPeriodicReport = New System.Windows.Forms.TabControl()
        Me.tpgMemberConsumptionDetails = New System.Windows.Forms.TabPage()
        Me.lblMemberBalance = New System.Windows.Forms.Label()
        Me.stbMemberBalance = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblAllocatedPremium = New System.Windows.Forms.Label()
        Me.stbAllocatedPremium = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboMainMemberNo = New System.Windows.Forms.ComboBox()
        Me.lblAmountWords = New System.Windows.Forms.Label()
        Me.stbAmountWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblTotalAmount = New System.Windows.Forms.Label()
        Me.stbGrandTotalAmount = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.StbMainMemberNameDetails = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.stbMainMemberCompanyName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dgvMemberConsumptionDetails = New System.Windows.Forms.DataGridView()
        Me.ColMembertype = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColMedicalCardNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColPatientNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColClaimAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgfullMemberConsumptionDetails = New System.Windows.Forms.TabPage()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.stbmemberConsumptionWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.stbMemberConsumptions = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cbofullMainMemberNo = New System.Windows.Forms.ComboBox()
        Me.stbfullMainMemberName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.stbfullCompanyName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dgvMemberfullConsumptionDetails = New System.Windows.Forms.DataGridView()
        Me.ColConsumptionMemberType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColConsumptionMedicalCardNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColConsumptionClaimNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColConsumptionFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColConsumptionVisitNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColColConsumptionItemName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColColConsumptionQuantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColColConsumptionUnitPrice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColColConsumptionAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgIPDMemberConsumptionDetails = New System.Windows.Forms.TabPage()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.stbIPDmemberConsumptionWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.stbIPDMemberConsumptions = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboIPDfullMainMemberNo = New System.Windows.Forms.ComboBox()
        Me.stbIPDMainMemberName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.StbIPDCompanyName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.dgvMemberfullIPDConsumptionDetails = New System.Windows.Forms.DataGridView()
        Me.colIPDMembertype = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIPDMedicalCardNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColIPDInvoiceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIPDFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColIPDVisitNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColIPDItemName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColIPDQuantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIPDUnitPrice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColIPDAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgGeneralConsumption = New System.Windows.Forms.TabPage()
        Me.lblTotalPremium = New System.Windows.Forms.Label()
        Me.stbTotalPremium = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lbltotalConsumptionWorfs = New System.Windows.Forms.Label()
        Me.stbtotalConsumptionWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblTotalConsumption = New System.Windows.Forms.Label()
        Me.stbtotalConsumption = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.dgvGeneralConsumption = New System.Windows.Forms.DataGridView()
        Me.stbGeneralCompanyName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboGeneralCompanyNo = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.stbGeneralInsuranceName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboGeneralInsuranceNo = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.tpgGeneralIPDConsumption = New System.Windows.Forms.TabPage()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.stbIPDTotalPremium = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.stbIPDtotalConsumptionWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.stbIPDtotalConsumption = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.dgvGeneralIPDConsumption = New System.Windows.Forms.DataGridView()
        Me.colGeneralIPDFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colGeneralIPDPatientNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGeneralIPDInsuranceName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGeneralIPDMedicalCardNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColIPDGeneralMemberType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colGeneralIPDCompanyName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGeneralIPDPolicyName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGeneralIPDMemberPremium = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoGeneralIPDTotalamount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colGeneralIPDPolicyStartDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colGeneralIPDPolicyEndDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colGeneralIPDMainMemberNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.stbGeneralIPDCompanyName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboGeneralIPDCompanyNo = New System.Windows.Forms.ComboBox()
        Me.lblGeneralIPDCompanyName = New System.Windows.Forms.Label()
        Me.lblGeneralIPDCompanyNo = New System.Windows.Forms.Label()
        Me.stbGeneralIPDInsuranceName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboGeneralIPDInsuranceNo = New System.Windows.Forms.ComboBox()
        Me.lblGeneralIPDInsuranceName = New System.Windows.Forms.Label()
        Me.lblGeneralIPDInsuranceNo = New System.Windows.Forms.Label()
        Me.tpgOverConsumption = New System.Windows.Forms.TabPage()
        Me.dgvOverConsumption = New System.Windows.Forms.DataGridView()
        Me.colOverConsumptionFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOverConsumptionPatientNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOverConsumptionInsuranceName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOverConsumptionMedicalCardNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOverConsumptionMemberType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOverConsumptionCompanyName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOverConsumedPolicyName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColOverConsumptionMemberPremium = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumedAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColExcess = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOverConsumptionPolicyStartDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOverConsumptionPolicyEndDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColOverConsumedMainMemberNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.stbOVOPDCompanyName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboOVCompanyNo = New System.Windows.Forms.ComboBox()
        Me.lblOVOPDCompanyName = New System.Windows.Forms.Label()
        Me.lblOVCompanyNo = New System.Windows.Forms.Label()
        Me.stbOVInsuranceName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboOVInsuranceNo = New System.Windows.Forms.ComboBox()
        Me.lblOVInsuranceName = New System.Windows.Forms.Label()
        Me.lblOVInsuranceNo = New System.Windows.Forms.Label()
        Me.cmsClaimSummaries = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmsClaimSummariesCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmsClaimSummariesSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.fbnLoad = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker()
        Me.lblEndDate = New System.Windows.Forms.Label()
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker()
        Me.lblStartDate = New System.Windows.Forms.Label()
        Me.fbnExportTo = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.grpPeriod = New System.Windows.Forms.GroupBox()
        Me.fbnPrint = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ColGeneralMainMemberNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGeneralConsumptionMainMemberName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGeneralMedicalCardNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGeneralMemberType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colGeneralFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colGeneralPatientNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGeneralInsuranceName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGeneralCompanyName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGeneralPolicyName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGeneralMemberPremium = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGeneralTotalamount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGeneralOPDConsumptionBalance = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGeneralPolicyStartDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGeneralPolicyEndDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tbcPeriodicReport.SuspendLayout()
        Me.tpgMemberConsumptionDetails.SuspendLayout()
        CType(Me.dgvMemberConsumptionDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgfullMemberConsumptionDetails.SuspendLayout()
        CType(Me.dgvMemberfullConsumptionDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgIPDMemberConsumptionDetails.SuspendLayout()
        CType(Me.dgvMemberfullIPDConsumptionDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgGeneralConsumption.SuspendLayout()
        CType(Me.dgvGeneralConsumption, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgGeneralIPDConsumption.SuspendLayout()
        CType(Me.dgvGeneralIPDConsumption, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgOverConsumption.SuspendLayout()
        CType(Me.dgvOverConsumption, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsClaimSummaries.SuspendLayout()
        Me.grpPeriod.SuspendLayout()
        Me.SuspendLayout()
        '
        'fbnClose
        '
        Me.fbnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(948, 564)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(79, 24)
        Me.fbnClose.TabIndex = 2
        Me.fbnClose.Text = "Close"
        '
        'tbcPeriodicReport
        '
        Me.tbcPeriodicReport.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbcPeriodicReport.Controls.Add(Me.tpgMemberConsumptionDetails)
        Me.tbcPeriodicReport.Controls.Add(Me.tpgfullMemberConsumptionDetails)
        Me.tbcPeriodicReport.Controls.Add(Me.tpgIPDMemberConsumptionDetails)
        Me.tbcPeriodicReport.Controls.Add(Me.tpgGeneralConsumption)
        Me.tbcPeriodicReport.Controls.Add(Me.tpgGeneralIPDConsumption)
        Me.tbcPeriodicReport.Controls.Add(Me.tpgOverConsumption)
        Me.tbcPeriodicReport.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcPeriodicReport.HotTrack = True
        Me.tbcPeriodicReport.Location = New System.Drawing.Point(12, 57)
        Me.tbcPeriodicReport.Name = "tbcPeriodicReport"
        Me.tbcPeriodicReport.SelectedIndex = 0
        Me.tbcPeriodicReport.Size = New System.Drawing.Size(1026, 501)
        Me.tbcPeriodicReport.TabIndex = 1
        '
        'tpgMemberConsumptionDetails
        '
        Me.tpgMemberConsumptionDetails.Controls.Add(Me.lblMemberBalance)
        Me.tpgMemberConsumptionDetails.Controls.Add(Me.stbMemberBalance)
        Me.tpgMemberConsumptionDetails.Controls.Add(Me.lblAllocatedPremium)
        Me.tpgMemberConsumptionDetails.Controls.Add(Me.stbAllocatedPremium)
        Me.tpgMemberConsumptionDetails.Controls.Add(Me.cboMainMemberNo)
        Me.tpgMemberConsumptionDetails.Controls.Add(Me.lblAmountWords)
        Me.tpgMemberConsumptionDetails.Controls.Add(Me.stbAmountWords)
        Me.tpgMemberConsumptionDetails.Controls.Add(Me.lblTotalAmount)
        Me.tpgMemberConsumptionDetails.Controls.Add(Me.stbGrandTotalAmount)
        Me.tpgMemberConsumptionDetails.Controls.Add(Me.StbMainMemberNameDetails)
        Me.tpgMemberConsumptionDetails.Controls.Add(Me.Label1)
        Me.tpgMemberConsumptionDetails.Controls.Add(Me.stbMainMemberCompanyName)
        Me.tpgMemberConsumptionDetails.Controls.Add(Me.Label2)
        Me.tpgMemberConsumptionDetails.Controls.Add(Me.Label4)
        Me.tpgMemberConsumptionDetails.Controls.Add(Me.dgvMemberConsumptionDetails)
        Me.tpgMemberConsumptionDetails.Location = New System.Drawing.Point(4, 22)
        Me.tpgMemberConsumptionDetails.Name = "tpgMemberConsumptionDetails"
        Me.tpgMemberConsumptionDetails.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgMemberConsumptionDetails.Size = New System.Drawing.Size(1018, 475)
        Me.tpgMemberConsumptionDetails.TabIndex = 7
        Me.tpgMemberConsumptionDetails.Text = "Member Consumption Summaries"
        Me.tpgMemberConsumptionDetails.UseVisualStyleBackColor = True
        '
        'lblMemberBalance
        '
        Me.lblMemberBalance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblMemberBalance.Location = New System.Drawing.Point(6, 409)
        Me.lblMemberBalance.Name = "lblMemberBalance"
        Me.lblMemberBalance.Size = New System.Drawing.Size(116, 20)
        Me.lblMemberBalance.TabIndex = 72
        Me.lblMemberBalance.Text = "Member Balance"
        '
        'stbMemberBalance
        '
        Me.stbMemberBalance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbMemberBalance.BackColor = System.Drawing.SystemColors.Info
        Me.stbMemberBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbMemberBalance.CapitalizeFirstLetter = False
        Me.stbMemberBalance.Enabled = False
        Me.stbMemberBalance.EntryErrorMSG = ""
        Me.stbMemberBalance.Location = New System.Drawing.Point(132, 407)
        Me.stbMemberBalance.MaxLength = 20
        Me.stbMemberBalance.Name = "stbMemberBalance"
        Me.stbMemberBalance.RegularExpression = ""
        Me.stbMemberBalance.Size = New System.Drawing.Size(243, 20)
        Me.stbMemberBalance.TabIndex = 73
        Me.stbMemberBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAllocatedPremium
        '
        Me.lblAllocatedPremium.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblAllocatedPremium.Location = New System.Drawing.Point(7, 385)
        Me.lblAllocatedPremium.Name = "lblAllocatedPremium"
        Me.lblAllocatedPremium.Size = New System.Drawing.Size(119, 20)
        Me.lblAllocatedPremium.TabIndex = 70
        Me.lblAllocatedPremium.Text = "Allocated Premium"
        '
        'stbAllocatedPremium
        '
        Me.stbAllocatedPremium.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbAllocatedPremium.BackColor = System.Drawing.SystemColors.Info
        Me.stbAllocatedPremium.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAllocatedPremium.CapitalizeFirstLetter = False
        Me.stbAllocatedPremium.Enabled = False
        Me.stbAllocatedPremium.EntryErrorMSG = ""
        Me.stbAllocatedPremium.Location = New System.Drawing.Point(132, 383)
        Me.stbAllocatedPremium.MaxLength = 20
        Me.stbAllocatedPremium.Name = "stbAllocatedPremium"
        Me.stbAllocatedPremium.RegularExpression = ""
        Me.stbAllocatedPremium.Size = New System.Drawing.Size(243, 20)
        Me.stbAllocatedPremium.TabIndex = 71
        Me.stbAllocatedPremium.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboMainMemberNo
        '
        Me.cboMainMemberNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboMainMemberNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboMainMemberNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboMainMemberNo.DropDownWidth = 256
        Me.cboMainMemberNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboMainMemberNo.FormattingEnabled = True
        Me.cboMainMemberNo.ItemHeight = 13
        Me.cboMainMemberNo.Location = New System.Drawing.Point(147, 3)
        Me.cboMainMemberNo.Name = "cboMainMemberNo"
        Me.cboMainMemberNo.Size = New System.Drawing.Size(168, 21)
        Me.cboMainMemberNo.TabIndex = 69
        '
        'lblAmountWords
        '
        Me.lblAmountWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblAmountWords.Location = New System.Drawing.Point(7, 431)
        Me.lblAmountWords.Name = "lblAmountWords"
        Me.lblAmountWords.Size = New System.Drawing.Size(121, 22)
        Me.lblAmountWords.TabIndex = 51
        Me.lblAmountWords.Text = "Balance In Words"
        '
        'stbAmountWords
        '
        Me.stbAmountWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbAmountWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbAmountWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAmountWords.CapitalizeFirstLetter = True
        Me.stbAmountWords.Enabled = False
        Me.stbAmountWords.EntryErrorMSG = ""
        Me.stbAmountWords.Location = New System.Drawing.Point(132, 429)
        Me.stbAmountWords.MaxLength = 20
        Me.stbAmountWords.Multiline = True
        Me.stbAmountWords.Name = "stbAmountWords"
        Me.stbAmountWords.RegularExpression = ""
        Me.stbAmountWords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbAmountWords.Size = New System.Drawing.Size(411, 40)
        Me.stbAmountWords.TabIndex = 52
        Me.stbAmountWords.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalAmount.Location = New System.Drawing.Point(407, 378)
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.Size = New System.Drawing.Size(116, 20)
        Me.lblTotalAmount.TabIndex = 49
        Me.lblTotalAmount.Text = "Total Consumption"
        '
        'stbGrandTotalAmount
        '
        Me.stbGrandTotalAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbGrandTotalAmount.BackColor = System.Drawing.SystemColors.Info
        Me.stbGrandTotalAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbGrandTotalAmount.CapitalizeFirstLetter = False
        Me.stbGrandTotalAmount.Enabled = False
        Me.stbGrandTotalAmount.EntryErrorMSG = ""
        Me.stbGrandTotalAmount.Location = New System.Drawing.Point(529, 376)
        Me.stbGrandTotalAmount.MaxLength = 20
        Me.stbGrandTotalAmount.Name = "stbGrandTotalAmount"
        Me.stbGrandTotalAmount.RegularExpression = ""
        Me.stbGrandTotalAmount.Size = New System.Drawing.Size(243, 20)
        Me.stbGrandTotalAmount.TabIndex = 50
        Me.stbGrandTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'StbMainMemberNameDetails
        '
        Me.StbMainMemberNameDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.StbMainMemberNameDetails.CapitalizeFirstLetter = False
        Me.StbMainMemberNameDetails.EntryErrorMSG = ""
        Me.StbMainMemberNameDetails.Location = New System.Drawing.Point(145, 27)
        Me.StbMainMemberNameDetails.MaxLength = 60
        Me.StbMainMemberNameDetails.Multiline = True
        Me.StbMainMemberNameDetails.Name = "StbMainMemberNameDetails"
        Me.StbMainMemberNameDetails.ReadOnly = True
        Me.StbMainMemberNameDetails.RegularExpression = ""
        Me.StbMainMemberNameDetails.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.StbMainMemberNameDetails.Size = New System.Drawing.Size(172, 28)
        Me.StbMainMemberNameDetails.TabIndex = 46
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(9, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(130, 20)
        Me.Label1.TabIndex = 45
        Me.Label1.Text = "Main Member Name"
        '
        'stbMainMemberCompanyName
        '
        Me.stbMainMemberCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbMainMemberCompanyName.CapitalizeFirstLetter = False
        Me.stbMainMemberCompanyName.EntryErrorMSG = ""
        Me.stbMainMemberCompanyName.Location = New System.Drawing.Point(462, 5)
        Me.stbMainMemberCompanyName.MaxLength = 41
        Me.stbMainMemberCompanyName.Multiline = True
        Me.stbMainMemberCompanyName.Name = "stbMainMemberCompanyName"
        Me.stbMainMemberCompanyName.ReadOnly = True
        Me.stbMainMemberCompanyName.RegularExpression = ""
        Me.stbMainMemberCompanyName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbMainMemberCompanyName.Size = New System.Drawing.Size(218, 50)
        Me.stbMainMemberCompanyName.TabIndex = 48
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(326, 7)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(130, 20)
        Me.Label2.TabIndex = 47
        Me.Label2.Text = "Company Name"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(11, 5)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(130, 20)
        Me.Label4.TabIndex = 41
        Me.Label4.Text = "Main Member No"
        '
        'dgvMemberConsumptionDetails
        '
        Me.dgvMemberConsumptionDetails.AllowUserToAddRows = False
        Me.dgvMemberConsumptionDetails.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvMemberConsumptionDetails.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvMemberConsumptionDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvMemberConsumptionDetails.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvMemberConsumptionDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvMemberConsumptionDetails.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvMemberConsumptionDetails.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMemberConsumptionDetails.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvMemberConsumptionDetails.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColMembertype, Me.ColMedicalCardNo, Me.ColPatientNo, Me.ColFullName, Me.ColClaimAmount})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvMemberConsumptionDetails.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvMemberConsumptionDetails.GridColor = System.Drawing.Color.Khaki
        Me.dgvMemberConsumptionDetails.Location = New System.Drawing.Point(5, 61)
        Me.dgvMemberConsumptionDetails.Name = "dgvMemberConsumptionDetails"
        Me.dgvMemberConsumptionDetails.ReadOnly = True
        Me.dgvMemberConsumptionDetails.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMemberConsumptionDetails.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvMemberConsumptionDetails.RowHeadersVisible = False
        Me.dgvMemberConsumptionDetails.Size = New System.Drawing.Size(1009, 309)
        Me.dgvMemberConsumptionDetails.TabIndex = 26
        Me.dgvMemberConsumptionDetails.Text = "DataGridView1"
        '
        'ColMembertype
        '
        Me.ColMembertype.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColMembertype.DataPropertyName = "Membertype"
        Me.ColMembertype.HeaderText = "Member Type"
        Me.ColMembertype.Name = "ColMembertype"
        Me.ColMembertype.ReadOnly = True
        '
        'ColMedicalCardNo
        '
        Me.ColMedicalCardNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColMedicalCardNo.DataPropertyName = "MedicalCardNo"
        Me.ColMedicalCardNo.HeaderText = "Medical Card No"
        Me.ColMedicalCardNo.Name = "ColMedicalCardNo"
        Me.ColMedicalCardNo.ReadOnly = True
        '
        'ColPatientNo
        '
        Me.ColPatientNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColPatientNo.DataPropertyName = "PatientNo"
        Me.ColPatientNo.HeaderText = "Patient No"
        Me.ColPatientNo.Name = "ColPatientNo"
        Me.ColPatientNo.ReadOnly = True
        '
        'ColFullName
        '
        Me.ColFullName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColFullName.DataPropertyName = "FullName"
        Me.ColFullName.HeaderText = "Full Name"
        Me.ColFullName.Name = "ColFullName"
        Me.ColFullName.ReadOnly = True
        '
        'ColClaimAmount
        '
        Me.ColClaimAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColClaimAmount.DataPropertyName = "InvoiceAmount"
        Me.ColClaimAmount.HeaderText = "Invoice Amount"
        Me.ColClaimAmount.Name = "ColClaimAmount"
        Me.ColClaimAmount.ReadOnly = True
        '
        'tpgfullMemberConsumptionDetails
        '
        Me.tpgfullMemberConsumptionDetails.Controls.Add(Me.Label11)
        Me.tpgfullMemberConsumptionDetails.Controls.Add(Me.stbmemberConsumptionWords)
        Me.tpgfullMemberConsumptionDetails.Controls.Add(Me.Label12)
        Me.tpgfullMemberConsumptionDetails.Controls.Add(Me.stbMemberConsumptions)
        Me.tpgfullMemberConsumptionDetails.Controls.Add(Me.cbofullMainMemberNo)
        Me.tpgfullMemberConsumptionDetails.Controls.Add(Me.stbfullMainMemberName)
        Me.tpgfullMemberConsumptionDetails.Controls.Add(Me.Label3)
        Me.tpgfullMemberConsumptionDetails.Controls.Add(Me.stbfullCompanyName)
        Me.tpgfullMemberConsumptionDetails.Controls.Add(Me.Label5)
        Me.tpgfullMemberConsumptionDetails.Controls.Add(Me.Label6)
        Me.tpgfullMemberConsumptionDetails.Controls.Add(Me.dgvMemberfullConsumptionDetails)
        Me.tpgfullMemberConsumptionDetails.Location = New System.Drawing.Point(4, 22)
        Me.tpgfullMemberConsumptionDetails.Name = "tpgfullMemberConsumptionDetails"
        Me.tpgfullMemberConsumptionDetails.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgfullMemberConsumptionDetails.Size = New System.Drawing.Size(1018, 475)
        Me.tpgfullMemberConsumptionDetails.TabIndex = 8
        Me.tpgfullMemberConsumptionDetails.Text = "Member Consumption Details"
        Me.tpgfullMemberConsumptionDetails.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label11.Location = New System.Drawing.Point(335, 440)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(121, 22)
        Me.Label11.TabIndex = 73
        Me.Label11.Text = "Total In Words"
        '
        'stbmemberConsumptionWords
        '
        Me.stbmemberConsumptionWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbmemberConsumptionWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbmemberConsumptionWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbmemberConsumptionWords.CapitalizeFirstLetter = True
        Me.stbmemberConsumptionWords.Enabled = False
        Me.stbmemberConsumptionWords.EntryErrorMSG = ""
        Me.stbmemberConsumptionWords.Location = New System.Drawing.Point(463, 429)
        Me.stbmemberConsumptionWords.MaxLength = 20
        Me.stbmemberConsumptionWords.Multiline = True
        Me.stbmemberConsumptionWords.Name = "stbmemberConsumptionWords"
        Me.stbmemberConsumptionWords.RegularExpression = ""
        Me.stbmemberConsumptionWords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbmemberConsumptionWords.Size = New System.Drawing.Size(411, 40)
        Me.stbmemberConsumptionWords.TabIndex = 74
        Me.stbmemberConsumptionWords.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label12.Location = New System.Drawing.Point(11, 440)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(57, 20)
        Me.Label12.TabIndex = 71
        Me.Label12.Text = "Total"
        '
        'stbMemberConsumptions
        '
        Me.stbMemberConsumptions.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbMemberConsumptions.BackColor = System.Drawing.SystemColors.Info
        Me.stbMemberConsumptions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbMemberConsumptions.CapitalizeFirstLetter = False
        Me.stbMemberConsumptions.Enabled = False
        Me.stbMemberConsumptions.EntryErrorMSG = ""
        Me.stbMemberConsumptions.Location = New System.Drawing.Point(74, 438)
        Me.stbMemberConsumptions.MaxLength = 20
        Me.stbMemberConsumptions.Name = "stbMemberConsumptions"
        Me.stbMemberConsumptions.RegularExpression = ""
        Me.stbMemberConsumptions.Size = New System.Drawing.Size(243, 20)
        Me.stbMemberConsumptions.TabIndex = 72
        Me.stbMemberConsumptions.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cbofullMainMemberNo
        '
        Me.cbofullMainMemberNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbofullMainMemberNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbofullMainMemberNo.BackColor = System.Drawing.SystemColors.Window
        Me.cbofullMainMemberNo.DropDownWidth = 256
        Me.cbofullMainMemberNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbofullMainMemberNo.FormattingEnabled = True
        Me.cbofullMainMemberNo.ItemHeight = 13
        Me.cbofullMainMemberNo.Location = New System.Drawing.Point(147, 6)
        Me.cbofullMainMemberNo.Name = "cbofullMainMemberNo"
        Me.cbofullMainMemberNo.Size = New System.Drawing.Size(173, 21)
        Me.cbofullMainMemberNo.TabIndex = 70
        '
        'stbfullMainMemberName
        '
        Me.stbfullMainMemberName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbfullMainMemberName.CapitalizeFirstLetter = False
        Me.stbfullMainMemberName.EntryErrorMSG = ""
        Me.stbfullMainMemberName.Location = New System.Drawing.Point(145, 30)
        Me.stbfullMainMemberName.MaxLength = 60
        Me.stbfullMainMemberName.Multiline = True
        Me.stbfullMainMemberName.Name = "stbfullMainMemberName"
        Me.stbfullMainMemberName.ReadOnly = True
        Me.stbfullMainMemberName.RegularExpression = ""
        Me.stbfullMainMemberName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbfullMainMemberName.Size = New System.Drawing.Size(172, 39)
        Me.stbfullMainMemberName.TabIndex = 53
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(9, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(130, 20)
        Me.Label3.TabIndex = 52
        Me.Label3.Text = "Main Member Name"
        '
        'stbfullCompanyName
        '
        Me.stbfullCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbfullCompanyName.CapitalizeFirstLetter = False
        Me.stbfullCompanyName.EntryErrorMSG = ""
        Me.stbfullCompanyName.Location = New System.Drawing.Point(462, 8)
        Me.stbfullCompanyName.MaxLength = 41
        Me.stbfullCompanyName.Multiline = True
        Me.stbfullCompanyName.Name = "stbfullCompanyName"
        Me.stbfullCompanyName.ReadOnly = True
        Me.stbfullCompanyName.RegularExpression = ""
        Me.stbfullCompanyName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbfullCompanyName.Size = New System.Drawing.Size(218, 61)
        Me.stbfullCompanyName.TabIndex = 55
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(326, 10)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(130, 20)
        Me.Label5.TabIndex = 54
        Me.Label5.Text = "Company Name"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(11, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(130, 20)
        Me.Label6.TabIndex = 50
        Me.Label6.Text = "Main Member No"
        '
        'dgvMemberfullConsumptionDetails
        '
        Me.dgvMemberfullConsumptionDetails.AllowUserToAddRows = False
        Me.dgvMemberfullConsumptionDetails.AllowUserToDeleteRows = False
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle5.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvMemberfullConsumptionDetails.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvMemberfullConsumptionDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvMemberfullConsumptionDetails.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvMemberfullConsumptionDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvMemberfullConsumptionDetails.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvMemberfullConsumptionDetails.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMemberfullConsumptionDetails.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvMemberfullConsumptionDetails.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColConsumptionMemberType, Me.ColConsumptionMedicalCardNo, Me.ColConsumptionClaimNo, Me.ColConsumptionFullName, Me.ColConsumptionVisitNo, Me.ColColConsumptionItemName, Me.ColColConsumptionQuantity, Me.ColColConsumptionUnitPrice, Me.ColColConsumptionAmount})
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvMemberfullConsumptionDetails.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgvMemberfullConsumptionDetails.GridColor = System.Drawing.Color.Khaki
        Me.dgvMemberfullConsumptionDetails.Location = New System.Drawing.Point(5, 75)
        Me.dgvMemberfullConsumptionDetails.Name = "dgvMemberfullConsumptionDetails"
        Me.dgvMemberfullConsumptionDetails.ReadOnly = True
        Me.dgvMemberfullConsumptionDetails.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMemberfullConsumptionDetails.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvMemberfullConsumptionDetails.RowHeadersVisible = False
        Me.dgvMemberfullConsumptionDetails.Size = New System.Drawing.Size(1009, 348)
        Me.dgvMemberfullConsumptionDetails.TabIndex = 49
        Me.dgvMemberfullConsumptionDetails.Text = "DataGridView1"
        '
        'ColConsumptionMemberType
        '
        Me.ColConsumptionMemberType.DataPropertyName = "Membertype"
        Me.ColConsumptionMemberType.HeaderText = "Member Type"
        Me.ColConsumptionMemberType.Name = "ColConsumptionMemberType"
        Me.ColConsumptionMemberType.ReadOnly = True
        '
        'ColConsumptionMedicalCardNo
        '
        Me.ColConsumptionMedicalCardNo.DataPropertyName = "MedicalCardNo"
        Me.ColConsumptionMedicalCardNo.HeaderText = "Medical Card No"
        Me.ColConsumptionMedicalCardNo.Name = "ColConsumptionMedicalCardNo"
        Me.ColConsumptionMedicalCardNo.ReadOnly = True
        '
        'ColConsumptionClaimNo
        '
        Me.ColConsumptionClaimNo.DataPropertyName = "InvoiceNo"
        Me.ColConsumptionClaimNo.HeaderText = "Invoice No"
        Me.ColConsumptionClaimNo.Name = "ColConsumptionClaimNo"
        Me.ColConsumptionClaimNo.ReadOnly = True
        '
        'ColConsumptionFullName
        '
        Me.ColConsumptionFullName.DataPropertyName = "FullName"
        Me.ColConsumptionFullName.HeaderText = "Full Name"
        Me.ColConsumptionFullName.Name = "ColConsumptionFullName"
        Me.ColConsumptionFullName.ReadOnly = True
        '
        'ColConsumptionVisitNo
        '
        Me.ColConsumptionVisitNo.DataPropertyName = "VisitNo"
        Me.ColConsumptionVisitNo.HeaderText = "Visit No"
        Me.ColConsumptionVisitNo.Name = "ColConsumptionVisitNo"
        Me.ColConsumptionVisitNo.ReadOnly = True
        '
        'ColColConsumptionItemName
        '
        Me.ColColConsumptionItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColColConsumptionItemName.DataPropertyName = "ItemName"
        Me.ColColConsumptionItemName.HeaderText = "Item Name"
        Me.ColColConsumptionItemName.Name = "ColColConsumptionItemName"
        Me.ColColConsumptionItemName.ReadOnly = True
        '
        'ColColConsumptionQuantity
        '
        Me.ColColConsumptionQuantity.DataPropertyName = "Quantity"
        Me.ColColConsumptionQuantity.HeaderText = "Quantity"
        Me.ColColConsumptionQuantity.Name = "ColColConsumptionQuantity"
        Me.ColColConsumptionQuantity.ReadOnly = True
        '
        'ColColConsumptionUnitPrice
        '
        Me.ColColConsumptionUnitPrice.DataPropertyName = "UnitPrice"
        Me.ColColConsumptionUnitPrice.HeaderText = "Unit Price"
        Me.ColColConsumptionUnitPrice.Name = "ColColConsumptionUnitPrice"
        Me.ColColConsumptionUnitPrice.ReadOnly = True
        '
        'ColColConsumptionAmount
        '
        Me.ColColConsumptionAmount.DataPropertyName = "Amount"
        Me.ColColConsumptionAmount.HeaderText = "Amount"
        Me.ColColConsumptionAmount.Name = "ColColConsumptionAmount"
        Me.ColColConsumptionAmount.ReadOnly = True
        '
        'tpgIPDMemberConsumptionDetails
        '
        Me.tpgIPDMemberConsumptionDetails.Controls.Add(Me.Label7)
        Me.tpgIPDMemberConsumptionDetails.Controls.Add(Me.stbIPDmemberConsumptionWords)
        Me.tpgIPDMemberConsumptionDetails.Controls.Add(Me.Label8)
        Me.tpgIPDMemberConsumptionDetails.Controls.Add(Me.stbIPDMemberConsumptions)
        Me.tpgIPDMemberConsumptionDetails.Controls.Add(Me.cboIPDfullMainMemberNo)
        Me.tpgIPDMemberConsumptionDetails.Controls.Add(Me.stbIPDMainMemberName)
        Me.tpgIPDMemberConsumptionDetails.Controls.Add(Me.Label9)
        Me.tpgIPDMemberConsumptionDetails.Controls.Add(Me.StbIPDCompanyName)
        Me.tpgIPDMemberConsumptionDetails.Controls.Add(Me.Label10)
        Me.tpgIPDMemberConsumptionDetails.Controls.Add(Me.Label13)
        Me.tpgIPDMemberConsumptionDetails.Controls.Add(Me.dgvMemberfullIPDConsumptionDetails)
        Me.tpgIPDMemberConsumptionDetails.Location = New System.Drawing.Point(4, 22)
        Me.tpgIPDMemberConsumptionDetails.Name = "tpgIPDMemberConsumptionDetails"
        Me.tpgIPDMemberConsumptionDetails.Size = New System.Drawing.Size(1018, 475)
        Me.tpgIPDMemberConsumptionDetails.TabIndex = 10
        Me.tpgIPDMemberConsumptionDetails.Text = "IPD Member Consumption Details"
        Me.tpgIPDMemberConsumptionDetails.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.Location = New System.Drawing.Point(335, 440)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(121, 22)
        Me.Label7.TabIndex = 84
        Me.Label7.Text = "Total In Words"
        '
        'stbIPDmemberConsumptionWords
        '
        Me.stbIPDmemberConsumptionWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbIPDmemberConsumptionWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbIPDmemberConsumptionWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbIPDmemberConsumptionWords.CapitalizeFirstLetter = True
        Me.stbIPDmemberConsumptionWords.Enabled = False
        Me.stbIPDmemberConsumptionWords.EntryErrorMSG = ""
        Me.stbIPDmemberConsumptionWords.Location = New System.Drawing.Point(463, 429)
        Me.stbIPDmemberConsumptionWords.MaxLength = 20
        Me.stbIPDmemberConsumptionWords.Multiline = True
        Me.stbIPDmemberConsumptionWords.Name = "stbIPDmemberConsumptionWords"
        Me.stbIPDmemberConsumptionWords.RegularExpression = ""
        Me.stbIPDmemberConsumptionWords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbIPDmemberConsumptionWords.Size = New System.Drawing.Size(411, 40)
        Me.stbIPDmemberConsumptionWords.TabIndex = 85
        Me.stbIPDmemberConsumptionWords.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label8.Location = New System.Drawing.Point(11, 440)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(57, 20)
        Me.Label8.TabIndex = 82
        Me.Label8.Text = "Total"
        '
        'stbIPDMemberConsumptions
        '
        Me.stbIPDMemberConsumptions.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbIPDMemberConsumptions.BackColor = System.Drawing.SystemColors.Info
        Me.stbIPDMemberConsumptions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbIPDMemberConsumptions.CapitalizeFirstLetter = False
        Me.stbIPDMemberConsumptions.Enabled = False
        Me.stbIPDMemberConsumptions.EntryErrorMSG = ""
        Me.stbIPDMemberConsumptions.Location = New System.Drawing.Point(74, 438)
        Me.stbIPDMemberConsumptions.MaxLength = 20
        Me.stbIPDMemberConsumptions.Name = "stbIPDMemberConsumptions"
        Me.stbIPDMemberConsumptions.RegularExpression = ""
        Me.stbIPDMemberConsumptions.Size = New System.Drawing.Size(243, 20)
        Me.stbIPDMemberConsumptions.TabIndex = 83
        Me.stbIPDMemberConsumptions.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboIPDfullMainMemberNo
        '
        Me.cboIPDfullMainMemberNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboIPDfullMainMemberNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboIPDfullMainMemberNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboIPDfullMainMemberNo.DropDownWidth = 256
        Me.cboIPDfullMainMemberNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboIPDfullMainMemberNo.FormattingEnabled = True
        Me.cboIPDfullMainMemberNo.ItemHeight = 13
        Me.cboIPDfullMainMemberNo.Location = New System.Drawing.Point(147, 6)
        Me.cboIPDfullMainMemberNo.Name = "cboIPDfullMainMemberNo"
        Me.cboIPDfullMainMemberNo.Size = New System.Drawing.Size(173, 21)
        Me.cboIPDfullMainMemberNo.TabIndex = 81
        '
        'stbIPDMainMemberName
        '
        Me.stbIPDMainMemberName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbIPDMainMemberName.CapitalizeFirstLetter = False
        Me.stbIPDMainMemberName.EntryErrorMSG = ""
        Me.stbIPDMainMemberName.Location = New System.Drawing.Point(145, 30)
        Me.stbIPDMainMemberName.MaxLength = 60
        Me.stbIPDMainMemberName.Multiline = True
        Me.stbIPDMainMemberName.Name = "stbIPDMainMemberName"
        Me.stbIPDMainMemberName.ReadOnly = True
        Me.stbIPDMainMemberName.RegularExpression = ""
        Me.stbIPDMainMemberName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbIPDMainMemberName.Size = New System.Drawing.Size(172, 39)
        Me.stbIPDMainMemberName.TabIndex = 78
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(9, 32)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(130, 20)
        Me.Label9.TabIndex = 77
        Me.Label9.Text = "Main Member Name"
        '
        'StbIPDCompanyName
        '
        Me.StbIPDCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.StbIPDCompanyName.CapitalizeFirstLetter = False
        Me.StbIPDCompanyName.EntryErrorMSG = ""
        Me.StbIPDCompanyName.Location = New System.Drawing.Point(462, 8)
        Me.StbIPDCompanyName.MaxLength = 41
        Me.StbIPDCompanyName.Multiline = True
        Me.StbIPDCompanyName.Name = "StbIPDCompanyName"
        Me.StbIPDCompanyName.ReadOnly = True
        Me.StbIPDCompanyName.RegularExpression = ""
        Me.StbIPDCompanyName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.StbIPDCompanyName.Size = New System.Drawing.Size(218, 61)
        Me.StbIPDCompanyName.TabIndex = 80
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(326, 10)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(130, 20)
        Me.Label10.TabIndex = 79
        Me.Label10.Text = "Company Name"
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(11, 8)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(130, 20)
        Me.Label13.TabIndex = 76
        Me.Label13.Text = "Main Member No"
        '
        'dgvMemberfullIPDConsumptionDetails
        '
        Me.dgvMemberfullIPDConsumptionDetails.AllowUserToAddRows = False
        Me.dgvMemberfullIPDConsumptionDetails.AllowUserToDeleteRows = False
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle9.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvMemberfullIPDConsumptionDetails.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle9
        Me.dgvMemberfullIPDConsumptionDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvMemberfullIPDConsumptionDetails.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvMemberfullIPDConsumptionDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvMemberfullIPDConsumptionDetails.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvMemberfullIPDConsumptionDetails.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMemberfullIPDConsumptionDetails.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvMemberfullIPDConsumptionDetails.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIPDMembertype, Me.colIPDMedicalCardNo, Me.ColIPDInvoiceNo, Me.colIPDFullName, Me.ColIPDVisitNo, Me.ColIPDItemName, Me.ColIPDQuantity, Me.colIPDUnitPrice, Me.ColIPDAmount})
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvMemberfullIPDConsumptionDetails.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgvMemberfullIPDConsumptionDetails.GridColor = System.Drawing.Color.Khaki
        Me.dgvMemberfullIPDConsumptionDetails.Location = New System.Drawing.Point(5, 75)
        Me.dgvMemberfullIPDConsumptionDetails.Name = "dgvMemberfullIPDConsumptionDetails"
        Me.dgvMemberfullIPDConsumptionDetails.ReadOnly = True
        Me.dgvMemberfullIPDConsumptionDetails.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMemberfullIPDConsumptionDetails.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvMemberfullIPDConsumptionDetails.RowHeadersVisible = False
        Me.dgvMemberfullIPDConsumptionDetails.Size = New System.Drawing.Size(1009, 348)
        Me.dgvMemberfullIPDConsumptionDetails.TabIndex = 75
        Me.dgvMemberfullIPDConsumptionDetails.Text = "DataGridView1"
        '
        'colIPDMembertype
        '
        Me.colIPDMembertype.DataPropertyName = "Membertype"
        Me.colIPDMembertype.HeaderText = "Member Type"
        Me.colIPDMembertype.Name = "colIPDMembertype"
        Me.colIPDMembertype.ReadOnly = True
        '
        'colIPDMedicalCardNo
        '
        Me.colIPDMedicalCardNo.DataPropertyName = "MedicalCardNo"
        Me.colIPDMedicalCardNo.HeaderText = "Medical Card No"
        Me.colIPDMedicalCardNo.Name = "colIPDMedicalCardNo"
        Me.colIPDMedicalCardNo.ReadOnly = True
        '
        'ColIPDInvoiceNo
        '
        Me.ColIPDInvoiceNo.DataPropertyName = "ExtraBillNo"
        Me.ColIPDInvoiceNo.HeaderText = "Extra Bill No"
        Me.ColIPDInvoiceNo.Name = "ColIPDInvoiceNo"
        Me.ColIPDInvoiceNo.ReadOnly = True
        '
        'colIPDFullName
        '
        Me.colIPDFullName.DataPropertyName = "FullName"
        Me.colIPDFullName.HeaderText = "Full Name"
        Me.colIPDFullName.Name = "colIPDFullName"
        Me.colIPDFullName.ReadOnly = True
        '
        'ColIPDVisitNo
        '
        Me.ColIPDVisitNo.DataPropertyName = "VisitNo"
        Me.ColIPDVisitNo.HeaderText = "Visit No"
        Me.ColIPDVisitNo.Name = "ColIPDVisitNo"
        Me.ColIPDVisitNo.ReadOnly = True
        '
        'ColIPDItemName
        '
        Me.ColIPDItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColIPDItemName.DataPropertyName = "ItemName"
        Me.ColIPDItemName.HeaderText = "Item Name"
        Me.ColIPDItemName.Name = "ColIPDItemName"
        Me.ColIPDItemName.ReadOnly = True
        '
        'ColIPDQuantity
        '
        Me.ColIPDQuantity.DataPropertyName = "Quantity"
        Me.ColIPDQuantity.HeaderText = "Quantity"
        Me.ColIPDQuantity.Name = "ColIPDQuantity"
        Me.ColIPDQuantity.ReadOnly = True
        '
        'colIPDUnitPrice
        '
        Me.colIPDUnitPrice.DataPropertyName = "UnitPrice"
        Me.colIPDUnitPrice.HeaderText = "Unit Price"
        Me.colIPDUnitPrice.Name = "colIPDUnitPrice"
        Me.colIPDUnitPrice.ReadOnly = True
        '
        'ColIPDAmount
        '
        Me.ColIPDAmount.DataPropertyName = "Amount"
        Me.ColIPDAmount.HeaderText = "Amount"
        Me.ColIPDAmount.Name = "ColIPDAmount"
        Me.ColIPDAmount.ReadOnly = True
        '
        'tpgGeneralConsumption
        '
        Me.tpgGeneralConsumption.Controls.Add(Me.lblTotalPremium)
        Me.tpgGeneralConsumption.Controls.Add(Me.stbTotalPremium)
        Me.tpgGeneralConsumption.Controls.Add(Me.lbltotalConsumptionWorfs)
        Me.tpgGeneralConsumption.Controls.Add(Me.stbtotalConsumptionWords)
        Me.tpgGeneralConsumption.Controls.Add(Me.lblTotalConsumption)
        Me.tpgGeneralConsumption.Controls.Add(Me.stbtotalConsumption)
        Me.tpgGeneralConsumption.Controls.Add(Me.dgvGeneralConsumption)
        Me.tpgGeneralConsumption.Controls.Add(Me.stbGeneralCompanyName)
        Me.tpgGeneralConsumption.Controls.Add(Me.cboGeneralCompanyNo)
        Me.tpgGeneralConsumption.Controls.Add(Me.Label14)
        Me.tpgGeneralConsumption.Controls.Add(Me.Label15)
        Me.tpgGeneralConsumption.Controls.Add(Me.stbGeneralInsuranceName)
        Me.tpgGeneralConsumption.Controls.Add(Me.cboGeneralInsuranceNo)
        Me.tpgGeneralConsumption.Controls.Add(Me.Label16)
        Me.tpgGeneralConsumption.Controls.Add(Me.Label17)
        Me.tpgGeneralConsumption.Location = New System.Drawing.Point(4, 22)
        Me.tpgGeneralConsumption.Name = "tpgGeneralConsumption"
        Me.tpgGeneralConsumption.Size = New System.Drawing.Size(1018, 475)
        Me.tpgGeneralConsumption.TabIndex = 11
        Me.tpgGeneralConsumption.Text = "General OPD Consumption"
        Me.tpgGeneralConsumption.UseVisualStyleBackColor = True
        '
        'lblTotalPremium
        '
        Me.lblTotalPremium.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalPremium.Location = New System.Drawing.Point(9, 430)
        Me.lblTotalPremium.Name = "lblTotalPremium"
        Me.lblTotalPremium.Size = New System.Drawing.Size(121, 20)
        Me.lblTotalPremium.TabIndex = 79
        Me.lblTotalPremium.Text = "Total Premium"
        '
        'stbTotalPremium
        '
        Me.stbTotalPremium.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbTotalPremium.BackColor = System.Drawing.SystemColors.Info
        Me.stbTotalPremium.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalPremium.CapitalizeFirstLetter = False
        Me.stbTotalPremium.Enabled = False
        Me.stbTotalPremium.EntryErrorMSG = ""
        Me.stbTotalPremium.Location = New System.Drawing.Point(148, 428)
        Me.stbTotalPremium.MaxLength = 20
        Me.stbTotalPremium.Name = "stbTotalPremium"
        Me.stbTotalPremium.RegularExpression = ""
        Me.stbTotalPremium.Size = New System.Drawing.Size(243, 20)
        Me.stbTotalPremium.TabIndex = 80
        Me.stbTotalPremium.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lbltotalConsumptionWorfs
        '
        Me.lbltotalConsumptionWorfs.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lbltotalConsumptionWorfs.Location = New System.Drawing.Point(448, 444)
        Me.lbltotalConsumptionWorfs.Name = "lbltotalConsumptionWorfs"
        Me.lbltotalConsumptionWorfs.Size = New System.Drawing.Size(121, 22)
        Me.lbltotalConsumptionWorfs.TabIndex = 77
        Me.lbltotalConsumptionWorfs.Text = "Total In Words"
        '
        'stbtotalConsumptionWords
        '
        Me.stbtotalConsumptionWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbtotalConsumptionWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbtotalConsumptionWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbtotalConsumptionWords.CapitalizeFirstLetter = True
        Me.stbtotalConsumptionWords.Enabled = False
        Me.stbtotalConsumptionWords.EntryErrorMSG = ""
        Me.stbtotalConsumptionWords.Location = New System.Drawing.Point(575, 430)
        Me.stbtotalConsumptionWords.MaxLength = 20
        Me.stbtotalConsumptionWords.Multiline = True
        Me.stbtotalConsumptionWords.Name = "stbtotalConsumptionWords"
        Me.stbtotalConsumptionWords.RegularExpression = ""
        Me.stbtotalConsumptionWords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbtotalConsumptionWords.Size = New System.Drawing.Size(411, 40)
        Me.stbtotalConsumptionWords.TabIndex = 78
        Me.stbtotalConsumptionWords.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotalConsumption
        '
        Me.lblTotalConsumption.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalConsumption.Location = New System.Drawing.Point(10, 453)
        Me.lblTotalConsumption.Name = "lblTotalConsumption"
        Me.lblTotalConsumption.Size = New System.Drawing.Size(121, 20)
        Me.lblTotalConsumption.TabIndex = 75
        Me.lblTotalConsumption.Text = "Total Consumption"
        '
        'stbtotalConsumption
        '
        Me.stbtotalConsumption.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbtotalConsumption.BackColor = System.Drawing.SystemColors.Info
        Me.stbtotalConsumption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbtotalConsumption.CapitalizeFirstLetter = False
        Me.stbtotalConsumption.Enabled = False
        Me.stbtotalConsumption.EntryErrorMSG = ""
        Me.stbtotalConsumption.Location = New System.Drawing.Point(149, 451)
        Me.stbtotalConsumption.MaxLength = 20
        Me.stbtotalConsumption.Name = "stbtotalConsumption"
        Me.stbtotalConsumption.RegularExpression = ""
        Me.stbtotalConsumption.Size = New System.Drawing.Size(243, 20)
        Me.stbtotalConsumption.TabIndex = 76
        Me.stbtotalConsumption.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgvGeneralConsumption
        '
        Me.dgvGeneralConsumption.AllowUserToAddRows = False
        Me.dgvGeneralConsumption.AllowUserToDeleteRows = False
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle13.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvGeneralConsumption.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvGeneralConsumption.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvGeneralConsumption.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvGeneralConsumption.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvGeneralConsumption.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvGeneralConsumption.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle14.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGeneralConsumption.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.dgvGeneralConsumption.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColGeneralMainMemberNo, Me.ColGeneralConsumptionMainMemberName, Me.ColGeneralMedicalCardNo, Me.ColGeneralMemberType, Me.colGeneralFullName, Me.colGeneralPatientNo, Me.ColGeneralInsuranceName, Me.ColGeneralCompanyName, Me.ColGeneralPolicyName, Me.ColGeneralMemberPremium, Me.ColGeneralTotalamount, Me.ColGeneralOPDConsumptionBalance, Me.ColGeneralPolicyStartDate, Me.ColGeneralPolicyEndDate})
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvGeneralConsumption.DefaultCellStyle = DataGridViewCellStyle15
        Me.dgvGeneralConsumption.GridColor = System.Drawing.Color.Khaki
        Me.dgvGeneralConsumption.Location = New System.Drawing.Point(3, 62)
        Me.dgvGeneralConsumption.Name = "dgvGeneralConsumption"
        Me.dgvGeneralConsumption.ReadOnly = True
        Me.dgvGeneralConsumption.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle16.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGeneralConsumption.RowHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.dgvGeneralConsumption.RowHeadersVisible = False
        Me.dgvGeneralConsumption.Size = New System.Drawing.Size(1010, 354)
        Me.dgvGeneralConsumption.TabIndex = 41
        Me.dgvGeneralConsumption.Text = "DataGridView1"
        '
        'stbGeneralCompanyName
        '
        Me.stbGeneralCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbGeneralCompanyName.CapitalizeFirstLetter = True
        Me.stbGeneralCompanyName.EntryErrorMSG = ""
        Me.stbGeneralCompanyName.Location = New System.Drawing.Point(475, 26)
        Me.stbGeneralCompanyName.MaxLength = 60
        Me.stbGeneralCompanyName.Multiline = True
        Me.stbGeneralCompanyName.Name = "stbGeneralCompanyName"
        Me.stbGeneralCompanyName.ReadOnly = True
        Me.stbGeneralCompanyName.RegularExpression = ""
        Me.stbGeneralCompanyName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbGeneralCompanyName.Size = New System.Drawing.Size(157, 34)
        Me.stbGeneralCompanyName.TabIndex = 40
        '
        'cboGeneralCompanyNo
        '
        Me.cboGeneralCompanyNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboGeneralCompanyNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboGeneralCompanyNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboGeneralCompanyNo.DropDownWidth = 256
        Me.cboGeneralCompanyNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboGeneralCompanyNo.FormattingEnabled = True
        Me.cboGeneralCompanyNo.ItemHeight = 13
        Me.cboGeneralCompanyNo.Location = New System.Drawing.Point(475, 3)
        Me.cboGeneralCompanyNo.Name = "cboGeneralCompanyNo"
        Me.cboGeneralCompanyNo.Size = New System.Drawing.Size(157, 21)
        Me.cboGeneralCompanyNo.TabIndex = 38
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(326, 33)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(143, 18)
        Me.Label14.TabIndex = 39
        Me.Label14.Text = "Company Name"
        '
        'Label15
        '
        Me.Label15.Location = New System.Drawing.Point(326, 4)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(143, 18)
        Me.Label15.TabIndex = 37
        Me.Label15.Text = "Company Number"
        '
        'stbGeneralInsuranceName
        '
        Me.stbGeneralInsuranceName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbGeneralInsuranceName.CapitalizeFirstLetter = False
        Me.stbGeneralInsuranceName.EntryErrorMSG = ""
        Me.stbGeneralInsuranceName.Location = New System.Drawing.Point(163, 26)
        Me.stbGeneralInsuranceName.MaxLength = 41
        Me.stbGeneralInsuranceName.Multiline = True
        Me.stbGeneralInsuranceName.Name = "stbGeneralInsuranceName"
        Me.stbGeneralInsuranceName.ReadOnly = True
        Me.stbGeneralInsuranceName.RegularExpression = ""
        Me.stbGeneralInsuranceName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbGeneralInsuranceName.Size = New System.Drawing.Size(157, 34)
        Me.stbGeneralInsuranceName.TabIndex = 36
        '
        'cboGeneralInsuranceNo
        '
        Me.cboGeneralInsuranceNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboGeneralInsuranceNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboGeneralInsuranceNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboGeneralInsuranceNo.DropDownWidth = 276
        Me.cboGeneralInsuranceNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboGeneralInsuranceNo.FormattingEnabled = True
        Me.cboGeneralInsuranceNo.ItemHeight = 13
        Me.cboGeneralInsuranceNo.Location = New System.Drawing.Point(163, 4)
        Me.cboGeneralInsuranceNo.Name = "cboGeneralInsuranceNo"
        Me.cboGeneralInsuranceNo.Size = New System.Drawing.Size(157, 21)
        Me.cboGeneralInsuranceNo.TabIndex = 34
        '
        'Label16
        '
        Me.Label16.Location = New System.Drawing.Point(5, 33)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(152, 18)
        Me.Label16.TabIndex = 35
        Me.Label16.Text = "Insurance Name"
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(5, 7)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(152, 18)
        Me.Label17.TabIndex = 33
        Me.Label17.Text = "Insurance Number"
        '
        'tpgGeneralIPDConsumption
        '
        Me.tpgGeneralIPDConsumption.Controls.Add(Me.Label18)
        Me.tpgGeneralIPDConsumption.Controls.Add(Me.stbIPDTotalPremium)
        Me.tpgGeneralIPDConsumption.Controls.Add(Me.Label19)
        Me.tpgGeneralIPDConsumption.Controls.Add(Me.stbIPDtotalConsumptionWords)
        Me.tpgGeneralIPDConsumption.Controls.Add(Me.Label20)
        Me.tpgGeneralIPDConsumption.Controls.Add(Me.stbIPDtotalConsumption)
        Me.tpgGeneralIPDConsumption.Controls.Add(Me.dgvGeneralIPDConsumption)
        Me.tpgGeneralIPDConsumption.Controls.Add(Me.stbGeneralIPDCompanyName)
        Me.tpgGeneralIPDConsumption.Controls.Add(Me.cboGeneralIPDCompanyNo)
        Me.tpgGeneralIPDConsumption.Controls.Add(Me.lblGeneralIPDCompanyName)
        Me.tpgGeneralIPDConsumption.Controls.Add(Me.lblGeneralIPDCompanyNo)
        Me.tpgGeneralIPDConsumption.Controls.Add(Me.stbGeneralIPDInsuranceName)
        Me.tpgGeneralIPDConsumption.Controls.Add(Me.cboGeneralIPDInsuranceNo)
        Me.tpgGeneralIPDConsumption.Controls.Add(Me.lblGeneralIPDInsuranceName)
        Me.tpgGeneralIPDConsumption.Controls.Add(Me.lblGeneralIPDInsuranceNo)
        Me.tpgGeneralIPDConsumption.Location = New System.Drawing.Point(4, 22)
        Me.tpgGeneralIPDConsumption.Name = "tpgGeneralIPDConsumption"
        Me.tpgGeneralIPDConsumption.Size = New System.Drawing.Size(1018, 475)
        Me.tpgGeneralIPDConsumption.TabIndex = 12
        Me.tpgGeneralIPDConsumption.Text = "General IPD Consumption"
        Me.tpgGeneralIPDConsumption.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label18.Location = New System.Drawing.Point(10, 429)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(121, 20)
        Me.Label18.TabIndex = 94
        Me.Label18.Text = "Total Premium"
        '
        'stbIPDTotalPremium
        '
        Me.stbIPDTotalPremium.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbIPDTotalPremium.BackColor = System.Drawing.SystemColors.Info
        Me.stbIPDTotalPremium.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbIPDTotalPremium.CapitalizeFirstLetter = False
        Me.stbIPDTotalPremium.Enabled = False
        Me.stbIPDTotalPremium.EntryErrorMSG = ""
        Me.stbIPDTotalPremium.Location = New System.Drawing.Point(149, 427)
        Me.stbIPDTotalPremium.MaxLength = 20
        Me.stbIPDTotalPremium.Name = "stbIPDTotalPremium"
        Me.stbIPDTotalPremium.RegularExpression = ""
        Me.stbIPDTotalPremium.Size = New System.Drawing.Size(243, 20)
        Me.stbIPDTotalPremium.TabIndex = 95
        Me.stbIPDTotalPremium.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label19
        '
        Me.Label19.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label19.Location = New System.Drawing.Point(449, 443)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(121, 22)
        Me.Label19.TabIndex = 92
        Me.Label19.Text = "Total In Words"
        '
        'stbIPDtotalConsumptionWords
        '
        Me.stbIPDtotalConsumptionWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbIPDtotalConsumptionWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbIPDtotalConsumptionWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbIPDtotalConsumptionWords.CapitalizeFirstLetter = True
        Me.stbIPDtotalConsumptionWords.Enabled = False
        Me.stbIPDtotalConsumptionWords.EntryErrorMSG = ""
        Me.stbIPDtotalConsumptionWords.Location = New System.Drawing.Point(576, 429)
        Me.stbIPDtotalConsumptionWords.MaxLength = 20
        Me.stbIPDtotalConsumptionWords.Multiline = True
        Me.stbIPDtotalConsumptionWords.Name = "stbIPDtotalConsumptionWords"
        Me.stbIPDtotalConsumptionWords.RegularExpression = ""
        Me.stbIPDtotalConsumptionWords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbIPDtotalConsumptionWords.Size = New System.Drawing.Size(411, 40)
        Me.stbIPDtotalConsumptionWords.TabIndex = 93
        Me.stbIPDtotalConsumptionWords.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label20
        '
        Me.Label20.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label20.Location = New System.Drawing.Point(11, 452)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(121, 20)
        Me.Label20.TabIndex = 90
        Me.Label20.Text = "Total Consumption"
        '
        'stbIPDtotalConsumption
        '
        Me.stbIPDtotalConsumption.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbIPDtotalConsumption.BackColor = System.Drawing.SystemColors.Info
        Me.stbIPDtotalConsumption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbIPDtotalConsumption.CapitalizeFirstLetter = False
        Me.stbIPDtotalConsumption.Enabled = False
        Me.stbIPDtotalConsumption.EntryErrorMSG = ""
        Me.stbIPDtotalConsumption.Location = New System.Drawing.Point(150, 450)
        Me.stbIPDtotalConsumption.MaxLength = 20
        Me.stbIPDtotalConsumption.Name = "stbIPDtotalConsumption"
        Me.stbIPDtotalConsumption.RegularExpression = ""
        Me.stbIPDtotalConsumption.Size = New System.Drawing.Size(243, 20)
        Me.stbIPDtotalConsumption.TabIndex = 91
        Me.stbIPDtotalConsumption.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgvGeneralIPDConsumption
        '
        Me.dgvGeneralIPDConsumption.AllowUserToAddRows = False
        Me.dgvGeneralIPDConsumption.AllowUserToDeleteRows = False
        DataGridViewCellStyle17.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle17.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvGeneralIPDConsumption.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle17
        Me.dgvGeneralIPDConsumption.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvGeneralIPDConsumption.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvGeneralIPDConsumption.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvGeneralIPDConsumption.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvGeneralIPDConsumption.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle18.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGeneralIPDConsumption.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.dgvGeneralIPDConsumption.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colGeneralIPDFullName, Me.colGeneralIPDPatientNo, Me.ColGeneralIPDInsuranceName, Me.ColGeneralIPDMedicalCardNo, Me.ColIPDGeneralMemberType, Me.colGeneralIPDCompanyName, Me.ColGeneralIPDPolicyName, Me.ColGeneralIPDMemberPremium, Me.CoGeneralIPDTotalamount, Me.colGeneralIPDPolicyStartDate, Me.colGeneralIPDPolicyEndDate, Me.colGeneralIPDMainMemberNo})
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvGeneralIPDConsumption.DefaultCellStyle = DataGridViewCellStyle19
        Me.dgvGeneralIPDConsumption.GridColor = System.Drawing.Color.Khaki
        Me.dgvGeneralIPDConsumption.Location = New System.Drawing.Point(4, 61)
        Me.dgvGeneralIPDConsumption.Name = "dgvGeneralIPDConsumption"
        Me.dgvGeneralIPDConsumption.ReadOnly = True
        Me.dgvGeneralIPDConsumption.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle20.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGeneralIPDConsumption.RowHeadersDefaultCellStyle = DataGridViewCellStyle20
        Me.dgvGeneralIPDConsumption.RowHeadersVisible = False
        Me.dgvGeneralIPDConsumption.Size = New System.Drawing.Size(1010, 354)
        Me.dgvGeneralIPDConsumption.TabIndex = 89
        Me.dgvGeneralIPDConsumption.Text = "DataGridView1"
        '
        'colGeneralIPDFullName
        '
        Me.colGeneralIPDFullName.DataPropertyName = "FullName"
        Me.colGeneralIPDFullName.HeaderText = "Full Name"
        Me.colGeneralIPDFullName.Name = "colGeneralIPDFullName"
        Me.colGeneralIPDFullName.ReadOnly = True
        Me.colGeneralIPDFullName.Width = 200
        '
        'colGeneralIPDPatientNo
        '
        Me.colGeneralIPDPatientNo.DataPropertyName = "PatientNo"
        Me.colGeneralIPDPatientNo.HeaderText = "Patient No"
        Me.colGeneralIPDPatientNo.Name = "colGeneralIPDPatientNo"
        Me.colGeneralIPDPatientNo.ReadOnly = True
        '
        'ColGeneralIPDInsuranceName
        '
        Me.ColGeneralIPDInsuranceName.DataPropertyName = "InsuranceName"
        Me.ColGeneralIPDInsuranceName.HeaderText = "Insurance Name"
        Me.ColGeneralIPDInsuranceName.Name = "ColGeneralIPDInsuranceName"
        Me.ColGeneralIPDInsuranceName.ReadOnly = True
        '
        'ColGeneralIPDMedicalCardNo
        '
        Me.ColGeneralIPDMedicalCardNo.DataPropertyName = "MedicalCardNo"
        Me.ColGeneralIPDMedicalCardNo.HeaderText = "Medical Card No"
        Me.ColGeneralIPDMedicalCardNo.Name = "ColGeneralIPDMedicalCardNo"
        Me.ColGeneralIPDMedicalCardNo.ReadOnly = True
        '
        'ColIPDGeneralMemberType
        '
        Me.ColIPDGeneralMemberType.DataPropertyName = "MemberType"
        Me.ColIPDGeneralMemberType.HeaderText = "Member Type"
        Me.ColIPDGeneralMemberType.Name = "ColIPDGeneralMemberType"
        Me.ColIPDGeneralMemberType.ReadOnly = True
        '
        'colGeneralIPDCompanyName
        '
        Me.colGeneralIPDCompanyName.DataPropertyName = "CompanyName"
        Me.colGeneralIPDCompanyName.HeaderText = "Company Name"
        Me.colGeneralIPDCompanyName.Name = "colGeneralIPDCompanyName"
        Me.colGeneralIPDCompanyName.ReadOnly = True
        '
        'ColGeneralIPDPolicyName
        '
        Me.ColGeneralIPDPolicyName.DataPropertyName = "PolicyName"
        Me.ColGeneralIPDPolicyName.HeaderText = "Policy Name"
        Me.ColGeneralIPDPolicyName.Name = "ColGeneralIPDPolicyName"
        Me.ColGeneralIPDPolicyName.ReadOnly = True
        '
        'ColGeneralIPDMemberPremium
        '
        Me.ColGeneralIPDMemberPremium.DataPropertyName = "MemberPremium"
        Me.ColGeneralIPDMemberPremium.HeaderText = "Member Premium"
        Me.ColGeneralIPDMemberPremium.Name = "ColGeneralIPDMemberPremium"
        Me.ColGeneralIPDMemberPremium.ReadOnly = True
        '
        'CoGeneralIPDTotalamount
        '
        Me.CoGeneralIPDTotalamount.DataPropertyName = "Totalamount"
        Me.CoGeneralIPDTotalamount.HeaderText = "Total Amount"
        Me.CoGeneralIPDTotalamount.Name = "CoGeneralIPDTotalamount"
        Me.CoGeneralIPDTotalamount.ReadOnly = True
        '
        'colGeneralIPDPolicyStartDate
        '
        Me.colGeneralIPDPolicyStartDate.DataPropertyName = "PolicyStartDate"
        Me.colGeneralIPDPolicyStartDate.HeaderText = "Policy Start Date"
        Me.colGeneralIPDPolicyStartDate.Name = "colGeneralIPDPolicyStartDate"
        Me.colGeneralIPDPolicyStartDate.ReadOnly = True
        '
        'colGeneralIPDPolicyEndDate
        '
        Me.colGeneralIPDPolicyEndDate.DataPropertyName = "PolicyEndDate"
        Me.colGeneralIPDPolicyEndDate.HeaderText = "Policy End Date"
        Me.colGeneralIPDPolicyEndDate.Name = "colGeneralIPDPolicyEndDate"
        Me.colGeneralIPDPolicyEndDate.ReadOnly = True
        '
        'colGeneralIPDMainMemberNo
        '
        Me.colGeneralIPDMainMemberNo.DataPropertyName = "MainMemberNo"
        Me.colGeneralIPDMainMemberNo.HeaderText = "Main Member No"
        Me.colGeneralIPDMainMemberNo.Name = "colGeneralIPDMainMemberNo"
        Me.colGeneralIPDMainMemberNo.ReadOnly = True
        '
        'stbGeneralIPDCompanyName
        '
        Me.stbGeneralIPDCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbGeneralIPDCompanyName.CapitalizeFirstLetter = True
        Me.stbGeneralIPDCompanyName.EntryErrorMSG = ""
        Me.stbGeneralIPDCompanyName.Location = New System.Drawing.Point(476, 25)
        Me.stbGeneralIPDCompanyName.MaxLength = 60
        Me.stbGeneralIPDCompanyName.Multiline = True
        Me.stbGeneralIPDCompanyName.Name = "stbGeneralIPDCompanyName"
        Me.stbGeneralIPDCompanyName.ReadOnly = True
        Me.stbGeneralIPDCompanyName.RegularExpression = ""
        Me.stbGeneralIPDCompanyName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbGeneralIPDCompanyName.Size = New System.Drawing.Size(157, 34)
        Me.stbGeneralIPDCompanyName.TabIndex = 88
        '
        'cboGeneralIPDCompanyNo
        '
        Me.cboGeneralIPDCompanyNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboGeneralIPDCompanyNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboGeneralIPDCompanyNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboGeneralIPDCompanyNo.DropDownWidth = 256
        Me.cboGeneralIPDCompanyNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboGeneralIPDCompanyNo.FormattingEnabled = True
        Me.cboGeneralIPDCompanyNo.ItemHeight = 13
        Me.cboGeneralIPDCompanyNo.Location = New System.Drawing.Point(476, 2)
        Me.cboGeneralIPDCompanyNo.Name = "cboGeneralIPDCompanyNo"
        Me.cboGeneralIPDCompanyNo.Size = New System.Drawing.Size(157, 21)
        Me.cboGeneralIPDCompanyNo.TabIndex = 86
        '
        'lblGeneralIPDCompanyName
        '
        Me.lblGeneralIPDCompanyName.Location = New System.Drawing.Point(327, 32)
        Me.lblGeneralIPDCompanyName.Name = "lblGeneralIPDCompanyName"
        Me.lblGeneralIPDCompanyName.Size = New System.Drawing.Size(143, 18)
        Me.lblGeneralIPDCompanyName.TabIndex = 87
        Me.lblGeneralIPDCompanyName.Text = "Company Name"
        '
        'lblGeneralIPDCompanyNo
        '
        Me.lblGeneralIPDCompanyNo.Location = New System.Drawing.Point(327, 3)
        Me.lblGeneralIPDCompanyNo.Name = "lblGeneralIPDCompanyNo"
        Me.lblGeneralIPDCompanyNo.Size = New System.Drawing.Size(143, 18)
        Me.lblGeneralIPDCompanyNo.TabIndex = 85
        Me.lblGeneralIPDCompanyNo.Text = "Company Number"
        '
        'stbGeneralIPDInsuranceName
        '
        Me.stbGeneralIPDInsuranceName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbGeneralIPDInsuranceName.CapitalizeFirstLetter = False
        Me.stbGeneralIPDInsuranceName.EntryErrorMSG = ""
        Me.stbGeneralIPDInsuranceName.Location = New System.Drawing.Point(164, 25)
        Me.stbGeneralIPDInsuranceName.MaxLength = 41
        Me.stbGeneralIPDInsuranceName.Multiline = True
        Me.stbGeneralIPDInsuranceName.Name = "stbGeneralIPDInsuranceName"
        Me.stbGeneralIPDInsuranceName.ReadOnly = True
        Me.stbGeneralIPDInsuranceName.RegularExpression = ""
        Me.stbGeneralIPDInsuranceName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbGeneralIPDInsuranceName.Size = New System.Drawing.Size(157, 34)
        Me.stbGeneralIPDInsuranceName.TabIndex = 84
        '
        'cboGeneralIPDInsuranceNo
        '
        Me.cboGeneralIPDInsuranceNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboGeneralIPDInsuranceNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboGeneralIPDInsuranceNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboGeneralIPDInsuranceNo.DropDownWidth = 276
        Me.cboGeneralIPDInsuranceNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboGeneralIPDInsuranceNo.FormattingEnabled = True
        Me.cboGeneralIPDInsuranceNo.ItemHeight = 13
        Me.cboGeneralIPDInsuranceNo.Location = New System.Drawing.Point(164, 3)
        Me.cboGeneralIPDInsuranceNo.Name = "cboGeneralIPDInsuranceNo"
        Me.cboGeneralIPDInsuranceNo.Size = New System.Drawing.Size(157, 21)
        Me.cboGeneralIPDInsuranceNo.TabIndex = 82
        '
        'lblGeneralIPDInsuranceName
        '
        Me.lblGeneralIPDInsuranceName.Location = New System.Drawing.Point(6, 32)
        Me.lblGeneralIPDInsuranceName.Name = "lblGeneralIPDInsuranceName"
        Me.lblGeneralIPDInsuranceName.Size = New System.Drawing.Size(152, 18)
        Me.lblGeneralIPDInsuranceName.TabIndex = 83
        Me.lblGeneralIPDInsuranceName.Text = "Insurance Name"
        '
        'lblGeneralIPDInsuranceNo
        '
        Me.lblGeneralIPDInsuranceNo.Location = New System.Drawing.Point(6, 6)
        Me.lblGeneralIPDInsuranceNo.Name = "lblGeneralIPDInsuranceNo"
        Me.lblGeneralIPDInsuranceNo.Size = New System.Drawing.Size(152, 18)
        Me.lblGeneralIPDInsuranceNo.TabIndex = 81
        Me.lblGeneralIPDInsuranceNo.Text = "Insurance Number"
        '
        'tpgOverConsumption
        '
        Me.tpgOverConsumption.Controls.Add(Me.dgvOverConsumption)
        Me.tpgOverConsumption.Controls.Add(Me.stbOVOPDCompanyName)
        Me.tpgOverConsumption.Controls.Add(Me.cboOVCompanyNo)
        Me.tpgOverConsumption.Controls.Add(Me.lblOVOPDCompanyName)
        Me.tpgOverConsumption.Controls.Add(Me.lblOVCompanyNo)
        Me.tpgOverConsumption.Controls.Add(Me.stbOVInsuranceName)
        Me.tpgOverConsumption.Controls.Add(Me.cboOVInsuranceNo)
        Me.tpgOverConsumption.Controls.Add(Me.lblOVInsuranceName)
        Me.tpgOverConsumption.Controls.Add(Me.lblOVInsuranceNo)
        Me.tpgOverConsumption.Location = New System.Drawing.Point(4, 22)
        Me.tpgOverConsumption.Name = "tpgOverConsumption"
        Me.tpgOverConsumption.Size = New System.Drawing.Size(1018, 475)
        Me.tpgOverConsumption.TabIndex = 9
        Me.tpgOverConsumption.Text = "Over Consumption"
        Me.tpgOverConsumption.UseVisualStyleBackColor = True
        '
        'dgvOverConsumption
        '
        Me.dgvOverConsumption.AllowUserToAddRows = False
        Me.dgvOverConsumption.AllowUserToDeleteRows = False
        DataGridViewCellStyle21.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle21.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvOverConsumption.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle21
        Me.dgvOverConsumption.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvOverConsumption.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvOverConsumption.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvOverConsumption.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvOverConsumption.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle22.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle22.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOverConsumption.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle22
        Me.dgvOverConsumption.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colOverConsumptionFullName, Me.colOverConsumptionPatientNo, Me.colOverConsumptionInsuranceName, Me.colOverConsumptionMedicalCardNo, Me.colOverConsumptionMemberType, Me.colOverConsumptionCompanyName, Me.colOverConsumedPolicyName, Me.ColOverConsumptionMemberPremium, Me.colConsumedAmount, Me.ColExcess, Me.colOverConsumptionPolicyStartDate, Me.colOverConsumptionPolicyEndDate, Me.ColOverConsumedMainMemberNo})
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvOverConsumption.DefaultCellStyle = DataGridViewCellStyle23
        Me.dgvOverConsumption.GridColor = System.Drawing.Color.Khaki
        Me.dgvOverConsumption.Location = New System.Drawing.Point(4, 61)
        Me.dgvOverConsumption.Name = "dgvOverConsumption"
        Me.dgvOverConsumption.ReadOnly = True
        Me.dgvOverConsumption.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle24.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle24.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOverConsumption.RowHeadersDefaultCellStyle = DataGridViewCellStyle24
        Me.dgvOverConsumption.RowHeadersVisible = False
        Me.dgvOverConsumption.Size = New System.Drawing.Size(1010, 411)
        Me.dgvOverConsumption.TabIndex = 104
        Me.dgvOverConsumption.Text = "DataGridView1"
        '
        'colOverConsumptionFullName
        '
        Me.colOverConsumptionFullName.DataPropertyName = "FullName"
        Me.colOverConsumptionFullName.HeaderText = "Full Name"
        Me.colOverConsumptionFullName.Name = "colOverConsumptionFullName"
        Me.colOverConsumptionFullName.ReadOnly = True
        Me.colOverConsumptionFullName.Width = 200
        '
        'colOverConsumptionPatientNo
        '
        Me.colOverConsumptionPatientNo.DataPropertyName = "PatientNo"
        Me.colOverConsumptionPatientNo.HeaderText = "Patient No"
        Me.colOverConsumptionPatientNo.Name = "colOverConsumptionPatientNo"
        Me.colOverConsumptionPatientNo.ReadOnly = True
        '
        'colOverConsumptionInsuranceName
        '
        Me.colOverConsumptionInsuranceName.DataPropertyName = "InsuranceName"
        Me.colOverConsumptionInsuranceName.HeaderText = "Insurance Name"
        Me.colOverConsumptionInsuranceName.Name = "colOverConsumptionInsuranceName"
        Me.colOverConsumptionInsuranceName.ReadOnly = True
        '
        'colOverConsumptionMedicalCardNo
        '
        Me.colOverConsumptionMedicalCardNo.DataPropertyName = "MedicalCardNo"
        Me.colOverConsumptionMedicalCardNo.HeaderText = "Medical Card No"
        Me.colOverConsumptionMedicalCardNo.Name = "colOverConsumptionMedicalCardNo"
        Me.colOverConsumptionMedicalCardNo.ReadOnly = True
        '
        'colOverConsumptionMemberType
        '
        Me.colOverConsumptionMemberType.DataPropertyName = "MemberType"
        Me.colOverConsumptionMemberType.HeaderText = "Member Type"
        Me.colOverConsumptionMemberType.Name = "colOverConsumptionMemberType"
        Me.colOverConsumptionMemberType.ReadOnly = True
        '
        'colOverConsumptionCompanyName
        '
        Me.colOverConsumptionCompanyName.DataPropertyName = "CompanyName"
        Me.colOverConsumptionCompanyName.HeaderText = "Company Name"
        Me.colOverConsumptionCompanyName.Name = "colOverConsumptionCompanyName"
        Me.colOverConsumptionCompanyName.ReadOnly = True
        '
        'colOverConsumedPolicyName
        '
        Me.colOverConsumedPolicyName.DataPropertyName = "PolicyName"
        Me.colOverConsumedPolicyName.HeaderText = "Policy Name"
        Me.colOverConsumedPolicyName.Name = "colOverConsumedPolicyName"
        Me.colOverConsumedPolicyName.ReadOnly = True
        '
        'ColOverConsumptionMemberPremium
        '
        Me.ColOverConsumptionMemberPremium.DataPropertyName = "MemberPremium"
        Me.ColOverConsumptionMemberPremium.HeaderText = "Member Premium"
        Me.ColOverConsumptionMemberPremium.Name = "ColOverConsumptionMemberPremium"
        Me.ColOverConsumptionMemberPremium.ReadOnly = True
        '
        'colConsumedAmount
        '
        Me.colConsumedAmount.DataPropertyName = "ConsumedAmount"
        Me.colConsumedAmount.HeaderText = "Consumed Amount"
        Me.colConsumedAmount.Name = "colConsumedAmount"
        Me.colConsumedAmount.ReadOnly = True
        '
        'ColExcess
        '
        Me.ColExcess.DataPropertyName = "Balance"
        Me.ColExcess.HeaderText = "Excess Consumption"
        Me.ColExcess.Name = "ColExcess"
        Me.ColExcess.ReadOnly = True
        '
        'colOverConsumptionPolicyStartDate
        '
        Me.colOverConsumptionPolicyStartDate.DataPropertyName = "PolicyStartDate"
        Me.colOverConsumptionPolicyStartDate.HeaderText = "Policy Start Date"
        Me.colOverConsumptionPolicyStartDate.Name = "colOverConsumptionPolicyStartDate"
        Me.colOverConsumptionPolicyStartDate.ReadOnly = True
        '
        'colOverConsumptionPolicyEndDate
        '
        Me.colOverConsumptionPolicyEndDate.DataPropertyName = "PolicyEndDate"
        Me.colOverConsumptionPolicyEndDate.HeaderText = "Policy End Date"
        Me.colOverConsumptionPolicyEndDate.Name = "colOverConsumptionPolicyEndDate"
        Me.colOverConsumptionPolicyEndDate.ReadOnly = True
        '
        'ColOverConsumedMainMemberNo
        '
        Me.ColOverConsumedMainMemberNo.DataPropertyName = "MainMemberNo"
        Me.ColOverConsumedMainMemberNo.HeaderText = "Main Member No"
        Me.ColOverConsumedMainMemberNo.Name = "ColOverConsumedMainMemberNo"
        Me.ColOverConsumedMainMemberNo.ReadOnly = True
        '
        'stbOVOPDCompanyName
        '
        Me.stbOVOPDCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbOVOPDCompanyName.CapitalizeFirstLetter = True
        Me.stbOVOPDCompanyName.EntryErrorMSG = ""
        Me.stbOVOPDCompanyName.Location = New System.Drawing.Point(476, 25)
        Me.stbOVOPDCompanyName.MaxLength = 60
        Me.stbOVOPDCompanyName.Multiline = True
        Me.stbOVOPDCompanyName.Name = "stbOVOPDCompanyName"
        Me.stbOVOPDCompanyName.ReadOnly = True
        Me.stbOVOPDCompanyName.RegularExpression = ""
        Me.stbOVOPDCompanyName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbOVOPDCompanyName.Size = New System.Drawing.Size(157, 34)
        Me.stbOVOPDCompanyName.TabIndex = 103
        '
        'cboOVCompanyNo
        '
        Me.cboOVCompanyNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboOVCompanyNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboOVCompanyNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboOVCompanyNo.DropDownWidth = 256
        Me.cboOVCompanyNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboOVCompanyNo.FormattingEnabled = True
        Me.cboOVCompanyNo.ItemHeight = 13
        Me.cboOVCompanyNo.Location = New System.Drawing.Point(476, 2)
        Me.cboOVCompanyNo.Name = "cboOVCompanyNo"
        Me.cboOVCompanyNo.Size = New System.Drawing.Size(157, 21)
        Me.cboOVCompanyNo.TabIndex = 101
        '
        'lblOVOPDCompanyName
        '
        Me.lblOVOPDCompanyName.Location = New System.Drawing.Point(327, 32)
        Me.lblOVOPDCompanyName.Name = "lblOVOPDCompanyName"
        Me.lblOVOPDCompanyName.Size = New System.Drawing.Size(143, 18)
        Me.lblOVOPDCompanyName.TabIndex = 102
        Me.lblOVOPDCompanyName.Text = "Company Name"
        '
        'lblOVCompanyNo
        '
        Me.lblOVCompanyNo.Location = New System.Drawing.Point(327, 3)
        Me.lblOVCompanyNo.Name = "lblOVCompanyNo"
        Me.lblOVCompanyNo.Size = New System.Drawing.Size(143, 18)
        Me.lblOVCompanyNo.TabIndex = 100
        Me.lblOVCompanyNo.Text = "Company Number"
        '
        'stbOVInsuranceName
        '
        Me.stbOVInsuranceName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbOVInsuranceName.CapitalizeFirstLetter = False
        Me.stbOVInsuranceName.EntryErrorMSG = ""
        Me.stbOVInsuranceName.Location = New System.Drawing.Point(164, 25)
        Me.stbOVInsuranceName.MaxLength = 41
        Me.stbOVInsuranceName.Multiline = True
        Me.stbOVInsuranceName.Name = "stbOVInsuranceName"
        Me.stbOVInsuranceName.ReadOnly = True
        Me.stbOVInsuranceName.RegularExpression = ""
        Me.stbOVInsuranceName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbOVInsuranceName.Size = New System.Drawing.Size(157, 34)
        Me.stbOVInsuranceName.TabIndex = 99
        '
        'cboOVInsuranceNo
        '
        Me.cboOVInsuranceNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboOVInsuranceNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboOVInsuranceNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboOVInsuranceNo.DropDownWidth = 276
        Me.cboOVInsuranceNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboOVInsuranceNo.FormattingEnabled = True
        Me.cboOVInsuranceNo.ItemHeight = 13
        Me.cboOVInsuranceNo.Location = New System.Drawing.Point(164, 3)
        Me.cboOVInsuranceNo.Name = "cboOVInsuranceNo"
        Me.cboOVInsuranceNo.Size = New System.Drawing.Size(157, 21)
        Me.cboOVInsuranceNo.TabIndex = 97
        '
        'lblOVInsuranceName
        '
        Me.lblOVInsuranceName.Location = New System.Drawing.Point(6, 32)
        Me.lblOVInsuranceName.Name = "lblOVInsuranceName"
        Me.lblOVInsuranceName.Size = New System.Drawing.Size(152, 18)
        Me.lblOVInsuranceName.TabIndex = 98
        Me.lblOVInsuranceName.Text = "Insurance Name"
        '
        'lblOVInsuranceNo
        '
        Me.lblOVInsuranceNo.Location = New System.Drawing.Point(6, 6)
        Me.lblOVInsuranceNo.Name = "lblOVInsuranceNo"
        Me.lblOVInsuranceNo.Size = New System.Drawing.Size(152, 18)
        Me.lblOVInsuranceNo.TabIndex = 96
        Me.lblOVInsuranceNo.Text = "Insurance Number"
        '
        'cmsClaimSummaries
        '
        Me.cmsClaimSummaries.BackColor = System.Drawing.Color.GhostWhite
        Me.cmsClaimSummaries.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmsClaimSummariesCopy, Me.cmsClaimSummariesSelectAll})
        Me.cmsClaimSummaries.Name = "cmsSearch"
        Me.cmsClaimSummaries.Size = New System.Drawing.Size(123, 48)
        '
        'cmsClaimSummariesCopy
        '
        Me.cmsClaimSummariesCopy.Enabled = False
        Me.cmsClaimSummariesCopy.Image = CType(resources.GetObject("cmsClaimSummariesCopy.Image"), System.Drawing.Image)
        Me.cmsClaimSummariesCopy.Name = "cmsClaimSummariesCopy"
        Me.cmsClaimSummariesCopy.Size = New System.Drawing.Size(122, 22)
        Me.cmsClaimSummariesCopy.Text = "Copy"
        Me.cmsClaimSummariesCopy.ToolTipText = "To copy with column headings, use Ctrl+C key combination"
        '
        'cmsClaimSummariesSelectAll
        '
        Me.cmsClaimSummariesSelectAll.Enabled = False
        Me.cmsClaimSummariesSelectAll.Name = "cmsClaimSummariesSelectAll"
        Me.cmsClaimSummariesSelectAll.Size = New System.Drawing.Size(122, 22)
        Me.cmsClaimSummariesSelectAll.Text = "Select All"
        '
        'fbnLoad
        '
        Me.fbnLoad.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnLoad.Location = New System.Drawing.Point(681, 13)
        Me.fbnLoad.Name = "fbnLoad"
        Me.fbnLoad.Size = New System.Drawing.Size(116, 22)
        Me.fbnLoad.TabIndex = 0
        Me.fbnLoad.Text = "Load..."
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Location = New System.Drawing.Point(439, 13)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(217, 20)
        Me.dtpEndDate.TabIndex = 3
        '
        'lblEndDate
        '
        Me.lblEndDate.Location = New System.Drawing.Point(350, 15)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(83, 20)
        Me.lblEndDate.TabIndex = 2
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Location = New System.Drawing.Point(105, 15)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(230, 20)
        Me.dtpStartDate.TabIndex = 1
        '
        'lblStartDate
        '
        Me.lblStartDate.Location = New System.Drawing.Point(7, 15)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(92, 20)
        Me.lblStartDate.TabIndex = 0
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'fbnExportTo
        '
        Me.fbnExportTo.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnExportTo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnExportTo.Location = New System.Drawing.Point(803, 13)
        Me.fbnExportTo.Name = "fbnExportTo"
        Me.fbnExportTo.Size = New System.Drawing.Size(106, 22)
        Me.fbnExportTo.TabIndex = 4
        Me.fbnExportTo.Text = "Export to Excel..."
        '
        'grpPeriod
        '
        Me.grpPeriod.Controls.Add(Me.fbnLoad)
        Me.grpPeriod.Controls.Add(Me.dtpEndDate)
        Me.grpPeriod.Controls.Add(Me.lblEndDate)
        Me.grpPeriod.Controls.Add(Me.fbnExportTo)
        Me.grpPeriod.Controls.Add(Me.dtpStartDate)
        Me.grpPeriod.Controls.Add(Me.lblStartDate)
        Me.grpPeriod.Location = New System.Drawing.Point(15, 5)
        Me.grpPeriod.Name = "grpPeriod"
        Me.grpPeriod.Size = New System.Drawing.Size(1012, 46)
        Me.grpPeriod.TabIndex = 0
        Me.grpPeriod.TabStop = False
        Me.grpPeriod.Text = "Period"
        '
        'fbnPrint
        '
        Me.fbnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnPrint.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnPrint.Location = New System.Drawing.Point(854, 565)
        Me.fbnPrint.Name = "fbnPrint"
        Me.fbnPrint.Size = New System.Drawing.Size(88, 22)
        Me.fbnPrint.TabIndex = 5
        Me.fbnPrint.Text = "Print"
        Me.fbnPrint.Visible = False
        '
        'ColGeneralMainMemberNo
        '
        Me.ColGeneralMainMemberNo.DataPropertyName = "MainMemberNo"
        Me.ColGeneralMainMemberNo.HeaderText = "Main Member No"
        Me.ColGeneralMainMemberNo.Name = "ColGeneralMainMemberNo"
        Me.ColGeneralMainMemberNo.ReadOnly = True
        '
        'ColGeneralConsumptionMainMemberName
        '
        Me.ColGeneralConsumptionMainMemberName.DataPropertyName = "MainMemberName"
        Me.ColGeneralConsumptionMainMemberName.HeaderText = "Main Member Name"
        Me.ColGeneralConsumptionMainMemberName.Name = "ColGeneralConsumptionMainMemberName"
        Me.ColGeneralConsumptionMainMemberName.ReadOnly = True
        Me.ColGeneralConsumptionMainMemberName.Width = 150
        '
        'ColGeneralMedicalCardNo
        '
        Me.ColGeneralMedicalCardNo.DataPropertyName = "MedicalCardNo"
        Me.ColGeneralMedicalCardNo.HeaderText = "Medical Card No"
        Me.ColGeneralMedicalCardNo.Name = "ColGeneralMedicalCardNo"
        Me.ColGeneralMedicalCardNo.ReadOnly = True
        '
        'ColGeneralMemberType
        '
        Me.ColGeneralMemberType.DataPropertyName = "MemberType"
        Me.ColGeneralMemberType.HeaderText = "Member Type"
        Me.ColGeneralMemberType.Name = "ColGeneralMemberType"
        Me.ColGeneralMemberType.ReadOnly = True
        '
        'colGeneralFullName
        '
        Me.colGeneralFullName.DataPropertyName = "FullName"
        Me.colGeneralFullName.HeaderText = "Full Name"
        Me.colGeneralFullName.Name = "colGeneralFullName"
        Me.colGeneralFullName.ReadOnly = True
        Me.colGeneralFullName.Width = 200
        '
        'colGeneralPatientNo
        '
        Me.colGeneralPatientNo.DataPropertyName = "PatientNo"
        Me.colGeneralPatientNo.HeaderText = "Patient No"
        Me.colGeneralPatientNo.Name = "colGeneralPatientNo"
        Me.colGeneralPatientNo.ReadOnly = True
        '
        'ColGeneralInsuranceName
        '
        Me.ColGeneralInsuranceName.DataPropertyName = "InsuranceName"
        Me.ColGeneralInsuranceName.HeaderText = "Insurance Name"
        Me.ColGeneralInsuranceName.Name = "ColGeneralInsuranceName"
        Me.ColGeneralInsuranceName.ReadOnly = True
        '
        'ColGeneralCompanyName
        '
        Me.ColGeneralCompanyName.DataPropertyName = "CompanyName"
        Me.ColGeneralCompanyName.HeaderText = "Company Name"
        Me.ColGeneralCompanyName.Name = "ColGeneralCompanyName"
        Me.ColGeneralCompanyName.ReadOnly = True
        '
        'ColGeneralPolicyName
        '
        Me.ColGeneralPolicyName.DataPropertyName = "PolicyName"
        Me.ColGeneralPolicyName.HeaderText = "Policy Name"
        Me.ColGeneralPolicyName.Name = "ColGeneralPolicyName"
        Me.ColGeneralPolicyName.ReadOnly = True
        '
        'ColGeneralMemberPremium
        '
        Me.ColGeneralMemberPremium.DataPropertyName = "MemberPremium"
        Me.ColGeneralMemberPremium.HeaderText = "Member Premium"
        Me.ColGeneralMemberPremium.Name = "ColGeneralMemberPremium"
        Me.ColGeneralMemberPremium.ReadOnly = True
        '
        'ColGeneralTotalamount
        '
        Me.ColGeneralTotalamount.DataPropertyName = "Totalamount"
        Me.ColGeneralTotalamount.HeaderText = "Total Amount"
        Me.ColGeneralTotalamount.Name = "ColGeneralTotalamount"
        Me.ColGeneralTotalamount.ReadOnly = True
        '
        'ColGeneralOPDConsumptionBalance
        '
        Me.ColGeneralOPDConsumptionBalance.DataPropertyName = "Balance"
        Me.ColGeneralOPDConsumptionBalance.HeaderText = "Balance"
        Me.ColGeneralOPDConsumptionBalance.Name = "ColGeneralOPDConsumptionBalance"
        Me.ColGeneralOPDConsumptionBalance.ReadOnly = True
        '
        'ColGeneralPolicyStartDate
        '
        Me.ColGeneralPolicyStartDate.DataPropertyName = "PolicyStartDate"
        Me.ColGeneralPolicyStartDate.HeaderText = "Policy Start Date"
        Me.ColGeneralPolicyStartDate.Name = "ColGeneralPolicyStartDate"
        Me.ColGeneralPolicyStartDate.ReadOnly = True
        '
        'ColGeneralPolicyEndDate
        '
        Me.ColGeneralPolicyEndDate.DataPropertyName = "PolicyEndDate"
        Me.ColGeneralPolicyEndDate.HeaderText = "Policy End Date"
        Me.ColGeneralPolicyEndDate.Name = "ColGeneralPolicyEndDate"
        Me.ColGeneralPolicyEndDate.ReadOnly = True
        '
        'frmInsuranceClaimSummaries
        '
        Me.AcceptButton = Me.fbnLoad
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(1043, 600)
        Me.Controls.Add(Me.fbnPrint)
        Me.Controls.Add(Me.grpPeriod)
        Me.Controls.Add(Me.tbcPeriodicReport)
        Me.Controls.Add(Me.fbnClose)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmInsuranceClaimSummaries"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Insurance Claim Summaries"
        Me.tbcPeriodicReport.ResumeLayout(False)
        Me.tpgMemberConsumptionDetails.ResumeLayout(False)
        Me.tpgMemberConsumptionDetails.PerformLayout()
        CType(Me.dgvMemberConsumptionDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgfullMemberConsumptionDetails.ResumeLayout(False)
        Me.tpgfullMemberConsumptionDetails.PerformLayout()
        CType(Me.dgvMemberfullConsumptionDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgIPDMemberConsumptionDetails.ResumeLayout(False)
        Me.tpgIPDMemberConsumptionDetails.PerformLayout()
        CType(Me.dgvMemberfullIPDConsumptionDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgGeneralConsumption.ResumeLayout(False)
        Me.tpgGeneralConsumption.PerformLayout()
        CType(Me.dgvGeneralConsumption, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgGeneralIPDConsumption.ResumeLayout(False)
        Me.tpgGeneralIPDConsumption.PerformLayout()
        CType(Me.dgvGeneralIPDConsumption, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgOverConsumption.ResumeLayout(False)
        Me.tpgOverConsumption.PerformLayout()
        CType(Me.dgvOverConsumption, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsClaimSummaries.ResumeLayout(False)
        Me.grpPeriod.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents tbcPeriodicReport As System.Windows.Forms.TabControl
    Friend WithEvents fbnLoad As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents fbnExportTo As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents grpPeriod As System.Windows.Forms.GroupBox
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents cmsClaimSummaries As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmsClaimSummariesCopy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmsClaimSummariesSelectAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tpgMemberConsumptionDetails As System.Windows.Forms.TabPage
    Friend WithEvents StbMainMemberNameDetails As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents stbMainMemberCompanyName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dgvMemberConsumptionDetails As System.Windows.Forms.DataGridView
    Friend WithEvents lblTotalAmount As System.Windows.Forms.Label
    Friend WithEvents stbGrandTotalAmount As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents tpgfullMemberConsumptionDetails As System.Windows.Forms.TabPage
    Friend WithEvents stbfullMainMemberName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents stbfullCompanyName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dgvMemberfullConsumptionDetails As System.Windows.Forms.DataGridView
    Friend WithEvents lblAmountWords As System.Windows.Forms.Label
    Friend WithEvents stbAmountWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents cboMainMemberNo As System.Windows.Forms.ComboBox
    Friend WithEvents cbofullMainMemberNo As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents stbmemberConsumptionWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents stbMemberConsumptions As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents fbnPrint As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents lblMemberBalance As System.Windows.Forms.Label
    Friend WithEvents stbMemberBalance As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAllocatedPremium As System.Windows.Forms.Label
    Friend WithEvents stbAllocatedPremium As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents ColMembertype As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColMedicalCardNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColPatientNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColClaimAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpgOverConsumption As System.Windows.Forms.TabPage
    Friend WithEvents ColConsumptionMemberType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColConsumptionMedicalCardNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColConsumptionClaimNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColConsumptionFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColConsumptionVisitNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColColConsumptionItemName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColColConsumptionQuantity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColColConsumptionUnitPrice As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColColConsumptionAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpgIPDMemberConsumptionDetails As System.Windows.Forms.TabPage
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents stbIPDmemberConsumptionWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents stbIPDMemberConsumptions As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents cboIPDfullMainMemberNo As System.Windows.Forms.ComboBox
    Friend WithEvents stbIPDMainMemberName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents StbIPDCompanyName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents dgvMemberfullIPDConsumptionDetails As System.Windows.Forms.DataGridView
    Friend WithEvents colIPDMembertype As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIPDMedicalCardNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColIPDInvoiceNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIPDFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColIPDVisitNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColIPDItemName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColIPDQuantity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIPDUnitPrice As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColIPDAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpgGeneralConsumption As System.Windows.Forms.TabPage
    Friend WithEvents dgvGeneralConsumption As System.Windows.Forms.DataGridView
    Friend WithEvents stbGeneralCompanyName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents cboGeneralCompanyNo As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents stbGeneralInsuranceName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents cboGeneralInsuranceNo As System.Windows.Forms.ComboBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lblTotalPremium As System.Windows.Forms.Label
    Friend WithEvents stbTotalPremium As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lbltotalConsumptionWorfs As System.Windows.Forms.Label
    Friend WithEvents stbtotalConsumptionWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblTotalConsumption As System.Windows.Forms.Label
    Friend WithEvents stbtotalConsumption As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents tpgGeneralIPDConsumption As System.Windows.Forms.TabPage
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents stbIPDTotalPremium As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents stbIPDtotalConsumptionWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents stbIPDtotalConsumption As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents dgvGeneralIPDConsumption As System.Windows.Forms.DataGridView
    Friend WithEvents stbGeneralIPDCompanyName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents cboGeneralIPDCompanyNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblGeneralIPDCompanyName As System.Windows.Forms.Label
    Friend WithEvents lblGeneralIPDCompanyNo As System.Windows.Forms.Label
    Friend WithEvents stbGeneralIPDInsuranceName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents cboGeneralIPDInsuranceNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblGeneralIPDInsuranceName As System.Windows.Forms.Label
    Friend WithEvents lblGeneralIPDInsuranceNo As System.Windows.Forms.Label
    Friend WithEvents colGeneralIPDFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colGeneralIPDPatientNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGeneralIPDInsuranceName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGeneralIPDMedicalCardNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColIPDGeneralMemberType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colGeneralIPDCompanyName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGeneralIPDPolicyName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGeneralIPDMemberPremium As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoGeneralIPDTotalamount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colGeneralIPDPolicyStartDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colGeneralIPDPolicyEndDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colGeneralIPDMainMemberNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvOverConsumption As System.Windows.Forms.DataGridView
    Friend WithEvents colOverConsumptionFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOverConsumptionPatientNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOverConsumptionInsuranceName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOverConsumptionMedicalCardNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOverConsumptionMemberType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOverConsumptionCompanyName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOverConsumedPolicyName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColOverConsumptionMemberPremium As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumedAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColExcess As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOverConsumptionPolicyStartDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOverConsumptionPolicyEndDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColOverConsumedMainMemberNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents stbOVOPDCompanyName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents cboOVCompanyNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblOVOPDCompanyName As System.Windows.Forms.Label
    Friend WithEvents lblOVCompanyNo As System.Windows.Forms.Label
    Friend WithEvents stbOVInsuranceName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents cboOVInsuranceNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblOVInsuranceName As System.Windows.Forms.Label
    Friend WithEvents lblOVInsuranceNo As System.Windows.Forms.Label
    Friend WithEvents ColGeneralMainMemberNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGeneralConsumptionMainMemberName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGeneralMedicalCardNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGeneralMemberType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colGeneralFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colGeneralPatientNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGeneralInsuranceName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGeneralCompanyName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGeneralPolicyName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGeneralMemberPremium As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGeneralTotalamount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGeneralOPDConsumptionBalance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGeneralPolicyStartDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGeneralPolicyEndDate As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
