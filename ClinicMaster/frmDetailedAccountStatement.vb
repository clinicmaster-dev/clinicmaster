
Option Strict On
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects
Imports SyncSoft.Common.Structures
Imports SyncSoft.Common.SQL.Methods
Imports LookupDataID = SyncSoft.SQLDb.Lookup.LookupDataID
Imports SyncSoft.SQLDb
Imports System.Drawing.Printing

Public Class frmDetailedAccountStatement

#Region " Fields "
    Private billCustomers As DataTable
    Private insuranceCompanies As DataTable
    Private billCompanies As DataTable
    Private WithEvents docPrintAccountStatement As New PrintDocument()
    Private accountStatement As Collection
    Private pageNo As Integer
    Private printFontName As String = "Courier New"
    Private bodyBoldFont As New Font(printFontName, 10, FontStyle.Bold)
    Private bodyNormalFont As New Font(printFontName, 10)
    Private bodySmallFont As New Font(printFontName, 6)
    Private oInsurances As New SyncSoft.SQLDb.Insurances()
    Private oCompanies As New SyncSoft.SQLDb.Companies()
    Private oBillModesID As New LookupDataID.BillModesID()
    Private oBillCustomerTypeID As New LookupDataID.BillCustomerTypeID()
    Private oInvoices As New Invoices()
#End Region

    Private Sub frmAccountStatement_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.Cursor = Cursors.WaitCursor()

            LoadLookupDataCombo(Me.cboBillMode, LookupObjects.BillModes, False)
            Me.dtpStartDateTime.Value = Today.AddDays(-1)
            Me.dtpEndDateTime.MaxDate = Now.AddDays(1)

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub

    Private Sub frmAccountStatement_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub

    Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub


    Private Sub LoadAccountClients(ByVal billModesID As String)

        Dim oBillCustomers As New SyncSoft.SQLDb.BillCustomers()
        Dim oBillModesID As New LookupDataID.BillModesID()
        Dim oSetupData As New SetupData()

        Try
            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.ClearControls()
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Select Case billModesID.ToUpper()

                Case oBillModesID.Cash.ToUpper()
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.lblAccountNo.Text = "Patient No"
                    Me.lblAccountName.Text = "Patient Name"
                    Me.Text = "Patient Statement"
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.SetInsuranceCompanyCTRLS(False)
                Case oBillModesID.Account.ToUpper()

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ' Load all from Bill Customers

                    If Not InitOptions.LoadBillCustomersAtStart Then
                        billCustomers = oBillCustomers.GetBillCustomers().Tables("BillCustomers")
                        oSetupData.BillCustomers = billCustomers
                    Else : billCustomers = oSetupData.BillCustomers
                    End If

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    LoadComboData(Me.cboAccountNo, billCustomers, "BillCustomerFullName")
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.lblAccountNo.Text = "Account No"
                    Me.lblAccountName.Text = "Account Name"
                    Me.Text = "Account Statement"
                    Me.SetInsuranceCompanyCTRLS(False)
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Insurance.ToUpper()
                    Dim insurances As DataTable = oInsurances.GetInsurances().Tables("Insurances")
                    LoadComboData(Me.cboAccountNo, insurances, "InsuranceFullName")


                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.lblAccountNo.Text = "Insurance No"
                    Me.lblAccountName.Text = "Insurance Name"
                    Me.Text = "Insurance Statement"
                    Me.SetInsuranceCompanyCTRLS(True)
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End Select

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetInsuranceCompanyCTRLS(ByVal state As Boolean)

        Me.cboCompanyNo.SelectedIndex = -1
        Me.cboCompanyNo.SelectedIndex = -1
        Me.stbCompanyName.Clear()
        Me.cboCompanyNo.Items.Clear()
        Me.cboCompanyNo.Text = String.Empty
        Me.cboCompanyNo.Enabled = state
    End Sub

    Private Sub ClearControls()
        Me.stbOutStandingBalance.Clear()
        Me.nbxAccountBalance.Clear()
        Me.stbInvoiceAmount.Clear()
        Me.stbInvoiceAdjustmentAmount.Clear()
        Me.stbExtraBillAmount.Clear()
        Me.stbTotalPayments.Clear()
        Me.stbTotalRefunds.Clear()
        Me.stbTotalCredit.Clear()
        Me.stbTotalDebit.Clear()
        Me.stbNetAmount.Clear()
        Me.stbBalance.Clear()
        Me.stbBillFormAmount.Clear()
        Me.stbBillFormAmountWords.Clear()
        Me.stbCompanyName.Clear()
        Me.lblRecordsNo.Text = String.Empty
        Me.stbInvoiceAmountWords.Clear()
        Me.stbINVAAmountWords.Clear()
        Me.stbExtraBillAmountlWords.Clear()
        Me.stbTotalPaymentWords.Clear()
        Me.stbTotalRefundWords.Clear()
        Me.stbNetAmountWords.Clear()
        Me.stbBalanceWords.Clear()
        Me.stbTotalBill.Clear()
        Me.stbTotalPaidAmount.Clear()
        Me.dgvInvoices.Rows.Clear()
        Me.dgvInvoiceAdjustments.Rows.Clear()
        Me.dgvCashReceipts.Rows.Clear()
        Me.dgvBillFormPayments.Rows.Clear()
        Me.dgvAccountTrasaction.Rows.Clear()
        Me.dgvRefunds.Rows.Clear()
        Me.dgvExtraBillRefunds.Rows.Clear()
        Me.dgvExtraBillItems.Rows.Clear()
        If Not String.IsNullOrEmpty(Me.cboAccountNo.Text) OrElse Not String.IsNullOrEmpty(Me.stbMainMemberNo.Text) Then Me.btnShowOutStandingBalance.Enabled = True

    End Sub

    Private Sub LoadData()

        Dim oAccounts As New SyncSoft.SQLDb.Accounts
        Try

            Me.Cursor = Cursors.WaitCursor
            Dim oInvoiceAdjustments As New InvoiceAdjustments()
            Dim oExtraBillItems As New ExtraBillItems()
            Dim oPaymentDetails As New PaymentDetails()
            Dim oPaymentExtraBillItems As New PaymentExtraBillItems()
            Dim oRefundDetails As New RefundDetails()
            Dim oRefundExtraBillItems As New RefundExtraBillItems()

            Dim oRefunds As New Refunds()


            Dim AccountNo As String = String.Empty
            Dim startDate As Date = DateTimeEnteredIn(Me.dtpStartDateTime, "Start Date")
            Dim endDate As Date = DateTimeEnteredIn(Me.dtpEndDateTime, "End  Date")
            Dim billModesID As String = StringValueMayBeEnteredIn(Me.cboBillMode, "Account Category!")
            Dim companyNo As String = StringMayBeEnteredIn(cboCompanyNo)
            Dim mainMemberNo As String = StringMayBeEnteredIn(stbMainMemberNo)
            If String.IsNullOrEmpty(mainMemberNo) Then
                AccountNo = RevertText(StringEnteredIn(Me.cboAccountNo, "Account No"))
            Else
                AccountNo = RevertText(StringMayBeEnteredIn(Me.cboAccountNo))
            End If
            If endDate < startDate Then Throw New ArgumentException("End Date can't be before Start Date!")

            If String.IsNullOrEmpty(billModesID) Then Return
            Dim invoices As DataTable
            Dim invoiceAdjustments As DataTable
            Dim extraBillItems As DataTable
            Dim paymentDetails As DataTable
            Dim paymentExtraBillItems As DataTable
            Dim refundDetails As DataTable
            Dim refundExtraBillItems As DataTable
            Dim accounts As DataTable
            Dim accountTranCo As String = AccountNo

            If String.IsNullOrEmpty(companyNo) Then
                accountTranCo = AccountNo
            Else : accountTranCo = companyNo
            End If
            If String.IsNullOrEmpty(mainMemberNo) Then
                invoices = oInvoices.GetPeriodicBillToCustomerInvoices(AccountNo, billModesID, startDate, endDate, companyNo).Tables("Invoices")
                invoiceAdjustments = oInvoiceAdjustments.GetPeriodicBillToCustomerInvoiceAdjustments(AccountNo, billModesID, startDate, endDate, companyNo).Tables("InvoiceAdjustments")
                extraBillItems = oExtraBillItems.GetPeriodicExtraBillItemsByBillToCustomerNo(AccountNo, billModesID, startDate, endDate, companyNo).Tables("ExtraBillItems")
                paymentDetails = oPaymentDetails.GetPeriodicBillNoPayments(AccountNo, billModesID, startDate, endDate, companyNo).Tables("PaymentDetails")
                paymentExtraBillItems = oPaymentExtraBillItems.GetPeriodicBillNoPaymentExtraBills(AccountNo, billModesID, startDate, endDate, companyNo).Tables("PaymentExtraBillItems")
                refundDetails = oRefundDetails.GetPeriodicBillNoRefunds(AccountNo, billModesID, startDate, endDate, companyNo).Tables("RefundDetails")
                refundExtraBillItems = oRefundExtraBillItems.GetPeriodicBillNoRefundExtraBills(AccountNo, billModesID, startDate, endDate, companyNo).Tables("RefundExtraBillItems")
                accounts = oAccounts.GetPeriodicAccountTransactionsByAccountBillNo(accountTranCo, startDate, endDate).Tables("Accounts")

            Else
                invoices = oInvoices.GetPeriodicMainMemberNoInvoices(mainMemberNo, startDate, endDate).Tables("Invoices")
                invoiceAdjustments = oInvoiceAdjustments.GetPeriodicMainMemberNoInvoiceAdjustments(mainMemberNo, startDate, endDate).Tables("InvoiceAdjustments")
                extraBillItems = oExtraBillItems.GetPeriodicMainMemberNoExtraBills(mainMemberNo, startDate, endDate).Tables("ExtraBillItems")
                paymentDetails = oPaymentDetails.GetPeriodicMainMemberNoPayments(mainMemberNo, startDate, endDate).Tables("PaymentDetails")
                paymentExtraBillItems = oPaymentExtraBillItems.GetPeriodicMainMemberNoPaymentExtraBills(mainMemberNo, startDate, endDate).Tables("PaymentExtraBillItems")
                refundDetails = oRefundDetails.GetPeriodicMainMemberNoRefunds(mainMemberNo, startDate, endDate).Tables("RefundDetails")
                refundExtraBillItems = oRefundExtraBillItems.GetPeriodicMainMemberNoRefundExtraBills(mainMemberNo, startDate, endDate).Tables("RefundExtraBillItems")
                accounts = oAccounts.GetPeriodicAccountTransactionsByAccountBillNo(mainMemberNo, startDate, endDate).Tables("Accounts")

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim message As String = "No " + Me.Text + " record(s) found for period between " +
                FormatDate(startDate) + " and " + FormatDate(endDate) + "!"

            LoadGridData(dgvInvoices, invoices)
            LoadGridData(dgvInvoiceAdjustments, invoiceAdjustments)
            LoadGridData(Me.dgvExtraBillItems, extraBillItems)
            LoadGridData(dgvCashReceipts, paymentDetails)
            LoadGridData(dgvBillFormPayments, paymentExtraBillItems)
            LoadGridData(dgvRefunds, refundDetails)
            LoadGridData(dgvExtraBillRefunds, refundExtraBillItems)
            LoadGridData(Me.dgvAccountTrasaction, accounts)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim totalInvoiceAmount As Decimal = CalculateGridAmount(dgvInvoices, Me.colInvoiceAmount)
            Dim totalInvoiceAdjustments As Decimal = CalculateGridAmount(dgvInvoiceAdjustments, Me.colIVAAmount)
            Dim totalExtraBills As Decimal = CalculateGridAmount(dgvExtraBillItems, Me.colBFAmount)
            Dim totalPaidAmount As Decimal = CalculateGridAmount(dgvCashReceipts, Me.colPayAmount)
            Dim totalExtraBillAmountPaid As Decimal = CalculateGridAmount(dgvBillFormPayments, Me.colBFPAmount)
            Dim totalOPDRefunds As Decimal = CalculateGridAmount(dgvRefunds, Me.colRefAmount)
            Dim totalExtraBillRefunds As Decimal = CalculateGridAmount(dgvExtraBillRefunds, Me.colEXTRefAmount)

            Dim totalCredit As Decimal = CalculateGridAmount(dgvAccountTrasaction, Me.colCredit)
            Dim totalDebit As Decimal = CalculateGridAmount(dgvAccountTrasaction, Me.colDebit)
            Dim accountBalance As Decimal = totalCredit - totalDebit
            Dim totalBill As Decimal = (totalInvoiceAmount + totalExtraBills - totalInvoiceAdjustments)
            Dim totalPaid As Decimal = (totalPaidAmount + totalExtraBillAmountPaid + accountBalance - (totalOPDRefunds + totalExtraBillRefunds))
            Dim balance As Decimal = totalBill - totalPaid
            Me.stbTotalBill.Text = FormatNumber(totalBill, AppData.DecimalPlaces)
            Me.stbTotalPaidAmount.Text = FormatNumber(totalPaid, AppData.DecimalPlaces)

            Me.stbInvoiceAmount.Text = FormatNumber(totalInvoiceAmount, AppData.DecimalPlaces)
            Me.stbInvoiceAdjustmentAmount.Text = FormatNumber(totalInvoiceAdjustments, AppData.DecimalPlaces)
            Me.stbExtraBillAmount.Text = FormatNumber(totalExtraBills, AppData.DecimalPlaces)
            Me.stbTotalPayments.Text = FormatNumber(totalPaidAmount, AppData.DecimalPlaces)
            Me.stbBillFormAmount.Text = FormatNumber(totalExtraBillAmountPaid, AppData.DecimalPlaces)
            Me.stbTotalRefunds.Text = FormatNumber(totalOPDRefunds, AppData.DecimalPlaces)
            Me.stbExtraBillRefunds.Text = FormatNumber(totalExtraBillRefunds, AppData.DecimalPlaces)
            Me.stbTotalCredit.Text = FormatNumber(totalCredit, AppData.DecimalPlaces)
            Me.stbTotalDebit.Text = FormatNumber(totalDebit, AppData.DecimalPlaces)
            Me.stbNetAmount.Text = FormatNumber(accountBalance, AppData.DecimalPlaces)
            Me.stbBalance.Text = FormatNumber(balance, AppData.DecimalPlaces)
            ' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Me.stbInvoiceAmountWords.Text = NumberToWords(totalInvoiceAmount)
            Me.stbINVAAmountWords.Text = NumberToWords(totalInvoiceAdjustments)
            Me.stbExtraBillAmountlWords.Text = NumberToWords(totalExtraBills)
            Me.stbTotalPaymentWords.Text = NumberToWords(totalPaidAmount)
            Me.stbBillFormAmountWords.Text = NumberToWords(totalExtraBillAmountPaid)
            Me.stbTotalRefundWords.Text = NumberToWords(totalOPDRefunds)
            Me.stbTotalRefundWords.Text = NumberToWords(totalExtraBillRefunds)
            Me.stbNetAmountWords.Text = NumberToWords(accountBalance)
            Me.stbBalanceWords.Text = NumberToWords(balance)


            EnablePrintButtons(dgvInvoices.Rows.Count > 0 OrElse dgvInvoiceAdjustments.Rows.Count > 0 OrElse dgvExtraBillItems.Rows.Count > 0 OrElse
                               dgvCashReceipts.RowCount > 0 OrElse dgvBillFormPayments.RowCount > 0 OrElse dgvAccountTrasaction.RowCount > 0 OrElse
                               dgvRefunds.RowCount > 0 OrElse dgvExtraBillRefunds.RowCount > 0)

            TabSelectedTabs()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub



    Private Sub LoadAccountDetails()
        Try

            Dim billModesID As String = StringValueMayBeEnteredIn(Me.cboBillMode, "Account Category!")
            Dim accountNo As String = RevertText(SubstringRight(StringMayBeEnteredIn(Me.cboAccountNo)))

            If String.IsNullOrEmpty(accountNo) OrElse String.IsNullOrEmpty(billModesID) Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.LoadAccountDetails(billModesID, accountNo)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub EnablePrintButtons(bool As Boolean)
        btnPrint.Enabled = bool
        btnPrintPreview.Enabled = bool

    End Sub

    Private Sub LoadAccountDetails(ByVal billModesID As String, ByVal accountNo As String)

        Dim accountName As String = String.Empty

        Dim oPatients As New SyncSoft.SQLDb.Patients()
        Dim oBillCustomers As New SyncSoft.SQLDb.BillCustomers()
        Dim oInsurances As New SyncSoft.SQLDb.Insurances()

        Dim oBillModesID As New LookupDataID.BillModesID()

        Try
            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbAccountName.Clear()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Select Case billModesID.ToUpper()

                Case oBillModesID.Cash.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim row As DataRow = oPatients.GetPatients(accountNo).Tables("Patients").Rows(0)

                    Me.cboAccountNo.Text = FormatText(accountNo, "Patients", "PatientNo")
                    accountName = StringMayBeEnteredIn(row, "FullName")

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Account.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim row As DataRow = oBillCustomers.GetBillCustomers(accountNo).Tables("BillCustomers").Rows(0)

                    Me.cboAccountNo.Text = FormatText(accountNo, "BillCustomers", "AccountNo").ToUpper()
                    accountName = StringMayBeEnteredIn(row, "BillCustomerName")

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    Me.cboAccountNo.Text = FormatText(accountNo, "BillCustomers", "AccountNo").ToUpper()
                    Dim billCustomerName = StringMayBeEnteredIn(row, "BillCustomerName")
                    Dim billCustomerTypeID As String = StringMayBeEnteredIn(row, "BillCustomerTypeID")

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    If billCustomerTypeID.ToUpper().Equals(oBillCustomerTypeID.Insurance.ToUpper()) Then

                        Me.SetInsuranceCompanyCTRLS(True)

                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        billCompanies = oBillCustomers.GetBillCustomersByInsuranceNo(accountNo).Tables("BillCustomers")

                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        LoadComboData(Me.cboCompanyNo, billCompanies, "BillCustomerFullName")
                        Me.cboCompanyNo.Items.Insert(0, String.Empty)
                    Else
                        Me.cboCompanyNo.Items.Clear()
                        Me.SetInsuranceCompanyCTRLS(False)
                    End If
                Case oBillModesID.Insurance.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim row As DataRow = oInsurances.GetInsurances(accountNo).Tables("Insurances").Rows(0)

                    Me.cboAccountNo.Text = FormatText(accountNo, "Insurances", "InsuranceNo")
                    accountName = StringMayBeEnteredIn(row, "InsuranceName")


                    insuranceCompanies = oCompanies.GetCompanies().Tables("Companies")
                    LoadComboData(Me.cboCompanyNo, insuranceCompanies, "companyFullName")
                    Me.cboCompanyNo.Items.Insert(0, String.Empty)
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End Select

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbAccountName.Text = accountName

            SetBalances()
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetBalances()

        Me.nbxAccountBalance.Clear()

        Try
            Me.Cursor = Cursors.WaitCursor
            Dim AccountNo As String = StringMayBeEnteredIn(cboAccountNo)
            Dim billModesID As String = StringValueMayBeEnteredIn(Me.cboBillMode, "Account Category!")
            Dim companyNo As String = StringMayBeEnteredIn(cboCompanyNo)
            Dim mainMemberNo As String = StringMayBeEnteredIn(stbMainMemberNo)
            Dim accountBalance As Decimal


            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Load Account Balance '''''''''''''''''''''''''

            Select Case billModesID.ToUpper()

                Case oBillModesID.Cash.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    accountBalance = GetAccountBalance(oBillModesID.Cash, AccountNo)
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Account.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    If String.IsNullOrEmpty(companyNo) Then
                        accountBalance = GetAccountBalance(oBillModesID.Account, AccountNo)
                    Else
                        accountBalance = GetAccountBalance(oBillModesID.Account, companyNo)
                    End If

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Insurance.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    If String.IsNullOrEmpty(companyNo) Then
                        accountBalance = GetAccountBalance(oBillModesID.Insurance, AccountNo)
                    Else
                        accountBalance = 0
                    End If

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            End Select

            nbxAccountBalance.Value = FormatNumber(accountBalance, AppData.DecimalPlaces)

        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub SeOutStandingBalance()

        Me.stbOutStandingBalance.Clear()

        Try
            Me.Cursor = Cursors.WaitCursor



            Dim mainMemberNo As String = StringMayBeEnteredIn(stbMainMemberNo)
            Dim outStandingInvoiceBalance As Decimal

            If String.IsNullOrEmpty(mainMemberNo) Then
                Dim billModesID As String = StringValueMayBeEnteredIn(Me.cboBillMode, "Account Category!")
                Dim AccountNo As String = StringEnteredIn(cboAccountNo, "Account No")
                Dim companyNo As String = StringMayBeEnteredIn(cboCompanyNo)

                outStandingInvoiceBalance = oInvoices.GetOutstandingInvoiceBalance(billModesID, AccountNo, companyNo)
            Else
                outStandingInvoiceBalance = oInvoices.uspGetOutstandingMainMemberInvoiceBalance(mainMemberNo)
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Load Account Balance '''''''''''''''''''''''''


            stbOutStandingBalance.Text = FormatNumber(outStandingInvoiceBalance, AppData.DecimalPlaces)

        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub


    Private Sub TabSelectedTabs()
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim returnedRecord As Integer
        Dim tabLabel As String
        Select Case Me.tbcAccountStatement.SelectedTab.Name
            Case tpgInvoices.Name

                returnedRecord = dgvInvoices.RowCount()

                tabLabel = tpgInvoices.Text
                lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " record(s)"
                fbnExport.Enabled = returnedRecord > 0

            Case Me.tpgInvoiceAdjustments.Name
                returnedRecord = dgvInvoiceAdjustments.RowCount()
                tabLabel = tpgInvoiceAdjustments.Text
                lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " record(s)"
                fbnExport.Enabled = returnedRecord > 0


            Case Me.tpgExtraBills.Name
                returnedRecord = dgvExtraBillItems.RowCount()
                tabLabel = tpgExtraBills.Text
                lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " record(s)"
                fbnExport.Enabled = returnedRecord > 0
            Case Me.tpgOPDPayments.Name
                returnedRecord = dgvCashReceipts.RowCount()
                tabLabel = tpgOPDPayments.Text
                lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " record(s)"
                fbnExport.Enabled = returnedRecord > 0

            Case Me.tpgBillFormPayments.Name
                returnedRecord = dgvBillFormPayments.RowCount()
                tabLabel = tpgBillFormPayments.Text
                lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " record(s)"
                fbnExport.Enabled = returnedRecord > 0

            Case Me.tpgOPDRefunds.Name
                returnedRecord = dgvRefunds.RowCount()
                tabLabel = tpgOPDRefunds.Text
                lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " record(s)"
                fbnExport.Enabled = returnedRecord > 0

            Case Me.tpgExtraBillRefunds.Name
                returnedRecord = dgvExtraBillRefunds.RowCount()
                tabLabel = tpgExtraBillRefunds.Text
                lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " record(s)"
                fbnExport.Enabled = returnedRecord > 0

            Case Me.tpgAccountTransactions.Name
                returnedRecord = dgvAccountTrasaction.RowCount()
                tabLabel = tpgAccountTransactions.Text
                lblRecordsNo.Text = returnedRecord.ToString() + " " + tabLabel + " record(s)"
                fbnExport.Enabled = returnedRecord > 0
        End Select



    End Sub


    Private Sub cboBillMode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBillMode.SelectedIndexChanged
        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oBillModesID As New LookupDataID.BillModesID()
            Me.cboAccountNo.Items.Clear()
            Me.cboCompanyNo.Text = String.Empty
            Me.cboAccountNo.Text = String.Empty
            Me.stbAccountName.Clear()
            Me.stbCompanyName.Clear()
            Me.stbMainMemberNo.Clear()
            Me.ClearControls()


            Dim billModesID As String = StringValueMayBeEnteredIn(Me.cboBillMode, "Account Category!")
            If String.IsNullOrEmpty(billModesID) Then Return
            Me.btnLoad.Visible = billModesID.ToUpper().Equals(oBillModesID.Cash().ToUpper())
            Me.LoadAccountClients(billModesID)
            stbMainMemberNo.ReadOnly = Not billModesID.ToUpper.Equals(oBillModesID.Insurance)
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub



    Private Sub fbnLoad_Click(sender As Object, e As EventArgs) Handles fbnLoad.Click

        Try

            Me.Cursor = Cursors.WaitCursor
            LoadData()
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub



    Private Sub cboAccountNo_Leave(sender As Object, e As EventArgs) Handles cboAccountNo.Leave
        Try
            Me.Cursor = Cursors.WaitCursor
            LoadAccountDetails()

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnLoad_Click(sender As Object, e As EventArgs) Handles btnLoad.Click
        Try

            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fQuickSearch As New SyncSoft.SQL.Win.Forms.QuickSearch("Patients", Me.cboAccountNo)
            fQuickSearch.ShowDialog(Me)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim patientNo As String = RevertText(StringMayBeEnteredIn(Me.cboAccountNo))
            If Not String.IsNullOrEmpty(patientNo) Then Me.LoadAccountDetails()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

#Region "Print"

    Private Sub docPrintCashReceipts_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles docPrintAccountStatement.PrintPage

        Try

            Dim titleFont As New Font(printFontName, 12, FontStyle.Bold)

            Dim xPos As Single = e.MarginBounds.Left
            Dim yPos As Single = e.MarginBounds.Top

            Dim lineHeight As Single = bodyNormalFont.GetHeight(e.Graphics)

            Dim title As String = Me.Text

            Dim startDate As String = FormatDate(DateMayBeEnteredIn(Me.dtpStartDateTime))
            Dim endDate As String = FormatDate(DateMayBeEnteredIn(Me.dtpEndDateTime))
            Dim accountNo As String = StringMayBeEnteredIn(cboAccountNo)
            Dim accountName As String = StringMayBeEnteredIn(stbAccountName)
            Dim companyNo As String = StringMayBeEnteredIn(cboCompanyNo)
            Dim companyName As String = StringMayBeEnteredIn(stbCompanyName)
            Dim mainMemberNo As String = StringMayBeEnteredIn(stbMainMemberNo)
            Dim mainMemberName As String = StringMayBeEnteredIn(stbMainMemberName)

            Dim accountBalance As String = FormatNumber(DecimalMayBeEnteredIn(nbxAccountBalance), AppData.DecimalPlaces)
            If Me.btnShowOutStandingBalance.Enabled Then SeOutStandingBalance()
            Dim outStandingBalance As String = FormatNumber(DecimalMayBeEnteredIn(stbOutStandingBalance), AppData.DecimalPlaces)

            If Not String.IsNullOrEmpty(accountName) Then
                title += " of: " + accountName
            End If
            If Not String.IsNullOrEmpty(mainMemberName) Then
                title += " of: " + mainMemberName
            End If

            title = title.ToUpper()
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Increment the page number.
            pageNo += 1

            With e.Graphics

                'Dim widthTop As Single = .MeasureString("Received from width", titleFont).Width

                Dim widthTopFirst As Single = .MeasureString("W", titleFont).Width
                Dim widthTopSecond As Single = 11 * widthTopFirst
                Dim widthTopThird As Single = 18 * widthTopFirst
                Dim widthTopFourth As Single = 26 * widthTopFirst

                If pageNo < 2 Then

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    yPos = PrintPageHeader(e, bodyNormalFont, bodyBoldFont)
                    Dim oProductOwner As ProductOwner = GetProductOwnerInfo()
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    .DrawString(title, titleFont, Brushes.Black, xPos, yPos)
                    yPos += 3 * lineHeight
                    If Not String.IsNullOrEmpty(accountNo) Then
                        .DrawString(lblAccountNo.Text + ": ", bodyNormalFont, Brushes.Black, xPos, yPos)
                        .DrawString(accountNo, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                        yPos += lineHeight
                        .DrawString(lblAccountName.Text + ": ", bodyNormalFont, Brushes.Black, xPos, yPos)
                        .DrawString(accountName, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                        yPos += lineHeight
                    End If
                    If Not String.IsNullOrEmpty(companyNo) Then
                        .DrawString(lblCompanyNo.Text + ": ", bodyNormalFont, Brushes.Black, xPos, yPos)
                        .DrawString(accountNo, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                        yPos += lineHeight
                        .DrawString(lblCompanyName.Text + ": ", bodyNormalFont, Brushes.Black, xPos + widthTopSecond, yPos)
                        .DrawString(accountName, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                        yPos += lineHeight
                    End If

                    If Not String.IsNullOrEmpty(mainMemberNo) Then
                        .DrawString(lblMainMemberNo.Text + ": ", bodyNormalFont, Brushes.Black, xPos, yPos)
                        .DrawString(mainMemberNo, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                        yPos += lineHeight
                        .DrawString(lblMainMemberName.Text + ": ", bodyNormalFont, Brushes.Black, xPos, yPos)
                        .DrawString(mainMemberName, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                        yPos += lineHeight
                    End If
                    .DrawString("Start Date: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(startDate, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight
                    .DrawString("End Date: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(endDate, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight

                    .DrawString("Account Balance: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(accountBalance, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight

                    .DrawString("Outstanding Balance: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(outStandingBalance, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight

                End If

                Dim _StringFormat As New StringFormat()

                ' Draw the rest of the text left justified,
                ' wrap at words, and don't draw partial lines.

                With _StringFormat
                    .Alignment = StringAlignment.Near
                    .FormatFlags = StringFormatFlags.LineLimit
                    .Trimming = StringTrimming.Word
                End With

                Dim charactersFitted As Integer
                Dim linesFilled As Integer

                If accountStatement Is Nothing Then Return

                Do While accountStatement.Count > 0

                    ' Print the next paragraph.
                    Dim oPrintParagraps As PrintParagraps = DirectCast(accountStatement(1), PrintParagraps)
                    accountStatement.Remove(1)

                    ' Get the area available for this paragraph.
                    Dim printAreaRectangle As RectangleF = New RectangleF(e.MarginBounds.Left, yPos, e.MarginBounds.Width, e.MarginBounds.Bottom - yPos)

                    ' If the printing area rectangle's height < 1, make it 1.
                    If printAreaRectangle.Height < 1 Then printAreaRectangle.Height = 1

                    ' See how big the text will be and how many characters will fit.
                    Dim textSize As SizeF = .MeasureString(oPrintParagraps.Text, oPrintParagraps.TheFont,
                        New SizeF(printAreaRectangle.Width, printAreaRectangle.Height), _StringFormat, charactersFitted, linesFilled)

                    ' See if any characters will fit.
                    If charactersFitted > 0 Then
                        ' Draw the text.
                        .DrawString(oPrintParagraps.Text, oPrintParagraps.TheFont, Brushes.Black, printAreaRectangle, _StringFormat)
                        ' Increase the location where we can start, add a little interparagraph spacing.
                        yPos += textSize.Height ' + oPrintParagraps.TheFont.GetHeight(e.Graphics))

                    End If

                    ' See if some of the paragraph didn't fit on the page.
                    If charactersFitted < oPrintParagraps.Text.Length Then
                        ' Some of the paragraph didn't fit, prepare to print the rest on the next page.
                        oPrintParagraps.Text = oPrintParagraps.Text.Substring(charactersFitted)
                        accountStatement.Add(oPrintParagraps, Before:=1)
                        Exit Do
                    End If
                Loop

                ' If we have more paragraphs, we have more pages.
                e.HasMorePages = (accountStatement.Count > 0)

            End With

        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Private Sub SetAccountStatementPrintData()



        Dim footerFont As New Font(printFontName, 9)

        pageNo = 0
        accountStatement = New Collection()
        Dim padAmount As Integer = 60

        Try

            If Me.dgvInvoices.Rows.Count > 0 Then
                Dim accountHeader As New System.Text.StringBuilder(String.Empty)
                accountHeader.Append(ControlChars.NewLine)
                accountStatement.Add(New PrintParagraps(bodyBoldFont, accountHeader.ToString()))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, Me.GetInvoicePrintData))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, ControlChars.NewLine))
                Dim totalAmountBuilder As New System.Text.StringBuilder(String.Empty)
                Dim totalAmount As String = stbInvoiceAmount.Text
                totalAmountBuilder.Append("Total Amount")
                totalAmountBuilder.Append(totalAmount.PadLeft(padAmount))
                accountStatement.Add(New PrintParagraps(bodyBoldFont, totalAmountBuilder.ToString()))


            End If

            If Me.dgvInvoiceAdjustments.Rows.Count > 0 Then
                Dim accountHeader As New System.Text.StringBuilder(String.Empty)
                accountHeader.Append(ControlChars.NewLine)
                accountStatement.Add(New PrintParagraps(bodyBoldFont, accountHeader.ToString()))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, Me.GetInvoiceAdjustmentPrintData()))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, ControlChars.NewLine))
                Dim totalAmountBuilder As New System.Text.StringBuilder(String.Empty)
                Dim totalAmount As String = stbInvoiceAdjustmentAmount.Text
                totalAmountBuilder.Append("Total Amount")
                totalAmountBuilder.Append(totalAmount.PadLeft(padAmount))
                accountStatement.Add(New PrintParagraps(bodyBoldFont, totalAmountBuilder.ToString()))


            End If

            If Me.dgvExtraBillItems.Rows.Count > 0 Then
                Dim accountHeader As New System.Text.StringBuilder(String.Empty)
                accountHeader.Append(ControlChars.NewLine)
                accountStatement.Add(New PrintParagraps(bodyBoldFont, accountHeader.ToString()))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, Me.GetExtraBillPrintData))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, ControlChars.NewLine))
                Dim totalAmountBuilder As New System.Text.StringBuilder(String.Empty)
                Dim totalAmount As String = stbExtraBillAmount.Text
                totalAmountBuilder.Append("Total Amount")
                totalAmountBuilder.Append(totalAmount.PadLeft(padAmount))
                accountStatement.Add(New PrintParagraps(bodyBoldFont, totalAmountBuilder.ToString()))
            End If


            If Me.dgvCashReceipts.Rows.Count > 0 Then
                Dim accountHeader As New System.Text.StringBuilder(String.Empty)
                accountHeader.Append(ControlChars.NewLine)
                accountStatement.Add(New PrintParagraps(bodyBoldFont, accountHeader.ToString()))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, Me.GeOPDPaymentPrintData))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, ControlChars.NewLine))
                Dim totalAmountBuilder As New System.Text.StringBuilder(String.Empty)
                Dim totalAmount As String = stbTotalPayments.Text
                totalAmountBuilder.Append("Total Amount")
                totalAmountBuilder.Append(totalAmount.PadLeft(padAmount))
                accountStatement.Add(New PrintParagraps(bodyBoldFont, totalAmountBuilder.ToString()))
            End If

            If Me.dgvBillFormPayments.Rows.Count > 0 Then
                Dim accountHeader As New System.Text.StringBuilder(String.Empty)
                accountHeader.Append(ControlChars.NewLine)
                accountStatement.Add(New PrintParagraps(bodyBoldFont, accountHeader.ToString()))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, Me.GetBFPPaymentPrintData))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, ControlChars.NewLine))
                Dim totalAmountBuilder As New System.Text.StringBuilder(String.Empty)
                Dim totalAmount As String = stbBillFormAmount.Text
                totalAmountBuilder.Append("Total Amount")
                totalAmountBuilder.Append(totalAmount.PadLeft(padAmount))
                accountStatement.Add(New PrintParagraps(bodyBoldFont, totalAmountBuilder.ToString()))
            End If

            If Me.dgvRefunds.Rows.Count > 0 Then
                Dim refundHeader As New System.Text.StringBuilder(String.Empty)
                refundHeader.Append(ControlChars.NewLine)
                accountStatement.Add(New PrintParagraps(bodyBoldFont, refundHeader.ToString()))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, Me.GetRefundPrintData()))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, ControlChars.NewLine))
                Dim totalAmountBuilder As New System.Text.StringBuilder(String.Empty)
                Dim totalAmount As String = stbTotalRefunds.Text
                totalAmountBuilder.Append("Total Amount")
                totalAmountBuilder.Append(totalAmount.PadLeft(padAmount))
                accountStatement.Add(New PrintParagraps(bodyBoldFont, totalAmountBuilder.ToString()))
            End If

            If Me.dgvExtraBillRefunds.Rows.Count > 0 Then
                Dim extraBillefundHeader As New System.Text.StringBuilder(String.Empty)
                extraBillefundHeader.Append(ControlChars.NewLine)
                accountStatement.Add(New PrintParagraps(bodyBoldFont, extraBillefundHeader.ToString()))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, Me.GetBFRefundPrintData()))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, ControlChars.NewLine))
                Dim totalAmountBuilder As New System.Text.StringBuilder(String.Empty)
                Dim totalAmount As String = stbExtraBillRefunds.Text
                totalAmountBuilder.Append("Total Amount")
                totalAmountBuilder.Append(totalAmount.PadLeft(padAmount))
                accountStatement.Add(New PrintParagraps(bodyBoldFont, totalAmountBuilder.ToString()))
            End If


            If Me.dgvAccountTrasaction.Rows.Count > 0 Then
                Dim accountHeader As New System.Text.StringBuilder(String.Empty)
                accountHeader.Append(ControlChars.NewLine)
                accountStatement.Add(New PrintParagraps(bodyBoldFont, accountHeader.ToString()))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, Me.GetAccountTransactionsPrintData()))
                accountStatement.Add(New PrintParagraps(bodyNormalFont, ControlChars.NewLine))
                Dim totalAmountBuilder As New System.Text.StringBuilder(String.Empty)
                Dim totalAmount As String = stbNetAmount.Text
                totalAmountBuilder.Append("Total Amount")
                totalAmountBuilder.Append(totalAmount.PadLeft(padAmount))
                accountStatement.Add(New PrintParagraps(bodyBoldFont, totalAmountBuilder.ToString()))
            End If

            Dim netAmountBuilder As New System.Text.StringBuilder(String.Empty)
            Dim balance As Decimal = DecimalMayBeEnteredIn(stbBalance)
            netAmountBuilder.Append(ControlChars.NewLine)
            netAmountBuilder.Append("Balance")
            netAmountBuilder.Append(stbBalance.Text.PadLeft(padAmount + 5))
            If Not balance.Equals(0) Then
                netAmountBuilder.Append(ControlChars.NewLine)
                netAmountBuilder.Append("(" + stbBalanceWords.Text.Trim() + ")")
            End If

            accountStatement.Add(New PrintParagraps(bodyBoldFont, netAmountBuilder.ToString()))


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim patientSignData As New System.Text.StringBuilder(String.Empty)
            patientSignData.Append(ControlChars.NewLine)
            patientSignData.Append(ControlChars.NewLine)

            patientSignData.Append("Received By:      " + GetCharacters("."c, 20))
            patientSignData.Append(GetSpaces(4))
            patientSignData.Append("Date:         " + GetCharacters("."c, 20))
            patientSignData.Append(ControlChars.NewLine)
            accountStatement.Add(New PrintParagraps(footerFont, patientSignData.ToString()))

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim checkedSignData As New System.Text.StringBuilder(String.Empty)
            checkedSignData.Append(ControlChars.NewLine)

            checkedSignData.Append("Checked By:       " + GetCharacters("."c, 20))
            checkedSignData.Append(GetSpaces(4))
            checkedSignData.Append("Date:         " + GetCharacters("."c, 20))
            checkedSignData.Append(ControlChars.NewLine)
            accountStatement.Add(New PrintParagraps(footerFont, checkedSignData.ToString()))

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim footerData As New System.Text.StringBuilder(String.Empty)
            footerData.Append(ControlChars.NewLine)
            footerData.Append("Printed by " + CurrentUser.FullName + " on " + FormatDate(Now) + " at " +
                              Now.ToString("hh:mm tt") + " from " + AppData.AppTitle)
            footerData.Append(ControlChars.NewLine)
            accountStatement.Add(New PrintParagraps(footerFont, footerData.ToString()))

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Function GetInvoicePrintData() As String

        Dim padItemNo As Integer = 4
        Dim padVisitNo As Integer = 20
        Dim padInvoiceNo As Integer = 20
        Dim padInvoiceDate As Integer = 20
        Dim padAmount As Integer = 20



        Dim invoices As New System.Text.StringBuilder(String.Empty)

        Try


            Dim tableHeader As New System.Text.StringBuilder("Invoices")
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append("No".PadRight(padItemNo))
            tableHeader.Append("Visit No".PadRight(padVisitNo))
            tableHeader.Append("Invoice No".PadRight(padInvoiceNo))
            tableHeader.Append("Invoice Date".PadRight(padInvoiceDate))
            tableHeader.Append("Amount".PadRight(padAmount))

            tableHeader.Append(ControlChars.NewLine)
            invoices.Append(tableHeader)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim tableData As New System.Text.StringBuilder(String.Empty)
            Dim itemCount As Integer = 0

            For rowNo As Integer = 0 To Me.dgvInvoices.RowCount - 1

                Dim cells As DataGridViewCellCollection = Me.dgvInvoices.Rows(rowNo).Cells

                itemCount += 1

                Dim itemNo As String = (itemCount).ToString()
                Dim visitNo As String = StringMayBeEnteredIn(cells, Me.colInvVisitNo)
                Dim invoiceNo As String = StringMayBeEnteredIn(cells, colInvInvoiceNo)
                Dim invoiceDate As String = StringMayBeEnteredIn(cells, colInvoiceDate)
                Dim amount As String = StringMayBeEnteredIn(cells, Me.colInvoiceAmount)


                tableData.Append(itemNo.PadRight(padItemNo))
                tableData.Append(visitNo.PadRight(padVisitNo))
                tableData.Append(invoiceNo.PadRight(padInvoiceNo))
                tableData.Append(invoiceDate.PadRight(padInvoiceDate))
                tableData.Append(amount.PadRight(padAmount))


                tableData.Append(ControlChars.NewLine)

            Next

            invoices.Append(tableData)



        Catch ex As Exception
            Throw ex
        End Try
        Return invoices.ToString()
    End Function

    Private Function GetInvoiceAdjustmentPrintData() As String


        Dim padItemNo As Integer = 4
        Dim padVisitNo As Integer = 16
        Dim padInvoiceNo As Integer = 12
        Dim padAdjustmentNo As Integer = 15
        Dim padAdjustmentDate As Integer = 17
        Dim padAmount As Integer = 20


        Dim invoiceAdjustments As New System.Text.StringBuilder(String.Empty)

        Try


            Dim tableHeader As New System.Text.StringBuilder("Invoice Adjustments")
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append("No".PadRight(padItemNo))
            tableHeader.Append("Visit No".PadRight(padVisitNo))
            tableHeader.Append("Invoice No".PadRight(padInvoiceNo))
            tableHeader.Append("Adjustment No".PadRight(padAdjustmentNo))
            tableHeader.Append("Adjustment Date".PadRight(padAdjustmentDate))
            tableHeader.Append("Amount".PadRight(padAmount))

            tableHeader.Append(ControlChars.NewLine)
            invoiceAdjustments.Append(tableHeader)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim tableData As New System.Text.StringBuilder(String.Empty)
            Dim itemCount As Integer = 0

            For rowNo As Integer = 0 To Me.dgvInvoiceAdjustments.RowCount - 1

                Dim cells As DataGridViewCellCollection = Me.dgvInvoiceAdjustments.Rows(rowNo).Cells

                itemCount += 1

                Dim itemNo As String = (itemCount).ToString()
                Dim visitNo As String = StringMayBeEnteredIn(cells, Me.colIVAVisitNo)
                Dim invoiceNo As String = StringMayBeEnteredIn(cells, colIVAInvoiceNo)
                Dim adjustmentNo As String = StringMayBeEnteredIn(cells, colAdjustmentNo)
                Dim adjustmentDate As String = StringMayBeEnteredIn(cells, colAdjustmentDate)
                Dim amount As String = StringMayBeEnteredIn(cells, Me.colIVAAmount)


                tableData.Append(itemNo.PadRight(padItemNo))
                tableData.Append(visitNo.PadRight(padVisitNo))
                tableData.Append(invoiceNo.PadRight(padInvoiceNo))
                tableData.Append(adjustmentNo.PadRight(padAdjustmentNo))
                tableData.Append(adjustmentDate.PadRight(padAdjustmentDate))
                tableData.Append(amount.PadRight(padAmount))


                tableData.Append(ControlChars.NewLine)

            Next

            invoiceAdjustments.Append(tableData)



        Catch ex As Exception
            Throw ex
        End Try
        Return invoiceAdjustments.ToString()
    End Function


    Private Function GetExtraBillPrintData() As String

        Dim padItemNo As Integer = 4
        Dim padVisitNo As Integer = 20
        Dim padExtraBillNo As Integer = 20
        Dim padExtraBillDate As Integer = 20
        Dim padAmount As Integer = 20


        Dim _extraBills As New System.Text.StringBuilder(String.Empty)

        Try


            Dim tableHeader As New System.Text.StringBuilder("Bill Form")
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append("No".PadRight(padItemNo))
            tableHeader.Append("Visit No".PadRight(padVisitNo))
            tableHeader.Append("Extra Bill No".PadRight(padExtraBillNo))
            tableHeader.Append("Extra Bill Date".PadRight(padExtraBillDate))
            tableHeader.Append("Amount".PadRight(padAmount))


            tableHeader.Append(ControlChars.NewLine)
            _extraBills.Append(tableHeader)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim tableData As New System.Text.StringBuilder(String.Empty)
            Dim itemCount As Integer = 0

            For rowNo As Integer = 0 To Me.dgvExtraBillItems.RowCount - 1

                Dim cells As DataGridViewCellCollection = Me.dgvExtraBillItems.Rows(rowNo).Cells

                itemCount += 1

                Dim itemNo As String = (itemCount).ToString()
                Dim visitNo As String = StringMayBeEnteredIn(cells, Me.colBFVisitNo)
                Dim extraBillNo As String = StringMayBeEnteredIn(cells, Me.colExtraBillNo)
                Dim extraBillDate As String = StringMayBeEnteredIn(cells, colExtraBillDate)
                Dim amount As String = StringMayBeEnteredIn(cells, Me.colBFAmount)

                tableData.Append(itemNo.PadRight(padItemNo))
                tableData.Append(visitNo.PadRight(padVisitNo))
                tableData.Append(visitNo.PadRight(padExtraBillNo))
                tableData.Append(extraBillDate.PadRight(padExtraBillDate))
                tableData.Append(amount.PadRight(padAmount))
                tableData.Append(ControlChars.NewLine)

            Next

            _extraBills.Append(tableData)


        Catch ex As Exception
            Throw ex
        End Try
        Return _extraBills.ToString()
    End Function


    Private Function GeOPDPaymentPrintData() As String

        Dim padItemNo As Integer = 4
        Dim padVisitNo As Integer = 16
        Dim padReceiptNo As Integer = 14
        Dim padInvoiceNo As Integer = 14
        Dim padRecordDate As Integer = 16
        Dim padAmount As Integer = 16

        Dim _OPDPayments As New System.Text.StringBuilder(String.Empty)

        Try


            Dim tableHeader As New System.Text.StringBuilder("OPD Payments")
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append("No".PadRight(padItemNo))
            tableHeader.Append("Visit No".PadRight(padVisitNo))
            tableHeader.Append("Receipt No".PadRight(padReceiptNo))
            tableHeader.Append("Invoice No".PadRight(padInvoiceNo))
            tableHeader.Append("Record Date".PadRight(padRecordDate))
            tableHeader.Append("Amount".PadRight(padAmount))



            tableHeader.Append(ControlChars.NewLine)
            _OPDPayments.Append(tableHeader)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim tableData As New System.Text.StringBuilder(String.Empty)
            Dim itemCount As Integer = 0

            For rowNo As Integer = 0 To Me.dgvCashReceipts.RowCount - 1

                Dim cells As DataGridViewCellCollection = Me.dgvCashReceipts.Rows(rowNo).Cells

                itemCount += 1

                Dim itemNo As String = (itemCount).ToString()
                Dim visitNo As String = StringMayBeEnteredIn(cells, Me.colPayVisitNo)
                Dim receiptNo As String = StringMayBeEnteredIn(cells, Me.colPayReceiptNo)
                Dim invoiceNo As String = StringMayBeEnteredIn(cells, Me.colPayInvoiceNo)
                Dim RecordDate As String = StringMayBeEnteredIn(cells, Me.colPayRecordDate)
                Dim amount As String = StringMayBeEnteredIn(cells, Me.colPayAmount)

                tableData.Append(itemNo.PadRight(padItemNo))
                tableData.Append(visitNo.PadRight(padVisitNo))
                tableData.Append(receiptNo.PadRight(padReceiptNo))
                tableData.Append(invoiceNo.PadRight(padInvoiceNo))
                tableData.Append(RecordDate.PadRight(padRecordDate))
                tableData.Append(amount.PadRight(padAmount))



                tableData.Append(ControlChars.NewLine)

            Next
            _OPDPayments.Append(tableData)


        Catch ex As Exception
            Throw ex
        End Try
        Return _OPDPayments.ToString()
    End Function

    Private Function GetBFPPaymentPrintData() As String

        Dim padItemNo As Integer = 4
        Dim padVisitNo As Integer = 16
        Dim padReceiptNo As Integer = 12
        Dim padExtraBillNo As Integer = 16
        Dim padRecordDate As Integer = 16
        Dim padAmount As Integer = 16


        Dim _OPDPayments As New System.Text.StringBuilder(String.Empty)

        Try


            Dim tableHeader As New System.Text.StringBuilder("Bill Form Payments")
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append("No".PadRight(padItemNo))
            tableHeader.Append("Visit No".PadRight(padVisitNo))
            tableHeader.Append("Receipt No".PadRight(padReceiptNo))
            tableHeader.Append("Extra Bill No".PadRight(padExtraBillNo))
            tableHeader.Append("Record Date".PadRight(padRecordDate))
            tableHeader.Append("Amount".PadRight(padAmount))



            tableHeader.Append(ControlChars.NewLine)
            _OPDPayments.Append(tableHeader)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim tableData As New System.Text.StringBuilder(String.Empty)
            Dim itemCount As Integer = 0

            For rowNo As Integer = 0 To Me.dgvBillFormPayments.RowCount - 1

                Dim cells As DataGridViewCellCollection = Me.dgvBillFormPayments.Rows(rowNo).Cells

                itemCount += 1

                Dim itemNo As String = (itemCount).ToString()
                Dim visitNo As String = StringMayBeEnteredIn(cells, Me.colBFPVisitNo)
                Dim receiptNo As String = StringMayBeEnteredIn(cells, Me.colBFPReceiptNo)
                Dim extraBillNo As String = StringMayBeEnteredIn(cells, Me.colBFPExtraBillNo)
                Dim RecordDate As String = StringMayBeEnteredIn(cells, Me.colBFPRecordDate)
                Dim amount As String = StringMayBeEnteredIn(cells, Me.colBFPAmount)


                tableData.Append(itemNo.PadRight(padItemNo))
                tableData.Append(visitNo.PadRight(padVisitNo))
                tableData.Append(receiptNo.PadRight(padReceiptNo))
                tableData.Append(extraBillNo.PadRight(padExtraBillNo))
                tableData.Append(RecordDate.PadRight(padRecordDate))
                tableData.Append(amount.PadRight(padAmount))



                tableData.Append(ControlChars.NewLine)

            Next
            _OPDPayments.Append(tableData)


        Catch ex As Exception
            Throw ex
        End Try
        Return _OPDPayments.ToString()
    End Function


    Private Function GetRefundPrintData() As String

        Dim padItemNo As Integer = 4
        Dim padReceiptNo As Integer = 14
        Dim padRefundNo As Integer = 16
        Dim padInvoiceNo As Integer = 14
        Dim padAmount As Integer = 16
        Dim padRecordDate As Integer = 16


        Dim refundPrintString As New System.Text.StringBuilder(String.Empty)
        Try

            Dim tableHeader As New System.Text.StringBuilder("OPD Refunds")
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append("No".PadRight(padItemNo))
            tableHeader.Append("Receipt No".PadRight(padReceiptNo))
            tableHeader.Append("Invoice No".PadRight(padInvoiceNo))
            tableHeader.Append("Refund No".PadRight(padRefundNo))
            tableHeader.Append("Record Date".PadRight(padRecordDate))
            tableHeader.Append("Amount".PadRight(padAmount))

            tableHeader.Append(ControlChars.NewLine)
            refundPrintString.Append(tableHeader)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim tableData As New System.Text.StringBuilder(String.Empty)
            Dim itemCount As Integer = 0

            For rowNo As Integer = 0 To Me.dgvRefunds.RowCount - 1

                Dim cells As DataGridViewCellCollection = Me.dgvRefunds.Rows(rowNo).Cells

                itemCount += 1

                Dim itemNo As String = (itemCount).ToString()
                Dim receiptNo As String = StringMayBeEnteredIn(cells, Me.colRefReceiptNo)
                Dim invoiceNo As String = StringMayBeEnteredIn(cells, Me.colRefInvoiceNo)
                Dim refundNo As String = StringMayBeEnteredIn(cells, Me.colRefRefundNo)
                Dim amount As String = StringMayBeEnteredIn(cells, Me.colRefAmount)
                Dim recordDate As String = StringMayBeEnteredIn(cells, colRefundRecordDate)

                tableData.Append(itemNo.PadRight(padItemNo))
                tableData.Append(receiptNo.PadRight(padReceiptNo))
                tableData.Append(invoiceNo.PadRight(padInvoiceNo))
                tableData.Append(refundNo.PadRight(padRefundNo))
                tableData.Append(recordDate.PadRight(padRecordDate))
                tableData.Append(amount.PadRight(padAmount))



                tableData.Append(ControlChars.NewLine)

            Next

            refundPrintString.Append(tableData)




        Catch ex As Exception
            Throw ex
        End Try
        Return refundPrintString.ToString()
    End Function

    Private Function GetBFRefundPrintData() As String

        Dim padItemNo As Integer = 4
        Dim padReceiptNo As Integer = 14
        Dim padRefundNo As Integer = 14
        Dim padExtraBillNo As Integer = 16
        Dim padAmount As Integer = 16
        Dim padRecordDate As Integer = 16


        Dim refundPrintString As New System.Text.StringBuilder(String.Empty)
        Try

            Dim tableHeader As New System.Text.StringBuilder("Bill Form Refunds")
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append("No".PadRight(padItemNo))
            tableHeader.Append("Receipt No".PadRight(padReceiptNo))
            tableHeader.Append("Extra Bill No".PadRight(padExtraBillNo))
            tableHeader.Append("Refund No".PadRight(padRefundNo))
            tableHeader.Append("Record Date".PadRight(padRecordDate))
            tableHeader.Append("Amount".PadRight(padAmount))

            tableHeader.Append(ControlChars.NewLine)
            refundPrintString.Append(tableHeader)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim tableData As New System.Text.StringBuilder(String.Empty)
            Dim itemCount As Integer = 0

            For rowNo As Integer = 0 To Me.dgvExtraBillRefunds.RowCount - 1

                Dim cells As DataGridViewCellCollection = Me.dgvExtraBillRefunds.Rows(rowNo).Cells

                itemCount += 1

                Dim itemNo As String = (itemCount).ToString()
                Dim receiptNo As String = StringMayBeEnteredIn(cells, Me.colEXTRefReceiptNo)
                Dim extraBillNo As String = StringMayBeEnteredIn(cells, Me.colEXTRefExtraBillNo)
                Dim refundNo As String = StringMayBeEnteredIn(cells, Me.colEXTRefRefundNo)
                Dim amount As String = StringMayBeEnteredIn(cells, Me.colEXTRefAmount)
                Dim recordDate As String = StringMayBeEnteredIn(cells, colEXTRefRecordDate)

                tableData.Append(itemNo.PadRight(padItemNo))
                tableData.Append(receiptNo.PadRight(padReceiptNo))
                tableData.Append(extraBillNo.PadRight(padExtraBillNo))
                tableData.Append(refundNo.PadRight(padRefundNo))
                tableData.Append(recordDate.PadRight(padRecordDate))
                tableData.Append(amount.PadRight(padAmount))



                tableData.Append(ControlChars.NewLine)

            Next

            refundPrintString.Append(tableData)




        Catch ex As Exception
            Throw ex
        End Try
        Return refundPrintString.ToString()
    End Function


    Private Function GetAccountTransactionsPrintData() As String

        Dim padItemNo As Integer = 4
        Dim padTranNo As Integer = 12
        Dim padRecordDate As Integer = 16
        Dim padCredit As Integer = 16
        Dim padDebit As Integer = 16
        Dim padBalance As Integer = 16



        pageNo = 0
        Dim accountTransactionsParagraphy As New System.Text.StringBuilder("Account Transactions")

        Try


            Dim tableHeader As New System.Text.StringBuilder(String.Empty)
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append("No".PadRight(padItemNo))
            tableHeader.Append("Tran No".PadRight(padTranNo))
            tableHeader.Append("Tran Date".PadRight(padRecordDate))
            tableHeader.Append("Credit".PadRight(padCredit))
            tableHeader.Append("Debit".PadRight(padDebit))
            tableHeader.Append("Balance".PadRight(padBalance))



            tableHeader.Append(ControlChars.NewLine)
            accountTransactionsParagraphy.Append(tableHeader)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim tableData As New System.Text.StringBuilder(String.Empty)
            Dim itemCount As Integer = 0

            For rowNo As Integer = 0 To Me.dgvAccountTrasaction.RowCount - 1

                Dim cells As DataGridViewCellCollection = Me.dgvAccountTrasaction.Rows(rowNo).Cells

                itemCount += 1

                Dim itemNo As String = (itemCount).ToString()
                Dim tranNo As String = StringMayBeEnteredIn(cells, Me.colTranNo)
                Dim recordDate As String = StringMayBeEnteredIn(cells, colAccRecordDate)
                Dim credit As String = StringMayBeEnteredIn(cells, Me.colCredit)
                Dim debit As String = StringMayBeEnteredIn(cells, Me.colDebit)
                Dim balance As String = StringMayBeEnteredIn(cells, Me.colBalance)
                Dim notes As String = StringMayBeEnteredIn(cells, Me.colAccNotes)


                tableData.Append(itemNo.PadRight(padItemNo))
                tableData.Append(tranNo.PadRight(padTranNo))
                tableData.Append(recordDate.PadRight(padRecordDate))
                tableData.Append(credit.PadRight(padCredit))
                tableData.Append(debit.PadRight(padDebit))
                tableData.Append(balance.PadRight(padBalance))



                tableData.Append(ControlChars.NewLine)

            Next

            accountTransactionsParagraphy.Append(tableData)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


        Catch ex As Exception
            Throw ex
        End Try
        Return accountTransactionsParagraphy.ToString()
    End Function


    Private Sub PrintPatientData()

        Dim dlgPrint As New PrintDialog()

        Try

            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.SetAccountStatementPrintData()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            dlgPrint.Document = docPrintAccountStatement

            dlgPrint.Document.PrinterSettings.Collate = True
            If dlgPrint.ShowDialog = DialogResult.OK Then docPrintAccountStatement.Print()

        Catch ex As Exception
            Throw ex

        End Try

    End Sub



    Private Sub btnPrintPreview_Click(sender As Object, e As EventArgs) Handles btnPrintPreview.Click

        Try

            Me.Cursor = Cursors.WaitCursor

            ' Make a PrintDocument and attach it to the PrintPreview dialog.
            Dim dlgPrintPreview As New PrintPreviewDialog()

            Me.SetAccountStatementPrintData()

            With dlgPrintPreview
                .Document = docPrintAccountStatement
                .Document.PrinterSettings.Collate = True
                .ShowIcon = False
                .WindowState = FormWindowState.Maximized
                .ShowDialog()
            End With

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Try

            Me.Cursor = Cursors.WaitCursor

            Me.PrintPatientData()

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub


#End Region


    Private Sub cboCompanyNo_Leave(sender As Object, e As System.EventArgs) Handles cboCompanyNo.Leave
        Dim companyName As String
        Dim oBillModesID As New LookupDataID.BillModesID()

        Try

            Dim companyNo As String = RevertText(SubstringRight(StringMayBeEnteredIn(Me.cboCompanyNo)))
            Dim billModesID As String = StringValueMayBeEnteredIn(Me.cboBillMode, "To-Bill Account Category!")

            If String.IsNullOrEmpty(billModesID) Then Return


            Select Case billModesID.ToUpper()

                Case oBillModesID.Cash.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Account.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    If Not String.IsNullOrEmpty(companyNo) Then

                        Me.cboCompanyNo.Text = FormatText(companyNo, "BillCustomers", "AccountNo").ToUpper()
                        For Each row As DataRow In billCompanies.Select("AccountNo = '" + companyNo + "'")

                            If Not IsDBNull(row.Item("BillCustomerName")) Then
                                companyName = StringEnteredIn(row, "BillCustomerName")
                                companyNo = StringMayBeEnteredIn(row, "AccountNo")
                                Me.cboCompanyNo.Text = FormatText(companyNo, "BillCustomers", "AccountNo").ToUpper()
                            Else
                                companyName = String.Empty
                                companyNo = String.Empty
                            End If

                            Me.stbCompanyName.Text = companyName
                        Next

                    End If
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Insurance.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    If Not String.IsNullOrEmpty(companyNo) Then

                        Me.cboCompanyNo.Text = FormatText(companyNo, "Companies", "CompanyNo").ToUpper()

                        For Each row As DataRow In insuranceCompanies.Select("CompanyNo = '" + companyNo + "'")

                            If Not IsDBNull(row.Item("CompanyName")) Then
                                companyName = StringEnteredIn(row, "CompanyName")
                                companyNo = StringMayBeEnteredIn(row, "CompanyNo")
                                Me.cboCompanyNo.Text = FormatText(companyNo, "Companies", "CompanyNo").ToUpper()
                            Else
                                companyName = String.Empty
                                companyNo = String.Empty
                            End If

                            Me.stbCompanyName.Text = companyName
                        Next

                    End If
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End Select
            SetBalances()


        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub


    Private Sub tbcAccountStatement_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles tbcAccountStatement.SelectedIndexChanged
        TabSelectedTabs()
    End Sub

    Private Sub fbnExport_Click(sender As System.Object, e As System.EventArgs) Handles fbnExport.Click


        Select Case Me.tbcAccountStatement.SelectedTab.Name
            Case tpgInvoices.Name
                ExportToExcel(dgvInvoices, tpgInvoices.Text)

            Case Me.tpgInvoiceAdjustments.Name
                ExportToExcel(dgvInvoiceAdjustments, tpgInvoiceAdjustments.Text)
            Case Me.tpgExtraBills.Name
                ExportToExcel(dgvExtraBillItems, tpgExtraBills.Text)
            Case Me.tpgOPDPayments.Name
                ExportToExcel(dgvCashReceipts, tpgOPDPayments.Text)

            Case Me.tpgBillFormPayments.Name
                ExportToExcel(dgvBillFormPayments, tpgBillFormPayments.Text)
            Case Me.tpgAccountTransactions.Name
                ExportToExcel(dgvAccountTrasaction, tpgAccountTransactions.Text)

            Case Me.tpgOPDRefunds.Name
                ExportToExcel(dgvRefunds, tpgOPDRefunds.Text)

            Case Me.tpgExtraBillRefunds.Name
                ExportToExcel(dgvExtraBillRefunds, tpgExtraBillRefunds.Text)
        End Select
    End Sub

    Private Sub stbMainMemberNo_Leave(sender As Object, e As System.EventArgs) Handles stbMainMemberNo.Leave
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim oSchemeMembers As New SchemeMembers()
            Dim enteredMainMemberNo As String = StringMayBeEnteredIn(stbMainMemberNo)
            If String.IsNullOrEmpty(enteredMainMemberNo) Then Return
            Dim schemeMembers As DataTable = oSchemeMembers.GetSchemeMembers(enteredMainMemberNo).Tables("SchemeMembers")
            Dim row As DataRow = schemeMembers.Rows(0)
            Me.Text = "Main Member Statement"
            Dim mainMemeberNo As String = StringEnteredIn(row, "MainMemberNo")
            If Not enteredMainMemberNo.ToUpper().Equals(mainMemeberNo.ToUpper()) Then Throw New Exception("The MedicalCard No: " + enteredMainMemberNo + " you have entered does not belong to any Main Member")
            stbMainMemberName.Text = StringEnteredIn(row, "FullName")
            SetBalances()
            Me.LoadData()
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub stbMainMemberNo_TextChanged(sender As System.Object, e As System.EventArgs) Handles stbMainMemberNo.TextChanged
        Me.stbMainMemberName.Clear()
        Me.cboAccountNo.Text = String.Empty
        Me.stbAccountName.Text = String.Empty
        Me.cboCompanyNo.Text = String.Empty
        Me.ClearControls()
    End Sub

    Private Sub cboCompanyNo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboCompanyNo.SelectedIndexChanged
        Me.ClearControls()
    End Sub

    Private Sub cmsEdit_Opening(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles cmsEdit.Opening
        Try
            Me.Cursor = Cursors.WaitCursor
            Select Case Me.tbcAccountStatement.SelectedTab.Name
                Case tpgInvoices.Name
                    enableContextMenu(dgvInvoices)
                Case Me.tpgInvoiceAdjustments.Name
                    enableContextMenu(dgvInvoiceAdjustments)
                Case Me.tpgExtraBills.Name
                    enableContextMenu(dgvExtraBillItems)
                Case Me.tpgOPDPayments.Name
                    enableContextMenu(dgvCashReceipts)
                Case Me.tpgBillFormPayments.Name
                    enableContextMenu(dgvBillFormPayments)
                Case Me.tpgAccountTransactions.Name
                    enableContextMenu(dgvAccountTrasaction)
                Case Me.tpgOPDRefunds.Name
                    enableContextMenu(dgvRefunds)
                Case Me.tpgExtraBillRefunds.Name
                    enableContextMenu(dgvExtraBillRefunds)
            End Select
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub enableContextMenu(dataGridView As DataGridView)
        If dataGridView.ColumnCount < 1 OrElse dataGridView.RowCount < 1 Then
            Me.cmsEditCopy.Enabled = False
            Me.cmsEditSelectAll.Enabled = False
        Else
            Me.cmsEditCopy.Enabled = True
            Me.cmsEditSelectAll.Enabled = True
        End If
    End Sub

    Private Sub cmsEditCopy_Click(sender As System.Object, e As System.EventArgs) Handles cmsEditCopy.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            Select Case Me.tbcAccountStatement.SelectedTab.Name
                Case tpgInvoices.Name
                    copyText(dgvInvoices)
                Case Me.tpgInvoiceAdjustments.Name
                    copyText(dgvInvoiceAdjustments)
                Case Me.tpgExtraBills.Name
                    copyText(dgvExtraBillItems)
                Case Me.tpgOPDPayments.Name
                    copyText(dgvCashReceipts)
                Case Me.tpgBillFormPayments.Name
                    copyText(dgvBillFormPayments)
                Case Me.tpgAccountTransactions.Name
                    copyText(dgvAccountTrasaction)
                Case Me.tpgOPDRefunds.Name
                    copyText(dgvRefunds)
                Case Me.tpgExtraBillRefunds.Name
                    copyText(dgvExtraBillRefunds)
            End Select
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub copyText(dataGridView As DataGridView)
        Clipboard.SetText(CopyFromControl(dataGridView))
    End Sub

    Private Sub cmsEditSelectAll_Click(sender As System.Object, e As System.EventArgs) Handles cmsEditSelectAll.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            Select Case Me.tbcAccountStatement.SelectedTab.Name
                Case tpgInvoices.Name
                    SelectAll(dgvInvoices)
                Case Me.tpgInvoiceAdjustments.Name
                    SelectAll(dgvInvoiceAdjustments)
                Case Me.tpgExtraBills.Name
                    SelectAll(dgvExtraBillItems)
                Case Me.tpgOPDPayments.Name
                    SelectAll(dgvCashReceipts)
                Case Me.tpgBillFormPayments.Name
                    SelectAll(dgvBillFormPayments)
                Case Me.tpgAccountTransactions.Name
                    SelectAll(dgvAccountTrasaction)
                Case Me.tpgOPDRefunds.Name
                    SelectAll(dgvRefunds)
                Case Me.tpgExtraBillRefunds.Name
                    SelectAll(dgvExtraBillRefunds)
            End Select
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub SelectAll(dataGridView As DataGridView)
        dataGridView.SelectAll()
    End Sub

    Private Sub cboAccountNo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboAccountNo.SelectedIndexChanged
        cboCompanyNo.Text = String.Empty
        Me.ClearControls()
    End Sub

    Private Sub btnShowOutStandingBalance_Click(sender As System.Object, e As System.EventArgs) Handles btnShowOutStandingBalance.Click
        Try
            Me.stbOutStandingBalance.Clear()
            Me.Cursor = Cursors.WaitCursor
            Me.SeOutStandingBalance()
            Me.btnShowOutStandingBalance.Enabled = False
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub
End Class