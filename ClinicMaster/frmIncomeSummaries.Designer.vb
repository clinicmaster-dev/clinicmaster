<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIncomeSummaries
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmIncomeSummaries))
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle47 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle48 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle36 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle37 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle38 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle39 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle40 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle41 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle42 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle43 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle44 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle45 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle46 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle49 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle50 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle63 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle64 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle51 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle52 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle53 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle54 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle55 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle56 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle57 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle58 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle59 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle60 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle61 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle62 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle65 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle66 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle69 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle70 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle67 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle68 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle71 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle72 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle79 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle80 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle73 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle74 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle75 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle76 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle77 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle78 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle81 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle82 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle89 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle90 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle83 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle84 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle85 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle86 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle87 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle88 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.tbcPeriodicReport = New System.Windows.Forms.TabControl()
        Me.tpgIncomeServicePoint = New System.Windows.Forms.TabPage()
        Me.dgvIncomeServicePoint = New System.Windows.Forms.DataGridView()
        Me.colIncomeCategory = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotalAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCashAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCoPayAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTotalCash = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCashPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCashDiscount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCashNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCoPayNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAccountAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAccountNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInsuranceAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInsuranceNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmsIncomeSummaries = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmsIncomeSummariesCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmsIncomeSummariesSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.tpgExtraCharges = New System.Windows.Forms.TabPage()
        Me.dgvExtraChargeSummaries = New System.Windows.Forms.DataGridView()
        Me.colExtraChargeCategory = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraTotalAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraCashAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraCoPayAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraTotalCash = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraCashPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraCashDiscount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraCashNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraCoPayNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraAccountAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraAccountNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraInsuranceAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraInsuranceNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgDoctorVisits = New System.Windows.Forms.TabPage()
        Me.dgvDoctorVisitSummaries = New System.Windows.Forms.DataGridView()
        Me.colDoctorSeenDoctor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorTotalVisits = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorTotalOnServices = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorTotalAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorCashAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorCoPayAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorTotalCash = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorCashPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorCashDiscount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorCashNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorAccountAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorAccountNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorInsuranceAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorInsuranceNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgDoctorSpecialtyVisits = New System.Windows.Forms.TabPage()
        Me.dgvDoctorSpecialtyVisitSummaries = New System.Windows.Forms.DataGridView()
        Me.colDoctorSpecialtySeenDoctorSpecialty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorSpecialtyTotalVisits = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorSpecialtyTotalOnServices = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorSpecialtyTotalAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorSpecialtyCashAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorSpecialtyCoPayAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorSpecialtyTotalCash = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorSpecialtyCashPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorSpecialtyCashDiscount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorSpecialtyCashNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorSpecialtyAccountAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorSpecialtyAccountNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorSpecialtyInsuranceAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDoctorSpecialtyInsuranceNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgIncomePaymentDetails = New System.Windows.Forms.TabPage()
        Me.dgvIncomePaymentDetails = New System.Windows.Forms.DataGridView()
        Me.colPaymentDetailsReceiptNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPaymentDetailsVisitDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPaymentDetailsFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIncomePaymentDetailsCategory = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPaymentDetailsTotalAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPaymentDetailsPaidAfterDays = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPaymentDetailsRecordDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPaymentDetailsRecordTime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgAccounts = New System.Windows.Forms.TabPage()
        Me.nbxNetBalance = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblNetBalance = New System.Windows.Forms.Label()
        Me.nbxTotalCredit = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblTotalCredit = New System.Windows.Forms.Label()
        Me.nbxTotalDebit = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblTotalDebit = New System.Windows.Forms.Label()
        Me.tpgBillsIncomeServicePoint = New System.Windows.Forms.TabPage()
        Me.dgvBillsIncomeServicePoint = New System.Windows.Forms.DataGridView()
        Me.colBillIncomeCategory = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBillTotalAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBillCoPayAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBillDiscount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBillPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBillAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBillNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.stbCompanyName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboCompanyNo = New System.Windows.Forms.ComboBox()
        Me.lblCompanyName = New System.Windows.Forms.Label()
        Me.lblCompanyNo = New System.Windows.Forms.Label()
        Me.cboBillModesID = New System.Windows.Forms.ComboBox()
        Me.lblBPBillModesID = New System.Windows.Forms.Label()
        Me.stbBillCustomerName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboBillAccountNo = New System.Windows.Forms.ComboBox()
        Me.lblBillCustomerName = New System.Windows.Forms.Label()
        Me.lblBillAccountNo = New System.Windows.Forms.Label()
        Me.tpgBillsExtraCharges = New System.Windows.Forms.TabPage()
        Me.dgvBillsExtraChargeSummaries = New System.Windows.Forms.DataGridView()
        Me.colExtraBillChargeCategory = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraBillTotalAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraBillCoPayAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraBillDiscount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraBillPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraBillAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExtraBillNotPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.stbExtraCompanyName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboExtraCompanyNo = New System.Windows.Forms.ComboBox()
        Me.lblExtraCompanyName = New System.Windows.Forms.Label()
        Me.lblExtraCompanyNo = New System.Windows.Forms.Label()
        Me.cboExtraBillModesID = New System.Windows.Forms.ComboBox()
        Me.lblExtraBillModesID = New System.Windows.Forms.Label()
        Me.stbExtraCustomerName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboExtraAccountNo = New System.Windows.Forms.ComboBox()
        Me.lblExtraCustomerName = New System.Windows.Forms.Label()
        Me.lblExtraAccountNo = New System.Windows.Forms.Label()
        Me.fbnLoad = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.lblEndDate = New System.Windows.Forms.Label()
        Me.lblStartDate = New System.Windows.Forms.Label()
        Me.fbnExportTo = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.grpPeriod = New System.Windows.Forms.GroupBox()
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker()
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.tbcPeriodicReport.SuspendLayout()
        Me.tpgIncomeServicePoint.SuspendLayout()
        CType(Me.dgvIncomeServicePoint, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsIncomeSummaries.SuspendLayout()
        Me.tpgExtraCharges.SuspendLayout()
        CType(Me.dgvExtraChargeSummaries, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgDoctorVisits.SuspendLayout()
        CType(Me.dgvDoctorVisitSummaries, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgDoctorSpecialtyVisits.SuspendLayout()
        CType(Me.dgvDoctorSpecialtyVisitSummaries, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgIncomePaymentDetails.SuspendLayout()
        CType(Me.dgvIncomePaymentDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgAccounts.SuspendLayout()
        Me.tpgBillsIncomeServicePoint.SuspendLayout()
        CType(Me.dgvBillsIncomeServicePoint, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgBillsExtraCharges.SuspendLayout()
        CType(Me.dgvBillsExtraChargeSummaries, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpPeriod.SuspendLayout()
        Me.SuspendLayout()
        '
        'fbnClose
        '
        Me.fbnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(964, 456)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(79, 24)
        Me.fbnClose.TabIndex = 3
        Me.fbnClose.Text = "Close"
        '
        'tbcPeriodicReport
        '
        Me.tbcPeriodicReport.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbcPeriodicReport.Controls.Add(Me.tpgIncomeServicePoint)
        Me.tbcPeriodicReport.Controls.Add(Me.tpgExtraCharges)
        Me.tbcPeriodicReport.Controls.Add(Me.tpgDoctorVisits)
        Me.tbcPeriodicReport.Controls.Add(Me.tpgDoctorSpecialtyVisits)
        Me.tbcPeriodicReport.Controls.Add(Me.tpgIncomePaymentDetails)
        Me.tbcPeriodicReport.Controls.Add(Me.tpgAccounts)
        Me.tbcPeriodicReport.Controls.Add(Me.tpgBillsIncomeServicePoint)
        Me.tbcPeriodicReport.Controls.Add(Me.tpgBillsExtraCharges)
        Me.tbcPeriodicReport.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcPeriodicReport.HotTrack = True
        Me.tbcPeriodicReport.Location = New System.Drawing.Point(15, 57)
        Me.tbcPeriodicReport.Name = "tbcPeriodicReport"
        Me.tbcPeriodicReport.SelectedIndex = 0
        Me.tbcPeriodicReport.Size = New System.Drawing.Size(1032, 371)
        Me.tbcPeriodicReport.TabIndex = 1
        '
        'tpgIncomeServicePoint
        '
        Me.tpgIncomeServicePoint.Controls.Add(Me.dgvIncomeServicePoint)
        Me.tpgIncomeServicePoint.Location = New System.Drawing.Point(4, 22)
        Me.tpgIncomeServicePoint.Name = "tpgIncomeServicePoint"
        Me.tpgIncomeServicePoint.Size = New System.Drawing.Size(1024, 345)
        Me.tpgIncomeServicePoint.TabIndex = 2
        Me.tpgIncomeServicePoint.Tag = "IncomeSummaries"
        Me.tpgIncomeServicePoint.Text = "Income per Service Point"
        Me.tpgIncomeServicePoint.UseVisualStyleBackColor = True
        '
        'dgvIncomeServicePoint
        '
        Me.dgvIncomeServicePoint.AllowUserToAddRows = False
        Me.dgvIncomeServicePoint.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvIncomeServicePoint.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvIncomeServicePoint.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvIncomeServicePoint.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvIncomeServicePoint.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvIncomeServicePoint.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvIncomeServicePoint.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvIncomeServicePoint.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIncomeCategory, Me.colTotalAmount, Me.colCashAmount, Me.colCoPayAmount, Me.colTotalCash, Me.colCashPaid, Me.colCashDiscount, Me.colCashNotPaid, Me.colCoPayNotPaid, Me.colAccountAmount, Me.colAccountNotPaid, Me.colInsuranceAmount, Me.colInsuranceNotPaid})
        Me.dgvIncomeServicePoint.ContextMenuStrip = Me.cmsIncomeSummaries
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvIncomeServicePoint.DefaultCellStyle = DataGridViewCellStyle15
        Me.dgvIncomeServicePoint.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvIncomeServicePoint.GridColor = System.Drawing.Color.Khaki
        Me.dgvIncomeServicePoint.Location = New System.Drawing.Point(0, 0)
        Me.dgvIncomeServicePoint.Name = "dgvIncomeServicePoint"
        Me.dgvIncomeServicePoint.ReadOnly = True
        Me.dgvIncomeServicePoint.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle16.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvIncomeServicePoint.RowHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.dgvIncomeServicePoint.RowHeadersVisible = False
        Me.dgvIncomeServicePoint.Size = New System.Drawing.Size(1024, 345)
        Me.dgvIncomeServicePoint.TabIndex = 0
        Me.dgvIncomeServicePoint.Text = "DataGridView1"
        '
        'colIncomeCategory
        '
        Me.colIncomeCategory.DataPropertyName = "IncomeCategory"
        Me.colIncomeCategory.HeaderText = "Income Category"
        Me.colIncomeCategory.Name = "colIncomeCategory"
        Me.colIncomeCategory.ReadOnly = True
        '
        'colTotalAmount
        '
        Me.colTotalAmount.DataPropertyName = "TotalAmount"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colTotalAmount.DefaultCellStyle = DataGridViewCellStyle3
        Me.colTotalAmount.HeaderText = "Total Amount"
        Me.colTotalAmount.Name = "colTotalAmount"
        Me.colTotalAmount.ReadOnly = True
        Me.colTotalAmount.Width = 120
        '
        'colCashAmount
        '
        Me.colCashAmount.DataPropertyName = "CashAmount"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colCashAmount.DefaultCellStyle = DataGridViewCellStyle4
        Me.colCashAmount.HeaderText = "Cash Amount"
        Me.colCashAmount.Name = "colCashAmount"
        Me.colCashAmount.ReadOnly = True
        '
        'colCoPayAmount
        '
        Me.colCoPayAmount.DataPropertyName = "CoPayAmount"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colCoPayAmount.DefaultCellStyle = DataGridViewCellStyle5
        Me.colCoPayAmount.HeaderText = "Co-Pay Amount"
        Me.colCoPayAmount.Name = "colCoPayAmount"
        Me.colCoPayAmount.ReadOnly = True
        '
        'colTotalCash
        '
        Me.colTotalCash.DataPropertyName = "TotalCash"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colTotalCash.DefaultCellStyle = DataGridViewCellStyle6
        Me.colTotalCash.HeaderText = "Total Cash Amount"
        Me.colTotalCash.Name = "colTotalCash"
        Me.colTotalCash.ReadOnly = True
        Me.colTotalCash.Width = 120
        '
        'colCashPaid
        '
        Me.colCashPaid.DataPropertyName = "CashPaid"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colCashPaid.DefaultCellStyle = DataGridViewCellStyle7
        Me.colCashPaid.HeaderText = "Cash Amount Paid"
        Me.colCashPaid.Name = "colCashPaid"
        Me.colCashPaid.ReadOnly = True
        '
        'colCashDiscount
        '
        Me.colCashDiscount.DataPropertyName = "CashDiscount"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colCashDiscount.DefaultCellStyle = DataGridViewCellStyle8
        Me.colCashDiscount.HeaderText = "Cash Discount"
        Me.colCashDiscount.Name = "colCashDiscount"
        Me.colCashDiscount.ReadOnly = True
        '
        'colCashNotPaid
        '
        Me.colCashNotPaid.DataPropertyName = "CashNotPaid"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colCashNotPaid.DefaultCellStyle = DataGridViewCellStyle9
        Me.colCashNotPaid.HeaderText = "Cash Amount Not Paid"
        Me.colCashNotPaid.Name = "colCashNotPaid"
        Me.colCashNotPaid.ReadOnly = True
        Me.colCashNotPaid.Width = 120
        '
        'colCoPayNotPaid
        '
        Me.colCoPayNotPaid.DataPropertyName = "CoPayNotPaid"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.colCoPayNotPaid.DefaultCellStyle = DataGridViewCellStyle10
        Me.colCoPayNotPaid.HeaderText = "Co-Pay Not Paid"
        Me.colCoPayNotPaid.Name = "colCoPayNotPaid"
        Me.colCoPayNotPaid.ReadOnly = True
        '
        'colAccountAmount
        '
        Me.colAccountAmount.DataPropertyName = "AccountAmount"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colAccountAmount.DefaultCellStyle = DataGridViewCellStyle11
        Me.colAccountAmount.HeaderText = "Account Amount"
        Me.colAccountAmount.Name = "colAccountAmount"
        Me.colAccountAmount.ReadOnly = True
        '
        'colAccountNotPaid
        '
        Me.colAccountNotPaid.DataPropertyName = "AccountNotPaid"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colAccountNotPaid.DefaultCellStyle = DataGridViewCellStyle12
        Me.colAccountNotPaid.HeaderText = "Account Amount Not Paid"
        Me.colAccountNotPaid.Name = "colAccountNotPaid"
        Me.colAccountNotPaid.ReadOnly = True
        Me.colAccountNotPaid.Width = 140
        '
        'colInsuranceAmount
        '
        Me.colInsuranceAmount.DataPropertyName = "InsuranceAmount"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colInsuranceAmount.DefaultCellStyle = DataGridViewCellStyle13
        Me.colInsuranceAmount.HeaderText = "Insurance Amount"
        Me.colInsuranceAmount.Name = "colInsuranceAmount"
        Me.colInsuranceAmount.ReadOnly = True
        '
        'colInsuranceNotPaid
        '
        Me.colInsuranceNotPaid.DataPropertyName = "InsuranceNotPaid"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colInsuranceNotPaid.DefaultCellStyle = DataGridViewCellStyle14
        Me.colInsuranceNotPaid.HeaderText = "Insurance Amount Not Paid"
        Me.colInsuranceNotPaid.Name = "colInsuranceNotPaid"
        Me.colInsuranceNotPaid.ReadOnly = True
        Me.colInsuranceNotPaid.Width = 150
        '
        'cmsIncomeSummaries
        '
        Me.cmsIncomeSummaries.BackColor = System.Drawing.Color.GhostWhite
        Me.cmsIncomeSummaries.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmsIncomeSummariesCopy, Me.cmsIncomeSummariesSelectAll})
        Me.cmsIncomeSummaries.Name = "cmsSearch"
        Me.cmsIncomeSummaries.Size = New System.Drawing.Size(123, 48)
        '
        'cmsIncomeSummariesCopy
        '
        Me.cmsIncomeSummariesCopy.Enabled = False
        Me.cmsIncomeSummariesCopy.Image = CType(resources.GetObject("cmsIncomeSummariesCopy.Image"), System.Drawing.Image)
        Me.cmsIncomeSummariesCopy.Name = "cmsIncomeSummariesCopy"
        Me.cmsIncomeSummariesCopy.Size = New System.Drawing.Size(122, 22)
        Me.cmsIncomeSummariesCopy.Text = "Copy"
        Me.cmsIncomeSummariesCopy.ToolTipText = "To copy with column headings, use Ctrl+C key combination"
        '
        'cmsIncomeSummariesSelectAll
        '
        Me.cmsIncomeSummariesSelectAll.Enabled = False
        Me.cmsIncomeSummariesSelectAll.Name = "cmsIncomeSummariesSelectAll"
        Me.cmsIncomeSummariesSelectAll.Size = New System.Drawing.Size(122, 22)
        Me.cmsIncomeSummariesSelectAll.Text = "Select All"
        '
        'tpgExtraCharges
        '
        Me.tpgExtraCharges.Controls.Add(Me.dgvExtraChargeSummaries)
        Me.tpgExtraCharges.Location = New System.Drawing.Point(4, 22)
        Me.tpgExtraCharges.Name = "tpgExtraCharges"
        Me.tpgExtraCharges.Size = New System.Drawing.Size(1024, 345)
        Me.tpgExtraCharges.TabIndex = 3
        Me.tpgExtraCharges.Text = "Extra Charges"
        Me.tpgExtraCharges.UseVisualStyleBackColor = True
        '
        'dgvExtraChargeSummaries
        '
        Me.dgvExtraChargeSummaries.AllowUserToAddRows = False
        Me.dgvExtraChargeSummaries.AllowUserToDeleteRows = False
        DataGridViewCellStyle17.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle17.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvExtraChargeSummaries.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle17
        Me.dgvExtraChargeSummaries.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvExtraChargeSummaries.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvExtraChargeSummaries.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvExtraChargeSummaries.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle18.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvExtraChargeSummaries.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.dgvExtraChargeSummaries.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colExtraChargeCategory, Me.colExtraTotalAmount, Me.colExtraCashAmount, Me.colExtraCoPayAmount, Me.colExtraTotalCash, Me.colExtraCashPaid, Me.colExtraCashDiscount, Me.colExtraCashNotPaid, Me.colExtraCoPayNotPaid, Me.colExtraAccountAmount, Me.colExtraAccountNotPaid, Me.colExtraInsuranceAmount, Me.colExtraInsuranceNotPaid})
        Me.dgvExtraChargeSummaries.ContextMenuStrip = Me.cmsIncomeSummaries
        DataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvExtraChargeSummaries.DefaultCellStyle = DataGridViewCellStyle31
        Me.dgvExtraChargeSummaries.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvExtraChargeSummaries.GridColor = System.Drawing.Color.Khaki
        Me.dgvExtraChargeSummaries.Location = New System.Drawing.Point(0, 0)
        Me.dgvExtraChargeSummaries.Name = "dgvExtraChargeSummaries"
        Me.dgvExtraChargeSummaries.ReadOnly = True
        Me.dgvExtraChargeSummaries.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle32.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle32.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvExtraChargeSummaries.RowHeadersDefaultCellStyle = DataGridViewCellStyle32
        Me.dgvExtraChargeSummaries.RowHeadersVisible = False
        Me.dgvExtraChargeSummaries.Size = New System.Drawing.Size(1024, 345)
        Me.dgvExtraChargeSummaries.TabIndex = 23
        Me.dgvExtraChargeSummaries.Text = "DataGridView1"
        '
        'colExtraChargeCategory
        '
        Me.colExtraChargeCategory.DataPropertyName = "ExtraChargeCategory"
        Me.colExtraChargeCategory.HeaderText = "Extra Charge Category"
        Me.colExtraChargeCategory.Name = "colExtraChargeCategory"
        Me.colExtraChargeCategory.ReadOnly = True
        Me.colExtraChargeCategory.Width = 120
        '
        'colExtraTotalAmount
        '
        Me.colExtraTotalAmount.DataPropertyName = "TotalAmount"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraTotalAmount.DefaultCellStyle = DataGridViewCellStyle19
        Me.colExtraTotalAmount.HeaderText = "Total Amount"
        Me.colExtraTotalAmount.Name = "colExtraTotalAmount"
        Me.colExtraTotalAmount.ReadOnly = True
        Me.colExtraTotalAmount.Width = 120
        '
        'colExtraCashAmount
        '
        Me.colExtraCashAmount.DataPropertyName = "CashAmount"
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraCashAmount.DefaultCellStyle = DataGridViewCellStyle20
        Me.colExtraCashAmount.HeaderText = "Cash Amount"
        Me.colExtraCashAmount.Name = "colExtraCashAmount"
        Me.colExtraCashAmount.ReadOnly = True
        '
        'colExtraCoPayAmount
        '
        Me.colExtraCoPayAmount.DataPropertyName = "CoPayAmount"
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraCoPayAmount.DefaultCellStyle = DataGridViewCellStyle21
        Me.colExtraCoPayAmount.HeaderText = "Co-Pay Amount"
        Me.colExtraCoPayAmount.Name = "colExtraCoPayAmount"
        Me.colExtraCoPayAmount.ReadOnly = True
        '
        'colExtraTotalCash
        '
        Me.colExtraTotalCash.DataPropertyName = "TotalCash"
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraTotalCash.DefaultCellStyle = DataGridViewCellStyle22
        Me.colExtraTotalCash.HeaderText = "Total Cash Amount"
        Me.colExtraTotalCash.Name = "colExtraTotalCash"
        Me.colExtraTotalCash.ReadOnly = True
        Me.colExtraTotalCash.Width = 120
        '
        'colExtraCashPaid
        '
        Me.colExtraCashPaid.DataPropertyName = "CashPaid"
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraCashPaid.DefaultCellStyle = DataGridViewCellStyle23
        Me.colExtraCashPaid.HeaderText = "Cash Amount Paid"
        Me.colExtraCashPaid.Name = "colExtraCashPaid"
        Me.colExtraCashPaid.ReadOnly = True
        '
        'colExtraCashDiscount
        '
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraCashDiscount.DefaultCellStyle = DataGridViewCellStyle24
        Me.colExtraCashDiscount.HeaderText = "Cash Discount"
        Me.colExtraCashDiscount.Name = "colExtraCashDiscount"
        Me.colExtraCashDiscount.ReadOnly = True
        '
        'colExtraCashNotPaid
        '
        Me.colExtraCashNotPaid.DataPropertyName = "CashNotPaid"
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraCashNotPaid.DefaultCellStyle = DataGridViewCellStyle25
        Me.colExtraCashNotPaid.HeaderText = "Cash Amount Not Paid"
        Me.colExtraCashNotPaid.Name = "colExtraCashNotPaid"
        Me.colExtraCashNotPaid.ReadOnly = True
        Me.colExtraCashNotPaid.Width = 120
        '
        'colExtraCoPayNotPaid
        '
        Me.colExtraCoPayNotPaid.DataPropertyName = "CoPayNotPaid"
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle26.Format = "N2"
        DataGridViewCellStyle26.NullValue = Nothing
        Me.colExtraCoPayNotPaid.DefaultCellStyle = DataGridViewCellStyle26
        Me.colExtraCoPayNotPaid.HeaderText = "Co-Pay Not Paid"
        Me.colExtraCoPayNotPaid.Name = "colExtraCoPayNotPaid"
        Me.colExtraCoPayNotPaid.ReadOnly = True
        '
        'colExtraAccountAmount
        '
        Me.colExtraAccountAmount.DataPropertyName = "AccountAmount"
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraAccountAmount.DefaultCellStyle = DataGridViewCellStyle27
        Me.colExtraAccountAmount.HeaderText = "Account Amount"
        Me.colExtraAccountAmount.Name = "colExtraAccountAmount"
        Me.colExtraAccountAmount.ReadOnly = True
        '
        'colExtraAccountNotPaid
        '
        Me.colExtraAccountNotPaid.DataPropertyName = "AccountNotPaid"
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraAccountNotPaid.DefaultCellStyle = DataGridViewCellStyle28
        Me.colExtraAccountNotPaid.HeaderText = "Account Amount Not Paid"
        Me.colExtraAccountNotPaid.Name = "colExtraAccountNotPaid"
        Me.colExtraAccountNotPaid.ReadOnly = True
        Me.colExtraAccountNotPaid.Width = 140
        '
        'colExtraInsuranceAmount
        '
        Me.colExtraInsuranceAmount.DataPropertyName = "InsuranceAmount"
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraInsuranceAmount.DefaultCellStyle = DataGridViewCellStyle29
        Me.colExtraInsuranceAmount.HeaderText = "Insurance Amount"
        Me.colExtraInsuranceAmount.Name = "colExtraInsuranceAmount"
        Me.colExtraInsuranceAmount.ReadOnly = True
        '
        'colExtraInsuranceNotPaid
        '
        Me.colExtraInsuranceNotPaid.DataPropertyName = "InsuranceNotPaid"
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraInsuranceNotPaid.DefaultCellStyle = DataGridViewCellStyle30
        Me.colExtraInsuranceNotPaid.HeaderText = "Insurance Amount Not Paid"
        Me.colExtraInsuranceNotPaid.Name = "colExtraInsuranceNotPaid"
        Me.colExtraInsuranceNotPaid.ReadOnly = True
        Me.colExtraInsuranceNotPaid.Width = 150
        '
        'tpgDoctorVisits
        '
        Me.tpgDoctorVisits.Controls.Add(Me.dgvDoctorVisitSummaries)
        Me.tpgDoctorVisits.Location = New System.Drawing.Point(4, 22)
        Me.tpgDoctorVisits.Name = "tpgDoctorVisits"
        Me.tpgDoctorVisits.Size = New System.Drawing.Size(1024, 345)
        Me.tpgDoctorVisits.TabIndex = 1
        Me.tpgDoctorVisits.Text = "Visits per Doctor"
        Me.tpgDoctorVisits.UseVisualStyleBackColor = True
        '
        'dgvDoctorVisitSummaries
        '
        Me.dgvDoctorVisitSummaries.AllowUserToAddRows = False
        Me.dgvDoctorVisitSummaries.AllowUserToDeleteRows = False
        DataGridViewCellStyle33.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle33.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvDoctorVisitSummaries.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle33
        Me.dgvDoctorVisitSummaries.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvDoctorVisitSummaries.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvDoctorVisitSummaries.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvDoctorVisitSummaries.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle34.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle34.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle34.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDoctorVisitSummaries.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle34
        Me.dgvDoctorVisitSummaries.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colDoctorSeenDoctor, Me.colDoctorTotalVisits, Me.colDoctorTotalOnServices, Me.colDoctorTotalAmount, Me.colDoctorCashAmount, Me.colDoctorCoPayAmount, Me.colDoctorTotalCash, Me.colDoctorCashPaid, Me.colDoctorCashDiscount, Me.colDoctorCashNotPaid, Me.colDoctorAccountAmount, Me.colDoctorAccountNotPaid, Me.colDoctorInsuranceAmount, Me.colDoctorInsuranceNotPaid})
        Me.dgvDoctorVisitSummaries.ContextMenuStrip = Me.cmsIncomeSummaries
        DataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle47.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle47.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle47.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle47.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle47.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle47.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDoctorVisitSummaries.DefaultCellStyle = DataGridViewCellStyle47
        Me.dgvDoctorVisitSummaries.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDoctorVisitSummaries.GridColor = System.Drawing.Color.Khaki
        Me.dgvDoctorVisitSummaries.Location = New System.Drawing.Point(0, 0)
        Me.dgvDoctorVisitSummaries.Name = "dgvDoctorVisitSummaries"
        Me.dgvDoctorVisitSummaries.ReadOnly = True
        Me.dgvDoctorVisitSummaries.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle48.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle48.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle48.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle48.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle48.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle48.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle48.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDoctorVisitSummaries.RowHeadersDefaultCellStyle = DataGridViewCellStyle48
        Me.dgvDoctorVisitSummaries.RowHeadersVisible = False
        Me.dgvDoctorVisitSummaries.Size = New System.Drawing.Size(1024, 345)
        Me.dgvDoctorVisitSummaries.TabIndex = 25
        Me.dgvDoctorVisitSummaries.Text = "DataGridView1"
        '
        'colDoctorSeenDoctor
        '
        Me.colDoctorSeenDoctor.DataPropertyName = "SeenDoctor"
        Me.colDoctorSeenDoctor.HeaderText = "Seen Doctor"
        Me.colDoctorSeenDoctor.Name = "colDoctorSeenDoctor"
        Me.colDoctorSeenDoctor.ReadOnly = True
        Me.colDoctorSeenDoctor.Width = 140
        '
        'colDoctorTotalVisits
        '
        Me.colDoctorTotalVisits.DataPropertyName = "TotalVisits"
        Me.colDoctorTotalVisits.HeaderText = "Total Visits"
        Me.colDoctorTotalVisits.Name = "colDoctorTotalVisits"
        Me.colDoctorTotalVisits.ReadOnly = True
        '
        'colDoctorTotalOnServices
        '
        Me.colDoctorTotalOnServices.DataPropertyName = "TotalOnServices"
        DataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorTotalOnServices.DefaultCellStyle = DataGridViewCellStyle35
        Me.colDoctorTotalOnServices.HeaderText = "Total On Services"
        Me.colDoctorTotalOnServices.Name = "colDoctorTotalOnServices"
        Me.colDoctorTotalOnServices.ReadOnly = True
        '
        'colDoctorTotalAmount
        '
        Me.colDoctorTotalAmount.DataPropertyName = "TotalAmount"
        DataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorTotalAmount.DefaultCellStyle = DataGridViewCellStyle36
        Me.colDoctorTotalAmount.HeaderText = "Total Amount"
        Me.colDoctorTotalAmount.Name = "colDoctorTotalAmount"
        Me.colDoctorTotalAmount.ReadOnly = True
        Me.colDoctorTotalAmount.Width = 120
        '
        'colDoctorCashAmount
        '
        Me.colDoctorCashAmount.DataPropertyName = "CashAmount"
        DataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorCashAmount.DefaultCellStyle = DataGridViewCellStyle37
        Me.colDoctorCashAmount.HeaderText = "Cash Amount"
        Me.colDoctorCashAmount.Name = "colDoctorCashAmount"
        Me.colDoctorCashAmount.ReadOnly = True
        '
        'colDoctorCoPayAmount
        '
        Me.colDoctorCoPayAmount.DataPropertyName = "CoPayAmount"
        DataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorCoPayAmount.DefaultCellStyle = DataGridViewCellStyle38
        Me.colDoctorCoPayAmount.HeaderText = "Co-Pay Amount"
        Me.colDoctorCoPayAmount.Name = "colDoctorCoPayAmount"
        Me.colDoctorCoPayAmount.ReadOnly = True
        '
        'colDoctorTotalCash
        '
        Me.colDoctorTotalCash.DataPropertyName = "TotalCash"
        DataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorTotalCash.DefaultCellStyle = DataGridViewCellStyle39
        Me.colDoctorTotalCash.HeaderText = "Total Cash Amount"
        Me.colDoctorTotalCash.Name = "colDoctorTotalCash"
        Me.colDoctorTotalCash.ReadOnly = True
        Me.colDoctorTotalCash.Width = 120
        '
        'colDoctorCashPaid
        '
        Me.colDoctorCashPaid.DataPropertyName = "CashPaid"
        DataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorCashPaid.DefaultCellStyle = DataGridViewCellStyle40
        Me.colDoctorCashPaid.HeaderText = "Cash Amount Paid"
        Me.colDoctorCashPaid.Name = "colDoctorCashPaid"
        Me.colDoctorCashPaid.ReadOnly = True
        '
        'colDoctorCashDiscount
        '
        DataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorCashDiscount.DefaultCellStyle = DataGridViewCellStyle41
        Me.colDoctorCashDiscount.HeaderText = "Cash Discount"
        Me.colDoctorCashDiscount.Name = "colDoctorCashDiscount"
        Me.colDoctorCashDiscount.ReadOnly = True
        '
        'colDoctorCashNotPaid
        '
        Me.colDoctorCashNotPaid.DataPropertyName = "CashNotPaid"
        DataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorCashNotPaid.DefaultCellStyle = DataGridViewCellStyle42
        Me.colDoctorCashNotPaid.HeaderText = "Cash Amount Not Paid"
        Me.colDoctorCashNotPaid.Name = "colDoctorCashNotPaid"
        Me.colDoctorCashNotPaid.ReadOnly = True
        Me.colDoctorCashNotPaid.Width = 120
        '
        'colDoctorAccountAmount
        '
        Me.colDoctorAccountAmount.DataPropertyName = "AccountAmount"
        DataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorAccountAmount.DefaultCellStyle = DataGridViewCellStyle43
        Me.colDoctorAccountAmount.HeaderText = "Account Amount"
        Me.colDoctorAccountAmount.Name = "colDoctorAccountAmount"
        Me.colDoctorAccountAmount.ReadOnly = True
        '
        'colDoctorAccountNotPaid
        '
        Me.colDoctorAccountNotPaid.DataPropertyName = "AccountNotPaid"
        DataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorAccountNotPaid.DefaultCellStyle = DataGridViewCellStyle44
        Me.colDoctorAccountNotPaid.HeaderText = "Account Amount Not Paid"
        Me.colDoctorAccountNotPaid.Name = "colDoctorAccountNotPaid"
        Me.colDoctorAccountNotPaid.ReadOnly = True
        Me.colDoctorAccountNotPaid.Width = 140
        '
        'colDoctorInsuranceAmount
        '
        Me.colDoctorInsuranceAmount.DataPropertyName = "InsuranceAmount"
        DataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorInsuranceAmount.DefaultCellStyle = DataGridViewCellStyle45
        Me.colDoctorInsuranceAmount.HeaderText = "Insurance Amount"
        Me.colDoctorInsuranceAmount.Name = "colDoctorInsuranceAmount"
        Me.colDoctorInsuranceAmount.ReadOnly = True
        '
        'colDoctorInsuranceNotPaid
        '
        Me.colDoctorInsuranceNotPaid.DataPropertyName = "InsuranceNotPaid"
        DataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorInsuranceNotPaid.DefaultCellStyle = DataGridViewCellStyle46
        Me.colDoctorInsuranceNotPaid.HeaderText = "Insurance Amount Not Paid"
        Me.colDoctorInsuranceNotPaid.Name = "colDoctorInsuranceNotPaid"
        Me.colDoctorInsuranceNotPaid.ReadOnly = True
        Me.colDoctorInsuranceNotPaid.Width = 150
        '
        'tpgDoctorSpecialtyVisits
        '
        Me.tpgDoctorSpecialtyVisits.Controls.Add(Me.dgvDoctorSpecialtyVisitSummaries)
        Me.tpgDoctorSpecialtyVisits.Location = New System.Drawing.Point(4, 22)
        Me.tpgDoctorSpecialtyVisits.Name = "tpgDoctorSpecialtyVisits"
        Me.tpgDoctorSpecialtyVisits.Size = New System.Drawing.Size(1024, 345)
        Me.tpgDoctorSpecialtyVisits.TabIndex = 7
        Me.tpgDoctorSpecialtyVisits.Text = "Visits per Dr. Specialty"
        Me.tpgDoctorSpecialtyVisits.UseVisualStyleBackColor = True
        '
        'dgvDoctorSpecialtyVisitSummaries
        '
        Me.dgvDoctorSpecialtyVisitSummaries.AllowUserToAddRows = False
        Me.dgvDoctorSpecialtyVisitSummaries.AllowUserToDeleteRows = False
        DataGridViewCellStyle49.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle49.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvDoctorSpecialtyVisitSummaries.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle49
        Me.dgvDoctorSpecialtyVisitSummaries.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvDoctorSpecialtyVisitSummaries.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvDoctorSpecialtyVisitSummaries.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvDoctorSpecialtyVisitSummaries.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle50.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle50.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle50.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle50.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle50.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle50.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle50.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDoctorSpecialtyVisitSummaries.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle50
        Me.dgvDoctorSpecialtyVisitSummaries.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colDoctorSpecialtySeenDoctorSpecialty, Me.colDoctorSpecialtyTotalVisits, Me.colDoctorSpecialtyTotalOnServices, Me.colDoctorSpecialtyTotalAmount, Me.colDoctorSpecialtyCashAmount, Me.colDoctorSpecialtyCoPayAmount, Me.colDoctorSpecialtyTotalCash, Me.colDoctorSpecialtyCashPaid, Me.colDoctorSpecialtyCashDiscount, Me.colDoctorSpecialtyCashNotPaid, Me.colDoctorSpecialtyAccountAmount, Me.colDoctorSpecialtyAccountNotPaid, Me.colDoctorSpecialtyInsuranceAmount, Me.colDoctorSpecialtyInsuranceNotPaid})
        Me.dgvDoctorSpecialtyVisitSummaries.ContextMenuStrip = Me.cmsIncomeSummaries
        DataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle63.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle63.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle63.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle63.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle63.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle63.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDoctorSpecialtyVisitSummaries.DefaultCellStyle = DataGridViewCellStyle63
        Me.dgvDoctorSpecialtyVisitSummaries.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDoctorSpecialtyVisitSummaries.GridColor = System.Drawing.Color.Khaki
        Me.dgvDoctorSpecialtyVisitSummaries.Location = New System.Drawing.Point(0, 0)
        Me.dgvDoctorSpecialtyVisitSummaries.Name = "dgvDoctorSpecialtyVisitSummaries"
        Me.dgvDoctorSpecialtyVisitSummaries.ReadOnly = True
        Me.dgvDoctorSpecialtyVisitSummaries.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle64.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle64.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle64.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle64.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle64.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle64.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle64.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDoctorSpecialtyVisitSummaries.RowHeadersDefaultCellStyle = DataGridViewCellStyle64
        Me.dgvDoctorSpecialtyVisitSummaries.RowHeadersVisible = False
        Me.dgvDoctorSpecialtyVisitSummaries.Size = New System.Drawing.Size(1024, 345)
        Me.dgvDoctorSpecialtyVisitSummaries.TabIndex = 27
        Me.dgvDoctorSpecialtyVisitSummaries.Text = "DataGridView1"
        '
        'colDoctorSpecialtySeenDoctorSpecialty
        '
        Me.colDoctorSpecialtySeenDoctorSpecialty.DataPropertyName = "SeenDoctorSpecialty"
        Me.colDoctorSpecialtySeenDoctorSpecialty.HeaderText = "Seen Dr. Specialty"
        Me.colDoctorSpecialtySeenDoctorSpecialty.Name = "colDoctorSpecialtySeenDoctorSpecialty"
        Me.colDoctorSpecialtySeenDoctorSpecialty.ReadOnly = True
        Me.colDoctorSpecialtySeenDoctorSpecialty.Width = 140
        '
        'colDoctorSpecialtyTotalVisits
        '
        Me.colDoctorSpecialtyTotalVisits.DataPropertyName = "TotalVisits"
        Me.colDoctorSpecialtyTotalVisits.HeaderText = "Total Visits"
        Me.colDoctorSpecialtyTotalVisits.Name = "colDoctorSpecialtyTotalVisits"
        Me.colDoctorSpecialtyTotalVisits.ReadOnly = True
        '
        'colDoctorSpecialtyTotalOnServices
        '
        Me.colDoctorSpecialtyTotalOnServices.DataPropertyName = "TotalOnServices"
        DataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorSpecialtyTotalOnServices.DefaultCellStyle = DataGridViewCellStyle51
        Me.colDoctorSpecialtyTotalOnServices.HeaderText = "Total On Services"
        Me.colDoctorSpecialtyTotalOnServices.Name = "colDoctorSpecialtyTotalOnServices"
        Me.colDoctorSpecialtyTotalOnServices.ReadOnly = True
        '
        'colDoctorSpecialtyTotalAmount
        '
        Me.colDoctorSpecialtyTotalAmount.DataPropertyName = "TotalAmount"
        DataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorSpecialtyTotalAmount.DefaultCellStyle = DataGridViewCellStyle52
        Me.colDoctorSpecialtyTotalAmount.HeaderText = "Total Amount"
        Me.colDoctorSpecialtyTotalAmount.Name = "colDoctorSpecialtyTotalAmount"
        Me.colDoctorSpecialtyTotalAmount.ReadOnly = True
        Me.colDoctorSpecialtyTotalAmount.Width = 120
        '
        'colDoctorSpecialtyCashAmount
        '
        Me.colDoctorSpecialtyCashAmount.DataPropertyName = "CashAmount"
        DataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorSpecialtyCashAmount.DefaultCellStyle = DataGridViewCellStyle53
        Me.colDoctorSpecialtyCashAmount.HeaderText = "Cash Amount"
        Me.colDoctorSpecialtyCashAmount.Name = "colDoctorSpecialtyCashAmount"
        Me.colDoctorSpecialtyCashAmount.ReadOnly = True
        '
        'colDoctorSpecialtyCoPayAmount
        '
        Me.colDoctorSpecialtyCoPayAmount.DataPropertyName = "CoPayAmount"
        DataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorSpecialtyCoPayAmount.DefaultCellStyle = DataGridViewCellStyle54
        Me.colDoctorSpecialtyCoPayAmount.HeaderText = "Co-Pay Amount"
        Me.colDoctorSpecialtyCoPayAmount.Name = "colDoctorSpecialtyCoPayAmount"
        Me.colDoctorSpecialtyCoPayAmount.ReadOnly = True
        '
        'colDoctorSpecialtyTotalCash
        '
        Me.colDoctorSpecialtyTotalCash.DataPropertyName = "TotalCash"
        DataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorSpecialtyTotalCash.DefaultCellStyle = DataGridViewCellStyle55
        Me.colDoctorSpecialtyTotalCash.HeaderText = "Total Cash Amount"
        Me.colDoctorSpecialtyTotalCash.Name = "colDoctorSpecialtyTotalCash"
        Me.colDoctorSpecialtyTotalCash.ReadOnly = True
        Me.colDoctorSpecialtyTotalCash.Width = 120
        '
        'colDoctorSpecialtyCashPaid
        '
        Me.colDoctorSpecialtyCashPaid.DataPropertyName = "CashPaid"
        DataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorSpecialtyCashPaid.DefaultCellStyle = DataGridViewCellStyle56
        Me.colDoctorSpecialtyCashPaid.HeaderText = "Cash Amount Paid"
        Me.colDoctorSpecialtyCashPaid.Name = "colDoctorSpecialtyCashPaid"
        Me.colDoctorSpecialtyCashPaid.ReadOnly = True
        '
        'colDoctorSpecialtyCashDiscount
        '
        DataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorSpecialtyCashDiscount.DefaultCellStyle = DataGridViewCellStyle57
        Me.colDoctorSpecialtyCashDiscount.HeaderText = "Cash Discount"
        Me.colDoctorSpecialtyCashDiscount.Name = "colDoctorSpecialtyCashDiscount"
        Me.colDoctorSpecialtyCashDiscount.ReadOnly = True
        '
        'colDoctorSpecialtyCashNotPaid
        '
        Me.colDoctorSpecialtyCashNotPaid.DataPropertyName = "CashNotPaid"
        DataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorSpecialtyCashNotPaid.DefaultCellStyle = DataGridViewCellStyle58
        Me.colDoctorSpecialtyCashNotPaid.HeaderText = "Cash Amount Not Paid"
        Me.colDoctorSpecialtyCashNotPaid.Name = "colDoctorSpecialtyCashNotPaid"
        Me.colDoctorSpecialtyCashNotPaid.ReadOnly = True
        Me.colDoctorSpecialtyCashNotPaid.Width = 120
        '
        'colDoctorSpecialtyAccountAmount
        '
        Me.colDoctorSpecialtyAccountAmount.DataPropertyName = "AccountAmount"
        DataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorSpecialtyAccountAmount.DefaultCellStyle = DataGridViewCellStyle59
        Me.colDoctorSpecialtyAccountAmount.HeaderText = "Account Amount"
        Me.colDoctorSpecialtyAccountAmount.Name = "colDoctorSpecialtyAccountAmount"
        Me.colDoctorSpecialtyAccountAmount.ReadOnly = True
        '
        'colDoctorSpecialtyAccountNotPaid
        '
        Me.colDoctorSpecialtyAccountNotPaid.DataPropertyName = "AccountNotPaid"
        DataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorSpecialtyAccountNotPaid.DefaultCellStyle = DataGridViewCellStyle60
        Me.colDoctorSpecialtyAccountNotPaid.HeaderText = "Account Amount Not Paid"
        Me.colDoctorSpecialtyAccountNotPaid.Name = "colDoctorSpecialtyAccountNotPaid"
        Me.colDoctorSpecialtyAccountNotPaid.ReadOnly = True
        Me.colDoctorSpecialtyAccountNotPaid.Width = 140
        '
        'colDoctorSpecialtyInsuranceAmount
        '
        Me.colDoctorSpecialtyInsuranceAmount.DataPropertyName = "InsuranceAmount"
        DataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorSpecialtyInsuranceAmount.DefaultCellStyle = DataGridViewCellStyle61
        Me.colDoctorSpecialtyInsuranceAmount.HeaderText = "Insurance Amount"
        Me.colDoctorSpecialtyInsuranceAmount.Name = "colDoctorSpecialtyInsuranceAmount"
        Me.colDoctorSpecialtyInsuranceAmount.ReadOnly = True
        '
        'colDoctorSpecialtyInsuranceNotPaid
        '
        Me.colDoctorSpecialtyInsuranceNotPaid.DataPropertyName = "InsuranceNotPaid"
        DataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDoctorSpecialtyInsuranceNotPaid.DefaultCellStyle = DataGridViewCellStyle62
        Me.colDoctorSpecialtyInsuranceNotPaid.HeaderText = "Insurance Amount Not Paid"
        Me.colDoctorSpecialtyInsuranceNotPaid.Name = "colDoctorSpecialtyInsuranceNotPaid"
        Me.colDoctorSpecialtyInsuranceNotPaid.ReadOnly = True
        Me.colDoctorSpecialtyInsuranceNotPaid.Width = 150
        '
        'tpgIncomePaymentDetails
        '
        Me.tpgIncomePaymentDetails.Controls.Add(Me.dgvIncomePaymentDetails)
        Me.tpgIncomePaymentDetails.Location = New System.Drawing.Point(4, 22)
        Me.tpgIncomePaymentDetails.Name = "tpgIncomePaymentDetails"
        Me.tpgIncomePaymentDetails.Size = New System.Drawing.Size(1024, 345)
        Me.tpgIncomePaymentDetails.TabIndex = 8
        Me.tpgIncomePaymentDetails.Tag = "IncomeSummaries"
        Me.tpgIncomePaymentDetails.Text = "Cash Payments"
        Me.tpgIncomePaymentDetails.UseVisualStyleBackColor = True
        '
        'dgvIncomePaymentDetails
        '
        Me.dgvIncomePaymentDetails.AllowUserToAddRows = False
        Me.dgvIncomePaymentDetails.AllowUserToDeleteRows = False
        DataGridViewCellStyle65.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle65.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvIncomePaymentDetails.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle65
        Me.dgvIncomePaymentDetails.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvIncomePaymentDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvIncomePaymentDetails.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvIncomePaymentDetails.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle66.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle66.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle66.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle66.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle66.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle66.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle66.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle66.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvIncomePaymentDetails.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle66
        Me.dgvIncomePaymentDetails.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colPaymentDetailsReceiptNo, Me.colPaymentDetailsVisitDate, Me.colPaymentDetailsFullName, Me.colIncomePaymentDetailsCategory, Me.colPaymentDetailsTotalAmount, Me.colPaymentDetailsPaidAfterDays, Me.colPaymentDetailsRecordDate, Me.colPaymentDetailsRecordTime})
        Me.dgvIncomePaymentDetails.ContextMenuStrip = Me.cmsIncomeSummaries
        DataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle69.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle69.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle69.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle69.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle69.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle69.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvIncomePaymentDetails.DefaultCellStyle = DataGridViewCellStyle69
        Me.dgvIncomePaymentDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvIncomePaymentDetails.GridColor = System.Drawing.Color.Khaki
        Me.dgvIncomePaymentDetails.Location = New System.Drawing.Point(0, 0)
        Me.dgvIncomePaymentDetails.Name = "dgvIncomePaymentDetails"
        Me.dgvIncomePaymentDetails.ReadOnly = True
        Me.dgvIncomePaymentDetails.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle70.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle70.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle70.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle70.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle70.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle70.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle70.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvIncomePaymentDetails.RowHeadersDefaultCellStyle = DataGridViewCellStyle70
        Me.dgvIncomePaymentDetails.RowHeadersVisible = False
        Me.dgvIncomePaymentDetails.Size = New System.Drawing.Size(1024, 345)
        Me.dgvIncomePaymentDetails.TabIndex = 21
        Me.dgvIncomePaymentDetails.Text = "DataGridView1"
        '
        'colPaymentDetailsReceiptNo
        '
        Me.colPaymentDetailsReceiptNo.DataPropertyName = "ReceiptNo"
        Me.colPaymentDetailsReceiptNo.HeaderText = "Receipt No"
        Me.colPaymentDetailsReceiptNo.Name = "colPaymentDetailsReceiptNo"
        Me.colPaymentDetailsReceiptNo.ReadOnly = True
        '
        'colPaymentDetailsVisitDate
        '
        Me.colPaymentDetailsVisitDate.DataPropertyName = "VisitDate"
        Me.colPaymentDetailsVisitDate.HeaderText = "Visit Date"
        Me.colPaymentDetailsVisitDate.Name = "colPaymentDetailsVisitDate"
        Me.colPaymentDetailsVisitDate.ReadOnly = True
        '
        'colPaymentDetailsFullName
        '
        Me.colPaymentDetailsFullName.DataPropertyName = "FullName"
        Me.colPaymentDetailsFullName.HeaderText = "Full Name"
        Me.colPaymentDetailsFullName.Name = "colPaymentDetailsFullName"
        Me.colPaymentDetailsFullName.ReadOnly = True
        Me.colPaymentDetailsFullName.Width = 180
        '
        'colIncomePaymentDetailsCategory
        '
        Me.colIncomePaymentDetailsCategory.DataPropertyName = "IncomeCategory"
        Me.colIncomePaymentDetailsCategory.HeaderText = "Income Category"
        Me.colIncomePaymentDetailsCategory.Name = "colIncomePaymentDetailsCategory"
        Me.colIncomePaymentDetailsCategory.ReadOnly = True
        Me.colIncomePaymentDetailsCategory.Width = 150
        '
        'colPaymentDetailsTotalAmount
        '
        Me.colPaymentDetailsTotalAmount.DataPropertyName = "TotalAmount"
        DataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colPaymentDetailsTotalAmount.DefaultCellStyle = DataGridViewCellStyle67
        Me.colPaymentDetailsTotalAmount.HeaderText = "Total Amount"
        Me.colPaymentDetailsTotalAmount.Name = "colPaymentDetailsTotalAmount"
        Me.colPaymentDetailsTotalAmount.ReadOnly = True
        '
        'colPaymentDetailsPaidAfterDays
        '
        Me.colPaymentDetailsPaidAfterDays.DataPropertyName = "PaidAfterDays"
        DataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colPaymentDetailsPaidAfterDays.DefaultCellStyle = DataGridViewCellStyle68
        Me.colPaymentDetailsPaidAfterDays.HeaderText = "Paid After (Days)"
        Me.colPaymentDetailsPaidAfterDays.Name = "colPaymentDetailsPaidAfterDays"
        Me.colPaymentDetailsPaidAfterDays.ReadOnly = True
        '
        'colPaymentDetailsRecordDate
        '
        Me.colPaymentDetailsRecordDate.DataPropertyName = "RecordDate"
        Me.colPaymentDetailsRecordDate.HeaderText = "Record Date"
        Me.colPaymentDetailsRecordDate.Name = "colPaymentDetailsRecordDate"
        Me.colPaymentDetailsRecordDate.ReadOnly = True
        '
        'colPaymentDetailsRecordTime
        '
        Me.colPaymentDetailsRecordTime.DataPropertyName = "RecordTime"
        Me.colPaymentDetailsRecordTime.HeaderText = "Record Time"
        Me.colPaymentDetailsRecordTime.Name = "colPaymentDetailsRecordTime"
        Me.colPaymentDetailsRecordTime.ReadOnly = True
        Me.colPaymentDetailsRecordTime.Width = 80
        '
        'tpgAccounts
        '
        Me.tpgAccounts.Controls.Add(Me.nbxNetBalance)
        Me.tpgAccounts.Controls.Add(Me.lblNetBalance)
        Me.tpgAccounts.Controls.Add(Me.nbxTotalCredit)
        Me.tpgAccounts.Controls.Add(Me.lblTotalCredit)
        Me.tpgAccounts.Controls.Add(Me.nbxTotalDebit)
        Me.tpgAccounts.Controls.Add(Me.lblTotalDebit)
        Me.tpgAccounts.Location = New System.Drawing.Point(4, 22)
        Me.tpgAccounts.Name = "tpgAccounts"
        Me.tpgAccounts.Size = New System.Drawing.Size(1024, 345)
        Me.tpgAccounts.TabIndex = 4
        Me.tpgAccounts.Tag = ""
        Me.tpgAccounts.Text = "Accounts"
        Me.tpgAccounts.UseVisualStyleBackColor = True
        '
        'nbxNetBalance
        '
        Me.nbxNetBalance.BackColor = System.Drawing.SystemColors.Info
        Me.nbxNetBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxNetBalance.ControlCaption = "Net Balance"
        Me.nbxNetBalance.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxNetBalance.DecimalPlaces = -1
        Me.nbxNetBalance.Enabled = False
        Me.nbxNetBalance.Location = New System.Drawing.Point(146, 55)
        Me.nbxNetBalance.MaxValue = 0.0R
        Me.nbxNetBalance.MinValue = 0.0R
        Me.nbxNetBalance.MustEnterNumeric = True
        Me.nbxNetBalance.Name = "nbxNetBalance"
        Me.nbxNetBalance.Size = New System.Drawing.Size(183, 20)
        Me.nbxNetBalance.TabIndex = 5
        Me.nbxNetBalance.Value = ""
        '
        'lblNetBalance
        '
        Me.lblNetBalance.Location = New System.Drawing.Point(11, 55)
        Me.lblNetBalance.Name = "lblNetBalance"
        Me.lblNetBalance.Size = New System.Drawing.Size(129, 18)
        Me.lblNetBalance.TabIndex = 4
        Me.lblNetBalance.Text = "Net Balance"
        '
        'nbxTotalCredit
        '
        Me.nbxTotalCredit.BackColor = System.Drawing.SystemColors.Info
        Me.nbxTotalCredit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxTotalCredit.ControlCaption = "Total Credit"
        Me.nbxTotalCredit.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxTotalCredit.DecimalPlaces = -1
        Me.nbxTotalCredit.Enabled = False
        Me.nbxTotalCredit.Location = New System.Drawing.Point(146, 34)
        Me.nbxTotalCredit.MaxValue = 0.0R
        Me.nbxTotalCredit.MinValue = 0.0R
        Me.nbxTotalCredit.MustEnterNumeric = True
        Me.nbxTotalCredit.Name = "nbxTotalCredit"
        Me.nbxTotalCredit.Size = New System.Drawing.Size(183, 20)
        Me.nbxTotalCredit.TabIndex = 3
        Me.nbxTotalCredit.Value = ""
        '
        'lblTotalCredit
        '
        Me.lblTotalCredit.Location = New System.Drawing.Point(11, 34)
        Me.lblTotalCredit.Name = "lblTotalCredit"
        Me.lblTotalCredit.Size = New System.Drawing.Size(129, 18)
        Me.lblTotalCredit.TabIndex = 2
        Me.lblTotalCredit.Text = "Total Credit"
        '
        'nbxTotalDebit
        '
        Me.nbxTotalDebit.BackColor = System.Drawing.SystemColors.Info
        Me.nbxTotalDebit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxTotalDebit.ControlCaption = "Total Debit"
        Me.nbxTotalDebit.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxTotalDebit.DecimalPlaces = -1
        Me.nbxTotalDebit.Enabled = False
        Me.nbxTotalDebit.Location = New System.Drawing.Point(146, 13)
        Me.nbxTotalDebit.MaxValue = 0.0R
        Me.nbxTotalDebit.MinValue = 0.0R
        Me.nbxTotalDebit.MustEnterNumeric = True
        Me.nbxTotalDebit.Name = "nbxTotalDebit"
        Me.nbxTotalDebit.Size = New System.Drawing.Size(183, 20)
        Me.nbxTotalDebit.TabIndex = 1
        Me.nbxTotalDebit.Value = ""
        '
        'lblTotalDebit
        '
        Me.lblTotalDebit.Location = New System.Drawing.Point(11, 13)
        Me.lblTotalDebit.Name = "lblTotalDebit"
        Me.lblTotalDebit.Size = New System.Drawing.Size(129, 18)
        Me.lblTotalDebit.TabIndex = 0
        Me.lblTotalDebit.Text = "Total Debit"
        '
        'tpgBillsIncomeServicePoint
        '
        Me.tpgBillsIncomeServicePoint.Controls.Add(Me.dgvBillsIncomeServicePoint)
        Me.tpgBillsIncomeServicePoint.Controls.Add(Me.stbCompanyName)
        Me.tpgBillsIncomeServicePoint.Controls.Add(Me.cboCompanyNo)
        Me.tpgBillsIncomeServicePoint.Controls.Add(Me.lblCompanyName)
        Me.tpgBillsIncomeServicePoint.Controls.Add(Me.lblCompanyNo)
        Me.tpgBillsIncomeServicePoint.Controls.Add(Me.cboBillModesID)
        Me.tpgBillsIncomeServicePoint.Controls.Add(Me.lblBPBillModesID)
        Me.tpgBillsIncomeServicePoint.Controls.Add(Me.stbBillCustomerName)
        Me.tpgBillsIncomeServicePoint.Controls.Add(Me.cboBillAccountNo)
        Me.tpgBillsIncomeServicePoint.Controls.Add(Me.lblBillCustomerName)
        Me.tpgBillsIncomeServicePoint.Controls.Add(Me.lblBillAccountNo)
        Me.tpgBillsIncomeServicePoint.Location = New System.Drawing.Point(4, 22)
        Me.tpgBillsIncomeServicePoint.Name = "tpgBillsIncomeServicePoint"
        Me.tpgBillsIncomeServicePoint.Size = New System.Drawing.Size(1024, 345)
        Me.tpgBillsIncomeServicePoint.TabIndex = 5
        Me.tpgBillsIncomeServicePoint.Tag = "IncomeSummaries"
        Me.tpgBillsIncomeServicePoint.Text = "Bills Income per Service Point"
        Me.tpgBillsIncomeServicePoint.UseVisualStyleBackColor = True
        '
        'dgvBillsIncomeServicePoint
        '
        Me.dgvBillsIncomeServicePoint.AllowUserToAddRows = False
        Me.dgvBillsIncomeServicePoint.AllowUserToDeleteRows = False
        DataGridViewCellStyle71.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle71.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvBillsIncomeServicePoint.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle71
        Me.dgvBillsIncomeServicePoint.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvBillsIncomeServicePoint.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvBillsIncomeServicePoint.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvBillsIncomeServicePoint.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvBillsIncomeServicePoint.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle72.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle72.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle72.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle72.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle72.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle72.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle72.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBillsIncomeServicePoint.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle72
        Me.dgvBillsIncomeServicePoint.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colBillIncomeCategory, Me.colBillTotalAmount, Me.colBillCoPayAmount, Me.colBillDiscount, Me.colBillPaid, Me.colBillAmount, Me.colBillNotPaid})
        Me.dgvBillsIncomeServicePoint.ContextMenuStrip = Me.cmsIncomeSummaries
        DataGridViewCellStyle79.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle79.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle79.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle79.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle79.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle79.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle79.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvBillsIncomeServicePoint.DefaultCellStyle = DataGridViewCellStyle79
        Me.dgvBillsIncomeServicePoint.GridColor = System.Drawing.Color.Khaki
        Me.dgvBillsIncomeServicePoint.Location = New System.Drawing.Point(15, 98)
        Me.dgvBillsIncomeServicePoint.Name = "dgvBillsIncomeServicePoint"
        Me.dgvBillsIncomeServicePoint.ReadOnly = True
        Me.dgvBillsIncomeServicePoint.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle80.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle80.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle80.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle80.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle80.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle80.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle80.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle80.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBillsIncomeServicePoint.RowHeadersDefaultCellStyle = DataGridViewCellStyle80
        Me.dgvBillsIncomeServicePoint.RowHeadersVisible = False
        Me.dgvBillsIncomeServicePoint.Size = New System.Drawing.Size(930, 257)
        Me.dgvBillsIncomeServicePoint.TabIndex = 23
        Me.dgvBillsIncomeServicePoint.Text = "DataGridView1"
        '
        'colBillIncomeCategory
        '
        Me.colBillIncomeCategory.DataPropertyName = "IncomeCategory"
        Me.colBillIncomeCategory.HeaderText = "Income Category"
        Me.colBillIncomeCategory.Name = "colBillIncomeCategory"
        Me.colBillIncomeCategory.ReadOnly = True
        '
        'colBillTotalAmount
        '
        Me.colBillTotalAmount.DataPropertyName = "TotalAmount"
        DataGridViewCellStyle73.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colBillTotalAmount.DefaultCellStyle = DataGridViewCellStyle73
        Me.colBillTotalAmount.HeaderText = "Total Amount"
        Me.colBillTotalAmount.Name = "colBillTotalAmount"
        Me.colBillTotalAmount.ReadOnly = True
        Me.colBillTotalAmount.Width = 120
        '
        'colBillCoPayAmount
        '
        Me.colBillCoPayAmount.DataPropertyName = "CoPayAmount"
        DataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colBillCoPayAmount.DefaultCellStyle = DataGridViewCellStyle74
        Me.colBillCoPayAmount.HeaderText = "Co-Pay Amount"
        Me.colBillCoPayAmount.Name = "colBillCoPayAmount"
        Me.colBillCoPayAmount.ReadOnly = True
        '
        'colBillDiscount
        '
        Me.colBillDiscount.DataPropertyName = "BillDiscount"
        DataGridViewCellStyle75.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colBillDiscount.DefaultCellStyle = DataGridViewCellStyle75
        Me.colBillDiscount.HeaderText = "Bill Discount"
        Me.colBillDiscount.Name = "colBillDiscount"
        Me.colBillDiscount.ReadOnly = True
        '
        'colBillPaid
        '
        Me.colBillPaid.DataPropertyName = "BillPaid"
        DataGridViewCellStyle76.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colBillPaid.DefaultCellStyle = DataGridViewCellStyle76
        Me.colBillPaid.HeaderText = "Bill Paid"
        Me.colBillPaid.Name = "colBillPaid"
        Me.colBillPaid.ReadOnly = True
        '
        'colBillAmount
        '
        Me.colBillAmount.DataPropertyName = "BillAmount"
        DataGridViewCellStyle77.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colBillAmount.DefaultCellStyle = DataGridViewCellStyle77
        Me.colBillAmount.HeaderText = "Bill Amount"
        Me.colBillAmount.Name = "colBillAmount"
        Me.colBillAmount.ReadOnly = True
        '
        'colBillNotPaid
        '
        Me.colBillNotPaid.DataPropertyName = "BillNotPaid"
        DataGridViewCellStyle78.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colBillNotPaid.DefaultCellStyle = DataGridViewCellStyle78
        Me.colBillNotPaid.HeaderText = "Bill Not Paid"
        Me.colBillNotPaid.Name = "colBillNotPaid"
        Me.colBillNotPaid.ReadOnly = True
        '
        'stbCompanyName
        '
        Me.stbCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbCompanyName.CapitalizeFirstLetter = True
        Me.stbCompanyName.Enabled = False
        Me.stbCompanyName.EntryErrorMSG = ""
        Me.stbCompanyName.Location = New System.Drawing.Point(476, 35)
        Me.stbCompanyName.MaxLength = 60
        Me.stbCompanyName.Multiline = True
        Me.stbCompanyName.Name = "stbCompanyName"
        Me.stbCompanyName.ReadOnly = True
        Me.stbCompanyName.RegularExpression = ""
        Me.stbCompanyName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbCompanyName.Size = New System.Drawing.Size(157, 34)
        Me.stbCompanyName.TabIndex = 19
        '
        'cboCompanyNo
        '
        Me.cboCompanyNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboCompanyNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboCompanyNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboCompanyNo.DropDownWidth = 256
        Me.cboCompanyNo.Enabled = False
        Me.cboCompanyNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboCompanyNo.FormattingEnabled = True
        Me.cboCompanyNo.ItemHeight = 13
        Me.cboCompanyNo.Location = New System.Drawing.Point(476, 12)
        Me.cboCompanyNo.Name = "cboCompanyNo"
        Me.cboCompanyNo.Size = New System.Drawing.Size(157, 21)
        Me.cboCompanyNo.TabIndex = 17
        '
        'lblCompanyName
        '
        Me.lblCompanyName.Enabled = False
        Me.lblCompanyName.Location = New System.Drawing.Point(327, 42)
        Me.lblCompanyName.Name = "lblCompanyName"
        Me.lblCompanyName.Size = New System.Drawing.Size(143, 18)
        Me.lblCompanyName.TabIndex = 18
        Me.lblCompanyName.Text = "To-Bill Company Name"
        '
        'lblCompanyNo
        '
        Me.lblCompanyNo.Enabled = False
        Me.lblCompanyNo.Location = New System.Drawing.Point(327, 13)
        Me.lblCompanyNo.Name = "lblCompanyNo"
        Me.lblCompanyNo.Size = New System.Drawing.Size(143, 18)
        Me.lblCompanyNo.TabIndex = 16
        Me.lblCompanyNo.Text = "To-Bill Company Number"
        '
        'cboBillModesID
        '
        Me.cboBillModesID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboBillModesID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBillModesID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBillModesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboBillModesID.FormattingEnabled = True
        Me.cboBillModesID.ItemHeight = 13
        Me.cboBillModesID.Location = New System.Drawing.Point(161, 9)
        Me.cboBillModesID.Name = "cboBillModesID"
        Me.cboBillModesID.Size = New System.Drawing.Size(157, 21)
        Me.cboBillModesID.TabIndex = 11
        '
        'lblBPBillModesID
        '
        Me.lblBPBillModesID.Location = New System.Drawing.Point(12, 12)
        Me.lblBPBillModesID.Name = "lblBPBillModesID"
        Me.lblBPBillModesID.Size = New System.Drawing.Size(143, 18)
        Me.lblBPBillModesID.TabIndex = 10
        Me.lblBPBillModesID.Text = "To-Bill Account Category"
        '
        'stbBillCustomerName
        '
        Me.stbBillCustomerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBillCustomerName.CapitalizeFirstLetter = False
        Me.stbBillCustomerName.EntryErrorMSG = ""
        Me.stbBillCustomerName.Location = New System.Drawing.Point(161, 58)
        Me.stbBillCustomerName.MaxLength = 41
        Me.stbBillCustomerName.Multiline = True
        Me.stbBillCustomerName.Name = "stbBillCustomerName"
        Me.stbBillCustomerName.ReadOnly = True
        Me.stbBillCustomerName.RegularExpression = ""
        Me.stbBillCustomerName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbBillCustomerName.Size = New System.Drawing.Size(157, 34)
        Me.stbBillCustomerName.TabIndex = 15
        '
        'cboBillAccountNo
        '
        Me.cboBillAccountNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboBillAccountNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBillAccountNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboBillAccountNo.DropDownWidth = 276
        Me.cboBillAccountNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboBillAccountNo.FormattingEnabled = True
        Me.cboBillAccountNo.ItemHeight = 13
        Me.cboBillAccountNo.Location = New System.Drawing.Point(161, 32)
        Me.cboBillAccountNo.Name = "cboBillAccountNo"
        Me.cboBillAccountNo.Size = New System.Drawing.Size(157, 21)
        Me.cboBillAccountNo.TabIndex = 13
        '
        'lblBillCustomerName
        '
        Me.lblBillCustomerName.Location = New System.Drawing.Point(12, 65)
        Me.lblBillCustomerName.Name = "lblBillCustomerName"
        Me.lblBillCustomerName.Size = New System.Drawing.Size(143, 18)
        Me.lblBillCustomerName.TabIndex = 14
        Me.lblBillCustomerName.Text = "To-Bill Customer Name"
        '
        'lblBillAccountNo
        '
        Me.lblBillAccountNo.Location = New System.Drawing.Point(12, 35)
        Me.lblBillAccountNo.Name = "lblBillAccountNo"
        Me.lblBillAccountNo.Size = New System.Drawing.Size(143, 18)
        Me.lblBillAccountNo.TabIndex = 12
        Me.lblBillAccountNo.Text = "To-Bill Account Number"
        '
        'tpgBillsExtraCharges
        '
        Me.tpgBillsExtraCharges.Controls.Add(Me.dgvBillsExtraChargeSummaries)
        Me.tpgBillsExtraCharges.Controls.Add(Me.stbExtraCompanyName)
        Me.tpgBillsExtraCharges.Controls.Add(Me.cboExtraCompanyNo)
        Me.tpgBillsExtraCharges.Controls.Add(Me.lblExtraCompanyName)
        Me.tpgBillsExtraCharges.Controls.Add(Me.lblExtraCompanyNo)
        Me.tpgBillsExtraCharges.Controls.Add(Me.cboExtraBillModesID)
        Me.tpgBillsExtraCharges.Controls.Add(Me.lblExtraBillModesID)
        Me.tpgBillsExtraCharges.Controls.Add(Me.stbExtraCustomerName)
        Me.tpgBillsExtraCharges.Controls.Add(Me.cboExtraAccountNo)
        Me.tpgBillsExtraCharges.Controls.Add(Me.lblExtraCustomerName)
        Me.tpgBillsExtraCharges.Controls.Add(Me.lblExtraAccountNo)
        Me.tpgBillsExtraCharges.Location = New System.Drawing.Point(4, 22)
        Me.tpgBillsExtraCharges.Name = "tpgBillsExtraCharges"
        Me.tpgBillsExtraCharges.Size = New System.Drawing.Size(1024, 345)
        Me.tpgBillsExtraCharges.TabIndex = 6
        Me.tpgBillsExtraCharges.Text = "Bills Extra Charges"
        Me.tpgBillsExtraCharges.UseVisualStyleBackColor = True
        '
        'dgvBillsExtraChargeSummaries
        '
        Me.dgvBillsExtraChargeSummaries.AllowUserToAddRows = False
        Me.dgvBillsExtraChargeSummaries.AllowUserToDeleteRows = False
        DataGridViewCellStyle81.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle81.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvBillsExtraChargeSummaries.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle81
        Me.dgvBillsExtraChargeSummaries.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvBillsExtraChargeSummaries.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvBillsExtraChargeSummaries.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvBillsExtraChargeSummaries.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvBillsExtraChargeSummaries.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle82.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle82.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle82.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle82.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle82.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle82.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle82.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle82.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBillsExtraChargeSummaries.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle82
        Me.dgvBillsExtraChargeSummaries.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colExtraBillChargeCategory, Me.colExtraBillTotalAmount, Me.colExtraBillCoPayAmount, Me.colExtraBillDiscount, Me.colExtraBillPaid, Me.colExtraBillAmount, Me.colExtraBillNotPaid})
        Me.dgvBillsExtraChargeSummaries.ContextMenuStrip = Me.cmsIncomeSummaries
        DataGridViewCellStyle89.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle89.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle89.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle89.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle89.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle89.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle89.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvBillsExtraChargeSummaries.DefaultCellStyle = DataGridViewCellStyle89
        Me.dgvBillsExtraChargeSummaries.GridColor = System.Drawing.Color.Khaki
        Me.dgvBillsExtraChargeSummaries.Location = New System.Drawing.Point(15, 98)
        Me.dgvBillsExtraChargeSummaries.Name = "dgvBillsExtraChargeSummaries"
        Me.dgvBillsExtraChargeSummaries.ReadOnly = True
        Me.dgvBillsExtraChargeSummaries.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle90.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle90.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle90.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle90.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle90.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle90.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle90.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBillsExtraChargeSummaries.RowHeadersDefaultCellStyle = DataGridViewCellStyle90
        Me.dgvBillsExtraChargeSummaries.RowHeadersVisible = False
        Me.dgvBillsExtraChargeSummaries.Size = New System.Drawing.Size(930, 257)
        Me.dgvBillsExtraChargeSummaries.TabIndex = 40
        Me.dgvBillsExtraChargeSummaries.Text = "DataGridView1"
        '
        'colExtraBillChargeCategory
        '
        Me.colExtraBillChargeCategory.DataPropertyName = "ExtraChargeCategory"
        Me.colExtraBillChargeCategory.HeaderText = "Extra Charge Category"
        Me.colExtraBillChargeCategory.Name = "colExtraBillChargeCategory"
        Me.colExtraBillChargeCategory.ReadOnly = True
        Me.colExtraBillChargeCategory.Width = 140
        '
        'colExtraBillTotalAmount
        '
        Me.colExtraBillTotalAmount.DataPropertyName = "TotalAmount"
        DataGridViewCellStyle83.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraBillTotalAmount.DefaultCellStyle = DataGridViewCellStyle83
        Me.colExtraBillTotalAmount.HeaderText = "Total Amount"
        Me.colExtraBillTotalAmount.Name = "colExtraBillTotalAmount"
        Me.colExtraBillTotalAmount.ReadOnly = True
        Me.colExtraBillTotalAmount.Width = 120
        '
        'colExtraBillCoPayAmount
        '
        Me.colExtraBillCoPayAmount.DataPropertyName = "CoPayAmount"
        DataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraBillCoPayAmount.DefaultCellStyle = DataGridViewCellStyle84
        Me.colExtraBillCoPayAmount.HeaderText = "Co-Pay Amount"
        Me.colExtraBillCoPayAmount.Name = "colExtraBillCoPayAmount"
        Me.colExtraBillCoPayAmount.ReadOnly = True
        '
        'colExtraBillDiscount
        '
        Me.colExtraBillDiscount.DataPropertyName = "BillDiscount"
        DataGridViewCellStyle85.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraBillDiscount.DefaultCellStyle = DataGridViewCellStyle85
        Me.colExtraBillDiscount.HeaderText = "Bill Discount"
        Me.colExtraBillDiscount.Name = "colExtraBillDiscount"
        Me.colExtraBillDiscount.ReadOnly = True
        '
        'colExtraBillPaid
        '
        Me.colExtraBillPaid.DataPropertyName = "BillPaid"
        DataGridViewCellStyle86.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraBillPaid.DefaultCellStyle = DataGridViewCellStyle86
        Me.colExtraBillPaid.HeaderText = "Bill Paid"
        Me.colExtraBillPaid.Name = "colExtraBillPaid"
        Me.colExtraBillPaid.ReadOnly = True
        '
        'colExtraBillAmount
        '
        Me.colExtraBillAmount.DataPropertyName = "BillAmount"
        DataGridViewCellStyle87.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraBillAmount.DefaultCellStyle = DataGridViewCellStyle87
        Me.colExtraBillAmount.HeaderText = "Bill Amount"
        Me.colExtraBillAmount.Name = "colExtraBillAmount"
        Me.colExtraBillAmount.ReadOnly = True
        '
        'colExtraBillNotPaid
        '
        Me.colExtraBillNotPaid.DataPropertyName = "BillNotPaid"
        DataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colExtraBillNotPaid.DefaultCellStyle = DataGridViewCellStyle88
        Me.colExtraBillNotPaid.HeaderText = "Bill Not Paid"
        Me.colExtraBillNotPaid.Name = "colExtraBillNotPaid"
        Me.colExtraBillNotPaid.ReadOnly = True
        '
        'stbExtraCompanyName
        '
        Me.stbExtraCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbExtraCompanyName.CapitalizeFirstLetter = True
        Me.stbExtraCompanyName.Enabled = False
        Me.stbExtraCompanyName.EntryErrorMSG = ""
        Me.stbExtraCompanyName.Location = New System.Drawing.Point(476, 35)
        Me.stbExtraCompanyName.MaxLength = 60
        Me.stbExtraCompanyName.Multiline = True
        Me.stbExtraCompanyName.Name = "stbExtraCompanyName"
        Me.stbExtraCompanyName.ReadOnly = True
        Me.stbExtraCompanyName.RegularExpression = ""
        Me.stbExtraCompanyName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbExtraCompanyName.Size = New System.Drawing.Size(157, 34)
        Me.stbExtraCompanyName.TabIndex = 39
        '
        'cboExtraCompanyNo
        '
        Me.cboExtraCompanyNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboExtraCompanyNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboExtraCompanyNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboExtraCompanyNo.DropDownWidth = 256
        Me.cboExtraCompanyNo.Enabled = False
        Me.cboExtraCompanyNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboExtraCompanyNo.FormattingEnabled = True
        Me.cboExtraCompanyNo.ItemHeight = 13
        Me.cboExtraCompanyNo.Location = New System.Drawing.Point(476, 12)
        Me.cboExtraCompanyNo.Name = "cboExtraCompanyNo"
        Me.cboExtraCompanyNo.Size = New System.Drawing.Size(157, 21)
        Me.cboExtraCompanyNo.TabIndex = 37
        '
        'lblExtraCompanyName
        '
        Me.lblExtraCompanyName.Enabled = False
        Me.lblExtraCompanyName.Location = New System.Drawing.Point(327, 42)
        Me.lblExtraCompanyName.Name = "lblExtraCompanyName"
        Me.lblExtraCompanyName.Size = New System.Drawing.Size(143, 18)
        Me.lblExtraCompanyName.TabIndex = 38
        Me.lblExtraCompanyName.Text = "To-Bill Company Name"
        '
        'lblExtraCompanyNo
        '
        Me.lblExtraCompanyNo.Enabled = False
        Me.lblExtraCompanyNo.Location = New System.Drawing.Point(327, 13)
        Me.lblExtraCompanyNo.Name = "lblExtraCompanyNo"
        Me.lblExtraCompanyNo.Size = New System.Drawing.Size(143, 18)
        Me.lblExtraCompanyNo.TabIndex = 36
        Me.lblExtraCompanyNo.Text = "To-Bill Company Number"
        '
        'cboExtraBillModesID
        '
        Me.cboExtraBillModesID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboExtraBillModesID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboExtraBillModesID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExtraBillModesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboExtraBillModesID.FormattingEnabled = True
        Me.cboExtraBillModesID.ItemHeight = 13
        Me.cboExtraBillModesID.Location = New System.Drawing.Point(161, 9)
        Me.cboExtraBillModesID.Name = "cboExtraBillModesID"
        Me.cboExtraBillModesID.Size = New System.Drawing.Size(157, 21)
        Me.cboExtraBillModesID.TabIndex = 31
        '
        'lblExtraBillModesID
        '
        Me.lblExtraBillModesID.Location = New System.Drawing.Point(12, 12)
        Me.lblExtraBillModesID.Name = "lblExtraBillModesID"
        Me.lblExtraBillModesID.Size = New System.Drawing.Size(143, 18)
        Me.lblExtraBillModesID.TabIndex = 30
        Me.lblExtraBillModesID.Text = "To-Bill Account Category"
        '
        'stbExtraCustomerName
        '
        Me.stbExtraCustomerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbExtraCustomerName.CapitalizeFirstLetter = False
        Me.stbExtraCustomerName.EntryErrorMSG = ""
        Me.stbExtraCustomerName.Location = New System.Drawing.Point(161, 58)
        Me.stbExtraCustomerName.MaxLength = 41
        Me.stbExtraCustomerName.Multiline = True
        Me.stbExtraCustomerName.Name = "stbExtraCustomerName"
        Me.stbExtraCustomerName.ReadOnly = True
        Me.stbExtraCustomerName.RegularExpression = ""
        Me.stbExtraCustomerName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbExtraCustomerName.Size = New System.Drawing.Size(157, 34)
        Me.stbExtraCustomerName.TabIndex = 35
        '
        'cboExtraAccountNo
        '
        Me.cboExtraAccountNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboExtraAccountNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboExtraAccountNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboExtraAccountNo.DropDownWidth = 276
        Me.cboExtraAccountNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboExtraAccountNo.FormattingEnabled = True
        Me.cboExtraAccountNo.ItemHeight = 13
        Me.cboExtraAccountNo.Location = New System.Drawing.Point(161, 32)
        Me.cboExtraAccountNo.Name = "cboExtraAccountNo"
        Me.cboExtraAccountNo.Size = New System.Drawing.Size(157, 21)
        Me.cboExtraAccountNo.TabIndex = 33
        '
        'lblExtraCustomerName
        '
        Me.lblExtraCustomerName.Location = New System.Drawing.Point(12, 65)
        Me.lblExtraCustomerName.Name = "lblExtraCustomerName"
        Me.lblExtraCustomerName.Size = New System.Drawing.Size(143, 18)
        Me.lblExtraCustomerName.TabIndex = 34
        Me.lblExtraCustomerName.Text = "To-Bill Customer Name"
        '
        'lblExtraAccountNo
        '
        Me.lblExtraAccountNo.Location = New System.Drawing.Point(12, 35)
        Me.lblExtraAccountNo.Name = "lblExtraAccountNo"
        Me.lblExtraAccountNo.Size = New System.Drawing.Size(143, 18)
        Me.lblExtraAccountNo.TabIndex = 32
        Me.lblExtraAccountNo.Text = "To-Bill Account Number"
        '
        'fbnLoad
        '
        Me.fbnLoad.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnLoad.Location = New System.Drawing.Point(588, 11)
        Me.fbnLoad.Name = "fbnLoad"
        Me.fbnLoad.Size = New System.Drawing.Size(116, 22)
        Me.fbnLoad.TabIndex = 4
        Me.fbnLoad.Text = "Load..."
        '
        'lblEndDate
        '
        Me.lblEndDate.Location = New System.Drawing.Point(292, 15)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(83, 20)
        Me.lblEndDate.TabIndex = 2
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStartDate
        '
        Me.lblStartDate.Location = New System.Drawing.Point(7, 15)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(72, 20)
        Me.lblStartDate.TabIndex = 0
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'fbnExportTo
        '
        Me.fbnExportTo.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnExportTo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnExportTo.Location = New System.Drawing.Point(719, 11)
        Me.fbnExportTo.Name = "fbnExportTo"
        Me.fbnExportTo.Size = New System.Drawing.Size(106, 22)
        Me.fbnExportTo.TabIndex = 5
        Me.fbnExportTo.Text = "Export to Excel..."
        '
        'grpPeriod
        '
        Me.grpPeriod.Controls.Add(Me.dtpEndDate)
        Me.grpPeriod.Controls.Add(Me.dtpStartDate)
        Me.grpPeriod.Controls.Add(Me.fbnLoad)
        Me.grpPeriod.Controls.Add(Me.lblEndDate)
        Me.grpPeriod.Controls.Add(Me.fbnExportTo)
        Me.grpPeriod.Controls.Add(Me.lblStartDate)
        Me.grpPeriod.Location = New System.Drawing.Point(15, 5)
        Me.grpPeriod.Name = "grpPeriod"
        Me.grpPeriod.Size = New System.Drawing.Size(857, 46)
        Me.grpPeriod.TabIndex = 0
        Me.grpPeriod.TabStop = False
        Me.grpPeriod.Text = "Visit Period"
        '
        'dtpEndDate
        '
        Me.dtpEndDate.CustomFormat = "dd MMM yyyy hh:mm tt"
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpEndDate.Location = New System.Drawing.Point(381, 13)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(189, 20)
        Me.dtpEndDate.TabIndex = 4
        '
        'dtpStartDate
        '
        Me.dtpStartDate.CustomFormat = "dd MMM yyyy hh:mm tt"
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpStartDate.Location = New System.Drawing.Point(85, 16)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(189, 20)
        Me.dtpStartDate.TabIndex = 6
        '
        'lblMessage
        '
        Me.lblMessage.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblMessage.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblMessage.Location = New System.Drawing.Point(53, 431)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(867, 46)
        Me.lblMessage.TabIndex = 2
        Me.lblMessage.Text = "Note: Payment done at a later date for a visit falling in the selected period is " & _
    "considered and payment done for a visit not falling in selected period, is not c" & _
    "onsidered."
        Me.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmIncomeSummaries
        '
        Me.AcceptButton = Me.fbnLoad
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(1059, 492)
        Me.Controls.Add(Me.lblMessage)
        Me.Controls.Add(Me.grpPeriod)
        Me.Controls.Add(Me.tbcPeriodicReport)
        Me.Controls.Add(Me.fbnClose)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmIncomeSummaries"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Income Summaries"
        Me.tbcPeriodicReport.ResumeLayout(False)
        Me.tpgIncomeServicePoint.ResumeLayout(False)
        CType(Me.dgvIncomeServicePoint, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsIncomeSummaries.ResumeLayout(False)
        Me.tpgExtraCharges.ResumeLayout(False)
        CType(Me.dgvExtraChargeSummaries, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgDoctorVisits.ResumeLayout(False)
        CType(Me.dgvDoctorVisitSummaries, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgDoctorSpecialtyVisits.ResumeLayout(False)
        CType(Me.dgvDoctorSpecialtyVisitSummaries, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgIncomePaymentDetails.ResumeLayout(False)
        CType(Me.dgvIncomePaymentDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgAccounts.ResumeLayout(False)
        Me.tpgAccounts.PerformLayout()
        Me.tpgBillsIncomeServicePoint.ResumeLayout(False)
        Me.tpgBillsIncomeServicePoint.PerformLayout()
        CType(Me.dgvBillsIncomeServicePoint, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgBillsExtraCharges.ResumeLayout(False)
        Me.tpgBillsExtraCharges.PerformLayout()
        CType(Me.dgvBillsExtraChargeSummaries, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpPeriod.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents tbcPeriodicReport As System.Windows.Forms.TabControl
    Friend WithEvents fbnLoad As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents tpgDoctorVisits As System.Windows.Forms.TabPage
    Friend WithEvents fbnExportTo As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents grpPeriod As System.Windows.Forms.GroupBox
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents tpgIncomeServicePoint As System.Windows.Forms.TabPage
    Friend WithEvents dgvIncomeServicePoint As System.Windows.Forms.DataGridView
    Friend WithEvents cmsIncomeSummaries As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmsIncomeSummariesCopy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmsIncomeSummariesSelectAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tpgExtraCharges As System.Windows.Forms.TabPage
    Friend WithEvents dgvExtraChargeSummaries As System.Windows.Forms.DataGridView
    Friend WithEvents dgvDoctorVisitSummaries As System.Windows.Forms.DataGridView
    Friend WithEvents tpgAccounts As System.Windows.Forms.TabPage
    Friend WithEvents nbxNetBalance As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblNetBalance As System.Windows.Forms.Label
    Friend WithEvents nbxTotalCredit As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblTotalCredit As System.Windows.Forms.Label
    Friend WithEvents nbxTotalDebit As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblTotalDebit As System.Windows.Forms.Label
    Friend WithEvents tpgBillsIncomeServicePoint As System.Windows.Forms.TabPage
    Friend WithEvents tpgBillsExtraCharges As System.Windows.Forms.TabPage
    Friend WithEvents stbCompanyName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents cboCompanyNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompanyName As System.Windows.Forms.Label
    Friend WithEvents lblCompanyNo As System.Windows.Forms.Label
    Friend WithEvents cboBillModesID As System.Windows.Forms.ComboBox
    Friend WithEvents lblBPBillModesID As System.Windows.Forms.Label
    Friend WithEvents stbBillCustomerName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents cboBillAccountNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblBillCustomerName As System.Windows.Forms.Label
    Friend WithEvents lblBillAccountNo As System.Windows.Forms.Label
    Friend WithEvents stbExtraCompanyName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents cboExtraCompanyNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblExtraCompanyName As System.Windows.Forms.Label
    Friend WithEvents lblExtraCompanyNo As System.Windows.Forms.Label
    Friend WithEvents cboExtraBillModesID As System.Windows.Forms.ComboBox
    Friend WithEvents lblExtraBillModesID As System.Windows.Forms.Label
    Friend WithEvents stbExtraCustomerName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents cboExtraAccountNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblExtraCustomerName As System.Windows.Forms.Label
    Friend WithEvents lblExtraAccountNo As System.Windows.Forms.Label
    Friend WithEvents dgvBillsIncomeServicePoint As System.Windows.Forms.DataGridView
    Friend WithEvents colBillIncomeCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBillTotalAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBillCoPayAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBillDiscount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBillPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBillAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBillNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvBillsExtraChargeSummaries As System.Windows.Forms.DataGridView
    Friend WithEvents colExtraBillChargeCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraBillTotalAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraBillCoPayAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraBillDiscount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraBillPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraBillAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraBillNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents colDoctorSeenDoctor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorTotalVisits As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorTotalOnServices As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorTotalAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorCashAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorCoPayAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorTotalCash As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorCashPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorCashDiscount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorCashNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorAccountAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorAccountNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorInsuranceAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorInsuranceNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpgDoctorSpecialtyVisits As System.Windows.Forms.TabPage
    Friend WithEvents dgvDoctorSpecialtyVisitSummaries As System.Windows.Forms.DataGridView
    Friend WithEvents colDoctorSpecialtySeenDoctorSpecialty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorSpecialtyTotalVisits As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorSpecialtyTotalOnServices As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorSpecialtyTotalAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorSpecialtyCashAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorSpecialtyCoPayAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorSpecialtyTotalCash As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorSpecialtyCashPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorSpecialtyCashDiscount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorSpecialtyCashNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorSpecialtyAccountAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorSpecialtyAccountNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorSpecialtyInsuranceAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDoctorSpecialtyInsuranceNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpgIncomePaymentDetails As System.Windows.Forms.TabPage
    Friend WithEvents dgvIncomePaymentDetails As System.Windows.Forms.DataGridView
    Friend WithEvents colPaymentDetailsReceiptNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPaymentDetailsVisitDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPaymentDetailsFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIncomePaymentDetailsCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPaymentDetailsTotalAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPaymentDetailsPaidAfterDays As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPaymentDetailsRecordDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPaymentDetailsRecordTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIncomeCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotalAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCashAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCoPayAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTotalCash As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCashPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCashDiscount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCashNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCoPayNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAccountAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAccountNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInsuranceAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInsuranceNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraChargeCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraTotalAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraCashAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraCoPayAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraTotalCash As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraCashPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraCashDiscount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraCashNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraCoPayNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraAccountAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraAccountNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraInsuranceAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExtraInsuranceNotPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
End Class
