
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOPDSmartBilling : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

    Public Sub New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

    Public Sub New(ByVal defaultVisitNo As String)
        MyClass.New()
        Me.defaultVisitNo = defaultVisitNo
    End Sub
'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOPDSmartBilling))
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.stbVisitNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbPatientNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbPatientName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbAge = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbGender = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbVisitDate = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbToBillNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbToBillInsurance = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbCopayType = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbCopayPercent = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbCopayValue = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbAmount = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbAmountWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.lblVisitNo = New System.Windows.Forms.Label()
        Me.lblPatientNo = New System.Windows.Forms.Label()
        Me.lblPatientName = New System.Windows.Forms.Label()
        Me.lblAge = New System.Windows.Forms.Label()
        Me.lblGender = New System.Windows.Forms.Label()
        Me.lblVisitDate = New System.Windows.Forms.Label()
        Me.lblToBillNo = New System.Windows.Forms.Label()
        Me.lblToBillInsurance = New System.Windows.Forms.Label()
        Me.lblCopayType = New System.Windows.Forms.Label()
        Me.lblCopayPercent = New System.Windows.Forms.Label()
        Me.lblCopayValue = New System.Windows.Forms.Label()
        Me.lblAmount = New System.Windows.Forms.Label()
        Me.lblAmountWords = New System.Windows.Forms.Label()
        Me.dgvSmartBills = New System.Windows.Forms.DataGridView()
        Me.colInclude = New SyncSoft.Common.Win.Controls.GridCheckBoxColumn()
        Me.colVisitNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colItemCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colItemName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colItemCategory = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colQuantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colUnitPrice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCashAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPayStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colItemCategoryID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.lblAgeString = New System.Windows.Forms.Label()
        Me.stbProvisionalLimit = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblProvisionalLimit = New System.Windows.Forms.Label()
        Me.lblPendingToSmartItems = New System.Windows.Forms.Label()
        CType(Me.dgvSmartBills, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(15, 485)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 2
        Me.ebnSaveUpdate.Tag = "OPDSmartBilling"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = False
        '
        'stbVisitNo
        '
        Me.stbVisitNo.BackColor = System.Drawing.SystemColors.Window
        Me.stbVisitNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbVisitNo.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbVisitNo, "VisitNo")
        Me.stbVisitNo.EntryErrorMSG = ""
        Me.stbVisitNo.Location = New System.Drawing.Point(218, 12)
        Me.stbVisitNo.Name = "stbVisitNo"
        Me.stbVisitNo.RegularExpression = ""
        Me.stbVisitNo.Size = New System.Drawing.Size(170, 20)
        Me.stbVisitNo.TabIndex = 4
        '
        'stbPatientNo
        '
        Me.stbPatientNo.BackColor = System.Drawing.SystemColors.Info
        Me.stbPatientNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPatientNo.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbPatientNo, "PatientNo")
        Me.stbPatientNo.Enabled = False
        Me.stbPatientNo.EntryErrorMSG = ""
        Me.stbPatientNo.Location = New System.Drawing.Point(218, 35)
        Me.stbPatientNo.Name = "stbPatientNo"
        Me.stbPatientNo.RegularExpression = ""
        Me.stbPatientNo.Size = New System.Drawing.Size(170, 20)
        Me.stbPatientNo.TabIndex = 6
        '
        'stbPatientName
        '
        Me.stbPatientName.BackColor = System.Drawing.SystemColors.Info
        Me.stbPatientName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPatientName.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbPatientName, "PatientName")
        Me.stbPatientName.Enabled = False
        Me.stbPatientName.EntryErrorMSG = ""
        Me.stbPatientName.Location = New System.Drawing.Point(218, 58)
        Me.stbPatientName.Name = "stbPatientName"
        Me.stbPatientName.RegularExpression = ""
        Me.stbPatientName.Size = New System.Drawing.Size(170, 20)
        Me.stbPatientName.TabIndex = 8
        '
        'stbAge
        '
        Me.stbAge.BackColor = System.Drawing.SystemColors.Info
        Me.stbAge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAge.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbAge, "Age")
        Me.stbAge.Enabled = False
        Me.stbAge.EntryErrorMSG = ""
        Me.stbAge.Location = New System.Drawing.Point(218, 81)
        Me.stbAge.Name = "stbAge"
        Me.stbAge.RegularExpression = ""
        Me.stbAge.Size = New System.Drawing.Size(64, 20)
        Me.stbAge.TabIndex = 10
        '
        'stbGender
        '
        Me.stbGender.BackColor = System.Drawing.SystemColors.Info
        Me.stbGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbGender.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbGender, "Gender")
        Me.stbGender.Enabled = False
        Me.stbGender.EntryErrorMSG = ""
        Me.stbGender.Location = New System.Drawing.Point(218, 104)
        Me.stbGender.Name = "stbGender"
        Me.stbGender.RegularExpression = ""
        Me.stbGender.Size = New System.Drawing.Size(169, 20)
        Me.stbGender.TabIndex = 12
        '
        'stbVisitDate
        '
        Me.stbVisitDate.BackColor = System.Drawing.SystemColors.Info
        Me.stbVisitDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbVisitDate.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbVisitDate, "JoinDate")
        Me.stbVisitDate.Enabled = False
        Me.stbVisitDate.EntryErrorMSG = ""
        Me.stbVisitDate.Location = New System.Drawing.Point(218, 127)
        Me.stbVisitDate.Name = "stbVisitDate"
        Me.stbVisitDate.RegularExpression = ""
        Me.stbVisitDate.Size = New System.Drawing.Size(170, 20)
        Me.stbVisitDate.TabIndex = 14
        '
        'stbToBillNo
        '
        Me.stbToBillNo.BackColor = System.Drawing.SystemColors.Info
        Me.stbToBillNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbToBillNo.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbToBillNo, "ToBillNo")
        Me.stbToBillNo.Enabled = False
        Me.stbToBillNo.EntryErrorMSG = ""
        Me.stbToBillNo.Location = New System.Drawing.Point(218, 150)
        Me.stbToBillNo.Name = "stbToBillNo"
        Me.stbToBillNo.RegularExpression = ""
        Me.stbToBillNo.Size = New System.Drawing.Size(170, 20)
        Me.stbToBillNo.TabIndex = 16
        '
        'stbToBillInsurance
        '
        Me.stbToBillInsurance.BackColor = System.Drawing.SystemColors.Info
        Me.stbToBillInsurance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbToBillInsurance.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbToBillInsurance, "ToBillInsurance")
        Me.stbToBillInsurance.Enabled = False
        Me.stbToBillInsurance.EntryErrorMSG = ""
        Me.stbToBillInsurance.Location = New System.Drawing.Point(680, 12)
        Me.stbToBillInsurance.Name = "stbToBillInsurance"
        Me.stbToBillInsurance.RegularExpression = ""
        Me.stbToBillInsurance.Size = New System.Drawing.Size(170, 20)
        Me.stbToBillInsurance.TabIndex = 18
        '
        'stbCopayType
        '
        Me.stbCopayType.BackColor = System.Drawing.SystemColors.Info
        Me.stbCopayType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbCopayType.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbCopayType, "CopayType")
        Me.stbCopayType.Enabled = False
        Me.stbCopayType.EntryErrorMSG = ""
        Me.stbCopayType.Location = New System.Drawing.Point(680, 35)
        Me.stbCopayType.Name = "stbCopayType"
        Me.stbCopayType.RegularExpression = ""
        Me.stbCopayType.Size = New System.Drawing.Size(170, 20)
        Me.stbCopayType.TabIndex = 20
        '
        'stbCopayPercent
        '
        Me.stbCopayPercent.BackColor = System.Drawing.SystemColors.Info
        Me.stbCopayPercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbCopayPercent.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbCopayPercent, "CopayPercent")
        Me.stbCopayPercent.Enabled = False
        Me.stbCopayPercent.EntryErrorMSG = ""
        Me.stbCopayPercent.Location = New System.Drawing.Point(680, 58)
        Me.stbCopayPercent.Name = "stbCopayPercent"
        Me.stbCopayPercent.RegularExpression = ""
        Me.stbCopayPercent.Size = New System.Drawing.Size(170, 20)
        Me.stbCopayPercent.TabIndex = 22
        '
        'stbCopayValue
        '
        Me.stbCopayValue.BackColor = System.Drawing.SystemColors.Info
        Me.stbCopayValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbCopayValue.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbCopayValue, "CopayValue")
        Me.stbCopayValue.Enabled = False
        Me.stbCopayValue.EntryErrorMSG = ""
        Me.stbCopayValue.Location = New System.Drawing.Point(680, 81)
        Me.stbCopayValue.Name = "stbCopayValue"
        Me.stbCopayValue.RegularExpression = ""
        Me.stbCopayValue.Size = New System.Drawing.Size(170, 20)
        Me.stbCopayValue.TabIndex = 24
        '
        'stbAmount
        '
        Me.stbAmount.BackColor = System.Drawing.SystemColors.Info
        Me.stbAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAmount.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbAmount, "Amount")
        Me.stbAmount.Enabled = False
        Me.stbAmount.EntryErrorMSG = ""
        Me.stbAmount.Location = New System.Drawing.Point(680, 104)
        Me.stbAmount.Name = "stbAmount"
        Me.stbAmount.RegularExpression = ""
        Me.stbAmount.Size = New System.Drawing.Size(170, 20)
        Me.stbAmount.TabIndex = 26
        '
        'stbAmountWords
        '
        Me.stbAmountWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbAmountWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAmountWords.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbAmountWords, "AmountWords")
        Me.stbAmountWords.Enabled = False
        Me.stbAmountWords.EntryErrorMSG = ""
        Me.stbAmountWords.Location = New System.Drawing.Point(680, 127)
        Me.stbAmountWords.Multiline = True
        Me.stbAmountWords.Name = "stbAmountWords"
        Me.stbAmountWords.RegularExpression = ""
        Me.stbAmountWords.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.stbAmountWords.Size = New System.Drawing.Size(170, 43)
        Me.stbAmountWords.TabIndex = 28
        '
        'fbnClose
        '
        Me.fbnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(969, 484)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 3
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'lblVisitNo
        '
        Me.lblVisitNo.Location = New System.Drawing.Point(12, 12)
        Me.lblVisitNo.Name = "lblVisitNo"
        Me.lblVisitNo.Size = New System.Drawing.Size(200, 20)
        Me.lblVisitNo.TabIndex = 5
        Me.lblVisitNo.Text = "Visit No"
        '
        'lblPatientNo
        '
        Me.lblPatientNo.Location = New System.Drawing.Point(12, 35)
        Me.lblPatientNo.Name = "lblPatientNo"
        Me.lblPatientNo.Size = New System.Drawing.Size(200, 20)
        Me.lblPatientNo.TabIndex = 7
        Me.lblPatientNo.Text = "Patient No"
        '
        'lblPatientName
        '
        Me.lblPatientName.Location = New System.Drawing.Point(12, 58)
        Me.lblPatientName.Name = "lblPatientName"
        Me.lblPatientName.Size = New System.Drawing.Size(200, 20)
        Me.lblPatientName.TabIndex = 9
        Me.lblPatientName.Text = "Patient Name"
        '
        'lblAge
        '
        Me.lblAge.Location = New System.Drawing.Point(12, 81)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(200, 20)
        Me.lblAge.TabIndex = 11
        Me.lblAge.Text = "Age"
        '
        'lblGender
        '
        Me.lblGender.Location = New System.Drawing.Point(12, 104)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(200, 20)
        Me.lblGender.TabIndex = 13
        Me.lblGender.Text = "Gender"
        '
        'lblVisitDate
        '
        Me.lblVisitDate.Location = New System.Drawing.Point(12, 127)
        Me.lblVisitDate.Name = "lblVisitDate"
        Me.lblVisitDate.Size = New System.Drawing.Size(200, 20)
        Me.lblVisitDate.TabIndex = 15
        Me.lblVisitDate.Text = "Visit Date"
        '
        'lblToBillNo
        '
        Me.lblToBillNo.Location = New System.Drawing.Point(12, 150)
        Me.lblToBillNo.Name = "lblToBillNo"
        Me.lblToBillNo.Size = New System.Drawing.Size(200, 20)
        Me.lblToBillNo.TabIndex = 17
        Me.lblToBillNo.Text = "To Bill No"
        '
        'lblToBillInsurance
        '
        Me.lblToBillInsurance.Location = New System.Drawing.Point(474, 12)
        Me.lblToBillInsurance.Name = "lblToBillInsurance"
        Me.lblToBillInsurance.Size = New System.Drawing.Size(200, 20)
        Me.lblToBillInsurance.TabIndex = 19
        Me.lblToBillInsurance.Text = "To Bill Insurance No"
        '
        'lblCopayType
        '
        Me.lblCopayType.Location = New System.Drawing.Point(474, 35)
        Me.lblCopayType.Name = "lblCopayType"
        Me.lblCopayType.Size = New System.Drawing.Size(200, 20)
        Me.lblCopayType.TabIndex = 21
        Me.lblCopayType.Text = "Copay Type"
        '
        'lblCopayPercent
        '
        Me.lblCopayPercent.Location = New System.Drawing.Point(474, 58)
        Me.lblCopayPercent.Name = "lblCopayPercent"
        Me.lblCopayPercent.Size = New System.Drawing.Size(200, 20)
        Me.lblCopayPercent.TabIndex = 23
        Me.lblCopayPercent.Text = "Copay Percent"
        '
        'lblCopayValue
        '
        Me.lblCopayValue.Location = New System.Drawing.Point(474, 81)
        Me.lblCopayValue.Name = "lblCopayValue"
        Me.lblCopayValue.Size = New System.Drawing.Size(200, 20)
        Me.lblCopayValue.TabIndex = 25
        Me.lblCopayValue.Text = "Copay Value"
        '
        'lblAmount
        '
        Me.lblAmount.Location = New System.Drawing.Point(474, 104)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(200, 20)
        Me.lblAmount.TabIndex = 27
        Me.lblAmount.Text = "Amount"
        '
        'lblAmountWords
        '
        Me.lblAmountWords.Location = New System.Drawing.Point(474, 127)
        Me.lblAmountWords.Name = "lblAmountWords"
        Me.lblAmountWords.Size = New System.Drawing.Size(200, 20)
        Me.lblAmountWords.TabIndex = 29
        Me.lblAmountWords.Text = "Amount Words"
        '
        'dgvSmartBills
        '
        Me.dgvSmartBills.AllowUserToAddRows = False
        Me.dgvSmartBills.AllowUserToDeleteRows = False
        Me.dgvSmartBills.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvSmartBills.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSmartBills.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSmartBills.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colInclude, Me.colVisitNo, Me.colItemCode, Me.colItemName, Me.colItemCategory, Me.colQuantity, Me.colUnitPrice, Me.colAmount, Me.colCashAmount, Me.colPayStatus, Me.colItemCategoryID})
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvSmartBills.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgvSmartBills.EnableHeadersVisualStyles = False
        Me.dgvSmartBills.GridColor = System.Drawing.Color.Khaki
        Me.dgvSmartBills.Location = New System.Drawing.Point(15, 206)
        Me.dgvSmartBills.Name = "dgvSmartBills"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSmartBills.RowHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvSmartBills.Size = New System.Drawing.Size(1026, 276)
        Me.dgvSmartBills.TabIndex = 40
        Me.dgvSmartBills.Text = "DataGridView1"
        '
        'colInclude
        '
        Me.colInclude.ControlCaption = Nothing
        Me.colInclude.DataPropertyName = "AutoInclude"
        Me.colInclude.HeaderText = "Include"
        Me.colInclude.Name = "colInclude"
        Me.colInclude.SourceColumn = Nothing
        Me.colInclude.Width = 80
        '
        'colVisitNo
        '
        Me.colVisitNo.DataPropertyName = "VisitNo"
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Info
        Me.colVisitNo.DefaultCellStyle = DataGridViewCellStyle2
        Me.colVisitNo.HeaderText = "Visit No"
        Me.colVisitNo.Name = "colVisitNo"
        Me.colVisitNo.ReadOnly = True
        Me.colVisitNo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colItemCode
        '
        Me.colItemCode.DataPropertyName = "ItemCode"
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Info
        Me.colItemCode.DefaultCellStyle = DataGridViewCellStyle3
        Me.colItemCode.HeaderText = "Item Code"
        Me.colItemCode.Name = "colItemCode"
        Me.colItemCode.ReadOnly = True
        '
        'colItemName
        '
        Me.colItemName.DataPropertyName = "ItemName"
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Info
        Me.colItemName.DefaultCellStyle = DataGridViewCellStyle4
        Me.colItemName.HeaderText = "Item Name"
        Me.colItemName.Name = "colItemName"
        Me.colItemName.ReadOnly = True
        Me.colItemName.Width = 200
        '
        'colItemCategory
        '
        Me.colItemCategory.DataPropertyName = "ItemCategory"
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Info
        Me.colItemCategory.DefaultCellStyle = DataGridViewCellStyle5
        Me.colItemCategory.HeaderText = "ItemCategory"
        Me.colItemCategory.Name = "colItemCategory"
        Me.colItemCategory.ReadOnly = True
        Me.colItemCategory.Width = 80
        '
        'colQuantity
        '
        Me.colQuantity.DataPropertyName = "Quantity"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle6.NullValue = Nothing
        Me.colQuantity.DefaultCellStyle = DataGridViewCellStyle6
        Me.colQuantity.HeaderText = "Quantity"
        Me.colQuantity.Name = "colQuantity"
        Me.colQuantity.ReadOnly = True
        Me.colQuantity.Width = 60
        '
        'colUnitPrice
        '
        Me.colUnitPrice.DataPropertyName = "UnitPrice"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.colUnitPrice.DefaultCellStyle = DataGridViewCellStyle7
        Me.colUnitPrice.HeaderText = "Unit Price"
        Me.colUnitPrice.Name = "colUnitPrice"
        Me.colUnitPrice.ReadOnly = True
        Me.colUnitPrice.Width = 80
        '
        'colAmount
        '
        Me.colAmount.DataPropertyName = "Amount"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.colAmount.DefaultCellStyle = DataGridViewCellStyle8
        Me.colAmount.HeaderText = "Amount"
        Me.colAmount.Name = "colAmount"
        Me.colAmount.ReadOnly = True
        '
        'colCashAmount
        '
        Me.colCashAmount.DataPropertyName = "CashAmount"
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Info
        Me.colCashAmount.DefaultCellStyle = DataGridViewCellStyle9
        Me.colCashAmount.HeaderText = "Cash Amount"
        Me.colCashAmount.Name = "colCashAmount"
        Me.colCashAmount.ReadOnly = True
        '
        'colPayStatus
        '
        Me.colPayStatus.DataPropertyName = "PayStatus"
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Info
        Me.colPayStatus.DefaultCellStyle = DataGridViewCellStyle10
        Me.colPayStatus.HeaderText = "Pay Status"
        Me.colPayStatus.Name = "colPayStatus"
        Me.colPayStatus.ReadOnly = True
        Me.colPayStatus.Width = 80
        '
        'colItemCategoryID
        '
        Me.colItemCategoryID.DataPropertyName = "ItemCategoryID"
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Info
        Me.colItemCategoryID.DefaultCellStyle = DataGridViewCellStyle11
        Me.colItemCategoryID.HeaderText = "ItemCategoryID"
        Me.colItemCategoryID.Name = "colItemCategoryID"
        Me.colItemCategoryID.ReadOnly = True
        Me.colItemCategoryID.Visible = False
        '
        'btnLoad
        '
        Me.btnLoad.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoad.Location = New System.Drawing.Point(680, 175)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(44, 24)
        Me.btnLoad.TabIndex = 41
        Me.btnLoad.Tag = ""
        Me.btnLoad.Text = "&Load"
        '
        'lblAgeString
        '
        Me.lblAgeString.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAgeString.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblAgeString.Location = New System.Drawing.Point(287, 81)
        Me.lblAgeString.Name = "lblAgeString"
        Me.lblAgeString.Size = New System.Drawing.Size(100, 19)
        Me.lblAgeString.TabIndex = 42
        '
        'stbProvisionalLimit
        '
        Me.stbProvisionalLimit.BackColor = System.Drawing.SystemColors.Info
        Me.stbProvisionalLimit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbProvisionalLimit.CapitalizeFirstLetter = False
        Me.stbProvisionalLimit.Enabled = False
        Me.stbProvisionalLimit.EntryErrorMSG = ""
        Me.stbProvisionalLimit.Location = New System.Drawing.Point(218, 173)
        Me.stbProvisionalLimit.MaxLength = 60
        Me.stbProvisionalLimit.Name = "stbProvisionalLimit"
        Me.stbProvisionalLimit.RegularExpression = ""
        Me.stbProvisionalLimit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbProvisionalLimit.Size = New System.Drawing.Size(170, 20)
        Me.stbProvisionalLimit.TabIndex = 111
        '
        'lblProvisionalLimit
        '
        Me.lblProvisionalLimit.Location = New System.Drawing.Point(12, 175)
        Me.lblProvisionalLimit.Name = "lblProvisionalLimit"
        Me.lblProvisionalLimit.Size = New System.Drawing.Size(200, 18)
        Me.lblProvisionalLimit.TabIndex = 110
        Me.lblProvisionalLimit.Text = "Provisional Limit"
        '
        'lblPendingToSmartItems
        '
        Me.lblPendingToSmartItems.ForeColor = System.Drawing.Color.Red
        Me.lblPendingToSmartItems.Location = New System.Drawing.Point(474, 173)
        Me.lblPendingToSmartItems.Name = "lblPendingToSmartItems"
        Me.lblPendingToSmartItems.Size = New System.Drawing.Size(200, 20)
        Me.lblPendingToSmartItems.TabIndex = 112
        Me.lblPendingToSmartItems.Text = "Pending To Smart Items"
        '
        'frmOPDSmartBilling
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(1053, 515)
        Me.Controls.Add(Me.lblPendingToSmartItems)
        Me.Controls.Add(Me.stbProvisionalLimit)
        Me.Controls.Add(Me.lblProvisionalLimit)
        Me.Controls.Add(Me.lblAgeString)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.dgvSmartBills)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.stbVisitNo)
        Me.Controls.Add(Me.lblVisitNo)
        Me.Controls.Add(Me.stbPatientNo)
        Me.Controls.Add(Me.lblPatientNo)
        Me.Controls.Add(Me.stbPatientName)
        Me.Controls.Add(Me.lblPatientName)
        Me.Controls.Add(Me.stbAge)
        Me.Controls.Add(Me.lblAge)
        Me.Controls.Add(Me.stbGender)
        Me.Controls.Add(Me.lblGender)
        Me.Controls.Add(Me.stbVisitDate)
        Me.Controls.Add(Me.lblVisitDate)
        Me.Controls.Add(Me.stbToBillNo)
        Me.Controls.Add(Me.lblToBillNo)
        Me.Controls.Add(Me.stbToBillInsurance)
        Me.Controls.Add(Me.lblToBillInsurance)
        Me.Controls.Add(Me.stbCopayType)
        Me.Controls.Add(Me.lblCopayType)
        Me.Controls.Add(Me.stbCopayPercent)
        Me.Controls.Add(Me.lblCopayPercent)
        Me.Controls.Add(Me.stbCopayValue)
        Me.Controls.Add(Me.lblCopayValue)
        Me.Controls.Add(Me.stbAmount)
        Me.Controls.Add(Me.lblAmount)
        Me.Controls.Add(Me.stbAmountWords)
        Me.Controls.Add(Me.lblAmountWords)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmOPDSmartBilling"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "OPD Smart Billing"
        CType(Me.dgvSmartBills, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents stbVisitNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblVisitNo As System.Windows.Forms.Label
    Friend WithEvents stbPatientNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblPatientNo As System.Windows.Forms.Label
    Friend WithEvents stbPatientName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblPatientName As System.Windows.Forms.Label
    Friend WithEvents stbAge As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAge As System.Windows.Forms.Label
    Friend WithEvents stbGender As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents stbVisitDate As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblVisitDate As System.Windows.Forms.Label
    Friend WithEvents stbToBillNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblToBillNo As System.Windows.Forms.Label
    Friend WithEvents stbToBillInsurance As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblToBillInsurance As System.Windows.Forms.Label
    Friend WithEvents stbCopayType As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblCopayType As System.Windows.Forms.Label
    Friend WithEvents stbCopayPercent As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblCopayPercent As System.Windows.Forms.Label
    Friend WithEvents stbCopayValue As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblCopayValue As System.Windows.Forms.Label
    Friend WithEvents stbAmount As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents stbAmountWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAmountWords As System.Windows.Forms.Label
    Friend WithEvents dgvSmartBills As System.Windows.Forms.DataGridView
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents lblAgeString As System.Windows.Forms.Label
    Friend WithEvents stbProvisionalLimit As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblProvisionalLimit As System.Windows.Forms.Label
    Friend WithEvents colInclude As SyncSoft.Common.Win.Controls.GridCheckBoxColumn
    Friend WithEvents colVisitNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colItemCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colItemName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colItemCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colQuantity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colUnitPrice As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCashAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPayStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colItemCategoryID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblPendingToSmartItems As System.Windows.Forms.Label

End Class