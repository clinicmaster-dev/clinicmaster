
Option Strict On

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls

Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects
Imports SyncSoft.SQLDb

Public Class frmToApproveAccountWithdrawRequests

#Region " Fields "
    Private defautRequestStatusID As String
    Dim withdrawTypeID As String = String.Empty
    Private alertControl As Control
#End Region




    Private Sub frmToApproveAccountWithdrawRequests_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            Me.Cursor = Cursors.WaitCursor()
            Me.dtpStartDate.MaxDate = Today
            Me.dtpEndDate.MaxDate = Today.AddDays(1)

            Me.dtpStartDate.Value = Today.AddDays(-1)
            Me.dtpEndDate.Value = Today.AddDays(1)
            Me.LoadToApproveAccountWithdrawRequests()

        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default()
        End Try

    End Sub

    Private Sub LoadToApproveAccountWithdrawRequests()

        Try
            Dim oAccountWithdrawRequests As New AccountWithdrawRequests()
            Dim startDate As Date = DateEnteredIn(Me.dtpStartDate, "Start Date")
            Dim endDate As Date = DateEnteredIn(Me.dtpEndDate, "End Date")
            Dim AccountWithdrawRequests As DataTable = oAccountWithdrawRequests.GetAccountWithdrawRequestsByRequestStatusID(Me.defautRequestStatusID, startDate, endDate, Me.withdrawTypeID).Tables("AccountWithdrawRequests")

            lblRecordsNo.Text = AccountWithdrawRequests.Rows.Count.ToString + " record(s) returned"
            LoadGridData(dgvToApproveAccountWithdraws, AccountWithdrawRequests)
            FormatGridColumn(dgvToApproveAccountWithdraws)

        Catch ex As Exception
            ErrorMessage(ex)
        End Try

    End Sub

    Private Sub fbnLoad_Click(sender As System.Object, e As System.EventArgs) Handles fbnLoad.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.LoadToApproveAccountWithdrawRequests()
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub frmToApproveAccountWithdrawRequests_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub

    Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub


#Region " Edit Methods "

    Public Sub Edit()

        Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update
        Me.ebnSaveUpdate.Enabled = False
        Me.fbnDelete.Visible = True
        Me.fbnDelete.Enabled = False
        Me.fbnSearch.Visible = True

        ResetControlsIn(Me)

    End Sub

    Public Sub Save()

        Me.ebnSaveUpdate.ButtonText = ButtonCaption.Save
        Me.ebnSaveUpdate.Enabled = True
        Me.fbnDelete.Visible = False
        Me.fbnDelete.Enabled = True
        Me.fbnSearch.Visible = False

        ResetControlsIn(Me)

    End Sub

    Private Sub DisplayData(ByVal dataSource As DataTable)

        Try

            Me.ebnSaveUpdate.DataSource = dataSource
            Me.ebnSaveUpdate.LoadData(Me)

            Me.ebnSaveUpdate.Enabled = dataSource.Rows.Count > 0
            Me.fbnDelete.Enabled = dataSource.Rows.Count > 0

            Security.Apply(Me.ebnSaveUpdate, AccessRights.Update)
            Security.Apply(Me.fbnDelete, AccessRights.Delete)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub CallOnKeyEdit()
        If Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update Then
            Me.ebnSaveUpdate.Enabled = False
            Me.fbnDelete.Enabled = False
        End If
    End Sub

#End Region


    Private Sub dgvToApproveAccountWithdraws_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvToApproveAccountWithdraws.CellDoubleClick
        Try

            Dim requestNo As String = Me.dgvToApproveAccountWithdraws.Item(Me.colRequestNo.Name, e.RowIndex).Value.ToString()

            If TypeOf Me.alertControl Is TextBox Then
                CType(Me.alertControl, TextBox).Text = requestNo
                CType(Me.alertControl, TextBox).Focus()

            ElseIf TypeOf Me.alertControl Is SmartTextBox Then
                CType(Me.alertControl, SmartTextBox).Text = requestNo
                CType(Me.alertControl, SmartTextBox).Focus()

            ElseIf TypeOf Me.alertControl Is ComboBox Then
                CType(Me.alertControl, ComboBox).Text = requestNo
                CType(Me.alertControl, ComboBox).Focus()
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Me.Close()

        Catch ex As Exception
            Return
        End Try
    End Sub

End Class