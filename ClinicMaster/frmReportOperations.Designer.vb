﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportOperations
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportOperations))
        Me.lblItemCategory = New System.Windows.Forms.Label()
        Me.cboItemCategoryID = New System.Windows.Forms.ComboBox()
        Me.fbnReportOperations = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.grpSetParameters = New System.Windows.Forms.GroupBox()
        Me.fbnExport = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.pnlPeriod = New System.Windows.Forms.Panel()
        Me.dtpEndDateTime = New System.Windows.Forms.DateTimePicker()
        Me.lblStartDateTime = New System.Windows.Forms.Label()
        Me.dtpStartDateTime = New System.Windows.Forms.DateTimePicker()
        Me.lblEndDateTime = New System.Windows.Forms.Label()
        Me.fbClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.tpgOPDItemsStatus = New System.Windows.Forms.TabPage()
        Me.stbAmountWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbTotalAmount = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblExpenditureTotalAmount = New System.Windows.Forms.Label()
        Me.lblExpenditureAmountWords = New System.Windows.Forms.Label()
        Me.lblPayStatus = New System.Windows.Forms.Label()
        Me.CboPayStatusID = New System.Windows.Forms.ComboBox()
        Me.lblItemStatus = New System.Windows.Forms.Label()
        Me.cboItemStatusID = New System.Windows.Forms.ComboBox()
        Me.dgvOPDItemsStatus = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColOPDUnitCost = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColOPDTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColOPDBillMode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColOPDOfferedBy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColOfferingMachine = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgDoctorVisits = New System.Windows.Forms.TabPage()
        Me.dgvDoctorVisits = New System.Windows.Forms.DataGridView()
        Me.colSeenSpeciality = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColSeenDoctor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColSeenVisits = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBillMode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgServices = New System.Windows.Forms.TabPage()
        Me.dgvItems = New System.Windows.Forms.DataGridView()
        Me.ColItemName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColNoOfPatients = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColDone = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColPending = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColProcessing = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tbcItemOperations = New System.Windows.Forms.TabControl()
        Me.tpgPatientRegistration = New System.Windows.Forms.TabPage()
        Me.dgvPatientRegistration = New System.Windows.Forms.DataGridView()
        Me.colPatientLoginID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPatientFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPatientTotalPatients = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgVisitRegistration = New System.Windows.Forms.TabPage()
        Me.dgvVisitRegistration = New System.Windows.Forms.DataGridView()
        Me.colVisitLoginID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colVisitFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colVisitTotalVisits = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgFileStatus = New System.Windows.Forms.TabPage()
        Me.dgvFileStatus = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgFilesNotSeenByDoctor = New System.Windows.Forms.TabPage()
        Me.dgvFilesNotSeenByDoctor = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColLastVisitDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColGender = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColTakenDateTime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColTakenBy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColBillAccountName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgFilesSeenByDoctor = New System.Windows.Forms.TabPage()
        Me.dgvFilesSeenByDoctor = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColBillModeID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColTotalBill = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColTotalPaid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColBillBalance = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgOverStayedAdmissions = New System.Windows.Forms.TabPage()
        Me.dgvUnclosedVisits = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColAdmissions = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColBedNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColBed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColWard = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColVisitDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColAdmissionDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColDaysOnWard = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColAdmissionNotes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grpSetParameters.SuspendLayout()
        Me.pnlPeriod.SuspendLayout()
        Me.tpgOPDItemsStatus.SuspendLayout()
        CType(Me.dgvOPDItemsStatus, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgDoctorVisits.SuspendLayout()
        CType(Me.dgvDoctorVisits, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgServices.SuspendLayout()
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbcItemOperations.SuspendLayout()
        Me.tpgPatientRegistration.SuspendLayout()
        CType(Me.dgvPatientRegistration, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgVisitRegistration.SuspendLayout()
        CType(Me.dgvVisitRegistration, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgFileStatus.SuspendLayout()
        CType(Me.dgvFileStatus, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgFilesNotSeenByDoctor.SuspendLayout()
        CType(Me.dgvFilesNotSeenByDoctor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgFilesSeenByDoctor.SuspendLayout()
        CType(Me.dgvFilesSeenByDoctor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgOverStayedAdmissions.SuspendLayout()
        CType(Me.dgvUnclosedVisits, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblItemCategory
        '
        Me.lblItemCategory.AutoSize = True
        Me.lblItemCategory.Location = New System.Drawing.Point(80, 46)
        Me.lblItemCategory.Name = "lblItemCategory"
        Me.lblItemCategory.Size = New System.Drawing.Size(72, 13)
        Me.lblItemCategory.TabIndex = 7
        Me.lblItemCategory.Text = "Item Category"
        '
        'cboItemCategoryID
        '
        Me.cboItemCategoryID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboItemCategoryID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboItemCategoryID.BackColor = System.Drawing.SystemColors.Window
        Me.cboItemCategoryID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboItemCategoryID.DropDownWidth = 203
        Me.cboItemCategoryID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboItemCategoryID.FormattingEnabled = True
        Me.cboItemCategoryID.ItemHeight = 13
        Me.cboItemCategoryID.Location = New System.Drawing.Point(158, 43)
        Me.cboItemCategoryID.MaxLength = 20
        Me.cboItemCategoryID.Name = "cboItemCategoryID"
        Me.cboItemCategoryID.Size = New System.Drawing.Size(189, 21)
        Me.cboItemCategoryID.TabIndex = 5
        '
        'fbnReportOperations
        '
        Me.fbnReportOperations.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnReportOperations.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnReportOperations.Location = New System.Drawing.Point(711, 18)
        Me.fbnReportOperations.Name = "fbnReportOperations"
        Me.fbnReportOperations.Size = New System.Drawing.Size(74, 22)
        Me.fbnReportOperations.TabIndex = 6
        Me.fbnReportOperations.Text = "&Load"
        '
        'grpSetParameters
        '
        Me.grpSetParameters.Controls.Add(Me.fbnReportOperations)
        Me.grpSetParameters.Controls.Add(Me.lblItemCategory)
        Me.grpSetParameters.Controls.Add(Me.cboItemCategoryID)
        Me.grpSetParameters.Controls.Add(Me.fbnExport)
        Me.grpSetParameters.Controls.Add(Me.pnlPeriod)
        Me.grpSetParameters.Location = New System.Drawing.Point(9, 8)
        Me.grpSetParameters.Name = "grpSetParameters"
        Me.grpSetParameters.Size = New System.Drawing.Size(953, 82)
        Me.grpSetParameters.TabIndex = 4
        Me.grpSetParameters.TabStop = False
        Me.grpSetParameters.Text = "Collection Period"
        '
        'fbnExport
        '
        Me.fbnExport.Enabled = False
        Me.fbnExport.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnExport.Location = New System.Drawing.Point(791, 18)
        Me.fbnExport.Name = "fbnExport"
        Me.fbnExport.Size = New System.Drawing.Size(74, 22)
        Me.fbnExport.TabIndex = 6
        Me.fbnExport.Text = "&Export"
        Me.fbnExport.UseVisualStyleBackColor = False
        '
        'pnlPeriod
        '
        Me.pnlPeriod.Controls.Add(Me.dtpEndDateTime)
        Me.pnlPeriod.Controls.Add(Me.lblStartDateTime)
        Me.pnlPeriod.Controls.Add(Me.dtpStartDateTime)
        Me.pnlPeriod.Controls.Add(Me.lblEndDateTime)
        Me.pnlPeriod.Location = New System.Drawing.Point(5, 15)
        Me.pnlPeriod.Name = "pnlPeriod"
        Me.pnlPeriod.Size = New System.Drawing.Size(926, 61)
        Me.pnlPeriod.TabIndex = 0
        '
        'dtpEndDateTime
        '
        Me.dtpEndDateTime.CustomFormat = "dd MMM yyyy hh:mm tt"
        Me.dtpEndDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpEndDateTime.Location = New System.Drawing.Point(502, 5)
        Me.dtpEndDateTime.Name = "dtpEndDateTime"
        Me.dtpEndDateTime.ShowCheckBox = True
        Me.dtpEndDateTime.Size = New System.Drawing.Size(189, 20)
        Me.dtpEndDateTime.TabIndex = 3
        '
        'lblStartDateTime
        '
        Me.lblStartDateTime.Location = New System.Drawing.Point(10, 5)
        Me.lblStartDateTime.Name = "lblStartDateTime"
        Me.lblStartDateTime.Size = New System.Drawing.Size(137, 20)
        Me.lblStartDateTime.TabIndex = 0
        Me.lblStartDateTime.Text = "Start Record Date && Time"
        Me.lblStartDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStartDateTime
        '
        Me.dtpStartDateTime.CustomFormat = "dd MMM yyyy hh:mm tt"
        Me.dtpStartDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpStartDateTime.Location = New System.Drawing.Point(153, 5)
        Me.dtpStartDateTime.Name = "dtpStartDateTime"
        Me.dtpStartDateTime.ShowCheckBox = True
        Me.dtpStartDateTime.Size = New System.Drawing.Size(189, 20)
        Me.dtpStartDateTime.TabIndex = 1
        '
        'lblEndDateTime
        '
        Me.lblEndDateTime.Location = New System.Drawing.Point(357, 5)
        Me.lblEndDateTime.Name = "lblEndDateTime"
        Me.lblEndDateTime.Size = New System.Drawing.Size(139, 20)
        Me.lblEndDateTime.TabIndex = 2
        Me.lblEndDateTime.Text = "End Record Date && Time"
        Me.lblEndDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'fbClose
        '
        Me.fbClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbClose.Location = New System.Drawing.Point(945, 499)
        Me.fbClose.Name = "fbClose"
        Me.fbClose.Size = New System.Drawing.Size(74, 22)
        Me.fbClose.TabIndex = 7
        Me.fbClose.Text = "&Close"
        Me.fbClose.UseVisualStyleBackColor = False
        '
        'tpgOPDItemsStatus
        '
        Me.tpgOPDItemsStatus.Controls.Add(Me.stbAmountWords)
        Me.tpgOPDItemsStatus.Controls.Add(Me.stbTotalAmount)
        Me.tpgOPDItemsStatus.Controls.Add(Me.lblExpenditureTotalAmount)
        Me.tpgOPDItemsStatus.Controls.Add(Me.lblExpenditureAmountWords)
        Me.tpgOPDItemsStatus.Controls.Add(Me.lblPayStatus)
        Me.tpgOPDItemsStatus.Controls.Add(Me.CboPayStatusID)
        Me.tpgOPDItemsStatus.Controls.Add(Me.lblItemStatus)
        Me.tpgOPDItemsStatus.Controls.Add(Me.cboItemStatusID)
        Me.tpgOPDItemsStatus.Controls.Add(Me.dgvOPDItemsStatus)
        Me.tpgOPDItemsStatus.Location = New System.Drawing.Point(4, 22)
        Me.tpgOPDItemsStatus.Name = "tpgOPDItemsStatus"
        Me.tpgOPDItemsStatus.Size = New System.Drawing.Size(1001, 371)
        Me.tpgOPDItemsStatus.TabIndex = 13
        Me.tpgOPDItemsStatus.Text = "Items Status"
        Me.tpgOPDItemsStatus.UseVisualStyleBackColor = True
        '
        'stbAmountWords
        '
        Me.stbAmountWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbAmountWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbAmountWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAmountWords.CapitalizeFirstLetter = False
        Me.stbAmountWords.EntryErrorMSG = ""
        Me.stbAmountWords.Location = New System.Drawing.Point(445, 323)
        Me.stbAmountWords.MaxLength = 100
        Me.stbAmountWords.Multiline = True
        Me.stbAmountWords.Name = "stbAmountWords"
        Me.stbAmountWords.ReadOnly = True
        Me.stbAmountWords.RegularExpression = ""
        Me.stbAmountWords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbAmountWords.Size = New System.Drawing.Size(377, 39)
        Me.stbAmountWords.TabIndex = 16
        '
        'stbTotalAmount
        '
        Me.stbTotalAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbTotalAmount.BackColor = System.Drawing.SystemColors.Info
        Me.stbTotalAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalAmount.CapitalizeFirstLetter = False
        Me.stbTotalAmount.Enabled = False
        Me.stbTotalAmount.EntryErrorMSG = ""
        Me.stbTotalAmount.Location = New System.Drawing.Point(103, 332)
        Me.stbTotalAmount.MaxLength = 20
        Me.stbTotalAmount.Name = "stbTotalAmount"
        Me.stbTotalAmount.RegularExpression = ""
        Me.stbTotalAmount.Size = New System.Drawing.Size(184, 20)
        Me.stbTotalAmount.TabIndex = 14
        Me.stbTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblExpenditureTotalAmount
        '
        Me.lblExpenditureTotalAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblExpenditureTotalAmount.Location = New System.Drawing.Point(13, 332)
        Me.lblExpenditureTotalAmount.Name = "lblExpenditureTotalAmount"
        Me.lblExpenditureTotalAmount.Size = New System.Drawing.Size(84, 20)
        Me.lblExpenditureTotalAmount.TabIndex = 13
        Me.lblExpenditureTotalAmount.Text = "Total Amount"
        '
        'lblExpenditureAmountWords
        '
        Me.lblExpenditureAmountWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblExpenditureAmountWords.Location = New System.Drawing.Point(313, 332)
        Me.lblExpenditureAmountWords.Name = "lblExpenditureAmountWords"
        Me.lblExpenditureAmountWords.Size = New System.Drawing.Size(126, 21)
        Me.lblExpenditureAmountWords.TabIndex = 15
        Me.lblExpenditureAmountWords.Text = "Amount in Words"
        '
        'lblPayStatus
        '
        Me.lblPayStatus.AutoSize = True
        Me.lblPayStatus.Location = New System.Drawing.Point(286, 11)
        Me.lblPayStatus.Name = "lblPayStatus"
        Me.lblPayStatus.Size = New System.Drawing.Size(58, 13)
        Me.lblPayStatus.TabIndex = 11
        Me.lblPayStatus.Text = "Pay Status"
        '
        'CboPayStatusID
        '
        Me.CboPayStatusID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CboPayStatusID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CboPayStatusID.BackColor = System.Drawing.SystemColors.Window
        Me.CboPayStatusID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboPayStatusID.DropDownWidth = 203
        Me.CboPayStatusID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CboPayStatusID.FormattingEnabled = True
        Me.CboPayStatusID.ItemHeight = 13
        Me.CboPayStatusID.Location = New System.Drawing.Point(364, 8)
        Me.CboPayStatusID.MaxLength = 20
        Me.CboPayStatusID.Name = "CboPayStatusID"
        Me.CboPayStatusID.Size = New System.Drawing.Size(189, 21)
        Me.CboPayStatusID.TabIndex = 10
        '
        'lblItemStatus
        '
        Me.lblItemStatus.AutoSize = True
        Me.lblItemStatus.Location = New System.Drawing.Point(9, 11)
        Me.lblItemStatus.Name = "lblItemStatus"
        Me.lblItemStatus.Size = New System.Drawing.Size(60, 13)
        Me.lblItemStatus.TabIndex = 9
        Me.lblItemStatus.Text = "Item Status"
        '
        'cboItemStatusID
        '
        Me.cboItemStatusID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboItemStatusID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboItemStatusID.BackColor = System.Drawing.SystemColors.Window
        Me.cboItemStatusID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboItemStatusID.DropDownWidth = 203
        Me.cboItemStatusID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboItemStatusID.FormattingEnabled = True
        Me.cboItemStatusID.ItemHeight = 13
        Me.cboItemStatusID.Location = New System.Drawing.Point(87, 8)
        Me.cboItemStatusID.MaxLength = 20
        Me.cboItemStatusID.Name = "cboItemStatusID"
        Me.cboItemStatusID.Size = New System.Drawing.Size(189, 21)
        Me.cboItemStatusID.TabIndex = 8
        '
        'dgvOPDItemsStatus
        '
        Me.dgvOPDItemsStatus.AllowUserToAddRows = False
        Me.dgvOPDItemsStatus.AllowUserToDeleteRows = False
        Me.dgvOPDItemsStatus.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvOPDItemsStatus.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvOPDItemsStatus.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvOPDItemsStatus.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvOPDItemsStatus.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvOPDItemsStatus.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvOPDItemsStatus.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOPDItemsStatus.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvOPDItemsStatus.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.ColOPDUnitCost, Me.ColOPDTotal, Me.ColOPDBillMode, Me.ColOPDOfferedBy, Me.ColOfferingMachine})
        Me.dgvOPDItemsStatus.EnableHeadersVisualStyles = False
        Me.dgvOPDItemsStatus.GridColor = System.Drawing.Color.Khaki
        Me.dgvOPDItemsStatus.Location = New System.Drawing.Point(3, 35)
        Me.dgvOPDItemsStatus.Name = "dgvOPDItemsStatus"
        Me.dgvOPDItemsStatus.ReadOnly = True
        Me.dgvOPDItemsStatus.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOPDItemsStatus.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvOPDItemsStatus.RowHeadersVisible = False
        Me.dgvOPDItemsStatus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvOPDItemsStatus.Size = New System.Drawing.Size(995, 279)
        Me.dgvOPDItemsStatus.TabIndex = 2
        Me.dgvOPDItemsStatus.Text = "DataGridView1"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "VisitNo"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Visit No"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "PatientFullName"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Patient Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Phone"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Phone Number"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "ItemName"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Item Name"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Quantity"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn5.HeaderText = "Quantity"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'ColOPDUnitCost
        '
        Me.ColOPDUnitCost.DataPropertyName = "UnitPrice"
        Me.ColOPDUnitCost.HeaderText = "Unit Price"
        Me.ColOPDUnitCost.Name = "ColOPDUnitCost"
        Me.ColOPDUnitCost.ReadOnly = True
        '
        'ColOPDTotal
        '
        Me.ColOPDTotal.DataPropertyName = "TotalAmount"
        Me.ColOPDTotal.HeaderText = "Total"
        Me.ColOPDTotal.Name = "ColOPDTotal"
        Me.ColOPDTotal.ReadOnly = True
        '
        'ColOPDBillMode
        '
        Me.ColOPDBillMode.DataPropertyName = "BillMode"
        Me.ColOPDBillMode.HeaderText = "Bill Mode"
        Me.ColOPDBillMode.Name = "ColOPDBillMode"
        Me.ColOPDBillMode.ReadOnly = True
        '
        'ColOPDOfferedBy
        '
        Me.ColOPDOfferedBy.DataPropertyName = "Offeredby"
        Me.ColOPDOfferedBy.HeaderText = "Offered By"
        Me.ColOPDOfferedBy.Name = "ColOPDOfferedBy"
        Me.ColOPDOfferedBy.ReadOnly = True
        '
        'ColOfferingMachine
        '
        Me.ColOfferingMachine.DataPropertyName = "CreatorClientMachine"
        Me.ColOfferingMachine.HeaderText = "Client Machine"
        Me.ColOfferingMachine.Name = "ColOfferingMachine"
        Me.ColOfferingMachine.ReadOnly = True
        '
        'tpgDoctorVisits
        '
        Me.tpgDoctorVisits.Controls.Add(Me.dgvDoctorVisits)
        Me.tpgDoctorVisits.Location = New System.Drawing.Point(4, 22)
        Me.tpgDoctorVisits.Name = "tpgDoctorVisits"
        Me.tpgDoctorVisits.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgDoctorVisits.Size = New System.Drawing.Size(1001, 371)
        Me.tpgDoctorVisits.TabIndex = 12
        Me.tpgDoctorVisits.Text = "OPD Doctor Visits"
        Me.tpgDoctorVisits.UseVisualStyleBackColor = True
        '
        'dgvDoctorVisits
        '
        Me.dgvDoctorVisits.AllowUserToAddRows = False
        Me.dgvDoctorVisits.AllowUserToDeleteRows = False
        Me.dgvDoctorVisits.AllowUserToOrderColumns = True
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle5.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvDoctorVisits.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvDoctorVisits.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvDoctorVisits.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvDoctorVisits.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvDoctorVisits.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDoctorVisits.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvDoctorVisits.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colSeenSpeciality, Me.ColSeenDoctor, Me.ColSeenVisits, Me.colBillMode})
        Me.dgvDoctorVisits.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDoctorVisits.EnableHeadersVisualStyles = False
        Me.dgvDoctorVisits.GridColor = System.Drawing.Color.Khaki
        Me.dgvDoctorVisits.Location = New System.Drawing.Point(3, 3)
        Me.dgvDoctorVisits.Name = "dgvDoctorVisits"
        Me.dgvDoctorVisits.ReadOnly = True
        Me.dgvDoctorVisits.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDoctorVisits.RowHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvDoctorVisits.RowHeadersVisible = False
        Me.dgvDoctorVisits.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvDoctorVisits.Size = New System.Drawing.Size(995, 365)
        Me.dgvDoctorVisits.TabIndex = 2
        Me.dgvDoctorVisits.Text = "DataGridView1"
        '
        'colSeenSpeciality
        '
        Me.colSeenSpeciality.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colSeenSpeciality.DataPropertyName = "SeenSpeciality"
        Me.colSeenSpeciality.HeaderText = "Speciality"
        Me.colSeenSpeciality.Name = "colSeenSpeciality"
        Me.colSeenSpeciality.ReadOnly = True
        '
        'ColSeenDoctor
        '
        Me.ColSeenDoctor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColSeenDoctor.DataPropertyName = "SeenDoctor"
        Me.ColSeenDoctor.HeaderText = "Seen Doctor"
        Me.ColSeenDoctor.Name = "ColSeenDoctor"
        Me.ColSeenDoctor.ReadOnly = True
        '
        'ColSeenVisits
        '
        Me.ColSeenVisits.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColSeenVisits.DataPropertyName = "SeenVisits"
        Me.ColSeenVisits.HeaderText = "Seen Visits"
        Me.ColSeenVisits.Name = "ColSeenVisits"
        Me.ColSeenVisits.ReadOnly = True
        '
        'colBillMode
        '
        Me.colBillMode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colBillMode.DataPropertyName = "BillMode"
        Me.colBillMode.HeaderText = "Bill Mode"
        Me.colBillMode.Name = "colBillMode"
        Me.colBillMode.ReadOnly = True
        '
        'tpgServices
        '
        Me.tpgServices.Controls.Add(Me.dgvItems)
        Me.tpgServices.Location = New System.Drawing.Point(4, 22)
        Me.tpgServices.Name = "tpgServices"
        Me.tpgServices.Size = New System.Drawing.Size(1001, 371)
        Me.tpgServices.TabIndex = 11
        Me.tpgServices.Text = "OPD Services"
        Me.tpgServices.UseVisualStyleBackColor = True
        '
        'dgvItems
        '
        Me.dgvItems.AllowUserToAddRows = False
        Me.dgvItems.AllowUserToDeleteRows = False
        Me.dgvItems.AllowUserToOrderColumns = True
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle8.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvItems.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvItems.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvItems.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvItems.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvItems.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvItems.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgvItems.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColItemName, Me.ColNoOfPatients, Me.ColDone, Me.ColPending, Me.ColProcessing})
        Me.dgvItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvItems.EnableHeadersVisualStyles = False
        Me.dgvItems.GridColor = System.Drawing.Color.Khaki
        Me.dgvItems.Location = New System.Drawing.Point(0, 0)
        Me.dgvItems.Name = "dgvItems"
        Me.dgvItems.ReadOnly = True
        Me.dgvItems.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle11.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvItems.RowHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgvItems.RowHeadersVisible = False
        Me.dgvItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvItems.Size = New System.Drawing.Size(1001, 371)
        Me.dgvItems.TabIndex = 1
        Me.dgvItems.Text = "DataGridView1"
        '
        'ColItemName
        '
        Me.ColItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColItemName.DataPropertyName = "ItemName"
        Me.ColItemName.HeaderText = "Item Name"
        Me.ColItemName.Name = "ColItemName"
        Me.ColItemName.ReadOnly = True
        '
        'ColNoOfPatients
        '
        Me.ColNoOfPatients.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColNoOfPatients.DataPropertyName = "NoOfPatients"
        Me.ColNoOfPatients.HeaderText = "No Of Patients"
        Me.ColNoOfPatients.Name = "ColNoOfPatients"
        Me.ColNoOfPatients.ReadOnly = True
        '
        'ColDone
        '
        Me.ColDone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColDone.DataPropertyName = "Offered"
        Me.ColDone.HeaderText = "Done"
        Me.ColDone.Name = "ColDone"
        Me.ColDone.ReadOnly = True
        '
        'ColPending
        '
        Me.ColPending.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColPending.DataPropertyName = "Pending"
        Me.ColPending.HeaderText = "Pending"
        Me.ColPending.Name = "ColPending"
        Me.ColPending.ReadOnly = True
        '
        'ColProcessing
        '
        Me.ColProcessing.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColProcessing.DataPropertyName = "Processing"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.ColProcessing.DefaultCellStyle = DataGridViewCellStyle10
        Me.ColProcessing.HeaderText = "Processing"
        Me.ColProcessing.Name = "ColProcessing"
        Me.ColProcessing.ReadOnly = True
        '
        'tbcItemOperations
        '
        Me.tbcItemOperations.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbcItemOperations.Controls.Add(Me.tpgServices)
        Me.tbcItemOperations.Controls.Add(Me.tpgDoctorVisits)
        Me.tbcItemOperations.Controls.Add(Me.tpgOPDItemsStatus)
        Me.tbcItemOperations.Controls.Add(Me.tpgPatientRegistration)
        Me.tbcItemOperations.Controls.Add(Me.tpgVisitRegistration)
        Me.tbcItemOperations.Controls.Add(Me.tpgFileStatus)
        Me.tbcItemOperations.Controls.Add(Me.tpgFilesNotSeenByDoctor)
        Me.tbcItemOperations.Controls.Add(Me.tpgFilesSeenByDoctor)
        Me.tbcItemOperations.Controls.Add(Me.tpgOverStayedAdmissions)
        Me.tbcItemOperations.HotTrack = True
        Me.tbcItemOperations.Location = New System.Drawing.Point(14, 96)
        Me.tbcItemOperations.Name = "tbcItemOperations"
        Me.tbcItemOperations.SelectedIndex = 0
        Me.tbcItemOperations.Size = New System.Drawing.Size(1009, 397)
        Me.tbcItemOperations.TabIndex = 5
        '
        'tpgPatientRegistration
        '
        Me.tpgPatientRegistration.Controls.Add(Me.dgvPatientRegistration)
        Me.tpgPatientRegistration.Location = New System.Drawing.Point(4, 22)
        Me.tpgPatientRegistration.Name = "tpgPatientRegistration"
        Me.tpgPatientRegistration.Size = New System.Drawing.Size(1001, 371)
        Me.tpgPatientRegistration.TabIndex = 14
        Me.tpgPatientRegistration.Tag = "PatientRegistration"
        Me.tpgPatientRegistration.Text = "Patient Registration "
        Me.tpgPatientRegistration.UseVisualStyleBackColor = True
        '
        'dgvPatientRegistration
        '
        Me.dgvPatientRegistration.AllowUserToAddRows = False
        Me.dgvPatientRegistration.AllowUserToDeleteRows = False
        Me.dgvPatientRegistration.AllowUserToOrderColumns = True
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle12.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvPatientRegistration.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvPatientRegistration.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvPatientRegistration.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvPatientRegistration.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvPatientRegistration.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPatientRegistration.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvPatientRegistration.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colPatientLoginID, Me.colPatientFullName, Me.colPatientTotalPatients})
        Me.dgvPatientRegistration.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvPatientRegistration.EnableHeadersVisualStyles = False
        Me.dgvPatientRegistration.GridColor = System.Drawing.Color.Khaki
        Me.dgvPatientRegistration.Location = New System.Drawing.Point(0, 0)
        Me.dgvPatientRegistration.Name = "dgvPatientRegistration"
        Me.dgvPatientRegistration.ReadOnly = True
        Me.dgvPatientRegistration.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle14.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPatientRegistration.RowHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.dgvPatientRegistration.RowHeadersVisible = False
        Me.dgvPatientRegistration.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvPatientRegistration.Size = New System.Drawing.Size(1001, 371)
        Me.dgvPatientRegistration.TabIndex = 2
        Me.dgvPatientRegistration.Text = "DataGridView1"
        '
        'colPatientLoginID
        '
        Me.colPatientLoginID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colPatientLoginID.DataPropertyName = "LoginID"
        Me.colPatientLoginID.HeaderText = "Login ID"
        Me.colPatientLoginID.Name = "colPatientLoginID"
        Me.colPatientLoginID.ReadOnly = True
        '
        'colPatientFullName
        '
        Me.colPatientFullName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colPatientFullName.DataPropertyName = "FullName"
        Me.colPatientFullName.HeaderText = "Full Name"
        Me.colPatientFullName.Name = "colPatientFullName"
        Me.colPatientFullName.ReadOnly = True
        '
        'colPatientTotalPatients
        '
        Me.colPatientTotalPatients.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colPatientTotalPatients.DataPropertyName = "TotalPatients"
        Me.colPatientTotalPatients.HeaderText = "No. of Patients"
        Me.colPatientTotalPatients.Name = "colPatientTotalPatients"
        Me.colPatientTotalPatients.ReadOnly = True
        '
        'tpgVisitRegistration
        '
        Me.tpgVisitRegistration.Controls.Add(Me.dgvVisitRegistration)
        Me.tpgVisitRegistration.Location = New System.Drawing.Point(4, 22)
        Me.tpgVisitRegistration.Name = "tpgVisitRegistration"
        Me.tpgVisitRegistration.Size = New System.Drawing.Size(1001, 371)
        Me.tpgVisitRegistration.TabIndex = 15
        Me.tpgVisitRegistration.Tag = "VisitRegistration"
        Me.tpgVisitRegistration.Text = "Visit Registration"
        Me.tpgVisitRegistration.UseVisualStyleBackColor = True
        '
        'dgvVisitRegistration
        '
        Me.dgvVisitRegistration.AllowUserToAddRows = False
        Me.dgvVisitRegistration.AllowUserToDeleteRows = False
        Me.dgvVisitRegistration.AllowUserToOrderColumns = True
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle15.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvVisitRegistration.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle15
        Me.dgvVisitRegistration.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvVisitRegistration.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvVisitRegistration.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvVisitRegistration.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle16.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvVisitRegistration.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.dgvVisitRegistration.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colVisitLoginID, Me.colVisitFullName, Me.colVisitTotalVisits})
        Me.dgvVisitRegistration.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvVisitRegistration.EnableHeadersVisualStyles = False
        Me.dgvVisitRegistration.GridColor = System.Drawing.Color.Khaki
        Me.dgvVisitRegistration.Location = New System.Drawing.Point(0, 0)
        Me.dgvVisitRegistration.Name = "dgvVisitRegistration"
        Me.dgvVisitRegistration.ReadOnly = True
        Me.dgvVisitRegistration.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle17.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvVisitRegistration.RowHeadersDefaultCellStyle = DataGridViewCellStyle17
        Me.dgvVisitRegistration.RowHeadersVisible = False
        Me.dgvVisitRegistration.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvVisitRegistration.Size = New System.Drawing.Size(1001, 371)
        Me.dgvVisitRegistration.TabIndex = 3
        Me.dgvVisitRegistration.Text = "DataGridView1"
        '
        'colVisitLoginID
        '
        Me.colVisitLoginID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colVisitLoginID.DataPropertyName = "LoginID"
        Me.colVisitLoginID.HeaderText = "Login ID"
        Me.colVisitLoginID.Name = "colVisitLoginID"
        Me.colVisitLoginID.ReadOnly = True
        '
        'colVisitFullName
        '
        Me.colVisitFullName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colVisitFullName.DataPropertyName = "FullName"
        Me.colVisitFullName.HeaderText = "Full Name"
        Me.colVisitFullName.Name = "colVisitFullName"
        Me.colVisitFullName.ReadOnly = True
        '
        'colVisitTotalVisits
        '
        Me.colVisitTotalVisits.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colVisitTotalVisits.DataPropertyName = "TotalVisits"
        Me.colVisitTotalVisits.HeaderText = "No. of Visits"
        Me.colVisitTotalVisits.Name = "colVisitTotalVisits"
        Me.colVisitTotalVisits.ReadOnly = True
        '
        'tpgFileStatus
        '
        Me.tpgFileStatus.Controls.Add(Me.dgvFileStatus)
        Me.tpgFileStatus.Location = New System.Drawing.Point(4, 22)
        Me.tpgFileStatus.Name = "tpgFileStatus"
        Me.tpgFileStatus.Size = New System.Drawing.Size(1001, 371)
        Me.tpgFileStatus.TabIndex = 16
        Me.tpgFileStatus.Text = "File Status"
        Me.tpgFileStatus.UseVisualStyleBackColor = True
        '
        'dgvFileStatus
        '
        Me.dgvFileStatus.AllowUserToAddRows = False
        Me.dgvFileStatus.AllowUserToDeleteRows = False
        Me.dgvFileStatus.AllowUserToOrderColumns = True
        DataGridViewCellStyle18.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle18.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvFileStatus.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle18
        Me.dgvFileStatus.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvFileStatus.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvFileStatus.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvFileStatus.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle19.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle19.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvFileStatus.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle19
        Me.dgvFileStatus.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8})
        Me.dgvFileStatus.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvFileStatus.EnableHeadersVisualStyles = False
        Me.dgvFileStatus.GridColor = System.Drawing.Color.Khaki
        Me.dgvFileStatus.Location = New System.Drawing.Point(0, 0)
        Me.dgvFileStatus.Name = "dgvFileStatus"
        Me.dgvFileStatus.ReadOnly = True
        Me.dgvFileStatus.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle20.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvFileStatus.RowHeadersDefaultCellStyle = DataGridViewCellStyle20
        Me.dgvFileStatus.RowHeadersVisible = False
        Me.dgvFileStatus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvFileStatus.Size = New System.Drawing.Size(1001, 371)
        Me.dgvFileStatus.TabIndex = 4
        Me.dgvFileStatus.Text = "DataGridView1"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "FilesOut"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Out Ward Files No"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "SeenDoctor"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Seen Doctor No"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "NotSeenDoctor"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Not Seen Doctor No"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'tpgFilesNotSeenByDoctor
        '
        Me.tpgFilesNotSeenByDoctor.Controls.Add(Me.dgvFilesNotSeenByDoctor)
        Me.tpgFilesNotSeenByDoctor.Location = New System.Drawing.Point(4, 22)
        Me.tpgFilesNotSeenByDoctor.Name = "tpgFilesNotSeenByDoctor"
        Me.tpgFilesNotSeenByDoctor.Size = New System.Drawing.Size(1001, 371)
        Me.tpgFilesNotSeenByDoctor.TabIndex = 17
        Me.tpgFilesNotSeenByDoctor.Text = "Files Not Seen By Doctor"
        Me.tpgFilesNotSeenByDoctor.UseVisualStyleBackColor = True
        '
        'dgvFilesNotSeenByDoctor
        '
        Me.dgvFilesNotSeenByDoctor.AllowUserToAddRows = False
        Me.dgvFilesNotSeenByDoctor.AllowUserToDeleteRows = False
        Me.dgvFilesNotSeenByDoctor.AllowUserToOrderColumns = True
        DataGridViewCellStyle21.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle21.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvFilesNotSeenByDoctor.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle21
        Me.dgvFilesNotSeenByDoctor.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvFilesNotSeenByDoctor.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvFilesNotSeenByDoctor.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvFilesNotSeenByDoctor.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle22.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle22.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvFilesNotSeenByDoctor.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle22
        Me.dgvFilesNotSeenByDoctor.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.ColLastVisitDate, Me.ColGender, Me.ColTakenDateTime, Me.ColTakenBy, Me.ColBillAccountName})
        Me.dgvFilesNotSeenByDoctor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvFilesNotSeenByDoctor.EnableHeadersVisualStyles = False
        Me.dgvFilesNotSeenByDoctor.GridColor = System.Drawing.Color.Khaki
        Me.dgvFilesNotSeenByDoctor.Location = New System.Drawing.Point(0, 0)
        Me.dgvFilesNotSeenByDoctor.Name = "dgvFilesNotSeenByDoctor"
        Me.dgvFilesNotSeenByDoctor.ReadOnly = True
        Me.dgvFilesNotSeenByDoctor.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle24.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle24.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvFilesNotSeenByDoctor.RowHeadersDefaultCellStyle = DataGridViewCellStyle24
        Me.dgvFilesNotSeenByDoctor.RowHeadersVisible = False
        Me.dgvFilesNotSeenByDoctor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvFilesNotSeenByDoctor.Size = New System.Drawing.Size(1001, 371)
        Me.dgvFilesNotSeenByDoctor.TabIndex = 2
        Me.dgvFilesNotSeenByDoctor.Text = "DataGridView1"
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "FullName"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Patient Full Name"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "JoinDate"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Join Date"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "Phone"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Phone"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "Age"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Age"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "VisitNo"
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn13.DefaultCellStyle = DataGridViewCellStyle23
        Me.DataGridViewTextBoxColumn13.HeaderText = "Visit No"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        '
        'ColLastVisitDate
        '
        Me.ColLastVisitDate.DataPropertyName = "LastVisitDate"
        Me.ColLastVisitDate.HeaderText = "Last Visit Date"
        Me.ColLastVisitDate.Name = "ColLastVisitDate"
        Me.ColLastVisitDate.ReadOnly = True
        '
        'ColGender
        '
        Me.ColGender.DataPropertyName = "Gender"
        Me.ColGender.HeaderText = "Gender"
        Me.ColGender.Name = "ColGender"
        Me.ColGender.ReadOnly = True
        '
        'ColTakenDateTime
        '
        Me.ColTakenDateTime.DataPropertyName = "TakenDateTime"
        Me.ColTakenDateTime.HeaderText = "Taken Date Time"
        Me.ColTakenDateTime.Name = "ColTakenDateTime"
        Me.ColTakenDateTime.ReadOnly = True
        '
        'ColTakenBy
        '
        Me.ColTakenBy.DataPropertyName = "TakenBy"
        Me.ColTakenBy.HeaderText = "Taken By"
        Me.ColTakenBy.Name = "ColTakenBy"
        Me.ColTakenBy.ReadOnly = True
        '
        'ColBillAccountName
        '
        Me.ColBillAccountName.DataPropertyName = "BillAccountName"
        Me.ColBillAccountName.HeaderText = "Bill Account Name"
        Me.ColBillAccountName.Name = "ColBillAccountName"
        Me.ColBillAccountName.ReadOnly = True
        '
        'tpgFilesSeenByDoctor
        '
        Me.tpgFilesSeenByDoctor.Controls.Add(Me.dgvFilesSeenByDoctor)
        Me.tpgFilesSeenByDoctor.Location = New System.Drawing.Point(4, 22)
        Me.tpgFilesSeenByDoctor.Name = "tpgFilesSeenByDoctor"
        Me.tpgFilesSeenByDoctor.Size = New System.Drawing.Size(1001, 371)
        Me.tpgFilesSeenByDoctor.TabIndex = 18
        Me.tpgFilesSeenByDoctor.Text = "Files Seen By Doctor"
        Me.tpgFilesSeenByDoctor.UseVisualStyleBackColor = True
        '
        'dgvFilesSeenByDoctor
        '
        Me.dgvFilesSeenByDoctor.AllowUserToAddRows = False
        Me.dgvFilesSeenByDoctor.AllowUserToDeleteRows = False
        Me.dgvFilesSeenByDoctor.AllowUserToOrderColumns = True
        DataGridViewCellStyle25.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle25.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvFilesSeenByDoctor.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle25
        Me.dgvFilesSeenByDoctor.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvFilesSeenByDoctor.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvFilesSeenByDoctor.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvFilesSeenByDoctor.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle26.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle26.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvFilesSeenByDoctor.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle26
        Me.dgvFilesSeenByDoctor.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.ColBillModeID, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn20, Me.ColTotalBill, Me.ColTotalPaid, Me.ColBillBalance, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn21, Me.DataGridViewTextBoxColumn22, Me.DataGridViewTextBoxColumn23})
        Me.dgvFilesSeenByDoctor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvFilesSeenByDoctor.EnableHeadersVisualStyles = False
        Me.dgvFilesSeenByDoctor.GridColor = System.Drawing.Color.Khaki
        Me.dgvFilesSeenByDoctor.Location = New System.Drawing.Point(0, 0)
        Me.dgvFilesSeenByDoctor.Name = "dgvFilesSeenByDoctor"
        Me.dgvFilesSeenByDoctor.ReadOnly = True
        Me.dgvFilesSeenByDoctor.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle28.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle28.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvFilesSeenByDoctor.RowHeadersDefaultCellStyle = DataGridViewCellStyle28
        Me.dgvFilesSeenByDoctor.RowHeadersVisible = False
        Me.dgvFilesSeenByDoctor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvFilesSeenByDoctor.Size = New System.Drawing.Size(1001, 371)
        Me.dgvFilesSeenByDoctor.TabIndex = 3
        Me.dgvFilesSeenByDoctor.Text = "DataGridView1"
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "FullName"
        Me.DataGridViewTextBoxColumn14.HeaderText = "Patient Full Name"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Width = 120
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "JoinDate"
        Me.DataGridViewTextBoxColumn15.HeaderText = "Join Date"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "Phone"
        Me.DataGridViewTextBoxColumn16.HeaderText = "Phone"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "Age"
        Me.DataGridViewTextBoxColumn17.HeaderText = "Age"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        '
        'ColBillModeID
        '
        Me.ColBillModeID.DataPropertyName = "BillMode"
        Me.ColBillModeID.HeaderText = "Bill Mode"
        Me.ColBillModeID.Name = "ColBillModeID"
        Me.ColBillModeID.ReadOnly = True
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.DataPropertyName = "VisitNo"
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn18.DefaultCellStyle = DataGridViewCellStyle27
        Me.DataGridViewTextBoxColumn18.HeaderText = "Visit No"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.DataPropertyName = "Gender"
        Me.DataGridViewTextBoxColumn20.HeaderText = "Gender"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        '
        'ColTotalBill
        '
        Me.ColTotalBill.DataPropertyName = "TotalBill"
        Me.ColTotalBill.HeaderText = "Total Bill"
        Me.ColTotalBill.Name = "ColTotalBill"
        Me.ColTotalBill.ReadOnly = True
        '
        'ColTotalPaid
        '
        Me.ColTotalPaid.DataPropertyName = "TotalPaid"
        Me.ColTotalPaid.HeaderText = "Total Paid"
        Me.ColTotalPaid.Name = "ColTotalPaid"
        Me.ColTotalPaid.ReadOnly = True
        '
        'ColBillBalance
        '
        Me.ColBillBalance.DataPropertyName = "BillBalance"
        Me.ColBillBalance.HeaderText = "Bill Balance"
        Me.ColBillBalance.Name = "ColBillBalance"
        Me.ColBillBalance.ReadOnly = True
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.DataPropertyName = "LastVisitDate"
        Me.DataGridViewTextBoxColumn19.HeaderText = "Last Visit Date"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.DataPropertyName = "TakenDateTime"
        Me.DataGridViewTextBoxColumn21.HeaderText = "Taken Date Time"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.DataPropertyName = "TakenBy"
        Me.DataGridViewTextBoxColumn22.HeaderText = "Taken By"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.DataPropertyName = "BillAccountName"
        Me.DataGridViewTextBoxColumn23.HeaderText = "Bill Account Name"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        '
        'tpgOverStayedAdmissions
        '
        Me.tpgOverStayedAdmissions.Controls.Add(Me.dgvUnclosedVisits)
        Me.tpgOverStayedAdmissions.Location = New System.Drawing.Point(4, 22)
        Me.tpgOverStayedAdmissions.Name = "tpgOverStayedAdmissions"
        Me.tpgOverStayedAdmissions.Size = New System.Drawing.Size(1001, 371)
        Me.tpgOverStayedAdmissions.TabIndex = 19
        Me.tpgOverStayedAdmissions.Text = "Admissions Over Due"
        Me.tpgOverStayedAdmissions.UseVisualStyleBackColor = True
        '
        'dgvUnclosedVisits
        '
        Me.dgvUnclosedVisits.AllowUserToAddRows = False
        Me.dgvUnclosedVisits.AllowUserToDeleteRows = False
        Me.dgvUnclosedVisits.AllowUserToOrderColumns = True
        DataGridViewCellStyle29.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle29.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvUnclosedVisits.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle29
        Me.dgvUnclosedVisits.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvUnclosedVisits.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvUnclosedVisits.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvUnclosedVisits.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle30.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle30.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvUnclosedVisits.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle30
        Me.dgvUnclosedVisits.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn25, Me.DataGridViewTextBoxColumn24, Me.DataGridViewTextBoxColumn26, Me.ColAdmissions, Me.ColBedNo, Me.ColBed, Me.ColWard, Me.ColVisitDate, Me.ColAdmissionDate, Me.ColDaysOnWard, Me.ColAdmissionNotes})
        Me.dgvUnclosedVisits.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvUnclosedVisits.EnableHeadersVisualStyles = False
        Me.dgvUnclosedVisits.GridColor = System.Drawing.Color.Khaki
        Me.dgvUnclosedVisits.Location = New System.Drawing.Point(0, 0)
        Me.dgvUnclosedVisits.Name = "dgvUnclosedVisits"
        Me.dgvUnclosedVisits.ReadOnly = True
        Me.dgvUnclosedVisits.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle31.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle31.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvUnclosedVisits.RowHeadersDefaultCellStyle = DataGridViewCellStyle31
        Me.dgvUnclosedVisits.RowHeadersVisible = False
        Me.dgvUnclosedVisits.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvUnclosedVisits.Size = New System.Drawing.Size(1001, 371)
        Me.dgvUnclosedVisits.TabIndex = 5
        Me.dgvUnclosedVisits.Text = "DataGridView1"
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn25.DataPropertyName = "VisitNo"
        Me.DataGridViewTextBoxColumn25.HeaderText = "Visit No"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.ReadOnly = True
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn24.DataPropertyName = "FullName"
        Me.DataGridViewTextBoxColumn24.HeaderText = "Full Name"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = True
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn26.DataPropertyName = "Phone"
        Me.DataGridViewTextBoxColumn26.HeaderText = "Phone No"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        Me.DataGridViewTextBoxColumn26.ReadOnly = True
        '
        'ColAdmissions
        '
        Me.ColAdmissions.DataPropertyName = "AdmissionNo"
        Me.ColAdmissions.HeaderText = "Admission No"
        Me.ColAdmissions.Name = "ColAdmissions"
        Me.ColAdmissions.ReadOnly = True
        '
        'ColBedNo
        '
        Me.ColBedNo.DataPropertyName = "BedNo"
        Me.ColBedNo.HeaderText = "Bed No"
        Me.ColBedNo.Name = "ColBedNo"
        Me.ColBedNo.ReadOnly = True
        '
        'ColBed
        '
        Me.ColBed.DataPropertyName = "BedName"
        Me.ColBed.HeaderText = "Bed"
        Me.ColBed.Name = "ColBed"
        Me.ColBed.ReadOnly = True
        '
        'ColWard
        '
        Me.ColWard.DataPropertyName = "RoomName"
        Me.ColWard.HeaderText = "Ward"
        Me.ColWard.Name = "ColWard"
        Me.ColWard.ReadOnly = True
        '
        'ColVisitDate
        '
        Me.ColVisitDate.DataPropertyName = "VisitDate"
        Me.ColVisitDate.HeaderText = "Visit Date"
        Me.ColVisitDate.Name = "ColVisitDate"
        Me.ColVisitDate.ReadOnly = True
        '
        'ColAdmissionDate
        '
        Me.ColAdmissionDate.DataPropertyName = "AdmissionDate"
        Me.ColAdmissionDate.HeaderText = "Admission Date"
        Me.ColAdmissionDate.Name = "ColAdmissionDate"
        Me.ColAdmissionDate.ReadOnly = True
        '
        'ColDaysOnWard
        '
        Me.ColDaysOnWard.DataPropertyName = "DaysOnWard"
        Me.ColDaysOnWard.HeaderText = "Days On Ward"
        Me.ColDaysOnWard.Name = "ColDaysOnWard"
        Me.ColDaysOnWard.ReadOnly = True
        '
        'ColAdmissionNotes
        '
        Me.ColAdmissionNotes.DataPropertyName = "AdmissionNotes"
        Me.ColAdmissionNotes.HeaderText = "Admission Notes"
        Me.ColAdmissionNotes.Name = "ColAdmissionNotes"
        Me.ColAdmissionNotes.ReadOnly = True
        '
        'frmReportOperations
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1034, 529)
        Me.Controls.Add(Me.fbClose)
        Me.Controls.Add(Me.tbcItemOperations)
        Me.Controls.Add(Me.grpSetParameters)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmReportOperations"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "GeneralOperations"
        Me.Text = "Report Operations"
        Me.grpSetParameters.ResumeLayout(False)
        Me.grpSetParameters.PerformLayout()
        Me.pnlPeriod.ResumeLayout(False)
        Me.tpgOPDItemsStatus.ResumeLayout(False)
        Me.tpgOPDItemsStatus.PerformLayout()
        CType(Me.dgvOPDItemsStatus, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgDoctorVisits.ResumeLayout(False)
        CType(Me.dgvDoctorVisits, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgServices.ResumeLayout(False)
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbcItemOperations.ResumeLayout(False)
        Me.tpgPatientRegistration.ResumeLayout(False)
        CType(Me.dgvPatientRegistration, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgVisitRegistration.ResumeLayout(False)
        CType(Me.dgvVisitRegistration, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgFileStatus.ResumeLayout(False)
        CType(Me.dgvFileStatus, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgFilesNotSeenByDoctor.ResumeLayout(False)
        CType(Me.dgvFilesNotSeenByDoctor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgFilesSeenByDoctor.ResumeLayout(False)
        CType(Me.dgvFilesSeenByDoctor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgOverStayedAdmissions.ResumeLayout(False)
        CType(Me.dgvUnclosedVisits, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblItemCategory As System.Windows.Forms.Label
    Friend WithEvents cboItemCategoryID As System.Windows.Forms.ComboBox
    Friend WithEvents fbnReportOperations As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents grpSetParameters As System.Windows.Forms.GroupBox
    Friend WithEvents fbnExport As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents pnlPeriod As System.Windows.Forms.Panel
    Friend WithEvents dtpEndDateTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDateTime As System.Windows.Forms.Label
    Friend WithEvents dtpStartDateTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEndDateTime As System.Windows.Forms.Label
    Friend WithEvents fbClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents tpgOPDItemsStatus As System.Windows.Forms.TabPage
    Friend WithEvents stbAmountWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents stbTotalAmount As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblExpenditureTotalAmount As System.Windows.Forms.Label
    Friend WithEvents lblExpenditureAmountWords As System.Windows.Forms.Label
    Friend WithEvents lblPayStatus As System.Windows.Forms.Label
    Friend WithEvents CboPayStatusID As System.Windows.Forms.ComboBox
    Friend WithEvents lblItemStatus As System.Windows.Forms.Label
    Friend WithEvents cboItemStatusID As System.Windows.Forms.ComboBox
    Friend WithEvents dgvOPDItemsStatus As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColOPDUnitCost As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColOPDTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColOPDBillMode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColOPDOfferedBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColOfferingMachine As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpgDoctorVisits As System.Windows.Forms.TabPage
    Friend WithEvents dgvDoctorVisits As System.Windows.Forms.DataGridView
    Friend WithEvents colSeenSpeciality As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColSeenDoctor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColSeenVisits As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBillMode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpgServices As System.Windows.Forms.TabPage
    Friend WithEvents dgvItems As System.Windows.Forms.DataGridView
    Friend WithEvents ColItemName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColNoOfPatients As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColDone As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColPending As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColProcessing As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tbcItemOperations As System.Windows.Forms.TabControl
    Friend WithEvents tpgPatientRegistration As System.Windows.Forms.TabPage
    Friend WithEvents dgvPatientRegistration As System.Windows.Forms.DataGridView
    Friend WithEvents colPatientLoginID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPatientFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPatientTotalPatients As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpgVisitRegistration As System.Windows.Forms.TabPage
    Friend WithEvents dgvVisitRegistration As System.Windows.Forms.DataGridView
    Friend WithEvents colVisitLoginID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colVisitFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colVisitTotalVisits As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpgFileStatus As System.Windows.Forms.TabPage
    Friend WithEvents dgvFileStatus As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpgFilesNotSeenByDoctor As System.Windows.Forms.TabPage
    Friend WithEvents dgvFilesNotSeenByDoctor As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColLastVisitDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColTakenDateTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColTakenBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColBillAccountName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpgFilesSeenByDoctor As System.Windows.Forms.TabPage
    Friend WithEvents dgvFilesSeenByDoctor As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColBillModeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColTotalBill As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColTotalPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColBillBalance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tpgOverStayedAdmissions As System.Windows.Forms.TabPage
    Friend WithEvents dgvUnclosedVisits As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColAdmissions As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColBedNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColBed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColWard As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColVisitDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColAdmissionDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColDaysOnWard As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColAdmissionNotes As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
