
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOPDStaffPaymentDetails : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOPDStaffPaymentDetails))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.cmsAlertListSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmsAlertListIncludeNone = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblAmountInWords = New System.Windows.Forms.Label()
        Me.stbAmountInWords = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblTotalAmount = New System.Windows.Forms.Label()
        Me.stbTotalAmount = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblPaymentVoucherNo = New System.Windows.Forms.Label()
        Me.clbItemCategory = New System.Windows.Forms.CheckedListBox()
        Me.stbPaymentVoucherNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblItemCategoryID = New System.Windows.Forms.Label()
        Me.lblPayModes = New System.Windows.Forms.Label()
        Me.pnlPeriod = New System.Windows.Forms.Panel()
        Me.cboStaffTitle = New System.Windows.Forms.ComboBox()
        Me.lblStaffTitle = New System.Windows.Forms.Label()
        Me.cboCompanyNo = New System.Windows.Forms.ComboBox()
        Me.lblBPCompanyNo = New System.Windows.Forms.Label()
        Me.cboAccountNo = New System.Windows.Forms.ComboBox()
        Me.btnFindVoucherPaymentNo = New System.Windows.Forms.Button()
        Me.cboItemStatus = New System.Windows.Forms.ComboBox()
        Me.lblBPBillAccountNo = New System.Windows.Forms.Label()
        Me.lblItemStatus = New System.Windows.Forms.Label()
        Me.cboBillModesID = New System.Windows.Forms.ComboBox()
        Me.fbnLoad = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.cboStaffNo = New System.Windows.Forms.ComboBox()
        Me.lblStaff = New System.Windows.Forms.Label()
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker()
        Me.lblStartDate = New System.Windows.Forms.Label()
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker()
        Me.lblEndDate = New System.Windows.Forms.Label()
        Me.cmsAlertList = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmsAlertListCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmsAlertListIncludeAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmsAlertListFillAmount = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmsAlertListUndoFillAmount = New System.Windows.Forms.ToolStripMenuItem()
        Me.grpSetParameters = New System.Windows.Forms.GroupBox()
        Me.stbStaffTitle = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbGender = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblRecordsNo = New System.Windows.Forms.Label()
        Me.lblStaffTitleID = New System.Windows.Forms.Label()
        Me.stbFirstName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbSpeciality = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbLastName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.lblFristName = New System.Windows.Forms.Label()
        Me.lblGenderID = New System.Windows.Forms.Label()
        Me.lblSpeciality = New System.Windows.Forms.Label()
        Me.chkPrintVoucherOnSaving = New System.Windows.Forms.CheckBox()
        Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.tbcVoucherDetails = New System.Windows.Forms.TabControl()
        Me.tpgOPDVoucherDetails = New System.Windows.Forms.TabPage()
        Me.dgvOPDVoucherDetails = New System.Windows.Forms.DataGridView()
        Me.colOPDVoucherInclude = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colOPDVoucherVisitNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherPatientNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherVisitDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherVisitStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherVisitCategory = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherQuantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherUnitPrice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherTotalFee = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherItemCategory = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherItemCategoryID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherItemCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherItemName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherItemStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherPayStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherToBillCustomerNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOPDVoucherToBillCustomerName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chkApplyPercentage = New System.Windows.Forms.CheckBox()
        Me.nbxApplyPercentage = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.pnlPeriod.SuspendLayout()
        Me.cmsAlertList.SuspendLayout()
        Me.grpSetParameters.SuspendLayout()
        Me.tbcVoucherDetails.SuspendLayout()
        Me.tpgOPDVoucherDetails.SuspendLayout()
        CType(Me.dgvOPDVoucherDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmsAlertListSelectAll
        '
        Me.cmsAlertListSelectAll.Name = "cmsAlertListSelectAll"
        Me.cmsAlertListSelectAll.Size = New System.Drawing.Size(168, 22)
        Me.cmsAlertListSelectAll.Text = "Select All"
        '
        'cmsAlertListIncludeNone
        '
        Me.cmsAlertListIncludeNone.Name = "cmsAlertListIncludeNone"
        Me.cmsAlertListIncludeNone.Size = New System.Drawing.Size(168, 22)
        Me.cmsAlertListIncludeNone.Text = "Include None"
        '
        'lblAmountInWords
        '
        Me.lblAmountInWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblAmountInWords.Location = New System.Drawing.Point(447, 516)
        Me.lblAmountInWords.Name = "lblAmountInWords"
        Me.lblAmountInWords.Size = New System.Drawing.Size(107, 21)
        Me.lblAmountInWords.TabIndex = 135
        Me.lblAmountInWords.Text = "Amount in Words"
        '
        'stbAmountInWords
        '
        Me.stbAmountInWords.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbAmountInWords.BackColor = System.Drawing.SystemColors.Info
        Me.stbAmountInWords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAmountInWords.CapitalizeFirstLetter = False
        Me.stbAmountInWords.EntryErrorMSG = ""
        Me.stbAmountInWords.Location = New System.Drawing.Point(558, 508)
        Me.stbAmountInWords.MaxLength = 100
        Me.stbAmountInWords.Multiline = True
        Me.stbAmountInWords.Name = "stbAmountInWords"
        Me.stbAmountInWords.ReadOnly = True
        Me.stbAmountInWords.RegularExpression = ""
        Me.stbAmountInWords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbAmountInWords.Size = New System.Drawing.Size(377, 39)
        Me.stbAmountInWords.TabIndex = 136
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotalAmount.Location = New System.Drawing.Point(447, 486)
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.Size = New System.Drawing.Size(84, 20)
        Me.lblTotalAmount.TabIndex = 133
        Me.lblTotalAmount.Text = "Total Amount"
        '
        'stbTotalAmount
        '
        Me.stbTotalAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbTotalAmount.BackColor = System.Drawing.SystemColors.Info
        Me.stbTotalAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalAmount.CapitalizeFirstLetter = False
        Me.stbTotalAmount.Enabled = False
        Me.stbTotalAmount.EntryErrorMSG = ""
        Me.stbTotalAmount.Location = New System.Drawing.Point(557, 486)
        Me.stbTotalAmount.MaxLength = 20
        Me.stbTotalAmount.Name = "stbTotalAmount"
        Me.stbTotalAmount.RegularExpression = ""
        Me.stbTotalAmount.Size = New System.Drawing.Size(184, 20)
        Me.stbTotalAmount.TabIndex = 134
        Me.stbTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPaymentVoucherNo
        '
        Me.lblPaymentVoucherNo.Location = New System.Drawing.Point(343, 125)
        Me.lblPaymentVoucherNo.Name = "lblPaymentVoucherNo"
        Me.lblPaymentVoucherNo.Size = New System.Drawing.Size(113, 20)
        Me.lblPaymentVoucherNo.TabIndex = 130
        Me.lblPaymentVoucherNo.Text = "PaymentVoucher No"
        '
        'clbItemCategory
        '
        Me.clbItemCategory.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.clbItemCategory.FormattingEnabled = True
        Me.clbItemCategory.Location = New System.Drawing.Point(142, 74)
        Me.clbItemCategory.Name = "clbItemCategory"
        Me.clbItemCategory.Size = New System.Drawing.Size(184, 90)
        Me.clbItemCategory.TabIndex = 132
        '
        'stbPaymentVoucherNo
        '
        Me.stbPaymentVoucherNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPaymentVoucherNo.CapitalizeFirstLetter = False
        Me.stbPaymentVoucherNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.stbPaymentVoucherNo.EntryErrorMSG = ""
        Me.stbPaymentVoucherNo.Location = New System.Drawing.Point(491, 124)
        Me.stbPaymentVoucherNo.MaxLength = 20
        Me.stbPaymentVoucherNo.Name = "stbPaymentVoucherNo"
        Me.stbPaymentVoucherNo.RegularExpression = ""
        Me.stbPaymentVoucherNo.Size = New System.Drawing.Size(150, 20)
        Me.stbPaymentVoucherNo.TabIndex = 131
        '
        'lblItemCategoryID
        '
        Me.lblItemCategoryID.Location = New System.Drawing.Point(19, 76)
        Me.lblItemCategoryID.Name = "lblItemCategoryID"
        Me.lblItemCategoryID.Size = New System.Drawing.Size(108, 20)
        Me.lblItemCategoryID.TabIndex = 8
        Me.lblItemCategoryID.Text = "Item Category"
        '
        'lblPayModes
        '
        Me.lblPayModes.Location = New System.Drawing.Point(343, 56)
        Me.lblPayModes.Name = "lblPayModes"
        Me.lblPayModes.Size = New System.Drawing.Size(142, 20)
        Me.lblPayModes.TabIndex = 10
        Me.lblPayModes.Text = "Bill Mode"
        '
        'pnlPeriod
        '
        Me.pnlPeriod.Controls.Add(Me.cboStaffTitle)
        Me.pnlPeriod.Controls.Add(Me.lblStaffTitle)
        Me.pnlPeriod.Controls.Add(Me.cboCompanyNo)
        Me.pnlPeriod.Controls.Add(Me.lblBPCompanyNo)
        Me.pnlPeriod.Controls.Add(Me.cboAccountNo)
        Me.pnlPeriod.Controls.Add(Me.btnFindVoucherPaymentNo)
        Me.pnlPeriod.Controls.Add(Me.cboItemStatus)
        Me.pnlPeriod.Controls.Add(Me.lblBPBillAccountNo)
        Me.pnlPeriod.Controls.Add(Me.lblItemStatus)
        Me.pnlPeriod.Controls.Add(Me.lblPaymentVoucherNo)
        Me.pnlPeriod.Controls.Add(Me.clbItemCategory)
        Me.pnlPeriod.Controls.Add(Me.stbPaymentVoucherNo)
        Me.pnlPeriod.Controls.Add(Me.lblItemCategoryID)
        Me.pnlPeriod.Controls.Add(Me.cboBillModesID)
        Me.pnlPeriod.Controls.Add(Me.fbnLoad)
        Me.pnlPeriod.Controls.Add(Me.lblPayModes)
        Me.pnlPeriod.Controls.Add(Me.cboStaffNo)
        Me.pnlPeriod.Controls.Add(Me.lblStaff)
        Me.pnlPeriod.Controls.Add(Me.dtpEndDate)
        Me.pnlPeriod.Controls.Add(Me.lblStartDate)
        Me.pnlPeriod.Controls.Add(Me.dtpStartDate)
        Me.pnlPeriod.Controls.Add(Me.lblEndDate)
        Me.pnlPeriod.Location = New System.Drawing.Point(6, 12)
        Me.pnlPeriod.Name = "pnlPeriod"
        Me.pnlPeriod.Size = New System.Drawing.Size(733, 165)
        Me.pnlPeriod.TabIndex = 4
        '
        'cboStaffTitle
        '
        Me.cboStaffTitle.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboStaffTitle.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ebnSaveUpdate.SetDataMember(Me.cboStaffTitle, "StaffFullName")
        Me.cboStaffTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStaffTitle.DropDownWidth = 220
        Me.cboStaffTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboStaffTitle.FormattingEnabled = True
        Me.cboStaffTitle.Location = New System.Drawing.Point(142, 28)
        Me.cboStaffTitle.Name = "cboStaffTitle"
        Me.cboStaffTitle.Size = New System.Drawing.Size(184, 21)
        Me.cboStaffTitle.Sorted = True
        Me.cboStaffTitle.TabIndex = 147
        '
        'lblStaffTitle
        '
        Me.lblStaffTitle.Location = New System.Drawing.Point(19, 30)
        Me.lblStaffTitle.Name = "lblStaffTitle"
        Me.lblStaffTitle.Size = New System.Drawing.Size(108, 20)
        Me.lblStaffTitle.TabIndex = 146
        Me.lblStaffTitle.Text = "Staff Title"
        '
        'cboCompanyNo
        '
        Me.cboCompanyNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboCompanyNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboCompanyNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboCompanyNo.DropDownWidth = 256
        Me.cboCompanyNo.Enabled = False
        Me.cboCompanyNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboCompanyNo.FormattingEnabled = True
        Me.cboCompanyNo.ItemHeight = 13
        Me.cboCompanyNo.Location = New System.Drawing.Point(491, 99)
        Me.cboCompanyNo.Name = "cboCompanyNo"
        Me.cboCompanyNo.Size = New System.Drawing.Size(221, 21)
        Me.cboCompanyNo.TabIndex = 145
        '
        'lblBPCompanyNo
        '
        Me.lblBPCompanyNo.Location = New System.Drawing.Point(343, 101)
        Me.lblBPCompanyNo.Name = "lblBPCompanyNo"
        Me.lblBPCompanyNo.Size = New System.Drawing.Size(142, 18)
        Me.lblBPCompanyNo.TabIndex = 144
        Me.lblBPCompanyNo.Text = "To-Bill Company Number"
        '
        'cboAccountNo
        '
        Me.cboAccountNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboAccountNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAccountNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboAccountNo.DropDownWidth = 276
        Me.cboAccountNo.Enabled = False
        Me.cboAccountNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboAccountNo.FormattingEnabled = True
        Me.cboAccountNo.ItemHeight = 13
        Me.cboAccountNo.Location = New System.Drawing.Point(491, 76)
        Me.cboAccountNo.Name = "cboAccountNo"
        Me.cboAccountNo.Size = New System.Drawing.Size(221, 21)
        Me.cboAccountNo.TabIndex = 131
        '
        'btnFindVoucherPaymentNo
        '
        Me.btnFindVoucherPaymentNo.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnFindVoucherPaymentNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFindVoucherPaymentNo.Image = CType(resources.GetObject("btnFindVoucherPaymentNo.Image"), System.Drawing.Image)
        Me.btnFindVoucherPaymentNo.Location = New System.Drawing.Point(458, 123)
        Me.btnFindVoucherPaymentNo.Name = "btnFindVoucherPaymentNo"
        Me.btnFindVoucherPaymentNo.Size = New System.Drawing.Size(27, 21)
        Me.btnFindVoucherPaymentNo.TabIndex = 138
        '
        'cboItemStatus
        '
        Me.cboItemStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboItemStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboItemStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboItemStatus.Location = New System.Drawing.Point(491, 30)
        Me.cboItemStatus.Name = "cboItemStatus"
        Me.cboItemStatus.Size = New System.Drawing.Size(221, 21)
        Me.cboItemStatus.TabIndex = 134
        '
        'lblBPBillAccountNo
        '
        Me.lblBPBillAccountNo.Location = New System.Drawing.Point(343, 79)
        Me.lblBPBillAccountNo.Name = "lblBPBillAccountNo"
        Me.lblBPBillAccountNo.Size = New System.Drawing.Size(142, 18)
        Me.lblBPBillAccountNo.TabIndex = 130
        Me.lblBPBillAccountNo.Text = "To-Bill Account Number"
        '
        'lblItemStatus
        '
        Me.lblItemStatus.Location = New System.Drawing.Point(343, 32)
        Me.lblItemStatus.Name = "lblItemStatus"
        Me.lblItemStatus.Size = New System.Drawing.Size(142, 20)
        Me.lblItemStatus.TabIndex = 133
        Me.lblItemStatus.Text = "Item Status"
        '
        'cboBillModesID
        '
        Me.cboBillModesID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboBillModesID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ebnSaveUpdate.SetDataMember(Me.cboBillModesID, "BillModes")
        Me.cboBillModesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboBillModesID.Location = New System.Drawing.Point(491, 53)
        Me.cboBillModesID.Name = "cboBillModesID"
        Me.cboBillModesID.Size = New System.Drawing.Size(221, 21)
        Me.cboBillModesID.TabIndex = 11
        '
        'fbnLoad
        '
        Me.fbnLoad.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnLoad.Location = New System.Drawing.Point(647, 124)
        Me.fbnLoad.Name = "fbnLoad"
        Me.fbnLoad.Size = New System.Drawing.Size(65, 22)
        Me.fbnLoad.TabIndex = 5
        Me.fbnLoad.Text = "Load..."
        '
        'cboStaffNo
        '
        Me.cboStaffNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboStaffNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ebnSaveUpdate.SetDataMember(Me.cboStaffNo, "StaffFullName")
        Me.cboStaffNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStaffNo.DropDownWidth = 220
        Me.cboStaffNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboStaffNo.FormattingEnabled = True
        Me.cboStaffNo.Location = New System.Drawing.Point(142, 51)
        Me.cboStaffNo.Name = "cboStaffNo"
        Me.cboStaffNo.Size = New System.Drawing.Size(184, 21)
        Me.cboStaffNo.Sorted = True
        Me.cboStaffNo.TabIndex = 7
        '
        'lblStaff
        '
        Me.lblStaff.Location = New System.Drawing.Point(19, 52)
        Me.lblStaff.Name = "lblStaff"
        Me.lblStaff.Size = New System.Drawing.Size(108, 20)
        Me.lblStaff.TabIndex = 6
        Me.lblStaff.Text = "Primary Dr. (Staff)"
        '
        'dtpEndDate
        '
        Me.dtpEndDate.CustomFormat = "dd MMM yyyy hh:mm tt"
        Me.ebnSaveUpdate.SetDataMember(Me.dtpEndDate, "EndDateTime")
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpEndDate.Location = New System.Drawing.Point(491, 8)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(221, 20)
        Me.dtpEndDate.TabIndex = 3
        '
        'lblStartDate
        '
        Me.lblStartDate.Location = New System.Drawing.Point(19, 6)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(108, 20)
        Me.lblStartDate.TabIndex = 0
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStartDate
        '
        Me.dtpStartDate.CustomFormat = "dd MMM yyyy hh:mm tt"
        Me.ebnSaveUpdate.SetDataMember(Me.dtpStartDate, "StartDateTime")
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpStartDate.Location = New System.Drawing.Point(142, 6)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(184, 20)
        Me.dtpStartDate.TabIndex = 1
        '
        'lblEndDate
        '
        Me.lblEndDate.Location = New System.Drawing.Point(343, 7)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(142, 20)
        Me.lblEndDate.TabIndex = 2
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmsAlertList
        '
        Me.cmsAlertList.BackColor = System.Drawing.Color.GhostWhite
        Me.cmsAlertList.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmsAlertListCopy, Me.cmsAlertListSelectAll, Me.cmsAlertListIncludeAll, Me.cmsAlertListIncludeNone, Me.cmsAlertListFillAmount, Me.cmsAlertListUndoFillAmount})
        Me.cmsAlertList.Name = "cmsSearch"
        Me.cmsAlertList.Size = New System.Drawing.Size(169, 136)
        '
        'cmsAlertListCopy
        '
        Me.cmsAlertListCopy.Image = CType(resources.GetObject("cmsAlertListCopy.Image"), System.Drawing.Image)
        Me.cmsAlertListCopy.Name = "cmsAlertListCopy"
        Me.cmsAlertListCopy.Size = New System.Drawing.Size(168, 22)
        Me.cmsAlertListCopy.Text = "Copy"
        Me.cmsAlertListCopy.ToolTipText = "To copy with column headings, use Ctrl+C key combination"
        '
        'cmsAlertListIncludeAll
        '
        Me.cmsAlertListIncludeAll.Name = "cmsAlertListIncludeAll"
        Me.cmsAlertListIncludeAll.Size = New System.Drawing.Size(168, 22)
        Me.cmsAlertListIncludeAll.Text = "Include All"
        '
        'cmsAlertListFillAmount
        '
        Me.cmsAlertListFillAmount.Name = "cmsAlertListFillAmount"
        Me.cmsAlertListFillAmount.Size = New System.Drawing.Size(168, 22)
        Me.cmsAlertListFillAmount.Text = "Fill Amount"
        '
        'cmsAlertListUndoFillAmount
        '
        Me.cmsAlertListUndoFillAmount.Name = "cmsAlertListUndoFillAmount"
        Me.cmsAlertListUndoFillAmount.Size = New System.Drawing.Size(168, 22)
        Me.cmsAlertListUndoFillAmount.Text = "Undo Fill Amount"
        '
        'grpSetParameters
        '
        Me.grpSetParameters.Controls.Add(Me.pnlPeriod)
        Me.grpSetParameters.Controls.Add(Me.stbStaffTitle)
        Me.grpSetParameters.Controls.Add(Me.stbGender)
        Me.grpSetParameters.Controls.Add(Me.lblRecordsNo)
        Me.grpSetParameters.Controls.Add(Me.lblStaffTitleID)
        Me.grpSetParameters.Controls.Add(Me.stbFirstName)
        Me.grpSetParameters.Controls.Add(Me.stbSpeciality)
        Me.grpSetParameters.Controls.Add(Me.stbLastName)
        Me.grpSetParameters.Controls.Add(Me.lblLastName)
        Me.grpSetParameters.Controls.Add(Me.lblFristName)
        Me.grpSetParameters.Controls.Add(Me.lblGenderID)
        Me.grpSetParameters.Controls.Add(Me.lblSpeciality)
        Me.grpSetParameters.Location = New System.Drawing.Point(12, 6)
        Me.grpSetParameters.Name = "grpSetParameters"
        Me.grpSetParameters.Size = New System.Drawing.Size(1038, 185)
        Me.grpSetParameters.TabIndex = 130
        Me.grpSetParameters.TabStop = False
        Me.grpSetParameters.Text = "Visit Period"
        '
        'stbStaffTitle
        '
        Me.stbStaffTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbStaffTitle.CapitalizeFirstLetter = True
        Me.stbStaffTitle.EntryErrorMSG = ""
        Me.stbStaffTitle.Location = New System.Drawing.Point(823, 91)
        Me.stbStaffTitle.MaxLength = 20
        Me.stbStaffTitle.Name = "stbStaffTitle"
        Me.stbStaffTitle.ReadOnly = True
        Me.stbStaffTitle.RegularExpression = ""
        Me.stbStaffTitle.Size = New System.Drawing.Size(191, 20)
        Me.stbStaffTitle.TabIndex = 129
        '
        'stbGender
        '
        Me.stbGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbGender.CapitalizeFirstLetter = True
        Me.stbGender.EntryErrorMSG = ""
        Me.stbGender.Location = New System.Drawing.Point(823, 69)
        Me.stbGender.MaxLength = 20
        Me.stbGender.Name = "stbGender"
        Me.stbGender.ReadOnly = True
        Me.stbGender.RegularExpression = ""
        Me.stbGender.Size = New System.Drawing.Size(191, 20)
        Me.stbGender.TabIndex = 128
        '
        'lblRecordsNo
        '
        Me.lblRecordsNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblRecordsNo.ForeColor = System.Drawing.Color.Blue
        Me.lblRecordsNo.Location = New System.Drawing.Point(778, 11)
        Me.lblRecordsNo.Name = "lblRecordsNo"
        Me.lblRecordsNo.Size = New System.Drawing.Size(240, 13)
        Me.lblRecordsNo.TabIndex = 3
        Me.lblRecordsNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblStaffTitleID
        '
        Me.lblStaffTitleID.Location = New System.Drawing.Point(752, 93)
        Me.lblStaffTitleID.Name = "lblStaffTitleID"
        Me.lblStaffTitleID.Size = New System.Drawing.Size(65, 21)
        Me.lblStaffTitleID.TabIndex = 124
        Me.lblStaffTitleID.Text = "Staff Title"
        '
        'stbFirstName
        '
        Me.stbFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbFirstName.CapitalizeFirstLetter = True
        Me.stbFirstName.EntryErrorMSG = ""
        Me.stbFirstName.Location = New System.Drawing.Point(823, 27)
        Me.stbFirstName.MaxLength = 20
        Me.stbFirstName.Name = "stbFirstName"
        Me.stbFirstName.ReadOnly = True
        Me.stbFirstName.RegularExpression = ""
        Me.stbFirstName.Size = New System.Drawing.Size(191, 20)
        Me.stbFirstName.TabIndex = 119
        '
        'stbSpeciality
        '
        Me.stbSpeciality.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbSpeciality.CapitalizeFirstLetter = False
        Me.stbSpeciality.EntryErrorMSG = ""
        Me.stbSpeciality.Location = New System.Drawing.Point(823, 112)
        Me.stbSpeciality.MaxLength = 20
        Me.stbSpeciality.Name = "stbSpeciality"
        Me.stbSpeciality.ReadOnly = True
        Me.stbSpeciality.RegularExpression = ""
        Me.stbSpeciality.Size = New System.Drawing.Size(191, 20)
        Me.stbSpeciality.TabIndex = 127
        '
        'stbLastName
        '
        Me.stbLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbLastName.CapitalizeFirstLetter = True
        Me.stbLastName.EntryErrorMSG = ""
        Me.stbLastName.Location = New System.Drawing.Point(823, 48)
        Me.stbLastName.MaxLength = 20
        Me.stbLastName.Name = "stbLastName"
        Me.stbLastName.ReadOnly = True
        Me.stbLastName.RegularExpression = ""
        Me.stbLastName.Size = New System.Drawing.Size(191, 20)
        Me.stbLastName.TabIndex = 121
        '
        'lblLastName
        '
        Me.lblLastName.Location = New System.Drawing.Point(752, 48)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(65, 21)
        Me.lblLastName.TabIndex = 120
        Me.lblLastName.Text = "Last Name"
        '
        'lblFristName
        '
        Me.lblFristName.Location = New System.Drawing.Point(752, 26)
        Me.lblFristName.Name = "lblFristName"
        Me.lblFristName.Size = New System.Drawing.Size(65, 21)
        Me.lblFristName.TabIndex = 118
        Me.lblFristName.Text = "First Name"
        '
        'lblGenderID
        '
        Me.lblGenderID.Location = New System.Drawing.Point(752, 70)
        Me.lblGenderID.Name = "lblGenderID"
        Me.lblGenderID.Size = New System.Drawing.Size(65, 21)
        Me.lblGenderID.TabIndex = 122
        Me.lblGenderID.Text = "Gender"
        '
        'lblSpeciality
        '
        Me.lblSpeciality.Location = New System.Drawing.Point(752, 112)
        Me.lblSpeciality.Name = "lblSpeciality"
        Me.lblSpeciality.Size = New System.Drawing.Size(65, 21)
        Me.lblSpeciality.TabIndex = 126
        Me.lblSpeciality.Text = "Speciality"
        '
        'chkPrintVoucherOnSaving
        '
        Me.chkPrintVoucherOnSaving.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkPrintVoucherOnSaving.AutoSize = True
        Me.chkPrintVoucherOnSaving.Checked = True
        Me.chkPrintVoucherOnSaving.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPrintVoucherOnSaving.Location = New System.Drawing.Point(16, 485)
        Me.chkPrintVoucherOnSaving.Name = "chkPrintVoucherOnSaving"
        Me.chkPrintVoucherOnSaving.Size = New System.Drawing.Size(146, 17)
        Me.chkPrintVoucherOnSaving.TabIndex = 131
        Me.chkPrintVoucherOnSaving.Text = " Print Voucher On Saving"
        '
        'fbnSearch
        '
        Me.fbnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnSearch.Location = New System.Drawing.Point(12, 504)
        Me.fbnSearch.Name = "fbnSearch"
        Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
        Me.fbnSearch.TabIndex = 126
        Me.fbnSearch.Text = "S&earch"
        Me.fbnSearch.UseVisualStyleBackColor = True
        Me.fbnSearch.Visible = False
        '
        'fbnDelete
        '
        Me.fbnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(974, 503)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 127
        Me.fbnDelete.Tag = "OPDStaffPaymentDetails"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = False
        Me.fbnDelete.Visible = False
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(12, 530)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 128
        Me.ebnSaveUpdate.Tag = "OPDStaffPaymentDetails"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = False
        '
        'fbnClose
        '
        Me.fbnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(974, 529)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 129
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'tbcVoucherDetails
        '
        Me.tbcVoucherDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbcVoucherDetails.Controls.Add(Me.tpgOPDVoucherDetails)
        Me.tbcVoucherDetails.HotTrack = True
        Me.tbcVoucherDetails.Location = New System.Drawing.Point(12, 197)
        Me.tbcVoucherDetails.Name = "tbcVoucherDetails"
        Me.tbcVoucherDetails.SelectedIndex = 0
        Me.tbcVoucherDetails.Size = New System.Drawing.Size(1038, 282)
        Me.tbcVoucherDetails.TabIndex = 137
        '
        'tpgOPDVoucherDetails
        '
        Me.tpgOPDVoucherDetails.Controls.Add(Me.dgvOPDVoucherDetails)
        Me.tpgOPDVoucherDetails.Location = New System.Drawing.Point(4, 22)
        Me.tpgOPDVoucherDetails.Name = "tpgOPDVoucherDetails"
        Me.tpgOPDVoucherDetails.Size = New System.Drawing.Size(1030, 256)
        Me.tpgOPDVoucherDetails.TabIndex = 7
        Me.tpgOPDVoucherDetails.Tag = "IPD Voucher Details"
        Me.tpgOPDVoucherDetails.Text = "OPD Voucher Details"
        Me.tpgOPDVoucherDetails.UseVisualStyleBackColor = True
        '
        'dgvOPDVoucherDetails
        '
        Me.dgvOPDVoucherDetails.AllowUserToAddRows = False
        Me.dgvOPDVoucherDetails.AllowUserToDeleteRows = False
        Me.dgvOPDVoucherDetails.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvOPDVoucherDetails.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvOPDVoucherDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvOPDVoucherDetails.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvOPDVoucherDetails.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvOPDVoucherDetails.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvOPDVoucherDetails.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOPDVoucherDetails.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvOPDVoucherDetails.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colOPDVoucherInclude, Me.colOPDVoucherVisitNo, Me.colOPDVoucherPatientNo, Me.colOPDVoucherFullName, Me.colOPDVoucherVisitDate, Me.colOPDVoucherVisitStatus, Me.colOPDVoucherVisitCategory, Me.colOPDVoucherQuantity, Me.colOPDVoucherUnitPrice, Me.colOPDVoucherTotalFee, Me.colOPDVoucherAmount, Me.colOPDVoucherItemCategory, Me.colOPDVoucherItemCategoryID, Me.colOPDVoucherItemCode, Me.colOPDVoucherItemName, Me.colOPDVoucherItemStatus, Me.colOPDVoucherPayStatus, Me.colOPDVoucherToBillCustomerNo, Me.colOPDVoucherToBillCustomerName})
        Me.dgvOPDVoucherDetails.ContextMenuStrip = Me.cmsAlertList
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvOPDVoucherDetails.DefaultCellStyle = DataGridViewCellStyle20
        Me.dgvOPDVoucherDetails.EnableHeadersVisualStyles = False
        Me.dgvOPDVoucherDetails.GridColor = System.Drawing.Color.Khaki
        Me.dgvOPDVoucherDetails.Location = New System.Drawing.Point(6, 4)
        Me.dgvOPDVoucherDetails.Name = "dgvOPDVoucherDetails"
        Me.dgvOPDVoucherDetails.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle21.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle21.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOPDVoucherDetails.RowHeadersDefaultCellStyle = DataGridViewCellStyle21
        Me.dgvOPDVoucherDetails.RowHeadersVisible = False
        Me.dgvOPDVoucherDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvOPDVoucherDetails.Size = New System.Drawing.Size(1018, 249)
        Me.dgvOPDVoucherDetails.TabIndex = 109
        Me.dgvOPDVoucherDetails.Text = "DataGridView1"
        '
        'colOPDVoucherInclude
        '
        Me.colOPDVoucherInclude.HeaderText = "Include"
        Me.colOPDVoucherInclude.Name = "colOPDVoucherInclude"
        Me.colOPDVoucherInclude.Width = 50
        '
        'colOPDVoucherVisitNo
        '
        Me.colOPDVoucherVisitNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.colOPDVoucherVisitNo.DataPropertyName = "VisitNo"
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherVisitNo.DefaultCellStyle = DataGridViewCellStyle3
        Me.colOPDVoucherVisitNo.HeaderText = "Visit No"
        Me.colOPDVoucherVisitNo.Name = "colOPDVoucherVisitNo"
        Me.colOPDVoucherVisitNo.ReadOnly = True
        '
        'colOPDVoucherPatientNo
        '
        Me.colOPDVoucherPatientNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.colOPDVoucherPatientNo.DataPropertyName = "PatientNo"
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherPatientNo.DefaultCellStyle = DataGridViewCellStyle4
        Me.colOPDVoucherPatientNo.HeaderText = "Patient No"
        Me.colOPDVoucherPatientNo.Name = "colOPDVoucherPatientNo"
        Me.colOPDVoucherPatientNo.ReadOnly = True
        '
        'colOPDVoucherFullName
        '
        Me.colOPDVoucherFullName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.colOPDVoucherFullName.DataPropertyName = "FullName"
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherFullName.DefaultCellStyle = DataGridViewCellStyle5
        Me.colOPDVoucherFullName.HeaderText = "Full Name"
        Me.colOPDVoucherFullName.Name = "colOPDVoucherFullName"
        Me.colOPDVoucherFullName.ReadOnly = True
        Me.colOPDVoucherFullName.Width = 150
        '
        'colOPDVoucherVisitDate
        '
        Me.colOPDVoucherVisitDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.colOPDVoucherVisitDate.DataPropertyName = "VisitDate"
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherVisitDate.DefaultCellStyle = DataGridViewCellStyle6
        Me.colOPDVoucherVisitDate.HeaderText = "Visit Date"
        Me.colOPDVoucherVisitDate.Name = "colOPDVoucherVisitDate"
        Me.colOPDVoucherVisitDate.ReadOnly = True
        '
        'colOPDVoucherVisitStatus
        '
        Me.colOPDVoucherVisitStatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.colOPDVoucherVisitStatus.DataPropertyName = "colOPDVoucherVisitStatus"
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherVisitStatus.DefaultCellStyle = DataGridViewCellStyle7
        Me.colOPDVoucherVisitStatus.HeaderText = "Visit Status"
        Me.colOPDVoucherVisitStatus.Name = "colOPDVoucherVisitStatus"
        '
        'colOPDVoucherVisitCategory
        '
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherVisitCategory.DefaultCellStyle = DataGridViewCellStyle8
        Me.colOPDVoucherVisitCategory.HeaderText = "Visit Category"
        Me.colOPDVoucherVisitCategory.Name = "colOPDVoucherVisitCategory"
        Me.colOPDVoucherVisitCategory.ReadOnly = True
        '
        'colOPDVoucherQuantity
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherQuantity.DefaultCellStyle = DataGridViewCellStyle9
        Me.colOPDVoucherQuantity.HeaderText = "Quanity"
        Me.colOPDVoucherQuantity.Name = "colOPDVoucherQuantity"
        Me.colOPDVoucherQuantity.ReadOnly = True
        Me.colOPDVoucherQuantity.Width = 50
        '
        'colOPDVoucherUnitPrice
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherUnitPrice.DefaultCellStyle = DataGridViewCellStyle10
        Me.colOPDVoucherUnitPrice.HeaderText = "Unit Price"
        Me.colOPDVoucherUnitPrice.Name = "colOPDVoucherUnitPrice"
        Me.colOPDVoucherUnitPrice.ReadOnly = True
        Me.colOPDVoucherUnitPrice.Width = 70
        '
        'colOPDVoucherTotalFee
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherTotalFee.DefaultCellStyle = DataGridViewCellStyle11
        Me.colOPDVoucherTotalFee.HeaderText = "Total Fee"
        Me.colOPDVoucherTotalFee.Name = "colOPDVoucherTotalFee"
        Me.colOPDVoucherTotalFee.ReadOnly = True
        Me.colOPDVoucherTotalFee.Width = 70
        '
        'colOPDVoucherAmount
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.colOPDVoucherAmount.DefaultCellStyle = DataGridViewCellStyle12
        Me.colOPDVoucherAmount.HeaderText = "Amount"
        Me.colOPDVoucherAmount.Name = "colOPDVoucherAmount"
        Me.colOPDVoucherAmount.Width = 80
        '
        'colOPDVoucherItemCategory
        '
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherItemCategory.DefaultCellStyle = DataGridViewCellStyle13
        Me.colOPDVoucherItemCategory.HeaderText = "Item Category"
        Me.colOPDVoucherItemCategory.Name = "colOPDVoucherItemCategory"
        Me.colOPDVoucherItemCategory.ReadOnly = True
        '
        'colOPDVoucherItemCategoryID
        '
        Me.colOPDVoucherItemCategoryID.HeaderText = "Item Category ID"
        Me.colOPDVoucherItemCategoryID.Name = "colOPDVoucherItemCategoryID"
        Me.colOPDVoucherItemCategoryID.Visible = False
        '
        'colOPDVoucherItemCode
        '
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherItemCode.DefaultCellStyle = DataGridViewCellStyle14
        Me.colOPDVoucherItemCode.HeaderText = "ItemCode"
        Me.colOPDVoucherItemCode.Name = "colOPDVoucherItemCode"
        Me.colOPDVoucherItemCode.ReadOnly = True
        '
        'colOPDVoucherItemName
        '
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherItemName.DefaultCellStyle = DataGridViewCellStyle15
        Me.colOPDVoucherItemName.HeaderText = "Item Name"
        Me.colOPDVoucherItemName.Name = "colOPDVoucherItemName"
        Me.colOPDVoucherItemName.ReadOnly = True
        '
        'colOPDVoucherItemStatus
        '
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherItemStatus.DefaultCellStyle = DataGridViewCellStyle16
        Me.colOPDVoucherItemStatus.HeaderText = "ItemStatus"
        Me.colOPDVoucherItemStatus.Name = "colOPDVoucherItemStatus"
        Me.colOPDVoucherItemStatus.ReadOnly = True
        '
        'colOPDVoucherPayStatus
        '
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherPayStatus.DefaultCellStyle = DataGridViewCellStyle17
        Me.colOPDVoucherPayStatus.HeaderText = "PayStatus"
        Me.colOPDVoucherPayStatus.Name = "colOPDVoucherPayStatus"
        Me.colOPDVoucherPayStatus.ReadOnly = True
        '
        'colOPDVoucherToBillCustomerNo
        '
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherToBillCustomerNo.DefaultCellStyle = DataGridViewCellStyle18
        Me.colOPDVoucherToBillCustomerNo.HeaderText = "To Bill Customer No"
        Me.colOPDVoucherToBillCustomerNo.Name = "colOPDVoucherToBillCustomerNo"
        Me.colOPDVoucherToBillCustomerNo.ReadOnly = True
        '
        'colOPDVoucherToBillCustomerName
        '
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Info
        Me.colOPDVoucherToBillCustomerName.DefaultCellStyle = DataGridViewCellStyle19
        Me.colOPDVoucherToBillCustomerName.HeaderText = "ToBill Customer Name"
        Me.colOPDVoucherToBillCustomerName.Name = "colOPDVoucherToBillCustomerName"
        Me.colOPDVoucherToBillCustomerName.ReadOnly = True
        Me.colOPDVoucherToBillCustomerName.Width = 200
        '
        'chkApplyPercentage
        '
        Me.chkApplyPercentage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkApplyPercentage.AutoSize = True
        Me.chkApplyPercentage.Location = New System.Drawing.Point(193, 487)
        Me.chkApplyPercentage.Name = "chkApplyPercentage"
        Me.chkApplyPercentage.Size = New System.Drawing.Size(110, 17)
        Me.chkApplyPercentage.TabIndex = 139
        Me.chkApplyPercentage.Text = "Apply Percentage"
        Me.chkApplyPercentage.UseVisualStyleBackColor = True
        '
        'nbxApplyPercentage
        '
        Me.nbxApplyPercentage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.nbxApplyPercentage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxApplyPercentage.ControlCaption = "Percentage Value"
        Me.nbxApplyPercentage.DataFormat = SyncSoft.Common.Win.Controls.DisplayFormat.Standard
        Me.nbxApplyPercentage.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxApplyPercentage.DecimalPlaces = -1
        Me.nbxApplyPercentage.DenyNegativeEntryValue = True
        Me.nbxApplyPercentage.DenyZeroEntryValue = True
        Me.nbxApplyPercentage.Enabled = False
        Me.nbxApplyPercentage.Location = New System.Drawing.Point(307, 485)
        Me.nbxApplyPercentage.MaxValue = 0.0R
        Me.nbxApplyPercentage.MinValue = 0.0R
        Me.nbxApplyPercentage.MustEnterNumeric = True
        Me.nbxApplyPercentage.Name = "nbxApplyPercentage"
        Me.nbxApplyPercentage.Size = New System.Drawing.Size(53, 20)
        Me.nbxApplyPercentage.TabIndex = 140
        Me.nbxApplyPercentage.Value = ""
        '
        'frmOPDStaffPaymentDetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1062, 557)
        Me.Controls.Add(Me.nbxApplyPercentage)
        Me.Controls.Add(Me.chkApplyPercentage)
        Me.Controls.Add(Me.tbcVoucherDetails)
        Me.Controls.Add(Me.lblAmountInWords)
        Me.Controls.Add(Me.stbAmountInWords)
        Me.Controls.Add(Me.lblTotalAmount)
        Me.Controls.Add(Me.stbTotalAmount)
        Me.Controls.Add(Me.grpSetParameters)
        Me.Controls.Add(Me.chkPrintVoucherOnSaving)
        Me.Controls.Add(Me.fbnSearch)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.fbnClose)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmOPDStaffPaymentDetails"
        Me.Text = "OPD Staff Payment"
        Me.pnlPeriod.ResumeLayout(False)
        Me.pnlPeriod.PerformLayout()
        Me.cmsAlertList.ResumeLayout(False)
        Me.grpSetParameters.ResumeLayout(False)
        Me.grpSetParameters.PerformLayout()
        Me.tbcVoucherDetails.ResumeLayout(False)
        Me.tpgOPDVoucherDetails.ResumeLayout(False)
        CType(Me.dgvOPDVoucherDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmsAlertListSelectAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmsAlertListIncludeNone As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblAmountInWords As System.Windows.Forms.Label
    Friend WithEvents stbAmountInWords As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblTotalAmount As System.Windows.Forms.Label
    Friend WithEvents stbTotalAmount As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblPaymentVoucherNo As System.Windows.Forms.Label
    Friend WithEvents clbItemCategory As System.Windows.Forms.CheckedListBox
    Friend WithEvents stbPaymentVoucherNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblItemCategoryID As System.Windows.Forms.Label
    Friend WithEvents lblPayModes As System.Windows.Forms.Label
    Friend WithEvents pnlPeriod As System.Windows.Forms.Panel
    Friend WithEvents cboBillModesID As System.Windows.Forms.ComboBox
    Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
    Friend WithEvents fbnLoad As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents cboStaffNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblStaff As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents cmsAlertList As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmsAlertListCopy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmsAlertListIncludeAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents grpSetParameters As System.Windows.Forms.GroupBox
    Friend WithEvents stbStaffTitle As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents stbGender As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblRecordsNo As System.Windows.Forms.Label
    Friend WithEvents lblStaffTitleID As System.Windows.Forms.Label
    Friend WithEvents stbFirstName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents stbSpeciality As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents stbLastName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents lblFristName As System.Windows.Forms.Label
    Friend WithEvents lblGenderID As System.Windows.Forms.Label
    Friend WithEvents lblSpeciality As System.Windows.Forms.Label
    Friend WithEvents chkPrintVoucherOnSaving As System.Windows.Forms.CheckBox
    Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents tbcVoucherDetails As System.Windows.Forms.TabControl
    Friend WithEvents tpgOPDVoucherDetails As System.Windows.Forms.TabPage
    Friend WithEvents dgvOPDVoucherDetails As System.Windows.Forms.DataGridView
    Friend WithEvents cboItemStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblItemStatus As System.Windows.Forms.Label
    Friend WithEvents btnFindVoucherPaymentNo As System.Windows.Forms.Button
    Friend WithEvents chkApplyPercentage As System.Windows.Forms.CheckBox
    Friend WithEvents cmsAlertListFillAmount As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmsAlertListUndoFillAmount As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents nbxApplyPercentage As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents cboAccountNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblBPBillAccountNo As System.Windows.Forms.Label
    Friend WithEvents cboCompanyNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblBPCompanyNo As System.Windows.Forms.Label
    Friend WithEvents colOPDVoucherInclude As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colOPDVoucherVisitNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherPatientNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherVisitDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherVisitStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherVisitCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherQuantity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherUnitPrice As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherTotalFee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherItemCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherItemCategoryID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherItemCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherItemName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherItemStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherPayStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherToBillCustomerNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOPDVoucherToBillCustomerName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboStaffTitle As System.Windows.Forms.ComboBox
    Friend WithEvents lblStaffTitle As System.Windows.Forms.Label


End Class