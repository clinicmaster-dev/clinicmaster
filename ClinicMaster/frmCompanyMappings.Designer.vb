
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCompanyMappings : Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCompanyMappings))
        Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.cboAgentNo = New System.Windows.Forms.ComboBox()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.lblAgentNo = New System.Windows.Forms.Label()
        Me.dgvCompanyMappings = New System.Windows.Forms.DataGridView()
        Me.colInclude = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colCompanyNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCompanyName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAgentCompanyNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAgentCompanyName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        CType(Me.dgvCompanyMappings, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'fbnSearch
        '
        Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnSearch.Location = New System.Drawing.Point(12, 402)
        Me.fbnSearch.Margin = New System.Windows.Forms.Padding(4)
        Me.fbnSearch.Name = "fbnSearch"
        Me.fbnSearch.Size = New System.Drawing.Size(103, 28)
        Me.fbnSearch.TabIndex = 0
        Me.fbnSearch.Text = "S&earch"
        Me.fbnSearch.UseVisualStyleBackColor = True
        Me.fbnSearch.Visible = False
        '
        'fbnDelete
        '
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(760, 400)
        Me.fbnDelete.Margin = New System.Windows.Forms.Padding(4)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(96, 30)
        Me.fbnDelete.TabIndex = 1
        Me.fbnDelete.Tag = "INTCompanyMappings"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = False
        Me.fbnDelete.Visible = False
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(12, 435)
        Me.ebnSaveUpdate.Margin = New System.Windows.Forms.Padding(4)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(103, 28)
        Me.ebnSaveUpdate.TabIndex = 2
        Me.ebnSaveUpdate.Tag = "INTCompanyMappings"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = False
        '
        'cboAgentNo
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboAgentNo, "CompanyNo")
        Me.cboAgentNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAgentNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboAgentNo.Location = New System.Drawing.Point(291, 12)
        Me.cboAgentNo.Margin = New System.Windows.Forms.Padding(4)
        Me.cboAgentNo.Name = "cboAgentNo"
        Me.cboAgentNo.Size = New System.Drawing.Size(225, 24)
        Me.cboAgentNo.TabIndex = 12
        '
        'fbnClose
        '
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(760, 433)
        Me.fbnClose.Margin = New System.Windows.Forms.Padding(4)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(96, 30)
        Me.fbnClose.TabIndex = 3
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'lblAgentNo
        '
        Me.lblAgentNo.Location = New System.Drawing.Point(16, 9)
        Me.lblAgentNo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblAgentNo.Name = "lblAgentNo"
        Me.lblAgentNo.Size = New System.Drawing.Size(267, 25)
        Me.lblAgentNo.TabIndex = 5
        Me.lblAgentNo.Text = "Agent No"
        '
        'dgvCompanyMappings
        '
        Me.dgvCompanyMappings.AllowUserToAddRows = False
        Me.dgvCompanyMappings.AllowUserToDeleteRows = False
        Me.dgvCompanyMappings.AllowUserToOrderColumns = True
        Me.dgvCompanyMappings.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvCompanyMappings.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCompanyMappings.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvCompanyMappings.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colInclude, Me.colCompanyNo, Me.colCompanyName, Me.colAgentCompanyNo, Me.colAgentCompanyName, Me.colSaved})
        Me.dgvCompanyMappings.EnableHeadersVisualStyles = False
        Me.dgvCompanyMappings.GridColor = System.Drawing.Color.Khaki
        Me.dgvCompanyMappings.Location = New System.Drawing.Point(13, 44)
        Me.dgvCompanyMappings.Margin = New System.Windows.Forms.Padding(4)
        Me.dgvCompanyMappings.Name = "dgvCompanyMappings"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCompanyMappings.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvCompanyMappings.Size = New System.Drawing.Size(843, 348)
        Me.dgvCompanyMappings.TabIndex = 14
        Me.dgvCompanyMappings.Text = "DataGridView1"
        '
        'colInclude
        '
        Me.colInclude.HeaderText = "Include"
        Me.colInclude.Name = "colInclude"
        Me.colInclude.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colInclude.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colInclude.Width = 60
        '
        'colCompanyNo
        '
        Me.colCompanyNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Info
        Me.colCompanyNo.DefaultCellStyle = DataGridViewCellStyle2
        Me.colCompanyNo.HeaderText = "Company No"
        Me.colCompanyNo.Name = "colCompanyNo"
        Me.colCompanyNo.ReadOnly = True
        Me.colCompanyNo.Width = 114
        '
        'colCompanyName
        '
        Me.colCompanyName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colCompanyName.HeaderText = "Company Name"
        Me.colCompanyName.Name = "colCompanyName"
        '
        'colAgentCompanyNo
        '
        Me.colAgentCompanyNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.colAgentCompanyNo.HeaderText = "Agent Company No"
        Me.colAgentCompanyNo.Name = "colAgentCompanyNo"
        Me.colAgentCompanyNo.ReadOnly = True
        Me.colAgentCompanyNo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colAgentCompanyNo.Width = 155
        '
        'colAgentCompanyName
        '
        Me.colAgentCompanyName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.colAgentCompanyName.HeaderText = "Agent Company Name"
        Me.colAgentCompanyName.Name = "colAgentCompanyName"
        Me.colAgentCompanyName.ReadOnly = True
        Me.colAgentCompanyName.Width = 174
        '
        'colSaved
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle3.NullValue = False
        Me.colSaved.DefaultCellStyle = DataGridViewCellStyle3
        Me.colSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colSaved.HeaderText = "Saved"
        Me.colSaved.Name = "colSaved"
        Me.colSaved.ReadOnly = True
        Me.colSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colSaved.Width = 50
        '
        'frmCompanyMappings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(856, 468)
        Me.Controls.Add(Me.dgvCompanyMappings)
        Me.Controls.Add(Me.cboAgentNo)
        Me.Controls.Add(Me.fbnSearch)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.lblAgentNo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.Name = "frmCompanyMappings"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "CompanyMappings"
        Me.Text = "Company Mappings"
        CType(Me.dgvCompanyMappings, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents lblAgentNo As System.Windows.Forms.Label
    Friend WithEvents cboAgentNo As System.Windows.Forms.ComboBox
    Friend WithEvents dgvCompanyMappings As System.Windows.Forms.DataGridView
    Friend WithEvents colInclude As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colCompanyNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCompanyName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAgentCompanyNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAgentCompanyName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSaved As System.Windows.Forms.DataGridViewCheckBoxColumn

End Class