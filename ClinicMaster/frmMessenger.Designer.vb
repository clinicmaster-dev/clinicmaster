
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMessenger : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal openReceive As Boolean)
        MyClass.New()
        Me.openReceive = openReceive
    End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMessenger))
        Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.stbMessage = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboStaffNo = New System.Windows.Forms.ComboBox()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.lblReceiverLoginID = New System.Windows.Forms.Label()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.tbcMessenger = New System.Windows.Forms.TabControl()
        Me.tpgCompose = New System.Windows.Forms.TabPage()
        Me.tpgInbox = New System.Windows.Forms.TabPage()
        Me.dgvInbox = New System.Windows.Forms.DataGridView()
        Me.colFromFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRecordDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMessageInfo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgOutbox = New System.Windows.Forms.TabPage()
        Me.dgvOutbox = New System.Windows.Forms.DataGridView()
        Me.ColSentTo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColSendRecordDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColSendMessageInfo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblUnreadMessageAlerts = New System.Windows.Forms.Label()
        Me.tmrAlerts = New System.Windows.Forms.Timer(Me.components)
        Me.tbcMessenger.SuspendLayout()
        Me.tpgCompose.SuspendLayout()
        Me.tpgInbox.SuspendLayout()
        CType(Me.dgvInbox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgOutbox.SuspendLayout()
        CType(Me.dgvOutbox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'fbnSearch
        '
        Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnSearch.Location = New System.Drawing.Point(8, 408)
        Me.fbnSearch.Name = "fbnSearch"
        Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
        Me.fbnSearch.TabIndex = 0
        Me.fbnSearch.Text = "S&earch"
        Me.fbnSearch.UseVisualStyleBackColor = True
        Me.fbnSearch.Visible = False
        '
        'fbnDelete
        '
        Me.fbnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(845, 407)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 1
        Me.fbnDelete.Tag = "Messenger"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = False
        Me.fbnDelete.Visible = False
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(8, 435)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 2
        Me.ebnSaveUpdate.Tag = "Messenger"
        Me.ebnSaveUpdate.Text = "&Send"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = False
        '
        'stbMessage
        '
        Me.stbMessage.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.stbMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbMessage.CapitalizeFirstLetter = True
        Me.ebnSaveUpdate.SetDataMember(Me.stbMessage, "Message")
        Me.stbMessage.EntryErrorMSG = ""
        Me.stbMessage.Location = New System.Drawing.Point(94, 38)
        Me.stbMessage.MaxLength = 500
        Me.stbMessage.Multiline = True
        Me.stbMessage.Name = "stbMessage"
        Me.stbMessage.RegularExpression = ""
        Me.stbMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbMessage.Size = New System.Drawing.Size(806, 298)
        Me.stbMessage.TabIndex = 8
        '
        'cboStaffNo
        '
        Me.cboStaffNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboStaffNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ebnSaveUpdate.SetDataMember(Me.cboStaffNo, "StaffFullName")
        Me.cboStaffNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStaffNo.DropDownWidth = 230
        Me.cboStaffNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboStaffNo.FormattingEnabled = True
        Me.cboStaffNo.Location = New System.Drawing.Point(97, 10)
        Me.cboStaffNo.Name = "cboStaffNo"
        Me.cboStaffNo.Size = New System.Drawing.Size(228, 21)
        Me.cboStaffNo.Sorted = True
        Me.cboStaffNo.TabIndex = 16
        '
        'fbnClose
        '
        Me.fbnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(845, 434)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 3
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'lblReceiverLoginID
        '
        Me.lblReceiverLoginID.Location = New System.Drawing.Point(14, 9)
        Me.lblReceiverLoginID.Name = "lblReceiverLoginID"
        Me.lblReceiverLoginID.Size = New System.Drawing.Size(59, 20)
        Me.lblReceiverLoginID.TabIndex = 7
        Me.lblReceiverLoginID.Text = "Receiver"
        '
        'lblMessage
        '
        Me.lblMessage.Location = New System.Drawing.Point(14, 38)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(74, 20)
        Me.lblMessage.TabIndex = 9
        Me.lblMessage.Text = "Message"
        '
        'tbcMessenger
        '
        Me.tbcMessenger.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbcMessenger.Controls.Add(Me.tpgCompose)
        Me.tbcMessenger.Controls.Add(Me.tpgInbox)
        Me.tbcMessenger.Controls.Add(Me.tpgOutbox)
        Me.tbcMessenger.Location = New System.Drawing.Point(6, 33)
        Me.tbcMessenger.Name = "tbcMessenger"
        Me.tbcMessenger.SelectedIndex = 0
        Me.tbcMessenger.Size = New System.Drawing.Size(911, 368)
        Me.tbcMessenger.TabIndex = 17
        '
        'tpgCompose
        '
        Me.tpgCompose.Controls.Add(Me.stbMessage)
        Me.tpgCompose.Controls.Add(Me.cboStaffNo)
        Me.tpgCompose.Controls.Add(Me.lblMessage)
        Me.tpgCompose.Controls.Add(Me.lblReceiverLoginID)
        Me.tpgCompose.Location = New System.Drawing.Point(4, 22)
        Me.tpgCompose.Name = "tpgCompose"
        Me.tpgCompose.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgCompose.Size = New System.Drawing.Size(903, 342)
        Me.tpgCompose.TabIndex = 0
        Me.tpgCompose.Text = "Compose"
        Me.tpgCompose.UseVisualStyleBackColor = True
        '
        'tpgInbox
        '
        Me.tpgInbox.Controls.Add(Me.dgvInbox)
        Me.tpgInbox.Location = New System.Drawing.Point(4, 22)
        Me.tpgInbox.Name = "tpgInbox"
        Me.tpgInbox.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgInbox.Size = New System.Drawing.Size(903, 342)
        Me.tpgInbox.TabIndex = 1
        Me.tpgInbox.Text = "Received"
        Me.tpgInbox.UseVisualStyleBackColor = True
        '
        'dgvInbox
        '
        Me.dgvInbox.AllowUserToAddRows = False
        Me.dgvInbox.AllowUserToDeleteRows = False
        Me.dgvInbox.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvInbox.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvInbox.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvInbox.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvInbox.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvInbox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvInbox.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvInbox.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInbox.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvInbox.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colFromFullName, Me.colRecordDate, Me.colMessageInfo})
        Me.dgvInbox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvInbox.EnableHeadersVisualStyles = False
        Me.dgvInbox.GridColor = System.Drawing.Color.Khaki
        Me.dgvInbox.Location = New System.Drawing.Point(3, 3)
        Me.dgvInbox.Name = "dgvInbox"
        Me.dgvInbox.ReadOnly = True
        Me.dgvInbox.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInbox.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvInbox.RowHeadersVisible = False
        Me.dgvInbox.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvInbox.Size = New System.Drawing.Size(897, 336)
        Me.dgvInbox.TabIndex = 3
        Me.dgvInbox.Text = "DataGridView1"
        '
        'colFromFullName
        '
        Me.colFromFullName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle3.NullValue = Nothing
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colFromFullName.DefaultCellStyle = DataGridViewCellStyle3
        Me.colFromFullName.HeaderText = "From"
        Me.colFromFullName.Name = "colFromFullName"
        Me.colFromFullName.ReadOnly = True
        '
        'colRecordDate
        '
        Me.colRecordDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colRecordDate.DefaultCellStyle = DataGridViewCellStyle4
        Me.colRecordDate.HeaderText = "Date & Time"
        Me.colRecordDate.Name = "colRecordDate"
        Me.colRecordDate.ReadOnly = True
        '
        'colMessageInfo
        '
        Me.colMessageInfo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colMessageInfo.DefaultCellStyle = DataGridViewCellStyle5
        Me.colMessageInfo.HeaderText = "Message"
        Me.colMessageInfo.Name = "colMessageInfo"
        Me.colMessageInfo.ReadOnly = True
        '
        'tpgOutbox
        '
        Me.tpgOutbox.Controls.Add(Me.dgvOutbox)
        Me.tpgOutbox.Location = New System.Drawing.Point(4, 22)
        Me.tpgOutbox.Name = "tpgOutbox"
        Me.tpgOutbox.Size = New System.Drawing.Size(903, 342)
        Me.tpgOutbox.TabIndex = 2
        Me.tpgOutbox.Text = "Sent"
        Me.tpgOutbox.UseVisualStyleBackColor = True
        '
        'dgvOutbox
        '
        Me.dgvOutbox.AllowUserToAddRows = False
        Me.dgvOutbox.AllowUserToDeleteRows = False
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle7.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvOutbox.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvOutbox.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvOutbox.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvOutbox.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvOutbox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvOutbox.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvOutbox.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOutbox.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvOutbox.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColSentTo, Me.ColSendRecordDate, Me.ColSendMessageInfo})
        Me.dgvOutbox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOutbox.EnableHeadersVisualStyles = False
        Me.dgvOutbox.GridColor = System.Drawing.Color.Khaki
        Me.dgvOutbox.Location = New System.Drawing.Point(0, 0)
        Me.dgvOutbox.Name = "dgvOutbox"
        Me.dgvOutbox.ReadOnly = True
        Me.dgvOutbox.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOutbox.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvOutbox.RowHeadersVisible = False
        Me.dgvOutbox.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvOutbox.Size = New System.Drawing.Size(903, 342)
        Me.dgvOutbox.TabIndex = 4
        Me.dgvOutbox.Text = "DataGridView1"
        '
        'ColSentTo
        '
        Me.ColSentTo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ColSentTo.DefaultCellStyle = DataGridViewCellStyle9
        Me.ColSentTo.HeaderText = "To"
        Me.ColSentTo.Name = "ColSentTo"
        Me.ColSentTo.ReadOnly = True
        '
        'ColSendRecordDate
        '
        Me.ColSendRecordDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ColSendRecordDate.DefaultCellStyle = DataGridViewCellStyle10
        Me.ColSendRecordDate.HeaderText = "Date & Time"
        Me.ColSendRecordDate.Name = "ColSendRecordDate"
        Me.ColSendRecordDate.ReadOnly = True
        '
        'ColSendMessageInfo
        '
        Me.ColSendMessageInfo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ColSendMessageInfo.DefaultCellStyle = DataGridViewCellStyle11
        Me.ColSendMessageInfo.HeaderText = "Message"
        Me.ColSendMessageInfo.Name = "ColSendMessageInfo"
        Me.ColSendMessageInfo.ReadOnly = True
        '
        'lblUnreadMessageAlerts
        '
        Me.lblUnreadMessageAlerts.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnreadMessageAlerts.ForeColor = System.Drawing.Color.Red
        Me.lblUnreadMessageAlerts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblUnreadMessageAlerts.Location = New System.Drawing.Point(8, 4)
        Me.lblUnreadMessageAlerts.Name = "lblUnreadMessageAlerts"
        Me.lblUnreadMessageAlerts.Size = New System.Drawing.Size(176, 20)
        Me.lblUnreadMessageAlerts.TabIndex = 17
        Me.lblUnreadMessageAlerts.Text = "Unread Messages : 0"
        Me.lblUnreadMessageAlerts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tmrAlerts
        '
        Me.tmrAlerts.Enabled = True
        Me.tmrAlerts.Interval = 120000
        '
        'frmMessenger
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(929, 469)
        Me.Controls.Add(Me.lblUnreadMessageAlerts)
        Me.Controls.Add(Me.tbcMessenger)
        Me.Controls.Add(Me.fbnSearch)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.fbnDelete)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmMessenger"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Chat"
        Me.tbcMessenger.ResumeLayout(False)
        Me.tpgCompose.ResumeLayout(False)
        Me.tpgCompose.PerformLayout()
        Me.tpgInbox.ResumeLayout(False)
        CType(Me.dgvInbox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgOutbox.ResumeLayout(False)
        CType(Me.dgvOutbox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents lblReceiverLoginID As System.Windows.Forms.Label
    Friend WithEvents stbMessage As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents cboStaffNo As System.Windows.Forms.ComboBox
    Friend WithEvents tbcMessenger As System.Windows.Forms.TabControl
    Friend WithEvents tpgCompose As System.Windows.Forms.TabPage
    Friend WithEvents tpgInbox As System.Windows.Forms.TabPage
    Friend WithEvents dgvInbox As System.Windows.Forms.DataGridView
    Friend WithEvents tpgOutbox As System.Windows.Forms.TabPage
    Friend WithEvents dgvOutbox As System.Windows.Forms.DataGridView
    Friend WithEvents lblUnreadMessageAlerts As System.Windows.Forms.Label
    Friend WithEvents tmrAlerts As System.Windows.Forms.Timer
    Friend WithEvents ColSentTo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColSendRecordDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColSendMessageInfo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFromFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRecordDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colMessageInfo As System.Windows.Forms.DataGridViewTextBoxColumn

End Class