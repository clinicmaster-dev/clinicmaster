
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccountActivations : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAccountActivations))
        Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.cboAccountBillModesID = New System.Windows.Forms.ComboBox()
        Me.cboAccountStatusID = New System.Windows.Forms.ComboBox()
        Me.nbxOutstandingBalance = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.nbxAccountBalance = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.stbAccountName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.lblAccountBillModesID = New System.Windows.Forms.Label()
        Me.lblAccountStatusID = New System.Windows.Forms.Label()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.cboAccountBillNo = New System.Windows.Forms.ComboBox()
        Me.lblAccountBillNo = New System.Windows.Forms.Label()
        Me.lblOutstandingBalance = New System.Windows.Forms.Label()
        Me.lblAccountBalance = New System.Windows.Forms.Label()
        Me.lblAccountName = New System.Windows.Forms.Label()
        Me.SuspendLayout
        '
        'fbnSearch
        '
        Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnSearch.Location = New System.Drawing.Point(13, 190)
        Me.fbnSearch.Name = "fbnSearch"
        Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
        Me.fbnSearch.TabIndex = 13
        Me.fbnSearch.Text = "S&earch"
        Me.fbnSearch.UseVisualStyleBackColor = true
        Me.fbnSearch.Visible = false
        '
        'fbnDelete
        '
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(362, 190)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 14
        Me.fbnDelete.Tag = "AccountActivations"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = false
        Me.fbnDelete.Visible = false
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(13, 217)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 15
        Me.ebnSaveUpdate.Tag = "AccountActivations"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = false
        '
        'cboAccountBillModesID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboAccountBillModesID, "AccountBillModes,AccountBillModesID")
        Me.cboAccountBillModesID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountBillModesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboAccountBillModesID.Location = New System.Drawing.Point(189, 12)
        Me.cboAccountBillModesID.Name = "cboAccountBillModesID"
        Me.cboAccountBillModesID.Size = New System.Drawing.Size(199, 21)
        Me.cboAccountBillModesID.TabIndex = 1
        '
        'cboAccountStatusID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboAccountStatusID, "AccountStatus,AccountStatusID")
        Me.cboAccountStatusID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountStatusID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboAccountStatusID.Location = New System.Drawing.Point(189, 162)
        Me.cboAccountStatusID.Name = "cboAccountStatusID"
        Me.cboAccountStatusID.Size = New System.Drawing.Size(199, 21)
        Me.cboAccountStatusID.TabIndex = 12
        '
        'nbxOutstandingBalance
        '
        Me.nbxOutstandingBalance.BackColor = System.Drawing.SystemColors.Info
        Me.nbxOutstandingBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxOutstandingBalance.ControlCaption = "Amount"
        Me.ebnSaveUpdate.SetDataMember(Me.nbxOutstandingBalance, "Amount")
        Me.nbxOutstandingBalance.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxOutstandingBalance.DecimalPlaces = -1
        Me.nbxOutstandingBalance.Location = New System.Drawing.Point(189, 136)
        Me.nbxOutstandingBalance.MaxValue = 0R
        Me.nbxOutstandingBalance.MinValue = 0R
        Me.nbxOutstandingBalance.Multiline = true
        Me.nbxOutstandingBalance.MustEnterNumeric = true
        Me.nbxOutstandingBalance.Name = "nbxOutstandingBalance"
        Me.nbxOutstandingBalance.ReadOnly = true
        Me.nbxOutstandingBalance.Size = New System.Drawing.Size(199, 20)
        Me.nbxOutstandingBalance.TabIndex = 10
        Me.nbxOutstandingBalance.Value = ""
        '
        'nbxAccountBalance
        '
        Me.nbxAccountBalance.BackColor = System.Drawing.SystemColors.Info
        Me.nbxAccountBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxAccountBalance.ControlCaption = "Amount"
        Me.ebnSaveUpdate.SetDataMember(Me.nbxAccountBalance, "Amount")
        Me.nbxAccountBalance.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxAccountBalance.DecimalPlaces = -1
        Me.nbxAccountBalance.Location = New System.Drawing.Point(189, 112)
        Me.nbxAccountBalance.MaxValue = 0R
        Me.nbxAccountBalance.MinValue = 0R
        Me.nbxAccountBalance.Multiline = true
        Me.nbxAccountBalance.MustEnterNumeric = true
        Me.nbxAccountBalance.Name = "nbxAccountBalance"
        Me.nbxAccountBalance.ReadOnly = true
        Me.nbxAccountBalance.Size = New System.Drawing.Size(199, 20)
        Me.nbxAccountBalance.TabIndex = 8
        Me.nbxAccountBalance.Value = ""
        '
        'stbAccountName
        '
        Me.stbAccountName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAccountName.CapitalizeFirstLetter = false
        Me.ebnSaveUpdate.SetDataMember(Me.stbAccountName, "AccountName")
        Me.stbAccountName.EntryErrorMSG = ""
        Me.stbAccountName.Location = New System.Drawing.Point(189, 62)
        Me.stbAccountName.Multiline = true
        Me.stbAccountName.Name = "stbAccountName"
        Me.stbAccountName.ReadOnly = true
        Me.stbAccountName.RegularExpression = ""
        Me.stbAccountName.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.stbAccountName.Size = New System.Drawing.Size(199, 47)
        Me.stbAccountName.TabIndex = 6
        '
        'fbnClose
        '
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(362, 217)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 16
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = false
        '
        'lblAccountBillModesID
        '
        Me.lblAccountBillModesID.Location = New System.Drawing.Point(12, 12)
        Me.lblAccountBillModesID.Name = "lblAccountBillModesID"
        Me.lblAccountBillModesID.Size = New System.Drawing.Size(155, 20)
        Me.lblAccountBillModesID.TabIndex = 0
        Me.lblAccountBillModesID.Text = "Account Bill Mode"
        '
        'lblAccountStatusID
        '
        Me.lblAccountStatusID.Location = New System.Drawing.Point(12, 159)
        Me.lblAccountStatusID.Name = "lblAccountStatusID"
        Me.lblAccountStatusID.Size = New System.Drawing.Size(155, 20)
        Me.lblAccountStatusID.TabIndex = 11
        Me.lblAccountStatusID.Text = "Account Status"
        '
        'btnLoad
        '
        Me.btnLoad.AccessibleDescription = ""
        Me.btnLoad.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoad.Location = New System.Drawing.Point(392, 36)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(46, 24)
        Me.btnLoad.TabIndex = 4
        Me.btnLoad.Tag = ""
        Me.btnLoad.Text = "&Load"
        Me.btnLoad.Visible = false
        '
        'cboAccountBillNo
        '
        Me.cboAccountBillNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboAccountBillNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAccountBillNo.BackColor = System.Drawing.SystemColors.Window
        Me.cboAccountBillNo.DropDownWidth = 256
        Me.cboAccountBillNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboAccountBillNo.FormattingEnabled = true
        Me.cboAccountBillNo.ItemHeight = 13
        Me.cboAccountBillNo.Location = New System.Drawing.Point(189, 37)
        Me.cboAccountBillNo.Name = "cboAccountBillNo"
        Me.cboAccountBillNo.Size = New System.Drawing.Size(199, 21)
        Me.cboAccountBillNo.TabIndex = 3
        '
        'lblAccountBillNo
        '
        Me.lblAccountBillNo.Location = New System.Drawing.Point(12, 38)
        Me.lblAccountBillNo.Name = "lblAccountBillNo"
        Me.lblAccountBillNo.Size = New System.Drawing.Size(155, 20)
        Me.lblAccountBillNo.TabIndex = 2
        Me.lblAccountBillNo.Text = "Account Bill No"
        '
        'lblOutstandingBalance
        '
        Me.lblOutstandingBalance.Location = New System.Drawing.Point(12, 136)
        Me.lblOutstandingBalance.Name = "lblOutstandingBalance"
        Me.lblOutstandingBalance.Size = New System.Drawing.Size(155, 20)
        Me.lblOutstandingBalance.TabIndex = 9
        Me.lblOutstandingBalance.Text = "Outstanding Balance"
        '
        'lblAccountBalance
        '
        Me.lblAccountBalance.Location = New System.Drawing.Point(12, 112)
        Me.lblAccountBalance.Name = "lblAccountBalance"
        Me.lblAccountBalance.Size = New System.Drawing.Size(155, 20)
        Me.lblAccountBalance.TabIndex = 7
        Me.lblAccountBalance.Text = "Account Balance"
        '
        'lblAccountName
        '
        Me.lblAccountName.Location = New System.Drawing.Point(12, 62)
        Me.lblAccountName.Name = "lblAccountName"
        Me.lblAccountName.Size = New System.Drawing.Size(155, 20)
        Me.lblAccountName.TabIndex = 5
        Me.lblAccountName.Text = "Account Name"
        '
        'frmAccountActivations
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(440, 252)
        Me.Controls.Add(Me.nbxOutstandingBalance)
        Me.Controls.Add(Me.lblOutstandingBalance)
        Me.Controls.Add(Me.nbxAccountBalance)
        Me.Controls.Add(Me.lblAccountBalance)
        Me.Controls.Add(Me.stbAccountName)
        Me.Controls.Add(Me.lblAccountName)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.cboAccountBillNo)
        Me.Controls.Add(Me.lblAccountBillNo)
        Me.Controls.Add(Me.fbnSearch)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.cboAccountBillModesID)
        Me.Controls.Add(Me.lblAccountBillModesID)
        Me.Controls.Add(Me.cboAccountStatusID)
        Me.Controls.Add(Me.lblAccountStatusID)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.KeyPreview = true
        Me.MaximizeBox = false
        Me.Name = "frmAccountActivations"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "AccountActivations"
        Me.Text = "Account Activations"
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents cboAccountBillModesID As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccountBillModesID As System.Windows.Forms.Label
    Friend WithEvents cboAccountStatusID As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccountStatusID As System.Windows.Forms.Label
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents cboAccountBillNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccountBillNo As System.Windows.Forms.Label
    Friend WithEvents nbxOutstandingBalance As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblOutstandingBalance As System.Windows.Forms.Label
    Friend WithEvents nbxAccountBalance As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblAccountBalance As System.Windows.Forms.Label
    Friend WithEvents stbAccountName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAccountName As System.Windows.Forms.Label

End Class