﻿
Option Strict On
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.SQLDb.Lookup
Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects


Imports SyncSoft.Common.Structures

Imports SyncSoft.Common.SQL.Methods
Imports LookupDataID = SyncSoft.SQLDb.Lookup.LookupDataID
Imports SyncSoft.SQLDb
Imports System.Drawing.Printing

Public Class frmAccountStatement

#Region " Fields "
    Private billCustomers As DataTable
    Private WithEvents docPrintAccountStatement As New PrintDocument()
    Private accountStatement As Collection
    Private pageNo As Integer
    Private printFontName As String = "Courier New"
    Private bodyBoldFont As New Font(printFontName, 7, FontStyle.Bold)
    Private bodyNormalFont As New Font(printFontName, 7)
    Private bodySmallFont As New Font(printFontName, 6)
#End Region

    Private Sub frmAccountStatement_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Cursor = Cursors.WaitCursor()

            LoadLookupDataCombo(Me.cboBillMode, LookupObjects.BillModes, False)
            Me.dtpStartDate.Value = Today.AddDays(-1)

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try
    End Sub

    Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub

    Private Sub ClearControls()
        Me.stbAccountName.Clear()
        Me.nbxAccountBalance.Clear()
        Me.nbxOutstandingBill.Clear()
        Me.stbAccountName.Clear()
        Me.dgvAccountStatement.Rows.Clear()
        Me.cboAccountNo.Text = String.Empty
        Me.lblRecordsNo.Text = String.Empty

    End Sub

    Private Sub LoadAccountClients(ByVal billModesID As String)

        Dim oBillCustomers As New SyncSoft.SQLDb.BillCustomers()
        Dim oBillModesID As New LookupDataID.BillModesID()
        Dim oSetupData As New SetupData()

        Try
            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.ClearControls()
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Select Case billModesID.ToUpper()

                Case oBillModesID.Cash.ToUpper()
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.lblAccountNo.Text = "Account No"
                    Me.lblAccountName.Text = "Patient Name"
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Account.ToUpper()

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ' Load all from Bill Customers

                    If Not InitOptions.LoadBillCustomersAtStart Then
                        billCustomers = oBillCustomers.GetBillCustomers().Tables("BillCustomers")
                        oSetupData.BillCustomers = billCustomers
                    Else : billCustomers = oSetupData.BillCustomers
                    End If

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    LoadComboData(Me.cboAccountNo, billCustomers, "BillCustomerFullName")
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.lblAccountNo.Text = "Account No"
                    Me.lblAccountName.Text = "Account Name"
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Insurance.ToUpper()

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.lblAccountNo.Text = "Insurance No"
                    Me.lblAccountName.Text = "Insurance Name"
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End Select

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub LoadAccountDetails()
        Try
            Me.dgvAccountStatement.Rows.Clear()

            Dim billModesID As String = StringValueMayBeEnteredIn(Me.cboBillMode, "Account Category!")
            Dim accountNo As String = RevertText(SubstringRight(StringMayBeEnteredIn(Me.cboAccountNo)))

            If String.IsNullOrEmpty(accountNo) OrElse String.IsNullOrEmpty(billModesID) Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.LoadAccountDetails(billModesID, accountNo)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub LoadAccountDetails(ByVal billModesID As String, ByVal accountNo As String)

        Dim accountName As String = String.Empty
        Dim accountBalance As Decimal

        Dim oPatients As New SyncSoft.SQLDb.Patients()
        Dim oBillCustomers As New SyncSoft.SQLDb.BillCustomers()
        Dim oInsurances As New SyncSoft.SQLDb.Insurances()

        Dim oBillModesID As New LookupDataID.BillModesID()

        Try
            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbAccountName.Clear()
            Me.nbxAccountBalance.Clear()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Select Case billModesID.ToUpper()

                Case oBillModesID.Cash.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim row As DataRow = oPatients.GetPatients(accountNo).Tables("Patients").Rows(0)

                    Me.cboAccountNo.Text = FormatText(accountNo, "Patients", "PatientNo")
                    accountName = StringMayBeEnteredIn(row, "FullName")
                    accountBalance = GetAccountBalance(oBillModesID.Cash, accountNo)
                    Dim outstandingBalance As Decimal = DecimalMayBeEnteredIn(row, "OutstandingBalance")
                    Me.nbxOutstandingBill.Value = FormatNumber(outstandingBalance, AppData.DecimalPlaces)
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Account.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim row As DataRow = oBillCustomers.GetBillCustomers(accountNo).Tables("BillCustomers").Rows(0)

                    Me.cboAccountNo.Text = FormatText(accountNo, "BillCustomers", "AccountNo").ToUpper()
                    accountName = StringMayBeEnteredIn(row, "BillCustomerName")
                    accountBalance = GetAccountBalance(oBillModesID.Account, accountNo)
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Insurance.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim row As DataRow = oInsurances.GetInsurances(accountNo).Tables("Insurances").Rows(0)

                    Me.cboAccountNo.Text = FormatText(accountNo, "Insurances", "InsuranceNo")
                    accountName = StringMayBeEnteredIn(row, "InsuranceName")
                    accountBalance = GetAccountBalance(oBillModesID.Insurance, accountNo)
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End Select

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbAccountName.Text = accountName
            Me.nbxAccountBalance.Value = FormatNumber(accountBalance, AppData.DecimalPlaces)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub cboBillMode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBillMode.SelectedIndexChanged
        Try

            Me.Cursor = Cursors.WaitCursor

            Dim oBillModesID As New LookupDataID.BillModesID()
            Me.cboAccountNo.Items.Clear()
            Me.ClearControls()


            Dim billModesID As String = StringValueMayBeEnteredIn(Me.cboBillMode, "Account Category!")
            If String.IsNullOrEmpty(billModesID) Then Return
            Me.btnLoad.Visible = billModesID.ToUpper().Equals(oBillModesID.Cash().ToUpper())
            Me.LoadAccountClients(billModesID)

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub cboAccountNo_Leave(sender As Object, e As EventArgs) Handles cboAccountNo.Leave
        Try
            Me.Cursor = Cursors.WaitCursor
            LoadAccountDetails()
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default
            Me.Cursor = Cursors.Default
        End Try
    End Sub
    Private Sub fbnRefresh_Click(sender As Object, e As EventArgs) Handles fbnRefresh.Click
        Try
            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''''''''''
            Me.fbnLoad.PerformClick()
            ''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub btnLoad_Click(sender As Object, e As EventArgs) Handles btnLoad.Click
        Try

            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim fQuickSearch As New SyncSoft.SQL.Win.Forms.QuickSearch("Patients", Me.cboAccountNo)
            fQuickSearch.ShowDialog(Me)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim patientNo As String = RevertText(StringMayBeEnteredIn(Me.cboAccountNo))
            If Not String.IsNullOrEmpty(patientNo) Then Me.LoadAccountDetails()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub fbnLoad_Click(sender As System.Object, e As System.EventArgs) Handles fbnLoad.Click
        Try

            Me.Cursor = Cursors.WaitCursor
            loadData()
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub loadData()

        Dim oAccounts As New SyncSoft.SQLDb.Accounts

        Try

            Me.Cursor = Cursors.WaitCursor
            Dim oItems As New Items()
            Dim Invoicecategorised As New DataTable()
            Dim refunds As New DataTable
            Dim oBillModesID As New LookupDataID.BillModesID()
            Dim oPayments As New SyncSoft.SQLDb.Payments()
            Dim AccountStatement As New DataTable()
            Dim rowCount As Integer

            Dim billModesID As String = StringValueEnteredIn(Me.cboBillMode, "Account Category!")
            Dim AccountNo As String = RevertText(SubstringRight(StringEnteredIn(Me.cboAccountNo, "Account Name!")))
            Dim startDate As Date = DateTimeEnteredIn(Me.dtpStartDate, "Start Record")
            Dim endDate As Date = DateTimeEnteredIn(Me.dtpEndDate, "End Date")

            Dim message As String = "No " + Me.Text + " record(s) found for period between " +
                FormatDate(startDate) + " and " + FormatDate(endDate) + "!"


            If endDate < startDate Then Throw New ArgumentException("End Date can't be before Start Date!")
            If String.IsNullOrEmpty(billModesID) Then Return


            Me.LoadAccountDetails(billModesID, AccountNo)
            AccountStatement = oPayments.GetPeriodicAccountStatementByToBillCustomerNo(billModesID, AccountNo, startDate, endDate).Tables("AccountStatement")
            LoadGridData(Me.dgvAccountStatement, AccountStatement)


            rowCount = dgvAccountStatement.RowCount

            EnablePrintButtons(rowCount > 0)

            Me.fbnExport.Enabled = rowCount > 0
            Me.lblRecordsNo.Text = "Record(s): " + rowCount.ToString()


            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


#Region "Print"



    Private Sub EnablePrintButtons(bool As Boolean)
        btnPrint.Enabled = bool
        btnPrintPreview.Enabled = bool

    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Try

            Me.Cursor = Cursors.WaitCursor

            Me.SetAccountStatementPrintData()

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub





    Private Sub btnPrintPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintPreview.Click

        Try

            Me.Cursor = Cursors.WaitCursor

            ' Make a PrintDocument and attach it to the PrintPreview dialog.
            Dim dlgPrintPreview As New PrintPreviewDialog()

            Me.SetAccountStatementPrintData()

            With dlgPrintPreview
                .Document = docPrintAccountStatement
                .Document.PrinterSettings.Collate = True
                .ShowIcon = False
                .WindowState = FormWindowState.Maximized
                .ShowDialog()
            End With

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


    Private Sub PrintBillInvoice()

        Dim dlgPrint As New PrintDialog()

        Try

            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.SetAccountStatementPrintData()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            dlgPrint.Document = docPrintAccountStatement
            dlgPrint.Document.PrinterSettings.Collate = True
            If dlgPrint.ShowDialog = DialogResult.OK Then docPrintAccountStatement.Print()

        Catch ex As Exception
            Throw ex

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub docPrintAccountStatement_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles docPrintAccountStatement.PrintPage

        Try

            Dim titleFont As New Font(printFontName, 12, FontStyle.Bold)

            Dim xPos As Single = e.MarginBounds.Left
            Dim yPos As Single = e.MarginBounds.Top

            Dim lineHeight As Single = bodyNormalFont.GetHeight(e.Graphics)

            Dim title As String = Me.Text

            Dim startDate As String = FormatDate(DateMayBeEnteredIn(Me.dtpStartDate))
            Dim endDate As String = FormatDate(DateMayBeEnteredIn(Me.dtpEndDate))
            Dim accountNo As String = StringMayBeEnteredIn(cboAccountNo)
            Dim accountName As String = StringMayBeEnteredIn(stbAccountName)

            Dim accountBalance As String = FormatNumber(DecimalMayBeEnteredIn(nbxAccountBalance), AppData.DecimalPlaces)
            Dim outStandingBalance As String = FormatNumber(DecimalMayBeEnteredIn(nbxOutstandingBill), AppData.DecimalPlaces)

            If Not String.IsNullOrEmpty(accountName) Then
                title += " of: " + accountName
            End If

            title = title.ToUpper()
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ' Increment the page number.
            pageNo += 1

            With e.Graphics

                'Dim widthTop As Single = .MeasureString("Received from width", titleFont).Width

                Dim widthTopFirst As Single = .MeasureString("W", titleFont).Width
                Dim widthTopSecond As Single = 11 * widthTopFirst
                Dim widthTopThird As Single = 18 * widthTopFirst
                Dim widthTopFourth As Single = 26 * widthTopFirst

                If pageNo < 2 Then

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    yPos = PrintPageHeader(e, bodyNormalFont, bodyBoldFont)
                    Dim oProductOwner As ProductOwner = GetProductOwnerInfo()
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    .DrawString(title, titleFont, Brushes.Black, xPos, yPos)
                    yPos += 3 * lineHeight
                    If Not String.IsNullOrEmpty(accountNo) Then
                        .DrawString(lblAccountNo.Text + ": ", bodyNormalFont, Brushes.Black, xPos, yPos)
                        .DrawString(accountNo, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                        yPos += lineHeight
                        .DrawString(lblAccountName.Text + ": ", bodyNormalFont, Brushes.Black, xPos, yPos)
                        .DrawString(accountName, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                        yPos += lineHeight
                    End If

                    .DrawString("Start Date: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(startDate, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight
                    .DrawString("End Date: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(endDate, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight

                    .DrawString("Account Balance: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(accountBalance, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight

                    .DrawString("Outstanding Balance: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(outStandingBalance, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight

                End If

                Dim _StringFormat As New StringFormat()

                ' Draw the rest of the text left justified,
                ' wrap at words, and don't draw partial lines.

                With _StringFormat
                    .Alignment = StringAlignment.Near
                    .FormatFlags = StringFormatFlags.LineLimit
                    .Trimming = StringTrimming.Word
                End With

                Dim charactersFitted As Integer
                Dim linesFilled As Integer

                If accountStatement Is Nothing Then Return

                Do While accountStatement.Count > 0

                    ' Print the next paragraph.
                    Dim oPrintParagraps As PrintParagraps = DirectCast(accountStatement(1), PrintParagraps)
                    accountStatement.Remove(1)

                    ' Get the area available for this paragraph.
                    Dim printAreaRectangle As RectangleF = New RectangleF(e.MarginBounds.Left, yPos, e.MarginBounds.Width, e.MarginBounds.Bottom - yPos)

                    ' If the printing area rectangle's height < 1, make it 1.
                    If printAreaRectangle.Height < 1 Then printAreaRectangle.Height = 1

                    ' See how big the text will be and how many characters will fit.
                    Dim textSize As SizeF = .MeasureString(oPrintParagraps.Text, oPrintParagraps.TheFont,
                        New SizeF(printAreaRectangle.Width, printAreaRectangle.Height), _StringFormat, charactersFitted, linesFilled)

                    ' See if any characters will fit.
                    If charactersFitted > 0 Then
                        ' Draw the text.
                        .DrawString(oPrintParagraps.Text, oPrintParagraps.TheFont, Brushes.Black, printAreaRectangle, _StringFormat)
                        ' Increase the location where we can start, add a little interparagraph spacing.
                        yPos += textSize.Height ' + oPrintParagraps.TheFont.GetHeight(e.Graphics))

                    End If

                    ' See if some of the paragraph didn't fit on the page.
                    If charactersFitted < oPrintParagraps.Text.Length Then
                        ' Some of the paragraph didn't fit, prepare to print the rest on the next page.
                        oPrintParagraps.Text = oPrintParagraps.Text.Substring(charactersFitted)
                        accountStatement.Add(oPrintParagraps, Before:=1)
                        Exit Do
                    End If
                Loop

                ' If we have more paragraphs, we have more pages.
                e.HasMorePages = (accountStatement.Count > 0)

            End With

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetAccountStatementPrintData()

        Dim padTotalAmount As Integer = 44
        Dim footerFont As New Font(printFontName, 9)
        Dim oVariousOptions As New VariousOptions()

        pageNo = 0
        accountStatement = New Collection()

        Try
            Dim count As Integer = 0
            Dim tableHeader As New System.Text.StringBuilder(String.Empty)
            Dim tableData As New System.Text.StringBuilder(String.Empty)


            Dim padItemNo As Integer = 4
            Dim padTransactionDate As Integer = 13
            Dim padNotes As Integer = 19
            Dim padType As Integer = 12
            Dim padTransactionalNo As Integer = 13
            Dim padCredit As Integer = 14
            Dim padDebit As Integer = 14
            Dim padBalance As Integer = 15

            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append("No: ".PadRight(padItemNo))
            tableHeader.Append("Date: ".PadRight(padTransactionDate))
            tableHeader.Append("Details: ".PadRight(padNotes))
            tableHeader.Append("Type: ".PadRight(padType))
            tableHeader.Append("Trans No: ".PadRight(padTransactionalNo))
            tableHeader.Append("Credit: ".PadLeft(padCredit))
            tableHeader.Append("Debit: ".PadLeft(padDebit))
            tableHeader.Append("Balance: ".PadLeft(padBalance))
            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append(ControlChars.NewLine)

            accountStatement.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            For rowNo As Integer = 0 To Me.dgvAccountStatement.RowCount - 1

                Dim cells As DataGridViewCellCollection = Me.dgvAccountStatement.Rows(rowNo).Cells

                count += 1

                Dim itemNo As String = (count).ToString()
                Dim TransactionDate As String = StringMayBeEnteredIn(cells, Me.ColTransactionDate)
                Dim Notes As String = StringMayBeEnteredIn(cells, ColNotes)
                Dim Type As String = StringMayBeEnteredIn(cells, colObjectName)
                Dim TransactionalNo As String = StringMayBeEnteredIn(cells, Me.ColTransactionalNo)
                Dim Credit As String = StringMayBeEnteredIn(cells, Me.colCredit)
                Dim Debit As String = StringMayBeEnteredIn(cells, Me.colDebit)
                Dim Balance As String = StringMayBeEnteredIn(cells, Me.colBalance)


                tableData.Append(itemNo.PadRight(padItemNo))
                tableData.Append(TransactionDate.PadRight(padTransactionDate))


                Dim wrappedFullName As List(Of String) = WrapText(Notes, padNotes)

                If wrappedFullName.Count > 1 Then
                    For pos As Integer = 0 To wrappedFullName.Count - 1
                        tableData.Append(FixDataLength(wrappedFullName(pos).Trim(), padNotes))
                        If Not pos = wrappedFullName.Count - 1 Then
                            tableData.Append(ControlChars.NewLine)
                            tableData.Append(GetSpaces(padItemNo + padTransactionDate))
                        Else
                            Dim wrappedType As List(Of String) = WrapText(Type, padType)

                            If wrappedType.Count > 1 Then
                                For Typepos As Integer = 0 To wrappedType.Count - 1
                                    tableData.Append(FixDataLength(wrappedType(Typepos).Trim(), padType))
                                    If Not Typepos = wrappedType.Count - 1 Then
                                        tableData.Append(ControlChars.NewLine)
                                        tableData.Append(GetSpaces(padItemNo + padTransactionDate + padNotes))
                                    Else
                                        tableData.Append(TransactionalNo.PadRight(padTransactionalNo))
                                        tableData.Append(Credit.PadLeft(padCredit))
                                        tableData.Append(Debit.PadLeft(padDebit))
                                        tableData.Append(Balance.PadLeft(padBalance))
                                    End If
                                Next
                            Else
                                tableData.Append(Type.PadRight(padType))
                                tableData.Append(TransactionalNo.PadRight(padTransactionalNo))
                                tableData.Append(Credit.PadLeft(padCredit))
                                tableData.Append(Debit.PadLeft(padDebit))
                                tableData.Append(Balance.PadLeft(padBalance))

                            End If
                        End If
                    Next
                Else

                    tableData.Append(Notes.PadRight(padNotes))


                    Dim wrappedType As List(Of String) = WrapText(Type, padType)

                    If wrappedType.Count > 1 Then
                        For pos As Integer = 0 To wrappedType.Count - 1
                            tableData.Append(FixDataLength(wrappedType(pos).Trim(), padType))
                            If Not pos = wrappedType.Count - 1 Then
                                tableData.Append(ControlChars.NewLine)
                                tableData.Append(GetSpaces(padItemNo + padTransactionDate + padNotes))
                            Else
                                tableData.Append(TransactionalNo.PadRight(padTransactionalNo))
                                tableData.Append(Credit.PadLeft(padCredit))
                                tableData.Append(Debit.PadLeft(padDebit))
                                tableData.Append(Balance.PadLeft(padBalance))
                            End If
                        Next
                    Else
                        tableData.Append(Type.PadRight(padType))
                        tableData.Append(TransactionalNo.PadRight(padTransactionalNo))
                        tableData.Append(Credit.PadLeft(padCredit))
                        tableData.Append(Debit.PadLeft(padDebit))
                        tableData.Append(Balance.PadLeft(padBalance))

                    End If
                End If

                    tableData.Append(ControlChars.NewLine)

            Next


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            accountStatement.Add(New PrintParagraps(bodyNormalFont, tableData.ToString()))

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim checkedSignData As New System.Text.StringBuilder(String.Empty)
            checkedSignData.Append(ControlChars.NewLine)

            checkedSignData.Append("Checked By:       " + GetCharacters("."c, 20))
            checkedSignData.Append(GetSpaces(4))
            checkedSignData.Append("Date:  " + GetCharacters("."c, 20))
            checkedSignData.Append(ControlChars.NewLine)
            accountStatement.Add(New PrintParagraps(footerFont, checkedSignData.ToString()))

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim footerData As New System.Text.StringBuilder(String.Empty)
            footerData.Append(ControlChars.NewLine)
            footerData.Append("Printed by " + CurrentUser.FullName + " on " + FormatDate(Now) + " at " + Now.ToString("hh:mm tt") +
                                " from " + AppData.AppTitle)
            footerData.Append(ControlChars.NewLine)
            accountStatement.Add(New PrintParagraps(footerFont, footerData.ToString()))
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

#End Region

    Private Sub fbnExport_Click(sender As System.Object, e As System.EventArgs) Handles fbnExport.Click
     
        Try

         
            ExportToExcel(dgvAccountStatement, Me.Text)



        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


End Class