
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccountWithdrawApprovals : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAccountWithdrawApprovals))
        Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.stbAccountBillModes = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbAccountName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.dtpApprovalDate = New System.Windows.Forms.DateTimePicker()
        Me.stbNotes = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboRequestStatusID = New System.Windows.Forms.ComboBox()
        Me.stbAccountBillNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.nbxAmount = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.stbWithdrawRequestNotes = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbWithRequestReason = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.nbxOutstandingBalance = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.nbxAccountBalance = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.stbLastValidityDateTime = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbWithdrawType = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbRequestDate = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.stbRequestNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblRequestNo = New System.Windows.Forms.Label()
        Me.lblAccountBillModes = New System.Windows.Forms.Label()
        Me.lblAccountName = New System.Windows.Forms.Label()
        Me.lblApprovalDate = New System.Windows.Forms.Label()
        Me.lblNotes = New System.Windows.Forms.Label()
        Me.lblRequestStatusID = New System.Windows.Forms.Label()
        Me.lblAmount = New System.Windows.Forms.Label()
        Me.lblWithdrawRequestReasonID = New System.Windows.Forms.Label()
        Me.lblRequestNotes = New System.Windows.Forms.Label()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.lblOutstandingBalance = New System.Windows.Forms.Label()
        Me.lblAccountBalance = New System.Windows.Forms.Label()
        Me.lblLastValidityDateTime = New System.Windows.Forms.Label()
        Me.lblWithdrawType = New System.Windows.Forms.Label()
        Me.lblAccountBillNo = New System.Windows.Forms.Label()
        Me.lblRequestDate = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'fbnSearch
        '
        Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnSearch.Location = New System.Drawing.Point(17, 440)
        Me.fbnSearch.Name = "fbnSearch"
        Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
        Me.fbnSearch.TabIndex = 0
        Me.fbnSearch.Text = "S&earch"
        Me.fbnSearch.UseVisualStyleBackColor = True
        Me.fbnSearch.Visible = False
        '
        'fbnDelete
        '
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(316, 440)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 1
        Me.fbnDelete.Tag = "AccountWithdrawApprovals"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = False
        Me.fbnDelete.Visible = False
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(17, 467)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 2
        Me.ebnSaveUpdate.Tag = "AccountWithdrawApprovals"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = False
        '
        'stbAccountBillModes
        '
        Me.stbAccountBillModes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAccountBillModes.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbAccountBillModes, "AccountBillModes")
        Me.stbAccountBillModes.EntryErrorMSG = ""
        Me.stbAccountBillModes.Location = New System.Drawing.Point(190, 34)
        Me.stbAccountBillModes.Name = "stbAccountBillModes"
        Me.stbAccountBillModes.ReadOnly = True
        Me.stbAccountBillModes.RegularExpression = ""
        Me.stbAccountBillModes.Size = New System.Drawing.Size(198, 20)
        Me.stbAccountBillModes.TabIndex = 6
        '
        'stbAccountName
        '
        Me.stbAccountName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAccountName.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbAccountName, "AccountName")
        Me.stbAccountName.EntryErrorMSG = ""
        Me.stbAccountName.Location = New System.Drawing.Point(190, 81)
        Me.stbAccountName.Name = "stbAccountName"
        Me.stbAccountName.ReadOnly = True
        Me.stbAccountName.RegularExpression = ""
        Me.stbAccountName.Size = New System.Drawing.Size(198, 20)
        Me.stbAccountName.TabIndex = 8
        '
        'dtpApprovalDate
        '
        Me.dtpApprovalDate.Checked = False
        Me.ebnSaveUpdate.SetDataMember(Me.dtpApprovalDate, "ApprovalDate")
        Me.dtpApprovalDate.Location = New System.Drawing.Point(190, 149)
        Me.dtpApprovalDate.Name = "dtpApprovalDate"
        Me.dtpApprovalDate.ShowCheckBox = True
        Me.dtpApprovalDate.Size = New System.Drawing.Size(198, 20)
        Me.dtpApprovalDate.TabIndex = 10
        '
        'stbNotes
        '
        Me.stbNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbNotes.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbNotes, "Notes")
        Me.stbNotes.EntryErrorMSG = ""
        Me.stbNotes.Location = New System.Drawing.Point(190, 170)
        Me.stbNotes.Multiline = True
        Me.stbNotes.Name = "stbNotes"
        Me.stbNotes.RegularExpression = ""
        Me.stbNotes.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.stbNotes.Size = New System.Drawing.Size(198, 59)
        Me.stbNotes.TabIndex = 12
        '
        'cboRequestStatusID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboRequestStatusID, "RequestStatus,RequestStatusID")
        Me.cboRequestStatusID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRequestStatusID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboRequestStatusID.Location = New System.Drawing.Point(189, 233)
        Me.cboRequestStatusID.Name = "cboRequestStatusID"
        Me.cboRequestStatusID.Size = New System.Drawing.Size(198, 21)
        Me.cboRequestStatusID.TabIndex = 14
        '
        'stbAccountBillNo
        '
        Me.stbAccountBillNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAccountBillNo.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbAccountBillNo, "AccountBillModes")
        Me.stbAccountBillNo.EntryErrorMSG = ""
        Me.stbAccountBillNo.Location = New System.Drawing.Point(190, 57)
        Me.stbAccountBillNo.Name = "stbAccountBillNo"
        Me.stbAccountBillNo.ReadOnly = True
        Me.stbAccountBillNo.RegularExpression = ""
        Me.stbAccountBillNo.Size = New System.Drawing.Size(198, 20)
        Me.stbAccountBillNo.TabIndex = 41
        '
        'nbxAmount
        '
        Me.nbxAmount.BackColor = System.Drawing.SystemColors.Info
        Me.nbxAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxAmount.ControlCaption = "Amount"
        Me.nbxAmount.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxAmount.DecimalPlaces = -1
        Me.nbxAmount.Location = New System.Drawing.Point(190, 258)
        Me.nbxAmount.MaxValue = 0.0R
        Me.nbxAmount.MinValue = 0.0R
        Me.nbxAmount.Multiline = True
        Me.nbxAmount.MustEnterNumeric = True
        Me.nbxAmount.Name = "nbxAmount"
        Me.nbxAmount.ReadOnly = True
        Me.nbxAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.nbxAmount.Size = New System.Drawing.Size(198, 20)
        Me.nbxAmount.TabIndex = 25
        Me.nbxAmount.Value = ""
        '
        'stbWithdrawRequestNotes
        '
        Me.stbWithdrawRequestNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbWithdrawRequestNotes.CapitalizeFirstLetter = False
        Me.stbWithdrawRequestNotes.EntryErrorMSG = ""
        Me.stbWithdrawRequestNotes.Location = New System.Drawing.Point(190, 373)
        Me.stbWithdrawRequestNotes.Multiline = True
        Me.stbWithdrawRequestNotes.Name = "stbWithdrawRequestNotes"
        Me.stbWithdrawRequestNotes.ReadOnly = True
        Me.stbWithdrawRequestNotes.RegularExpression = ""
        Me.stbWithdrawRequestNotes.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.stbWithdrawRequestNotes.Size = New System.Drawing.Size(198, 57)
        Me.stbWithdrawRequestNotes.TabIndex = 28
        '
        'stbWithRequestReason
        '
        Me.stbWithRequestReason.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbWithRequestReason.CapitalizeFirstLetter = False
        Me.stbWithRequestReason.EntryErrorMSG = ""
        Me.stbWithRequestReason.Location = New System.Drawing.Point(190, 304)
        Me.stbWithRequestReason.Name = "stbWithRequestReason"
        Me.stbWithRequestReason.ReadOnly = True
        Me.stbWithRequestReason.RegularExpression = ""
        Me.stbWithRequestReason.Size = New System.Drawing.Size(198, 20)
        Me.stbWithRequestReason.TabIndex = 32
        '
        'nbxOutstandingBalance
        '
        Me.nbxOutstandingBalance.BackColor = System.Drawing.SystemColors.Info
        Me.nbxOutstandingBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxOutstandingBalance.ControlCaption = "Outstanding Balance"
        Me.nbxOutstandingBalance.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxOutstandingBalance.DecimalPlaces = -1
        Me.nbxOutstandingBalance.Location = New System.Drawing.Point(190, 126)
        Me.nbxOutstandingBalance.MaxValue = 0.0R
        Me.nbxOutstandingBalance.MinValue = 0.0R
        Me.nbxOutstandingBalance.Multiline = True
        Me.nbxOutstandingBalance.MustEnterNumeric = True
        Me.nbxOutstandingBalance.Name = "nbxOutstandingBalance"
        Me.nbxOutstandingBalance.ReadOnly = True
        Me.nbxOutstandingBalance.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.nbxOutstandingBalance.Size = New System.Drawing.Size(197, 20)
        Me.nbxOutstandingBalance.TabIndex = 35
        Me.nbxOutstandingBalance.Value = ""
        '
        'nbxAccountBalance
        '
        Me.nbxAccountBalance.BackColor = System.Drawing.SystemColors.Info
        Me.nbxAccountBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxAccountBalance.ControlCaption = "Account Balance"
        Me.nbxAccountBalance.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxAccountBalance.DecimalPlaces = -1
        Me.nbxAccountBalance.Location = New System.Drawing.Point(190, 104)
        Me.nbxAccountBalance.MaxValue = 0.0R
        Me.nbxAccountBalance.MinValue = 0.0R
        Me.nbxAccountBalance.Multiline = True
        Me.nbxAccountBalance.MustEnterNumeric = True
        Me.nbxAccountBalance.Name = "nbxAccountBalance"
        Me.nbxAccountBalance.ReadOnly = True
        Me.nbxAccountBalance.Size = New System.Drawing.Size(197, 20)
        Me.nbxAccountBalance.TabIndex = 33
        Me.nbxAccountBalance.Value = ""
        '
        'stbLastValidityDateTime
        '
        Me.stbLastValidityDateTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbLastValidityDateTime.CapitalizeFirstLetter = False
        Me.stbLastValidityDateTime.EntryErrorMSG = ""
        Me.stbLastValidityDateTime.Location = New System.Drawing.Point(190, 349)
        Me.stbLastValidityDateTime.Name = "stbLastValidityDateTime"
        Me.stbLastValidityDateTime.ReadOnly = True
        Me.stbLastValidityDateTime.RegularExpression = ""
        Me.stbLastValidityDateTime.Size = New System.Drawing.Size(198, 20)
        Me.stbLastValidityDateTime.TabIndex = 38
        '
        'stbWithdrawType
        '
        Me.stbWithdrawType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbWithdrawType.CapitalizeFirstLetter = False
        Me.stbWithdrawType.EntryErrorMSG = ""
        Me.stbWithdrawType.Location = New System.Drawing.Point(189, 280)
        Me.stbWithdrawType.Name = "stbWithdrawType"
        Me.stbWithdrawType.ReadOnly = True
        Me.stbWithdrawType.RegularExpression = ""
        Me.stbWithdrawType.Size = New System.Drawing.Size(198, 20)
        Me.stbWithdrawType.TabIndex = 40
        '
        'stbRequestDate
        '
        Me.stbRequestDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbRequestDate.CapitalizeFirstLetter = False
        Me.stbRequestDate.EntryErrorMSG = ""
        Me.stbRequestDate.Location = New System.Drawing.Point(190, 326)
        Me.stbRequestDate.Name = "stbRequestDate"
        Me.stbRequestDate.ReadOnly = True
        Me.stbRequestDate.RegularExpression = ""
        Me.stbRequestDate.Size = New System.Drawing.Size(198, 20)
        Me.stbRequestDate.TabIndex = 44
        '
        'fbnClose
        '
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(316, 467)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 3
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'stbRequestNo
        '
        Me.stbRequestNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbRequestNo.CapitalizeFirstLetter = False
        Me.stbRequestNo.EntryErrorMSG = ""
        Me.stbRequestNo.Location = New System.Drawing.Point(190, 12)
        Me.stbRequestNo.Name = "stbRequestNo"
        Me.stbRequestNo.RegularExpression = ""
        Me.stbRequestNo.Size = New System.Drawing.Size(148, 20)
        Me.stbRequestNo.TabIndex = 4
        '
        'lblRequestNo
        '
        Me.lblRequestNo.Location = New System.Drawing.Point(12, 12)
        Me.lblRequestNo.Name = "lblRequestNo"
        Me.lblRequestNo.Size = New System.Drawing.Size(157, 20)
        Me.lblRequestNo.TabIndex = 5
        Me.lblRequestNo.Text = "Request No"
        '
        'lblAccountBillModes
        '
        Me.lblAccountBillModes.Location = New System.Drawing.Point(12, 34)
        Me.lblAccountBillModes.Name = "lblAccountBillModes"
        Me.lblAccountBillModes.Size = New System.Drawing.Size(157, 20)
        Me.lblAccountBillModes.TabIndex = 7
        Me.lblAccountBillModes.Text = "Account Bill Mode"
        '
        'lblAccountName
        '
        Me.lblAccountName.Location = New System.Drawing.Point(12, 81)
        Me.lblAccountName.Name = "lblAccountName"
        Me.lblAccountName.Size = New System.Drawing.Size(157, 20)
        Me.lblAccountName.TabIndex = 9
        Me.lblAccountName.Text = "AccountName"
        '
        'lblApprovalDate
        '
        Me.lblApprovalDate.Location = New System.Drawing.Point(12, 149)
        Me.lblApprovalDate.Name = "lblApprovalDate"
        Me.lblApprovalDate.Size = New System.Drawing.Size(157, 20)
        Me.lblApprovalDate.TabIndex = 11
        Me.lblApprovalDate.Text = "Approval Date"
        '
        'lblNotes
        '
        Me.lblNotes.Location = New System.Drawing.Point(12, 170)
        Me.lblNotes.Name = "lblNotes"
        Me.lblNotes.Size = New System.Drawing.Size(157, 20)
        Me.lblNotes.TabIndex = 13
        Me.lblNotes.Text = "Notes"
        '
        'lblRequestStatusID
        '
        Me.lblRequestStatusID.Location = New System.Drawing.Point(11, 233)
        Me.lblRequestStatusID.Name = "lblRequestStatusID"
        Me.lblRequestStatusID.Size = New System.Drawing.Size(157, 20)
        Me.lblRequestStatusID.TabIndex = 15
        Me.lblRequestStatusID.Text = "Request Status"
        '
        'lblAmount
        '
        Me.lblAmount.Location = New System.Drawing.Point(12, 258)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(157, 20)
        Me.lblAmount.TabIndex = 26
        Me.lblAmount.Text = "Amount"
        '
        'lblWithdrawRequestReasonID
        '
        Me.lblWithdrawRequestReasonID.Location = New System.Drawing.Point(12, 307)
        Me.lblWithdrawRequestReasonID.Name = "lblWithdrawRequestReasonID"
        Me.lblWithdrawRequestReasonID.Size = New System.Drawing.Size(157, 20)
        Me.lblWithdrawRequestReasonID.TabIndex = 27
        Me.lblWithdrawRequestReasonID.Text = "Withdraw Request Reason"
        '
        'lblRequestNotes
        '
        Me.lblRequestNotes.Location = New System.Drawing.Point(12, 373)
        Me.lblRequestNotes.Name = "lblRequestNotes"
        Me.lblRequestNotes.Size = New System.Drawing.Size(157, 20)
        Me.lblRequestNotes.TabIndex = 29
        Me.lblRequestNotes.Text = "Request Notes"
        '
        'btnLoad
        '
        Me.btnLoad.AccessibleDescription = ""
        Me.btnLoad.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoad.Location = New System.Drawing.Point(341, 8)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(46, 24)
        Me.btnLoad.TabIndex = 31
        Me.btnLoad.Tag = ""
        Me.btnLoad.Text = "&Load"
        '
        'lblOutstandingBalance
        '
        Me.lblOutstandingBalance.Location = New System.Drawing.Point(12, 128)
        Me.lblOutstandingBalance.Name = "lblOutstandingBalance"
        Me.lblOutstandingBalance.Size = New System.Drawing.Size(155, 20)
        Me.lblOutstandingBalance.TabIndex = 36
        Me.lblOutstandingBalance.Text = "Outstanding Balance"
        '
        'lblAccountBalance
        '
        Me.lblAccountBalance.Location = New System.Drawing.Point(12, 104)
        Me.lblAccountBalance.Name = "lblAccountBalance"
        Me.lblAccountBalance.Size = New System.Drawing.Size(155, 20)
        Me.lblAccountBalance.TabIndex = 34
        Me.lblAccountBalance.Text = "Account Balance"
        '
        'lblLastValidityDateTime
        '
        Me.lblLastValidityDateTime.Location = New System.Drawing.Point(12, 352)
        Me.lblLastValidityDateTime.Name = "lblLastValidityDateTime"
        Me.lblLastValidityDateTime.Size = New System.Drawing.Size(157, 20)
        Me.lblLastValidityDateTime.TabIndex = 37
        Me.lblLastValidityDateTime.Text = "Last Validity Date Time"
        '
        'lblWithdrawType
        '
        Me.lblWithdrawType.Location = New System.Drawing.Point(11, 283)
        Me.lblWithdrawType.Name = "lblWithdrawType"
        Me.lblWithdrawType.Size = New System.Drawing.Size(157, 20)
        Me.lblWithdrawType.TabIndex = 39
        Me.lblWithdrawType.Text = "Withdraw Type"
        '
        'lblAccountBillNo
        '
        Me.lblAccountBillNo.Location = New System.Drawing.Point(12, 57)
        Me.lblAccountBillNo.Name = "lblAccountBillNo"
        Me.lblAccountBillNo.Size = New System.Drawing.Size(157, 20)
        Me.lblAccountBillNo.TabIndex = 42
        Me.lblAccountBillNo.Text = "Account Bill No"
        '
        'lblRequestDate
        '
        Me.lblRequestDate.Location = New System.Drawing.Point(12, 329)
        Me.lblRequestDate.Name = "lblRequestDate"
        Me.lblRequestDate.Size = New System.Drawing.Size(157, 20)
        Me.lblRequestDate.TabIndex = 43
        Me.lblRequestDate.Text = "Request Date"
        '
        'frmAccountWithdrawApprovals
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(407, 498)
        Me.Controls.Add(Me.stbRequestDate)
        Me.Controls.Add(Me.lblRequestDate)
        Me.Controls.Add(Me.stbAccountBillNo)
        Me.Controls.Add(Me.lblAccountBillNo)
        Me.Controls.Add(Me.stbWithdrawType)
        Me.Controls.Add(Me.lblWithdrawType)
        Me.Controls.Add(Me.stbLastValidityDateTime)
        Me.Controls.Add(Me.lblLastValidityDateTime)
        Me.Controls.Add(Me.nbxOutstandingBalance)
        Me.Controls.Add(Me.lblOutstandingBalance)
        Me.Controls.Add(Me.nbxAccountBalance)
        Me.Controls.Add(Me.lblAccountBalance)
        Me.Controls.Add(Me.stbWithRequestReason)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.nbxAmount)
        Me.Controls.Add(Me.lblAmount)
        Me.Controls.Add(Me.lblWithdrawRequestReasonID)
        Me.Controls.Add(Me.stbWithdrawRequestNotes)
        Me.Controls.Add(Me.lblRequestNotes)
        Me.Controls.Add(Me.fbnSearch)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.stbRequestNo)
        Me.Controls.Add(Me.lblRequestNo)
        Me.Controls.Add(Me.stbAccountBillModes)
        Me.Controls.Add(Me.lblAccountBillModes)
        Me.Controls.Add(Me.stbAccountName)
        Me.Controls.Add(Me.lblAccountName)
        Me.Controls.Add(Me.dtpApprovalDate)
        Me.Controls.Add(Me.lblApprovalDate)
        Me.Controls.Add(Me.stbNotes)
        Me.Controls.Add(Me.lblNotes)
        Me.Controls.Add(Me.cboRequestStatusID)
        Me.Controls.Add(Me.lblRequestStatusID)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmAccountWithdrawApprovals"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "AccountWithdrawApprovals"
        Me.Text = "Account Withdraw Approvals"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents stbRequestNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblRequestNo As System.Windows.Forms.Label
    Friend WithEvents stbAccountBillModes As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAccountBillModes As System.Windows.Forms.Label
    Friend WithEvents stbAccountName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAccountName As System.Windows.Forms.Label
    Friend WithEvents dtpApprovalDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblApprovalDate As System.Windows.Forms.Label
    Friend WithEvents stbNotes As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblNotes As System.Windows.Forms.Label
    Friend WithEvents cboRequestStatusID As System.Windows.Forms.ComboBox
    Friend WithEvents lblRequestStatusID As System.Windows.Forms.Label
    Friend WithEvents nbxAmount As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents lblWithdrawRequestReasonID As System.Windows.Forms.Label
    Friend WithEvents stbWithdrawRequestNotes As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblRequestNotes As System.Windows.Forms.Label
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents stbWithRequestReason As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents nbxOutstandingBalance As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblOutstandingBalance As System.Windows.Forms.Label
    Friend WithEvents nbxAccountBalance As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblAccountBalance As System.Windows.Forms.Label
    Friend WithEvents stbLastValidityDateTime As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblLastValidityDateTime As System.Windows.Forms.Label
    Friend WithEvents stbWithdrawType As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblWithdrawType As System.Windows.Forms.Label
    Friend WithEvents stbAccountBillNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAccountBillNo As System.Windows.Forms.Label
    Friend WithEvents stbRequestDate As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblRequestDate As System.Windows.Forms.Label

End Class