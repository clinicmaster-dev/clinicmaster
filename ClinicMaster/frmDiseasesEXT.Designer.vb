
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDiseasesEXT : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDiseasesEXT))
        Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.stbDescription = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboDiseaseCategoriesID = New System.Windows.Forms.ComboBox()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.stbCode = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblCode = New System.Windows.Forms.Label()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.lblReportTypeID = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'fbnSearch
        '
        Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnSearch.Location = New System.Drawing.Point(4, 130)
        Me.fbnSearch.Name = "fbnSearch"
        Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
        Me.fbnSearch.TabIndex = 0
        Me.fbnSearch.Text = "S&earch"
        Me.fbnSearch.UseVisualStyleBackColor = True
        Me.fbnSearch.Visible = False
        '
        'fbnDelete
        '
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(346, 129)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 1
        Me.fbnDelete.Tag = "DiseasesEXT"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = False
        Me.fbnDelete.Visible = False
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(4, 157)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 2
        Me.ebnSaveUpdate.Tag = "DiseasesEXT"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = False
        '
        'stbDescription
        '
        Me.stbDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbDescription.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbDescription, "Description")
        Me.stbDescription.EntryErrorMSG = ""
        Me.stbDescription.Location = New System.Drawing.Point(159, 35)
        Me.stbDescription.Multiline = True
        Me.stbDescription.Name = "stbDescription"
        Me.stbDescription.RegularExpression = ""
        Me.stbDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbDescription.Size = New System.Drawing.Size(229, 52)
        Me.stbDescription.TabIndex = 6
        '
        'cboDiseaseCategoriesID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboDiseaseCategoriesID, "ReportType,ReportTypeID")
        Me.cboDiseaseCategoriesID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDiseaseCategoriesID.DropDownWidth = 351
        Me.cboDiseaseCategoriesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboDiseaseCategoriesID.Location = New System.Drawing.Point(159, 91)
        Me.cboDiseaseCategoriesID.Name = "cboDiseaseCategoriesID"
        Me.cboDiseaseCategoriesID.Size = New System.Drawing.Size(224, 21)
        Me.cboDiseaseCategoriesID.TabIndex = 10
        '
        'fbnClose
        '
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(346, 156)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 3
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'stbCode
        '
        Me.stbCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbCode.CapitalizeFirstLetter = False
        Me.stbCode.EntryErrorMSG = ""
        Me.stbCode.Location = New System.Drawing.Point(159, 12)
        Me.stbCode.Name = "stbCode"
        Me.stbCode.RegularExpression = ""
        Me.stbCode.Size = New System.Drawing.Size(229, 20)
        Me.stbCode.TabIndex = 4
        '
        'lblCode
        '
        Me.lblCode.Location = New System.Drawing.Point(12, 12)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(141, 20)
        Me.lblCode.TabIndex = 5
        Me.lblCode.Text = "Code"
        '
        'lblDescription
        '
        Me.lblDescription.Location = New System.Drawing.Point(12, 35)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(141, 20)
        Me.lblDescription.TabIndex = 7
        Me.lblDescription.Text = "Description"
        '
        'lblReportTypeID
        '
        Me.lblReportTypeID.Location = New System.Drawing.Point(12, 92)
        Me.lblReportTypeID.Name = "lblReportTypeID"
        Me.lblReportTypeID.Size = New System.Drawing.Size(141, 20)
        Me.lblReportTypeID.TabIndex = 9
        Me.lblReportTypeID.Text = "Report Type"
        '
        'frmDiseasesEXT
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(441, 192)
        Me.Controls.Add(Me.cboDiseaseCategoriesID)
        Me.Controls.Add(Me.fbnSearch)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.stbCode)
        Me.Controls.Add(Me.lblCode)
        Me.Controls.Add(Me.stbDescription)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.lblReportTypeID)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmDiseasesEXT"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Diseases EXT"
        Me.ResumeLayout(False)
        Me.PerformLayout()

End Sub

Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents stbCode As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblCode As System.Windows.Forms.Label
Friend WithEvents stbDescription As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblReportTypeID As System.Windows.Forms.Label
    Friend WithEvents cboDiseaseCategoriesID As System.Windows.Forms.ComboBox

End Class