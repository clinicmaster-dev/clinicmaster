﻿Option Strict On
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects

Public Class frmDashBoardUnbilledAdmissions

    Private Sub ShowUnbilledAdmissions(ByVal startDateTime As Date, ByVal endDateTime As Date)

        Dim oAdmissions As New SyncSoft.SQLDb.Admissions()
        Try
            Me.Cursor = Cursors.WaitCursor
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim oGetUnbilledAdmissions As DataTable = oAdmissions.GetUnbilledAdmissions(startDateTime, endDateTime).Tables("Admissions")

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvUnbilledAdmissions, oGetUnbilledAdmissions)
            FormatGridRow(Me.dgvUnbilledAdmissions)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            Throw ex

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


    Private Sub LoadUnBilledAdmissions()
        Try
            Me.Cursor = Cursors.WaitCursor

            Dim startDateTime As Date = DateTimeEnteredIn(Me.dtpStartDateTime, "Start Record Date and Time")
            Dim endDateTime As Date = DateTimeEnteredIn(Me.dtpEndDateTime, "End Record Date and Time")
            If endDateTime < startDateTime Then Throw New ArgumentException("End Date and Time can't be before Start Date and Time!")

            Me.ShowUnbilledAdmissions(startDateTime, endDateTime)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim rowCount As Integer = Me.dgvUnbilledAdmissions.RowCount
            Me.fbnExport.Enabled = rowCount > 0
            Me.lblRecordsNo.Text = " Returned Record(s): " + rowCount.ToString()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception

            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub frmDashBoardUnbilledAdmissions_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Try
            Me.Cursor = Cursors.WaitCursor()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.dtpStartDateTime.MaxDate = Today.AddDays(1)
            Me.dtpEndDateTime.MaxDate = Today.AddDays(1)

            Me.dtpStartDateTime.Value = Today
            Me.dtpEndDateTime.Value = Now

            Me.LoadUnBilledAdmissions()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try


    End Sub

    Private Sub fbnReportOperations_Click(sender As System.Object, e As System.EventArgs) Handles fbnReportOperations.Click
        Me.LoadUnBilledAdmissions()
    End Sub

    Private Sub fbnExport_Click(sender As System.Object, e As System.EventArgs) Handles fbnExport.Click
        ExportToExcel(Me.dgvUnbilledAdmissions, Me.tpgUnbilledAdmissions.Text)
    End Sub
    Private Sub fbClose_Click(sender As System.Object, e As System.EventArgs) Handles fbClose.Click
        Me.Close()
    End Sub
End Class