
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdmissions : Inherits System.Windows.Forms.Form

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal visitNo As String, ByVal staffFullName As String)
        MyClass.New()
        Me.defaultVisitNo = visitNo
        Me.doctorFullName = staffFullName
    End Sub

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdmissions))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.dtpAdmissionDateTime = New System.Windows.Forms.DateTimePicker()
        Me.cboAdmissionStatusID = New System.Windows.Forms.ComboBox()
        Me.stbVisitNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbVisitDate = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbPatientNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbServiceName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbAge = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbGender = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbFullName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboWardsID = New System.Windows.Forms.ComboBox()
        Me.cboRoomNo = New System.Windows.Forms.ComboBox()
        Me.cboBedNo = New System.Windows.Forms.ComboBox()
        Me.stbBillCustomerName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.nbxUnitPrice = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.cboStaffNo = New System.Windows.Forms.ComboBox()
        Me.stbChartNumber = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboAssociatedBillNo = New System.Windows.Forms.ComboBox()
        Me.cboBillModesID = New System.Windows.Forms.ComboBox()
        Me.stbMainMemberName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbClaimReferenceNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboBillNo = New System.Windows.Forms.ComboBox()
        Me.stbMemberCardNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbAdmissionNotes = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboCoPayTypeID = New System.Windows.Forms.ComboBox()
        Me.nbxCoPayValue = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.nbxCoPayPercent = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.stbInsuranceNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbInsuranceName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.chkSmartCardApplicable = New System.Windows.Forms.CheckBox()
        Me.chkAccessCashServices = New System.Windows.Forms.CheckBox()
        Me.cboServiceCode = New System.Windows.Forms.ComboBox()
        Me.nbxCoverAmount = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.stbAdmissionNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblAdmissionNo = New System.Windows.Forms.Label()
        Me.lblAdmissionDateTime = New System.Windows.Forms.Label()
        Me.lblAdmissionStatusID = New System.Windows.Forms.Label()
        Me.lblPatientsNo = New System.Windows.Forms.Label()
        Me.lblServiceName = New System.Windows.Forms.Label()
        Me.lblAge = New System.Windows.Forms.Label()
        Me.lblGenderID = New System.Windows.Forms.Label()
        Me.lblVisitDate = New System.Windows.Forms.Label()
        Me.lblFullName = New System.Windows.Forms.Label()
        Me.btnFindVisitNo = New System.Windows.Forms.Button()
        Me.lblVisitNo = New System.Windows.Forms.Label()
        Me.pnlAdmissionStatusID = New System.Windows.Forms.Panel()
        Me.btnFindAdmissionNo = New System.Windows.Forms.Button()
        Me.grpLocation = New System.Windows.Forms.GroupBox()
        Me.lblBedNo = New System.Windows.Forms.Label()
        Me.lblWardsID = New System.Windows.Forms.Label()
        Me.lblRoomNo = New System.Windows.Forms.Label()
        Me.lblBillCustomerName = New System.Windows.Forms.Label()
        Me.btnLoadPeriodicVisits = New System.Windows.Forms.Button()
        Me.lblStaff = New System.Windows.Forms.Label()
        Me.lblUnitPrice = New System.Windows.Forms.Label()
        Me.lblChatNo = New System.Windows.Forms.Label()
        Me.imgIDAutomation = New System.Windows.Forms.PictureBox()
        Me.btnPrintBarcode = New System.Windows.Forms.Button()
        Me.chkPrintAdmissionForm = New System.Windows.Forms.CheckBox()
        Me.lblAssociatedBillNo = New System.Windows.Forms.Label()
        Me.lblBillMode = New System.Windows.Forms.Label()
        Me.lblMainMemberName = New System.Windows.Forms.Label()
        Me.lblBillNo = New System.Windows.Forms.Label()
        Me.lblClaimReferenceNo = New System.Windows.Forms.Label()
        Me.lblMemberCardNo = New System.Windows.Forms.Label()
        Me.lblAdmissionNotes = New System.Windows.Forms.Label()
        Me.lblCoPayPercent = New System.Windows.Forms.Label()
        Me.lblCoPayValue = New System.Windows.Forms.Label()
        Me.lblCoPayType = New System.Windows.Forms.Label()
        Me.lblInsuranceNo = New System.Windows.Forms.Label()
        Me.lblBillInsuranceName = New System.Windows.Forms.Label()
        Me.lblAgeString = New System.Windows.Forms.Label()
        Me.chkPrintAdmissionConsent = New System.Windows.Forms.CheckBox()
        Me.lblCoverAmount = New System.Windows.Forms.Label()
        Me.chkPrintAdmissionFaceSheet = New System.Windows.Forms.CheckBox()
        Me.nbxToBillServiceFee = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.lblServiceCode = New System.Windows.Forms.Label()
        Me.lblServiceFee = New System.Windows.Forms.Label()
        Me.tbcAdmissions = New System.Windows.Forms.TabControl()
        Me.tpgGeneral = New System.Windows.Forms.TabPage()
        Me.tpgDiagnosis = New System.Windows.Forms.TabPage()
        Me.dgvDiagnosis = New System.Windows.Forms.DataGridView()
        Me.ColDiagnosisSelect = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.colICDDiagnosisCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDiseaseCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColDiagnosedBy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDiseaseCategory = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNotes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDiagnosisSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.btnFaceSheet = New System.Windows.Forms.Button()
        Me.btnAdmConsent = New System.Windows.Forms.Button()
        Me.pnlAdmissionStatusID.SuspendLayout()
        Me.grpLocation.SuspendLayout()
        CType(Me.imgIDAutomation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbcAdmissions.SuspendLayout()
        Me.tpgGeneral.SuspendLayout()
        Me.tpgDiagnosis.SuspendLayout()
        CType(Me.dgvDiagnosis, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'fbnSearch
        '
        Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnSearch.Location = New System.Drawing.Point(4, 525)
        Me.fbnSearch.Name = "fbnSearch"
        Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
        Me.fbnSearch.TabIndex = 1
        Me.fbnSearch.Text = "S&earch"
        Me.fbnSearch.UseVisualStyleBackColor = True
        Me.fbnSearch.Visible = False
        '
        'fbnDelete
        '
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(623, 524)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 7
        Me.fbnDelete.Tag = "Admissions"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = False
        Me.fbnDelete.Visible = False
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(4, 552)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 2
        Me.ebnSaveUpdate.Tag = "Admissions"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = False
        '
        'dtpAdmissionDateTime
        '
        Me.dtpAdmissionDateTime.Checked = False
        Me.dtpAdmissionDateTime.CustomFormat = "dd MMM yyyy hh:mm tt"
        Me.ebnSaveUpdate.SetDataMember(Me.dtpAdmissionDateTime, "AdmissionDateTime")
        Me.dtpAdmissionDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpAdmissionDateTime.Location = New System.Drawing.Point(160, 168)
        Me.dtpAdmissionDateTime.Name = "dtpAdmissionDateTime"
        Me.dtpAdmissionDateTime.ShowCheckBox = True
        Me.dtpAdmissionDateTime.Size = New System.Drawing.Size(174, 20)
        Me.dtpAdmissionDateTime.TabIndex = 11
        '
        'cboAdmissionStatusID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboAdmissionStatusID, "AdmissionStatus,AdmissionStatusID")
        Me.cboAdmissionStatusID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAdmissionStatusID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboAdmissionStatusID.Location = New System.Drawing.Point(128, 4)
        Me.cboAdmissionStatusID.Name = "cboAdmissionStatusID"
        Me.cboAdmissionStatusID.Size = New System.Drawing.Size(172, 21)
        Me.cboAdmissionStatusID.TabIndex = 1
        '
        'stbVisitNo
        '
        Me.stbVisitNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbVisitNo.CapitalizeFirstLetter = False
        Me.stbVisitNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ebnSaveUpdate.SetDataMember(Me.stbVisitNo, "VisitNo")
        Me.stbVisitNo.EntryErrorMSG = ""
        Me.stbVisitNo.Location = New System.Drawing.Point(160, 10)
        Me.stbVisitNo.MaxLength = 20
        Me.stbVisitNo.Name = "stbVisitNo"
        Me.stbVisitNo.RegularExpression = ""
        Me.stbVisitNo.Size = New System.Drawing.Size(124, 20)
        Me.stbVisitNo.TabIndex = 2
        '
        'stbVisitDate
        '
        Me.stbVisitDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbVisitDate.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbVisitDate, "VisitDate")
        Me.stbVisitDate.Enabled = False
        Me.stbVisitDate.EntryErrorMSG = ""
        Me.stbVisitDate.Location = New System.Drawing.Point(494, 48)
        Me.stbVisitDate.MaxLength = 60
        Me.stbVisitDate.Name = "stbVisitDate"
        Me.stbVisitDate.RegularExpression = ""
        Me.stbVisitDate.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbVisitDate.Size = New System.Drawing.Size(174, 20)
        Me.stbVisitDate.TabIndex = 37
        '
        'stbPatientNo
        '
        Me.stbPatientNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPatientNo.CapitalizeFirstLetter = False
        Me.stbPatientNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ebnSaveUpdate.SetDataMember(Me.stbPatientNo, "PatientNo")
        Me.stbPatientNo.EntryErrorMSG = ""
        Me.stbPatientNo.Location = New System.Drawing.Point(494, 27)
        Me.stbPatientNo.MaxLength = 7
        Me.stbPatientNo.Name = "stbPatientNo"
        Me.stbPatientNo.ReadOnly = True
        Me.stbPatientNo.RegularExpression = ""
        Me.stbPatientNo.Size = New System.Drawing.Size(174, 20)
        Me.stbPatientNo.TabIndex = 35
        '
        'stbServiceName
        '
        Me.stbServiceName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbServiceName.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbServiceName, "ServiceName")
        Me.stbServiceName.Enabled = False
        Me.stbServiceName.EntryErrorMSG = ""
        Me.stbServiceName.Location = New System.Drawing.Point(494, 143)
        Me.stbServiceName.MaxLength = 60
        Me.stbServiceName.Name = "stbServiceName"
        Me.stbServiceName.RegularExpression = ""
        Me.stbServiceName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbServiceName.Size = New System.Drawing.Size(174, 20)
        Me.stbServiceName.TabIndex = 45
        '
        'stbAge
        '
        Me.stbAge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAge.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbAge, "Age")
        Me.stbAge.Enabled = False
        Me.stbAge.EntryErrorMSG = ""
        Me.stbAge.Location = New System.Drawing.Point(494, 69)
        Me.stbAge.MaxLength = 60
        Me.stbAge.Name = "stbAge"
        Me.stbAge.RegularExpression = ""
        Me.stbAge.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbAge.Size = New System.Drawing.Size(63, 20)
        Me.stbAge.TabIndex = 39
        '
        'stbGender
        '
        Me.stbGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbGender.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbGender, "Gender")
        Me.stbGender.Enabled = False
        Me.stbGender.EntryErrorMSG = ""
        Me.stbGender.Location = New System.Drawing.Point(494, 90)
        Me.stbGender.MaxLength = 60
        Me.stbGender.Name = "stbGender"
        Me.stbGender.RegularExpression = ""
        Me.stbGender.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbGender.Size = New System.Drawing.Size(174, 20)
        Me.stbGender.TabIndex = 42
        '
        'stbFullName
        '
        Me.stbFullName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbFullName.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbFullName, "FullName")
        Me.stbFullName.Enabled = False
        Me.stbFullName.EntryErrorMSG = ""
        Me.stbFullName.Location = New System.Drawing.Point(494, 6)
        Me.stbFullName.MaxLength = 60
        Me.stbFullName.Name = "stbFullName"
        Me.stbFullName.RegularExpression = ""
        Me.stbFullName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbFullName.Size = New System.Drawing.Size(174, 20)
        Me.stbFullName.TabIndex = 33
        '
        'cboWardsID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboWardsID, "Wards,WardsID")
        Me.cboWardsID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWardsID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboWardsID.Location = New System.Drawing.Point(156, 12)
        Me.cboWardsID.Name = "cboWardsID"
        Me.cboWardsID.Size = New System.Drawing.Size(174, 21)
        Me.cboWardsID.TabIndex = 1
        '
        'cboRoomNo
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboRoomNo, "RoomNo")
        Me.cboRoomNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRoomNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboRoomNo.Location = New System.Drawing.Point(156, 35)
        Me.cboRoomNo.Name = "cboRoomNo"
        Me.cboRoomNo.Size = New System.Drawing.Size(174, 21)
        Me.cboRoomNo.TabIndex = 3
        '
        'cboBedNo
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboBedNo, "BedNo")
        Me.cboBedNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBedNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboBedNo.Location = New System.Drawing.Point(156, 58)
        Me.cboBedNo.Name = "cboBedNo"
        Me.cboBedNo.Size = New System.Drawing.Size(174, 21)
        Me.cboBedNo.TabIndex = 5
        '
        'stbBillCustomerName
        '
        Me.stbBillCustomerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbBillCustomerName.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbBillCustomerName, "BillCustomerName")
        Me.stbBillCustomerName.EntryErrorMSG = ""
        Me.stbBillCustomerName.Location = New System.Drawing.Point(494, 165)
        Me.stbBillCustomerName.MaxLength = 41
        Me.stbBillCustomerName.Multiline = True
        Me.stbBillCustomerName.Name = "stbBillCustomerName"
        Me.stbBillCustomerName.ReadOnly = True
        Me.stbBillCustomerName.RegularExpression = ""
        Me.stbBillCustomerName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbBillCustomerName.Size = New System.Drawing.Size(174, 32)
        Me.stbBillCustomerName.TabIndex = 47
        '
        'nbxUnitPrice
        '
        Me.nbxUnitPrice.BackColor = System.Drawing.SystemColors.Info
        Me.nbxUnitPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxUnitPrice.ControlCaption = "Unit Price"
        Me.nbxUnitPrice.DataFormat = SyncSoft.Common.Win.Controls.DisplayFormat.Standard
        Me.ebnSaveUpdate.SetDataMember(Me.nbxUnitPrice, "UnitPrice")
        Me.nbxUnitPrice.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxUnitPrice.DecimalPlaces = -1
        Me.nbxUnitPrice.Enabled = False
        Me.nbxUnitPrice.Location = New System.Drawing.Point(494, 317)
        Me.nbxUnitPrice.MaxValue = 0.0R
        Me.nbxUnitPrice.MinValue = 0.0R
        Me.nbxUnitPrice.MustEnterNumeric = True
        Me.nbxUnitPrice.Name = "nbxUnitPrice"
        Me.nbxUnitPrice.Size = New System.Drawing.Size(174, 20)
        Me.nbxUnitPrice.TabIndex = 59
        Me.nbxUnitPrice.Value = ""
        '
        'cboStaffNo
        '
        Me.cboStaffNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboStaffNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ebnSaveUpdate.SetDataMember(Me.cboStaffNo, "StaffFullName")
        Me.cboStaffNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStaffNo.DropDownWidth = 220
        Me.cboStaffNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboStaffNo.FormattingEnabled = True
        Me.cboStaffNo.Location = New System.Drawing.Point(160, 55)
        Me.cboStaffNo.Name = "cboStaffNo"
        Me.cboStaffNo.Size = New System.Drawing.Size(174, 21)
        Me.cboStaffNo.Sorted = True
        Me.cboStaffNo.TabIndex = 8
        '
        'stbChartNumber
        '
        Me.stbChartNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbChartNumber.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbChartNumber, "ChartNumber")
        Me.stbChartNumber.EntryErrorMSG = ""
        Me.stbChartNumber.Location = New System.Drawing.Point(160, 369)
        Me.stbChartNumber.MaxLength = 60
        Me.stbChartNumber.Name = "stbChartNumber"
        Me.stbChartNumber.RegularExpression = ""
        Me.stbChartNumber.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbChartNumber.Size = New System.Drawing.Size(174, 20)
        Me.stbChartNumber.TabIndex = 29
        '
        'cboAssociatedBillNo
        '
        Me.cboAssociatedBillNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboAssociatedBillNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ebnSaveUpdate.SetDataMember(Me.cboAssociatedBillNo, "AssociatedFullBillCustomer")
        Me.cboAssociatedBillNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssociatedBillNo.DropDownWidth = 230
        Me.cboAssociatedBillNo.Enabled = False
        Me.cboAssociatedBillNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboAssociatedBillNo.FormattingEnabled = True
        Me.cboAssociatedBillNo.Location = New System.Drawing.Point(160, 302)
        Me.cboAssociatedBillNo.Name = "cboAssociatedBillNo"
        Me.cboAssociatedBillNo.Size = New System.Drawing.Size(174, 21)
        Me.cboAssociatedBillNo.Sorted = True
        Me.cboAssociatedBillNo.TabIndex = 23
        Me.cboAssociatedBillNo.Tag = "AdmissionsAssociatedBillCustomer"
        '
        'cboBillModesID
        '
        Me.cboBillModesID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboBillModesID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ebnSaveUpdate.SetDataMember(Me.cboBillModesID, "BillMode,BillModesID")
        Me.cboBillModesID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBillModesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboBillModesID.FormattingEnabled = True
        Me.cboBillModesID.ItemHeight = 13
        Me.cboBillModesID.Location = New System.Drawing.Point(160, 190)
        Me.cboBillModesID.Name = "cboBillModesID"
        Me.cboBillModesID.Size = New System.Drawing.Size(174, 21)
        Me.cboBillModesID.TabIndex = 13
        Me.cboBillModesID.Tag = "AdmissionsBillMode"
        '
        'stbMainMemberName
        '
        Me.stbMainMemberName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbMainMemberName.CapitalizeFirstLetter = True
        Me.ebnSaveUpdate.SetDataMember(Me.stbMainMemberName, "MainMemberName")
        Me.stbMainMemberName.EntryErrorMSG = ""
        Me.stbMainMemberName.Location = New System.Drawing.Point(160, 258)
        Me.stbMainMemberName.MaxLength = 41
        Me.stbMainMemberName.Name = "stbMainMemberName"
        Me.stbMainMemberName.RegularExpression = ""
        Me.stbMainMemberName.Size = New System.Drawing.Size(174, 20)
        Me.stbMainMemberName.TabIndex = 19
        '
        'stbClaimReferenceNo
        '
        Me.stbClaimReferenceNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbClaimReferenceNo.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbClaimReferenceNo, "ClaimReferenceNo")
        Me.stbClaimReferenceNo.EntryErrorMSG = ""
        Me.stbClaimReferenceNo.Location = New System.Drawing.Point(160, 280)
        Me.stbClaimReferenceNo.MaxLength = 30
        Me.stbClaimReferenceNo.Name = "stbClaimReferenceNo"
        Me.stbClaimReferenceNo.RegularExpression = ""
        Me.stbClaimReferenceNo.Size = New System.Drawing.Size(174, 20)
        Me.stbClaimReferenceNo.TabIndex = 21
        '
        'cboBillNo
        '
        Me.cboBillNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboBillNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBillNo.BackColor = System.Drawing.SystemColors.Window
        Me.ebnSaveUpdate.SetDataMember(Me.cboBillNo, "BillNo")
        Me.cboBillNo.DropDownWidth = 256
        Me.cboBillNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboBillNo.FormattingEnabled = True
        Me.cboBillNo.ItemHeight = 13
        Me.cboBillNo.Location = New System.Drawing.Point(160, 213)
        Me.cboBillNo.Name = "cboBillNo"
        Me.cboBillNo.Size = New System.Drawing.Size(174, 21)
        Me.cboBillNo.TabIndex = 15
        '
        'stbMemberCardNo
        '
        Me.stbMemberCardNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbMemberCardNo.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbMemberCardNo, "MemberCardNo")
        Me.stbMemberCardNo.EntryErrorMSG = ""
        Me.stbMemberCardNo.Location = New System.Drawing.Point(160, 236)
        Me.stbMemberCardNo.MaxLength = 30
        Me.stbMemberCardNo.Name = "stbMemberCardNo"
        Me.stbMemberCardNo.RegularExpression = ""
        Me.stbMemberCardNo.Size = New System.Drawing.Size(174, 20)
        Me.stbMemberCardNo.TabIndex = 17
        '
        'stbAdmissionNotes
        '
        Me.stbAdmissionNotes.AcceptsReturn = True
        Me.stbAdmissionNotes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.stbAdmissionNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAdmissionNotes.CapitalizeFirstLetter = True
        Me.ebnSaveUpdate.SetDataMember(Me.stbAdmissionNotes, "AdmissionNotes")
        Me.stbAdmissionNotes.EntryErrorMSG = ""
        Me.stbAdmissionNotes.Location = New System.Drawing.Point(160, 391)
        Me.stbAdmissionNotes.MaxLength = 2000
        Me.stbAdmissionNotes.Multiline = True
        Me.stbAdmissionNotes.Name = "stbAdmissionNotes"
        Me.stbAdmissionNotes.RegularExpression = ""
        Me.stbAdmissionNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbAdmissionNotes.Size = New System.Drawing.Size(311, 82)
        Me.stbAdmissionNotes.TabIndex = 31
        '
        'cboCoPayTypeID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboCoPayTypeID, "CoPayType,CoPayTypeID")
        Me.cboCoPayTypeID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCoPayTypeID.Enabled = False
        Me.cboCoPayTypeID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboCoPayTypeID.Location = New System.Drawing.Point(495, 251)
        Me.cboCoPayTypeID.Name = "cboCoPayTypeID"
        Me.cboCoPayTypeID.Size = New System.Drawing.Size(174, 21)
        Me.cboCoPayTypeID.TabIndex = 53
        '
        'nbxCoPayValue
        '
        Me.nbxCoPayValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxCoPayValue.ControlCaption = "Co-Pay Value"
        Me.nbxCoPayValue.DataFormat = SyncSoft.Common.Win.Controls.DisplayFormat.Standard
        Me.ebnSaveUpdate.SetDataMember(Me.nbxCoPayValue, "CoPayValue")
        Me.nbxCoPayValue.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxCoPayValue.DecimalPlaces = 2
        Me.nbxCoPayValue.Location = New System.Drawing.Point(495, 296)
        Me.nbxCoPayValue.MaxLength = 12
        Me.nbxCoPayValue.MaxValue = 0.0R
        Me.nbxCoPayValue.MinValue = 0.0R
        Me.nbxCoPayValue.MustEnterNumeric = True
        Me.nbxCoPayValue.Name = "nbxCoPayValue"
        Me.nbxCoPayValue.ReadOnly = True
        Me.nbxCoPayValue.Size = New System.Drawing.Size(174, 20)
        Me.nbxCoPayValue.TabIndex = 57
        Me.nbxCoPayValue.Value = ""
        '
        'nbxCoPayPercent
        '
        Me.nbxCoPayPercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxCoPayPercent.ControlCaption = "Co-Pay Percent"
        Me.ebnSaveUpdate.SetDataMember(Me.nbxCoPayPercent, "CoPayPercent")
        Me.nbxCoPayPercent.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
        Me.nbxCoPayPercent.DecimalPlaces = 2
        Me.nbxCoPayPercent.Enabled = False
        Me.nbxCoPayPercent.Location = New System.Drawing.Point(495, 274)
        Me.nbxCoPayPercent.MaxLength = 3
        Me.nbxCoPayPercent.MaxValue = 100.0R
        Me.nbxCoPayPercent.MinValue = 0.0R
        Me.nbxCoPayPercent.MustEnterNumeric = True
        Me.nbxCoPayPercent.Name = "nbxCoPayPercent"
        Me.nbxCoPayPercent.Size = New System.Drawing.Size(174, 20)
        Me.nbxCoPayPercent.TabIndex = 55
        Me.nbxCoPayPercent.Value = ""
        '
        'stbInsuranceNo
        '
        Me.stbInsuranceNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbInsuranceNo.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbInsuranceNo, "InsuranceNo")
        Me.stbInsuranceNo.EntryErrorMSG = ""
        Me.stbInsuranceNo.Location = New System.Drawing.Point(494, 198)
        Me.stbInsuranceNo.MaxLength = 20
        Me.stbInsuranceNo.Name = "stbInsuranceNo"
        Me.stbInsuranceNo.ReadOnly = True
        Me.stbInsuranceNo.RegularExpression = ""
        Me.stbInsuranceNo.Size = New System.Drawing.Size(177, 20)
        Me.stbInsuranceNo.TabIndex = 49
        '
        'stbInsuranceName
        '
        Me.stbInsuranceName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbInsuranceName.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbInsuranceName, "InsuranceName")
        Me.stbInsuranceName.EntryErrorMSG = ""
        Me.stbInsuranceName.Location = New System.Drawing.Point(494, 219)
        Me.stbInsuranceName.MaxLength = 41
        Me.stbInsuranceName.Multiline = True
        Me.stbInsuranceName.Name = "stbInsuranceName"
        Me.stbInsuranceName.ReadOnly = True
        Me.stbInsuranceName.RegularExpression = ""
        Me.stbInsuranceName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbInsuranceName.Size = New System.Drawing.Size(177, 29)
        Me.stbInsuranceName.TabIndex = 51
        '
        'chkSmartCardApplicable
        '
        Me.chkSmartCardApplicable.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkSmartCardApplicable, "SmartCardApplicable")
        Me.chkSmartCardApplicable.Enabled = False
        Me.chkSmartCardApplicable.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkSmartCardApplicable.Location = New System.Drawing.Point(495, 443)
        Me.chkSmartCardApplicable.Name = "chkSmartCardApplicable"
        Me.chkSmartCardApplicable.Size = New System.Drawing.Size(152, 20)
        Me.chkSmartCardApplicable.TabIndex = 62
        Me.chkSmartCardApplicable.Text = "Smart Card Applicable"
        '
        'chkAccessCashServices
        '
        Me.chkAccessCashServices.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkAccessCashServices, "AccessCashServices")
        Me.chkAccessCashServices.Enabled = False
        Me.chkAccessCashServices.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkAccessCashServices.Location = New System.Drawing.Point(494, 467)
        Me.chkAccessCashServices.Name = "chkAccessCashServices"
        Me.chkAccessCashServices.Size = New System.Drawing.Size(152, 20)
        Me.chkAccessCashServices.TabIndex = 63
        Me.chkAccessCashServices.Text = "Access Cash Services"
        '
        'cboServiceCode
        '
        Me.cboServiceCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboServiceCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboServiceCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboServiceCode.DropDownWidth = 220
        Me.cboServiceCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboServiceCode.FormattingEnabled = True
        Me.cboServiceCode.ItemHeight = 13
        Me.cboServiceCode.Location = New System.Drawing.Point(160, 325)
        Me.cboServiceCode.Name = "cboServiceCode"
        Me.cboServiceCode.Size = New System.Drawing.Size(174, 21)
        Me.cboServiceCode.TabIndex = 25
        '
        'nbxCoverAmount
        '
        Me.nbxCoverAmount.BackColor = System.Drawing.SystemColors.Info
        Me.nbxCoverAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxCoverAmount.ControlCaption = "CoverAmount"
        Me.nbxCoverAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.nbxCoverAmount.DataFormat = SyncSoft.Common.Win.Controls.DisplayFormat.Standard
        Me.nbxCoverAmount.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxCoverAmount.DecimalPlaces = -1
        Me.nbxCoverAmount.Enabled = False
        Me.nbxCoverAmount.Location = New System.Drawing.Point(494, 417)
        Me.nbxCoverAmount.MaxValue = 0.0R
        Me.nbxCoverAmount.MinValue = 0.0R
        Me.nbxCoverAmount.MustEnterNumeric = True
        Me.nbxCoverAmount.Name = "nbxCoverAmount"
        Me.nbxCoverAmount.Size = New System.Drawing.Size(174, 20)
        Me.nbxCoverAmount.TabIndex = 61
        Me.nbxCoverAmount.Value = ""
        '
        'fbnClose
        '
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(623, 551)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 8
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'stbAdmissionNo
        '
        Me.stbAdmissionNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAdmissionNo.CapitalizeFirstLetter = False
        Me.stbAdmissionNo.EntryErrorMSG = ""
        Me.stbAdmissionNo.Location = New System.Drawing.Point(160, 32)
        Me.stbAdmissionNo.MaxLength = 20
        Me.stbAdmissionNo.Name = "stbAdmissionNo"
        Me.stbAdmissionNo.RegularExpression = ""
        Me.stbAdmissionNo.Size = New System.Drawing.Size(174, 20)
        Me.stbAdmissionNo.TabIndex = 6
        '
        'lblAdmissionNo
        '
        Me.lblAdmissionNo.Location = New System.Drawing.Point(8, 30)
        Me.lblAdmissionNo.Name = "lblAdmissionNo"
        Me.lblAdmissionNo.Size = New System.Drawing.Size(96, 19)
        Me.lblAdmissionNo.TabIndex = 4
        Me.lblAdmissionNo.Text = "Admission No"
        '
        'lblAdmissionDateTime
        '
        Me.lblAdmissionDateTime.Location = New System.Drawing.Point(4, 169)
        Me.lblAdmissionDateTime.Name = "lblAdmissionDateTime"
        Me.lblAdmissionDateTime.Size = New System.Drawing.Size(140, 20)
        Me.lblAdmissionDateTime.TabIndex = 10
        Me.lblAdmissionDateTime.Text = "Admission Date Time "
        '
        'lblAdmissionStatusID
        '
        Me.lblAdmissionStatusID.Location = New System.Drawing.Point(5, 4)
        Me.lblAdmissionStatusID.Name = "lblAdmissionStatusID"
        Me.lblAdmissionStatusID.Size = New System.Drawing.Size(114, 20)
        Me.lblAdmissionStatusID.TabIndex = 0
        Me.lblAdmissionStatusID.Text = "Admission Status"
        '
        'lblPatientsNo
        '
        Me.lblPatientsNo.Location = New System.Drawing.Point(366, 27)
        Me.lblPatientsNo.Name = "lblPatientsNo"
        Me.lblPatientsNo.Size = New System.Drawing.Size(119, 20)
        Me.lblPatientsNo.TabIndex = 34
        Me.lblPatientsNo.Text = "Patient's No."
        '
        'lblServiceName
        '
        Me.lblServiceName.Location = New System.Drawing.Point(366, 145)
        Me.lblServiceName.Name = "lblServiceName"
        Me.lblServiceName.Size = New System.Drawing.Size(119, 20)
        Me.lblServiceName.TabIndex = 44
        Me.lblServiceName.Text = "To-Bill Service"
        '
        'lblAge
        '
        Me.lblAge.Location = New System.Drawing.Point(366, 68)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(119, 20)
        Me.lblAge.TabIndex = 38
        Me.lblAge.Text = "Age"
        '
        'lblGenderID
        '
        Me.lblGenderID.Location = New System.Drawing.Point(366, 89)
        Me.lblGenderID.Name = "lblGenderID"
        Me.lblGenderID.Size = New System.Drawing.Size(119, 20)
        Me.lblGenderID.TabIndex = 41
        Me.lblGenderID.Text = "Gender"
        '
        'lblVisitDate
        '
        Me.lblVisitDate.Location = New System.Drawing.Point(366, 47)
        Me.lblVisitDate.Name = "lblVisitDate"
        Me.lblVisitDate.Size = New System.Drawing.Size(119, 20)
        Me.lblVisitDate.TabIndex = 36
        Me.lblVisitDate.Text = "Visit Date"
        '
        'lblFullName
        '
        Me.lblFullName.Location = New System.Drawing.Point(366, 6)
        Me.lblFullName.Name = "lblFullName"
        Me.lblFullName.Size = New System.Drawing.Size(119, 20)
        Me.lblFullName.TabIndex = 32
        Me.lblFullName.Text = "Patient's Name"
        '
        'btnFindVisitNo
        '
        Me.btnFindVisitNo.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnFindVisitNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFindVisitNo.Image = CType(resources.GetObject("btnFindVisitNo.Image"), System.Drawing.Image)
        Me.btnFindVisitNo.Location = New System.Drawing.Point(128, 7)
        Me.btnFindVisitNo.Name = "btnFindVisitNo"
        Me.btnFindVisitNo.Size = New System.Drawing.Size(27, 21)
        Me.btnFindVisitNo.TabIndex = 1
        '
        'lblVisitNo
        '
        Me.lblVisitNo.Location = New System.Drawing.Point(8, 8)
        Me.lblVisitNo.Name = "lblVisitNo"
        Me.lblVisitNo.Size = New System.Drawing.Size(112, 20)
        Me.lblVisitNo.TabIndex = 0
        Me.lblVisitNo.Text = "Visit No"
        '
        'pnlAdmissionStatusID
        '
        Me.pnlAdmissionStatusID.Controls.Add(Me.cboAdmissionStatusID)
        Me.pnlAdmissionStatusID.Controls.Add(Me.lblAdmissionStatusID)
        Me.pnlAdmissionStatusID.Enabled = False
        Me.pnlAdmissionStatusID.Location = New System.Drawing.Point(366, 112)
        Me.pnlAdmissionStatusID.Name = "pnlAdmissionStatusID"
        Me.pnlAdmissionStatusID.Size = New System.Drawing.Size(305, 28)
        Me.pnlAdmissionStatusID.TabIndex = 43
        '
        'btnFindAdmissionNo
        '
        Me.btnFindAdmissionNo.Enabled = False
        Me.btnFindAdmissionNo.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnFindAdmissionNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFindAdmissionNo.Image = CType(resources.GetObject("btnFindAdmissionNo.Image"), System.Drawing.Image)
        Me.btnFindAdmissionNo.Location = New System.Drawing.Point(128, 32)
        Me.btnFindAdmissionNo.Name = "btnFindAdmissionNo"
        Me.btnFindAdmissionNo.Size = New System.Drawing.Size(27, 21)
        Me.btnFindAdmissionNo.TabIndex = 5
        '
        'grpLocation
        '
        Me.grpLocation.Controls.Add(Me.cboBedNo)
        Me.grpLocation.Controls.Add(Me.lblBedNo)
        Me.grpLocation.Controls.Add(Me.cboWardsID)
        Me.grpLocation.Controls.Add(Me.lblWardsID)
        Me.grpLocation.Controls.Add(Me.cboRoomNo)
        Me.grpLocation.Controls.Add(Me.lblRoomNo)
        Me.grpLocation.Location = New System.Drawing.Point(4, 79)
        Me.grpLocation.Name = "grpLocation"
        Me.grpLocation.Size = New System.Drawing.Size(338, 87)
        Me.grpLocation.TabIndex = 9
        Me.grpLocation.TabStop = False
        Me.grpLocation.Text = "Location"
        '
        'lblBedNo
        '
        Me.lblBedNo.Location = New System.Drawing.Point(4, 61)
        Me.lblBedNo.Name = "lblBedNo"
        Me.lblBedNo.Size = New System.Drawing.Size(140, 20)
        Me.lblBedNo.TabIndex = 4
        Me.lblBedNo.Text = "Bed No"
        '
        'lblWardsID
        '
        Me.lblWardsID.Location = New System.Drawing.Point(4, 15)
        Me.lblWardsID.Name = "lblWardsID"
        Me.lblWardsID.Size = New System.Drawing.Size(140, 20)
        Me.lblWardsID.TabIndex = 0
        Me.lblWardsID.Text = "Ward"
        '
        'lblRoomNo
        '
        Me.lblRoomNo.Location = New System.Drawing.Point(4, 38)
        Me.lblRoomNo.Name = "lblRoomNo"
        Me.lblRoomNo.Size = New System.Drawing.Size(140, 20)
        Me.lblRoomNo.TabIndex = 2
        Me.lblRoomNo.Text = "Room No"
        '
        'lblBillCustomerName
        '
        Me.lblBillCustomerName.Location = New System.Drawing.Point(366, 167)
        Me.lblBillCustomerName.Name = "lblBillCustomerName"
        Me.lblBillCustomerName.Size = New System.Drawing.Size(119, 20)
        Me.lblBillCustomerName.TabIndex = 46
        Me.lblBillCustomerName.Text = "To-Bill Customer"
        '
        'btnLoadPeriodicVisits
        '
        Me.btnLoadPeriodicVisits.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnLoadPeriodicVisits.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoadPeriodicVisits.Location = New System.Drawing.Point(288, 7)
        Me.btnLoadPeriodicVisits.Name = "btnLoadPeriodicVisits"
        Me.btnLoadPeriodicVisits.Size = New System.Drawing.Size(44, 24)
        Me.btnLoadPeriodicVisits.TabIndex = 3
        Me.btnLoadPeriodicVisits.Tag = ""
        Me.btnLoadPeriodicVisits.Text = "&Load"
        '
        'lblStaff
        '
        Me.lblStaff.Location = New System.Drawing.Point(8, 56)
        Me.lblStaff.Name = "lblStaff"
        Me.lblStaff.Size = New System.Drawing.Size(140, 20)
        Me.lblStaff.TabIndex = 7
        Me.lblStaff.Text = "Attending Doctor (Staff)"
        '
        'lblUnitPrice
        '
        Me.lblUnitPrice.Location = New System.Drawing.Point(366, 319)
        Me.lblUnitPrice.Name = "lblUnitPrice"
        Me.lblUnitPrice.Size = New System.Drawing.Size(119, 20)
        Me.lblUnitPrice.TabIndex = 58
        Me.lblUnitPrice.Text = "Unit Price"
        '
        'lblChatNo
        '
        Me.lblChatNo.Location = New System.Drawing.Point(4, 369)
        Me.lblChatNo.Name = "lblChatNo"
        Me.lblChatNo.Size = New System.Drawing.Size(140, 20)
        Me.lblChatNo.TabIndex = 28
        Me.lblChatNo.Text = "Chart Number"
        '
        'imgIDAutomation
        '
        Me.imgIDAutomation.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.imgIDAutomation.Location = New System.Drawing.Point(479, 339)
        Me.imgIDAutomation.Name = "imgIDAutomation"
        Me.imgIDAutomation.Size = New System.Drawing.Size(192, 53)
        Me.imgIDAutomation.TabIndex = 102
        Me.imgIDAutomation.TabStop = False
        '
        'btnPrintBarcode
        '
        Me.btnPrintBarcode.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnPrintBarcode.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrintBarcode.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnPrintBarcode.Location = New System.Drawing.Point(501, 550)
        Me.btnPrintBarcode.Name = "btnPrintBarcode"
        Me.btnPrintBarcode.Size = New System.Drawing.Size(119, 24)
        Me.btnPrintBarcode.TabIndex = 6
        Me.btnPrintBarcode.Text = "&Print Patient Sticker"
        '
        'chkPrintAdmissionForm
        '
        Me.chkPrintAdmissionForm.AutoSize = True
        Me.chkPrintAdmissionForm.CheckAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.chkPrintAdmissionForm.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkPrintAdmissionForm.Location = New System.Drawing.Point(91, 554)
        Me.chkPrintAdmissionForm.Name = "chkPrintAdmissionForm"
        Me.chkPrintAdmissionForm.Size = New System.Drawing.Size(120, 17)
        Me.chkPrintAdmissionForm.TabIndex = 4
        Me.chkPrintAdmissionForm.Text = "Print Admission Form"
        Me.chkPrintAdmissionForm.UseVisualStyleBackColor = True
        '
        'lblAssociatedBillNo
        '
        Me.lblAssociatedBillNo.Enabled = False
        Me.lblAssociatedBillNo.Location = New System.Drawing.Point(4, 301)
        Me.lblAssociatedBillNo.Name = "lblAssociatedBillNo"
        Me.lblAssociatedBillNo.Size = New System.Drawing.Size(140, 20)
        Me.lblAssociatedBillNo.TabIndex = 22
        Me.lblAssociatedBillNo.Text = "Associated Bill Customer"
        '
        'lblBillMode
        '
        Me.lblBillMode.Location = New System.Drawing.Point(4, 191)
        Me.lblBillMode.Name = "lblBillMode"
        Me.lblBillMode.Size = New System.Drawing.Size(140, 20)
        Me.lblBillMode.TabIndex = 12
        Me.lblBillMode.Text = "To-Bill Mode"
        '
        'lblMainMemberName
        '
        Me.lblMainMemberName.Location = New System.Drawing.Point(4, 257)
        Me.lblMainMemberName.Name = "lblMainMemberName"
        Me.lblMainMemberName.Size = New System.Drawing.Size(140, 20)
        Me.lblMainMemberName.TabIndex = 18
        Me.lblMainMemberName.Text = "Main Member Name"
        '
        'lblBillNo
        '
        Me.lblBillNo.Location = New System.Drawing.Point(4, 213)
        Me.lblBillNo.Name = "lblBillNo"
        Me.lblBillNo.Size = New System.Drawing.Size(140, 20)
        Me.lblBillNo.TabIndex = 14
        Me.lblBillNo.Text = "To-Bill Number"
        '
        'lblClaimReferenceNo
        '
        Me.lblClaimReferenceNo.Location = New System.Drawing.Point(4, 279)
        Me.lblClaimReferenceNo.Name = "lblClaimReferenceNo"
        Me.lblClaimReferenceNo.Size = New System.Drawing.Size(140, 20)
        Me.lblClaimReferenceNo.TabIndex = 20
        Me.lblClaimReferenceNo.Text = "Claim Reference No"
        '
        'lblMemberCardNo
        '
        Me.lblMemberCardNo.Location = New System.Drawing.Point(4, 235)
        Me.lblMemberCardNo.Name = "lblMemberCardNo"
        Me.lblMemberCardNo.Size = New System.Drawing.Size(140, 20)
        Me.lblMemberCardNo.TabIndex = 16
        Me.lblMemberCardNo.Text = "Member Card No"
        '
        'lblAdmissionNotes
        '
        Me.lblAdmissionNotes.Location = New System.Drawing.Point(8, 392)
        Me.lblAdmissionNotes.Name = "lblAdmissionNotes"
        Me.lblAdmissionNotes.Size = New System.Drawing.Size(144, 20)
        Me.lblAdmissionNotes.TabIndex = 30
        Me.lblAdmissionNotes.Text = "Admission Notes"
        '
        'lblCoPayPercent
        '
        Me.lblCoPayPercent.Location = New System.Drawing.Point(366, 276)
        Me.lblCoPayPercent.Name = "lblCoPayPercent"
        Me.lblCoPayPercent.Size = New System.Drawing.Size(125, 20)
        Me.lblCoPayPercent.TabIndex = 54
        Me.lblCoPayPercent.Text = "Co-Pay Percent"
        '
        'lblCoPayValue
        '
        Me.lblCoPayValue.Location = New System.Drawing.Point(367, 298)
        Me.lblCoPayValue.Name = "lblCoPayValue"
        Me.lblCoPayValue.Size = New System.Drawing.Size(118, 20)
        Me.lblCoPayValue.TabIndex = 56
        Me.lblCoPayValue.Text = "Co-Pay Value"
        '
        'lblCoPayType
        '
        Me.lblCoPayType.Location = New System.Drawing.Point(367, 249)
        Me.lblCoPayType.Name = "lblCoPayType"
        Me.lblCoPayType.Size = New System.Drawing.Size(118, 20)
        Me.lblCoPayType.TabIndex = 52
        Me.lblCoPayType.Text = "Co-Pay Type"
        '
        'lblInsuranceNo
        '
        Me.lblInsuranceNo.Location = New System.Drawing.Point(366, 199)
        Me.lblInsuranceNo.Name = "lblInsuranceNo"
        Me.lblInsuranceNo.Size = New System.Drawing.Size(119, 20)
        Me.lblInsuranceNo.TabIndex = 48
        Me.lblInsuranceNo.Text = "Insurance No"
        '
        'lblBillInsuranceName
        '
        Me.lblBillInsuranceName.Location = New System.Drawing.Point(367, 222)
        Me.lblBillInsuranceName.Name = "lblBillInsuranceName"
        Me.lblBillInsuranceName.Size = New System.Drawing.Size(134, 20)
        Me.lblBillInsuranceName.TabIndex = 50
        Me.lblBillInsuranceName.Text = "To-Bill Insurance Name"
        '
        'lblAgeString
        '
        Me.lblAgeString.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAgeString.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblAgeString.Location = New System.Drawing.Point(559, 69)
        Me.lblAgeString.Name = "lblAgeString"
        Me.lblAgeString.Size = New System.Drawing.Size(120, 19)
        Me.lblAgeString.TabIndex = 40
        '
        'chkPrintAdmissionConsent
        '
        Me.chkPrintAdmissionConsent.AutoSize = True
        Me.chkPrintAdmissionConsent.CheckAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.chkPrintAdmissionConsent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkPrintAdmissionConsent.Location = New System.Drawing.Point(217, 555)
        Me.chkPrintAdmissionConsent.Name = "chkPrintAdmissionConsent"
        Me.chkPrintAdmissionConsent.Size = New System.Drawing.Size(136, 17)
        Me.chkPrintAdmissionConsent.TabIndex = 5
        Me.chkPrintAdmissionConsent.Text = "Print Admission Consent"
        Me.chkPrintAdmissionConsent.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.chkPrintAdmissionConsent.UseVisualStyleBackColor = True
        '
        'lblCoverAmount
        '
        Me.lblCoverAmount.Location = New System.Drawing.Point(492, 395)
        Me.lblCoverAmount.Name = "lblCoverAmount"
        Me.lblCoverAmount.Size = New System.Drawing.Size(119, 20)
        Me.lblCoverAmount.TabIndex = 60
        Me.lblCoverAmount.Text = "Cover Amount"
        '
        'chkPrintAdmissionFaceSheet
        '
        Me.chkPrintAdmissionFaceSheet.AutoSize = True
        Me.chkPrintAdmissionFaceSheet.CheckAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.chkPrintAdmissionFaceSheet.Checked = True
        Me.chkPrintAdmissionFaceSheet.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPrintAdmissionFaceSheet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkPrintAdmissionFaceSheet.Location = New System.Drawing.Point(91, 531)
        Me.chkPrintAdmissionFaceSheet.Name = "chkPrintAdmissionFaceSheet"
        Me.chkPrintAdmissionFaceSheet.Size = New System.Drawing.Size(152, 17)
        Me.chkPrintAdmissionFaceSheet.TabIndex = 3
        Me.chkPrintAdmissionFaceSheet.Text = "Print Admission Face Sheet"
        Me.chkPrintAdmissionFaceSheet.UseVisualStyleBackColor = True
        Me.chkPrintAdmissionFaceSheet.Visible = False
        '
        'nbxToBillServiceFee
        '
        Me.nbxToBillServiceFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxToBillServiceFee.ControlCaption = "Consultation Fee"
        Me.nbxToBillServiceFee.DataFormat = SyncSoft.Common.Win.Controls.DisplayFormat.Standard
        Me.nbxToBillServiceFee.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxToBillServiceFee.DecimalPlaces = -1
        Me.nbxToBillServiceFee.Location = New System.Drawing.Point(160, 348)
        Me.nbxToBillServiceFee.MaxValue = 0.0R
        Me.nbxToBillServiceFee.MinValue = 0.0R
        Me.nbxToBillServiceFee.MustEnterNumeric = True
        Me.nbxToBillServiceFee.Name = "nbxToBillServiceFee"
        Me.nbxToBillServiceFee.Size = New System.Drawing.Size(174, 20)
        Me.nbxToBillServiceFee.TabIndex = 27
        Me.nbxToBillServiceFee.Value = ""
        '
        'lblServiceCode
        '
        Me.lblServiceCode.Location = New System.Drawing.Point(4, 323)
        Me.lblServiceCode.Name = "lblServiceCode"
        Me.lblServiceCode.Size = New System.Drawing.Size(140, 20)
        Me.lblServiceCode.TabIndex = 24
        Me.lblServiceCode.Text = "To-Bill Service"
        '
        'lblServiceFee
        '
        Me.lblServiceFee.Location = New System.Drawing.Point(4, 345)
        Me.lblServiceFee.Name = "lblServiceFee"
        Me.lblServiceFee.Size = New System.Drawing.Size(140, 20)
        Me.lblServiceFee.TabIndex = 26
        Me.lblServiceFee.Text = "To-Bill Service Fee"
        '
        'tbcAdmissions
        '
        Me.tbcAdmissions.Controls.Add(Me.tpgGeneral)
        Me.tbcAdmissions.Controls.Add(Me.tpgDiagnosis)
        Me.tbcAdmissions.HotTrack = True
        Me.tbcAdmissions.Location = New System.Drawing.Point(5, 5)
        Me.tbcAdmissions.Name = "tbcAdmissions"
        Me.tbcAdmissions.SelectedIndex = 0
        Me.tbcAdmissions.Size = New System.Drawing.Size(698, 515)
        Me.tbcAdmissions.TabIndex = 0
        '
        'tpgGeneral
        '
        Me.tpgGeneral.Controls.Add(Me.stbVisitNo)
        Me.tpgGeneral.Controls.Add(Me.nbxCoverAmount)
        Me.tpgGeneral.Controls.Add(Me.lblAdmissionDateTime)
        Me.tpgGeneral.Controls.Add(Me.lblCoverAmount)
        Me.tpgGeneral.Controls.Add(Me.dtpAdmissionDateTime)
        Me.tpgGeneral.Controls.Add(Me.lblAgeString)
        Me.tpgGeneral.Controls.Add(Me.chkSmartCardApplicable)
        Me.tpgGeneral.Controls.Add(Me.lblAdmissionNo)
        Me.tpgGeneral.Controls.Add(Me.chkAccessCashServices)
        Me.tpgGeneral.Controls.Add(Me.nbxToBillServiceFee)
        Me.tpgGeneral.Controls.Add(Me.stbInsuranceNo)
        Me.tpgGeneral.Controls.Add(Me.stbAdmissionNo)
        Me.tpgGeneral.Controls.Add(Me.lblInsuranceNo)
        Me.tpgGeneral.Controls.Add(Me.lblServiceCode)
        Me.tpgGeneral.Controls.Add(Me.stbInsuranceName)
        Me.tpgGeneral.Controls.Add(Me.lblVisitNo)
        Me.tpgGeneral.Controls.Add(Me.lblBillInsuranceName)
        Me.tpgGeneral.Controls.Add(Me.cboServiceCode)
        Me.tpgGeneral.Controls.Add(Me.nbxCoPayPercent)
        Me.tpgGeneral.Controls.Add(Me.btnFindVisitNo)
        Me.tpgGeneral.Controls.Add(Me.cboCoPayTypeID)
        Me.tpgGeneral.Controls.Add(Me.lblServiceFee)
        Me.tpgGeneral.Controls.Add(Me.lblCoPayPercent)
        Me.tpgGeneral.Controls.Add(Me.btnFindAdmissionNo)
        Me.tpgGeneral.Controls.Add(Me.nbxCoPayValue)
        Me.tpgGeneral.Controls.Add(Me.grpLocation)
        Me.tpgGeneral.Controls.Add(Me.lblCoPayValue)
        Me.tpgGeneral.Controls.Add(Me.lblStaff)
        Me.tpgGeneral.Controls.Add(Me.lblCoPayType)
        Me.tpgGeneral.Controls.Add(Me.cboStaffNo)
        Me.tpgGeneral.Controls.Add(Me.lblChatNo)
        Me.tpgGeneral.Controls.Add(Me.imgIDAutomation)
        Me.tpgGeneral.Controls.Add(Me.stbChartNumber)
        Me.tpgGeneral.Controls.Add(Me.nbxUnitPrice)
        Me.tpgGeneral.Controls.Add(Me.lblMemberCardNo)
        Me.tpgGeneral.Controls.Add(Me.lblUnitPrice)
        Me.tpgGeneral.Controls.Add(Me.stbMemberCardNo)
        Me.tpgGeneral.Controls.Add(Me.btnLoadPeriodicVisits)
        Me.tpgGeneral.Controls.Add(Me.cboBillNo)
        Me.tpgGeneral.Controls.Add(Me.stbBillCustomerName)
        Me.tpgGeneral.Controls.Add(Me.lblClaimReferenceNo)
        Me.tpgGeneral.Controls.Add(Me.lblBillCustomerName)
        Me.tpgGeneral.Controls.Add(Me.stbClaimReferenceNo)
        Me.tpgGeneral.Controls.Add(Me.pnlAdmissionStatusID)
        Me.tpgGeneral.Controls.Add(Me.lblBillNo)
        Me.tpgGeneral.Controls.Add(Me.stbVisitDate)
        Me.tpgGeneral.Controls.Add(Me.lblMainMemberName)
        Me.tpgGeneral.Controls.Add(Me.stbPatientNo)
        Me.tpgGeneral.Controls.Add(Me.stbMainMemberName)
        Me.tpgGeneral.Controls.Add(Me.lblPatientsNo)
        Me.tpgGeneral.Controls.Add(Me.cboBillModesID)
        Me.tpgGeneral.Controls.Add(Me.stbServiceName)
        Me.tpgGeneral.Controls.Add(Me.lblBillMode)
        Me.tpgGeneral.Controls.Add(Me.lblServiceName)
        Me.tpgGeneral.Controls.Add(Me.lblAssociatedBillNo)
        Me.tpgGeneral.Controls.Add(Me.stbAge)
        Me.tpgGeneral.Controls.Add(Me.cboAssociatedBillNo)
        Me.tpgGeneral.Controls.Add(Me.stbGender)
        Me.tpgGeneral.Controls.Add(Me.stbAdmissionNotes)
        Me.tpgGeneral.Controls.Add(Me.lblAge)
        Me.tpgGeneral.Controls.Add(Me.lblAdmissionNotes)
        Me.tpgGeneral.Controls.Add(Me.lblGenderID)
        Me.tpgGeneral.Controls.Add(Me.lblFullName)
        Me.tpgGeneral.Controls.Add(Me.lblVisitDate)
        Me.tpgGeneral.Controls.Add(Me.stbFullName)
        Me.tpgGeneral.Location = New System.Drawing.Point(4, 22)
        Me.tpgGeneral.Name = "tpgGeneral"
        Me.tpgGeneral.Size = New System.Drawing.Size(690, 489)
        Me.tpgGeneral.TabIndex = 4
        Me.tpgGeneral.Tag = "Patients"
        Me.tpgGeneral.Text = "General"
        Me.tpgGeneral.UseVisualStyleBackColor = True
        '
        'tpgDiagnosis
        '
        Me.tpgDiagnosis.Controls.Add(Me.dgvDiagnosis)
        Me.tpgDiagnosis.Location = New System.Drawing.Point(4, 22)
        Me.tpgDiagnosis.Name = "tpgDiagnosis"
        Me.tpgDiagnosis.Size = New System.Drawing.Size(690, 489)
        Me.tpgDiagnosis.TabIndex = 5
        Me.tpgDiagnosis.Tag = "Diagnosis"
        Me.tpgDiagnosis.Text = "Diagnosis"
        Me.tpgDiagnosis.UseVisualStyleBackColor = True
        '
        'dgvDiagnosis
        '
        Me.dgvDiagnosis.AllowUserToOrderColumns = True
        Me.dgvDiagnosis.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDiagnosis.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDiagnosis.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColDiagnosisSelect, Me.colICDDiagnosisCode, Me.colDiseaseCode, Me.ColDiagnosedBy, Me.colDiseaseCategory, Me.colNotes, Me.colDiagnosisSaved})
        Me.dgvDiagnosis.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDiagnosis.EnableHeadersVisualStyles = False
        Me.dgvDiagnosis.GridColor = System.Drawing.Color.Khaki
        Me.dgvDiagnosis.Location = New System.Drawing.Point(0, 0)
        Me.dgvDiagnosis.Name = "dgvDiagnosis"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDiagnosis.RowHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvDiagnosis.Size = New System.Drawing.Size(690, 489)
        Me.dgvDiagnosis.TabIndex = 2
        Me.dgvDiagnosis.Text = "DataGridView1"
        '
        'ColDiagnosisSelect
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Firebrick
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.DarkBlue
        Me.ColDiagnosisSelect.DefaultCellStyle = DataGridViewCellStyle2
        Me.ColDiagnosisSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ColDiagnosisSelect.HeaderText = "Select"
        Me.ColDiagnosisSelect.Name = "ColDiagnosisSelect"
        Me.ColDiagnosisSelect.ReadOnly = True
        Me.ColDiagnosisSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ColDiagnosisSelect.Text = "���"
        Me.ColDiagnosisSelect.UseColumnTextForButtonValue = True
        Me.ColDiagnosisSelect.Width = 50
        '
        'colICDDiagnosisCode
        '
        Me.colICDDiagnosisCode.HeaderText = "Code"
        Me.colICDDiagnosisCode.Name = "colICDDiagnosisCode"
        Me.colICDDiagnosisCode.Width = 50
        '
        'colDiseaseCode
        '
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Info
        Me.colDiseaseCode.DefaultCellStyle = DataGridViewCellStyle3
        Me.colDiseaseCode.HeaderText = "Diagnosis"
        Me.colDiseaseCode.Name = "colDiseaseCode"
        Me.colDiseaseCode.ReadOnly = True
        Me.colDiseaseCode.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colDiseaseCode.Width = 150
        '
        'ColDiagnosedBy
        '
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Info
        Me.ColDiagnosedBy.DefaultCellStyle = DataGridViewCellStyle4
        Me.ColDiagnosedBy.HeaderText = "Diagnosed By"
        Me.ColDiagnosedBy.Name = "ColDiagnosedBy"
        Me.ColDiagnosedBy.ReadOnly = True
        '
        'colDiseaseCategory
        '
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Info
        Me.colDiseaseCategory.DefaultCellStyle = DataGridViewCellStyle5
        Me.colDiseaseCategory.HeaderText = "Category"
        Me.colDiseaseCategory.Name = "colDiseaseCategory"
        Me.colDiseaseCategory.ReadOnly = True
        '
        'colNotes
        '
        Me.colNotes.HeaderText = "Notes"
        Me.colNotes.MaxInputLength = 100
        Me.colNotes.Name = "colNotes"
        Me.colNotes.Width = 120
        '
        'colDiagnosisSaved
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle6.NullValue = False
        Me.colDiagnosisSaved.DefaultCellStyle = DataGridViewCellStyle6
        Me.colDiagnosisSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colDiagnosisSaved.HeaderText = "Saved"
        Me.colDiagnosisSaved.Name = "colDiagnosisSaved"
        Me.colDiagnosisSaved.ReadOnly = True
        Me.colDiagnosisSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colDiagnosisSaved.Width = 50
        '
        'btnFaceSheet
        '
        Me.btnFaceSheet.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnFaceSheet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFaceSheet.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnFaceSheet.Location = New System.Drawing.Point(375, 550)
        Me.btnFaceSheet.Name = "btnFaceSheet"
        Me.btnFaceSheet.Size = New System.Drawing.Size(119, 24)
        Me.btnFaceSheet.TabIndex = 9
        Me.btnFaceSheet.Text = "&Print FaceSheet"
        '
        'btnAdmConsent
        '
        Me.btnAdmConsent.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnAdmConsent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdmConsent.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnAdmConsent.Location = New System.Drawing.Point(501, 524)
        Me.btnAdmConsent.Name = "btnAdmConsent"
        Me.btnAdmConsent.Size = New System.Drawing.Size(119, 24)
        Me.btnAdmConsent.TabIndex = 10
        Me.btnAdmConsent.Text = "&Print Adm. Consent"
        '
        'frmAdmissions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(704, 579)
        Me.Controls.Add(Me.btnAdmConsent)
        Me.Controls.Add(Me.btnFaceSheet)
        Me.Controls.Add(Me.tbcAdmissions)
        Me.Controls.Add(Me.chkPrintAdmissionFaceSheet)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.chkPrintAdmissionConsent)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.fbnSearch)
        Me.Controls.Add(Me.chkPrintAdmissionForm)
        Me.Controls.Add(Me.btnPrintBarcode)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAdmissions"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Admissions"
        Me.pnlAdmissionStatusID.ResumeLayout(False)
        Me.grpLocation.ResumeLayout(False)
        CType(Me.imgIDAutomation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbcAdmissions.ResumeLayout(False)
        Me.tpgGeneral.ResumeLayout(False)
        Me.tpgGeneral.PerformLayout()
        Me.tpgDiagnosis.ResumeLayout(False)
        CType(Me.dgvDiagnosis, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents stbAdmissionNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAdmissionNo As System.Windows.Forms.Label
    Friend WithEvents dtpAdmissionDateTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAdmissionDateTime As System.Windows.Forms.Label
    Friend WithEvents cboAdmissionStatusID As System.Windows.Forms.ComboBox
    Friend WithEvents lblAdmissionStatusID As System.Windows.Forms.Label
    Friend WithEvents stbVisitDate As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents stbPatientNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblPatientsNo As System.Windows.Forms.Label
    Friend WithEvents stbServiceName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblServiceName As System.Windows.Forms.Label
    Friend WithEvents stbAge As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents stbGender As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAge As System.Windows.Forms.Label
    Friend WithEvents lblGenderID As System.Windows.Forms.Label
    Friend WithEvents lblVisitDate As System.Windows.Forms.Label
    Friend WithEvents stbFullName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblFullName As System.Windows.Forms.Label
    Friend WithEvents btnFindVisitNo As System.Windows.Forms.Button
    Friend WithEvents stbVisitNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblVisitNo As System.Windows.Forms.Label
    Friend WithEvents pnlAdmissionStatusID As System.Windows.Forms.Panel
    Friend WithEvents btnFindAdmissionNo As System.Windows.Forms.Button
    Friend WithEvents grpLocation As System.Windows.Forms.GroupBox
    Friend WithEvents cboWardsID As System.Windows.Forms.ComboBox
    Friend WithEvents lblWardsID As System.Windows.Forms.Label
    Friend WithEvents cboRoomNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblRoomNo As System.Windows.Forms.Label
    Friend WithEvents lblBedNo As System.Windows.Forms.Label
    Friend WithEvents cboBedNo As System.Windows.Forms.ComboBox
    Friend WithEvents stbBillCustomerName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblBillCustomerName As System.Windows.Forms.Label
    Friend WithEvents btnLoadPeriodicVisits As System.Windows.Forms.Button
    Friend WithEvents cboStaffNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblStaff As System.Windows.Forms.Label
    Friend WithEvents nbxUnitPrice As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblUnitPrice As System.Windows.Forms.Label
    Friend WithEvents stbChartNumber As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblChatNo As System.Windows.Forms.Label
    Friend WithEvents imgIDAutomation As System.Windows.Forms.PictureBox
    Friend WithEvents btnPrintBarcode As System.Windows.Forms.Button
    Friend WithEvents chkPrintAdmissionForm As System.Windows.Forms.CheckBox
    Friend WithEvents cboAssociatedBillNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblAssociatedBillNo As System.Windows.Forms.Label
    Friend WithEvents lblBillMode As System.Windows.Forms.Label
    Friend WithEvents cboBillModesID As System.Windows.Forms.ComboBox
    Friend WithEvents stbMainMemberName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblMainMemberName As System.Windows.Forms.Label
    Friend WithEvents lblBillNo As System.Windows.Forms.Label
    Friend WithEvents stbClaimReferenceNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblClaimReferenceNo As System.Windows.Forms.Label
    Friend WithEvents cboBillNo As System.Windows.Forms.ComboBox
    Friend WithEvents stbMemberCardNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblMemberCardNo As System.Windows.Forms.Label
    Friend WithEvents stbAdmissionNotes As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAdmissionNotes As System.Windows.Forms.Label
    Friend WithEvents cboCoPayTypeID As System.Windows.Forms.ComboBox
    Friend WithEvents lblCoPayPercent As System.Windows.Forms.Label
    Friend WithEvents nbxCoPayValue As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblCoPayValue As System.Windows.Forms.Label
    Friend WithEvents lblCoPayType As System.Windows.Forms.Label
    Friend WithEvents nbxCoPayPercent As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents stbInsuranceNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblInsuranceNo As System.Windows.Forms.Label
    Friend WithEvents stbInsuranceName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblBillInsuranceName As System.Windows.Forms.Label
    Friend WithEvents chkSmartCardApplicable As System.Windows.Forms.CheckBox
    Friend WithEvents chkAccessCashServices As System.Windows.Forms.CheckBox
    Friend WithEvents lblAgeString As System.Windows.Forms.Label
    Friend WithEvents chkPrintAdmissionConsent As System.Windows.Forms.CheckBox
    Friend WithEvents nbxCoverAmount As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblCoverAmount As System.Windows.Forms.Label
    Friend WithEvents chkPrintAdmissionFaceSheet As System.Windows.Forms.CheckBox
    Friend WithEvents nbxToBillServiceFee As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblServiceCode As System.Windows.Forms.Label
    Friend WithEvents cboServiceCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblServiceFee As System.Windows.Forms.Label
    Friend WithEvents tbcAdmissions As System.Windows.Forms.TabControl
    Friend WithEvents tpgGeneral As System.Windows.Forms.TabPage
    Friend WithEvents tpgDiagnosis As System.Windows.Forms.TabPage


    Friend WithEvents dgvDiagnosis As System.Windows.Forms.DataGridView
    Friend WithEvents ColDiagnosisSelect As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents colICDDiagnosisCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDiseaseCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColDiagnosedBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDiseaseCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNotes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDiagnosisSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents btnFaceSheet As System.Windows.Forms.Button
    Friend WithEvents btnAdmConsent As System.Windows.Forms.Button

End Class