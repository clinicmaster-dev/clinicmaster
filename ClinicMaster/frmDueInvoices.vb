﻿Option Strict On
Option Infer On

Imports SyncSoft.SQLDb
Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Common.Structures
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.SQL.Classes
Imports SyncSoft.Common.Win.Controls
Imports SyncSoft.Common.SQL.Enumerations
Imports LookupData = SyncSoft.Lookup.SQL.LookupData
Imports LookupDataID = SyncSoft.SQLDb.Lookup.LookupDataID
Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects

Imports System.Drawing.Printing
Imports System.Collections.Generic
Imports System.Text

Imports SyncSoft.SQLDb.Lookup.LookupDataID
Imports SyncSoft.SQLDb.Lookup


Public Class frmDueInvoices
#Region " Fields "

    Private patientNo As String = String.Empty
    Private visitDate As String = String.Empty
    Private memberCardNo As String = String.Empty
    Private mainMemberName As String = String.Empty
    Private claimReferenceNo As String = String.Empty
    Private primaryDoctor As String = String.Empty
    Private billCustomerName As String = String.Empty
    Private insuranceName As String = String.Empty

    Private OPDRowCount As Integer = 0
    Private ExtraBillsRowCount As Integer = 0

    Private OPDTotalAmount As Decimal = 0
    Private ExtraTotalAmount As Decimal = 0
    Private billCompanies As DataTable
    Private billCustomers As DataTable
    Private insuranceCompanies As DataTable
    Private WithEvents docInvoices As New PrintDocument()

    ' The paragraphs.
    Private invoiceParagraphs As Collection
    Private pageNo As Integer
    Private printFontName As String = "Courier New"
    Private bodyBoldFont As New Font(printFontName, 10, FontStyle.Bold)
    Private bodyNormalFont As New Font(printFontName, 10)
    Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
    Dim oExtraItemCodes As New LookupDataID.ExtraItemCodes()
    Dim oEntryModeID As New LookupDataID.EntryModeID()
    Dim oObjectNames As New LookupDataID.AccessObjectNames()

    Private padFullName As Integer = 11
    Private padwrappedFullName As Integer = 18
    Private padNotes As Integer = 14
    Private padQuantity As Integer = 4
    Private padUnitPrice As Integer = 12
    Private padAmount As Integer = 12

    Private WithEvents docBillInvoice As New PrintDocument()

    Dim OPrintOPtionID As New PrintOptionID()

#End Region
    Private Sub frmDueInvoices_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Cursor = Cursors.WaitCursor()

            Dim oBillModesID As New LookupDataID.BillModesID()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.dtpStartDate.MaxDate = Today
            Me.dtpEndDate.MaxDate = Today

            Me.dtpStartDate.Value = Today.AddDays(-1)
            Me.dtpEndDate.Value = Today

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadLookupDataCombo(Me.cboBillModesID, LookupObjects.BillModes, False)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try
    End Sub

    Private Sub cboBillAccountNo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboBillAccountNo.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub

    Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub

    Private Sub cboBillModesID_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBillModesID.SelectedIndexChanged

        Try

            Me.Cursor = Cursors.WaitCursor

            Me.ClearAccountControls()
            Me.ClearControls()

            Dim oBillModesID As New LookupDataID.BillModesID()

            Dim billModesID As String = StringValueMayBeEnteredIn(Me.cboBillModesID, "Account Category!")
            If String.IsNullOrEmpty(billModesID) Then Return

            Me.LoadAccountClients(billModesID)

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub LoadAccountClients(ByVal billModesID As String)

        Dim oBillCustomers As New SyncSoft.SQLDb.BillCustomers()
        Dim oInsurances As New SyncSoft.SQLDb.Insurances()
        Dim oBillModesID As New LookupDataID.BillModesID()
        Dim oSetupData As New SetupData()

        Try
            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.ClearAccountControls()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Select Case billModesID.ToUpper()

                Case oBillModesID.Cash.ToUpper()
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.btnFindPatientNo.Enabled = True
                    Me.btnLoadPatients.Enabled = True
                    Me.lblBillAccountNo.Text = "Patient No"
                    Me.lblBillCustomerName.Text = "Patient Name"

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Account.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ' Load all from Bill Customers
                    If Not InitOptions.LoadBillCustomersAtStart Then
                        billCustomers = oBillCustomers.GetBillCustomers().Tables("BillCustomers")
                        oSetupData.BillCustomers = billCustomers
                    Else : billCustomers = oSetupData.BillCustomers
                    End If

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    LoadComboData(Me.cboBillAccountNo, billCustomers, "BillCustomerFullName")
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.btnFindPatientNo.Enabled = False
                    Me.btnLoadPatients.Enabled = False
                    Me.lblBillAccountNo.Text = "Account No"
                    Me.lblBillCustomerName.Text = "Account Name"
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Insurance.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ' Load all from Insurances

                    Dim insurances As DataTable = oInsurances.GetInsurances().Tables("Insurances")
                    LoadComboData(Me.cboBillAccountNo, insurances, "InsuranceFullName")

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.btnFindPatientNo.Enabled = False
                    Me.btnLoadPatients.Enabled = False
                    Me.lblBillAccountNo.Text = "Insurance No"
                    Me.lblBillCustomerName.Text = "Insurance Name"
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End Select

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


    Private Sub btnLoadPatients_Click(sender As System.Object, e As System.EventArgs) Handles btnLoadPatients.Click
        Try

            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim fQuickSearch As New SyncSoft.SQL.Win.Forms.QuickSearch("Patients", Me.cboBillAccountNo)
            fQuickSearch.ShowDialog(Me)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim patientNo As String = RevertText(StringMayBeEnteredIn(Me.cboBillAccountNo))
            If Not String.IsNullOrEmpty(patientNo) Then ShowPatientDetails(patientNo)


            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub ShowPatientDetails(ByVal patientNo As String)
        Try
            Dim oPatients As New SyncSoft.SQLDb.Patients()
            If Not String.IsNullOrEmpty(patientNo) Then

                Dim patients As DataTable = oPatients.GetPatients(patientNo).Tables("Patients")
                Dim row As DataRow = patients.Rows(0)

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Me.stbBillCustomerName.Text = StringEnteredIn(row, "FullName")
            End If

        Catch eX As Exception
            ErrorMessage(eX)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub btnFindPatientNo_Click(sender As System.Object, e As System.EventArgs) Handles btnFindPatientNo.Click
        Try

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim fFindObject As New frmFindObject(ObjectName.PatientNo)

            If fFindObject.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
                Dim patientNo As String = RevertText(fFindObject.GetPatientNo())

                Me.cboBillAccountNo.Text = patientNo

                If Not String.IsNullOrEmpty(patientNo) Then ShowPatientDetails(patientNo)

            End If

        Catch eX As Exception
            ErrorMessage(eX)
            Me.btnFindPatientNo.PerformClick()

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

   
    Private Sub btnLoadPendingBillsPayment_Click(sender As System.Object, e As System.EventArgs) Handles btnLoadPendingBillsPayment.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim visittype As New LookupDataID.VisitTypeID()
            Me.ClearBillsPayControls()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.LoadData()
            
            CalculateTotalDueInvoices()


            If (ExtraBillsRowCount + OPDRowCount) > 0 Then
                Me.lblRecordsNo.Text = " Returned Record(s): " + (ExtraBillsRowCount + OPDRowCount).ToString

            Else
                Throw New ArgumentException("No Record(s) found!")
            End If

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub LoadData()


        Dim oBillModesID As New LookupDataID.BillModesID()

        Try
            Me.Cursor = Cursors.WaitCursor
            Dim oExtraBills As New SyncSoft.SQLDb.ExtraBills()
            Dim oInvoices As New SyncSoft.SQLDb.Invoices()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim billModesID As String = StringValueEnteredIn(Me.cboBillModesID, "Account Category!")
            Dim billNo As String = RevertText(StringEnteredIn(Me.cboBillAccountNo, "To-Bill Number!"))
            Dim companyNo As String = RevertText(SubstringRight(StringMayBeEnteredIn(Me.cboCompanyNo)))
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim startDate As Date = DateEnteredIn(Me.dtpStartDate, "Start Date")
            Dim endDate As Date = DateEnteredIn(Me.dtpEndDate, "End Date")

            Dim ExtraBills As DataTable
            Dim Invoices As DataTable

            If endDate < startDate Then Throw New ArgumentException("End Date can't be before Start Date!")


            If String.IsNullOrEmpty(companyNo) Then
                extraBills = oExtraBills.GetPeriodicNotPaidExtraBillItemsByBillToCustomerNo(billModesID, billNo, startDate, endDate, String.Empty).Tables("ExtraBills")
                invoices = oInvoices.GetPeriodicNotPaidBillToCustomerInvoices(billModesID, billNo, startDate, endDate, String.Empty).Tables("Invoices")

            Else
                ExtraBills = oExtraBills.GetPeriodicNotPaidExtraBillItemsByBillToCustomerNo(billModesID, billNo, startDate, endDate, companyNo).Tables("ExtraBills")
                Invoices = oInvoices.GetPeriodicNotPaidBillToCustomerInvoices(billModesID, billNo, startDate, endDate, companyNo).Tables("Invoices")
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(dgvExtraBillsInvoice, extraBills)
            LoadGridData(dgvOPDBillsInvoice, Invoices)

            ExtraBillsRowCount = extraBills.Rows.Count

            OPDRowCount = Invoices.Rows.Count
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


    Private Sub LoadBillDetails(ByVal billModesID As String, ByVal billNo As String)

        Dim displayName As String = String.Empty
        Dim accountBalance As Decimal

        Dim oVisits As New SyncSoft.SQLDb.Visits()
        Dim oBillCustomers As New SyncSoft.SQLDb.BillCustomers()
        Dim oInsurances As New SyncSoft.SQLDb.Insurances()
        Dim oBillModesID As New LookupDataID.BillModesID()
        Dim oPatients As New SyncSoft.SQLDb.Patients()
        Dim oBillCustomerTypeID As New LookupDataID.BillCustomerTypeID()


        Try
            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbBillCustomerName.Clear()
            If String.IsNullOrEmpty(billNo) Then Return
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Select Case billModesID.ToUpper()
                Case oBillModesID.Cash.ToUpper()
                    Dim row As DataRow = oPatients.GetPatients(billNo).Tables("Patients").Rows(0)

                    Dim patientNo = FormatText(billNo, "Patients", "patientNo")
                    Me.cboBillAccountNo.Text = patientNo
                    displayName = StringMayBeEnteredIn(row, "FullName")

                Case oBillModesID.Account.ToUpper()

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim row As DataRow = oBillCustomers.GetBillCustomers(billNo).Tables("BillCustomers").Rows(0)

                    Me.cboBillAccountNo.Text = FormatText(billNo, "BillCustomers", "AccountNo").ToUpper()
                    displayName = StringMayBeEnteredIn(row, "BillCustomerName")
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    EnablePeriodCTLS(True)
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    billCustomerName = StringMayBeEnteredIn(row, "BillCustomerName")
                    Dim billCustomerTypeID As String = StringMayBeEnteredIn(row, "BillCustomerTypeID")

                    accountBalance = GetAccountBalance(oBillModesID.Account, billNo)
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    If billCustomerTypeID.ToUpper().Equals(oBillCustomerTypeID.Insurance.ToUpper()) Then

                        Me.SetCompanyCTRLS(True)

                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        billCompanies = oBillCustomers.GetBillCustomersByInsuranceNo(billNo).Tables("BillCustomers")

                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        LoadComboData(Me.cboCompanyNo, billCompanies, "BillCustomerFullName")
                        Me.cboCompanyNo.Items.Insert(0, String.Empty)
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    Else : Me.SetCompanyCTRLS(False)
                    End If


                Case oBillModesID.Insurance.ToUpper()

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim row As DataRow = oInsurances.GetInsurances(billNo).Tables("Insurances").Rows(0)

                    Me.cboBillAccountNo.Text = FormatText(billNo, "Insurances", "InsuranceNo").ToUpper()
                    displayName = StringMayBeEnteredIn(row, "InsuranceName")
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    EnablePeriodCTLS(True)
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    accountBalance = GetAccountBalance(oBillModesID.Insurance, billNo)
            End Select

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.stbBillCustomerName.Text = displayName
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SetCompanyCTRLS(ByVal state As Boolean)

        Me.cboCompanyNo.SelectedIndex = -1
        Me.cboCompanyNo.SelectedIndex = -1
        Me.stbCompanyName.Clear()
        Me.cboCompanyNo.Items.Clear()
        Me.cboCompanyNo.Text = String.Empty

        Me.lblCompanyNo.Enabled = state
        Me.lblCompanyName.Enabled = state
        Me.cboCompanyNo.Enabled = state
        Me.stbCompanyName.Enabled = state

    End Sub
    Private Sub LoadDueOPDInvoices()

        Dim Invoices As New DataTable
        Dim oBillModesID As New LookupDataID.BillModesID()

        Try
            Me.Cursor = Cursors.WaitCursor
            Dim oInvoices As New SyncSoft.SQLDb.Invoices()
            Dim cashAccount As String = GetLookupDataDes(oBillModesID.Cash)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim billModesID As String = StringValueEnteredIn(Me.cboBillModesID, "Account Category!")
            Dim billNo As String = RevertText(SubstringRight(StringEnteredIn(Me.cboBillAccountNo, "To-Bill Number!")))
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If billNo.ToUpper().Equals(cashAccount.ToUpper()) Then Throw New ArgumentException("Invalid entry (CASH) for an account!")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim startDate As Date = DateEnteredIn(Me.dtpStartDate, "Start Date")
            Dim endDate As Date = DateEnteredIn(Me.dtpEndDate, "End Date")


            If endDate < startDate Then Throw New ArgumentException("End Date can't be before Start Date!")

            If billModesID.ToUpper().Equals(oBillModesID.Cash.ToUpper()) Then


                Invoices = oInvoices.GetPeriodicNotPaidCashInvoices(billNo, startDate, endDate).Tables("Invoices")

            ElseIf billModesID.ToUpper().Equals(oBillModesID.Account.ToUpper()) Then
                Invoices = oInvoices.GetPeriodicNotPaidAccountInvoices(billNo, startDate, endDate).Tables("Invoices")

            ElseIf billModesID.ToUpper().Equals(oBillModesID.Insurance.ToUpper()) Then
                Invoices = oInvoices.GetPeriodicNotPaidInsuranceInvoices(billNo, startDate, endDate).Tables("Invoices")

            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Not Invoices Is Nothing OrElse Invoices.Rows.Count > 0 Then
                LoadGridData(dgvOPDBillsInvoice, Invoices)
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            OPDRowCount = Invoices.Rows.Count
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub EnablePeriodCTLS(ByVal state As Boolean)

        Me.dtpStartDate.Checked = state
        Me.dtpEndDate.Checked = state
        Me.pnlPeriod.Enabled = state

    End Sub

    Private Sub ClearAccountControls()
        Me.cboBillAccountNo.DataSource = Nothing
        Me.cboBillAccountNo.Items.Clear()
        Me.cboBillAccountNo.Text = String.Empty
    End Sub

    Private Sub ResetDueInvoiceControls()

        Me.stbInPatientBill.Clear()
        Me.stbOutPatientBill.Clear()
        Me.stbBPTotalBill.Clear()
        Me.stbBPAmountWords.Clear()
        Me.lblRecordsNo.Text = String.Empty

        ExtraTotalAmount = 0
        OPDTotalAmount = 0
        ExtraBillsRowCount = 0
        OPDRowCount = 0
    End Sub



    Private Sub ClearControls()
    
        Me.stbBPTotalBill.Clear()
        Me.stbBPAmountWords.Clear()
        Me.stbBillCustomerName.Clear()
        Me.dtpStartDate.Value = Today.AddDays(-1)
        Me.dtpEndDate.Value = Today
        
        lblRecordsNo.Text = String.Empty
        
        Me.btnFindPatientNo.Enabled = False
        Me.btnLoadPatients.Enabled = False

        Me.dgvExtraBillsInvoice.Rows.Clear()
        Me.dgvOPDBillsInvoice.Rows.Clear()

    End Sub

    Private Sub ClearBillsPayControls()

        ' Me.stbBillCustomerName.Clear()
        ResetDueInvoiceControls()

        ResetControlsIn(Me.dgvExtraBillsInvoice)
        ResetControlsIn(Me.dgvOPDBillsInvoice)
        Me.dgvExtraBillsInvoice.Rows.Clear()

    End Sub


    Private Sub CalculateTotalDueInvoices()

        Dim oVisitTypeID As New LookupDataID.VisitTypeID()
        Me.stbBPTotalBill.Clear()
        ExtraTotalAmount = CalculateGridAmount(Me.dgvExtraBillsInvoice, colAmount)
        OPDTotalAmount = CalculateGridAmount(Me.dgvOPDBillsInvoice, colOPDAmount)

        Dim totalBill As Decimal = ExtraTotalAmount + OPDTotalAmount

        Me.stbOutPatientBill.Text = FormatNumber((OPDTotalAmount + CalculateTotalByVisitType(oVisitTypeID.OutPatient)), AppData.DecimalPlaces)
        Me.stbInPatientBill.Text = FormatNumber(CalculateTotalByVisitType(oVisitTypeID.InPatient), AppData.DecimalPlaces)
        Me.stbBPTotalBill.Text = FormatNumber(totalBill, AppData.DecimalPlaces)
        Me.stbBPAmountWords.Text = NumberToWords(totalBill)


    End Sub

    Private Sub cboBillAccountNo_Leave(sender As Object, e As System.EventArgs) Handles cboBillAccountNo.Leave
        Try

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.ClearBillsPayControls()

            Dim billNo As String = RevertText(SubstringRight(StringMayBeEnteredIn(Me.cboBillAccountNo)))
            Dim billModesID As String = StringValueMayBeEnteredIn(Me.cboBillModesID, "Account Category!")

            If String.IsNullOrEmpty(billNo) OrElse String.IsNullOrEmpty(billModesID) Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.LoadBillDetails(billModesID, billNo)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


        Catch ex As Exception
            ErrorMessage(ex)

        End Try
    End Sub

    Private Sub cboBillAccountNo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboBillAccountNo.SelectedIndexChanged
        Try
            Me.stbBillCustomerName.Clear()
            Me.ClearBillsPayControls()
        Catch ex As Exception

        End Try
    End Sub


#Region " Invoice Printing "

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Try

            Me.Cursor = Cursors.WaitCursor

            Me.PrintBillInvoice()

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub btnPrintPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintPreview.Click

        Try

            Me.Cursor = Cursors.WaitCursor

            ' Make a PrintDocument and attach it to the PrintPreview dialog.
            Dim dlgPrintPreview As New PrintPreviewDialog()

            Me.SetInvoicePrintData()

            With dlgPrintPreview
                .Document = docBillInvoice
                .Document.PrinterSettings.Collate = True
                .ShowIcon = False
                .WindowState = FormWindowState.Maximized
                .ShowDialog()
            End With

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub PrintBillInvoice()

        Dim dlgPrint As New PrintDialog()

        Try

            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.SetInvoicePrintData()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            dlgPrint.Document = docBillInvoice
            dlgPrint.Document.PrinterSettings.Collate = True
            If dlgPrint.ShowDialog = DialogResult.OK Then docBillInvoice.Print()

        Catch ex As Exception
            Throw ex

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub docBillInvoice_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles docBillInvoice.PrintPage

        Try

            Dim titleFont As New Font(printFontName, 12, FontStyle.Bold)

            Dim xPos As Single = e.MarginBounds.Left
            Dim yPos As Single = e.MarginBounds.Top

            Dim lineHeight As Single = bodyNormalFont.GetHeight(e.Graphics)

            Dim title As String = AppData.ProductOwner.ToUpper() + " Due Invoices".ToUpper()
            
            Dim invoiceDate As String = FormatDate(Today)

            Dim StartDate As String = FormatDate(DateMayBeEnteredIn(Me.dtpStartDate))
            Dim EndDate As String = FormatDate(DateMayBeEnteredIn(Me.dtpEndDate))

            Dim billNo As String = StringMayBeEnteredIn(Me.cboBillAccountNo)
            Dim billCustomerName As String = StringMayBeEnteredIn(Me.stbBillCustomerName)


            ' Increment the page number.
            pageNo += 1

            With e.Graphics

                'Dim widthTop As Single = .MeasureString("Received from width", titleFont).Width

                Dim widthTopFirst As Single = .MeasureString("W", titleFont).Width
                Dim widthTopSecond As Single = 9 * widthTopFirst
                Dim widthTopThird As Single = 19 * widthTopFirst
                Dim widthTopFourth As Single = 27 * widthTopFirst

                If pageNo < 2 Then

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    yPos = PrintPageHeader(e, bodyNormalFont, bodyBoldFont)
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    .DrawString(title, titleFont, Brushes.Black, xPos, yPos)
                    yPos += 3 * lineHeight

                    '.DrawString("Patient's Name: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    '.DrawString(billCustomerName, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    'yPos += lineHeight

                    
                    .DrawString(lblBillAccountNo.Text, bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(billNo, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight

                    .DrawString(lblBillCustomerName.Text, bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(billCustomerName, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight

                    .DrawString("Start Date: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(StartDate, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    .DrawString("End Date: ", bodyNormalFont, Brushes.Black, xPos + widthTopThird, yPos)
                    .DrawString(EndDate, bodyBoldFont, Brushes.Black, xPos + widthTopFourth, yPos)
                    yPos += lineHeight


                    yPos += 2 * lineHeight

                End If

                Dim _StringFormat As New StringFormat()

                ' Draw the rest of the text left justified,
                ' wrap at words, and don't draw partial lines.

                With _StringFormat
                    .Alignment = StringAlignment.Near
                    .FormatFlags = StringFormatFlags.LineLimit
                    .Trimming = StringTrimming.Word
                End With

                Dim charactersFitted As Integer
                Dim linesFilled As Integer

                If invoiceParagraphs Is Nothing Then Return

                Do While invoiceParagraphs.Count > 0

                    ' Print the next paragraph.
                    Dim oPrintParagraps As PrintParagraps = DirectCast(invoiceParagraphs(1), PrintParagraps)
                    invoiceParagraphs.Remove(1)

                    ' Get the area available for this paragraph.
                    Dim printAreaRectangle As RectangleF = New RectangleF(e.MarginBounds.Left, yPos, e.MarginBounds.Width, e.MarginBounds.Bottom - yPos)

                    ' If the printing area rectangle's height < 1, make it 1.
                    If printAreaRectangle.Height < 1 Then printAreaRectangle.Height = 1

                    ' See how big the text will be and how many characters will fit.
                    Dim textSize As SizeF = .MeasureString(oPrintParagraps.Text, oPrintParagraps.TheFont, _
                        New SizeF(printAreaRectangle.Width, printAreaRectangle.Height), _StringFormat, charactersFitted, linesFilled)

                    ' See if any characters will fit.
                    If charactersFitted > 0 Then
                        ' Draw the text.
                        .DrawString(oPrintParagraps.Text, oPrintParagraps.TheFont, Brushes.Black, printAreaRectangle, _StringFormat)
                        ' Increase the location where we can start, add a little interparagraph spacing.
                        yPos += textSize.Height ' + oPrintParagraps.TheFont.GetHeight(e.Graphics))

                    End If

                    ' See if some of the paragraph didn't fit on the page.
                    If charactersFitted < oPrintParagraps.Text.Length Then
                        ' Some of the paragraph didn't fit, prepare to print the rest on the next page.
                        oPrintParagraps.Text = oPrintParagraps.Text.Substring(charactersFitted)
                        invoiceParagraphs.Add(oPrintParagraps, Before:=1)
                        Exit Do
                    End If
                Loop

                ' If we have more paragraphs, we have more pages.
                e.HasMorePages = (invoiceParagraphs.Count > 0)

            End With

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub SetInvoicePrintData()

        Dim padTotalAmount As Integer = 44
        Dim footerFont As New Font(printFontName, 9)
        Dim oVariousOptions As New VariousOptions()

        pageNo = 0
        invoiceParagraphs = New Collection()

        Try
            Dim count As Integer = 0
            Dim tableHeader As New System.Text.StringBuilder(String.Empty)
            Dim tableData As New System.Text.StringBuilder(String.Empty)
            Dim tableDataOPDBill As New System.Text.StringBuilder(String.Empty)

            Dim padExtraBillNo As Integer = 16
            Dim padExtraBillDate As Integer = 12
            Dim padVisitNo As Integer = 8
            Dim padFullname As Integer = 19
            Dim padGender As Integer = 7
            Dim padMemberCardNo As Integer = 14
            Dim padAge As Integer = 5
            Dim padAmount As Integer = 14


            tableHeader.Append("Invoice No: ".PadRight(padExtraBillNo))
            tableHeader.Append("Date: ".PadRight(padExtraBillDate))
            tableHeader.Append("Full Name: ".PadRight(padFullname))
            tableHeader.Append("Card No: ".PadRight(padMemberCardNo))
            tableHeader.Append("Amount: ".PadLeft(padAmount))

            tableHeader.Append(ControlChars.NewLine)
            tableHeader.Append(ControlChars.NewLine)
            invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))

            For rowNo As Integer = 0 To Me.dgvExtraBillsInvoice.RowCount - 1

                'If Not CBool(Me.dgvExtraBillsInvoice.Item(Me.colInclude.Name, rowNo).Value) = True Then

                Dim cells As DataGridViewCellCollection = Me.dgvExtraBillsInvoice.Rows(rowNo).Cells

                Dim ExtraBillNo As String = RevertText(StringMayBeEnteredIn(cells, Me.colExtraBillNo))
                Dim ExtraBillDate As String = StringMayBeEnteredIn(cells, Me.colExtraBillDate)
                Dim PatientNo As String = StringMayBeEnteredIn(cells, Me.colPatientNo)
                Dim VisitNo As String = StringMayBeEnteredIn(cells, Me.colVisitNo)
                Dim MemberCardNo As String = StringMayBeEnteredIn(cells, Me.colMemberCardNo)
                Dim Amount As String = StringMayBeEnteredIn(cells, Me.colAmount)
                Dim FullName As String = StringMayBeEnteredIn(cells, Me.colFullName)
                Dim Gender As String = StringMayBeEnteredIn(cells, Me.colGender)
                Dim Age As String = StringMayBeEnteredIn(cells, Me.colAge)

                tableData.Append(ExtraBillNo.PadRight(padExtraBillNo))
                tableData.Append(ExtraBillDate.PadRight(padExtraBillDate))

                'Dim wrappedFullName As List(Of String) = WrapText(FullName, padFullname)

                Dim wrappedFullName As List(Of String) = WrapText(FullName, padFullname)
                If wrappedFullName.Count > 1 Then
                    For pos As Integer = 0 To wrappedFullName.Count - 1
                        tableData.Append(FixDataLength(wrappedFullName(pos).Trim(), padFullname))
                        If Not pos = wrappedFullName.Count - 1 Then
                            tableData.Append(ControlChars.NewLine)
                            tableData.Append(GetSpaces(padExtraBillNo + padExtraBillDate))
                        Else
                            tableData.Append(MemberCardNo.PadRight(padMemberCardNo))
                            tableData.Append(Amount.PadLeft(padAmount))
                        End If
                    Next

                Else
                    tableData.Append(FullName.PadRight(padFullname))
                    tableData.Append(MemberCardNo.PadRight(padMemberCardNo))
                    tableData.Append(Amount.PadLeft(padAmount))
                End If

                tableData.Append(ControlChars.NewLine)



            Next


            For rowNo As Integer = 0 To Me.dgvOPDBillsInvoice.RowCount - 1

                'If Not CBool(Me.dgvExtraBillsInvoice.Item(Me.colInclude.Name, rowNo).Value) = True Then

                Dim cells As DataGridViewCellCollection = Me.dgvOPDBillsInvoice.Rows(rowNo).Cells

                Dim InvoiceNo As String = RevertText(StringMayBeEnteredIn(cells, Me.colInvoiceNo))
                Dim InvoiceDate As String = StringMayBeEnteredIn(cells, Me.colInvoiceDate)
                Dim OPDPatientNo As String = StringMayBeEnteredIn(cells, Me.colOPDPatientNo)
                Dim OPDVisitNo As String = StringMayBeEnteredIn(cells, Me.colOPDVisitNo)
                Dim OPDMemberCardNo As String = StringMayBeEnteredIn(cells, Me.colOPDMemberCardNo)
                Dim OPDAmount As String = StringMayBeEnteredIn(cells, Me.colOPDAmount)
                Dim OPDFullName As String = StringMayBeEnteredIn(cells, Me.colOPDFullName)
                Dim OPDGender As String = StringMayBeEnteredIn(cells, Me.colOPDGender)
                Dim OPDAge As String = StringMayBeEnteredIn(cells, Me.colOPDAge)

                tableDataOPDBill.Append(InvoiceNo.PadRight(padExtraBillNo))
                tableDataOPDBill.Append(InvoiceDate.PadRight(padExtraBillDate))

                'Dim wrappedFullName As List(Of String) = WrapText(FullName, padFullname)

                Dim wrappedOPDFullName As List(Of String) = WrapText(OPDFullName, padFullname)

                If wrappedOPDFullName.Count > 1 Then
                    For pos As Integer = 0 To wrappedOPDFullName.Count - 1
                        tableDataOPDBill.Append(FixDataLength(wrappedOPDFullName(pos).Trim(), padFullname))
                        If Not pos = wrappedOPDFullName.Count - 1 Then
                            tableDataOPDBill.Append(ControlChars.NewLine)
                            tableDataOPDBill.Append(GetSpaces(padExtraBillNo + padExtraBillDate))
                        Else
                            tableDataOPDBill.Append(OPDMemberCardNo.PadRight(padMemberCardNo))
                            tableDataOPDBill.Append(OPDAmount.PadLeft(padAmount))
                        End If
                    Next

                Else
                    tableDataOPDBill.Append(OPDFullName.PadRight(padFullname))
                    tableDataOPDBill.Append(OPDMemberCardNo.PadRight(padMemberCardNo))
                    tableDataOPDBill.Append(OPDAmount.PadLeft(padAmount))
                End If

                tableDataOPDBill.Append(ControlChars.NewLine)



            Next

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            invoiceParagraphs.Add(New PrintParagraps(bodyNormalFont, tableData.ToString()))
            invoiceParagraphs.Add(New PrintParagraps(bodyNormalFont, tableDataOPDBill.ToString()))

            Dim OPDIPDTotals As New System.Text.StringBuilder(String.Empty)
            OPDIPDTotals.Append(ControlChars.NewLine)
            OPDIPDTotals.Append("Out Patient Bill: ")
            OPDIPDTotals.Append((FormatNumber(CDec(Me.stbOutPatientBill.Text), AppData.DecimalPlaces)).PadLeft(57))
            OPDIPDTotals.Append(ControlChars.NewLine)
            OPDIPDTotals.Append("In Patient Bill: ")
            OPDIPDTotals.Append((FormatNumber(CDec(Me.stbInPatientBill.Text), AppData.DecimalPlaces)).PadLeft(58))

            invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, OPDIPDTotals.ToString()))
            invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, Me.GetTotalAmountData(54, (ExtraTotalAmount + OPDTotalAmount)).ToString()))
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim patientSignData As New System.Text.StringBuilder(String.Empty)
            patientSignData.Append(ControlChars.NewLine)
            patientSignData.Append(ControlChars.NewLine)


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim checkedSignData As New System.Text.StringBuilder(String.Empty)
            checkedSignData.Append(ControlChars.NewLine)

            checkedSignData.Append("Checked By:       " + GetCharacters("."c, 20))
            checkedSignData.Append(GetSpaces(4))
            checkedSignData.Append("Date:  " + GetCharacters("."c, 20))
            checkedSignData.Append(ControlChars.NewLine)
            invoiceParagraphs.Add(New PrintParagraps(footerFont, checkedSignData.ToString()))

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim footerData As New System.Text.StringBuilder(String.Empty)
            footerData.Append(ControlChars.NewLine)
            footerData.Append("Printed by " + CurrentUser.FullName + " on " + FormatDate(Now) + " at " + Now.ToString("hh:mm tt") +
                                " from " + AppData.AppTitle)
            footerData.Append(ControlChars.NewLine)
            invoiceParagraphs.Add(New PrintParagraps(footerFont, footerData.ToString()))
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Function GetTotalAmountData(padTotalAmount As Integer, totalBillInvoice As Decimal) As StringBuilder

        Dim totalAmount As New System.Text.StringBuilder(String.Empty)


        totalAmount.Append(ControlChars.NewLine)
        totalAmount.Append("Total Invoice Amount:")
        totalAmount.Append(FormatNumber(totalBillInvoice).PadLeft(padTotalAmount))

        totalAmount.Append(ControlChars.NewLine)

        Dim amountWordsData As New System.Text.StringBuilder(String.Empty)

        totalAmount.Append("(" + NumberToWords(totalBillInvoice).Trim() + " ONLY)")
        amountWordsData.Append(ControlChars.NewLine)

        Return totalAmount
    End Function


    Private Function GetExtraBillItemsList() As List(Of Tuple(Of String, String, String, String, String, String, Decimal))

        Try

            ' Create list of tuples with seven items each.
            Dim lExtraBillItems As New List(Of Tuple(Of String, String, String, String, String, String, Decimal))

            For rowNo As Integer = 0 To Me.dgvExtraBillsInvoice.RowCount - 1

                Dim cells As DataGridViewCellCollection = Me.dgvExtraBillsInvoice.Rows(rowNo).Cells
                'Dim itemCategoryID As String = cells.Item(Me.colItemCategoryID.Name).Value.ToString()
                'Dim itemCode As String = cells.Item(Me.colItemCode.Name).Value.ToString()
                'Dim category As String = cells.Item(Me.colItemCategory.Name).Value.ToString()
                'Dim itemName As String = cells.Item(Me.colItemName.Name).Value.ToString()
                'Dim quantity As Integer = IntegerEnteredIn(cells, Me.colQuantity, "quantity!")
                'Dim unitPrice As Decimal = DecimalEnteredIn(cells, Me.colUnitPrice, False, "unitPrice!")
                'Dim amount As Decimal = DecimalEnteredIn(cells, Me.colOriginalAmount, False, "Original Amount!")

                Dim ExtraBillNo As String = StringMayBeEnteredIn(cells, Me.colExtraBillNo)
                Dim ExtraBillDate As String = StringMayBeEnteredIn(cells, Me.colExtraBillDate)
                Dim PatientNo As String = StringMayBeEnteredIn(cells, Me.colPatientNo)
                Dim VisitNo As String = StringMayBeEnteredIn(cells, Me.colVisitNo)
                Dim VisitType As String = StringMayBeEnteredIn(cells, Me.colVisitType)
                Dim Amount As Decimal = DecimalMayBeEnteredIn(cells, Me.colAmount)
                Dim FullName As String = StringMayBeEnteredIn(cells, Me.colFullName)
                Dim Gender As String = StringMayBeEnteredIn(cells, Me.colGender)
                Dim Age As String = StringMayBeEnteredIn(cells, Me.colAge)

                lExtraBillItems.Add(New Tuple(Of String, String, String, String, String, String, Decimal)(ExtraBillNo, ExtraBillDate, FullName, Gender, Age, VisitType, Amount))

            Next
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Return lExtraBillItems

        Catch ex As Exception
            Throw ex
        End Try

    End Function


   
    'Public Function ExtraBillsData() As String

    '    Dim oEntryModeID As New LookupDataID.EntryModeID()

    '    Try

    '        Dim tableData As New System.Text.StringBuilder(String.Empty)

    '        For rowNo As Integer = 0 To Me.dgvExtraBillsInvoice.RowCount - 1

    '            If CBool(Me.dgvExtraBillsInvoice.Item(Me.colInclude.Name, rowNo).Value) = True Then

    '                Dim cells As DataGridViewCellCollection = Me.dgvExtraBillsInvoice.Rows(rowNo).Cells

    '                Dim billDate As String = FormatDate(DateMayBeEnteredIn(cells, Me.colExtraBillDate), "dd MMM yy")

    '                Dim ExtraBillNo As String = StringMayBeEnteredIn(cells, Me.colExtraBillNo)
    '                Dim ExtraBillDate As String = StringMayBeEnteredIn(cells, Me.colExtraBillDate)
    '                Dim PatientNo As String = StringMayBeEnteredIn(cells, Me.colPatientNo)
    '                Dim VisitNo As String = StringMayBeEnteredIn(cells, Me.colVisitNo)
    '                Dim VisitType As String = StringMayBeEnteredIn(cells, Me.colVisitType)
    '                Dim Amount As String = StringMayBeEnteredIn(cells, Me.colAmount)
    '                Dim FullName As String = StringMayBeEnteredIn(cells, Me.colFullName)
    '                Dim Gender As String = StringMayBeEnteredIn(cells, Me.colGender)
    '                Dim Age As String = StringMayBeEnteredIn(cells, Me.colAge)

    '                tableData.Append(billDate.PadRight(padAmount))



    '            End If


    '        Next

    '        Return tableData.ToString()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Function


    

    'Public Function OPDBillData() As String

    '    Try

    '        Dim tableData As New System.Text.StringBuilder(String.Empty)
    '        Dim visitDate As String = FormatDate(DateMayBeEnteredIn(Me.stbVisitDate), "dd MMM yy")

    '        For rowNo As Integer = 0 To Me.dgvOPDBillsInvoice.RowCount - 1

    '            If CBool(Me.dgvOPDBillsInvoice.Item(Me.colOPDInclude.Name, rowNo).Value) = True Then

    '                Dim cells As DataGridViewCellCollection = Me.dgvOPDBillsInvoice.Rows(rowNo).Cells

    '                Dim itemName As String = StringMayBeEnteredIn(cells, Me.colOPDItemName)
    '                Dim quantity As String = StringMayBeEnteredIn(cells, Me.colOPDQuantity)
    '                Dim unitPrice As String = StringMayBeEnteredIn(cells, Me.colOPDUnitPrice)
    '                Dim amount As String = StringMayBeEnteredIn(cells, Me.colOPDAmount)

    '                tableData.Append(visitDate.PadRight(padItemNo))
    '                Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
    '                If wrappeditemName.Count > 1 Then
    '                    For pos As Integer = 0 To wrappeditemName.Count - 1
    '                        tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
    '                        If Not pos = wrappeditemName.Count - 1 Then
    '                            tableData.Append(ControlChars.NewLine)
    '                            tableData.Append(GetSpaces(padItemNo))
    '                        Else
    '                            tableData.Append(String.Empty.PadRight(padNotes))
    '                            tableData.Append(quantity.PadLeft(padQuantity))
    '                            tableData.Append(unitPrice.PadLeft(padUnitPrice))
    '                            tableData.Append(amount.PadLeft(padAmount))
    '                        End If
    '                    Next

    '                Else
    '                    tableData.Append(FixDataLength(itemName, padItemName))
    '                    tableData.Append(String.Empty.PadRight(padNotes))
    '                    tableData.Append(quantity.PadLeft(padQuantity))
    '                    tableData.Append(unitPrice.PadLeft(padUnitPrice))
    '                    tableData.Append(amount.PadLeft(padAmount))
    '                End If
    '                tableData.Append(ControlChars.NewLine)
    '            End If

    '        Next

    '        Return tableData.ToString()

    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Function

#End Region

#Region "Context Menu Strip- Due invoices"
    Private Sub cmsDueInvoices_Opening(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles cmsDueInvoices.Opening

         Select Me.tbcExtraBillsInvoice.SelectedTab.Name

            Case Me.tpgExtraBills.Name
                Me.cmsDueInvoicesExtraBillNoDetails.Text = "Extra Bill No details"
                If Me.dgvExtraBillsInvoice.ColumnCount < 1 OrElse Me.dgvExtraBillsInvoice.RowCount < 1 Then
                    Me.cmsDueInvoicesCopy.Enabled = False
                    Me.cmsDueInvoicesSelectAll.Enabled = False
                    Me.cmsDueInvoicesExtraBillNoDetails.Enabled = False
                Else
                    Me.cmsDueInvoicesCopy.Enabled = True
                    Me.cmsDueInvoicesSelectAll.Enabled = True
                    Me.cmsDueInvoicesExtraBillNoDetails.Enabled = True
                End If

            Case Me.tpgOPDInvoices.Name

                Me.cmsDueInvoicesExtraBillNoDetails.Text = "Invoice No details"

                If Me.dgvOPDBillsInvoice.ColumnCount < 1 OrElse Me.dgvOPDBillsInvoice.RowCount < 1 Then
                    Me.cmsDueInvoicesCopy.Enabled = False
                    Me.cmsDueInvoicesSelectAll.Enabled = False
                    Me.cmsDueInvoicesExtraBillNoDetails.Enabled = False
                Else
                    Me.cmsDueInvoicesCopy.Enabled = True
                    Me.cmsDueInvoicesSelectAll.Enabled = True
                    Me.cmsDueInvoicesExtraBillNoDetails.Enabled = True
                End If

        End Select

    End Sub

    Private Sub cmsDueInvoicesCopy_Click(sender As System.Object, e As System.EventArgs) Handles cmsDueInvoicesCopy.Click

        Try

            Me.Cursor = Cursors.WaitCursor


            Select Case Me.tbcExtraBillsInvoice.SelectedTab.Name


                Case Me.tpgExtraBills.Name
                    If Me.dgvExtraBillsInvoice.SelectedCells.Count < 1 Then Return
                    Clipboard.SetText(CopyFromControl(Me.dgvExtraBillsInvoice))


                Case Me.tpgOPDInvoices.Name

                    If Me.dgvOPDBillsInvoice.SelectedCells.Count < 1 Then Return
                    Clipboard.SetText(CopyFromControl(Me.dgvOPDBillsInvoice))


            End Select

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try




    End Sub

    Private Sub cmsDueInvoicesSelectAll_Click(sender As System.Object, e As System.EventArgs) Handles cmsDueInvoicesSelectAll.Click

        Try

            Me.Cursor = Cursors.WaitCursor

            Select Case Me.tbcExtraBillsInvoice.SelectedTab.Name

                Case Me.tpgExtraBills.Name
                    Me.dgvExtraBillsInvoice.SelectAll()

                Case Me.tpgOPDInvoices.Name
                    Me.dgvOPDBillsInvoice.SelectAll()
            End Select

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub cmsDueInvoicesExtraBillNoDetails_Click(sender As System.Object, e As System.EventArgs) Handles cmsDueInvoicesExtraBillNoDetails.Click


        Try
            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim oAccessObjectNames As New LookupDataID.AccessObjectNames()

            Select Case Me.tbcExtraBillsInvoice.SelectedTab.Name

                Case Me.tpgExtraBills.Name
                    Dim rowIndex As Integer = Me.dgvExtraBillsInvoice.CurrentCell.RowIndex
                    Dim ExtraBillNo As String = StringMayBeEnteredIn(Me.dgvExtraBillsInvoice.Rows(rowIndex).Cells, Me.colExtraBillNo)
                    Dim fPrintVisitsInvoice As New frmPrintVisitsInvoice(oAccessObjectNames.ExtraBills, ExtraBillNo)
                    fPrintVisitsInvoice.ShowDialog()

                Case Me.tpgOPDInvoices.Name

                    Dim rowIndex As Integer = Me.dgvOPDBillsInvoice.CurrentCell.RowIndex
                    Dim InvoiceNo As String = StringMayBeEnteredIn(Me.dgvOPDBillsInvoice.Rows(rowIndex).Cells, Me.colInvoiceNo)
                    Dim fPrintVisitsInvoice As New frmPrintVisitsInvoice(oAccessObjectNames.Invoices, InvoiceNo)
                    fPrintVisitsInvoice.ShowDialog()
            End Select
        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try



    End Sub
#End Region
    


    Public Function CalculateTotalByVisitType(ByVal VisitType As String) As Decimal
        Dim TotalAmount As Decimal = 0

        For rowNo As Integer = 0 To Me.dgvExtraBillsInvoice.RowCount - 1

            Dim cells As DataGridViewCellCollection = Me.dgvExtraBillsInvoice.Rows(rowNo).Cells

            Dim Amount As Decimal = CDec(cells.Item(Me.colAmount.Name).Value)
            Dim VisitTypeValue As String = cells.Item(Me.colVisitTypeID.Name).Value.ToString()
            ''MsgBox(VisitTypeValue + "  - " + VisitType)
            If VisitTypeValue.Equals(VisitType) Then
                TotalAmount += Amount
            End If

        Next

        Return TotalAmount

    End Function



    Private Sub cboCompanyNo_Leave(sender As Object, e As System.EventArgs) Handles cboCompanyNo.Leave
        Dim companyName As String
        Dim oBillModesID As New LookupDataID.BillModesID()

        Try

            Dim companyNo As String = RevertText(SubstringRight(StringMayBeEnteredIn(Me.cboCompanyNo)))
            Dim billModesID As String = StringValueMayBeEnteredIn(Me.cboBillModesID, "To-Bill Account Category!")

            If String.IsNullOrEmpty(billModesID) Then Return

            Select Case billModesID.ToUpper()

                Case oBillModesID.Cash.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Account.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    If Not String.IsNullOrEmpty(companyNo) Then

                        Me.cboCompanyNo.Text = FormatText(companyNo, "BillCustomers", "AccountNo").ToUpper()

                        For Each row As DataRow In billCompanies.Select("AccountNo = '" + companyNo + "'")

                            If Not IsDBNull(row.Item("BillCustomerName")) Then
                                companyName = StringEnteredIn(row, "BillCustomerName")
                                companyNo = StringMayBeEnteredIn(row, "AccountNo")
                                Me.cboCompanyNo.Text = FormatText(companyNo, "BillCustomers", "AccountNo").ToUpper()
                            Else
                                companyName = String.Empty
                                companyNo = String.Empty
                            End If

                            Me.stbCompanyName.Text = companyName
                        Next

                    End If
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case oBillModesID.Insurance.ToUpper()

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    If Not String.IsNullOrEmpty(companyNo) Then

                        Me.cboCompanyNo.Text = FormatText(companyNo, "Companies", "CompanyNo").ToUpper()

                        For Each row As DataRow In insuranceCompanies.Select("CompanyNo = '" + companyNo + "'")

                            If Not IsDBNull(row.Item("CompanyName")) Then
                                companyName = StringEnteredIn(row, "CompanyName")
                                companyNo = StringMayBeEnteredIn(row, "CompanyNo")
                                Me.cboCompanyNo.Text = FormatText(companyNo, "Companies", "CompanyNo").ToUpper()
                            Else
                                companyName = String.Empty
                                companyNo = String.Empty
                            End If

                            Me.stbCompanyName.Text = companyName
                        Next

                    End If
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End Select

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        End Try
    End Sub

    Private Sub cboCompanyNo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboCompanyNo.SelectedIndexChanged
        Me.stbCompanyName.Clear()
    End Sub

End Class