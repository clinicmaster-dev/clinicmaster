
Option Strict On
Option Infer On
Imports SyncSoft.Common.Methods
Imports SyncSoft.Common.Structures
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports LookupDataID = SyncSoft.SQLDb.Lookup.LookupDataID
Imports System.Drawing.Printing
Imports System.Collections.Generic
Imports System.Text
Imports SyncSoft.SQLDb
Imports SyncSoft.SQLDb.Lookup

Public Class frmPrintVisitsInvoice

#Region " Fields "

    Private tipCoPayValueWords As New ToolTip()
    Private WithEvents docInvoices As New PrintDocument()
    Private defaultObjectName As String = String.Empty

    ' The paragraphs.
    Private invoiceParagraphs As Collection
    Private pageNo As Integer
    Private printFontName As String = "Courier New"
    Private bodyBoldFont As New Font(printFontName, 10, FontStyle.Bold)
    Private bodyNormalFont As New Font(printFontName, 10)
    Private oVariousOptions As New VariousOptions()
    Dim defaultInvoiceNo As String = String.Empty
    Private payTypeID As String = String.Empty
    Private oPayTypeID As New LookupDataID.PayTypeID()
    Dim oItemCategoryID As New LookupDataID.ItemCategoryID()
    Dim oExtraItemCodes As New LookupDataID.ExtraItemCodes()
    Private oExtraChargeItem As New ExtraChargeItems()
    Private oPrintOptionID As New LookupDataID.PrintOptionID()

#End Region

    Private Sub frmPrintVisitsInvoice_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      
        Try
            Me.tbcPrintInvoices.TabPages.Remove(tpgAdjustments)
            Me.tbcPrintInvoices.TabPages.Remove(tpgUpAdjustments)

            Me.ChangeControlNaming(defaultObjectName)
            Dim oAccessObjectNames As New LookupDataID.AccessObjectNames

            If defaultObjectName.Equals(oAccessObjectNames.ExtraBills) Then
                If Not String.IsNullOrEmpty(Me.defaultInvoiceNo) Then
                    Me.stbInvoiceNo.Text = Me.defaultInvoiceNo
                    Me.LoadExtraBillsData(RevertText(defaultInvoiceNo))
                End If
            Else
                If Not String.IsNullOrEmpty(Me.defaultInvoiceNo) Then
                    Me.stbInvoiceNo.Text = Me.defaultInvoiceNo
                    Me.LoadInvoicesData(RevertText(defaultInvoiceNo))
                End If
            End If



            LoadLookupDataCombo(Me.cboPrintOptions, LookupObjects.PrintOption, True)
        Catch ex As Exception
            ErrorMessage(ex)
        End Try
    End Sub

    Private Sub frmPrintVisitsInvoice_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub stbInvoiceNo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles stbInvoiceNo.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub
    Private Sub ChangeControlNaming(ByVal ObjectName As String)
        Dim oAccessObjectNames As New LookupDataID.AccessObjectNames()
        Dim oPayTypeID As New LookupDataID.PayTypeID()

        If ObjectName.Equals(oAccessObjectNames.ExtraBills) Then
            Me.Text = "Print Extra Bill"
            Me.lblInvoiceNo.Text = "Extra Bill No"
            Me.lblInvoiceDate.Text = "Extra Bill Date"
            Me.lblInvoiceAmount.Text = "Extra Bill Amount"
            Me.tpgInvoices.Text = "Extra Bills Details"
            Me.lblPayType.Visible = False
            Me.stbPayType.Visible = False
            Me.colDiscount.Visible = False
            Me.colPayStatus.Visible = True
            Me.payTypeID = oPayTypeID.ExtraBill
        Else
            Me.Text = "Print Invoice"
            Me.lblInvoiceNo.Text = "Invoice No"
            Me.lblInvoiceDate.Text = "Invoice Date"
            Me.lblInvoiceAmount.Text = "Invoice Amount"
            Me.tpgInvoices.Text = "Invoice Details"
            Me.lblPayType.Visible = True
            Me.stbPayType.Visible = True

            Me.colDiscount.Visible = True
            Me.colPayStatus.Visible = False
        End If

    End Sub

    Private Sub ClearControls()

        Me.stbFullName.Clear()
        Me.stbPatientNo.Clear()
        Me.stbVisitDate.Clear()
        Me.stbVisitNo.Clear()
        Me.stbInvoiceDate.Clear()
        Me.stbStartDate.Clear()
        Me.stbEndDate.Clear()
        Me.stbPrimaryDoctor.Clear()
        Me.stbMemberCardNo.Clear()
        Me.stbMainMemberName.Clear()
        Me.stbClaimReferenceNo.Clear()
        Me.stbBillNo.Clear()
        Me.stbBillCustomerName.Clear()
        Me.stbInsuranceNo.Clear()
        Me.stbInsuranceName.Clear()
        Me.stbInvoiceAmount.Clear()
        Me.stbAmountWords.Clear()
        Me.stbPayType.Clear()
        Me.stbCoPayType.Clear()
        Me.nbxCoPayPercent.Value = String.Empty
        Me.nbxCoPayValue.Value = String.Empty
        Me.tipCoPayValueWords.RemoveAll()
        Me.dgvInvoiceDetails.Rows.Clear()
        Me.dgvAdjustments.Rows.Clear()

    End Sub

    Private Sub stbInvoiceNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stbInvoiceNo.TextChanged
        Me.ClearControls()
    End Sub

    Private Sub stbInvoiceNo_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles stbInvoiceNo.Leave

        Try

            Me.Cursor = Cursors.WaitCursor
            Dim oAccessObjectNames As New LookupDataID.AccessObjectNames()

            Dim invoiceNo As String = RevertText(StringMayBeEnteredIn(Me.stbInvoiceNo))
            If String.IsNullOrEmpty(invoiceNo) Then Return

            If defaultObjectName.Equals(oAccessObjectNames.ExtraBills) Then
                Me.LoadExtraBillsData(invoiceNo)
            Else
                Me.LoadInvoicesData(invoiceNo)
            End If


        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub btnLoadInvoices_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoadInvoices.Click

        Try

            Me.Cursor = Cursors.WaitCursor
            Dim oBillModesID As New LookupDataID.BillModesID()
            Dim oAccessObjectNames As New LookupDataID.AccessObjectNames()
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            If defaultObjectName.Equals(oAccessObjectNames.ExtraBills) Then

                Dim fPeriodicExtraBills As New frmPeriodicExtraBills(Me.stbInvoiceNo)
                fPeriodicExtraBills.ShowDialog(Me)

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim ExtraBilllNo As String = RevertText(StringMayBeEnteredIn(Me.stbInvoiceNo))
                If String.IsNullOrEmpty(ExtraBilllNo) Then Return
                Me.LoadExtraBillsData(ExtraBilllNo)
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Else
                Dim fPeriodicInvoices As New frmPeriodicInvoices(Me.stbInvoiceNo, oBillModesID.Cash)
                fPeriodicInvoices.ShowDialog(Me)

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim invoiceNo As String = RevertText(StringMayBeEnteredIn(Me.stbInvoiceNo))
                If String.IsNullOrEmpty(invoiceNo) Then Return
                Me.LoadInvoicesData(invoiceNo)
            End If



        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub LoadInvoicesData(ByVal invoiceNo As String)

        Try

            '''''''''''''''''''''''''''''''''
            Me.ClearControls()

            '''''''''''''''''''''''''''''''''
            Me.LoadInvoices(invoiceNo)
            Me.LoadInvoiceDetails(invoiceNo)
            '''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub LoadExtraBillsData(ByVal ExtraBillNo As String)

        Try

            '''''''''''''''''''''''''''''''''
            Me.ClearControls()

            '''''''''''''''''''''''''''''''''
            Me.LoadExtraBills(ExtraBillNo)
            Me.LoadExtraBillItems(ExtraBillNo)
            '''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        End Try

    End Sub

    Private Sub LoadInvoices(ByVal invoiceNo As String)

        Dim oInvoices As New SyncSoft.SQLDb.Invoices()
        Dim oVisits As New SyncSoft.SQLDb.Visits()

        Try

            Dim invoicesRow As DataRow = oInvoices.GetInvoices(invoiceNo).Tables("Invoices").Rows(0)
            Dim visitsRow As DataRow
            Dim visitNo As String = RevertText(StringEnteredIn(invoicesRow, "PayNo"))
            Me.payTypeID = RevertText(StringEnteredIn(invoicesRow, "PayTypeID"))

            If Me.payTypeID.ToUpper.Equals(oPayTypeID.ExtraBill.ToUpper()) OrElse
                        Me.payTypeID.ToUpper.Equals(oPayTypeID.ExtraBillCASH.ToUpper()) OrElse
                        Me.payTypeID.ToUpper.Equals(oPayTypeID.ExtraBillAccount.ToUpper()) OrElse
                        Me.payTypeID.ToUpper.Equals(oPayTypeID.ExtraBillInsurance.ToUpper()) Then
                visitsRow = oVisits.GetAdmissionsDetails(visitNo).Tables("Visits").Rows(0)
            Else
                visitsRow = oVisits.GetVisits(visitNo).Tables("Visits").Rows(0)
            End If



            Dim patientNo As String = RevertText(StringEnteredIn(visitsRow, "PatientNo"))

            'Me.stbInvoiceNo.Text = FormatText(invoiceNo, "Invoices", "InvoiceNo")
            Me.stbFullName.Text = StringEnteredIn(visitsRow, "FullName")
            Me.stbPatientNo.Text = FormatText(patientNo, "Patients", "PatientNo")
            Me.stbVisitDate.Text = FormatDate(DateEnteredIn(visitsRow, "VisitDate"))
            Me.stbVisitNo.Text = FormatText(visitNo, "Visits", "VisitNo")
            Me.stbPrimaryDoctor.Text = StringMayBeEnteredIn(visitsRow, "PrimaryDoctor")
            Me.stbMemberCardNo.Text = StringMayBeEnteredIn(visitsRow, "MemberCardNo")
            Me.stbMainMemberName.Text = StringMayBeEnteredIn(visitsRow, "MainMemberName")
            Me.stbClaimReferenceNo.Text = StringMayBeEnteredIn(visitsRow, "ClaimReferenceNo")
            Me.stbBillNo.Text = FormatText(StringEnteredIn(visitsRow, "BillNo"), "BillCustomers", "AccountNo")
            Me.stbBillCustomerName.Text = StringMayBeEnteredIn(visitsRow, "BillCustomerName")
            Me.stbInsuranceNo.Text = FormatText(StringMayBeEnteredIn(visitsRow, "InsuranceNo"), "Insurances", "InsuranceNo")
            Me.stbInsuranceName.Text = StringMayBeEnteredIn(visitsRow, "InsuranceName")
            Me.stbCoPayType.Text = StringMayBeEnteredIn(visitsRow, "CoPayType")
            Me.nbxCoPayPercent.Value = SingleMayBeEnteredIn(visitsRow, "CoPayPercent").ToString()
            Me.nbxCoPayValue.Value = FormatNumber(DecimalMayBeEnteredIn(visitsRow, "CoPayValue"), AppData.DecimalPlaces)
            Me.tipCoPayValueWords.SetToolTip(Me.nbxCoPayValue, NumberToWords(DecimalMayBeEnteredIn(visitsRow, "CoPayValue")))

            Me.stbInvoiceDate.Text = FormatDate(DateEnteredIn(invoicesRow, "InvoiceDate"))
            Me.stbStartDate.Text = FormatDate(DateEnteredIn(invoicesRow, "StartDate"))
            Me.stbEndDate.Text = FormatDate(DateEnteredIn(invoicesRow, "EndDate"))
            'Me.stbInvoiceAmount.Text = FormatNumber(DecimalMayBeEnteredIn(invoicesRow, "NetAmount"), AppData.DecimalPlaces)
            ' Me.stbAmountWords.Text = StringMayBeEnteredIn(invoicesRow, "AmountWords")
            Me.stbPayType.Text = StringEnteredIn(invoicesRow, "PayType")
            Me.payTypeID = StringEnteredIn(invoicesRow, "PayTypeID")


        Catch eX As Exception
            Throw eX

        End Try

    End Sub

    Private Sub LoadExtraBills(ByVal ExtraBillNo As String)

        Dim oExtraBills As New SyncSoft.SQLDb.ExtraBills()
        Dim oVisits As New SyncSoft.SQLDb.Visits()

        Try

            Dim ExtraBillsRow As DataRow = oExtraBills.GetExtraBills(ExtraBillNo).Tables("ExtraBills").Rows(0)
            Dim visitsRow As DataRow
            Dim visitNo As String = RevertText(StringEnteredIn(ExtraBillsRow, "VisitNo"))

            visitsRow = oVisits.GetVisits(visitNo).Tables("Visits").Rows(0)

            Dim patientNo As String = RevertText(StringEnteredIn(visitsRow, "PatientNo"))

            Me.stbFullName.Text = StringEnteredIn(visitsRow, "FullName")
            Me.stbPatientNo.Text = FormatText(patientNo, "Patients", "PatientNo")
            Me.stbVisitDate.Text = FormatDate(DateEnteredIn(visitsRow, "VisitDate"))
            Me.stbVisitNo.Text = FormatText(visitNo, "Visits", "VisitNo")
            Me.stbPrimaryDoctor.Text = StringMayBeEnteredIn(visitsRow, "PrimaryDoctor")
            Me.stbMemberCardNo.Text = StringMayBeEnteredIn(visitsRow, "MemberCardNo")
            Me.stbMainMemberName.Text = StringMayBeEnteredIn(visitsRow, "MainMemberName")
            Me.stbClaimReferenceNo.Text = StringMayBeEnteredIn(visitsRow, "ClaimReferenceNo")
            Me.stbBillNo.Text = FormatText(StringEnteredIn(visitsRow, "BillNo"), "BillCustomers", "AccountNo")
            Me.stbBillCustomerName.Text = StringMayBeEnteredIn(visitsRow, "BillCustomerName")
            Me.stbInsuranceNo.Text = FormatText(StringMayBeEnteredIn(visitsRow, "InsuranceNo"), "Insurances", "InsuranceNo")
            Me.stbInsuranceName.Text = StringMayBeEnteredIn(visitsRow, "InsuranceName")
            Me.stbCoPayType.Text = StringMayBeEnteredIn(visitsRow, "CoPayType")
            Me.nbxCoPayPercent.Value = SingleMayBeEnteredIn(visitsRow, "CoPayPercent").ToString()
            Me.nbxCoPayValue.Value = FormatNumber(DecimalMayBeEnteredIn(visitsRow, "CoPayValue"), AppData.DecimalPlaces)
            Me.tipCoPayValueWords.SetToolTip(Me.nbxCoPayValue, NumberToWords(DecimalMayBeEnteredIn(visitsRow, "CoPayValue")))

            Me.stbInvoiceDate.Text = FormatDate(DateEnteredIn(ExtraBillsRow, "ExtraBillDate"))


        Catch eX As Exception
            Throw eX

        End Try

    End Sub

    Private Sub LoadInvoiceDetails(ByVal invoiceNo As String)

        Dim invoiceDetails As New DataTable
        Dim adjustments As New DataTable
        Dim oInvoiceDetails As New SyncSoft.SQLDb.InvoiceDetails()
        Dim oInvoiceExtraBillItems As New SyncSoft.SQLDb.InvoiceExtraBillItems()
        Dim oInvoiceDetailAdjustments As New SyncSoft.SQLDb.InvoiceDetailAdjustments()
        Dim onvoiceExtraBillItemAdjustments As New SyncSoft.SQLDb.InvoiceExtraBillItemAdjustments()

        Try

            Me.Cursor = Cursors.WaitCursor

            Me.dgvInvoiceDetails.Rows.Clear()

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.payTypeID.ToUpper.Equals(oPayTypeID.ExtraBill.ToUpper()) OrElse
                        Me.payTypeID.ToUpper.Equals(oPayTypeID.ExtraBillCASH.ToUpper()) OrElse
                        Me.payTypeID.ToUpper.Equals(oPayTypeID.ExtraBillAccount.ToUpper()) OrElse
                        Me.payTypeID.ToUpper.Equals(oPayTypeID.ExtraBillInsurance.ToUpper()) Then
                invoiceDetails = oInvoiceExtraBillItems.GetInvoiceExtraBillItems(invoiceNo).Tables("InvoiceExtraBillItems")
                adjustments = onvoiceExtraBillItemAdjustments.GetInvoiceExtraBillItemAdjustmentsByInvoiceNo(invoiceNo).Tables("InvoiceExtraBillItemAdjustments")
            ElseIf Me.payTypeID.ToUpper.Equals(oPayTypeID.VisitBill.ToUpper()) OrElse
                                   Me.payTypeID.ToUpper.Equals(oPayTypeID.VisitBillCASH.ToUpper()) OrElse
                                   Me.payTypeID.ToUpper.Equals(oPayTypeID.AccountBill.ToUpper()) OrElse
                                   Me.payTypeID.ToUpper.Equals(oPayTypeID.InsuranceBill.ToUpper()) Then
                invoiceDetails = oInvoiceDetails.GetInvoiceDetails(invoiceNo).Tables("InvoiceDetails")
                adjustments = oInvoiceDetailAdjustments.GetInvoiceDetailAdjustmentsByInvoiceNo(invoiceNo).Tables("InvoiceDetailAdjustments")
            Else
                invoiceDetails = oInvoiceDetails.GetInvoiceDetails(invoiceNo).Tables("InvoiceDetails")
                invoiceDetails.Merge(oInvoiceExtraBillItems.GetInvoiceExtraBillItems(invoiceNo).Tables("InvoiceExtraBillItems"))

                adjustments = onvoiceExtraBillItemAdjustments.GetInvoiceExtraBillItemAdjustmentsByInvoiceNo(invoiceNo).Tables("InvoiceExtraBillItemAdjustments")
                adjustments.Merge(oInvoiceDetailAdjustments.GetInvoiceDetailAdjustmentsByInvoiceNo(invoiceNo).Tables("InvoiceDetailAdjustments"))
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            LoadGridData(Me.dgvInvoiceDetails, invoiceDetails)
            LoadGridData(Me.dgvAdjustments, adjustments)




            If Not Me.payTypeID.ToUpper().Equals(oPayTypeID.VisitBillCASH.ToUpper()) Then
                calculculateDisplayAmount()

            End If


            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            If adjustments.Rows.Count > 0 Then
                If Not tbcPrintInvoices.Contains(tpgAdjustments) Then Me.tbcPrintInvoices.TabPages.Add(tpgAdjustments)
                Me.chkPrintNetFiguresOnly.Visible = True
            Else
                If tbcPrintInvoices.Contains(tpgAdjustments) Then Me.tbcPrintInvoices.TabPages.Remove(tpgAdjustments)
                Me.chkPrintNetFiguresOnly.Visible = False
            End If

            Dim originalInvoicedAmount As Decimal = CalculateGridAmount(dgvInvoiceDetails, colOriginalAmount)
            Dim adjustedAmount As Decimal = CalculateGridAmount(dgvAdjustments, colAdjAMount)
            Dim netInvoicedAmount As Decimal = originalInvoicedAmount - adjustedAmount
            Me.stbInvoiceAmount.Text = FormatNumber(netInvoicedAmount, AppData.DecimalPlaces)
            Me.stbAmountWords.Text = NumberToWords(netInvoicedAmount)



        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


    Private Sub LoadExtraBillItems(ByVal ExtraBillNo As String)

        Dim oExtraBillItems As New SyncSoft.SQLDb.ExtraBillItems()
        Dim oExtraBillItemAdjustments As New SyncSoft.SQLDb.ExtraBillItemAdjustments()
        Dim oAdjustmentTypeID As New LookupDataID.AdjustmentTypeID()


        Try

            Me.Cursor = Cursors.WaitCursor


            Me.dgvInvoiceDetails.Rows.Clear()
            Me.dgvAdjustments.Rows.Clear()
            Me.dgvUpAdjustments.Rows.Clear()
            Me.stbInvoiceAmount.Clear()
            stbAmountWords.Clear()
            Me.stbAmountWords.Clear()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim ExtraBillItems As DataTable = oExtraBillItems.GetExtraBillItemsByExtraBillNo(ExtraBillNo).Tables("ExtraBillItems")

            Dim downAdjustments As DataTable = oExtraBillItemAdjustments.GetExtraBillItemAdjustmentsByExtraBillNo(ExtraBillNo, oAdjustmentTypeID.Down).Tables("ExtraBillItemAdjustments")

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            If ExtraBillItems Is Nothing OrElse ExtraBillItems.Rows.Count < 1 Then Return

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            For pos As Integer = 0 To ExtraBillItems.Rows.Count - 1
                With Me.dgvInvoiceDetails
                    ' Ensure that you add a new row first
                    .Rows.Add()
                    .Item(Me.colItemCode.Name, pos).Value = ExtraBillItems.Rows(pos).Item("ItemCode")
                    .Item(Me.ColMappedCode.Name, pos).Value = ExtraBillItems.Rows(pos).Item("MappedCustomCode")
                    .Item(Me.colItemName.Name, pos).Value = ExtraBillItems.Rows(pos).Item("ItemName")
                    .Item(Me.colCategory.Name, pos).Value = ExtraBillItems.Rows(pos).Item("ItemCategory")
                    .Item(Me.colUnitPrice.Name, pos).Value = FormatNumber(CDec(ExtraBillItems.Rows(pos).Item("UnitPrice").ToString), AppData.DecimalPlaces)
                    .Item(Me.colOriginalAmount.Name, pos).Value = FormatNumber(CDec(ExtraBillItems.Rows(pos).Item("OriginalAmount").ToString), AppData.DecimalPlaces)
                    .Item(Me.colOriginalQuantity.Name, pos).Value = ExtraBillItems.Rows(pos).Item("OriginalQuantity")
                    .Item(Me.colItemCategoryID.Name, pos).Value = ExtraBillItems.Rows(pos).Item("ItemCategoryID")
                    .Item(Me.colQuantityBalance.Name, pos).Value = ExtraBillItems.Rows(pos).Item("Quantity")
                    .Item(Me.colAmountBalance.Name, pos).Value = ExtraBillItems.Rows(pos).Item("Amount")
                    .Item(Me.colPayStatus.Name, pos).Value = ExtraBillItems.Rows(pos).Item("PayStatus")
                    .Item(Me.colDiscount.Name, pos).Value = 0
                End With
            Next
            
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim downRows = downAdjustments.Rows.Count


            If downRows > 0 Then

                If Not tbcPrintInvoices.Contains(tpgAdjustments) Then Me.tbcPrintInvoices.TabPages.Add(tpgAdjustments)
                LoadGridData(Me.dgvAdjustments, downAdjustments)
            Else
                If tbcPrintInvoices.Contains(tpgAdjustments) Then Me.tbcPrintInvoices.TabPages.Remove(tpgAdjustments)
            End If

            Dim upAdjustments As DataTable = oExtraBillItemAdjustments.GetExtraBillItemAdjustmentsByExtraBillNo(ExtraBillNo, oAdjustmentTypeID.Up).Tables("ExtraBillItemAdjustments")
            Dim upRows = upAdjustments.Rows.Count
            If upRows > 0 Then

                If Not tbcPrintInvoices.Contains(tpgUpAdjustments) Then Me.tbcPrintInvoices.TabPages.Add(tpgUpAdjustments)
                LoadGridData(Me.dgvUpAdjustments, upAdjustments)
            Else
                If tbcPrintInvoices.Contains(tpgUpAdjustments) Then Me.tbcPrintInvoices.TabPages.Remove(tpgUpAdjustments)
            End If

            Me.chkPrintNetFiguresOnly.Visible = downRows > 0 Or upRows > 0

            Dim originalInvoicedAmount As Decimal = CalculateGridAmount(dgvInvoiceDetails, colOriginalAmount)
            Dim adjustedAmount As Decimal = CalculateGridAmount(dgvAdjustments, colAdjAMount)
            Dim upAmount As Decimal = CalculateGridAmount(dgvUpAdjustments, colUpAmount)
            Dim netInvoicedAmount As Decimal = originalInvoicedAmount + upAmount - adjustedAmount
            Me.stbInvoiceAmount.Text = FormatNumber(netInvoicedAmount, AppData.DecimalPlaces)
            Me.stbAmountWords.Text = NumberToWords(netInvoicedAmount)


        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub
    Private Sub calculculateDisplayAmount()
        Try
            Dim totalAmount As Decimal = 0
            For rowNo As Integer = 0 To Me.dgvInvoiceDetails.RowCount - 1

                Dim cells As DataGridViewCellCollection = Me.dgvInvoiceDetails.Rows(rowNo).Cells
                Dim itemCode As String = StringEnteredIn(cells, Me.colItemCode, "item!")
                Dim discount = DecimalEnteredIn(cells, Me.colDiscount, False, "Discount!")
                Dim itemCategoryID As String = StringEnteredIn(cells, Me.colItemCategoryID)
                Dim unitPrice As Decimal = 0
                Dim amount As Decimal = 0



                If itemCategoryID.ToUpper().Equals(oItemCategoryID.Extras.ToUpper()) AndAlso
                              (itemCode.ToUpper().Equals(oExtraItemCodes.COPAYVALUE.ToUpper())) Then
                    unitPrice = DecimalEnteredIn(cells, Me.colUnitPrice, True, "unit price!")
                Else
                    unitPrice = DecimalEnteredIn(cells, Me.colUnitPrice, False, "unit price!")
                End If

                amount = (DecimalEnteredIn(cells, Me.colOriginalQuantity, False, "Quantity!") * unitPrice) - discount


                dgvInvoiceDetails.Item(Me.colOriginalAmount.Name, rowNo).Value = FormatNumber(amount, AppData.DecimalPlaces)
            Next

        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Try

            Me.Cursor = Cursors.WaitCursor

            Me.PrintInvoice()

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub



#Region " Invoice Printing "

    Private Sub PrintInvoice()

        Dim dlgPrint As New PrintDialog()

        Try

            Me.Cursor = Cursors.WaitCursor

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.dgvInvoiceDetails.RowCount < 1 Then Throw New ArgumentException("Must set at least one entry on invoice details!")

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.SetVisitInvoicePrintData()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            dlgPrint.Document = docInvoices
            'dlgPrint.AllowPrintToFile = True
            'dlgPrint.AllowSelection = True
            'dlgPrint.AllowSomePages = True
            dlgPrint.Document.PrinterSettings.Collate = True
            If dlgPrint.ShowDialog = DialogResult.OK Then docInvoices.Print()

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub docInvoices_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles docInvoices.PrintPage

        Try


            Dim titleFont As New Font(printFontName, 12, FontStyle.Bold)

            Dim xPos As Single = e.MarginBounds.Left
            Dim yPos As Single = e.MarginBounds.Top

            Dim lineHeight As Single = bodyNormalFont.GetHeight(e.Graphics)
            Dim oProductOwner As ProductOwner = GetProductOwnerInfo()
            Dim title As String = AppData.ProductOwner.ToUpper() + " Invoice".ToUpper()
            Dim fullName As String = StringMayBeEnteredIn(Me.stbFullName)
            Dim invoiceNo As String = StringMayBeEnteredIn(Me.stbInvoiceNo)
            Dim patientNo As String = StringMayBeEnteredIn(Me.stbPatientNo)
            Dim invoiceDate As String = FormatDate(DateMayBeEnteredIn(Me.stbInvoiceDate))
            Dim visitDate As String = StringMayBeEnteredIn(Me.stbVisitDate)
            Dim startDate As String = StringMayBeEnteredIn(Me.stbStartDate)
            Dim endDate As String = StringMayBeEnteredIn(Me.stbEndDate)
            Dim visitNo As String = StringMayBeEnteredIn(Me.stbVisitNo)
            Dim billNo As String = StringMayBeEnteredIn(Me.stbBillNo)
            Dim memberCardNo As String = StringMayBeEnteredIn(Me.stbMemberCardNo)
            Dim mainMemberName As String = StringMayBeEnteredIn(Me.stbMainMemberName)
            Dim claimReferenceNo As String = StringMayBeEnteredIn(Me.stbClaimReferenceNo)
            Dim primaryDoctor As String = StringMayBeEnteredIn(Me.stbPrimaryDoctor)
            Dim billCustomerName As String = StringMayBeEnteredIn(Me.stbBillCustomerName)
            Dim insuranceName As String = StringMayBeEnteredIn(Me.stbInsuranceName)
            Dim payType As String = StringMayBeEnteredIn(Me.stbPayType)

            Dim oAccessObjectNames As New LookupDataID.AccessObjectNames()

            ' Increment the page number.
            pageNo += 1

            With e.Graphics

                'Dim widthTop As Single = .MeasureString("Received from width", titleFont).Width

                Dim widthTopFirst As Single = .MeasureString("W", titleFont).Width
                Dim widthTopSecond As Single = 9 * widthTopFirst
                Dim widthTopThird As Single = 19 * widthTopFirst
                Dim widthTopFourth As Single = 27 * widthTopFirst

                If pageNo < 2 Then

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    If Not oVariousOptions.HideInvoiceHeader Then yPos = PrintPageHeader(e, bodyNormalFont, bodyBoldFont)
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    .DrawString(title, titleFont, Brushes.Black, xPos, yPos)
                    yPos += 3 * lineHeight

                    .DrawString("Patient's Name: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(fullName, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    yPos += lineHeight

                    .DrawString(lblInvoiceNo.Text + ": ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(invoiceNo, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    .DrawString("Patient No: ", bodyNormalFont, Brushes.Black, xPos + widthTopThird, yPos)
                    .DrawString(patientNo, bodyBoldFont, Brushes.Black, xPos + widthTopFourth, yPos)
                    yPos += lineHeight

                    .DrawString(lblInvoiceDate.Text + ": ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(invoiceDate, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    .DrawString("Visit Date: ", bodyNormalFont, Brushes.Black, xPos + widthTopThird, yPos)
                    .DrawString(visitDate, bodyBoldFont, Brushes.Black, xPos + widthTopFourth, yPos)
                    yPos += lineHeight

                    .DrawString("Visit No: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(visitNo, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)
                    .DrawString("To-Bill No: ", bodyNormalFont, Brushes.Black, xPos + widthTopThird, yPos)
                    .DrawString(billNo, bodyBoldFont, Brushes.Black, xPos + widthTopFourth, yPos)
                    yPos += lineHeight

                    If Me.payTypeID.ToUpper.Equals(oPayTypeID.ExtraBill.ToUpper()) OrElse
                        Me.payTypeID.ToUpper.Equals(oPayTypeID.ExtraBillCASH.ToUpper()) OrElse
                        Me.payTypeID.ToUpper.Equals(oPayTypeID.ExtraBillAccount.ToUpper()) OrElse
                        Me.payTypeID.ToUpper.Equals(oPayTypeID.ExtraBillInsurance.ToUpper()) Then


                        If Me.defaultObjectName.Equals(oAccessObjectNames.ExtraBills) Then
                            .DrawString("Ref. Doctor: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                            .DrawString(primaryDoctor, bodyBoldFont, Brushes.Black, xPos + widthTopThird, yPos)
                            yPos += lineHeight
                        Else
                            .DrawString("Pay Type: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                            .DrawString(payType, bodyBoldFont, Brushes.Black, xPos + widthTopSecond, yPos)

                            .DrawString("Ref. Doctor: ", bodyNormalFont, Brushes.Black, xPos + widthTopThird, yPos)
                            .DrawString(primaryDoctor, bodyBoldFont, Brushes.Black, xPos + widthTopFourth, yPos)
                            yPos += lineHeight
                        End If

                       
                    Else
                        .DrawString("Ref. Doctor: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                        .DrawString(primaryDoctor, bodyBoldFont, Brushes.Black, xPos + widthTopThird, yPos)
                        yPos += lineHeight
                    End If

                    If Not String.IsNullOrEmpty(memberCardNo) Then
                        .DrawString("Member Card No: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                        .DrawString(memberCardNo, bodyBoldFont, Brushes.Black, xPos + widthTopThird, yPos)
                        yPos += lineHeight
                    End If

                    If Not String.IsNullOrEmpty(mainMemberName) Then
                        .DrawString("Main Member Name: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                        .DrawString(mainMemberName, bodyBoldFont, Brushes.Black, xPos + widthTopThird, yPos)
                        yPos += lineHeight
                    End If

                    If Not String.IsNullOrEmpty(claimReferenceNo) Then
                        .DrawString("Claim Reference No: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                        .DrawString(claimReferenceNo, bodyBoldFont, Brushes.Black, xPos + widthTopThird, yPos)
                        yPos += lineHeight
                    End If

                    .DrawString("Bill Customer Name: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                    .DrawString(billCustomerName, bodyBoldFont, Brushes.Black, xPos + widthTopThird, yPos)

                    If Not String.IsNullOrEmpty(insuranceName) Then
                        yPos += lineHeight

                        .DrawString("Bill Insurance Name: ", bodyNormalFont, Brushes.Black, xPos, yPos)
                        .DrawString(insuranceName, bodyBoldFont, Brushes.Black, xPos + widthTopThird, yPos)

                    End If

                    If Not String.IsNullOrEmpty(oProductOwner.TINNo) Then

                        yPos += lineHeight
                        .DrawString("TIN No: ", bodyNormalFont, Brushes.Black, xPos + widthTopThird, yPos)
                        .DrawString(oProductOwner.TINNo, bodyBoldFont, Brushes.Black, 35 + widthTopFourth, yPos)


                    End If

                    yPos += 2 * lineHeight

                End If

                Dim _StringFormat As New StringFormat()

                ' Draw the rest of the text left justified,
                ' wrap at words, and don't draw partial lines.

                With _StringFormat
                    .Alignment = StringAlignment.Near
                    .FormatFlags = StringFormatFlags.LineLimit
                    .Trimming = StringTrimming.Word
                End With

                Dim charactersFitted As Integer
                Dim linesFilled As Integer

                If invoiceParagraphs Is Nothing Then Return

                Do While invoiceParagraphs.Count > 0

                    ' Print the next paragraph.
                    Dim oPrintParagraps As PrintParagraps = DirectCast(invoiceParagraphs(1), PrintParagraps)
                    invoiceParagraphs.Remove(1)

                    ' Get the area available for this paragraph.
                    Dim printAreaRectangle As RectangleF = New RectangleF(e.MarginBounds.Left, yPos, e.MarginBounds.Width, e.MarginBounds.Bottom - yPos)

                    ' If the printing area rectangle's height < 1, make it 1.
                    If printAreaRectangle.Height < 1 Then printAreaRectangle.Height = 1

                    ' See how big the text will be and how many characters will fit.
                    Dim textSize As SizeF = .MeasureString(oPrintParagraps.Text, oPrintParagraps.TheFont, _
                        New SizeF(printAreaRectangle.Width, printAreaRectangle.Height), _StringFormat, charactersFitted, linesFilled)

                    ' See if any characters will fit.
                    If charactersFitted > 0 Then
                        ' Draw the text.
                        .DrawString(oPrintParagraps.Text, oPrintParagraps.TheFont, Brushes.Black, printAreaRectangle, _StringFormat)
                        ' Increase the location where we can start, add a little interparagraph spacing.
                        yPos += textSize.Height ' + oPrintParagraps.TheFont.GetHeight(e.Graphics))

                    End If

                    ' See if some of the paragraph didn't fit on the page.
                    If charactersFitted < oPrintParagraps.Text.Length Then
                        ' Some of the paragraph didn't fit, prepare to print the rest on the next page.
                        oPrintParagraps.Text = oPrintParagraps.Text.Substring(charactersFitted)
                        invoiceParagraphs.Add(oPrintParagraps, Before:=1)
                        Exit Do
                    End If
                Loop

                ' If we have more paragraphs, we have more pages.
                e.HasMorePages = (invoiceParagraphs.Count > 0)

            End With

        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Private Function GetInvoiceTotalAmount() As Decimal
        Try
            Return CalculateGridAmount(dgvInvoiceDetails, Me.colOriginalAmount)
        Catch ex As Exception
            Throw ex
        End Try

    End Function



    Private Function GetExtraChargeCategory(itemCategoryID As String, itemCode As String) As String

        Try

            If itemCategoryID.ToUpper().Equals(oItemCategoryID.Extras.ToUpper()) Then
                Return StringEnteredIn(oExtraChargeItem.GetExtraChargeItems(itemCode).Tables("ExtraChargeItems").Rows(0), "ExtraChargeCategory")
            Else : Return String.Empty
            End If


        Catch ex As Exception
            Throw ex
        End Try

    End Function


    Private Sub SetVisitInvoicePrintData()

        Dim padTotalAmount As Integer = 60
        Dim footerFont As New Font(printFontName, 9)

        pageNo = 0
        invoiceParagraphs = New Collection()
        Dim oVariousOptions As New VariousOptions()

        Try

            Dim oCoPayTypeID As New LookupDataID.CoPayTypeID()
            Dim oAccessObjectNames As New LookupDataID.AccessObjectNames()

            Dim originalBody As StringBuilder = Me.GetOriginalVisitInvoicePrintData
            Dim adjustmentBody As StringBuilder = Me.GetVisitInvoiceAdjustmentPrintData()
            Dim netInvoiceData As StringBuilder = GetNetVisitInvoicePrintData()

            Dim totalLebel As String = "Total Amount: "
            Dim seperator As New System.Text.StringBuilder(String.Empty)
            Dim totalAmount As Decimal = GetInvoiceTotalAmount()
            Dim adjustedAmount As Decimal = CalculateGridAmount(dgvAdjustments, colAdjAMount)
            Dim netAmount As String = stbInvoiceAmount.Text
            Dim printOptionID As String = StringValueMayBeEnteredIn(cboPrintOptions)

            ' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            If chkPrintNetFiguresOnly.Checked Then
                invoiceParagraphs.Add(New PrintParagraps(bodyNormalFont, netInvoiceData.ToString()))

            Else
                invoiceParagraphs.Add(New PrintParagraps(bodyNormalFont, originalBody.ToString()))

                If adjustmentBody.Length > 0 Then
                    seperator.Append(ControlChars.NewLine)
                    seperator.Append("Adjustments")
                    invoiceParagraphs.Add(New PrintParagraps(bodyNormalFont, seperator.ToString()))
                    invoiceParagraphs.Add(New PrintParagraps(bodyNormalFont, adjustmentBody.ToString()))
                    totalLebel = "Net Amount: "
                End If
            End If


            If printOptionID.ToUpper().Equals(oPrintOptionID.GroupedByItemName().ToUpper()) Then
                padTotalAmount = 38
            ElseIf printOptionID.ToUpper().Equals(oPrintOptionID.GroupedByItemCategory().ToUpper()) Then
                padTotalAmount = 36
            ElseIf printOptionID.ToUpper().Equals(oPrintOptionID.PrintItemCodes().ToUpper()) Then
                padTotalAmount = 54
            Else : padTotalAmount = 54
            End If
            Dim totalAmountData As New System.Text.StringBuilder(String.Empty)

            totalAmountData.Append(ControlChars.NewLine)
            totalAmountData.Append(totalLebel)
            totalAmountData.Append(netAmount.PadLeft(padTotalAmount))
            totalAmountData.Append(ControlChars.NewLine)
            invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, totalAmountData.ToString()))

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim coPayType As String = StringMayBeEnteredIn(Me.stbCoPayType)

            If coPayType.ToUpper().Equals(GetLookupDataDes(oCoPayTypeID.Percent).ToUpper()) AndAlso (Me.payTypeID.Equals(oPayTypeID.VisitBill()) OrElse Me.payTypeID.Equals(oPayTypeID.ExtraBill())) Then

                Dim coPayPercent As Single = Me.nbxCoPayPercent.GetSingle()
                Dim coPayAmount As Decimal = CDec(totalAmount * coPayPercent) / 100
                Dim balanceDue As Decimal = totalAmount - coPayAmount

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim coPayAmountData As New System.Text.StringBuilder(String.Empty)
                coPayAmountData.Append("Co-Pay Amount: ")
                coPayAmountData.Append(FormatNumber(coPayAmount, AppData.DecimalPlaces).PadLeft(padTotalAmount - 1))
                coPayAmountData.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, coPayAmountData.ToString()))

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim balanceDueData As New System.Text.StringBuilder(String.Empty)
                balanceDueData.Append("Balance Due: ")
                balanceDueData.Append(FormatNumber(balanceDue, AppData.DecimalPlaces).PadLeft(padTotalAmount + 1))
                balanceDueData.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, balanceDueData.ToString()))

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim balanceDueWordsData As New System.Text.StringBuilder(String.Empty)
                balanceDueWordsData.Append("(" + NumberToWords(balanceDue).Trim() + " ONLY)")
                balanceDueWordsData.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(footerFont, balanceDueWordsData.ToString()))

            Else
                Dim totalAmountWords As New System.Text.StringBuilder(String.Empty)
                Dim amountWords As String = stbAmountWords.Text

                totalAmountWords.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(footerFont, totalAmountWords.ToString()))
            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Not oVariousOptions.DisablePatientSignOnInvoices Then

                Dim patientSignData As New System.Text.StringBuilder(String.Empty)
                patientSignData.Append(ControlChars.NewLine)
                patientSignData.Append("Patient's Sign:   " + GetCharacters("."c, 20))
                patientSignData.Append(GetSpaces(4))
                patientSignData.Append("Date:  " + GetCharacters("."c, 20))
                patientSignData.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(footerFont, patientSignData.ToString()))

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim checkedSignData As New System.Text.StringBuilder(String.Empty)
                checkedSignData.Append(ControlChars.NewLine)

                checkedSignData.Append("Checked By:       " + GetCharacters("."c, 20))
                checkedSignData.Append(GetSpaces(4))
                checkedSignData.Append("Date:  " + GetCharacters("."c, 20))
                checkedSignData.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(footerFont, checkedSignData.ToString()))

            End If

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim footerData As New System.Text.StringBuilder(String.Empty)
            footerData.Append(ControlChars.NewLine)
            footerData.Append("Printed by " + CurrentUser.FullName + " on " + FormatDate(Now) + " at " +
                              Now.ToString("hh:mm tt") + " from " + AppData.AppTitle)
            footerData.Append(ControlChars.NewLine)
            invoiceParagraphs.Add(New PrintParagraps(footerFont, footerData.ToString()))

        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Private Function GetOriginalVisitInvoicePrintData() As StringBuilder

        Dim padItemNo As Integer = 4
        Dim padItemName As Integer = 27
        Dim padQuantity As Integer = 6
        Dim padUnitPrice As Integer = 14
        Dim padDiscount As Integer = 7
        Dim padAmount As Integer = 16
        Dim padTotalAmount As Integer = 60

        Dim padCategoryName As Integer = 36
        Dim padCategoryAmount As Integer = 20

        Dim footerFont As New Font(printFontName, 9)

        pageNo = 0
        invoiceParagraphs = New Collection()
        Dim oVariousOptions As New VariousOptions()

        Try

            Dim oCoPayTypeID As New LookupDataID.CoPayTypeID()

            Dim count As Integer = 0
            Dim tableHeader As New System.Text.StringBuilder(String.Empty)
            Dim tableData As New System.Text.StringBuilder(String.Empty)
            Dim printOptionID As String = StringValueMayBeEnteredIn(Me.cboPrintOptions)


            If printOptionID.ToUpper().Equals(oPrintOptionID.GroupedByItemName.ToUpper()) Then
                padQuantity = 10
                padAmount = 16

                tableHeader.Append("No: ".PadRight(padItemNo))
                tableHeader.Append("Item Name: ".PadRight(padItemName))
                tableHeader.Append("Qty: ".PadRight(padQuantity))
                tableHeader.Append("Amount: ".PadRight(padAmount))

                tableHeader.Append(ControlChars.NewLine)
                tableHeader.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))

                Dim lToPrintInvoiceDetails = From data In GetToPrintOriginalInvoiceDetails() Group By itemName = data.Item2 Into quantity = Sum(data.Item3), amount = Sum(data.Item6)

                For Each item In lToPrintInvoiceDetails


                    count += 1
                    Dim itemNo As String = (count).ToString()
                    Dim itemName As String = item.itemName
                    Dim quantity As String = item.quantity.ToString()
                    Dim amount As String = FormatNumber(item.amount, AppData.DecimalPlaces)


                    tableData.Append(itemNo.PadRight(padItemNo))
                    Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
                    If wrappeditemName.Count > 1 Then
                        For pos As Integer = 0 To wrappeditemName.Count - 1
                            tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
                            If Not pos = wrappeditemName.Count - 1 Then
                                tableData.Append(ControlChars.NewLine)
                                tableData.Append(GetSpaces(padItemNo))
                            Else
                                tableData.Append(quantity.PadRight(padQuantity))
                                tableData.Append(amount.PadRight(padAmount))
                            End If
                        Next

                    Else
                        tableData.Append(FixDataLength(itemName, padItemName))
                        tableData.Append(quantity.PadRight(padQuantity))
                        tableData.Append(amount.PadRight(padAmount))
                    End If

                    tableData.Append(ControlChars.NewLine)
                Next

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Return tableData
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            ElseIf printOptionID.ToUpper().Equals(oPrintOptionID.GroupedByItemCategory().ToUpper()) Then
                tableHeader.Append("No: ".PadRight(padItemNo))
                tableHeader.Append("Item Category: ".PadRight(padCategoryName))
                tableHeader.Append("Amount: ".PadRight(padCategoryAmount))
                tableHeader.Append(ControlChars.NewLine)
                tableHeader.Append(ControlChars.NewLine)
                ' tableData.Append(tableHeader.ToString())
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim extraChargeItemCategoryDes As String = GetLookupDataDes(oItemCategoryID.Extras)

                Dim invoiceDetails = From data In GetToPrintOriginalInvoiceDetails() Group By CategoryName = data.Item1 Into categoryAmount = Sum(data.Item6)
                                     Select CategoryName, categoryAmount
                                     Where Not CategoryName = extraChargeItemCategoryDes


                Dim extraChargeCategoryInvoiceDetails = From data In GetToPrintOriginalInvoiceDetails() Group By categoryName = data.Item1, ExtraChargeCategoryName = data.Item7 Into ExtraChargeCategoryAmount = Sum(data.Item6)
                                     Select categoryName, ExtraChargeCategoryName, ExtraChargeCategoryAmount
                                     Where categoryName = extraChargeItemCategoryDes


                For Each item In invoiceDetails

                    count += 1

                    Dim itemNo As String = (count).ToString()
                    Dim categoryName As String = GetPrintableItemCategoryDes(item.CategoryName)

                    Dim categoryAmount As String = FormatNumber(item.categoryAmount, AppData.DecimalPlaces)

                    tableData.Append(itemNo.PadRight(padItemNo))

                    If categoryName.Length > padCategoryName Then
                        tableData.Append(categoryName.Substring(0, padCategoryName).PadRight(padCategoryName))
                    Else : tableData.Append(categoryName.PadRight(padCategoryName))
                    End If
                    tableData.Append(categoryAmount.PadRight(padCategoryAmount))
                    tableData.Append(ControlChars.NewLine)

                Next

                If extraChargeCategoryInvoiceDetails.Count() > 0 Then
                    count += 1
                    Dim extraCount As Integer = 0
                    Dim extraChargetableData As New System.Text.StringBuilder(String.Empty)
                    tableData.Append(count.ToString().PadRight(padItemNo))
                    extraChargetableData.Append("Extras")
                    extraChargetableData.Append(ControlChars.NewLine)
                    For Each item In extraChargeCategoryInvoiceDetails


                        Dim itemNo As String = (extraCount).ToString()
                        Dim extraChargeItemCategory As String = item.ExtraChargeCategoryName

                        Dim extraChargeItemCategoryAmount As String = FormatNumber(item.ExtraChargeCategoryAmount, AppData.DecimalPlaces)
                        extraChargetableData.Append(String.Empty.PadRight(padItemNo + 2))



                        If extraChargeItemCategory.Length > padCategoryName Then
                            extraChargetableData.Append(extraChargeItemCategory.Substring(0, padCategoryName).PadRight((padCategoryName)))
                        Else : extraChargetableData.Append(extraChargeItemCategory.PadRight((padCategoryName)))
                        End If
                        extraChargetableData.Append(extraChargeItemCategoryAmount.PadRight(padCategoryAmount))
                        extraChargetableData.Append(ControlChars.NewLine)

                    Next
                    tableData.Append(extraChargetableData)
                End If
                Return tableData
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            ElseIf printOptionID.ToUpper().Equals(oPrintOptionID.PrintItemCodes.ToUpper()) Then
                padItemNo = 10
                tableHeader.Append("Code: ".PadRight(padItemNo))
                tableHeader.Append("Item Name: ".PadRight(padItemName))
                tableHeader.Append("Qty: ".PadRight(padQuantity))
                tableHeader.Append("Unit Price: ".PadRight(padUnitPrice))
                tableHeader.Append("Amount: ".PadRight(padAmount))

                tableHeader.Append(ControlChars.NewLine)
                tableHeader.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))

                For rowNo As Integer = 0 To Me.dgvInvoiceDetails.RowCount - 1

                    Dim cells As DataGridViewCellCollection = Me.dgvInvoiceDetails.Rows(rowNo).Cells
                    count += 1

                    'im itemNo As String = (count).ToString()
                    Dim itemNo As String = cells.Item(Me.colItemCode.Name).Value.ToString()
                    Dim itemName As String = cells.Item(Me.colItemName.Name).Value.ToString()
                    Dim quantity As String = cells.Item(Me.colOriginalQuantity.Name).Value.ToString()
                    Dim unitPrice As String = FormatNumber(CDec(cells.Item(Me.colUnitPrice.Name).Value.ToString()), AppData.DecimalPlaces)
                    'im discount As String = cells.Item(Me.colDiscount.Name).Value.ToString()
                    Dim amount As String = FormatNumber(CDec(cells.Item(Me.colOriginalAmount.Name).Value.ToString()), AppData.DecimalPlaces)



                    tableData.Append(itemNo.PadRight(padItemNo))
                    Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
                    If wrappeditemName.Count > 1 Then
                        For pos As Integer = 0 To wrappeditemName.Count - 1
                            tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
                            If Not pos = wrappeditemName.Count - 1 Then
                                tableData.Append(ControlChars.NewLine)
                                tableData.Append(GetSpaces(padItemNo))
                            Else
                                tableData.Append(quantity.PadRight(padQuantity))
                                tableData.Append(unitPrice.PadRight(padUnitPrice))
                                tableData.Append(amount.PadRight(padAmount))
                            End If
                        Next

                    Else
                        tableData.Append(FixDataLength(itemName, padItemName))
                        tableData.Append(quantity.PadRight(padQuantity))
                        tableData.Append(unitPrice.PadRight(padUnitPrice))
                        'tableData.Append(discount.PadLeft(padDiscount))
                        tableData.Append(amount.PadRight(padAmount))
                    End If

                    tableData.Append(ControlChars.NewLine)
                Next

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Return tableData
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Else

                tableHeader.Append("No: ".PadRight(padItemNo))
                tableHeader.Append("Item Name: ".PadRight(padItemName))
                tableHeader.Append("Qty: ".PadLeft(padQuantity))
                tableHeader.Append("Unit Price: ".PadRight(padUnitPrice))
                tableHeader.Append("Disc: ".PadRight(padDiscount))
                tableHeader.Append("Amount: ".PadRight(padAmount))

                tableHeader.Append(ControlChars.NewLine)
                tableHeader.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))

                For rowNo As Integer = 0 To Me.dgvInvoiceDetails.RowCount - 1

                    Dim cells As DataGridViewCellCollection = Me.dgvInvoiceDetails.Rows(rowNo).Cells
                    count += 1

                    Dim itemNo As String = (count).ToString()
                    Dim itemName As String = cells.Item(Me.colItemName.Name).Value.ToString()
                    Dim quantity As String = cells.Item(Me.colOriginalQuantity.Name).Value.ToString()
                    Dim unitPrice As String = FormatNumber(CDec(cells.Item(Me.colUnitPrice.Name).Value.ToString()), AppData.DecimalPlaces)
                    Dim discount As String = cells.Item(Me.colDiscount.Name).Value.ToString()
                    Dim amount As String = FormatNumber(CDec(cells.Item(Me.colOriginalAmount.Name).Value.ToString()), AppData.DecimalPlaces)


                    tableData.Append(itemNo.PadRight(padItemNo))
                    Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
                    If wrappeditemName.Count > 1 Then
                        For pos As Integer = 0 To wrappeditemName.Count - 1
                            tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
                            If Not pos = wrappeditemName.Count - 1 Then
                                tableData.Append(ControlChars.NewLine)
                                tableData.Append(GetSpaces(padItemNo))
                            Else
                                tableData.Append(quantity.PadRight(padQuantity))
                                tableData.Append(unitPrice.PadRight(padUnitPrice))
                                tableData.Append(discount.PadRight(padDiscount))
                                tableData.Append(amount.PadRight(padAmount))
                            End If
                        Next

                    Else
                        tableData.Append(FixDataLength(itemName, padItemName))
                        tableData.Append(quantity.PadRight(padQuantity))
                        tableData.Append(unitPrice.PadRight(padUnitPrice))
                        tableData.Append(discount.PadRight(padDiscount))
                        tableData.Append(amount.PadRight(padAmount))
                    End If

                    tableData.Append(ControlChars.NewLine)
                Next

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Return tableData
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



            End If




        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Function GetToPrintOriginalInvoiceDetails() As List(Of Tuple(Of String, String, Integer, Decimal, Decimal, Decimal, String))
        Dim lToPrintInvoiceDetails As New List(Of Tuple(Of String, String, Integer, Decimal, Decimal, Decimal, String))
        For rowNo As Integer = 0 To Me.dgvInvoiceDetails.RowCount - 1

            Dim cells As DataGridViewCellCollection = Me.dgvInvoiceDetails.Rows(rowNo).Cells
            Dim itemCategoryID As String = cells.Item(Me.colItemCategoryID.Name).Value.ToString()
            Dim itemCategory As String = cells.Item(Me.colCategory.Name).Value.ToString()
            Dim itemCode As String = cells.Item(Me.colItemCode.Name).Value.ToString()
            Dim itemName As String = cells.Item(Me.colItemName.Name).Value.ToString()
            Dim quantity As Integer = IntegerMayBeEnteredIn(cells, Me.colOriginalQuantity)
            Dim unitPrice As Decimal = DecimalMayBeEnteredIn(cells, Me.colUnitPrice)
            Dim discount As Decimal = DecimalMayBeEnteredIn(cells, Me.colDiscount)
            Dim amount As Decimal = DecimalMayBeEnteredIn(cells, Me.colOriginalAmount)
            Dim extraChargeCategory As String = Me.GetExtraChargeCategory(itemCategoryID, itemCode)
            lToPrintInvoiceDetails.Add(New Tuple(Of String, String, Integer, Decimal, Decimal, Decimal, String)(itemCategory, itemName, quantity, unitPrice, discount, amount, extraChargeCategory))

        Next

        Return lToPrintInvoiceDetails
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Function


#Region "Net Invoice"

    'Private Function GetNetVisitInvoicePrintData() As StringBuilder

    '    Dim padItemNo As Integer = 4
    '    Dim padItemName As Integer = 27
    '    Dim padQuantity As Integer = 4
    '    Dim padUnitPrice As Integer = 14
    '    Dim padDiscount As Integer = 7
    '    Dim padAmount As Integer = 16
    '    Dim padTotalAmount As Integer = 60


    '    Dim padICItemNo As Integer = 9
    '    Dim padICItemName As Integer = 34
    '    Dim padICQuantity As Integer = 4
    '    Dim padICUnitPrice As Integer = 14
    '    Dim padICDiscount As Integer = 9
    '    Dim padICAmount As Integer = 14
    '    Dim padICTotalAmount As Integer = 60

    '    Dim padAmountTendered As Integer = 53
    '    Dim padBalance As Integer = 56

    '    Dim padCategoryName As Integer = 47
    '    Dim padCategoryAmount As Integer = 20

    '    Dim footerFont As New Font(printFontName, 9)

    '    pageNo = 0
    '    invoiceParagraphs = New Collection()
    '    Dim oVariousOptions As New VariousOptions()

    '    Try

    '        Dim oCoPayTypeID As New LookupDataID.CoPayTypeID()

    '        Dim countExtra As Integer
    '        Dim count As Integer
    '        Dim tableHeader As New System.Text.StringBuilder(String.Empty)
    '        Dim tableData As New System.Text.StringBuilder(String.Empty)


    '        If chkPrintInvoiceDetailedOnSaving.Checked = True Then



    '            For rowNo As Integer = 0 To Me.dgvInvoiceDetails.RowCount - 1

    '                Dim cells As DataGridViewCellCollection = Me.dgvInvoiceDetails.Rows(rowNo).Cells



    '                Dim amount As String = cells.Item(Me.colAmountBalance.Name).Value.ToString()

    '                If CDec(amount) > 0 Then
    '                    count += 1
    '                    Dim itemNo As String = (count).ToString()
    '                    Dim itemName As String = cells.Item(Me.colItemName.Name).Value.ToString()
    '                    Dim quantity As String = cells.Item(Me.colQuantityBalance.Name).Value.ToString()
    '                    Dim unitPrice As String = cells.Item(Me.colUnitPrice.Name).Value.ToString()
    '                    Dim discount As String = cells.Item(Me.colDiscount.Name).Value.ToString()

    '                    tableData.Append(itemNo.PadRight(padItemNo))
    '                    Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
    '                    If wrappeditemName.Count > 1 Then
    '                        For pos As Integer = 0 To wrappeditemName.Count - 1
    '                            tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
    '                            If Not pos = wrappeditemName.Count - 1 Then
    '                                tableData.Append(ControlChars.NewLine)
    '                                tableData.Append(GetSpaces(padItemNo))
    '                            Else
    '                                tableData.Append(quantity.PadLeft(padQuantity))
    '                                tableData.Append(unitPrice.PadLeft(padUnitPrice))
    '                                tableData.Append(discount.PadLeft(padDiscount))
    '                                tableData.Append(amount.PadLeft(padAmount))
    '                            End If
    '                        Next

    '                    Else
    '                        tableData.Append(FixDataLength(itemName, padItemName))
    '                        tableData.Append(quantity.PadLeft(padQuantity))
    '                        tableData.Append(unitPrice.PadLeft(padUnitPrice))
    '                        tableData.Append(discount.PadLeft(padDiscount))
    '                        tableData.Append(amount.PadLeft(padAmount))
    '                    End If

    '                    tableData.Append(ControlChars.NewLine)
    '                End If
    '            Next

    '            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '            Return tableData
    '            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    '        Else

    '            If oVariousOptions.CategorizeVisitInvoiceByItemCategory Then


    '                invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))
    '                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '                Dim invoiceDetails = From data In Me.GetNetInvoiceDetailsList Group data By CategoryName = data.Item1
    '                                     Into CategoryAmount = Sum(data.Item2) Select CategoryName, CategoryAmount Where Not CategoryName = GetLookupDataDes(oItemCategoryID.Extras)


    '                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '                Dim extraChargeCategoryInvoiceDetails = From data In Me.GetNetInvoiceDetailsExtraChargeList Group By ExtraChargeItemCategory = data.Item1
    '                                   Into ExtraChargeItemCategoryAmount = Sum(data.Item2) Select ExtraChargeItemCategory, ExtraChargeItemCategoryAmount



    '                For Each item In invoiceDetails




    '                    Dim categoryAmount As String = FormatNumber(item.CategoryAmount, AppData.DecimalPlaces)
    '                    If CDec(categoryAmount) > 0 Then
    '                        count += 1
    '                        Dim itemNo As String = (count).ToString()
    '                        Dim categoryName As String = GetPrintableItemCategoryDes(item.CategoryName)

    '                        tableData.Append(itemNo.PadRight(padItemNo))

    '                        If categoryName.Length > 47 Then
    '                            tableData.Append(categoryName.Substring(0, 47).PadRight(padItemName + 20))
    '                        Else : tableData.Append(categoryName.PadRight(padItemName + 20))
    '                        End If
    '                        tableData.Append(categoryAmount.PadLeft(padAmount))
    '                        tableData.Append(ControlChars.NewLine)
    '                    End If
    '                Next

    '                If extraChargeCategoryInvoiceDetails.Count() > 0 Then
    '                    count += 1
    '                    Dim extraCount As Integer = 0
    '                    Dim extraChargetableData As New System.Text.StringBuilder(String.Empty)
    '                    tableData.Append(count.ToString().PadRight(padItemNo))
    '                    extraChargetableData.Append("Extras")
    '                    extraChargetableData.Append(ControlChars.NewLine)
    '                    For Each item In extraChargeCategoryInvoiceDetails


    '                        Dim itemNo As String = (extraCount).ToString()
    '                        Dim extraChargeItemCategory As String = item.ExtraChargeItemCategory

    '                        Dim extraChargeItemCategoryAmount As String = FormatNumber(item.ExtraChargeItemCategoryAmount, AppData.DecimalPlaces)
    '                        extraChargetableData.Append(String.Empty.PadRight(padItemNo + 2))



    '                        If extraChargeItemCategory.Length > 47 Then
    '                            extraChargetableData.Append(extraChargeItemCategory.Substring(0, 47).PadRight((padItemName + 18)))
    '                        Else : extraChargetableData.Append(extraChargeItemCategory.PadRight((padItemName + 18)))
    '                        End If
    '                        extraChargetableData.Append(extraChargeItemCategoryAmount.PadLeft(padAmount))
    '                        extraChargetableData.Append(ControlChars.NewLine)

    '                    Next
    '                    tableData.Append(extraChargetableData)
    '                End If
    '                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '                Return tableData
    '                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''




    '            ElseIf oVariousOptions.CategorizeVisitInvoiceDetails Then

    '                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    '                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '                Dim invoiceDetails = From data In Me.GetNetInvoiceDetailsList Group data By CategoryName = data.Item1
    '                                     Into CategoryAmount = Sum(data.Item2) Select CategoryName, CategoryAmount


    '                Dim invoiceDetailQuantities = From data In Me.GetNetInvoiceDetailsList Group data By CategoryName = data.Item1
    '                                    Into CategoryAmount = Sum(data.Item2) Select CategoryName, CategoryAmount


    '                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


    '                For Each item In invoiceDetails

    '                    Dim categoryAmount As String = FormatNumber(item.CategoryAmount, AppData.DecimalPlaces)
    '                    If CDec(categoryAmount) > 0 Then

    '                        count += 1

    '                        Dim itemNo As String = (count).ToString()
    '                        Dim categoryName As String = GetPrintableItemCategoryDes(item.CategoryName)

    '                        tableData.Append(GetSpaces(padItemNo))
    '                        If categoryName.Length > 47 Then
    '                            tableData.Append(categoryName.Substring(0, 47).PadRight(padItemName))
    '                        Else : tableData.Append(categoryName.PadRight(padItemName))
    '                        End If
    '                        tableData.Append(ControlChars.NewLine)
    '                        tableData.Append(ControlChars.NewLine)

    '                        Dim categorizedInvoiceDetails = (From data In Me.GetNetCategorizedInvoiceDetails Group data By CategoryNameExtra = data.Item1, itemName = data.Item2, itemQuantity = data.Item3, itemUnitPrice = data.Item4,
    '                                        itemDiscount = data.Item5, itemAmount = data.Item6 Into CategoryAmount2 = Sum(data.Item6) Select CategoryNameExtra, itemName, itemQuantity, itemUnitPrice, itemDiscount, itemAmount Where
    '                                        CategoryNameExtra.ToUpper().Equals(FormatText(categoryName, "ItemCategory", "ItemCategory").ToUpper()) Or
    '                                        CategoryNameExtra.ToUpper().Equals(categoryName.ToUpper()))

    '                        For Each categorizedItem In categorizedInvoiceDetails
    '                            countExtra += 1
    '                            Dim categorizedItemNo As String = (countExtra).ToString()
    '                            Dim itemName As String = categorizedItem.itemName
    '                            Dim categorizedInvoiceDetailQuantity = From data In Me.GetNetCategorizedInvoiceDetails Group data By data.Item2 Into totalQuantity = Sum(data.Item3)
    '                                                                   Select totalQuantity, Item2
    '                                                                   Where Item2 = itemName

    '                            Dim itemQuantity As String = categorizedInvoiceDetailQuantity.ElementAt(0).totalQuantity.ToString
    '                            Dim ItemUnitPrice As String = FormatNumber(categorizedItem.itemUnitPrice, AppData.DecimalPlaces)
    '                            Dim itemDiscount As String = FormatNumber(categorizedItem.itemDiscount, AppData.DecimalPlaces)
    '                            Dim itemAmount As String = FormatNumber(categorizedItem.itemAmount, AppData.DecimalPlaces)

    '                            tableData.Append(categorizedItemNo.PadRight(padItemNo))

    '                            Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
    '                            If wrappeditemName.Count > 1 Then
    '                                For pos As Integer = 0 To wrappeditemName.Count - 1
    '                                    tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
    '                                    If Not pos = wrappeditemName.Count - 1 Then
    '                                        tableData.Append(ControlChars.NewLine)
    '                                        tableData.Append(GetSpaces(padItemNo))
    '                                    Else
    '                                        tableData.Append(itemQuantity.PadLeft(padQuantity))
    '                                        tableData.Append(itemAmount.PadLeft(padAmount))
    '                                    End If
    '                                Next

    '                            Else
    '                                tableData.Append(FixDataLength(itemName, padItemName))
    '                                tableData.Append(itemQuantity.PadLeft(padQuantity))
    '                                tableData.Append(itemAmount.PadLeft(padAmount))
    '                            End If
    '                            tableData.Append(ControlChars.NewLine)
    '                        Next
    '                        tableData.Append("SUB TOTAL".PadLeft(padItemName - 7))
    '                        tableData.Append(categoryAmount.PadLeft(padAmount + 41))
    '                        tableData.Append(ControlChars.NewLine)
    '                        tableData.Append(ControlChars.NewLine)
    '                        countExtra = 0
    '                    End If
    '                Next
    '                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '                Return tableData
    '                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    '            ElseIf oVariousOptions.PrintItemCodesOnInvoices Then


    '                For rowNo As Integer = 0 To Me.dgvInvoiceDetails.RowCount - 1

    '                    Dim cells As DataGridViewCellCollection = Me.dgvInvoiceDetails.Rows(rowNo).Cells


    '                    Dim amount As String = cells.Item(Me.colAmountBalance.Name).Value.ToString()
    '                    If CDec(amount) > 0 Then
    '                        count += 1
    '                        Dim itemNo As String = cells.Item(Me.colItemCode.Name).Value.ToString()
    '                        Dim itemName As String = cells.Item(Me.colItemName.Name).Value.ToString()
    '                        Dim quantity As String = cells.Item(Me.colQuantityBalance.Name).Value.ToString()
    '                        Dim unitPrice As String = cells.Item(Me.colUnitPrice.Name).Value.ToString()

    '                        tableData.Append(itemNo.PadRight(padICItemNo))
    '                        Dim wrappeditemName As List(Of String) = WrapText(itemName, padICItemName)
    '                        If wrappeditemName.Count > 1 Then
    '                            For pos As Integer = 0 To wrappeditemName.Count - 1
    '                                tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padICItemName))
    '                                If Not pos = wrappeditemName.Count - 1 Then
    '                                    tableData.Append(ControlChars.NewLine)
    '                                    tableData.Append(GetSpaces(padICItemNo))
    '                                Else
    '                                    tableData.Append(quantity.PadLeft(padICQuantity))
    '                                    tableData.Append(unitPrice.PadLeft(padICUnitPrice))
    '                                    'tableData.Append(discount.PadLeft(padDiscount))
    '                                    tableData.Append(amount.PadLeft(padICAmount))
    '                                End If
    '                            Next

    '                        Else
    '                            tableData.Append(FixDataLength(itemName, padICItemName))
    '                            tableData.Append(quantity.PadLeft(padICQuantity))
    '                            tableData.Append(unitPrice.PadLeft(padICUnitPrice))
    '                            'tableData.Append(discount.PadLeft(padDiscount))
    '                            tableData.Append(amount.PadLeft(padICAmount))
    '                        End If

    '                        tableData.Append(ControlChars.NewLine)

    '                    End If
    '                Next

    '                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '                Return tableData
    '                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    '                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '            Else


    '                For rowNo As Integer = 0 To Me.dgvInvoiceDetails.RowCount - 1

    '                    Dim cells As DataGridViewCellCollection = Me.dgvInvoiceDetails.Rows(rowNo).Cells


    '                    Dim amount As String = cells.Item(Me.colAmountBalance.Name).Value.ToString()

    '                    If CDec(amount) > 0 Then
    '                        count += 1

    '                        Dim itemNo As String = (count).ToString()
    '                        Dim itemName As String = cells.Item(Me.colItemName.Name).Value.ToString()
    '                        Dim quantity As String = cells.Item(Me.colQuantityBalance.Name).Value.ToString()
    '                        Dim unitPrice As String = cells.Item(Me.colUnitPrice.Name).Value.ToString()
    '                        Dim discount As String = cells.Item(Me.colDiscount.Name).Value.ToString()

    '                        tableData.Append(itemNo.PadRight(padItemNo))
    '                        Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
    '                        If wrappeditemName.Count > 1 Then
    '                            For pos As Integer = 0 To wrappeditemName.Count - 1
    '                                tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
    '                                If Not pos = wrappeditemName.Count - 1 Then
    '                                    tableData.Append(ControlChars.NewLine)
    '                                    tableData.Append(GetSpaces(padItemNo))
    '                                Else
    '                                    tableData.Append(quantity.PadLeft(padQuantity))
    '                                    tableData.Append(unitPrice.PadLeft(padUnitPrice))
    '                                    tableData.Append(discount.PadLeft(padDiscount))
    '                                    tableData.Append(amount.PadLeft(padAmount))
    '                                End If
    '                            Next

    '                        Else
    '                            tableData.Append(FixDataLength(itemName, padItemName))
    '                            tableData.Append(quantity.PadLeft(padQuantity))
    '                            tableData.Append(unitPrice.PadLeft(padUnitPrice))
    '                            tableData.Append(discount.PadLeft(padDiscount))
    '                            tableData.Append(amount.PadLeft(padAmount))
    '                        End If

    '                        tableData.Append(ControlChars.NewLine)
    '                    End If
    '                Next

    '                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '                Return tableData
    '                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



    '            End If
    '        End If



    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Function


    Private Function GetToPrintNetVisitInvoiceDetails() As List(Of Tuple(Of String, String, Integer, Decimal, Decimal, Decimal, String))

        Try

            Dim invoiceDetailsList As New List(Of Tuple(Of String, String, Integer, Decimal, Decimal, Decimal, String))

            For rowNo As Integer = 0 To Me.dgvInvoiceDetails.RowCount - 1

                Dim cells As DataGridViewCellCollection = Me.dgvInvoiceDetails.Rows(rowNo).Cells
                Dim category As String = cells.Item(Me.colCategory.Name).Value.ToString()
                Dim itemName As String = cells.Item(Me.colItemName.Name).Value.ToString()
                Dim itemCategoryID As String = cells.Item(Me.colItemCategoryID.Name).Value.ToString()
                Dim itemCode As String = cells.Item(Me.colItemCode.Name).Value.ToString()
                Dim quantity As Integer = IntegerEnteredIn(cells, Me.colQuantityBalance, "quantity!")
                Dim unitPrice As Decimal = DecimalEnteredIn(cells, Me.colUnitPrice, False, "unitPrice!")
                Dim discount As Decimal = DecimalEnteredIn(cells, Me.colDiscount, False, "discount!")
                Dim amount As Decimal = DecimalEnteredIn(cells, Me.colAmountBalance, False, "amount!")
                Dim extraChargeCategory As String = Me.GetExtraChargeCategory(itemCategoryID, itemCode)

                invoiceDetailsList.Add(New Tuple(Of String, String, Integer, Decimal, Decimal, Decimal, String)(category, itemName, quantity, unitPrice, discount, amount, extraChargeCategory))

            Next

            Return invoiceDetailsList

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Function GetNetVisitInvoicePrintData() As StringBuilder

        Dim padItemNo As Integer = 4
        Dim padItemName As Integer = 27
        Dim padQuantity As Integer = 6
        Dim padUnitPrice As Integer = 14
        Dim padDiscount As Integer = 7
        Dim padAmount As Integer = 16
        Dim padTotalAmount As Integer = 60

        Dim padCategoryName As Integer = 36
        Dim padCategoryAmount As Integer = 20

        Dim footerFont As New Font(printFontName, 9)

        pageNo = 0
        invoiceParagraphs = New Collection()

        Dim oVariousOptions As New VariousOptions()

        Try

            Dim oCoPayTypeID As New LookupDataID.CoPayTypeID()

            Dim count As Integer
            Dim tableHeader As New System.Text.StringBuilder(String.Empty)
            Dim tableData As New System.Text.StringBuilder(String.Empty)
            Dim printOptionID As String = StringValueMayBeEnteredIn(Me.cboPrintOptions)


            If printOptionID.ToUpper().Equals(oPrintOptionID.GroupedByItemName.ToUpper()) Then
                padQuantity = 10
                padAmount = 16

                tableHeader.Append("No: ".PadRight(padItemNo))
                tableHeader.Append("Item Name: ".PadRight(padItemName))
                tableHeader.Append("Qty: ".PadRight(padQuantity))
                tableHeader.Append("Amount: ".PadRight(padAmount))

                tableHeader.Append(ControlChars.NewLine)
                tableHeader.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))

                Dim lToPrintInvoiceDetails = From data In GetToPrintNetVisitInvoiceDetails() Group By itemName = data.Item2 Into quantity = Sum(data.Item3), amount = Sum(data.Item6)

                For Each item In lToPrintInvoiceDetails


                    count += 1
                    Dim itemNo As String = (count).ToString()
                    Dim itemName As String = item.itemName
                    Dim quantity As String = item.quantity.ToString()
                    Dim amount As String = FormatNumber(item.amount, AppData.DecimalPlaces)


                    tableData.Append(itemNo.PadRight(padItemNo))
                    Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
                    If wrappeditemName.Count > 1 Then
                        For pos As Integer = 0 To wrappeditemName.Count - 1
                            tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
                            If Not pos = wrappeditemName.Count - 1 Then
                                tableData.Append(ControlChars.NewLine)
                                tableData.Append(GetSpaces(padItemNo))
                            Else
                                tableData.Append(quantity.PadRight(padQuantity))
                                tableData.Append(amount.PadRight(padAmount))
                            End If
                        Next

                    Else
                        tableData.Append(FixDataLength(itemName, padItemName))
                        tableData.Append(quantity.PadRight(padQuantity))
                        tableData.Append(amount.PadRight(padAmount))
                    End If

                    tableData.Append(ControlChars.NewLine)
                Next

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Return tableData
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            ElseIf printOptionID.ToUpper().Equals(oPrintOptionID.GroupedByItemCategory().ToUpper()) Then
                tableHeader.Append("No: ".PadRight(padItemNo))
                tableHeader.Append("Item Category: ".PadRight(padCategoryName))
                tableHeader.Append("Amount: ".PadRight(padCategoryAmount))
                tableHeader.Append(ControlChars.NewLine)
                tableHeader.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim extraChargeItemCategoryDes As String = GetLookupDataDes(oItemCategoryID.Extras)

                Dim invoiceDetails = From data In GetToPrintNetVisitInvoiceDetails() Group By CategoryName = data.Item1 Into categoryAmount = Sum(data.Item6)
                                     Select CategoryName, categoryAmount
                                     Where Not CategoryName = extraChargeItemCategoryDes



                Dim extraChargeCategoryInvoiceDetails = From data In GetToPrintNetVisitInvoiceDetails() Group By categoryName = data.Item1, ExtraChargeCategoryName = data.Item7 Into ExtraChargeCategoryAmount = Sum(data.Item6)
                                     Select categoryName, ExtraChargeCategoryName, ExtraChargeCategoryAmount
                                     Where categoryName = extraChargeItemCategoryDes



                For Each item In invoiceDetails

                    count += 1

                    Dim itemNo As String = (count).ToString()
                    Dim categoryName As String = GetPrintableItemCategoryDes(item.CategoryName)

                    Dim categoryAmount As String = FormatNumber(item.categoryAmount, AppData.DecimalPlaces)

                    tableData.Append(itemNo.PadRight(padItemNo))

                    If categoryName.Length > padCategoryName Then
                        tableData.Append(categoryName.Substring(0, padCategoryName).PadRight(padCategoryName))
                    Else : tableData.Append(categoryName.PadRight(padCategoryName))
                    End If
                    tableData.Append(categoryAmount.PadRight(padCategoryAmount))
                    tableData.Append(ControlChars.NewLine)

                Next

                If extraChargeCategoryInvoiceDetails.Count() > 0 Then
                    count += 1
                    Dim extraCount As Integer = 0
                    Dim extraChargetableData As New System.Text.StringBuilder(String.Empty)
                    tableData.Append(count.ToString().PadRight(padItemNo))
                    extraChargetableData.Append("Extras")
                    extraChargetableData.Append(ControlChars.NewLine)
                    For Each item In extraChargeCategoryInvoiceDetails


                        Dim itemNo As String = (extraCount).ToString()
                        Dim extraChargeItemCategory As String = item.ExtraChargeCategoryName

                        Dim extraChargeItemCategoryAmount As String = FormatNumber(item.ExtraChargeCategoryAmount, AppData.DecimalPlaces)
                        extraChargetableData.Append(String.Empty.PadRight(padItemNo + 2))



                        If extraChargeItemCategory.Length > padCategoryName Then
                            extraChargetableData.Append(extraChargeItemCategory.Substring(0, padCategoryName).PadRight((padCategoryName)))
                        Else : extraChargetableData.Append(extraChargeItemCategory.PadRight((padCategoryName)))
                        End If
                        extraChargetableData.Append(extraChargeItemCategoryAmount.PadRight(padCategoryAmount))
                        extraChargetableData.Append(ControlChars.NewLine)

                    Next
                    tableData.Append(extraChargetableData)
                End If
                Return tableData
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            ElseIf printOptionID.ToUpper().Equals(oPrintOptionID.PrintItemCodes.ToUpper()) Then
                padItemNo = 10
                tableHeader.Append("Code: ".PadRight(padItemNo))
                tableHeader.Append("Item Name: ".PadRight(padItemName))
                tableHeader.Append("Qty: ".PadRight(padQuantity))
                tableHeader.Append("Unit Price: ".PadRight(padUnitPrice))
                tableHeader.Append("Amount: ".PadRight(padAmount))

                tableHeader.Append(ControlChars.NewLine)
                tableHeader.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))

                For rowNo As Integer = 0 To Me.dgvInvoiceDetails.RowCount - 1

                    Dim cells As DataGridViewCellCollection = Me.dgvInvoiceDetails.Rows(rowNo).Cells
                    count += 1

                    Dim itemNo As String = cells.Item(Me.colItemCode.Name).Value.ToString()
                    Dim itemName As String = cells.Item(Me.colItemName.Name).Value.ToString()
                    Dim quantity As String = cells.Item(Me.colQuantityBalance.Name).Value.ToString()
                    Dim unitPrice As String = FormatNumber(CDec(cells.Item(Me.colUnitPrice.Name).Value.ToString()), AppData.DecimalPlaces)
                    Dim amount As String = FormatNumber(CDec(cells.Item(Me.colAmountBalance.Name).Value.ToString()), AppData.DecimalPlaces)



                    tableData.Append(itemNo.PadRight(padItemNo))
                    Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
                    If wrappeditemName.Count > 1 Then
                        For pos As Integer = 0 To wrappeditemName.Count - 1
                            tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
                            If Not pos = wrappeditemName.Count - 1 Then
                                tableData.Append(ControlChars.NewLine)
                                tableData.Append(GetSpaces(padItemNo))
                            Else
                                tableData.Append(quantity.PadRight(padQuantity))
                                tableData.Append(unitPrice.PadRight(padUnitPrice))
                                tableData.Append(amount.PadRight(padAmount))
                            End If
                        Next

                    Else
                        tableData.Append(FixDataLength(itemName, padItemName))
                        tableData.Append(quantity.PadRight(padQuantity))
                        tableData.Append(unitPrice.PadRight(padUnitPrice))
                        tableData.Append(amount.PadRight(padAmount))
                    End If

                    tableData.Append(ControlChars.NewLine)
                Next

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Return tableData
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Else

                tableHeader.Append("No: ".PadRight(padItemNo))
                tableHeader.Append("Item Name: ".PadRight(padItemName))
                tableHeader.Append("Qty: ".PadLeft(padQuantity))
                tableHeader.Append("Unit Price: ".PadRight(padUnitPrice))
                tableHeader.Append("Disc: ".PadRight(padDiscount))
                tableHeader.Append("Amount: ".PadRight(padAmount))

                tableHeader.Append(ControlChars.NewLine)
                tableHeader.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))

                For rowNo As Integer = 0 To Me.dgvInvoiceDetails.RowCount - 1

                    Dim cells As DataGridViewCellCollection = Me.dgvInvoiceDetails.Rows(rowNo).Cells
                    count += 1
                    Dim itemNo As String = (count).ToString()
                    Dim itemName As String = cells.Item(Me.colItemName.Name).Value.ToString()
                    Dim quantity As String = cells.Item(Me.colQuantityBalance.Name).Value.ToString()
                    Dim unitPrice As String = FormatNumber(CDec(cells.Item(Me.colUnitPrice.Name).Value.ToString()), AppData.DecimalPlaces)
                    Dim discount As String = cells.Item(Me.colDiscount.Name).Value.ToString()
                    Dim amount As String = FormatNumber(CDec(cells.Item(Me.colAmountBalance.Name).Value.ToString()), AppData.DecimalPlaces)


                    tableData.Append(itemNo.PadRight(padItemNo))
                    Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
                    If wrappeditemName.Count > 1 Then
                        For pos As Integer = 0 To wrappeditemName.Count - 1
                            tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
                            If Not pos = wrappeditemName.Count - 1 Then
                                tableData.Append(ControlChars.NewLine)
                                tableData.Append(GetSpaces(padItemNo))
                            Else
                                tableData.Append(quantity.PadRight(padQuantity))
                                tableData.Append(unitPrice.PadRight(padUnitPrice))
                                tableData.Append(discount.PadRight(padDiscount))
                                tableData.Append(amount.PadRight(padAmount))
                            End If
                        Next

                    Else
                        tableData.Append(FixDataLength(itemName, padItemName))
                        tableData.Append(quantity.PadRight(padQuantity))
                        tableData.Append(unitPrice.PadRight(padUnitPrice))
                        tableData.Append(discount.PadRight(padDiscount))
                        tableData.Append(amount.PadRight(padAmount))
                    End If

                    tableData.Append(ControlChars.NewLine)
                Next

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Return tableData
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Function

#End Region

#Region "Invoice Adjustments"

    Private Function GetInvoiceAdjustmentList() As List(Of Tuple(Of String, String, Integer, Decimal, String))

        Try

            ' Create list of tuples with two items each.
            Dim invoiceDetailAdjustement As New List(Of Tuple(Of String, String, Integer, Decimal, String))
            Dim count As Integer = Me.dgvAdjustments.RowCount
            If count < 1 Then Return invoiceDetailAdjustement
            For rowNo As Integer = 0 To count - 1

                Dim cells As DataGridViewCellCollection = Me.dgvAdjustments.Rows(rowNo).Cells
                Dim category As String = cells.Item(Me.colAdjCategory.Name).Value.ToString()
                Dim itemCategoryID As String = cells.Item(Me.colAdjItemCategoryID.Name).Value.ToString()
                Dim itemCode As String = cells.Item(Me.colAdjItemCode.Name).Value.ToString()
                Dim itemName As String = cells.Item(Me.colAdjItemName.Name).Value.ToString()
                Dim quantity As Integer = IntegerEnteredIn(cells, Me.colAdjQuantity, "quantity!")
                Dim amount As Decimal = DecimalEnteredIn(cells, Me.colAdjAMount, False, "amount!")
                Dim extraChargeCategory As String = Me.GetExtraChargeCategory(itemCategoryID, itemCode)
                invoiceDetailAdjustement.Add(New Tuple(Of String, String, Integer, Decimal, String)(category, itemName, quantity, amount, extraChargeCategory))

            Next

            Return invoiceDetailAdjustement

        Catch ex As Exception
            Throw ex
        End Try

    End Function



    Private Function GetUpExtraBilItemAdjustmetList() As List(Of Tuple(Of String, String, Integer, Decimal, String))

        Try

            ' Create list of tuples with two items each.
            Dim extraBillItemAdjustement As New List(Of Tuple(Of String, String, Integer, Decimal, String))
            Dim count As Integer = Me.dgvUpAdjustments.RowCount
            If count < 1 Then Return extraBillItemAdjustement
            For rowNo As Integer = 0 To count - 1

                Dim cells As DataGridViewCellCollection = Me.dgvUpAdjustments.Rows(rowNo).Cells
                Dim category As String = cells.Item(Me.colUpCategory.Name).Value.ToString()
                Dim itemCategoryID As String = cells.Item(Me.colUpItemCategoryID.Name).Value.ToString()
                Dim itemCode As String = cells.Item(Me.colUpItemCode.Name).Value.ToString()
                Dim itemName As String = cells.Item(Me.colUpItemName.Name).Value.ToString()
                Dim quantity As Integer = IntegerEnteredIn(cells, Me.colUpQuantity, "quantity!")
                Dim amount As Decimal = DecimalEnteredIn(cells, Me.colUpAmount, False, "amount!")
                Dim extraChargeCategory As String = Me.GetExtraChargeCategory(itemCategoryID, itemCode)
                extraBillItemAdjustement.Add(New Tuple(Of String, String, Integer, Decimal, String)(category, itemName, quantity, amount, extraChargeCategory))

            Next

            Return extraBillItemAdjustement

        Catch ex As Exception
            Throw ex
        End Try

    End Function


    Private Function GetVisitInvoiceAdjustmentPrintData() As StringBuilder

        Dim padItemNo As Integer = 4
        Dim padItemName As Integer = 27
        Dim padQuantity As Integer = 6
        Dim padUnitPrice As Integer = 14
        Dim padDiscount As Integer = 7
        Dim padAmount As Integer = 16
        Dim padTotalAmount As Integer = 60

        Dim padCategoryName As Integer = 36
        Dim padCategoryAmount As Integer = 20


        pageNo = 0
        invoiceParagraphs = New Collection()
        Dim oVariousOptions As New VariousOptions()
        Dim tableData As New System.Text.StringBuilder(String.Empty)
        Dim printOptionID As String = StringValueMayBeEnteredIn(cboPrintOptions)
        Try

            Dim oCoPayTypeID As New LookupDataID.CoPayTypeID()

            Dim count As Integer
            Dim tableHeader As New System.Text.StringBuilder(String.Empty)



            If printOptionID.ToUpper().Equals(oPrintOptionID.GroupedByItemName.ToUpper()) Then

                padQuantity = 10
                padAmount = 16

                tableHeader.Append("No: ".PadRight(padItemNo))
                tableHeader.Append("Item Name: ".PadRight(padItemName))
                tableHeader.Append("Qty: ".PadRight(padQuantity))
                tableHeader.Append("Amount: ".PadRight(padAmount))

                tableHeader.Append(ControlChars.NewLine)
                tableHeader.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))

                '''' down adjustments
                Dim invoiceAdjustmentDetails = From data In Me.GetInvoiceAdjustmentList() Group data By itemName = data.Item2
                                         Into itemQuantity = Sum(data.Item3), itemAmount = Sum(data.Item4) Select itemName, itemQuantity, itemAmount

                For Each item In invoiceAdjustmentDetails

                    count += 1

                    Dim itemNo As String = (count).ToString()
                    Dim itemName As String = item.itemName
                    Dim quantity As String = item.itemQuantity.ToString
                    Dim amount As String = FormatNumber(item.itemAmount, AppData.DecimalPlaces)


                    tableData.Append(itemNo.PadRight(padItemNo))
                    Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
                    If wrappeditemName.Count > 1 Then
                        For pos As Integer = 0 To wrappeditemName.Count - 1
                            tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
                            If Not pos = wrappeditemName.Count - 1 Then
                                tableData.Append(ControlChars.NewLine)
                                tableData.Append(GetSpaces(padItemNo))
                            Else
                                tableData.Append("-" + quantity.PadRight(padQuantity))
                                tableData.Append(("-" + amount).PadRight(padAmount))
                            End If
                        Next

                    Else
                        tableData.Append(FixDataLength(itemName, padItemName))
                        tableData.Append("-" + quantity.PadRight(padQuantity))
                        tableData.Append(("-" + amount).PadRight(padAmount))
                    End If

                    tableData.Append(ControlChars.NewLine)
                Next


                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


                '''' up adjustments
                Dim upExtraBilItemAdjustmetList = From data In Me.GetUpExtraBilItemAdjustmetList() Group data By itemName = data.Item2
                                         Into itemQuantity = Sum(data.Item3), itemAmount = Sum(data.Item4) Select itemName, itemQuantity, itemAmount

                For Each item In upExtraBilItemAdjustmetList

                    count += 1

                    Dim itemNo As String = (count).ToString()
                    Dim itemName As String = item.itemName
                    Dim quantity As String = item.itemQuantity.ToString
                    Dim amount As String = FormatNumber(item.itemAmount, AppData.DecimalPlaces)


                    tableData.Append(itemNo.PadRight(padItemNo))
                    Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
                    If wrappeditemName.Count > 1 Then
                        For pos As Integer = 0 To wrappeditemName.Count - 1
                            tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
                            If Not pos = wrappeditemName.Count - 1 Then
                                tableData.Append(ControlChars.NewLine)
                                tableData.Append(GetSpaces(padItemNo))
                            Else
                                tableData.Append("+" + quantity.PadRight(padQuantity))
                                tableData.Append(("+" + amount).PadRight(padAmount))
                            End If
                        Next

                    Else
                        tableData.Append(FixDataLength(itemName, padItemName))
                        tableData.Append("+" + quantity.PadRight(padQuantity))
                        tableData.Append(("+" + amount).PadRight(padAmount))
                    End If

                    tableData.Append(ControlChars.NewLine)
                Next



                Return tableData

            ElseIf printOptionID.ToUpper().Equals(oPrintOptionID.GroupedByItemCategory.ToUpper()) Then

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                tableHeader.Append("No: ".PadRight(padItemNo))
                tableHeader.Append("Item Category: ".PadRight(padCategoryName))
                tableHeader.Append("Amount: ".PadRight(padCategoryAmount))
                tableHeader.Append(ControlChars.NewLine)
                tableHeader.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))

                Dim extraChargeItemCategoryDes As String = GetLookupDataDes(oItemCategoryID.Extras)


                ' ''Down Adjustments

                Dim invoiceAdjustmentDetails = From data In Me.GetInvoiceAdjustmentList() Group data By CategoryName = data.Item1
                                     Into CategoryAmount = Sum(data.Item4) Select CategoryName, CategoryAmount
                                     Where CategoryName IsNot extraChargeItemCategoryDes

                Dim invoiceAdjustmentExtraCharge = From data In Me.GetInvoiceAdjustmentList() Group data By CategoryName = data.Item1, extraChargeCategory = data.Item5
                                     Into extraCategoryAmount = Sum(data.Item4) Select CategoryName, extraChargeCategory, extraCategoryAmount
                                     Where extraChargeCategory = extraChargeItemCategoryDes



                For Each item In invoiceAdjustmentDetails

                    count += 1

                    Dim itemNo As String = (count).ToString()
                    Dim categoryName As String = GetPrintableItemCategoryDes(item.CategoryName)

                    Dim categoryAmount As String = FormatNumber(item.CategoryAmount, AppData.DecimalPlaces)

                    tableData.Append(itemNo.PadRight(padItemNo))

                    If categoryName.Length > padCategoryName Then
                        tableData.Append(categoryName.Substring(0, padCategoryName).PadRight(padCategoryName))
                    Else : tableData.Append(categoryName.PadRight(padCategoryName))
                    End If
                    tableData.Append(("-" + categoryAmount).PadRight(padCategoryAmount))
                    tableData.Append(ControlChars.NewLine)

                Next
                Dim extraCount As Integer = 0
                If invoiceAdjustmentExtraCharge.Count() > 0 Then
                    extraCount += 1

                    Dim extraChargetableData As New System.Text.StringBuilder(String.Empty)
                    tableData.Append(count.ToString().PadRight(padItemNo))
                    extraChargetableData.Append("Extras")
                    extraChargetableData.Append(ControlChars.NewLine)
                    For Each item In invoiceAdjustmentExtraCharge


                        Dim itemNo As String = (extraCount).ToString()
                        Dim extraChargeItemCategory As String = item.extraChargeCategory

                        Dim extraChargeItemCategoryAmount As String = FormatNumber(item.extraCategoryAmount, AppData.DecimalPlaces)
                        extraChargetableData.Append(String.Empty.PadRight(padItemNo + 2))

                        If extraChargeItemCategory.Length > padCategoryName Then
                            extraChargetableData.Append(extraChargeItemCategory.Substring(0, padCategoryName).PadRight((padCategoryName)))
                        Else : extraChargetableData.Append(extraChargeItemCategory.PadRight((padCategoryName)))
                        End If
                        extraChargetableData.Append("-" + extraChargeItemCategoryAmount.PadRight(padCategoryAmount))
                        extraChargetableData.Append(ControlChars.NewLine)

                    Next
                    tableData.Append(extraChargetableData)
                End If

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''




                ' ''up Adjustments

                Dim upInvoiceAdjustmentDetails = From data In Me.GetUpExtraBilItemAdjustmetList Group data By CategoryName = data.Item1
                                     Into CategoryAmount = Sum(data.Item4) Select CategoryName, CategoryAmount
                                     Where CategoryName IsNot extraChargeItemCategoryDes

                Dim upinvoiceAdjustmentExtraCharge = From data In Me.GetUpExtraBilItemAdjustmetList Group data By CategoryName = data.Item1, extraChargeCategory = data.Item5
                                     Into extraCategoryAmount = Sum(data.Item4) Select CategoryName, extraChargeCategory, extraCategoryAmount
                                     Where extraChargeCategory = extraChargeItemCategoryDes



                For Each item In upInvoiceAdjustmentDetails

                    count += 1

                    Dim itemNo As String = (count).ToString()
                    Dim categoryName As String = GetPrintableItemCategoryDes(item.CategoryName)

                    Dim categoryAmount As String = FormatNumber(item.CategoryAmount, AppData.DecimalPlaces)

                    tableData.Append(itemNo.PadRight(padItemNo))

                    If categoryName.Length > padCategoryName Then
                        tableData.Append(categoryName.Substring(0, padCategoryName).PadRight(padCategoryName))
                    Else : tableData.Append(categoryName.PadRight(padCategoryName))
                    End If
                    tableData.Append(("+" + categoryAmount).PadRight(padCategoryAmount))
                    tableData.Append(ControlChars.NewLine)

                Next
                Dim upExtraCount As Integer = 0
                If upinvoiceAdjustmentExtraCharge.Count() > 0 Then
                    upExtraCount += 1

                    Dim extraChargetableData As New System.Text.StringBuilder(String.Empty)
                    tableData.Append(count.ToString().PadRight(padItemNo))
                    extraChargetableData.Append("Extras")
                    extraChargetableData.Append(ControlChars.NewLine)
                    For Each item In invoiceAdjustmentExtraCharge


                        Dim itemNo As String = (extraCount).ToString()
                        Dim extraChargeItemCategory As String = item.extraChargeCategory

                        Dim extraChargeItemCategoryAmount As String = FormatNumber(item.extraCategoryAmount, AppData.DecimalPlaces)
                        extraChargetableData.Append(String.Empty.PadRight(padItemNo + 2))

                        If extraChargeItemCategory.Length > padCategoryName Then
                            extraChargetableData.Append(extraChargeItemCategory.Substring(0, padCategoryName).PadRight((padCategoryName)))
                        Else : extraChargetableData.Append(extraChargeItemCategory.PadRight((padCategoryName)))
                        End If
                        extraChargetableData.Append("+" + extraChargeItemCategoryAmount.PadRight(padCategoryAmount))
                        extraChargetableData.Append(ControlChars.NewLine)

                    Next
                    tableData.Append(extraChargetableData)
                End If

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Return tableData

            ElseIf printOptionID.ToUpper().Equals(oPrintOptionID.PrintItemCodes.ToUpper()) Then
                padItemNo = 10
                tableHeader.Append("Code: ".PadRight(padItemNo))
                tableHeader.Append("Item Name: ".PadRight(padItemName))
                tableHeader.Append("Qty: ".PadRight(padQuantity + padUnitPrice))
                tableHeader.Append("Amount: ".PadRight(padAmount))

                tableHeader.Append(ControlChars.NewLine)
                tableHeader.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))

                '''' Down Adjustments
                For rowNo As Integer = 0 To Me.dgvAdjustments.RowCount - 1

                    Dim cells As DataGridViewCellCollection = Me.dgvAdjustments.Rows(rowNo).Cells
                    count += 1

                    'im itemNo As String = (count).ToString()
                    Dim itemNo As String = cells.Item(Me.colAdjItemCode.Name).Value.ToString()
                    Dim itemName As String = cells.Item(Me.colAdjItemName.Name).Value.ToString()
                    Dim quantity As String = cells.Item(Me.colAdjQuantity.Name).Value.ToString()
                    Dim amount As String = cells.Item(Me.colAdjAMount.Name).Value.ToString()



                    tableData.Append(itemNo.PadRight(padItemNo))
                    Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
                    If wrappeditemName.Count > 1 Then
                        For pos As Integer = 0 To wrappeditemName.Count - 1
                            tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
                            If Not pos = wrappeditemName.Count - 1 Then
                                tableData.Append(ControlChars.NewLine)
                                tableData.Append(GetSpaces(padItemNo))
                            Else
                                tableData.Append("-" + quantity.PadRight(padQuantity + padUnitPrice))

                                tableData.Append(("-" + amount).PadRight(padAmount))
                            End If
                        Next

                    Else
                        tableData.Append(FixDataLength(itemName, padItemName))
                        tableData.Append("-" + quantity.PadRight(padQuantity + padUnitPrice))
                        tableData.Append(("-" + amount).PadRight(padAmount))
                    End If

                    tableData.Append(ControlChars.NewLine)
                Next

                '''' Up Adjustments
                For rowNo As Integer = 0 To Me.dgvAdjustments.RowCount - 1

                    Dim cells As DataGridViewCellCollection = Me.dgvUpAdjustments.Rows(rowNo).Cells
                    count += 1

                    'im itemNo As String = (count).ToString()
                    Dim itemNo As String = cells.Item(Me.colUpItemCode.Name).Value.ToString()
                    Dim itemName As String = cells.Item(Me.colUpItemName.Name).Value.ToString()
                    Dim quantity As String = cells.Item(Me.colUpQuantity.Name).Value.ToString()
                    Dim amount As String = cells.Item(Me.colUpAmount.Name).Value.ToString()



                    tableData.Append(itemNo.PadRight(padItemNo))
                    Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
                    If wrappeditemName.Count > 1 Then
                        For pos As Integer = 0 To wrappeditemName.Count - 1
                            tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
                            If Not pos = wrappeditemName.Count - 1 Then
                                tableData.Append(ControlChars.NewLine)
                                tableData.Append(GetSpaces(padItemNo))
                            Else
                                tableData.Append("+" + quantity.PadRight(padQuantity + padUnitPrice))

                                tableData.Append(("+" + amount).PadRight(padAmount))
                            End If
                        Next

                    Else
                        tableData.Append(FixDataLength(itemName, padItemName))
                        tableData.Append("+" + quantity.PadRight(padQuantity + padUnitPrice))
                        tableData.Append(("+" + amount).PadRight(padAmount))
                    End If

                    tableData.Append(ControlChars.NewLine)
                Next

                Return tableData

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Else
                tableHeader.Append("No: ".PadRight(padItemNo))
                tableHeader.Append("Item Name: ".PadRight(padItemName))
                tableHeader.Append("Qty: ".PadRight(padQuantity + padUnitPrice + padDiscount))
                tableHeader.Append("Amount: ".PadRight(padAmount))

                tableHeader.Append(ControlChars.NewLine)
                tableHeader.Append(ControlChars.NewLine)
                invoiceParagraphs.Add(New PrintParagraps(bodyBoldFont, tableHeader.ToString()))

                ' ''''''''' Down Adjustments
                For rowNo As Integer = 0 To Me.dgvAdjustments.RowCount - 1

                    Dim cells As DataGridViewCellCollection = Me.dgvAdjustments.Rows(rowNo).Cells
                    count += 1

                    Dim itemNo As String = (count).ToString()
                    Dim itemName As String = cells.Item(Me.colAdjItemName.Name).Value.ToString()
                    Dim quantity As String = cells.Item(Me.colAdjQuantity.Name).Value.ToString()
                    Dim amount As String = cells.Item(Me.colAdjAMount.Name).Value.ToString()


                    tableData.Append(itemNo.PadRight(padItemNo))
                    Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
                    If wrappeditemName.Count > 1 Then
                        For pos As Integer = 0 To wrappeditemName.Count - 1
                            tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
                            If Not pos = wrappeditemName.Count - 1 Then
                                tableData.Append(ControlChars.NewLine)
                                tableData.Append(GetSpaces(padItemNo))
                            Else
                                tableData.Append(quantity.PadRight(padQuantity + padUnitPrice + padDiscount))
                                tableData.Append(("-" + amount).PadRight(padAmount))
                            End If
                        Next

                    Else
                        tableData.Append(FixDataLength(itemName, padItemName))
                        tableData.Append(quantity.PadRight(padQuantity + padUnitPrice + padDiscount))
                        tableData.Append(("-" + amount).PadRight(padAmount))
                    End If

                    tableData.Append(ControlChars.NewLine)
                Next

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                ' ''''''''' Up Adjustments
                For rowNo As Integer = 0 To Me.dgvUpAdjustments.RowCount - 1

                    Dim cells As DataGridViewCellCollection = Me.dgvUpAdjustments.Rows(rowNo).Cells
                    count += 1

                    Dim itemNo As String = (count).ToString()
                    Dim itemName As String = cells.Item(Me.colUpItemName.Name).Value.ToString()
                    Dim quantity As String = cells.Item(Me.colUpQuantity.Name).Value.ToString()
                    Dim amount As String = cells.Item(Me.colUpAmount.Name).Value.ToString()


                    tableData.Append(itemNo.PadRight(padItemNo))
                    Dim wrappeditemName As List(Of String) = WrapText(itemName, padItemName)
                    If wrappeditemName.Count > 1 Then
                        For pos As Integer = 0 To wrappeditemName.Count - 1
                            tableData.Append(FixDataLength(wrappeditemName(pos).Trim(), padItemName))
                            If Not pos = wrappeditemName.Count - 1 Then
                                tableData.Append(ControlChars.NewLine)
                                tableData.Append(GetSpaces(padItemNo))
                            Else
                                tableData.Append(quantity.PadRight(padQuantity + padUnitPrice + padDiscount))
                                tableData.Append(("+" + amount).PadRight(padAmount))
                            End If
                        Next

                    Else
                        tableData.Append(FixDataLength(itemName, padItemName))
                        tableData.Append(quantity.PadRight(padQuantity + padUnitPrice + padDiscount))
                        tableData.Append(("+" + amount).PadRight(padAmount))
                    End If

                    tableData.Append(ControlChars.NewLine)
                Next

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Return tableData

            End If


            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Throw ex
        End Try

    End Function

#End Region

#End Region

#Region " Invoice Details Extras "

    Private Sub cmsInvoiceDetails_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmsInvoiceDetails.Opening

        If Me.dgvInvoiceDetails.ColumnCount < 1 OrElse Me.dgvInvoiceDetails.RowCount < 1 Then
            Me.cmsInvoiceDetailsCopy.Enabled = False
            Me.cmsInvoiceDetailsSelectAll.Enabled = False
        Else
            Me.cmsInvoiceDetailsCopy.Enabled = True
            Me.cmsInvoiceDetailsSelectAll.Enabled = True
        End If

    End Sub

    Private Sub cmsInvoiceDetailsCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmsInvoiceDetailsCopy.Click

        Try

            Me.Cursor = Cursors.WaitCursor

            If Me.dgvInvoiceDetails.SelectedCells.Count < 1 Then Return
            Clipboard.SetText(CopyFromControl(Me.dgvInvoiceDetails))

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub cmsInvoiceDetailsSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmsInvoiceDetailsSelectAll.Click

        Try

            Me.Cursor = Cursors.WaitCursor
            Me.dgvInvoiceDetails.SelectAll()

        Catch ex As Exception
            Return

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

#End Region


    Private Sub btnPrintPreview_Click(sender As System.Object, e As System.EventArgs) Handles btnPrintPreview.Click

        Try

            Me.Cursor = Cursors.WaitCursor

            ' Make a PrintDocument and attach it to the PrintPreview dialog.
            Dim dlgPrintPreview As New PrintPreviewDialog()

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Me.SetVisitInvoicePrintData()

            With dlgPrintPreview
                .Document = docInvoices
                .Document.PrinterSettings.Collate = True
                .ShowIcon = False
                .WindowState = FormWindowState.Maximized
                .ShowDialog()
            End With


        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

End Class