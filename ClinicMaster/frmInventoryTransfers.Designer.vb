
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInventoryTransfers : Inherits System.Windows.Forms.Form

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal orderNo As String)
        MyClass.New()
        Me.defaultOrderNo = orderNo
    End Sub

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInventoryTransfers))
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle39 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle36 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle37 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle38 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.dtpTransferDate = New System.Windows.Forms.DateTimePicker()
        Me.cboFromLocationID = New System.Windows.Forms.ComboBox()
        Me.cboToLocationID = New System.Windows.Forms.ComboBox()
        Me.stbOrderNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.cboOrderType = New System.Windows.Forms.ComboBox()
        Me.cboTransferReasonID = New System.Windows.Forms.ComboBox()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.stbTransferNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblTransferNo = New System.Windows.Forms.Label()
        Me.lblTransferDate = New System.Windows.Forms.Label()
        Me.lblFromLocationID = New System.Windows.Forms.Label()
        Me.lblToLocationID = New System.Windows.Forms.Label()
        Me.stbClientMachine = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblClientMachine = New System.Windows.Forms.Label()
        Me.tbcInventoryTransfers = New System.Windows.Forms.TabControl()
        Me.tpgDrugs = New System.Windows.Forms.TabPage()
        Me.dgvDrugs = New System.Windows.Forms.DataGridView()
        Me.colDrugNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDrugName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDrugFromLocationBalance = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDrugToLocationBalance = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDrugQuantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDrugBatchQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDrugPack = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colDrugPackSize = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDrugTotalUnits = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDrugBatchNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDrugFromLocationBatchQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDrugExpiryDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDrugAddBatch = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.colDrugUnitCost = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDrugTotalCost = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDrugStockStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDrugsSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.cmsInventoryTransfers = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmsInventoryTransfersQuickSearch = New System.Windows.Forms.ToolStripMenuItem()
        Me.tpgConsumables = New System.Windows.Forms.TabPage()
        Me.dgvConsumables = New System.Windows.Forms.DataGridView()
        Me.tpgOtherItems = New System.Windows.Forms.TabPage()
        Me.dgvOtherItems = New System.Windows.Forms.DataGridView()
        Me.lblOrderNo = New System.Windows.Forms.Label()
        Me.stbOrderDate = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblOrderDate = New System.Windows.Forms.Label()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.pnlPrintOnSaving = New System.Windows.Forms.Panel()
        Me.chkPrintOnSaving = New System.Windows.Forms.CheckBox()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.stbConsumableCost = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblConsumableCost = New System.Windows.Forms.Label()
        Me.stbTotalcost = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.stbDrugCost = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblDrugCost = New System.Windows.Forms.Label()
        Me.lblOrderType = New System.Windows.Forms.Label()
        Me.fbnLoadOrders = New System.Windows.Forms.Button()
        Me.stbOtherItems = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblOtherItemsCost = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.colConsumableNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableFromLocationBalance = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableToLocationBalance = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableQuantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableBatchQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumablePack = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colConsumablePackSize = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableTotalUnits = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableBatchNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableFromLocationBatchQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableExpiryDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableAddBatch = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.colConsumableUnitCost = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableTotalCost = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumableStockStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colConsumablesSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colOtherItemsItemCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOtherItemsItemName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOtherItemsFromLocationBalance = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOtherItemsToLocationBalance = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOtherItemsQuantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOtherItemsBatchQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOtherItemsPack = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colOtherItemsPackSize = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOtherItemsTotalUnits = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOtherItemsBatchNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOtherItemsFromLocationBatchQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOtherItemsExpiryDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOtherItemsAddBatch = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.colOtherItemsUnitCost = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOtherItemsTotalCost = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOtherItemsStockStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colOtherItemsSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.tbcInventoryTransfers.SuspendLayout()
        Me.tpgDrugs.SuspendLayout()
        CType(Me.dgvDrugs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsInventoryTransfers.SuspendLayout()
        Me.tpgConsumables.SuspendLayout()
        CType(Me.dgvConsumables, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgOtherItems.SuspendLayout()
        CType(Me.dgvOtherItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlPrintOnSaving.SuspendLayout()
        Me.SuspendLayout()
        '
        'fbnSearch
        '
        Me.fbnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnSearch.Location = New System.Drawing.Point(15, 402)
        Me.fbnSearch.Name = "fbnSearch"
        Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
        Me.fbnSearch.TabIndex = 13
        Me.fbnSearch.Text = "S&earch"
        Me.fbnSearch.UseVisualStyleBackColor = True
        Me.fbnSearch.Visible = False
        '
        'fbnDelete
        '
        Me.fbnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(1093, 402)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 14
        Me.fbnDelete.Tag = "InventoryTransfers"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = False
        Me.fbnDelete.Visible = False
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(15, 429)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 15
        Me.ebnSaveUpdate.Tag = "InventoryTransfers"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = False
        '
        'dtpTransferDate
        '
        Me.dtpTransferDate.Checked = False
        Me.ebnSaveUpdate.SetDataMember(Me.dtpTransferDate, "TransferDate")
        Me.dtpTransferDate.Location = New System.Drawing.Point(137, 29)
        Me.dtpTransferDate.Name = "dtpTransferDate"
        Me.dtpTransferDate.ShowCheckBox = True
        Me.dtpTransferDate.Size = New System.Drawing.Size(180, 20)
        Me.dtpTransferDate.TabIndex = 3
        '
        'cboFromLocationID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboFromLocationID, "FromLocation,FromLocationID")
        Me.cboFromLocationID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromLocationID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboFromLocationID.Location = New System.Drawing.Point(461, 29)
        Me.cboFromLocationID.Name = "cboFromLocationID"
        Me.cboFromLocationID.Size = New System.Drawing.Size(180, 21)
        Me.cboFromLocationID.TabIndex = 7
        '
        'cboToLocationID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboToLocationID, "ToLocation,ToLocationID")
        Me.cboToLocationID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToLocationID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboToLocationID.Location = New System.Drawing.Point(461, 52)
        Me.cboToLocationID.Name = "cboToLocationID"
        Me.cboToLocationID.Size = New System.Drawing.Size(180, 21)
        Me.cboToLocationID.TabIndex = 9
        '
        'stbOrderNo
        '
        Me.stbOrderNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbOrderNo.CapitalizeFirstLetter = False
        Me.stbOrderNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ebnSaveUpdate.SetDataMember(Me.stbOrderNo, "OrderNo")
        Me.stbOrderNo.EntryErrorMSG = ""
        Me.stbOrderNo.Location = New System.Drawing.Point(461, 5)
        Me.stbOrderNo.MaxLength = 20
        Me.stbOrderNo.Name = "stbOrderNo"
        Me.stbOrderNo.RegularExpression = ""
        Me.stbOrderNo.Size = New System.Drawing.Size(125, 20)
        Me.stbOrderNo.TabIndex = 11
        '
        'cboOrderType
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboOrderType, "OrderType,OrderTypeID")
        Me.cboOrderType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOrderType.Enabled = False
        Me.cboOrderType.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboOrderType.Location = New System.Drawing.Point(811, 91)
        Me.cboOrderType.Name = "cboOrderType"
        Me.cboOrderType.Size = New System.Drawing.Size(180, 21)
        Me.cboOrderType.TabIndex = 35
        '
        'cboTransferReasonID
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboTransferReasonID, "TransferReason,TransferReasonID")
        Me.cboTransferReasonID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransferReasonID.Enabled = False
        Me.cboTransferReasonID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboTransferReasonID.Location = New System.Drawing.Point(461, 76)
        Me.cboTransferReasonID.Name = "cboTransferReasonID"
        Me.cboTransferReasonID.Size = New System.Drawing.Size(180, 21)
        Me.cboTransferReasonID.TabIndex = 40
        '
        'fbnClose
        '
        Me.fbnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(1093, 429)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 18
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'stbTransferNo
        '
        Me.stbTransferNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTransferNo.CapitalizeFirstLetter = False
        Me.stbTransferNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.stbTransferNo.EntryErrorMSG = ""
        Me.stbTransferNo.Location = New System.Drawing.Point(137, 8)
        Me.stbTransferNo.MaxLength = 20
        Me.stbTransferNo.Name = "stbTransferNo"
        Me.stbTransferNo.RegularExpression = ""
        Me.stbTransferNo.Size = New System.Drawing.Size(128, 20)
        Me.stbTransferNo.TabIndex = 1
        '
        'lblTransferNo
        '
        Me.lblTransferNo.Location = New System.Drawing.Point(12, 9)
        Me.lblTransferNo.Name = "lblTransferNo"
        Me.lblTransferNo.Size = New System.Drawing.Size(119, 20)
        Me.lblTransferNo.TabIndex = 0
        Me.lblTransferNo.Text = "Transfer No"
        '
        'lblTransferDate
        '
        Me.lblTransferDate.Location = New System.Drawing.Point(12, 30)
        Me.lblTransferDate.Name = "lblTransferDate"
        Me.lblTransferDate.Size = New System.Drawing.Size(119, 20)
        Me.lblTransferDate.TabIndex = 2
        Me.lblTransferDate.Text = "Transfer Date"
        '
        'lblFromLocationID
        '
        Me.lblFromLocationID.Location = New System.Drawing.Point(336, 29)
        Me.lblFromLocationID.Name = "lblFromLocationID"
        Me.lblFromLocationID.Size = New System.Drawing.Size(119, 20)
        Me.lblFromLocationID.TabIndex = 6
        Me.lblFromLocationID.Text = "From Location"
        '
        'lblToLocationID
        '
        Me.lblToLocationID.Location = New System.Drawing.Point(336, 52)
        Me.lblToLocationID.Name = "lblToLocationID"
        Me.lblToLocationID.Size = New System.Drawing.Size(119, 20)
        Me.lblToLocationID.TabIndex = 8
        Me.lblToLocationID.Text = "To Location"
        '
        'stbClientMachine
        '
        Me.stbClientMachine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbClientMachine.CapitalizeFirstLetter = False
        Me.stbClientMachine.EntryErrorMSG = ""
        Me.stbClientMachine.Location = New System.Drawing.Point(0, 0)
        Me.stbClientMachine.Name = "stbClientMachine"
        Me.stbClientMachine.RegularExpression = ""
        Me.stbClientMachine.Size = New System.Drawing.Size(100, 20)
        Me.stbClientMachine.TabIndex = 0
        '
        'lblClientMachine
        '
        Me.lblClientMachine.Location = New System.Drawing.Point(0, 0)
        Me.lblClientMachine.Name = "lblClientMachine"
        Me.lblClientMachine.Size = New System.Drawing.Size(100, 23)
        Me.lblClientMachine.TabIndex = 0
        '
        'tbcInventoryTransfers
        '
        Me.tbcInventoryTransfers.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbcInventoryTransfers.Controls.Add(Me.tpgDrugs)
        Me.tbcInventoryTransfers.Controls.Add(Me.tpgConsumables)
        Me.tbcInventoryTransfers.Controls.Add(Me.tpgOtherItems)
        Me.tbcInventoryTransfers.HotTrack = True
        Me.tbcInventoryTransfers.Location = New System.Drawing.Point(12, 118)
        Me.tbcInventoryTransfers.Name = "tbcInventoryTransfers"
        Me.tbcInventoryTransfers.SelectedIndex = 0
        Me.tbcInventoryTransfers.Size = New System.Drawing.Size(1160, 278)
        Me.tbcInventoryTransfers.TabIndex = 12
        '
        'tpgDrugs
        '
        Me.tpgDrugs.Controls.Add(Me.dgvDrugs)
        Me.tpgDrugs.Location = New System.Drawing.Point(4, 22)
        Me.tpgDrugs.Name = "tpgDrugs"
        Me.tpgDrugs.Size = New System.Drawing.Size(1152, 252)
        Me.tpgDrugs.TabIndex = 2
        Me.tpgDrugs.Tag = "DrugInventoryTransfers"
        Me.tpgDrugs.Text = "Drugs"
        Me.tpgDrugs.UseVisualStyleBackColor = True
        '
        'dgvDrugs
        '
        Me.dgvDrugs.AllowUserToOrderColumns = True
        Me.dgvDrugs.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDrugs.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDrugs.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colDrugNo, Me.colDrugName, Me.colDrugFromLocationBalance, Me.colDrugToLocationBalance, Me.colDrugQuantity, Me.colDrugBatchQty, Me.colDrugPack, Me.colDrugPackSize, Me.colDrugTotalUnits, Me.colDrugBatchNo, Me.colDrugFromLocationBatchQty, Me.colDrugExpiryDate, Me.colDrugAddBatch, Me.colDrugUnitCost, Me.colDrugTotalCost, Me.colDrugStockStatus, Me.colDrugsSaved})
        Me.dgvDrugs.ContextMenuStrip = Me.cmsInventoryTransfers
        Me.dgvDrugs.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDrugs.EnableHeadersVisualStyles = False
        Me.dgvDrugs.GridColor = System.Drawing.Color.Khaki
        Me.dgvDrugs.Location = New System.Drawing.Point(0, 0)
        Me.dgvDrugs.Name = "dgvDrugs"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDrugs.RowHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvDrugs.Size = New System.Drawing.Size(1152, 252)
        Me.dgvDrugs.TabIndex = 0
        Me.dgvDrugs.Text = "DataGridView1"
        '
        'colDrugNo
        '
        Me.colDrugNo.DataPropertyName = "ItemCode"
        Me.colDrugNo.HeaderText = "Drug No"
        Me.colDrugNo.MaxInputLength = 20
        Me.colDrugNo.Name = "colDrugNo"
        Me.colDrugNo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colDrugNo.Width = 90
        '
        'colDrugName
        '
        Me.colDrugName.DataPropertyName = "ItemName"
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Info
        Me.colDrugName.DefaultCellStyle = DataGridViewCellStyle2
        Me.colDrugName.HeaderText = "Drug Name"
        Me.colDrugName.MaxInputLength = 100
        Me.colDrugName.Name = "colDrugName"
        Me.colDrugName.ReadOnly = True
        Me.colDrugName.Width = 120
        '
        'colDrugFromLocationBalance
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle3.NullValue = Nothing
        Me.colDrugFromLocationBalance.DefaultCellStyle = DataGridViewCellStyle3
        Me.colDrugFromLocationBalance.HeaderText = "From Location Balance"
        Me.colDrugFromLocationBalance.MaxInputLength = 12
        Me.colDrugFromLocationBalance.Name = "colDrugFromLocationBalance"
        Me.colDrugFromLocationBalance.ReadOnly = True
        Me.colDrugFromLocationBalance.Width = 140
        '
        'colDrugToLocationBalance
        '
        Me.colDrugToLocationBalance.DataPropertyName = "ToLocationBalance"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.colDrugToLocationBalance.DefaultCellStyle = DataGridViewCellStyle4
        Me.colDrugToLocationBalance.HeaderText = "To Location Balance"
        Me.colDrugToLocationBalance.MaxInputLength = 12
        Me.colDrugToLocationBalance.Name = "colDrugToLocationBalance"
        Me.colDrugToLocationBalance.ReadOnly = True
        Me.colDrugToLocationBalance.Width = 120
        '
        'colDrugQuantity
        '
        Me.colDrugQuantity.DataPropertyName = "Quantity"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle5.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.colDrugQuantity.DefaultCellStyle = DataGridViewCellStyle5
        Me.colDrugQuantity.HeaderText = "Quantity"
        Me.colDrugQuantity.Name = "colDrugQuantity"
        Me.colDrugQuantity.ReadOnly = True
        Me.colDrugQuantity.Width = 50
        '
        'colDrugBatchQty
        '
        Me.colDrugBatchQty.DataPropertyName = "BatchQuantity"
        Me.colDrugBatchQty.HeaderText = "Batch Quantity"
        Me.colDrugBatchQty.Name = "colDrugBatchQty"
        '
        'colDrugPack
        '
        Me.colDrugPack.HeaderText = "Pack"
        Me.colDrugPack.Name = "colDrugPack"
        Me.colDrugPack.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colDrugPack.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'colDrugPackSize
        '
        Me.colDrugPackSize.DataPropertyName = "PackSize"
        Me.colDrugPackSize.HeaderText = "PackSize"
        Me.colDrugPackSize.Name = "colDrugPackSize"
        '
        'colDrugTotalUnits
        '
        Me.colDrugTotalUnits.HeaderText = "Total Units"
        Me.colDrugTotalUnits.Name = "colDrugTotalUnits"
        Me.colDrugTotalUnits.ReadOnly = True
        '
        'colDrugBatchNo
        '
        Me.colDrugBatchNo.DataPropertyName = "BatchNo"
        Me.colDrugBatchNo.HeaderText = "Batch No"
        Me.colDrugBatchNo.MaxInputLength = 20
        Me.colDrugBatchNo.Name = "colDrugBatchNo"
        '
        'colDrugFromLocationBatchQty
        '
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Info
        Me.colDrugFromLocationBatchQty.DefaultCellStyle = DataGridViewCellStyle6
        Me.colDrugFromLocationBatchQty.HeaderText = "From Location Batch Qty"
        Me.colDrugFromLocationBatchQty.Name = "colDrugFromLocationBatchQty"
        Me.colDrugFromLocationBatchQty.ReadOnly = True
        Me.colDrugFromLocationBatchQty.Width = 150
        '
        'colDrugExpiryDate
        '
        Me.colDrugExpiryDate.DataPropertyName = "ExpiryDate"
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle7.Format = "D"
        DataGridViewCellStyle7.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle7.NullValue = Nothing
        Me.colDrugExpiryDate.DefaultCellStyle = DataGridViewCellStyle7
        Me.colDrugExpiryDate.HeaderText = "Expiry Date"
        Me.colDrugExpiryDate.Name = "colDrugExpiryDate"
        Me.colDrugExpiryDate.ReadOnly = True
        Me.colDrugExpiryDate.Width = 80
        '
        'colDrugAddBatch
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.Firebrick
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Navy
        Me.colDrugAddBatch.DefaultCellStyle = DataGridViewCellStyle8
        Me.colDrugAddBatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colDrugAddBatch.HeaderText = "Add Batch"
        Me.colDrugAddBatch.Name = "colDrugAddBatch"
        Me.colDrugAddBatch.ReadOnly = True
        Me.colDrugAddBatch.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colDrugAddBatch.Text = "���"
        Me.colDrugAddBatch.UseColumnTextForButtonValue = True
        Me.colDrugAddBatch.Width = 70
        '
        'colDrugUnitCost
        '
        Me.colDrugUnitCost.DataPropertyName = "UnitCost"
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Info
        Me.colDrugUnitCost.DefaultCellStyle = DataGridViewCellStyle9
        Me.colDrugUnitCost.HeaderText = "Unit Cost"
        Me.colDrugUnitCost.Name = "colDrugUnitCost"
        Me.colDrugUnitCost.ReadOnly = True
        '
        'colDrugTotalCost
        '
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Info
        Me.colDrugTotalCost.DefaultCellStyle = DataGridViewCellStyle10
        Me.colDrugTotalCost.HeaderText = "Total Cost"
        Me.colDrugTotalCost.Name = "colDrugTotalCost"
        Me.colDrugTotalCost.ReadOnly = True
        '
        'colDrugStockStatus
        '
        Me.colDrugStockStatus.DataPropertyName = "StockStatus"
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Info
        Me.colDrugStockStatus.DefaultCellStyle = DataGridViewCellStyle11
        Me.colDrugStockStatus.HeaderText = "Stock Status"
        Me.colDrugStockStatus.Name = "colDrugStockStatus"
        Me.colDrugStockStatus.ReadOnly = True
        Me.colDrugStockStatus.Width = 80
        '
        'colDrugsSaved
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle12.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle12.NullValue = False
        Me.colDrugsSaved.DefaultCellStyle = DataGridViewCellStyle12
        Me.colDrugsSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colDrugsSaved.HeaderText = "Saved"
        Me.colDrugsSaved.Name = "colDrugsSaved"
        Me.colDrugsSaved.ReadOnly = True
        Me.colDrugsSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colDrugsSaved.Width = 50
        '
        'cmsInventoryTransfers
        '
        Me.cmsInventoryTransfers.BackColor = System.Drawing.Color.GhostWhite
        Me.cmsInventoryTransfers.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmsInventoryTransfersQuickSearch})
        Me.cmsInventoryTransfers.Name = "cmsSearch"
        Me.cmsInventoryTransfers.Size = New System.Drawing.Size(144, 26)
        '
        'cmsInventoryTransfersQuickSearch
        '
        Me.cmsInventoryTransfersQuickSearch.Image = CType(resources.GetObject("cmsInventoryTransfersQuickSearch.Image"), System.Drawing.Image)
        Me.cmsInventoryTransfersQuickSearch.Name = "cmsInventoryTransfersQuickSearch"
        Me.cmsInventoryTransfersQuickSearch.Size = New System.Drawing.Size(143, 22)
        Me.cmsInventoryTransfersQuickSearch.Text = "Quick Search"
        '
        'tpgConsumables
        '
        Me.tpgConsumables.Controls.Add(Me.dgvConsumables)
        Me.tpgConsumables.Location = New System.Drawing.Point(4, 22)
        Me.tpgConsumables.Name = "tpgConsumables"
        Me.tpgConsumables.Size = New System.Drawing.Size(1152, 252)
        Me.tpgConsumables.TabIndex = 12
        Me.tpgConsumables.Tag = "ConsumableInventoryTransfers"
        Me.tpgConsumables.Text = "Consumables"
        Me.tpgConsumables.UseVisualStyleBackColor = True
        '
        'dgvConsumables
        '
        Me.dgvConsumables.AllowUserToOrderColumns = True
        Me.dgvConsumables.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvConsumables.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.dgvConsumables.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colConsumableNo, Me.colConsumableName, Me.colConsumableFromLocationBalance, Me.colConsumableToLocationBalance, Me.colConsumableQuantity, Me.colConsumableBatchQty, Me.colConsumablePack, Me.colConsumablePackSize, Me.colConsumableTotalUnits, Me.colConsumableBatchNo, Me.colConsumableFromLocationBatchQty, Me.colConsumableExpiryDate, Me.colConsumableAddBatch, Me.colConsumableUnitCost, Me.colConsumableTotalCost, Me.colConsumableStockStatus, Me.colConsumablesSaved})
        Me.dgvConsumables.ContextMenuStrip = Me.cmsInventoryTransfers
        Me.dgvConsumables.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvConsumables.EnableHeadersVisualStyles = False
        Me.dgvConsumables.GridColor = System.Drawing.Color.Khaki
        Me.dgvConsumables.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.dgvConsumables.Location = New System.Drawing.Point(0, 0)
        Me.dgvConsumables.Name = "dgvConsumables"
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle26.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvConsumables.RowHeadersDefaultCellStyle = DataGridViewCellStyle26
        Me.dgvConsumables.Size = New System.Drawing.Size(1152, 252)
        Me.dgvConsumables.TabIndex = 44
        Me.dgvConsumables.Text = "DataGridView1"
        '
        'tpgOtherItems
        '
        Me.tpgOtherItems.Controls.Add(Me.dgvOtherItems)
        Me.tpgOtherItems.Location = New System.Drawing.Point(4, 22)
        Me.tpgOtherItems.Name = "tpgOtherItems"
        Me.tpgOtherItems.Size = New System.Drawing.Size(1152, 252)
        Me.tpgOtherItems.TabIndex = 13
        Me.tpgOtherItems.Text = "Non Medical Items"
        Me.tpgOtherItems.UseVisualStyleBackColor = True
        '
        'dgvOtherItems
        '
        Me.dgvOtherItems.AllowUserToOrderColumns = True
        Me.dgvOtherItems.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle27.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOtherItems.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle27
        Me.dgvOtherItems.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colOtherItemsItemCode, Me.colOtherItemsItemName, Me.colOtherItemsFromLocationBalance, Me.colOtherItemsToLocationBalance, Me.colOtherItemsQuantity, Me.colOtherItemsBatchQty, Me.colOtherItemsPack, Me.colOtherItemsPackSize, Me.colOtherItemsTotalUnits, Me.colOtherItemsBatchNo, Me.colOtherItemsFromLocationBatchQty, Me.colOtherItemsExpiryDate, Me.colOtherItemsAddBatch, Me.colOtherItemsUnitCost, Me.colOtherItemsTotalCost, Me.colOtherItemsStockStatus, Me.colOtherItemsSaved})
        Me.dgvOtherItems.ContextMenuStrip = Me.cmsInventoryTransfers
        Me.dgvOtherItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOtherItems.EnableHeadersVisualStyles = False
        Me.dgvOtherItems.GridColor = System.Drawing.Color.Khaki
        Me.dgvOtherItems.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.dgvOtherItems.Location = New System.Drawing.Point(0, 0)
        Me.dgvOtherItems.Name = "dgvOtherItems"
        DataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle39.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle39.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle39.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOtherItems.RowHeadersDefaultCellStyle = DataGridViewCellStyle39
        Me.dgvOtherItems.Size = New System.Drawing.Size(1152, 252)
        Me.dgvOtherItems.TabIndex = 45
        Me.dgvOtherItems.Text = "DataGridView1"
        '
        'lblOrderNo
        '
        Me.lblOrderNo.Location = New System.Drawing.Point(336, 6)
        Me.lblOrderNo.Name = "lblOrderNo"
        Me.lblOrderNo.Size = New System.Drawing.Size(119, 20)
        Me.lblOrderNo.TabIndex = 10
        Me.lblOrderNo.Text = "Order No"
        '
        'stbOrderDate
        '
        Me.stbOrderDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbOrderDate.CapitalizeFirstLetter = False
        Me.stbOrderDate.Enabled = False
        Me.stbOrderDate.EntryErrorMSG = ""
        Me.stbOrderDate.Location = New System.Drawing.Point(137, 51)
        Me.stbOrderDate.MaxLength = 30
        Me.stbOrderDate.Name = "stbOrderDate"
        Me.stbOrderDate.RegularExpression = ""
        Me.stbOrderDate.Size = New System.Drawing.Size(180, 20)
        Me.stbOrderDate.TabIndex = 5
        '
        'lblOrderDate
        '
        Me.lblOrderDate.Location = New System.Drawing.Point(12, 52)
        Me.lblOrderDate.Name = "lblOrderDate"
        Me.lblOrderDate.Size = New System.Drawing.Size(119, 20)
        Me.lblOrderDate.TabIndex = 4
        Me.lblOrderDate.Text = "Order Date"
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Enabled = False
        Me.btnPrint.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Location = New System.Drawing.Point(1015, 429)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(72, 24)
        Me.btnPrint.TabIndex = 17
        Me.btnPrint.Text = "&Print"
        '
        'pnlPrintOnSaving
        '
        Me.pnlPrintOnSaving.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnlPrintOnSaving.Controls.Add(Me.chkPrintOnSaving)
        Me.pnlPrintOnSaving.Location = New System.Drawing.Point(98, 429)
        Me.pnlPrintOnSaving.Name = "pnlPrintOnSaving"
        Me.pnlPrintOnSaving.Size = New System.Drawing.Size(191, 31)
        Me.pnlPrintOnSaving.TabIndex = 16
        '
        'chkPrintOnSaving
        '
        Me.chkPrintOnSaving.AccessibleDescription = ""
        Me.chkPrintOnSaving.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkPrintOnSaving.AutoSize = True
        Me.chkPrintOnSaving.Checked = True
        Me.chkPrintOnSaving.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPrintOnSaving.Location = New System.Drawing.Point(12, 3)
        Me.chkPrintOnSaving.Name = "chkPrintOnSaving"
        Me.chkPrintOnSaving.Size = New System.Drawing.Size(145, 17)
        Me.chkPrintOnSaving.TabIndex = 0
        Me.chkPrintOnSaving.Text = " Print Transfer On Saving"
        '
        'btnLoad
        '
        Me.btnLoad.AccessibleDescription = ""
        Me.btnLoad.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoad.Location = New System.Drawing.Point(271, 4)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(46, 23)
        Me.btnLoad.TabIndex = 19
        Me.btnLoad.Tag = ""
        Me.btnLoad.Text = "&Load"
        '
        'stbConsumableCost
        '
        Me.stbConsumableCost.BackColor = System.Drawing.SystemColors.Info
        Me.stbConsumableCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbConsumableCost.CapitalizeFirstLetter = False
        Me.stbConsumableCost.Enabled = False
        Me.stbConsumableCost.EntryErrorMSG = ""
        Me.stbConsumableCost.Location = New System.Drawing.Point(811, 25)
        Me.stbConsumableCost.MaxLength = 20
        Me.stbConsumableCost.Name = "stbConsumableCost"
        Me.stbConsumableCost.RegularExpression = ""
        Me.stbConsumableCost.Size = New System.Drawing.Size(180, 20)
        Me.stbConsumableCost.TabIndex = 33
        Me.stbConsumableCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblConsumableCost
        '
        Me.lblConsumableCost.Location = New System.Drawing.Point(698, 26)
        Me.lblConsumableCost.Name = "lblConsumableCost"
        Me.lblConsumableCost.Size = New System.Drawing.Size(107, 18)
        Me.lblConsumableCost.TabIndex = 32
        Me.lblConsumableCost.Text = "Consumable Cost"
        '
        'stbTotalcost
        '
        Me.stbTotalcost.BackColor = System.Drawing.SystemColors.Info
        Me.stbTotalcost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbTotalcost.CapitalizeFirstLetter = False
        Me.stbTotalcost.Enabled = False
        Me.stbTotalcost.EntryErrorMSG = ""
        Me.stbTotalcost.Location = New System.Drawing.Point(811, 68)
        Me.stbTotalcost.MaxLength = 20
        Me.stbTotalcost.Name = "stbTotalcost"
        Me.stbTotalcost.RegularExpression = ""
        Me.stbTotalcost.Size = New System.Drawing.Size(180, 20)
        Me.stbTotalcost.TabIndex = 31
        Me.stbTotalcost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(698, 71)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(107, 18)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "Total Cost"
        '
        'stbDrugCost
        '
        Me.stbDrugCost.BackColor = System.Drawing.SystemColors.Info
        Me.stbDrugCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbDrugCost.CapitalizeFirstLetter = False
        Me.stbDrugCost.Enabled = False
        Me.stbDrugCost.EntryErrorMSG = ""
        Me.stbDrugCost.Location = New System.Drawing.Point(811, 3)
        Me.stbDrugCost.MaxLength = 20
        Me.stbDrugCost.Name = "stbDrugCost"
        Me.stbDrugCost.RegularExpression = ""
        Me.stbDrugCost.Size = New System.Drawing.Size(180, 20)
        Me.stbDrugCost.TabIndex = 29
        Me.stbDrugCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDrugCost
        '
        Me.lblDrugCost.Location = New System.Drawing.Point(698, 6)
        Me.lblDrugCost.Name = "lblDrugCost"
        Me.lblDrugCost.Size = New System.Drawing.Size(107, 18)
        Me.lblDrugCost.TabIndex = 28
        Me.lblDrugCost.Text = "Drug Cost"
        '
        'lblOrderType
        '
        Me.lblOrderType.Location = New System.Drawing.Point(698, 92)
        Me.lblOrderType.Name = "lblOrderType"
        Me.lblOrderType.Size = New System.Drawing.Size(107, 20)
        Me.lblOrderType.TabIndex = 34
        Me.lblOrderType.Text = "Order Type"
        '
        'fbnLoadOrders
        '
        Me.fbnLoadOrders.AccessibleDescription = ""
        Me.fbnLoadOrders.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnLoadOrders.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnLoadOrders.Location = New System.Drawing.Point(592, 1)
        Me.fbnLoadOrders.Name = "fbnLoadOrders"
        Me.fbnLoadOrders.Size = New System.Drawing.Size(49, 24)
        Me.fbnLoadOrders.TabIndex = 36
        Me.fbnLoadOrders.Tag = ""
        Me.fbnLoadOrders.Text = "&Load"
        '
        'stbOtherItems
        '
        Me.stbOtherItems.BackColor = System.Drawing.SystemColors.Info
        Me.stbOtherItems.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbOtherItems.CapitalizeFirstLetter = False
        Me.stbOtherItems.Enabled = False
        Me.stbOtherItems.EntryErrorMSG = ""
        Me.stbOtherItems.Location = New System.Drawing.Point(811, 46)
        Me.stbOtherItems.MaxLength = 20
        Me.stbOtherItems.Name = "stbOtherItems"
        Me.stbOtherItems.RegularExpression = ""
        Me.stbOtherItems.Size = New System.Drawing.Size(180, 20)
        Me.stbOtherItems.TabIndex = 38
        Me.stbOtherItems.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOtherItemsCost
        '
        Me.lblOtherItemsCost.Location = New System.Drawing.Point(698, 47)
        Me.lblOtherItemsCost.Name = "lblOtherItemsCost"
        Me.lblOtherItemsCost.Size = New System.Drawing.Size(107, 18)
        Me.lblOtherItemsCost.TabIndex = 37
        Me.lblOtherItemsCost.Text = "Other Items Cost"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(336, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(107, 20)
        Me.Label2.TabIndex = 39
        Me.Label2.Text = "Transfer Reason"
        '
        'colConsumableNo
        '
        Me.colConsumableNo.DataPropertyName = "ItemCode"
        Me.colConsumableNo.HeaderText = "Consumable No"
        Me.colConsumableNo.MaxInputLength = 20
        Me.colConsumableNo.Name = "colConsumableNo"
        Me.colConsumableNo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colConsumableNo.Width = 90
        '
        'colConsumableName
        '
        Me.colConsumableName.DataPropertyName = "ItemName"
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Info
        Me.colConsumableName.DefaultCellStyle = DataGridViewCellStyle15
        Me.colConsumableName.HeaderText = "Consumable Name"
        Me.colConsumableName.MaxInputLength = 100
        Me.colConsumableName.Name = "colConsumableName"
        Me.colConsumableName.ReadOnly = True
        Me.colConsumableName.Width = 120
        '
        'colConsumableFromLocationBalance
        '
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle16.Format = "N0"
        DataGridViewCellStyle16.NullValue = Nothing
        Me.colConsumableFromLocationBalance.DefaultCellStyle = DataGridViewCellStyle16
        Me.colConsumableFromLocationBalance.HeaderText = "From Location Balance"
        Me.colConsumableFromLocationBalance.MaxInputLength = 12
        Me.colConsumableFromLocationBalance.Name = "colConsumableFromLocationBalance"
        Me.colConsumableFromLocationBalance.ReadOnly = True
        Me.colConsumableFromLocationBalance.Width = 140
        '
        'colConsumableToLocationBalance
        '
        Me.colConsumableToLocationBalance.DataPropertyName = "ToLocationBalance"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle17.Format = "N0"
        DataGridViewCellStyle17.NullValue = Nothing
        Me.colConsumableToLocationBalance.DefaultCellStyle = DataGridViewCellStyle17
        Me.colConsumableToLocationBalance.HeaderText = "To Location Balance"
        Me.colConsumableToLocationBalance.MaxInputLength = 12
        Me.colConsumableToLocationBalance.Name = "colConsumableToLocationBalance"
        Me.colConsumableToLocationBalance.ReadOnly = True
        Me.colConsumableToLocationBalance.Width = 120
        '
        'colConsumableQuantity
        '
        Me.colConsumableQuantity.DataPropertyName = "Quantity"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle18.Format = "N0"
        DataGridViewCellStyle18.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle18.NullValue = Nothing
        Me.colConsumableQuantity.DefaultCellStyle = DataGridViewCellStyle18
        Me.colConsumableQuantity.HeaderText = "Quantity"
        Me.colConsumableQuantity.MaxInputLength = 12
        Me.colConsumableQuantity.Name = "colConsumableQuantity"
        Me.colConsumableQuantity.ReadOnly = True
        Me.colConsumableQuantity.Width = 50
        '
        'colConsumableBatchQty
        '
        Me.colConsumableBatchQty.DataPropertyName = "BatchQuantity"
        Me.colConsumableBatchQty.HeaderText = "Batch Quantity"
        Me.colConsumableBatchQty.Name = "colConsumableBatchQty"
        '
        'colConsumablePack
        '
        Me.colConsumablePack.HeaderText = "Pack"
        Me.colConsumablePack.Name = "colConsumablePack"
        Me.colConsumablePack.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colConsumablePack.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'colConsumablePackSize
        '
        Me.colConsumablePackSize.DataPropertyName = "PackSize"
        Me.colConsumablePackSize.HeaderText = "Pack Size"
        Me.colConsumablePackSize.Name = "colConsumablePackSize"
        '
        'colConsumableTotalUnits
        '
        Me.colConsumableTotalUnits.HeaderText = "Total Units"
        Me.colConsumableTotalUnits.Name = "colConsumableTotalUnits"
        Me.colConsumableTotalUnits.ReadOnly = True
        '
        'colConsumableBatchNo
        '
        Me.colConsumableBatchNo.DataPropertyName = "BatchNo"
        Me.colConsumableBatchNo.HeaderText = "Batch No"
        Me.colConsumableBatchNo.MaxInputLength = 20
        Me.colConsumableBatchNo.Name = "colConsumableBatchNo"
        '
        'colConsumableFromLocationBatchQty
        '
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Info
        Me.colConsumableFromLocationBatchQty.DefaultCellStyle = DataGridViewCellStyle19
        Me.colConsumableFromLocationBatchQty.HeaderText = "From Location Batch Qty"
        Me.colConsumableFromLocationBatchQty.Name = "colConsumableFromLocationBatchQty"
        Me.colConsumableFromLocationBatchQty.ReadOnly = True
        Me.colConsumableFromLocationBatchQty.Width = 150
        '
        'colConsumableExpiryDate
        '
        Me.colConsumableExpiryDate.DataPropertyName = "ExpiryDate"
        DataGridViewCellStyle20.Format = "D"
        DataGridViewCellStyle20.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle20.NullValue = Nothing
        Me.colConsumableExpiryDate.DefaultCellStyle = DataGridViewCellStyle20
        Me.colConsumableExpiryDate.HeaderText = "Expiry Date"
        Me.colConsumableExpiryDate.MaxInputLength = 20
        Me.colConsumableExpiryDate.Name = "colConsumableExpiryDate"
        Me.colConsumableExpiryDate.Width = 80
        '
        'colConsumableAddBatch
        '
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle21.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle21.ForeColor = System.Drawing.Color.Firebrick
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.Color.Navy
        Me.colConsumableAddBatch.DefaultCellStyle = DataGridViewCellStyle21
        Me.colConsumableAddBatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colConsumableAddBatch.HeaderText = "Add Batch"
        Me.colConsumableAddBatch.Name = "colConsumableAddBatch"
        Me.colConsumableAddBatch.ReadOnly = True
        Me.colConsumableAddBatch.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colConsumableAddBatch.Text = "���"
        Me.colConsumableAddBatch.UseColumnTextForButtonValue = True
        Me.colConsumableAddBatch.Width = 70
        '
        'colConsumableUnitCost
        '
        Me.colConsumableUnitCost.DataPropertyName = "UnitCost"
        DataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Info
        Me.colConsumableUnitCost.DefaultCellStyle = DataGridViewCellStyle22
        Me.colConsumableUnitCost.HeaderText = "Unit Cost"
        Me.colConsumableUnitCost.Name = "colConsumableUnitCost"
        Me.colConsumableUnitCost.ReadOnly = True
        '
        'colConsumableTotalCost
        '
        DataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Info
        Me.colConsumableTotalCost.DefaultCellStyle = DataGridViewCellStyle23
        Me.colConsumableTotalCost.HeaderText = "Total Cost"
        Me.colConsumableTotalCost.Name = "colConsumableTotalCost"
        Me.colConsumableTotalCost.ReadOnly = True
        '
        'colConsumableStockStatus
        '
        Me.colConsumableStockStatus.DataPropertyName = "StockStatus"
        DataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Info
        Me.colConsumableStockStatus.DefaultCellStyle = DataGridViewCellStyle24
        Me.colConsumableStockStatus.HeaderText = "Stock Status"
        Me.colConsumableStockStatus.Name = "colConsumableStockStatus"
        Me.colConsumableStockStatus.ReadOnly = True
        Me.colConsumableStockStatus.Width = 80
        '
        'colConsumablesSaved
        '
        Me.colConsumablesSaved.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle25.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle25.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle25.NullValue = False
        Me.colConsumablesSaved.DefaultCellStyle = DataGridViewCellStyle25
        Me.colConsumablesSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colConsumablesSaved.HeaderText = "Saved"
        Me.colConsumablesSaved.Name = "colConsumablesSaved"
        Me.colConsumablesSaved.ReadOnly = True
        Me.colConsumablesSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colConsumablesSaved.Width = 50
        '
        'colOtherItemsItemCode
        '
        Me.colOtherItemsItemCode.DataPropertyName = "ItemCode"
        Me.colOtherItemsItemCode.HeaderText = "Item Code"
        Me.colOtherItemsItemCode.MaxInputLength = 20
        Me.colOtherItemsItemCode.Name = "colOtherItemsItemCode"
        Me.colOtherItemsItemCode.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colOtherItemsItemCode.Width = 90
        '
        'colOtherItemsItemName
        '
        Me.colOtherItemsItemName.DataPropertyName = "ItemName"
        DataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Info
        Me.colOtherItemsItemName.DefaultCellStyle = DataGridViewCellStyle28
        Me.colOtherItemsItemName.HeaderText = "Item Name"
        Me.colOtherItemsItemName.MaxInputLength = 100
        Me.colOtherItemsItemName.Name = "colOtherItemsItemName"
        Me.colOtherItemsItemName.ReadOnly = True
        Me.colOtherItemsItemName.Width = 120
        '
        'colOtherItemsFromLocationBalance
        '
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle29.Format = "N0"
        DataGridViewCellStyle29.NullValue = Nothing
        Me.colOtherItemsFromLocationBalance.DefaultCellStyle = DataGridViewCellStyle29
        Me.colOtherItemsFromLocationBalance.HeaderText = "From Location Balance"
        Me.colOtherItemsFromLocationBalance.MaxInputLength = 12
        Me.colOtherItemsFromLocationBalance.Name = "colOtherItemsFromLocationBalance"
        Me.colOtherItemsFromLocationBalance.ReadOnly = True
        Me.colOtherItemsFromLocationBalance.Width = 140
        '
        'colOtherItemsToLocationBalance
        '
        Me.colOtherItemsToLocationBalance.DataPropertyName = "ToLocationBalance"
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle30.Format = "N0"
        DataGridViewCellStyle30.NullValue = Nothing
        Me.colOtherItemsToLocationBalance.DefaultCellStyle = DataGridViewCellStyle30
        Me.colOtherItemsToLocationBalance.HeaderText = "To Location Balance"
        Me.colOtherItemsToLocationBalance.MaxInputLength = 12
        Me.colOtherItemsToLocationBalance.Name = "colOtherItemsToLocationBalance"
        Me.colOtherItemsToLocationBalance.ReadOnly = True
        Me.colOtherItemsToLocationBalance.Width = 120
        '
        'colOtherItemsQuantity
        '
        Me.colOtherItemsQuantity.DataPropertyName = "Quantity"
        DataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle31.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle31.NullValue = Nothing
        Me.colOtherItemsQuantity.DefaultCellStyle = DataGridViewCellStyle31
        Me.colOtherItemsQuantity.HeaderText = "Quantity"
        Me.colOtherItemsQuantity.MaxInputLength = 12
        Me.colOtherItemsQuantity.Name = "colOtherItemsQuantity"
        Me.colOtherItemsQuantity.Width = 50
        '
        'colOtherItemsBatchQty
        '
        Me.colOtherItemsBatchQty.DataPropertyName = "BatchQuantity"
        Me.colOtherItemsBatchQty.HeaderText = "Batch Quantity"
        Me.colOtherItemsBatchQty.Name = "colOtherItemsBatchQty"
        '
        'colOtherItemsPack
        '
        Me.colOtherItemsPack.HeaderText = "Pack"
        Me.colOtherItemsPack.Name = "colOtherItemsPack"
        Me.colOtherItemsPack.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colOtherItemsPack.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'colOtherItemsPackSize
        '
        Me.colOtherItemsPackSize.DataPropertyName = "PackSize"
        Me.colOtherItemsPackSize.HeaderText = "Pack Size"
        Me.colOtherItemsPackSize.Name = "colOtherItemsPackSize"
        '
        'colOtherItemsTotalUnits
        '
        Me.colOtherItemsTotalUnits.DataPropertyName = "TotalUnits"
        Me.colOtherItemsTotalUnits.HeaderText = "Total Units"
        Me.colOtherItemsTotalUnits.Name = "colOtherItemsTotalUnits"
        Me.colOtherItemsTotalUnits.ReadOnly = True
        '
        'colOtherItemsBatchNo
        '
        Me.colOtherItemsBatchNo.DataPropertyName = "BatchNo"
        Me.colOtherItemsBatchNo.HeaderText = "Batch No"
        Me.colOtherItemsBatchNo.MaxInputLength = 20
        Me.colOtherItemsBatchNo.Name = "colOtherItemsBatchNo"
        '
        'colOtherItemsFromLocationBatchQty
        '
        DataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Info
        Me.colOtherItemsFromLocationBatchQty.DefaultCellStyle = DataGridViewCellStyle32
        Me.colOtherItemsFromLocationBatchQty.HeaderText = "From Location Batch Qty"
        Me.colOtherItemsFromLocationBatchQty.Name = "colOtherItemsFromLocationBatchQty"
        Me.colOtherItemsFromLocationBatchQty.ReadOnly = True
        Me.colOtherItemsFromLocationBatchQty.Width = 150
        '
        'colOtherItemsExpiryDate
        '
        Me.colOtherItemsExpiryDate.DataPropertyName = "ExpiryDate"
        DataGridViewCellStyle33.Format = "D"
        DataGridViewCellStyle33.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle33.NullValue = Nothing
        Me.colOtherItemsExpiryDate.DefaultCellStyle = DataGridViewCellStyle33
        Me.colOtherItemsExpiryDate.HeaderText = "Expiry Date"
        Me.colOtherItemsExpiryDate.MaxInputLength = 20
        Me.colOtherItemsExpiryDate.Name = "colOtherItemsExpiryDate"
        Me.colOtherItemsExpiryDate.Width = 80
        '
        'colOtherItemsAddBatch
        '
        DataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle34.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle34.ForeColor = System.Drawing.Color.Firebrick
        DataGridViewCellStyle34.SelectionBackColor = System.Drawing.Color.Khaki
        DataGridViewCellStyle34.SelectionForeColor = System.Drawing.Color.Navy
        Me.colOtherItemsAddBatch.DefaultCellStyle = DataGridViewCellStyle34
        Me.colOtherItemsAddBatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colOtherItemsAddBatch.HeaderText = "Add Batch"
        Me.colOtherItemsAddBatch.Name = "colOtherItemsAddBatch"
        Me.colOtherItemsAddBatch.ReadOnly = True
        Me.colOtherItemsAddBatch.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colOtherItemsAddBatch.Text = "���"
        Me.colOtherItemsAddBatch.UseColumnTextForButtonValue = True
        Me.colOtherItemsAddBatch.Width = 70
        '
        'colOtherItemsUnitCost
        '
        Me.colOtherItemsUnitCost.DataPropertyName = "UnitCost"
        DataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Info
        Me.colOtherItemsUnitCost.DefaultCellStyle = DataGridViewCellStyle35
        Me.colOtherItemsUnitCost.HeaderText = "Unit Cost"
        Me.colOtherItemsUnitCost.Name = "colOtherItemsUnitCost"
        Me.colOtherItemsUnitCost.ReadOnly = True
        '
        'colOtherItemsTotalCost
        '
        Me.colOtherItemsTotalCost.DataPropertyName = "TotalCost"
        DataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Info
        Me.colOtherItemsTotalCost.DefaultCellStyle = DataGridViewCellStyle36
        Me.colOtherItemsTotalCost.HeaderText = "Total Cost"
        Me.colOtherItemsTotalCost.Name = "colOtherItemsTotalCost"
        Me.colOtherItemsTotalCost.ReadOnly = True
        '
        'colOtherItemsStockStatus
        '
        Me.colOtherItemsStockStatus.DataPropertyName = "StockStatus"
        DataGridViewCellStyle37.BackColor = System.Drawing.SystemColors.Info
        Me.colOtherItemsStockStatus.DefaultCellStyle = DataGridViewCellStyle37
        Me.colOtherItemsStockStatus.HeaderText = "Stock Status"
        Me.colOtherItemsStockStatus.Name = "colOtherItemsStockStatus"
        Me.colOtherItemsStockStatus.ReadOnly = True
        Me.colOtherItemsStockStatus.Width = 80
        '
        'colOtherItemsSaved
        '
        Me.colOtherItemsSaved.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle38.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle38.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle38.NullValue = False
        Me.colOtherItemsSaved.DefaultCellStyle = DataGridViewCellStyle38
        Me.colOtherItemsSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colOtherItemsSaved.HeaderText = "Saved"
        Me.colOtherItemsSaved.Name = "colOtherItemsSaved"
        Me.colOtherItemsSaved.ReadOnly = True
        Me.colOtherItemsSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colOtherItemsSaved.Width = 50
        '
        'frmInventoryTransfers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(1177, 471)
        Me.Controls.Add(Me.cboTransferReasonID)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.stbOtherItems)
        Me.Controls.Add(Me.lblOtherItemsCost)
        Me.Controls.Add(Me.fbnLoadOrders)
        Me.Controls.Add(Me.cboOrderType)
        Me.Controls.Add(Me.lblOrderType)
        Me.Controls.Add(Me.stbConsumableCost)
        Me.Controls.Add(Me.lblConsumableCost)
        Me.Controls.Add(Me.stbTotalcost)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.stbDrugCost)
        Me.Controls.Add(Me.lblDrugCost)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.pnlPrintOnSaving)
        Me.Controls.Add(Me.stbOrderDate)
        Me.Controls.Add(Me.lblOrderDate)
        Me.Controls.Add(Me.stbOrderNo)
        Me.Controls.Add(Me.lblOrderNo)
        Me.Controls.Add(Me.tbcInventoryTransfers)
        Me.Controls.Add(Me.fbnSearch)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.stbTransferNo)
        Me.Controls.Add(Me.lblTransferNo)
        Me.Controls.Add(Me.dtpTransferDate)
        Me.Controls.Add(Me.lblTransferDate)
        Me.Controls.Add(Me.cboFromLocationID)
        Me.Controls.Add(Me.lblFromLocationID)
        Me.Controls.Add(Me.cboToLocationID)
        Me.Controls.Add(Me.lblToLocationID)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.KeyPreview = true
        Me.MaximizeBox = false
        Me.Name = "frmInventoryTransfers"
        Me.Text = "Inventory Transfers"
        Me.tbcInventoryTransfers.ResumeLayout(false)
        Me.tpgDrugs.ResumeLayout(false)
        CType(Me.dgvDrugs,System.ComponentModel.ISupportInitialize).EndInit
        Me.cmsInventoryTransfers.ResumeLayout(false)
        Me.tpgConsumables.ResumeLayout(false)
        CType(Me.dgvConsumables,System.ComponentModel.ISupportInitialize).EndInit
        Me.tpgOtherItems.ResumeLayout(false)
        CType(Me.dgvOtherItems,System.ComponentModel.ISupportInitialize).EndInit
        Me.pnlPrintOnSaving.ResumeLayout(false)
        Me.pnlPrintOnSaving.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents stbTransferNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblTransferNo As System.Windows.Forms.Label
    Friend WithEvents dtpTransferDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTransferDate As System.Windows.Forms.Label
    Friend WithEvents cboFromLocationID As System.Windows.Forms.ComboBox
    Friend WithEvents lblFromLocationID As System.Windows.Forms.Label
    Friend WithEvents cboToLocationID As System.Windows.Forms.ComboBox
    Friend WithEvents lblToLocationID As System.Windows.Forms.Label
    Friend WithEvents stbClientMachine As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblClientMachine As System.Windows.Forms.Label
    Friend WithEvents tbcInventoryTransfers As System.Windows.Forms.TabControl
    Friend WithEvents tpgDrugs As System.Windows.Forms.TabPage
    Friend WithEvents dgvDrugs As System.Windows.Forms.DataGridView
    Friend WithEvents tpgConsumables As System.Windows.Forms.TabPage
    Friend WithEvents dgvConsumables As System.Windows.Forms.DataGridView
    Friend WithEvents cmsInventoryTransfers As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmsInventoryTransfersQuickSearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents stbOrderNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblOrderNo As System.Windows.Forms.Label
    Friend WithEvents stbOrderDate As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblOrderDate As System.Windows.Forms.Label
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents pnlPrintOnSaving As System.Windows.Forms.Panel
    Friend WithEvents chkPrintOnSaving As System.Windows.Forms.CheckBox
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents stbConsumableCost As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblConsumableCost As Label
    Friend WithEvents stbTotalcost As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents stbDrugCost As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblDrugCost As Label
    Friend WithEvents cboOrderType As ComboBox
    Friend WithEvents lblOrderType As Label
    Friend WithEvents fbnLoadOrders As Button
    Friend WithEvents tpgOtherItems As System.Windows.Forms.TabPage
    Friend WithEvents dgvOtherItems As System.Windows.Forms.DataGridView
    Friend WithEvents stbOtherItems As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblOtherItemsCost As System.Windows.Forms.Label
    Friend WithEvents cboTransferReasonID As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents colDrugNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDrugName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDrugFromLocationBalance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDrugToLocationBalance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDrugQuantity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDrugBatchQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDrugPack As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colDrugPackSize As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDrugTotalUnits As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDrugBatchNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDrugFromLocationBatchQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDrugExpiryDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDrugAddBatch As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents colDrugUnitCost As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDrugTotalCost As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDrugStockStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDrugsSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colConsumableNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableFromLocationBalance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableToLocationBalance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableQuantity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableBatchQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumablePack As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colConsumablePackSize As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableTotalUnits As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableBatchNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableFromLocationBatchQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableExpiryDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableAddBatch As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents colConsumableUnitCost As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableTotalCost As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumableStockStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colConsumablesSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colOtherItemsItemCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOtherItemsItemName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOtherItemsFromLocationBalance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOtherItemsToLocationBalance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOtherItemsQuantity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOtherItemsBatchQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOtherItemsPack As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colOtherItemsPackSize As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOtherItemsTotalUnits As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOtherItemsBatchNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOtherItemsFromLocationBatchQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOtherItemsExpiryDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOtherItemsAddBatch As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents colOtherItemsUnitCost As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOtherItemsTotalCost As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOtherItemsStockStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOtherItemsSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class