
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmResearchPatientsStop : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmResearchPatientsStop))
        Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.dtpResearchEndDate = New System.Windows.Forms.DateTimePicker()
        Me.stbOutCome = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbNotes = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.stbVisitNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblVisitNo = New System.Windows.Forms.Label()
        Me.lblResearchEndDate = New System.Windows.Forms.Label()
        Me.lblOutCome = New System.Windows.Forms.Label()
        Me.lblNotes = New System.Windows.Forms.Label()
        Me.btnFindVisitNo = New System.Windows.Forms.Button()
        Me.lblHospitalPID = New System.Windows.Forms.Label()
        Me.stbPatientNo = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbGender = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblGender = New System.Windows.Forms.Label()
        Me.stbAge = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblAge = New System.Windows.Forms.Label()
        Me.stbResearchName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.stbFullName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.lblFullName = New System.Windows.Forms.Label()
        Me.lblResearchNameID = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'fbnSearch
        '
        Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnSearch.Location = New System.Drawing.Point(15, 283)
        Me.fbnSearch.Name = "fbnSearch"
        Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
        Me.fbnSearch.TabIndex = 19
        Me.fbnSearch.Text = "S&earch"
        Me.fbnSearch.UseVisualStyleBackColor = True
        Me.fbnSearch.Visible = False
        '
        'fbnDelete
        '
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(330, 282)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 21
        Me.fbnDelete.Tag = "ResearchPatientsStop"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = False
        Me.fbnDelete.Visible = False
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(15, 310)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 20
        Me.ebnSaveUpdate.Tag = "ResearchPatientsStop"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = False
        '
        'dtpResearchEndDate
        '
        Me.dtpResearchEndDate.Checked = False
        Me.ebnSaveUpdate.SetDataMember(Me.dtpResearchEndDate, "ResearchEndDate")
        Me.dtpResearchEndDate.Location = New System.Drawing.Point(184, 137)
        Me.dtpResearchEndDate.Name = "dtpResearchEndDate"
        Me.dtpResearchEndDate.ShowCheckBox = True
        Me.dtpResearchEndDate.Size = New System.Drawing.Size(218, 20)
        Me.dtpResearchEndDate.TabIndex = 14
        '
        'stbOutCome
        '
        Me.stbOutCome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbOutCome.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbOutCome, "OutCome ")
        Me.stbOutCome.EntryErrorMSG = ""
        Me.stbOutCome.Location = New System.Drawing.Point(184, 159)
        Me.stbOutCome.Multiline = True
        Me.stbOutCome.Name = "stbOutCome"
        Me.stbOutCome.RegularExpression = ""
        Me.stbOutCome.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbOutCome.Size = New System.Drawing.Size(218, 45)
        Me.stbOutCome.TabIndex = 16
        '
        'stbNotes
        '
        Me.stbNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbNotes.CapitalizeFirstLetter = False
        Me.ebnSaveUpdate.SetDataMember(Me.stbNotes, "Notes")
        Me.stbNotes.EntryErrorMSG = ""
        Me.stbNotes.Location = New System.Drawing.Point(184, 206)
        Me.stbNotes.Multiline = True
        Me.stbNotes.Name = "stbNotes"
        Me.stbNotes.RegularExpression = ""
        Me.stbNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.stbNotes.Size = New System.Drawing.Size(218, 54)
        Me.stbNotes.TabIndex = 18
        '
        'fbnClose
        '
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(330, 309)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 22
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'stbVisitNo
        '
        Me.stbVisitNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbVisitNo.CapitalizeFirstLetter = False
        Me.stbVisitNo.EntryErrorMSG = ""
        Me.stbVisitNo.Location = New System.Drawing.Point(184, 10)
        Me.stbVisitNo.Name = "stbVisitNo"
        Me.stbVisitNo.RegularExpression = ""
        Me.stbVisitNo.Size = New System.Drawing.Size(218, 20)
        Me.stbVisitNo.TabIndex = 2
        '
        'lblVisitNo
        '
        Me.lblVisitNo.Location = New System.Drawing.Point(12, 12)
        Me.lblVisitNo.Name = "lblVisitNo"
        Me.lblVisitNo.Size = New System.Drawing.Size(127, 20)
        Me.lblVisitNo.TabIndex = 0
        Me.lblVisitNo.Text = "Visit No"
        '
        'lblResearchEndDate
        '
        Me.lblResearchEndDate.Location = New System.Drawing.Point(12, 143)
        Me.lblResearchEndDate.Name = "lblResearchEndDate"
        Me.lblResearchEndDate.Size = New System.Drawing.Size(127, 20)
        Me.lblResearchEndDate.TabIndex = 13
        Me.lblResearchEndDate.Text = "Research End Date"
        '
        'lblOutCome
        '
        Me.lblOutCome.Location = New System.Drawing.Point(12, 172)
        Me.lblOutCome.Name = "lblOutCome"
        Me.lblOutCome.Size = New System.Drawing.Size(127, 20)
        Me.lblOutCome.TabIndex = 15
        Me.lblOutCome.Text = "Out Come"
        '
        'lblNotes
        '
        Me.lblNotes.Location = New System.Drawing.Point(12, 217)
        Me.lblNotes.Name = "lblNotes"
        Me.lblNotes.Size = New System.Drawing.Size(127, 20)
        Me.lblNotes.TabIndex = 17
        Me.lblNotes.Text = "Notes"
        '
        'btnFindVisitNo
        '
        Me.btnFindVisitNo.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnFindVisitNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFindVisitNo.Image = CType(resources.GetObject("btnFindVisitNo.Image"), System.Drawing.Image)
        Me.btnFindVisitNo.Location = New System.Drawing.Point(151, 9)
        Me.btnFindVisitNo.Name = "btnFindVisitNo"
        Me.btnFindVisitNo.Size = New System.Drawing.Size(27, 21)
        Me.btnFindVisitNo.TabIndex = 1
        '
        'lblHospitalPID
        '
        Me.lblHospitalPID.Location = New System.Drawing.Point(12, 33)
        Me.lblHospitalPID.Name = "lblHospitalPID"
        Me.lblHospitalPID.Size = New System.Drawing.Size(127, 20)
        Me.lblHospitalPID.TabIndex = 3
        Me.lblHospitalPID.Text = "Patient No"
        '
        'stbPatientNo
        '
        Me.stbPatientNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbPatientNo.CapitalizeFirstLetter = False
        Me.stbPatientNo.Enabled = False
        Me.stbPatientNo.EntryErrorMSG = ""
        Me.stbPatientNo.Location = New System.Drawing.Point(184, 31)
        Me.stbPatientNo.Name = "stbPatientNo"
        Me.stbPatientNo.RegularExpression = ""
        Me.stbPatientNo.Size = New System.Drawing.Size(218, 20)
        Me.stbPatientNo.TabIndex = 4
        '
        'stbGender
        '
        Me.stbGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbGender.CapitalizeFirstLetter = False
        Me.stbGender.Enabled = False
        Me.stbGender.EntryErrorMSG = ""
        Me.stbGender.Location = New System.Drawing.Point(184, 73)
        Me.stbGender.MaxLength = 20
        Me.stbGender.Name = "stbGender"
        Me.stbGender.RegularExpression = ""
        Me.stbGender.Size = New System.Drawing.Size(218, 20)
        Me.stbGender.TabIndex = 8
        '
        'lblGender
        '
        Me.lblGender.Location = New System.Drawing.Point(12, 75)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(127, 20)
        Me.lblGender.TabIndex = 7
        Me.lblGender.Text = "Gender"
        '
        'stbAge
        '
        Me.stbAge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbAge.CapitalizeFirstLetter = False
        Me.stbAge.Enabled = False
        Me.stbAge.EntryErrorMSG = ""
        Me.stbAge.Location = New System.Drawing.Point(184, 95)
        Me.stbAge.MaxLength = 20
        Me.stbAge.Name = "stbAge"
        Me.stbAge.RegularExpression = ""
        Me.stbAge.Size = New System.Drawing.Size(218, 20)
        Me.stbAge.TabIndex = 10
        '
        'lblAge
        '
        Me.lblAge.Location = New System.Drawing.Point(12, 97)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(127, 20)
        Me.lblAge.TabIndex = 9
        Me.lblAge.Text = "Age"
        '
        'stbResearchName
        '
        Me.stbResearchName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbResearchName.CapitalizeFirstLetter = False
        Me.stbResearchName.Enabled = False
        Me.stbResearchName.EntryErrorMSG = ""
        Me.stbResearchName.Location = New System.Drawing.Point(184, 116)
        Me.stbResearchName.MaxLength = 20
        Me.stbResearchName.Name = "stbResearchName"
        Me.stbResearchName.RegularExpression = ""
        Me.stbResearchName.Size = New System.Drawing.Size(218, 20)
        Me.stbResearchName.TabIndex = 12
        '
        'stbFullName
        '
        Me.stbFullName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbFullName.CapitalizeFirstLetter = False
        Me.stbFullName.Enabled = False
        Me.stbFullName.EntryErrorMSG = ""
        Me.stbFullName.Location = New System.Drawing.Point(184, 52)
        Me.stbFullName.MaxLength = 41
        Me.stbFullName.Name = "stbFullName"
        Me.stbFullName.RegularExpression = ""
        Me.stbFullName.Size = New System.Drawing.Size(218, 20)
        Me.stbFullName.TabIndex = 6
        '
        'lblFullName
        '
        Me.lblFullName.Location = New System.Drawing.Point(12, 54)
        Me.lblFullName.Name = "lblFullName"
        Me.lblFullName.Size = New System.Drawing.Size(127, 20)
        Me.lblFullName.TabIndex = 5
        Me.lblFullName.Text = "Patient's Name"
        '
        'lblResearchNameID
        '
        Me.lblResearchNameID.Location = New System.Drawing.Point(12, 118)
        Me.lblResearchNameID.Name = "lblResearchNameID"
        Me.lblResearchNameID.Size = New System.Drawing.Size(127, 20)
        Me.lblResearchNameID.TabIndex = 11
        Me.lblResearchNameID.Text = "Research Name"
        '
        'frmResearchPatientsStop
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(414, 345)
        Me.Controls.Add(Me.lblHospitalPID)
        Me.Controls.Add(Me.stbPatientNo)
        Me.Controls.Add(Me.stbGender)
        Me.Controls.Add(Me.lblGender)
        Me.Controls.Add(Me.stbAge)
        Me.Controls.Add(Me.lblAge)
        Me.Controls.Add(Me.stbResearchName)
        Me.Controls.Add(Me.stbFullName)
        Me.Controls.Add(Me.lblFullName)
        Me.Controls.Add(Me.lblResearchNameID)
        Me.Controls.Add(Me.btnFindVisitNo)
        Me.Controls.Add(Me.fbnSearch)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.fbnClose)
        Me.Controls.Add(Me.stbVisitNo)
        Me.Controls.Add(Me.lblVisitNo)
        Me.Controls.Add(Me.dtpResearchEndDate)
        Me.Controls.Add(Me.lblResearchEndDate)
        Me.Controls.Add(Me.stbOutCome)
        Me.Controls.Add(Me.lblOutCome)
        Me.Controls.Add(Me.stbNotes)
        Me.Controls.Add(Me.lblNotes)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmResearchPatientsStop"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Research Patients Stop"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents stbVisitNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblVisitNo As System.Windows.Forms.Label
    Friend WithEvents dtpResearchEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblResearchEndDate As System.Windows.Forms.Label
    Friend WithEvents stbOutCome As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblOutCome As System.Windows.Forms.Label
    Friend WithEvents stbNotes As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblNotes As System.Windows.Forms.Label
    Friend WithEvents btnFindVisitNo As System.Windows.Forms.Button
    Friend WithEvents lblHospitalPID As System.Windows.Forms.Label
    Friend WithEvents stbPatientNo As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents stbGender As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents stbAge As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblAge As System.Windows.Forms.Label
    Friend WithEvents stbResearchName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents stbFullName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblFullName As System.Windows.Forms.Label
    Friend WithEvents lblResearchNameID As System.Windows.Forms.Label

End Class