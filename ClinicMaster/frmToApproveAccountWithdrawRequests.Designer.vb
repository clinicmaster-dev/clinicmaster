
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmToApproveAccountWithdrawRequests : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
    End Sub

    Private Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal requestStatusID As String, alertControl As Control)
        MyClass.New()
        Me.defautRequestStatusID = requestStatusID
        Me.alertControl = alertControl

    End Sub


    Public Sub New(ByVal requestStatusID As String, withdrawTypeID As String, alertControl As Control)
        MyClass.New()
        Me.defautRequestStatusID = requestStatusID
        Me.alertControl = alertControl
        Me.withdrawTypeID = withdrawTypeID

    End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmToApproveAccountWithdrawRequests))
        Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.dgvToApproveAccountWithdraws = New System.Windows.Forms.DataGridView()
        Me.grpSetParameters = New System.Windows.Forms.GroupBox()
        Me.pnlPeriod = New System.Windows.Forms.Panel()
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker()
        Me.lblStartDate = New System.Windows.Forms.Label()
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker()
        Me.lblEndDate = New System.Windows.Forms.Label()
        Me.lblRecordsNo = New System.Windows.Forms.Label()
        Me.fbnLoad = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.colRecordTime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRecordDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colLoginID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colEndDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colWithdrawType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRequestDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAccountName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAccountBillNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAccountBillModes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRequestNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvToApproveAccountWithdraws,System.ComponentModel.ISupportInitialize).BeginInit
        Me.grpSetParameters.SuspendLayout
        Me.pnlPeriod.SuspendLayout
        Me.SuspendLayout
        '
        'fbnSearch
        '
        Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnSearch.Location = New System.Drawing.Point(17, 393)
        Me.fbnSearch.Name = "fbnSearch"
        Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
        Me.fbnSearch.TabIndex = 0
        Me.fbnSearch.Text = "S&earch"
        Me.fbnSearch.UseVisualStyleBackColor = true
        Me.fbnSearch.Visible = false
        '
        'fbnDelete
        '
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(823, 395)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 1
        Me.fbnDelete.Tag = "ToApproveAccountWithdrawRequests"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = false
        Me.fbnDelete.Visible = false
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(17, 420)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 2
        Me.ebnSaveUpdate.Tag = "ToApproveAccountWithdrawRequests"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = false
        '
        'fbnClose
        '
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(823, 422)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 3
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = false
        '
        'dgvToApproveAccountWithdraws
        '
        Me.dgvToApproveAccountWithdraws.AllowUserToAddRows = false
        Me.dgvToApproveAccountWithdraws.AllowUserToDeleteRows = false
        Me.dgvToApproveAccountWithdraws.AllowUserToOrderColumns = true
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        Me.dgvToApproveAccountWithdraws.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvToApproveAccountWithdraws.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.dgvToApproveAccountWithdraws.BackgroundColor = System.Drawing.Color.GhostWhite
        Me.dgvToApproveAccountWithdraws.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvToApproveAccountWithdraws.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvToApproveAccountWithdraws.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvToApproveAccountWithdraws.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvToApproveAccountWithdraws.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colRequestNo, Me.colAccountBillModes, Me.colAccountBillNo, Me.colAccountName, Me.colRequestDate, Me.colWithdrawType, Me.colEndDate, Me.colLoginID, Me.colRecordDate, Me.colRecordTime})
        Me.dgvToApproveAccountWithdraws.EnableHeadersVisualStyles = false
        Me.dgvToApproveAccountWithdraws.GridColor = System.Drawing.Color.Khaki
        Me.dgvToApproveAccountWithdraws.Location = New System.Drawing.Point(17, 71)
        Me.dgvToApproveAccountWithdraws.Name = "dgvToApproveAccountWithdraws"
        Me.dgvToApproveAccountWithdraws.ReadOnly = true
        Me.dgvToApproveAccountWithdraws.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvToApproveAccountWithdraws.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvToApproveAccountWithdraws.RowHeadersVisible = false
        Me.dgvToApproveAccountWithdraws.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvToApproveAccountWithdraws.Size = New System.Drawing.Size(878, 318)
        Me.dgvToApproveAccountWithdraws.TabIndex = 8
        Me.dgvToApproveAccountWithdraws.Text = "DataGridView1"
        '
        'grpSetParameters
        '
        Me.grpSetParameters.Controls.Add(Me.pnlPeriod)
        Me.grpSetParameters.Controls.Add(Me.lblRecordsNo)
        Me.grpSetParameters.Controls.Add(Me.fbnLoad)
        Me.grpSetParameters.Location = New System.Drawing.Point(17, 3)
        Me.grpSetParameters.Name = "grpSetParameters"
        Me.grpSetParameters.Size = New System.Drawing.Size(878, 63)
        Me.grpSetParameters.TabIndex = 9
        Me.grpSetParameters.TabStop = false
        Me.grpSetParameters.Text = "Period"
        '
        'pnlPeriod
        '
        Me.pnlPeriod.Controls.Add(Me.dtpEndDate)
        Me.pnlPeriod.Controls.Add(Me.lblStartDate)
        Me.pnlPeriod.Controls.Add(Me.dtpStartDate)
        Me.pnlPeriod.Controls.Add(Me.lblEndDate)
        Me.pnlPeriod.Location = New System.Drawing.Point(5, 17)
        Me.pnlPeriod.Name = "pnlPeriod"
        Me.pnlPeriod.Size = New System.Drawing.Size(680, 31)
        Me.pnlPeriod.TabIndex = 4
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Location = New System.Drawing.Point(436, 3)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = true
        Me.dtpEndDate.Size = New System.Drawing.Size(229, 20)
        Me.dtpEndDate.TabIndex = 3
        '
        'lblStartDate
        '
        Me.lblStartDate.Location = New System.Drawing.Point(10, 5)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(83, 20)
        Me.lblStartDate.TabIndex = 0
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Location = New System.Drawing.Point(103, 5)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = true
        Me.dtpStartDate.Size = New System.Drawing.Size(223, 20)
        Me.dtpStartDate.TabIndex = 1
        '
        'lblEndDate
        '
        Me.lblEndDate.Location = New System.Drawing.Point(341, 3)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(89, 20)
        Me.lblEndDate.TabIndex = 2
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRecordsNo
        '
        Me.lblRecordsNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblRecordsNo.ForeColor = System.Drawing.Color.Blue
        Me.lblRecordsNo.Location = New System.Drawing.Point(692, 11)
        Me.lblRecordsNo.Name = "lblRecordsNo"
        Me.lblRecordsNo.Size = New System.Drawing.Size(178, 13)
        Me.lblRecordsNo.TabIndex = 3
        Me.lblRecordsNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'fbnLoad
        '
        Me.fbnLoad.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnLoad.Location = New System.Drawing.Point(693, 29)
        Me.fbnLoad.Name = "fbnLoad"
        Me.fbnLoad.Size = New System.Drawing.Size(106, 28)
        Me.fbnLoad.TabIndex = 5
        Me.fbnLoad.Text = "Load..."
        '
        'colRecordTime
        '
        Me.colRecordTime.DataPropertyName = "RecordTime"
        Me.colRecordTime.HeaderText = "Record Time"
        Me.colRecordTime.Name = "colRecordTime"
        Me.colRecordTime.ReadOnly = true
        Me.colRecordTime.Width = 75
        '
        'colRecordDate
        '
        Me.colRecordDate.DataPropertyName = "RecordDate"
        Me.colRecordDate.HeaderText = "Record Date"
        Me.colRecordDate.Name = "colRecordDate"
        Me.colRecordDate.ReadOnly = true
        Me.colRecordDate.Width = 80
        '
        'colLoginID
        '
        Me.colLoginID.DataPropertyName = "LoginID"
        Me.colLoginID.HeaderText = "Login ID"
        Me.colLoginID.Name = "colLoginID"
        Me.colLoginID.ReadOnly = true
        '
        'colEndDate
        '
        Me.colEndDate.DataPropertyName = "Amount"
        Me.colEndDate.HeaderText = "Amount"
        Me.colEndDate.Name = "colEndDate"
        Me.colEndDate.ReadOnly = true
        Me.colEndDate.Width = 80
        '
        'colWithdrawType
        '
        Me.colWithdrawType.DataPropertyName = "WithdrawType"
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Info
        Me.colWithdrawType.DefaultCellStyle = DataGridViewCellStyle3
        Me.colWithdrawType.HeaderText = "Withdraw Type"
        Me.colWithdrawType.Name = "colWithdrawType"
        Me.colWithdrawType.ReadOnly = true
        '
        'colRequestDate
        '
        Me.colRequestDate.DataPropertyName = "RequestDate"
        Me.colRequestDate.HeaderText = "Request Date"
        Me.colRequestDate.Name = "colRequestDate"
        Me.colRequestDate.ReadOnly = true
        Me.colRequestDate.Width = 80
        '
        'colAccountName
        '
        Me.colAccountName.DataPropertyName = "AccountName"
        Me.colAccountName.HeaderText = "Account Name"
        Me.colAccountName.Name = "colAccountName"
        Me.colAccountName.ReadOnly = true
        Me.colAccountName.Width = 160
        '
        'colAccountBillNo
        '
        Me.colAccountBillNo.DataPropertyName = "AccountBillNo"
        Me.colAccountBillNo.HeaderText = "Account Bill No"
        Me.colAccountBillNo.Name = "colAccountBillNo"
        Me.colAccountBillNo.ReadOnly = true
        '
        'colAccountBillModes
        '
        Me.colAccountBillModes.DataPropertyName = "AccountBillModes"
        Me.colAccountBillModes.HeaderText = "Account Bill Mode"
        Me.colAccountBillModes.Name = "colAccountBillModes"
        Me.colAccountBillModes.ReadOnly = true
        '
        'colRequestNo
        '
        Me.colRequestNo.DataPropertyName = "RequestNo"
        Me.colRequestNo.HeaderText = "Request No"
        Me.colRequestNo.Name = "colRequestNo"
        Me.colRequestNo.ReadOnly = true
        '
        'frmToApproveAccountWithdrawRequests
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(900, 449)
        Me.Controls.Add(Me.grpSetParameters)
        Me.Controls.Add(Me.dgvToApproveAccountWithdraws)
        Me.Controls.Add(Me.fbnSearch)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.fbnClose)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.KeyPreview = true
        Me.MaximizeBox = false
        Me.Name = "frmToApproveAccountWithdrawRequests"
        Me.Text = "To Approve Account Withdraw Requests"
        CType(Me.dgvToApproveAccountWithdraws,System.ComponentModel.ISupportInitialize).EndInit
        Me.grpSetParameters.ResumeLayout(false)
        Me.pnlPeriod.ResumeLayout(false)
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents dgvToApproveAccountWithdraws As System.Windows.Forms.DataGridView
    Friend WithEvents grpSetParameters As System.Windows.Forms.GroupBox
    Friend WithEvents pnlPeriod As System.Windows.Forms.Panel
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents lblRecordsNo As System.Windows.Forms.Label
    Friend WithEvents fbnLoad As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents colRequestNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAccountBillModes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAccountBillNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAccountName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRequestDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colWithdrawType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colEndDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colLoginID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRecordDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRecordTime As System.Windows.Forms.DataGridViewTextBoxColumn

End Class