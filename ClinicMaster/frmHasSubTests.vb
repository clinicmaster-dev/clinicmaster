﻿Option Strict On

Imports System.Text

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls

Imports LookupDataID = SyncSoft.SQLDb.Lookup.LookupDataID
Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects

Public Class frmHasSubTests

#Region " Fields "

    Private defaultTestCode As String = String.Empty
    Private _sourcegrid As DataGridView
    Private _sourcecolumn As DataGridViewColumn
    Private templateNoControl As DataGridViewColumn
    Private _RowIndex As Integer = -1
    Dim dataGridColumn As DataGridViewTextBoxColumn

    Private selectedRow As Integer
    Dim dataGridView As DataGridView
    Private alertNoControl As Control
#End Region

    Private Sub frmHasSubTests_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Me.Cursor = Cursors.WaitCursor


            If Not String.IsNullOrEmpty(defaultTestCode) Then

                Me.ShowTemplates(defaultTestCode)
                Me.ProcessTabKey(True)

            End If

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
    End Sub

    Private Sub ShowTemplates(ByVal testCode As String)

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim oTemplates As New SyncSoft.SQLDb.LabTestsEXT()

            ' Load from Templates

            Dim templates As DataTable = oTemplates.GetLabTestsEXTPossibleResults(testCode).Tables("LabTestsEXTPossibleResults")
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If templates Is Nothing OrElse templates.Rows.Count < 1 Then
                DisplayMessage("No template registered for " + testCode)
            End If
            LoadGridData(Me.dgvTemplates, templates)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub


    Private Sub dgvTemplates_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTemplates.CellDoubleClick
        Try

            Dim PossibleResult As String = Me.dgvTemplates.Item(Me.colPossibleResults.Name, e.RowIndex).Value.ToString()

            Me.dataGridView.Item(Me.dataGridColumn.Name, Me.selectedRow).Value = PossibleResult

            Me.Close()

        Catch ex As Exception
            Return
        End Try
    End Sub

    Private Sub fbnClose_Click(sender As System.Object, e As System.EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub
End Class