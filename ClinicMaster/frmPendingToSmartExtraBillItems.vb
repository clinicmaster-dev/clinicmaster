
Option Strict On

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls

Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects
Imports SyncSoft.SQLDb
Imports SyncSoft.SQLDb.Lookup.LookupDataID

Public Class frmPendingToSmartExtraBillItems

#Region " Fields "
    Private defaultVisitTpeID As String
    Private alertNoControl As Control
    Private oINTExtraBillItems As New INTExtraBillItems()
    Private oIntegrationAgent As New IntegrationAgents()
    Private oVisitTypeID As New VisitTypeID()
#End Region

    Private Sub frmPeriodicToSmartExtraBillItems_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.Cursor = Cursors.WaitCursor()


            Try
                Me.Cursor = Cursors.WaitCursor()
                Dim _INTExtraBillItems As New DataTable
                If Me.defaultVisitTpeID.Equals(oVisitTypeID.InPatient) Then
                    _INTExtraBillItems = oINTExtraBillItems.GetNotSyncedExtraBillItems(oIntegrationAgent.SMART, oVisitTypeID.InPatient, String.Empty).Tables("INTExtraBillItems")
                Else
                    _INTExtraBillItems = oINTExtraBillItems.GetNotSyncedExtraBillItems(oIntegrationAgent.SMART, oVisitTypeID.OutPatient, String.Empty).Tables("INTExtraBillItems")

                End If
                LoadGridData(dgvSmartBills, _INTExtraBillItems)
                FormatGridRow(dgvSmartBills)

            Catch ex As Exception
                ErrorMessage(ex)

            Finally
                Me.Cursor = Cursors.Default()

            End Try



        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try

    End Sub


    Private Sub frmPeriodicToSmartExtraBillItems_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
    End Sub

    Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub



    Private Sub dgvSmartBills_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSmartBills.CellDoubleClick
        Try

            Dim visitNo As String = Me.dgvSmartBills.Item(Me.colVisitNo.Name, e.RowIndex).Value.ToString()

            If TypeOf Me.alertNoControl Is TextBox Then
                CType(Me.alertNoControl, TextBox).Text = visitNo
                CType(Me.alertNoControl, TextBox).Focus()

            ElseIf TypeOf Me.alertNoControl Is SmartTextBox Then
                CType(Me.alertNoControl, SmartTextBox).Text = visitNo
                CType(Me.alertNoControl, SmartTextBox).Focus()

            ElseIf TypeOf Me.alertNoControl Is ComboBox Then
                CType(Me.alertNoControl, ComboBox).Text = visitNo
                CType(Me.alertNoControl, ComboBox).Focus()
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Me.Close()

        Catch ex As Exception
            Return
        End Try
    End Sub
End Class