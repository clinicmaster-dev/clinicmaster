<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.mdiClient = New System.Windows.Forms.MdiClient()
        Me.mnuMain = New System.Windows.Forms.MenuStrip()
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewPatients = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewVisits = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasSelfRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasClaims = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasQuotations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasConsumables = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasExtraCharge = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasConsumableInventory = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasPurchaseOrders = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasGoodsReceivedNote = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasInventoryOrders = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasProcessInventoryOrders = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasInventoryTransfers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasInventoryAcknowledges = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasVisitFiles = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasOutwardFiles = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasInwardFiles = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasSmartCardAuthorisations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasResearchRoutingForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasResearchResearchRoutingForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem43 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuFileNewExtrasResearchEnrollmentInformation = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem44 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuResearchPatientsEnrollment = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExtrasCancerDiagnosis = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewHCTClientCard = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewExternalReferralForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewCashier = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewTriage = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewTriageTriage = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewTriageVisionAssessment = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewTriageIPDVisionAssessment = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewInvoices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewInvoicesInvoices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewInvoicesIPDInvoices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewDoctor = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewLaboratory = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewLaboratoryLabRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewLaboratoryIPDLabRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewLaboratoryLabResults = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewRadiology = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewRadiologyRadiologyRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewRadiologyRadiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewRadiologyIPDRadiologyRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewRadiologyIPDRadiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewCardiology = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewCardiologyCardiologyRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewCardiologyCardiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem8 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuFileNewCardiologyIPDCardiologyRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewPathology = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewPathologyPathologyRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewPathologyPathologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator23 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuFileNewPathologyIPDPathologyRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewPharmacy = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewPharmacyDispense = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewPharmacyIPDDispense = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewPharmacyDrugInventory = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewPharmacyIssueConsumables = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewPharmacyIssueIPDConsumables = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewTheatre = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewTheatreTheatreOperations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewTheatreIPDTheatreOperations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewDental = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewDentalDentalReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewDentalIPDDentalReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewAppointments = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewInPatients = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewInPatientsAdmissions = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewInPatientsExtraBills = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewInPatientsIPDDoctor = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewInPatientsIPDConsumables = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewInPatientsReturnedExtraBillItems = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewInPatientsDischarges = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewManageART = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewManageARTARTRegimen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewManageARTARTStopped = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewManageARTExaminations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNewDeaths = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditPatients = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditVisits = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditExtrasSelfRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditExtrasClaims = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditExtrasQuotations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditExtrasPurchaseOrders = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditExtrasInventoryOrders = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditExtrasInventoryTransfers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditExtrasOutwardFiles = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditExtrasInwardFiles = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditExtrasSmartCardAuthorisations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditExtrasResearchRoutingForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditExtrasResearchEnrollmentInformation = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditExtrasExternalReferralForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditTriage = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditTriageTriage = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditTriageVisionAssessment = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditTriageIPDVisionAssessment = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditInvoices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditInvoicesInvoices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditInvoicesIPDInvoices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditLaboratory = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditLaboratoryLabRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditLaboratoryIPDLabRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditLaboratoryLabResults = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditRadiology = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditRadiologyRadiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditRadiologyIPDRadiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditCardiology = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditCardiologyCardiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditPathology = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditPathologyPathologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditPharmacy = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditTheatre = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditTheatreTheatreOperations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditTheatreIPDTheatreOperations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditDental = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditDentalDentalReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditDentalIPDDentalReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditAppointments = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditInPatients = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditInPatientsAdmissions = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditInPatientsExtraBills = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditInPatientsDischarges = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditManageART = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditManageARTARTRegimen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditManageARTARTStopped = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditManageARTExaminations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEditDeaths = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuFileImport = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileImportLabResults = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileImportLabResultsEXT = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileImportPatients = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileImportVisits = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuFileClose = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuFileSwitchUser = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuFileExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEditCut = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEditCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEditPaste = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEditDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEditSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuEditSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuView = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuViewToolBar = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuViewStatusBar = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuViewSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuViewSearch = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetup = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupLookupData = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSetupNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillable = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillableServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillableDrugs = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillableLabTests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillableCardiologyExaminations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillableRadiologyExaminations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewPathologyExaminations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillableConsumableItems = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillableExtraChargeItems = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillableDentalServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillableTheatreServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillableEyeServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillableOpticalServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillableMaternityServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillableICUServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillableProcedures = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewPackages = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillableBeds = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSetupNewOtherItems = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewDrugCategories = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewStaff = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewDrugCombinations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewRooms = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewDiseasesEXT = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewDiseases = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewCancerDiseases = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewPhysioDiseases = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewUCITopologySites = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewAllergies = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillCustomers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewBillCustomerMembers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewMemberBenefits = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnSetUpNewBankAccounts = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewSuppliers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewLabEXTPossibleResults = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator14 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSetupNewInsurance = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewInsuranceInsurances = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewInsuranceCompanies = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewInsuranceInsurancePolicies = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewInsuranceInsuranceSchemes = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewInsuranceSchemeMembers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewInsuranceHealthUnits = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewGeographicalLocation = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewGeographicalLocationCounties = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewGeographicalLocationSubCounties = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewGeographicalLocationParishes = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewGeographicalLocationVillages = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupNewRevenueStreams = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillable = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableDrugs = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableLabTests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableCardiologyExaminations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableRadiologyExaminations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditPathologyExaminations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableConsumableItems = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableExtraChargeItems = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableDentalServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableTheatreServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableEyeServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableOpticalServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableMaternityServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableICUServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableProcedures = New System.Windows.Forms.ToolStripMenuItem()
        Me.PackagesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableBeds = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillableItemUnitPrice = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator13 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSetupEditChartOfAccounts = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditChartOfAccountsCategories = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator20 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSetupEditChartOfAccountsSubCategories = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem36 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSetupEditOtherItems = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditDrugCategories = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditStaff = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditDrugCombinations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditRooms = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditDiseasesEXT = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditDiseases = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditCancerDiseases = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditTopologySites = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditAllergies = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillCustomers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditBillCustomerMembers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditMemberBenefits = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnSetupEditBankAccounts = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditSuppliers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditPossibleLabResultsSubTest = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator16 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSetupEditInsurance = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditInsuranceInsurances = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditInsuranceInsurancePolicies = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditInsuranceCompanies = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditInsuranceInsuranceSchemes = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditInsuranceSchemeMembers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditInsuranceHealthUnits = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditGeographicalLocation = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditGeographicalLocationCounties = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditGeographicalLocationSubCounties = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditGeographicalLocationParishes = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupEditGeographicalLocationVillages = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSetupExchangeRates = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSetupDrugBarcodes = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSetupNewConsumableBarCode = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupIntegrationAgents = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSetupImport = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportDrugs = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportConsumableItems = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportInventory = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportLabTests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportLabTestsEXT = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportLabPossibleResults = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportRadiologyExaminations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportProcedures = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportDentalServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportBillCustomers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportBillCustomFee = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportInsuranceCustomFee = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportBillExcludedItems = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportInsuranceExclusions = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportInsuranceExcludedItems = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportBillCustomerMembers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportDiseases = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupImportSchemeMembers = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSetupSecurity = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupSecurityUsers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupSecurityRoles = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetupSecurityChangePassword = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSetupSecurityLicenses = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsPayments = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPaymentsCashCollections = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsPaymentsCashReceipts = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsPaymentsDailyFinancialReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsPaymentsPatientsAccountStatement = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsPaymentCategorisation = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem49 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuReportsPaymentsAccountStatement = New System.Windows.Forms.ToolStripMenuItem()
        Me.mniReportdDetailedAccountStatement = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsInvoices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsInvoicesVisits = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsInvoicesVisitInvoices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsInvoicesBillCustomers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsInvoicesInsurances = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportInvoicesDueInvoices = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportInvoicesInvoiceCategorisation = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnReportsBillingForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsBillingFormByVisitNo = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsBillingFormByExtraBillNo = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsExtras = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsExtrasInventoryAcknowledges = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsExtrasGoodsReceivedNote = New System.Windows.Forms.ToolStripMenuItem()
        Me.mniReportsExtrasSupplierHistory = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsLabResults = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportLabResultsLabResultsReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuReportLabResultsLabReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsCardiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsIPDCardiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsRadiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsIPDRadiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsClaims = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsClaimsBillCustomersClaimForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsClaimsInsuranceClaimForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsClaimsInsuranceClaimSummaries = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsIncomeSummaries = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsIPDIncomeSummaries = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsInventory = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsInventoryDrugStockCard = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsInventoryConsumableStockCard = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsInventoryDrugInventorySummaries = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsInventoryConsumableInventorySummaries = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsExtrasPhysicalStockCount = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsDiagnosisSummaries = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsDiagnosisReportsDiagnosisSummaries = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsDiagnosisReportsDiagnosisReattendances = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsLineGraphs = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsLineGraphsLabResults = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsGeneral = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReportsGeneralAppointments = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuReportsGeneralOperations = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuReportsGeneraItemStatus = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem10 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuReportsGeneraPatientRecords = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem48 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuReportsGeneraIOPDSpecialistTransactions = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnReportsBankingRegister = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTools = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsCalculator = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuToolsDashboard = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsDatabase = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsDatabaseBackup = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsDatabaseRestore = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsErrorLog = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsErrorLogView = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsErrorLogClear = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnToolsQueue = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuToolsBulkSMS = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuToolsStaffLocations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnToolsBankingRegiser = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsManageSpecialEdits = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsManageRestrictedKeys = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsReversals = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsReversalsReceipts = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem41 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuToolsReveAccounts = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsManageBillUnitPrice = New System.Windows.Forms.ToolStripMenuItem()
        Me.OutPatientToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InPatientToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MnuToolsSuspenseAccount = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsOthers = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsOthersSagePastel = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsOthersUnsentTextMessages = New System.Windows.Forms.ToolStripMenuItem()
        Me.SMSRemindersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuToolsNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsNewServerCredentials = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsNewTemplates = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsNewImportDataInfo = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsEditServerCredentials = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsEditTemplates = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsEditImportDataInfo = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuToolsSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuToolsOptions = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWindow = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWindowCascade = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWindowTile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWindowTileHorizontal = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWindowTileVertical = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWindowArrangeIcons = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWindowCloseAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLocation = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMessenger = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelpHelpTopics = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelpSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuHelpAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelpEULA = New System.Windows.Forms.ToolStripMenuItem()
        Me.stbMain = New System.Windows.Forms.StatusStrip()
        Me.sbpLogin = New System.Windows.Forms.ToolStripStatusLabel()
        Me.sbpLevel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.sbpServerName = New System.Windows.Forms.ToolStripStatusLabel()
        Me.sbpConnectionMode = New System.Windows.Forms.ToolStripStatusLabel()
        Me.sbpUserID = New System.Windows.Forms.ToolStripStatusLabel()
        Me.sbpPoweredBy = New System.Windows.Forms.ToolStripStatusLabel()
        Me.sbpReady = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tlbMain = New System.Windows.Forms.ToolStrip()
        Me.ddbPatients = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiPatientsNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiPatientsEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.tbbSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbVisits = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiVisitsNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiVisitsEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.tbbSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbExtras = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiExtrasNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiNewOPDExtraBills = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewSelfRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiNewInventoryQuotation = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExtraNewEmergencyCase = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewARTPatient = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewPhysiotherapy = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewClients = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewClaims = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewOutwardFiles = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewInwardFiles = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewSmartCardAuthorisations = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewResearch = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewResearchResearchRoutingForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem9 = New System.Windows.Forms.ToolStripSeparator()
        Me.bmiExtrasNewResearchResearchEnrollmentInformation = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem42 = New System.Windows.Forms.ToolStripSeparator()
        Me.bmiExtrasNewResearchResearchPatientsEnrollments = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem47 = New System.Windows.Forms.ToolStripSeparator()
        Me.bmiExtrasNewResearchResearchPatientsEnd = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewExternalReferralForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewAssetsRegister = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewAssetMaintainanceLog = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewSymptomsHistory = New System.Windows.Forms.ToolStripMenuItem()
        Me.IPDStaffPaymentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OPDStaffPaymentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExtraNewCodeMapping = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewCodeMappingBillCustomers = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewCodeMappingFinance = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExtraNewImmunisation = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasNewServiceInvoices = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExtraNewOccupationalTherapy = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExtraNewPatientsEXT = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiTBIntensifiedCaseFindings = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiPatientRiskFactors = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditSelfRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiEditInventoryQuotation = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditARTPatient = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditPhysiotherapy = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditClients = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditClaims = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditOutwardFiles = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditInwardFiles = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditSmartCardAuthorisations = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditResearchRoutingForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditResearchResearchRoutingForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem45 = New System.Windows.Forms.ToolStripSeparator()
        Me.bmiExtrasEditResearchResearchEnrollmentInformation = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem46 = New System.Windows.Forms.ToolStripSeparator()
        Me.bmiExtraResearchEditResearchPatientsEnrollments = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditExternalReferralForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditAssetsRegister = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditAssetMaintainanceLog = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditSymptomsHistory = New System.Windows.Forms.ToolStripMenuItem()
        Me.IPDStaffPaymentsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.OPDStaffPaymentsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExtraEditCodeMapping = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExtraEditCodeMappingBillCustomers = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExtraEditCodeMappingFinance = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExtraEditCodeMappingBillableMappings = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnExtraEditImmunisation = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditServiceInvoices = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditMaternity = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtraEditMaternityEnrollment = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtraEditMaternityAntenatalVisits = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasEditTBIntensifiedCaseFindings = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiEditPatientRiskFactors = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasExtraCharge = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtraAttachPackage = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasVisitFiles = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiCancerDiagnosis = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiHCTClientCard = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasAccessCashServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClaimPaymentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClaimPaymentsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClaimPaymentDetailedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StaffPaymentApprovalsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtrasAntenatal = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtrasAntenatalnrollment = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtrasAntenatalVisit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtrasRefunds = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtrasRefundsRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtrasRefundApprovals = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiExtrasOPDBillAdjustments = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtrasCodingMappings = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtrasCodingMappingLookupData = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtrasCodingMappingBillableMappings = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtrasCodingMappingCompanies = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbTriage = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiTriageNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiTriageNewTriage = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiTriageNewVisionAssessment = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiTriageNewIPDVisionAssessment = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiTriageEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiTriageEditTriage = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiTriageEditVisionAssessment = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiTriageEditIPDVisionAssessment = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.tbbCashier = New System.Windows.Forms.ToolStripButton()
        Me.tbbSeparotor3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbInvoices = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiInvoicesNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInvoicesNewInvoices = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInvoicesNewIBillFormInvoices = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInvoicesInvoiceAdjustments = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator()
        Me.tbbDoctor = New System.Windows.Forms.ToolStripButton()
        Me.tbbSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbLaboratory = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiLaboratoryNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiLaboratoryNewLabRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiLaboratoryNewIPDLabRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiLaboratoryNewLabResults = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator21 = New System.Windows.Forms.ToolStripSeparator()
        Me.bmiPathologyNewPathologyRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiPathologyNewPathologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiPathologyNewIPDPathologyRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiPathologyNewIPDPathologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiLaboratoryEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiLaboratoryEditLabRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiLaboratoryEditIPDLabRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiLaboratoryEditLabResults = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator22 = New System.Windows.Forms.ToolStripSeparator()
        Me.bmiPathologyEditPathologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiPathologyEditIPDPathologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiLaboratoryApproveLabResults = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiLaboratoryRefundRequest = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiLaboratoryRefundRequestLaboratory = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiLaboratoryRefundRequestPathology = New System.Windows.Forms.ToolStripMenuItem()
        Me.tbbSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbCardiology = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiCardiologyNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiCardiologyNewCardiologyRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiCardiologyNewCardiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripSeparator()
        Me.bmiCardiologyNewIPDCardiologyRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiCardiologyNewIPDCardiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiCardiologyEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiCardiologyEditCardiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiCardiologyEditIPDCardiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiCardiologyRefundRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator19 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbRadiology = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiRadiologyNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiRadiologyNewRadiologyRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiRadiologyNewRadiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiRadiologyNewIPDRadiologyRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiRadiologyNewIPDRadiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiRadiologyEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiRadiologyEditRadiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiRadiologyEditIPDRadiologyReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiRadiologyRefundRequest = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiRadiologyReImages = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbPharmacy = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiPharmacyDispense = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiIPDPharmacyDispense = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiPharmacyIssueConsumables = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiPharmacyIssueIPDConsumables = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiPreviousPrescriptions = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiDrugAdministration = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiOPDPharmacyRefundRequests = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiOPDPharmacyRefundRequestsDrugs = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiOPDPharmacyRefundRequestsConsumables = New System.Windows.Forms.ToolStripMenuItem()
        Me.tbbSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbInventory = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiNewInventory = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiNewInventoryPurchaseOrders = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiNewInventoryInventoryOrders = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryNewInventoryTransfers = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryNewPhysicalStockCount = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem63 = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiEditInventoryPurchaseOrders = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiEditInventoryInventoryOrders = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryEditInventoryTransfers = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryEditPhysicalStockCount = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryConsumables = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiPharmacyConsumableInventory = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryDeliveryNote = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiPharmacyDrugInventory = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuInventoryOtherItemsInventory = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiinventoryInventoryAcknowledges = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiinventoryProcessInventoryOrders = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryGoodsReceived = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryGoodsReturned = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryAcknowledgeReturns = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryAcknowledgeReturnsAcknowledgeBillFormReturns = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryAcknowledgeReturnsAcknowledgeOPDReturns = New System.Windows.Forms.ToolStripMenuItem()
        Me.bminventory = New System.Windows.Forms.ToolStripMenuItem()
        Me.bminventoryDrugStockCard = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryConsumableStockCard = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryDrugInventorySummaries = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryConsumableInventorySummaries = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryPhysicalStockCountReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInventoryImportInventory = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator18 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbTheatreOperations = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiTheatreNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiTheatreNewTheatreOperations = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiTheatreNewIPDTheatreOperations = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiTheatreNewIPDPreOperativeForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiTheatreEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiTheatreEditTheatreOperations = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiTheatreEditIPDTheatreOperations = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddTheatreRefundReques = New System.Windows.Forms.ToolStripMenuItem()
        Me.tbbSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbDental = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiDentalNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiDentalNewDentalReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiDentalNewIPDDentalReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiDentalEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiDentalEditDentalReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiDentalEditIPDDentalReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddDentalRefundReques = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbAppointments = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiAppointmentsNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiAppointmentsEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.tbbSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbInPatients = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiInPatientsNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInPatientsNewAdmissions = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInPatientsNewExtraBills = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInPatientsNewDischarges = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInPatientsEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInPatientsEditAdmissions = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInPatientsEditDischarges = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInPatientsIPDDoctor = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInPatientsIPDConsumables = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInPatientsBillAdjustments = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiInPatientsIPDNurse = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmniInPatientsIPDCancerDiagnosis = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmniInPatientsSmartBilling = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmniInPatientsPara = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator17 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinances = New System.Windows.Forms.ToolStripDropDownButton()
        Me.ddbFinancesAccessCashServices = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem14 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesAccountStatements = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesAccountStatement = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem22 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesDetailedAccountStatement = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem23 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuFinancesAssets = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesNewAsse = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem37 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesAssetMaintenanceLog = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem38 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesInventory = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesInventoryDrugInventorySummaries = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem35 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesInventoryConsumableInventorySummaries = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem39 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesInventoryConsumableInventoryPayments = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem40 = New System.Windows.Forms.ToolStripSeparator()
        Me.PaymentVoucherBalances = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem34 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesCashier = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem19 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesClaims = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesClaimsToBillCustomersClaimForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem12 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesClaimsInsuranceClaimForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem18 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesInsuranceClaimSummaries = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesBillFormAdjustment = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem13 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesInvoiceAdjustments = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem11 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesBanking = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesBankingNewBankAccount = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem24 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesBankingRegister = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem25 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesBankingReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem26 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesClaimPayments = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesClaimPaymentsClaimPayments = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem16 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesClaimPaymentsClaimPaymentDetailed = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem17 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesIncome = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesIncomeCashCollections = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesOPDIncomeSummaries = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesIPDIncomeSummaries = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnFinCollectionBreakDownIncomeCollectionBreakdown = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem27 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesInvoices = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesInvoicesVisits = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem29 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesInvoicesToBillCustomers = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem30 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesInvoicesInsurances = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem31 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesInvoicesBillingForm = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem32 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesInvoicesInvoiceCategorisation = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem28 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesRefunds = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesRefundsRequest = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesRefundsApproval = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesAccountActivations = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesAccountWithdraws = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesAccountWithdrawsRequest = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesAccountWithdrawsRequestNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesAccountWithdrawsRequestEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddFinancesAccountWithdrawsApprovals = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddFinancesAccountWithdrawsApprovalsNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddFinancesAccountWithdrawsApprovalsEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesSuspenseAccount = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem15 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesStaffPaymentApprovals = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesStaffPaymentsOPDStaffPayments = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem20 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesStaffPaymentsIPDStaffPayments = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem21 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesStaffPaymentsPaymentApprovalsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem33 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbFinancesSmartBilling = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesSmartBillingOPD = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbFinancesSmartBillingIPD = New System.Windows.Forms.ToolStripMenuItem()
        Me.MobileMoneyPaymentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbMakePayment = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbCheckPayment = New System.Windows.Forms.ToolStripMenuItem()
        Me.ddbManageART = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiManageARTNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiManageARTNewARTRegimen = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiManageARTNewARTStopped = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiManageARTEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiManageARTEditARTRegimen = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiManageARTEditARTStopped = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator15 = New System.Windows.Forms.ToolStripSeparator()
        Me.ddbDeaths = New System.Windows.Forms.ToolStripDropDownButton()
        Me.bmiDeathsNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.bmiDeathsEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.tmrUserIdleDuration = New System.Windows.Forms.Timer(Me.components)
        Me.tmrSMSNotifications = New System.Windows.Forms.Timer(Me.components)
        Me.ntIMessages = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.mnuMain.SuspendLayout()
        Me.stbMain.SuspendLayout()
        Me.tlbMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'mdiClient
        '
        Me.mdiClient.Dock = System.Windows.Forms.DockStyle.Fill
        Me.mdiClient.Location = New System.Drawing.Point(0, 62)
        Me.mdiClient.Name = "mdiClient"
        Me.mdiClient.Size = New System.Drawing.Size(1155, 394)
        Me.mdiClient.TabIndex = 0
        '
        'mnuMain
        '
        Me.mnuMain.BackColor = System.Drawing.Color.WhiteSmoke
        Me.mnuMain.BackgroundImage = CType(resources.GetObject("mnuMain.BackgroundImage"), System.Drawing.Image)
        Me.mnuMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile, Me.mnuEdit, Me.mnuView, Me.mnuSetup, Me.mnuReports, Me.mnuTools, Me.mnuWindow, Me.mnuLocation, Me.mnuMessenger, Me.mnuHelp})
        Me.mnuMain.Location = New System.Drawing.Point(0, 0)
        Me.mnuMain.MdiWindowListItem = Me.mnuWindow
        Me.mnuMain.Name = "mnuMain"
        Me.mnuMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuMain.Size = New System.Drawing.Size(1155, 24)
        Me.mnuMain.TabIndex = 3
        Me.mnuMain.Text = "Main"
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNew, Me.mnuFileEdit, Me.mnuFileSeparator1, Me.mnuFileImport, Me.ToolStripSeparator5, Me.mnuFileClose, Me.mnuFileSeparator2, Me.mnuFileSwitchUser, Me.mnuFileSeparator3, Me.mnuFileExit})
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Size = New System.Drawing.Size(37, 20)
        Me.mnuFile.Text = "&File"
        '
        'mnuFileNew
        '
        Me.mnuFileNew.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewPatients, Me.mnuFileNewVisits, Me.mnuFileNewExtras, Me.mnuFileNewCashier, Me.mnuFileNewTriage, Me.mnuFileNewInvoices, Me.mnuFileNewDoctor, Me.mnuFileNewLaboratory, Me.mnuFileNewRadiology, Me.mnuFileNewCardiology, Me.mnuFileNewPathology, Me.mnuFileNewPharmacy, Me.mnuFileNewTheatre, Me.mnuFileNewDental, Me.mnuFileNewAppointments, Me.mnuFileNewInPatients, Me.mnuFileNewManageART, Me.mnuFileNewDeaths})
        Me.mnuFileNew.Image = CType(resources.GetObject("mnuFileNew.Image"), System.Drawing.Image)
        Me.mnuFileNew.Name = "mnuFileNew"
        Me.mnuFileNew.Size = New System.Drawing.Size(175, 22)
        Me.mnuFileNew.Text = "New"
        '
        'mnuFileNewPatients
        '
        Me.mnuFileNewPatients.Image = CType(resources.GetObject("mnuFileNewPatients.Image"), System.Drawing.Image)
        Me.mnuFileNewPatients.Name = "mnuFileNewPatients"
        Me.mnuFileNewPatients.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewPatients.Tag = "Patients"
        Me.mnuFileNewPatients.Text = "Patients"
        '
        'mnuFileNewVisits
        '
        Me.mnuFileNewVisits.Image = CType(resources.GetObject("mnuFileNewVisits.Image"), System.Drawing.Image)
        Me.mnuFileNewVisits.Name = "mnuFileNewVisits"
        Me.mnuFileNewVisits.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewVisits.Tag = "Visits"
        Me.mnuFileNewVisits.Text = "Visits"
        '
        'mnuFileNewExtras
        '
        Me.mnuFileNewExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewExtrasSelfRequests, Me.mnuFileNewExtrasClaims, Me.mnuFileNewExtrasQuotations, Me.mnuFileNewExtrasConsumables, Me.mnuFileNewExtrasExtraCharge, Me.mnuFileNewExtrasConsumableInventory, Me.mnuFileNewExtrasPurchaseOrders, Me.mnuFileNewExtrasGoodsReceivedNote, Me.mnuFileNewExtrasInventoryOrders, Me.mnuFileNewExtrasProcessInventoryOrders, Me.mnuFileNewExtrasInventoryTransfers, Me.mnuFileNewExtrasInventoryAcknowledges, Me.mnuFileNewExtrasVisitFiles, Me.mnuFileNewExtrasOutwardFiles, Me.mnuFileNewExtrasInwardFiles, Me.mnuFileNewExtrasSmartCardAuthorisations, Me.mnuFileNewExtrasResearchRoutingForm, Me.mnuFileNewExtrasCancerDiagnosis, Me.mnuFileNewHCTClientCard, Me.mnuFileNewExternalReferralForm})
        Me.mnuFileNewExtras.Image = CType(resources.GetObject("mnuFileNewExtras.Image"), System.Drawing.Image)
        Me.mnuFileNewExtras.Name = "mnuFileNewExtras"
        Me.mnuFileNewExtras.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewExtras.Tag = ""
        Me.mnuFileNewExtras.Text = "Extras"
        '
        'mnuFileNewExtrasSelfRequests
        '
        Me.mnuFileNewExtrasSelfRequests.Image = CType(resources.GetObject("mnuFileNewExtrasSelfRequests.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasSelfRequests.Name = "mnuFileNewExtrasSelfRequests"
        Me.mnuFileNewExtrasSelfRequests.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasSelfRequests.Tag = "SelfRequests"
        Me.mnuFileNewExtrasSelfRequests.Text = "Self Requests"
        '
        'mnuFileNewExtrasClaims
        '
        Me.mnuFileNewExtrasClaims.Image = CType(resources.GetObject("mnuFileNewExtrasClaims.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasClaims.Name = "mnuFileNewExtrasClaims"
        Me.mnuFileNewExtrasClaims.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasClaims.Tag = "Claims"
        Me.mnuFileNewExtrasClaims.Text = "Claims"
        '
        'mnuFileNewExtrasQuotations
        '
        Me.mnuFileNewExtrasQuotations.Image = CType(resources.GetObject("mnuFileNewExtrasQuotations.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasQuotations.Name = "mnuFileNewExtrasQuotations"
        Me.mnuFileNewExtrasQuotations.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasQuotations.Tag = "Quotations"
        Me.mnuFileNewExtrasQuotations.Text = "Quotations"
        '
        'mnuFileNewExtrasConsumables
        '
        Me.mnuFileNewExtrasConsumables.Image = CType(resources.GetObject("mnuFileNewExtrasConsumables.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasConsumables.Name = "mnuFileNewExtrasConsumables"
        Me.mnuFileNewExtrasConsumables.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasConsumables.Tag = "Consumables"
        Me.mnuFileNewExtrasConsumables.Text = "Consumables"
        '
        'mnuFileNewExtrasExtraCharge
        '
        Me.mnuFileNewExtrasExtraCharge.Image = CType(resources.GetObject("mnuFileNewExtrasExtraCharge.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasExtraCharge.Name = "mnuFileNewExtrasExtraCharge"
        Me.mnuFileNewExtrasExtraCharge.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasExtraCharge.Tag = "ExtraCharge"
        Me.mnuFileNewExtrasExtraCharge.Text = "Extra Charge"
        '
        'mnuFileNewExtrasConsumableInventory
        '
        Me.mnuFileNewExtrasConsumableInventory.Image = CType(resources.GetObject("mnuFileNewExtrasConsumableInventory.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasConsumableInventory.Name = "mnuFileNewExtrasConsumableInventory"
        Me.mnuFileNewExtrasConsumableInventory.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasConsumableInventory.Tag = "ConsumableInventory"
        Me.mnuFileNewExtrasConsumableInventory.Text = "Consumable Inventory"
        '
        'mnuFileNewExtrasPurchaseOrders
        '
        Me.mnuFileNewExtrasPurchaseOrders.Image = CType(resources.GetObject("mnuFileNewExtrasPurchaseOrders.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasPurchaseOrders.Name = "mnuFileNewExtrasPurchaseOrders"
        Me.mnuFileNewExtrasPurchaseOrders.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasPurchaseOrders.Tag = "PurchaseOrders"
        Me.mnuFileNewExtrasPurchaseOrders.Text = "Purchase Orders"
        '
        'mnuFileNewExtrasGoodsReceivedNote
        '
        Me.mnuFileNewExtrasGoodsReceivedNote.Image = CType(resources.GetObject("mnuFileNewExtrasGoodsReceivedNote.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasGoodsReceivedNote.Name = "mnuFileNewExtrasGoodsReceivedNote"
        Me.mnuFileNewExtrasGoodsReceivedNote.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasGoodsReceivedNote.Tag = "GoodsReceivedNote"
        Me.mnuFileNewExtrasGoodsReceivedNote.Text = "Goods Received Note"
        '
        'mnuFileNewExtrasInventoryOrders
        '
        Me.mnuFileNewExtrasInventoryOrders.Image = CType(resources.GetObject("mnuFileNewExtrasInventoryOrders.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasInventoryOrders.Name = "mnuFileNewExtrasInventoryOrders"
        Me.mnuFileNewExtrasInventoryOrders.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasInventoryOrders.Tag = "InventoryOrders"
        Me.mnuFileNewExtrasInventoryOrders.Text = "Inventory Orders"
        '
        'mnuFileNewExtrasProcessInventoryOrders
        '
        Me.mnuFileNewExtrasProcessInventoryOrders.Image = CType(resources.GetObject("mnuFileNewExtrasProcessInventoryOrders.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasProcessInventoryOrders.Name = "mnuFileNewExtrasProcessInventoryOrders"
        Me.mnuFileNewExtrasProcessInventoryOrders.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasProcessInventoryOrders.Tag = "InventoryOrders"
        Me.mnuFileNewExtrasProcessInventoryOrders.Text = "Process Inventory Orders"
        '
        'mnuFileNewExtrasInventoryTransfers
        '
        Me.mnuFileNewExtrasInventoryTransfers.Image = CType(resources.GetObject("mnuFileNewExtrasInventoryTransfers.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasInventoryTransfers.Name = "mnuFileNewExtrasInventoryTransfers"
        Me.mnuFileNewExtrasInventoryTransfers.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasInventoryTransfers.Tag = "InventoryTransfers"
        Me.mnuFileNewExtrasInventoryTransfers.Text = "Inventory Transfers"
        '
        'mnuFileNewExtrasInventoryAcknowledges
        '
        Me.mnuFileNewExtrasInventoryAcknowledges.Checked = True
        Me.mnuFileNewExtrasInventoryAcknowledges.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuFileNewExtrasInventoryAcknowledges.Image = CType(resources.GetObject("mnuFileNewExtrasInventoryAcknowledges.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasInventoryAcknowledges.Name = "mnuFileNewExtrasInventoryAcknowledges"
        Me.mnuFileNewExtrasInventoryAcknowledges.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasInventoryAcknowledges.Tag = "InventoryAcknowledges"
        Me.mnuFileNewExtrasInventoryAcknowledges.Text = "Inventory Acknowledges"
        '
        'mnuFileNewExtrasVisitFiles
        '
        Me.mnuFileNewExtrasVisitFiles.Image = CType(resources.GetObject("mnuFileNewExtrasVisitFiles.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasVisitFiles.Name = "mnuFileNewExtrasVisitFiles"
        Me.mnuFileNewExtrasVisitFiles.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasVisitFiles.Tag = "VisitFiles"
        Me.mnuFileNewExtrasVisitFiles.Text = "Visit Files"
        '
        'mnuFileNewExtrasOutwardFiles
        '
        Me.mnuFileNewExtrasOutwardFiles.Image = CType(resources.GetObject("mnuFileNewExtrasOutwardFiles.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasOutwardFiles.Name = "mnuFileNewExtrasOutwardFiles"
        Me.mnuFileNewExtrasOutwardFiles.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasOutwardFiles.Tag = "OutwardFiles"
        Me.mnuFileNewExtrasOutwardFiles.Text = "Outward Files"
        '
        'mnuFileNewExtrasInwardFiles
        '
        Me.mnuFileNewExtrasInwardFiles.Image = CType(resources.GetObject("mnuFileNewExtrasInwardFiles.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasInwardFiles.Name = "mnuFileNewExtrasInwardFiles"
        Me.mnuFileNewExtrasInwardFiles.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasInwardFiles.Tag = "InwardFiles"
        Me.mnuFileNewExtrasInwardFiles.Text = "Inward Files"
        '
        'mnuFileNewExtrasSmartCardAuthorisations
        '
        Me.mnuFileNewExtrasSmartCardAuthorisations.Image = CType(resources.GetObject("mnuFileNewExtrasSmartCardAuthorisations.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasSmartCardAuthorisations.Name = "mnuFileNewExtrasSmartCardAuthorisations"
        Me.mnuFileNewExtrasSmartCardAuthorisations.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasSmartCardAuthorisations.Tag = "SmartCardAuthorisations"
        Me.mnuFileNewExtrasSmartCardAuthorisations.Text = "Smart Card Authorisations"
        '
        'mnuFileNewExtrasResearchRoutingForm
        '
        Me.mnuFileNewExtrasResearchRoutingForm.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewExtrasResearchResearchRoutingForm, Me.ToolStripMenuItem43, Me.mnuFileNewExtrasResearchEnrollmentInformation, Me.ToolStripMenuItem44, Me.mnuResearchPatientsEnrollment})
        Me.mnuFileNewExtrasResearchRoutingForm.Image = CType(resources.GetObject("mnuFileNewExtrasResearchRoutingForm.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasResearchRoutingForm.Name = "mnuFileNewExtrasResearchRoutingForm"
        Me.mnuFileNewExtrasResearchRoutingForm.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasResearchRoutingForm.Tag = "ResearchRoutingForm"
        Me.mnuFileNewExtrasResearchRoutingForm.Text = "Research"
        '
        'mnuFileNewExtrasResearchResearchRoutingForm
        '
        Me.mnuFileNewExtrasResearchResearchRoutingForm.Name = "mnuFileNewExtrasResearchResearchRoutingForm"
        Me.mnuFileNewExtrasResearchResearchRoutingForm.Size = New System.Drawing.Size(248, 22)
        Me.mnuFileNewExtrasResearchResearchRoutingForm.Tag = "ResearchRoutingForm"
        Me.mnuFileNewExtrasResearchResearchRoutingForm.Text = "Research Routing Form"
        '
        'ToolStripMenuItem43
        '
        Me.ToolStripMenuItem43.Name = "ToolStripMenuItem43"
        Me.ToolStripMenuItem43.Size = New System.Drawing.Size(245, 6)
        '
        'mnuFileNewExtrasResearchEnrollmentInformation
        '
        Me.mnuFileNewExtrasResearchEnrollmentInformation.Name = "mnuFileNewExtrasResearchEnrollmentInformation"
        Me.mnuFileNewExtrasResearchEnrollmentInformation.Size = New System.Drawing.Size(248, 22)
        Me.mnuFileNewExtrasResearchEnrollmentInformation.Text = "Research Enrollment Information"
        '
        'ToolStripMenuItem44
        '
        Me.ToolStripMenuItem44.Name = "ToolStripMenuItem44"
        Me.ToolStripMenuItem44.Size = New System.Drawing.Size(245, 6)
        '
        'mnuResearchPatientsEnrollment
        '
        Me.mnuResearchPatientsEnrollment.Name = "mnuResearchPatientsEnrollment"
        Me.mnuResearchPatientsEnrollment.Size = New System.Drawing.Size(248, 22)
        Me.mnuResearchPatientsEnrollment.Text = "Research Patients Enrollment"
        '
        'mnuFileNewExtrasCancerDiagnosis
        '
        Me.mnuFileNewExtrasCancerDiagnosis.Image = CType(resources.GetObject("mnuFileNewExtrasCancerDiagnosis.Image"), System.Drawing.Image)
        Me.mnuFileNewExtrasCancerDiagnosis.Name = "mnuFileNewExtrasCancerDiagnosis"
        Me.mnuFileNewExtrasCancerDiagnosis.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExtrasCancerDiagnosis.Tag = "CancerDiagnosis"
        Me.mnuFileNewExtrasCancerDiagnosis.Text = "Cancer Diagnosis"
        '
        'mnuFileNewHCTClientCard
        '
        Me.mnuFileNewHCTClientCard.Image = CType(resources.GetObject("mnuFileNewHCTClientCard.Image"), System.Drawing.Image)
        Me.mnuFileNewHCTClientCard.Name = "mnuFileNewHCTClientCard"
        Me.mnuFileNewHCTClientCard.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewHCTClientCard.Tag = "HCTClientCard"
        Me.mnuFileNewHCTClientCard.Text = "HCT Client Card"
        '
        'mnuFileNewExternalReferralForm
        '
        Me.mnuFileNewExternalReferralForm.Image = CType(resources.GetObject("mnuFileNewExternalReferralForm.Image"), System.Drawing.Image)
        Me.mnuFileNewExternalReferralForm.Name = "mnuFileNewExternalReferralForm"
        Me.mnuFileNewExternalReferralForm.Size = New System.Drawing.Size(213, 22)
        Me.mnuFileNewExternalReferralForm.Tag = "ExternalReferrals"
        Me.mnuFileNewExternalReferralForm.Text = "External Referral Form "
        '
        'mnuFileNewCashier
        '
        Me.mnuFileNewCashier.Image = CType(resources.GetObject("mnuFileNewCashier.Image"), System.Drawing.Image)
        Me.mnuFileNewCashier.Name = "mnuFileNewCashier"
        Me.mnuFileNewCashier.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewCashier.Tag = "Payments"
        Me.mnuFileNewCashier.Text = "Cashier"
        '
        'mnuFileNewTriage
        '
        Me.mnuFileNewTriage.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewTriageTriage, Me.mnuFileNewTriageVisionAssessment, Me.mnuFileNewTriageIPDVisionAssessment})
        Me.mnuFileNewTriage.Image = CType(resources.GetObject("mnuFileNewTriage.Image"), System.Drawing.Image)
        Me.mnuFileNewTriage.Name = "mnuFileNewTriage"
        Me.mnuFileNewTriage.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewTriage.Tag = ""
        Me.mnuFileNewTriage.Text = "Triage"
        '
        'mnuFileNewTriageTriage
        '
        Me.mnuFileNewTriageTriage.Image = CType(resources.GetObject("mnuFileNewTriageTriage.Image"), System.Drawing.Image)
        Me.mnuFileNewTriageTriage.Name = "mnuFileNewTriageTriage"
        Me.mnuFileNewTriageTriage.Size = New System.Drawing.Size(192, 22)
        Me.mnuFileNewTriageTriage.Tag = "Triage"
        Me.mnuFileNewTriageTriage.Text = "Triage"
        '
        'mnuFileNewTriageVisionAssessment
        '
        Me.mnuFileNewTriageVisionAssessment.Image = CType(resources.GetObject("mnuFileNewTriageVisionAssessment.Image"), System.Drawing.Image)
        Me.mnuFileNewTriageVisionAssessment.Name = "mnuFileNewTriageVisionAssessment"
        Me.mnuFileNewTriageVisionAssessment.Size = New System.Drawing.Size(192, 22)
        Me.mnuFileNewTriageVisionAssessment.Tag = "VisionAssessment"
        Me.mnuFileNewTriageVisionAssessment.Text = "Vision Assessment"
        '
        'mnuFileNewTriageIPDVisionAssessment
        '
        Me.mnuFileNewTriageIPDVisionAssessment.Name = "mnuFileNewTriageIPDVisionAssessment"
        Me.mnuFileNewTriageIPDVisionAssessment.Size = New System.Drawing.Size(192, 22)
        Me.mnuFileNewTriageIPDVisionAssessment.Tag = "IPDVisionAssessment"
        Me.mnuFileNewTriageIPDVisionAssessment.Text = "IPD Vision Assessment"
        '
        'mnuFileNewInvoices
        '
        Me.mnuFileNewInvoices.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewInvoicesInvoices, Me.mnuFileNewInvoicesIPDInvoices})
        Me.mnuFileNewInvoices.Image = CType(resources.GetObject("mnuFileNewInvoices.Image"), System.Drawing.Image)
        Me.mnuFileNewInvoices.Name = "mnuFileNewInvoices"
        Me.mnuFileNewInvoices.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewInvoices.Tag = ""
        Me.mnuFileNewInvoices.Text = "Invoices"
        '
        'mnuFileNewInvoicesInvoices
        '
        Me.mnuFileNewInvoicesInvoices.Image = CType(resources.GetObject("mnuFileNewInvoicesInvoices.Image"), System.Drawing.Image)
        Me.mnuFileNewInvoicesInvoices.Name = "mnuFileNewInvoicesInvoices"
        Me.mnuFileNewInvoicesInvoices.Size = New System.Drawing.Size(138, 22)
        Me.mnuFileNewInvoicesInvoices.Tag = "Invoices"
        Me.mnuFileNewInvoicesInvoices.Text = "Invoices"
        '
        'mnuFileNewInvoicesIPDInvoices
        '
        Me.mnuFileNewInvoicesIPDInvoices.Image = CType(resources.GetObject("mnuFileNewInvoicesIPDInvoices.Image"), System.Drawing.Image)
        Me.mnuFileNewInvoicesIPDInvoices.Name = "mnuFileNewInvoicesIPDInvoices"
        Me.mnuFileNewInvoicesIPDInvoices.Size = New System.Drawing.Size(138, 22)
        Me.mnuFileNewInvoicesIPDInvoices.Tag = "Invoices"
        Me.mnuFileNewInvoicesIPDInvoices.Text = "IPD Invoices"
        '
        'mnuFileNewDoctor
        '
        Me.mnuFileNewDoctor.Image = CType(resources.GetObject("mnuFileNewDoctor.Image"), System.Drawing.Image)
        Me.mnuFileNewDoctor.Name = "mnuFileNewDoctor"
        Me.mnuFileNewDoctor.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewDoctor.Tag = "DoctorVisits"
        Me.mnuFileNewDoctor.Text = "Doctor"
        '
        'mnuFileNewLaboratory
        '
        Me.mnuFileNewLaboratory.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewLaboratoryLabRequests, Me.mnuFileNewLaboratoryIPDLabRequests, Me.mnuFileNewLaboratoryLabResults})
        Me.mnuFileNewLaboratory.Image = CType(resources.GetObject("mnuFileNewLaboratory.Image"), System.Drawing.Image)
        Me.mnuFileNewLaboratory.Name = "mnuFileNewLaboratory"
        Me.mnuFileNewLaboratory.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewLaboratory.Tag = ""
        Me.mnuFileNewLaboratory.Text = "Laboratory"
        '
        'mnuFileNewLaboratoryLabRequests
        '
        Me.mnuFileNewLaboratoryLabRequests.Image = CType(resources.GetObject("mnuFileNewLaboratoryLabRequests.Image"), System.Drawing.Image)
        Me.mnuFileNewLaboratoryLabRequests.Name = "mnuFileNewLaboratoryLabRequests"
        Me.mnuFileNewLaboratoryLabRequests.Size = New System.Drawing.Size(164, 22)
        Me.mnuFileNewLaboratoryLabRequests.Tag = "LabRequests"
        Me.mnuFileNewLaboratoryLabRequests.Text = "Lab Requests"
        '
        'mnuFileNewLaboratoryIPDLabRequests
        '
        Me.mnuFileNewLaboratoryIPDLabRequests.Image = CType(resources.GetObject("mnuFileNewLaboratoryIPDLabRequests.Image"), System.Drawing.Image)
        Me.mnuFileNewLaboratoryIPDLabRequests.Name = "mnuFileNewLaboratoryIPDLabRequests"
        Me.mnuFileNewLaboratoryIPDLabRequests.Size = New System.Drawing.Size(164, 22)
        Me.mnuFileNewLaboratoryIPDLabRequests.Tag = "IPDLabRequests"
        Me.mnuFileNewLaboratoryIPDLabRequests.Text = "IPD Lab Requests"
        '
        'mnuFileNewLaboratoryLabResults
        '
        Me.mnuFileNewLaboratoryLabResults.Image = CType(resources.GetObject("mnuFileNewLaboratoryLabResults.Image"), System.Drawing.Image)
        Me.mnuFileNewLaboratoryLabResults.Name = "mnuFileNewLaboratoryLabResults"
        Me.mnuFileNewLaboratoryLabResults.Size = New System.Drawing.Size(164, 22)
        Me.mnuFileNewLaboratoryLabResults.Tag = "LabResults"
        Me.mnuFileNewLaboratoryLabResults.Text = "Lab Results"
        '
        'mnuFileNewRadiology
        '
        Me.mnuFileNewRadiology.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewRadiologyRadiologyRequests, Me.mnuFileNewRadiologyRadiologyReports, Me.mnuFileNewRadiologyIPDRadiologyRequests, Me.mnuFileNewRadiologyIPDRadiologyReports})
        Me.mnuFileNewRadiology.Image = CType(resources.GetObject("mnuFileNewRadiology.Image"), System.Drawing.Image)
        Me.mnuFileNewRadiology.Name = "mnuFileNewRadiology"
        Me.mnuFileNewRadiology.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewRadiology.Tag = ""
        Me.mnuFileNewRadiology.Text = "Radiology"
        '
        'mnuFileNewRadiologyRadiologyRequests
        '
        Me.mnuFileNewRadiologyRadiologyRequests.Image = CType(resources.GetObject("mnuFileNewRadiologyRadiologyRequests.Image"), System.Drawing.Image)
        Me.mnuFileNewRadiologyRadiologyRequests.Name = "mnuFileNewRadiologyRadiologyRequests"
        Me.mnuFileNewRadiologyRadiologyRequests.Size = New System.Drawing.Size(198, 22)
        Me.mnuFileNewRadiologyRadiologyRequests.Tag = "RadiologyRequests"
        Me.mnuFileNewRadiologyRadiologyRequests.Text = "Radiology Requests"
        '
        'mnuFileNewRadiologyRadiologyReports
        '
        Me.mnuFileNewRadiologyRadiologyReports.Image = CType(resources.GetObject("mnuFileNewRadiologyRadiologyReports.Image"), System.Drawing.Image)
        Me.mnuFileNewRadiologyRadiologyReports.Name = "mnuFileNewRadiologyRadiologyReports"
        Me.mnuFileNewRadiologyRadiologyReports.Size = New System.Drawing.Size(198, 22)
        Me.mnuFileNewRadiologyRadiologyReports.Tag = "RadiologyReports"
        Me.mnuFileNewRadiologyRadiologyReports.Text = "Radiology Reports"
        '
        'mnuFileNewRadiologyIPDRadiologyRequests
        '
        Me.mnuFileNewRadiologyIPDRadiologyRequests.Image = CType(resources.GetObject("mnuFileNewRadiologyIPDRadiologyRequests.Image"), System.Drawing.Image)
        Me.mnuFileNewRadiologyIPDRadiologyRequests.Name = "mnuFileNewRadiologyIPDRadiologyRequests"
        Me.mnuFileNewRadiologyIPDRadiologyRequests.Size = New System.Drawing.Size(198, 22)
        Me.mnuFileNewRadiologyIPDRadiologyRequests.Tag = "IPDRadiologyRequests"
        Me.mnuFileNewRadiologyIPDRadiologyRequests.Text = "IPD Radiology Requests"
        '
        'mnuFileNewRadiologyIPDRadiologyReports
        '
        Me.mnuFileNewRadiologyIPDRadiologyReports.Image = CType(resources.GetObject("mnuFileNewRadiologyIPDRadiologyReports.Image"), System.Drawing.Image)
        Me.mnuFileNewRadiologyIPDRadiologyReports.Name = "mnuFileNewRadiologyIPDRadiologyReports"
        Me.mnuFileNewRadiologyIPDRadiologyReports.Size = New System.Drawing.Size(198, 22)
        Me.mnuFileNewRadiologyIPDRadiologyReports.Tag = "IPDRadiologyReports"
        Me.mnuFileNewRadiologyIPDRadiologyReports.Text = "IPD Radiology Reports"
        '
        'mnuFileNewCardiology
        '
        Me.mnuFileNewCardiology.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewCardiologyCardiologyRequests, Me.mnuFileNewCardiologyCardiologyReports, Me.ToolStripMenuItem8, Me.mnuFileNewCardiologyIPDCardiologyRequests})
        Me.mnuFileNewCardiology.Image = CType(resources.GetObject("mnuFileNewCardiology.Image"), System.Drawing.Image)
        Me.mnuFileNewCardiology.Name = "mnuFileNewCardiology"
        Me.mnuFileNewCardiology.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewCardiology.Tag = ""
        Me.mnuFileNewCardiology.Text = "Cardiology"
        '
        'mnuFileNewCardiologyCardiologyRequests
        '
        Me.mnuFileNewCardiologyCardiologyRequests.Image = CType(resources.GetObject("mnuFileNewCardiologyCardiologyRequests.Image"), System.Drawing.Image)
        Me.mnuFileNewCardiologyCardiologyRequests.Name = "mnuFileNewCardiologyCardiologyRequests"
        Me.mnuFileNewCardiologyCardiologyRequests.Size = New System.Drawing.Size(203, 22)
        Me.mnuFileNewCardiologyCardiologyRequests.Tag = "PathologyRequests"
        Me.mnuFileNewCardiologyCardiologyRequests.Text = "Cardiology Requests"
        '
        'mnuFileNewCardiologyCardiologyReports
        '
        Me.mnuFileNewCardiologyCardiologyReports.Image = CType(resources.GetObject("mnuFileNewCardiologyCardiologyReports.Image"), System.Drawing.Image)
        Me.mnuFileNewCardiologyCardiologyReports.Name = "mnuFileNewCardiologyCardiologyReports"
        Me.mnuFileNewCardiologyCardiologyReports.Size = New System.Drawing.Size(203, 22)
        Me.mnuFileNewCardiologyCardiologyReports.Tag = "PathologyReports"
        Me.mnuFileNewCardiologyCardiologyReports.Text = "Cardiology Reports"
        '
        'ToolStripMenuItem8
        '
        Me.ToolStripMenuItem8.Name = "ToolStripMenuItem8"
        Me.ToolStripMenuItem8.Size = New System.Drawing.Size(200, 6)
        '
        'mnuFileNewCardiologyIPDCardiologyRequests
        '
        Me.mnuFileNewCardiologyIPDCardiologyRequests.Image = CType(resources.GetObject("mnuFileNewCardiologyIPDCardiologyRequests.Image"), System.Drawing.Image)
        Me.mnuFileNewCardiologyIPDCardiologyRequests.Name = "mnuFileNewCardiologyIPDCardiologyRequests"
        Me.mnuFileNewCardiologyIPDCardiologyRequests.Size = New System.Drawing.Size(203, 22)
        Me.mnuFileNewCardiologyIPDCardiologyRequests.Tag = "PathologyRequests"
        Me.mnuFileNewCardiologyIPDCardiologyRequests.Text = "IPD Cardiology Requests"
        '
        'mnuFileNewPathology
        '
        Me.mnuFileNewPathology.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewPathologyPathologyRequests, Me.mnuFileNewPathologyPathologyReports, Me.ToolStripSeparator23, Me.mnuFileNewPathologyIPDPathologyRequests})
        Me.mnuFileNewPathology.Image = CType(resources.GetObject("mnuFileNewPathology.Image"), System.Drawing.Image)
        Me.mnuFileNewPathology.Name = "mnuFileNewPathology"
        Me.mnuFileNewPathology.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewPathology.Tag = ""
        Me.mnuFileNewPathology.Text = "Pathology"
        '
        'mnuFileNewPathologyPathologyRequests
        '
        Me.mnuFileNewPathologyPathologyRequests.Image = CType(resources.GetObject("mnuFileNewPathologyPathologyRequests.Image"), System.Drawing.Image)
        Me.mnuFileNewPathologyPathologyRequests.Name = "mnuFileNewPathologyPathologyRequests"
        Me.mnuFileNewPathologyPathologyRequests.Size = New System.Drawing.Size(199, 22)
        Me.mnuFileNewPathologyPathologyRequests.Tag = "PathologyRequests"
        Me.mnuFileNewPathologyPathologyRequests.Text = "Pathology Requests"
        '
        'mnuFileNewPathologyPathologyReports
        '
        Me.mnuFileNewPathologyPathologyReports.Image = CType(resources.GetObject("mnuFileNewPathologyPathologyReports.Image"), System.Drawing.Image)
        Me.mnuFileNewPathologyPathologyReports.Name = "mnuFileNewPathologyPathologyReports"
        Me.mnuFileNewPathologyPathologyReports.Size = New System.Drawing.Size(199, 22)
        Me.mnuFileNewPathologyPathologyReports.Tag = "PathologyReports"
        Me.mnuFileNewPathologyPathologyReports.Text = "Pathology Reports"
        '
        'ToolStripSeparator23
        '
        Me.ToolStripSeparator23.Name = "ToolStripSeparator23"
        Me.ToolStripSeparator23.Size = New System.Drawing.Size(196, 6)
        '
        'mnuFileNewPathologyIPDPathologyRequests
        '
        Me.mnuFileNewPathologyIPDPathologyRequests.Image = CType(resources.GetObject("mnuFileNewPathologyIPDPathologyRequests.Image"), System.Drawing.Image)
        Me.mnuFileNewPathologyIPDPathologyRequests.Name = "mnuFileNewPathologyIPDPathologyRequests"
        Me.mnuFileNewPathologyIPDPathologyRequests.Size = New System.Drawing.Size(199, 22)
        Me.mnuFileNewPathologyIPDPathologyRequests.Tag = "PathologyRequests"
        Me.mnuFileNewPathologyIPDPathologyRequests.Text = "IPD Pathology Requests"
        '
        'mnuFileNewPharmacy
        '
        Me.mnuFileNewPharmacy.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewPharmacyDispense, Me.mnuFileNewPharmacyIPDDispense, Me.mnuFileNewPharmacyDrugInventory, Me.mnuFileNewPharmacyIssueConsumables, Me.mnuFileNewPharmacyIssueIPDConsumables})
        Me.mnuFileNewPharmacy.Image = CType(resources.GetObject("mnuFileNewPharmacy.Image"), System.Drawing.Image)
        Me.mnuFileNewPharmacy.Name = "mnuFileNewPharmacy"
        Me.mnuFileNewPharmacy.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewPharmacy.Tag = ""
        Me.mnuFileNewPharmacy.Text = "Pharmacy"
        '
        'mnuFileNewPharmacyDispense
        '
        Me.mnuFileNewPharmacyDispense.Image = CType(resources.GetObject("mnuFileNewPharmacyDispense.Image"), System.Drawing.Image)
        Me.mnuFileNewPharmacyDispense.Name = "mnuFileNewPharmacyDispense"
        Me.mnuFileNewPharmacyDispense.Size = New System.Drawing.Size(196, 22)
        Me.mnuFileNewPharmacyDispense.Tag = "Pharmacy"
        Me.mnuFileNewPharmacyDispense.Text = "Dispense"
        '
        'mnuFileNewPharmacyIPDDispense
        '
        Me.mnuFileNewPharmacyIPDDispense.Image = CType(resources.GetObject("mnuFileNewPharmacyIPDDispense.Image"), System.Drawing.Image)
        Me.mnuFileNewPharmacyIPDDispense.Name = "mnuFileNewPharmacyIPDDispense"
        Me.mnuFileNewPharmacyIPDDispense.Size = New System.Drawing.Size(196, 22)
        Me.mnuFileNewPharmacyIPDDispense.Tag = "IPDPharmacy"
        Me.mnuFileNewPharmacyIPDDispense.Text = "IPD Dispense"
        '
        'mnuFileNewPharmacyDrugInventory
        '
        Me.mnuFileNewPharmacyDrugInventory.Image = CType(resources.GetObject("mnuFileNewPharmacyDrugInventory.Image"), System.Drawing.Image)
        Me.mnuFileNewPharmacyDrugInventory.Name = "mnuFileNewPharmacyDrugInventory"
        Me.mnuFileNewPharmacyDrugInventory.Size = New System.Drawing.Size(196, 22)
        Me.mnuFileNewPharmacyDrugInventory.Tag = "DrugInventory"
        Me.mnuFileNewPharmacyDrugInventory.Text = "Drug Inventory"
        '
        'mnuFileNewPharmacyIssueConsumables
        '
        Me.mnuFileNewPharmacyIssueConsumables.Image = CType(resources.GetObject("mnuFileNewPharmacyIssueConsumables.Image"), System.Drawing.Image)
        Me.mnuFileNewPharmacyIssueConsumables.Name = "mnuFileNewPharmacyIssueConsumables"
        Me.mnuFileNewPharmacyIssueConsumables.Size = New System.Drawing.Size(196, 22)
        Me.mnuFileNewPharmacyIssueConsumables.Tag = "IssueConsumables"
        Me.mnuFileNewPharmacyIssueConsumables.Text = "Issue Consumables"
        '
        'mnuFileNewPharmacyIssueIPDConsumables
        '
        Me.mnuFileNewPharmacyIssueIPDConsumables.Image = CType(resources.GetObject("mnuFileNewPharmacyIssueIPDConsumables.Image"), System.Drawing.Image)
        Me.mnuFileNewPharmacyIssueIPDConsumables.Name = "mnuFileNewPharmacyIssueIPDConsumables"
        Me.mnuFileNewPharmacyIssueIPDConsumables.Size = New System.Drawing.Size(196, 22)
        Me.mnuFileNewPharmacyIssueIPDConsumables.Tag = "IssueIPDConsumables"
        Me.mnuFileNewPharmacyIssueIPDConsumables.Text = "Issue IPD Consumables"
        '
        'mnuFileNewTheatre
        '
        Me.mnuFileNewTheatre.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewTheatreTheatreOperations, Me.mnuFileNewTheatreIPDTheatreOperations})
        Me.mnuFileNewTheatre.Image = CType(resources.GetObject("mnuFileNewTheatre.Image"), System.Drawing.Image)
        Me.mnuFileNewTheatre.Name = "mnuFileNewTheatre"
        Me.mnuFileNewTheatre.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewTheatre.Tag = ""
        Me.mnuFileNewTheatre.Text = "Theatre"
        '
        'mnuFileNewTheatreTheatreOperations
        '
        Me.mnuFileNewTheatreTheatreOperations.Image = CType(resources.GetObject("mnuFileNewTheatreTheatreOperations.Image"), System.Drawing.Image)
        Me.mnuFileNewTheatreTheatreOperations.Name = "mnuFileNewTheatreTheatreOperations"
        Me.mnuFileNewTheatreTheatreOperations.Size = New System.Drawing.Size(195, 22)
        Me.mnuFileNewTheatreTheatreOperations.Tag = "TheatreOperations"
        Me.mnuFileNewTheatreTheatreOperations.Text = "Theatre Operations"
        '
        'mnuFileNewTheatreIPDTheatreOperations
        '
        Me.mnuFileNewTheatreIPDTheatreOperations.Image = CType(resources.GetObject("mnuFileNewTheatreIPDTheatreOperations.Image"), System.Drawing.Image)
        Me.mnuFileNewTheatreIPDTheatreOperations.Name = "mnuFileNewTheatreIPDTheatreOperations"
        Me.mnuFileNewTheatreIPDTheatreOperations.Size = New System.Drawing.Size(195, 22)
        Me.mnuFileNewTheatreIPDTheatreOperations.Tag = "IPDTheatreOperations"
        Me.mnuFileNewTheatreIPDTheatreOperations.Text = "IPD Theatre Operations"
        '
        'mnuFileNewDental
        '
        Me.mnuFileNewDental.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewDentalDentalReports, Me.mnuFileNewDentalIPDDentalReports})
        Me.mnuFileNewDental.Image = CType(resources.GetObject("mnuFileNewDental.Image"), System.Drawing.Image)
        Me.mnuFileNewDental.Name = "mnuFileNewDental"
        Me.mnuFileNewDental.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewDental.Tag = ""
        Me.mnuFileNewDental.Text = "Dental"
        '
        'mnuFileNewDentalDentalReports
        '
        Me.mnuFileNewDentalDentalReports.AccessibleName = ""
        Me.mnuFileNewDentalDentalReports.Image = CType(resources.GetObject("mnuFileNewDentalDentalReports.Image"), System.Drawing.Image)
        Me.mnuFileNewDentalDentalReports.Name = "mnuFileNewDentalDentalReports"
        Me.mnuFileNewDentalDentalReports.Size = New System.Drawing.Size(172, 22)
        Me.mnuFileNewDentalDentalReports.Tag = "DentalReports"
        Me.mnuFileNewDentalDentalReports.Text = "Dental Reports"
        '
        'mnuFileNewDentalIPDDentalReports
        '
        Me.mnuFileNewDentalIPDDentalReports.Image = CType(resources.GetObject("mnuFileNewDentalIPDDentalReports.Image"), System.Drawing.Image)
        Me.mnuFileNewDentalIPDDentalReports.Name = "mnuFileNewDentalIPDDentalReports"
        Me.mnuFileNewDentalIPDDentalReports.Size = New System.Drawing.Size(172, 22)
        Me.mnuFileNewDentalIPDDentalReports.Tag = "IPDDentalReports"
        Me.mnuFileNewDentalIPDDentalReports.Text = "IPD Dental Reports"
        '
        'mnuFileNewAppointments
        '
        Me.mnuFileNewAppointments.Image = CType(resources.GetObject("mnuFileNewAppointments.Image"), System.Drawing.Image)
        Me.mnuFileNewAppointments.Name = "mnuFileNewAppointments"
        Me.mnuFileNewAppointments.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewAppointments.Tag = "Appointments"
        Me.mnuFileNewAppointments.Text = "Appointments"
        '
        'mnuFileNewInPatients
        '
        Me.mnuFileNewInPatients.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewInPatientsAdmissions, Me.mnuFileNewInPatientsExtraBills, Me.mnuFileNewInPatientsIPDDoctor, Me.mnuFileNewInPatientsIPDConsumables, Me.mnuFileNewInPatientsReturnedExtraBillItems, Me.mnuFileNewInPatientsDischarges})
        Me.mnuFileNewInPatients.Image = CType(resources.GetObject("mnuFileNewInPatients.Image"), System.Drawing.Image)
        Me.mnuFileNewInPatients.Name = "mnuFileNewInPatients"
        Me.mnuFileNewInPatients.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewInPatients.Tag = ""
        Me.mnuFileNewInPatients.Text = "In Patients"
        '
        'mnuFileNewInPatientsAdmissions
        '
        Me.mnuFileNewInPatientsAdmissions.Image = CType(resources.GetObject("mnuFileNewInPatientsAdmissions.Image"), System.Drawing.Image)
        Me.mnuFileNewInPatientsAdmissions.Name = "mnuFileNewInPatientsAdmissions"
        Me.mnuFileNewInPatientsAdmissions.Size = New System.Drawing.Size(167, 22)
        Me.mnuFileNewInPatientsAdmissions.Tag = "Admissions"
        Me.mnuFileNewInPatientsAdmissions.Text = "Admissions"
        '
        'mnuFileNewInPatientsExtraBills
        '
        Me.mnuFileNewInPatientsExtraBills.Image = CType(resources.GetObject("mnuFileNewInPatientsExtraBills.Image"), System.Drawing.Image)
        Me.mnuFileNewInPatientsExtraBills.Name = "mnuFileNewInPatientsExtraBills"
        Me.mnuFileNewInPatientsExtraBills.Size = New System.Drawing.Size(167, 22)
        Me.mnuFileNewInPatientsExtraBills.Tag = "ExtraBills"
        Me.mnuFileNewInPatientsExtraBills.Text = "Billing Form"
        '
        'mnuFileNewInPatientsIPDDoctor
        '
        Me.mnuFileNewInPatientsIPDDoctor.Image = CType(resources.GetObject("mnuFileNewInPatientsIPDDoctor.Image"), System.Drawing.Image)
        Me.mnuFileNewInPatientsIPDDoctor.Name = "mnuFileNewInPatientsIPDDoctor"
        Me.mnuFileNewInPatientsIPDDoctor.Size = New System.Drawing.Size(167, 22)
        Me.mnuFileNewInPatientsIPDDoctor.Tag = "IPDDoctor"
        Me.mnuFileNewInPatientsIPDDoctor.Text = "IPD Doctor"
        '
        'mnuFileNewInPatientsIPDConsumables
        '
        Me.mnuFileNewInPatientsIPDConsumables.Image = CType(resources.GetObject("mnuFileNewInPatientsIPDConsumables.Image"), System.Drawing.Image)
        Me.mnuFileNewInPatientsIPDConsumables.Name = "mnuFileNewInPatientsIPDConsumables"
        Me.mnuFileNewInPatientsIPDConsumables.Size = New System.Drawing.Size(167, 22)
        Me.mnuFileNewInPatientsIPDConsumables.Tag = "IPDConsumables"
        Me.mnuFileNewInPatientsIPDConsumables.Text = "IPD Consumables"
        '
        'mnuFileNewInPatientsReturnedExtraBillItems
        '
        Me.mnuFileNewInPatientsReturnedExtraBillItems.Image = CType(resources.GetObject("mnuFileNewInPatientsReturnedExtraBillItems.Image"), System.Drawing.Image)
        Me.mnuFileNewInPatientsReturnedExtraBillItems.Name = "mnuFileNewInPatientsReturnedExtraBillItems"
        Me.mnuFileNewInPatientsReturnedExtraBillItems.Size = New System.Drawing.Size(167, 22)
        Me.mnuFileNewInPatientsReturnedExtraBillItems.Tag = "ReturnedExtraBillItems"
        Me.mnuFileNewInPatientsReturnedExtraBillItems.Text = "Bill Form Returns"
        '
        'mnuFileNewInPatientsDischarges
        '
        Me.mnuFileNewInPatientsDischarges.Image = CType(resources.GetObject("mnuFileNewInPatientsDischarges.Image"), System.Drawing.Image)
        Me.mnuFileNewInPatientsDischarges.Name = "mnuFileNewInPatientsDischarges"
        Me.mnuFileNewInPatientsDischarges.Size = New System.Drawing.Size(167, 22)
        Me.mnuFileNewInPatientsDischarges.Tag = "Discharges"
        Me.mnuFileNewInPatientsDischarges.Text = "Discharges"
        '
        'mnuFileNewManageART
        '
        Me.mnuFileNewManageART.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNewManageARTARTRegimen, Me.mnuFileNewManageARTARTStopped, Me.mnuFileNewManageARTExaminations})
        Me.mnuFileNewManageART.Image = CType(resources.GetObject("mnuFileNewManageART.Image"), System.Drawing.Image)
        Me.mnuFileNewManageART.Name = "mnuFileNewManageART"
        Me.mnuFileNewManageART.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewManageART.Tag = ""
        Me.mnuFileNewManageART.Text = "Manage ART"
        '
        'mnuFileNewManageARTARTRegimen
        '
        Me.mnuFileNewManageARTARTRegimen.Image = CType(resources.GetObject("mnuFileNewManageARTARTRegimen.Image"), System.Drawing.Image)
        Me.mnuFileNewManageARTARTRegimen.Name = "mnuFileNewManageARTARTRegimen"
        Me.mnuFileNewManageARTARTRegimen.Size = New System.Drawing.Size(144, 22)
        Me.mnuFileNewManageARTARTRegimen.Tag = "ARTRegimen"
        Me.mnuFileNewManageARTARTRegimen.Text = "ART Regimen"
        '
        'mnuFileNewManageARTARTStopped
        '
        Me.mnuFileNewManageARTARTStopped.Image = CType(resources.GetObject("mnuFileNewManageARTARTStopped.Image"), System.Drawing.Image)
        Me.mnuFileNewManageARTARTStopped.Name = "mnuFileNewManageARTARTStopped"
        Me.mnuFileNewManageARTARTStopped.Size = New System.Drawing.Size(144, 22)
        Me.mnuFileNewManageARTARTStopped.Tag = "ARTStopped"
        Me.mnuFileNewManageARTARTStopped.Text = "ART Stopped"
        '
        'mnuFileNewManageARTExaminations
        '
        Me.mnuFileNewManageARTExaminations.Name = "mnuFileNewManageARTExaminations"
        Me.mnuFileNewManageARTExaminations.Size = New System.Drawing.Size(144, 22)
        Me.mnuFileNewManageARTExaminations.Tag = "Examinations"
        Me.mnuFileNewManageARTExaminations.Text = "Follow -up"
        '
        'mnuFileNewDeaths
        '
        Me.mnuFileNewDeaths.Image = CType(resources.GetObject("mnuFileNewDeaths.Image"), System.Drawing.Image)
        Me.mnuFileNewDeaths.Name = "mnuFileNewDeaths"
        Me.mnuFileNewDeaths.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileNewDeaths.Tag = "Deaths"
        Me.mnuFileNewDeaths.Text = "Deaths"
        '
        'mnuFileEdit
        '
        Me.mnuFileEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileEditPatients, Me.mnuFileEditVisits, Me.mnuFileEditExtras, Me.mnuFileEditTriage, Me.mnuFileEditInvoices, Me.mnuFileEditLaboratory, Me.mnuFileEditRadiology, Me.mnuFileEditCardiology, Me.mnuFileEditPathology, Me.mnuFileEditPharmacy, Me.mnuFileEditTheatre, Me.mnuFileEditDental, Me.mnuFileEditAppointments, Me.mnuFileEditInPatients, Me.mnuFileEditManageART, Me.mnuFileEditDeaths})
        Me.mnuFileEdit.Image = CType(resources.GetObject("mnuFileEdit.Image"), System.Drawing.Image)
        Me.mnuFileEdit.Name = "mnuFileEdit"
        Me.mnuFileEdit.Size = New System.Drawing.Size(175, 22)
        Me.mnuFileEdit.Text = "Edit"
        '
        'mnuFileEditPatients
        '
        Me.mnuFileEditPatients.Image = CType(resources.GetObject("mnuFileEditPatients.Image"), System.Drawing.Image)
        Me.mnuFileEditPatients.Name = "mnuFileEditPatients"
        Me.mnuFileEditPatients.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditPatients.Tag = "Patients"
        Me.mnuFileEditPatients.Text = "Patients"
        '
        'mnuFileEditVisits
        '
        Me.mnuFileEditVisits.Image = CType(resources.GetObject("mnuFileEditVisits.Image"), System.Drawing.Image)
        Me.mnuFileEditVisits.Name = "mnuFileEditVisits"
        Me.mnuFileEditVisits.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditVisits.Tag = "Visits"
        Me.mnuFileEditVisits.Text = "Visits"
        '
        'mnuFileEditExtras
        '
        Me.mnuFileEditExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileEditExtrasSelfRequests, Me.mnuFileEditExtrasClaims, Me.mnuFileEditExtrasQuotations, Me.mnuFileEditExtrasPurchaseOrders, Me.mnuFileEditExtrasInventoryOrders, Me.mnuFileEditExtrasInventoryTransfers, Me.mnuFileEditExtrasOutwardFiles, Me.mnuFileEditExtrasInwardFiles, Me.mnuFileEditExtrasSmartCardAuthorisations, Me.mnuFileEditExtrasResearchRoutingForm, Me.mnuFileEditExtrasResearchEnrollmentInformation, Me.mnuFileEditExtrasExternalReferralForm})
        Me.mnuFileEditExtras.Image = CType(resources.GetObject("mnuFileEditExtras.Image"), System.Drawing.Image)
        Me.mnuFileEditExtras.Name = "mnuFileEditExtras"
        Me.mnuFileEditExtras.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditExtras.Tag = ""
        Me.mnuFileEditExtras.Text = "Extras"
        '
        'mnuFileEditExtrasSelfRequests
        '
        Me.mnuFileEditExtrasSelfRequests.Image = CType(resources.GetObject("mnuFileEditExtrasSelfRequests.Image"), System.Drawing.Image)
        Me.mnuFileEditExtrasSelfRequests.Name = "mnuFileEditExtrasSelfRequests"
        Me.mnuFileEditExtrasSelfRequests.Size = New System.Drawing.Size(248, 22)
        Me.mnuFileEditExtrasSelfRequests.Tag = "SelfRequests"
        Me.mnuFileEditExtrasSelfRequests.Text = "Self Requests"
        '
        'mnuFileEditExtrasClaims
        '
        Me.mnuFileEditExtrasClaims.Image = CType(resources.GetObject("mnuFileEditExtrasClaims.Image"), System.Drawing.Image)
        Me.mnuFileEditExtrasClaims.Name = "mnuFileEditExtrasClaims"
        Me.mnuFileEditExtrasClaims.Size = New System.Drawing.Size(248, 22)
        Me.mnuFileEditExtrasClaims.Tag = "Claims"
        Me.mnuFileEditExtrasClaims.Text = "Claims"
        '
        'mnuFileEditExtrasQuotations
        '
        Me.mnuFileEditExtrasQuotations.Image = CType(resources.GetObject("mnuFileEditExtrasQuotations.Image"), System.Drawing.Image)
        Me.mnuFileEditExtrasQuotations.Name = "mnuFileEditExtrasQuotations"
        Me.mnuFileEditExtrasQuotations.Size = New System.Drawing.Size(248, 22)
        Me.mnuFileEditExtrasQuotations.Tag = "Quotations"
        Me.mnuFileEditExtrasQuotations.Text = "Quotations"
        '
        'mnuFileEditExtrasPurchaseOrders
        '
        Me.mnuFileEditExtrasPurchaseOrders.Image = CType(resources.GetObject("mnuFileEditExtrasPurchaseOrders.Image"), System.Drawing.Image)
        Me.mnuFileEditExtrasPurchaseOrders.Name = "mnuFileEditExtrasPurchaseOrders"
        Me.mnuFileEditExtrasPurchaseOrders.Size = New System.Drawing.Size(248, 22)
        Me.mnuFileEditExtrasPurchaseOrders.Tag = "PurchaseOrders"
        Me.mnuFileEditExtrasPurchaseOrders.Text = "Purchase Orders"
        '
        'mnuFileEditExtrasInventoryOrders
        '
        Me.mnuFileEditExtrasInventoryOrders.Image = CType(resources.GetObject("mnuFileEditExtrasInventoryOrders.Image"), System.Drawing.Image)
        Me.mnuFileEditExtrasInventoryOrders.Name = "mnuFileEditExtrasInventoryOrders"
        Me.mnuFileEditExtrasInventoryOrders.Size = New System.Drawing.Size(248, 22)
        Me.mnuFileEditExtrasInventoryOrders.Tag = "InventoryOrders"
        Me.mnuFileEditExtrasInventoryOrders.Text = "Inventory Orders"
        '
        'mnuFileEditExtrasInventoryTransfers
        '
        Me.mnuFileEditExtrasInventoryTransfers.Image = CType(resources.GetObject("mnuFileEditExtrasInventoryTransfers.Image"), System.Drawing.Image)
        Me.mnuFileEditExtrasInventoryTransfers.Name = "mnuFileEditExtrasInventoryTransfers"
        Me.mnuFileEditExtrasInventoryTransfers.Size = New System.Drawing.Size(248, 22)
        Me.mnuFileEditExtrasInventoryTransfers.Tag = "InventoryTransfers"
        Me.mnuFileEditExtrasInventoryTransfers.Text = "Inventory Transfers"
        '
        'mnuFileEditExtrasOutwardFiles
        '
        Me.mnuFileEditExtrasOutwardFiles.Image = CType(resources.GetObject("mnuFileEditExtrasOutwardFiles.Image"), System.Drawing.Image)
        Me.mnuFileEditExtrasOutwardFiles.Name = "mnuFileEditExtrasOutwardFiles"
        Me.mnuFileEditExtrasOutwardFiles.Size = New System.Drawing.Size(248, 22)
        Me.mnuFileEditExtrasOutwardFiles.Tag = "OutwardFiles"
        Me.mnuFileEditExtrasOutwardFiles.Text = "Outward Files"
        '
        'mnuFileEditExtrasInwardFiles
        '
        Me.mnuFileEditExtrasInwardFiles.Image = CType(resources.GetObject("mnuFileEditExtrasInwardFiles.Image"), System.Drawing.Image)
        Me.mnuFileEditExtrasInwardFiles.Name = "mnuFileEditExtrasInwardFiles"
        Me.mnuFileEditExtrasInwardFiles.Size = New System.Drawing.Size(248, 22)
        Me.mnuFileEditExtrasInwardFiles.Tag = "InwardFiles"
        Me.mnuFileEditExtrasInwardFiles.Text = "Inward Files"
        '
        'mnuFileEditExtrasSmartCardAuthorisations
        '
        Me.mnuFileEditExtrasSmartCardAuthorisations.Image = CType(resources.GetObject("mnuFileEditExtrasSmartCardAuthorisations.Image"), System.Drawing.Image)
        Me.mnuFileEditExtrasSmartCardAuthorisations.Name = "mnuFileEditExtrasSmartCardAuthorisations"
        Me.mnuFileEditExtrasSmartCardAuthorisations.Size = New System.Drawing.Size(248, 22)
        Me.mnuFileEditExtrasSmartCardAuthorisations.Tag = "SmartCardAuthorisations"
        Me.mnuFileEditExtrasSmartCardAuthorisations.Text = "Smart Card Authorisations"
        '
        'mnuFileEditExtrasResearchRoutingForm
        '
        Me.mnuFileEditExtrasResearchRoutingForm.Image = CType(resources.GetObject("mnuFileEditExtrasResearchRoutingForm.Image"), System.Drawing.Image)
        Me.mnuFileEditExtrasResearchRoutingForm.Name = "mnuFileEditExtrasResearchRoutingForm"
        Me.mnuFileEditExtrasResearchRoutingForm.Size = New System.Drawing.Size(248, 22)
        Me.mnuFileEditExtrasResearchRoutingForm.Tag = "ResearchRoutingForm"
        Me.mnuFileEditExtrasResearchRoutingForm.Text = "Research Routing Form"
        '
        'mnuFileEditExtrasResearchEnrollmentInformation
        '
        Me.mnuFileEditExtrasResearchEnrollmentInformation.Image = CType(resources.GetObject("mnuFileEditExtrasResearchEnrollmentInformation.Image"), System.Drawing.Image)
        Me.mnuFileEditExtrasResearchEnrollmentInformation.Name = "mnuFileEditExtrasResearchEnrollmentInformation"
        Me.mnuFileEditExtrasResearchEnrollmentInformation.Size = New System.Drawing.Size(248, 22)
        Me.mnuFileEditExtrasResearchEnrollmentInformation.Tag = "ResearchEnrollmentInformation"
        Me.mnuFileEditExtrasResearchEnrollmentInformation.Text = "Research Enrollment Information"
        '
        'mnuFileEditExtrasExternalReferralForm
        '
        Me.mnuFileEditExtrasExternalReferralForm.Image = CType(resources.GetObject("mnuFileEditExtrasExternalReferralForm.Image"), System.Drawing.Image)
        Me.mnuFileEditExtrasExternalReferralForm.Name = "mnuFileEditExtrasExternalReferralForm"
        Me.mnuFileEditExtrasExternalReferralForm.Size = New System.Drawing.Size(248, 22)
        Me.mnuFileEditExtrasExternalReferralForm.Tag = "ExternalReferrals"
        Me.mnuFileEditExtrasExternalReferralForm.Text = "External Referral Form "
        '
        'mnuFileEditTriage
        '
        Me.mnuFileEditTriage.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileEditTriageTriage, Me.mnuFileEditTriageVisionAssessment, Me.mnuFileEditTriageIPDVisionAssessment})
        Me.mnuFileEditTriage.Image = CType(resources.GetObject("mnuFileEditTriage.Image"), System.Drawing.Image)
        Me.mnuFileEditTriage.Name = "mnuFileEditTriage"
        Me.mnuFileEditTriage.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditTriage.Tag = ""
        Me.mnuFileEditTriage.Text = "Triage"
        '
        'mnuFileEditTriageTriage
        '
        Me.mnuFileEditTriageTriage.Image = CType(resources.GetObject("mnuFileEditTriageTriage.Image"), System.Drawing.Image)
        Me.mnuFileEditTriageTriage.Name = "mnuFileEditTriageTriage"
        Me.mnuFileEditTriageTriage.Size = New System.Drawing.Size(192, 22)
        Me.mnuFileEditTriageTriage.Tag = "Triage"
        Me.mnuFileEditTriageTriage.Text = "Triage"
        '
        'mnuFileEditTriageVisionAssessment
        '
        Me.mnuFileEditTriageVisionAssessment.Image = CType(resources.GetObject("mnuFileEditTriageVisionAssessment.Image"), System.Drawing.Image)
        Me.mnuFileEditTriageVisionAssessment.Name = "mnuFileEditTriageVisionAssessment"
        Me.mnuFileEditTriageVisionAssessment.Size = New System.Drawing.Size(192, 22)
        Me.mnuFileEditTriageVisionAssessment.Tag = "VisionAssessment"
        Me.mnuFileEditTriageVisionAssessment.Text = "Vision Assessment"
        '
        'mnuFileEditTriageIPDVisionAssessment
        '
        Me.mnuFileEditTriageIPDVisionAssessment.Name = "mnuFileEditTriageIPDVisionAssessment"
        Me.mnuFileEditTriageIPDVisionAssessment.Size = New System.Drawing.Size(192, 22)
        Me.mnuFileEditTriageIPDVisionAssessment.Tag = "IPDVisionAssessment"
        Me.mnuFileEditTriageIPDVisionAssessment.Text = "IPD Vision Assessment"
        '
        'mnuFileEditInvoices
        '
        Me.mnuFileEditInvoices.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileEditInvoicesInvoices, Me.mnuFileEditInvoicesIPDInvoices})
        Me.mnuFileEditInvoices.Image = CType(resources.GetObject("mnuFileEditInvoices.Image"), System.Drawing.Image)
        Me.mnuFileEditInvoices.Name = "mnuFileEditInvoices"
        Me.mnuFileEditInvoices.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditInvoices.Tag = ""
        Me.mnuFileEditInvoices.Text = "Invoices"
        '
        'mnuFileEditInvoicesInvoices
        '
        Me.mnuFileEditInvoicesInvoices.Image = CType(resources.GetObject("mnuFileEditInvoicesInvoices.Image"), System.Drawing.Image)
        Me.mnuFileEditInvoicesInvoices.Name = "mnuFileEditInvoicesInvoices"
        Me.mnuFileEditInvoicesInvoices.Size = New System.Drawing.Size(138, 22)
        Me.mnuFileEditInvoicesInvoices.Tag = "Invoices"
        Me.mnuFileEditInvoicesInvoices.Text = "Invoices"
        '
        'mnuFileEditInvoicesIPDInvoices
        '
        Me.mnuFileEditInvoicesIPDInvoices.Image = CType(resources.GetObject("mnuFileEditInvoicesIPDInvoices.Image"), System.Drawing.Image)
        Me.mnuFileEditInvoicesIPDInvoices.Name = "mnuFileEditInvoicesIPDInvoices"
        Me.mnuFileEditInvoicesIPDInvoices.Size = New System.Drawing.Size(138, 22)
        Me.mnuFileEditInvoicesIPDInvoices.Tag = "Invoices"
        Me.mnuFileEditInvoicesIPDInvoices.Text = "IPD Invoices"
        '
        'mnuFileEditLaboratory
        '
        Me.mnuFileEditLaboratory.AccessibleName = ""
        Me.mnuFileEditLaboratory.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileEditLaboratoryLabRequests, Me.mnuFileEditLaboratoryIPDLabRequests, Me.mnuFileEditLaboratoryLabResults})
        Me.mnuFileEditLaboratory.Image = CType(resources.GetObject("mnuFileEditLaboratory.Image"), System.Drawing.Image)
        Me.mnuFileEditLaboratory.Name = "mnuFileEditLaboratory"
        Me.mnuFileEditLaboratory.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditLaboratory.Tag = ""
        Me.mnuFileEditLaboratory.Text = "Laboratory"
        '
        'mnuFileEditLaboratoryLabRequests
        '
        Me.mnuFileEditLaboratoryLabRequests.Image = CType(resources.GetObject("mnuFileEditLaboratoryLabRequests.Image"), System.Drawing.Image)
        Me.mnuFileEditLaboratoryLabRequests.Name = "mnuFileEditLaboratoryLabRequests"
        Me.mnuFileEditLaboratoryLabRequests.Size = New System.Drawing.Size(164, 22)
        Me.mnuFileEditLaboratoryLabRequests.Tag = "LabRequests"
        Me.mnuFileEditLaboratoryLabRequests.Text = "Lab Requests"
        '
        'mnuFileEditLaboratoryIPDLabRequests
        '
        Me.mnuFileEditLaboratoryIPDLabRequests.Image = CType(resources.GetObject("mnuFileEditLaboratoryIPDLabRequests.Image"), System.Drawing.Image)
        Me.mnuFileEditLaboratoryIPDLabRequests.Name = "mnuFileEditLaboratoryIPDLabRequests"
        Me.mnuFileEditLaboratoryIPDLabRequests.Size = New System.Drawing.Size(164, 22)
        Me.mnuFileEditLaboratoryIPDLabRequests.Tag = "IPDLabRequests"
        Me.mnuFileEditLaboratoryIPDLabRequests.Text = "IPD Lab Requests"
        '
        'mnuFileEditLaboratoryLabResults
        '
        Me.mnuFileEditLaboratoryLabResults.Image = CType(resources.GetObject("mnuFileEditLaboratoryLabResults.Image"), System.Drawing.Image)
        Me.mnuFileEditLaboratoryLabResults.Name = "mnuFileEditLaboratoryLabResults"
        Me.mnuFileEditLaboratoryLabResults.Size = New System.Drawing.Size(164, 22)
        Me.mnuFileEditLaboratoryLabResults.Tag = "LabResults"
        Me.mnuFileEditLaboratoryLabResults.Text = "Lab Results"
        '
        'mnuFileEditRadiology
        '
        Me.mnuFileEditRadiology.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileEditRadiologyRadiologyReports, Me.mnuFileEditRadiologyIPDRadiologyReports})
        Me.mnuFileEditRadiology.Image = CType(resources.GetObject("mnuFileEditRadiology.Image"), System.Drawing.Image)
        Me.mnuFileEditRadiology.Name = "mnuFileEditRadiology"
        Me.mnuFileEditRadiology.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditRadiology.Tag = ""
        Me.mnuFileEditRadiology.Text = "Radiology"
        '
        'mnuFileEditRadiologyRadiologyReports
        '
        Me.mnuFileEditRadiologyRadiologyReports.Image = CType(resources.GetObject("mnuFileEditRadiologyRadiologyReports.Image"), System.Drawing.Image)
        Me.mnuFileEditRadiologyRadiologyReports.Name = "mnuFileEditRadiologyRadiologyReports"
        Me.mnuFileEditRadiologyRadiologyReports.Size = New System.Drawing.Size(191, 22)
        Me.mnuFileEditRadiologyRadiologyReports.Tag = "RadiologyReports"
        Me.mnuFileEditRadiologyRadiologyReports.Text = "Radiology Reports"
        '
        'mnuFileEditRadiologyIPDRadiologyReports
        '
        Me.mnuFileEditRadiologyIPDRadiologyReports.Image = CType(resources.GetObject("mnuFileEditRadiologyIPDRadiologyReports.Image"), System.Drawing.Image)
        Me.mnuFileEditRadiologyIPDRadiologyReports.Name = "mnuFileEditRadiologyIPDRadiologyReports"
        Me.mnuFileEditRadiologyIPDRadiologyReports.Size = New System.Drawing.Size(191, 22)
        Me.mnuFileEditRadiologyIPDRadiologyReports.Tag = "IPDRadiologyReports"
        Me.mnuFileEditRadiologyIPDRadiologyReports.Text = "IPD Radiology Reports"
        '
        'mnuFileEditCardiology
        '
        Me.mnuFileEditCardiology.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileEditCardiologyCardiologyReports})
        Me.mnuFileEditCardiology.Image = CType(resources.GetObject("mnuFileEditCardiology.Image"), System.Drawing.Image)
        Me.mnuFileEditCardiology.Name = "mnuFileEditCardiology"
        Me.mnuFileEditCardiology.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditCardiology.Tag = ""
        Me.mnuFileEditCardiology.Text = "Cardiology"
        '
        'mnuFileEditCardiologyCardiologyReports
        '
        Me.mnuFileEditCardiologyCardiologyReports.Image = CType(resources.GetObject("mnuFileEditCardiologyCardiologyReports.Image"), System.Drawing.Image)
        Me.mnuFileEditCardiologyCardiologyReports.Name = "mnuFileEditCardiologyCardiologyReports"
        Me.mnuFileEditCardiologyCardiologyReports.Size = New System.Drawing.Size(175, 22)
        Me.mnuFileEditCardiologyCardiologyReports.Tag = "PathologyReports"
        Me.mnuFileEditCardiologyCardiologyReports.Text = "Cardiology Reports"
        '
        'mnuFileEditPathology
        '
        Me.mnuFileEditPathology.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileEditPathologyPathologyReports})
        Me.mnuFileEditPathology.Image = CType(resources.GetObject("mnuFileEditPathology.Image"), System.Drawing.Image)
        Me.mnuFileEditPathology.Name = "mnuFileEditPathology"
        Me.mnuFileEditPathology.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditPathology.Tag = ""
        Me.mnuFileEditPathology.Text = "Pathology"
        '
        'mnuFileEditPathologyPathologyReports
        '
        Me.mnuFileEditPathologyPathologyReports.Image = CType(resources.GetObject("mnuFileEditPathologyPathologyReports.Image"), System.Drawing.Image)
        Me.mnuFileEditPathologyPathologyReports.Name = "mnuFileEditPathologyPathologyReports"
        Me.mnuFileEditPathologyPathologyReports.Size = New System.Drawing.Size(171, 22)
        Me.mnuFileEditPathologyPathologyReports.Tag = "PathologyReports"
        Me.mnuFileEditPathologyPathologyReports.Text = "Pathology Reports"
        '
        'mnuFileEditPharmacy
        '
        Me.mnuFileEditPharmacy.Enabled = False
        Me.mnuFileEditPharmacy.Image = CType(resources.GetObject("mnuFileEditPharmacy.Image"), System.Drawing.Image)
        Me.mnuFileEditPharmacy.Name = "mnuFileEditPharmacy"
        Me.mnuFileEditPharmacy.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditPharmacy.Tag = ""
        Me.mnuFileEditPharmacy.Text = "Pharmacy"
        '
        'mnuFileEditTheatre
        '
        Me.mnuFileEditTheatre.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileEditTheatreTheatreOperations, Me.mnuFileEditTheatreIPDTheatreOperations})
        Me.mnuFileEditTheatre.Image = CType(resources.GetObject("mnuFileEditTheatre.Image"), System.Drawing.Image)
        Me.mnuFileEditTheatre.Name = "mnuFileEditTheatre"
        Me.mnuFileEditTheatre.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditTheatre.Tag = ""
        Me.mnuFileEditTheatre.Text = "Theatre"
        '
        'mnuFileEditTheatreTheatreOperations
        '
        Me.mnuFileEditTheatreTheatreOperations.Image = CType(resources.GetObject("mnuFileEditTheatreTheatreOperations.Image"), System.Drawing.Image)
        Me.mnuFileEditTheatreTheatreOperations.Name = "mnuFileEditTheatreTheatreOperations"
        Me.mnuFileEditTheatreTheatreOperations.Size = New System.Drawing.Size(195, 22)
        Me.mnuFileEditTheatreTheatreOperations.Tag = "TheatreOperations"
        Me.mnuFileEditTheatreTheatreOperations.Text = "Theatre Operations"
        '
        'mnuFileEditTheatreIPDTheatreOperations
        '
        Me.mnuFileEditTheatreIPDTheatreOperations.Image = CType(resources.GetObject("mnuFileEditTheatreIPDTheatreOperations.Image"), System.Drawing.Image)
        Me.mnuFileEditTheatreIPDTheatreOperations.Name = "mnuFileEditTheatreIPDTheatreOperations"
        Me.mnuFileEditTheatreIPDTheatreOperations.Size = New System.Drawing.Size(195, 22)
        Me.mnuFileEditTheatreIPDTheatreOperations.Tag = "IPDTheatreOperations"
        Me.mnuFileEditTheatreIPDTheatreOperations.Text = "IPD Theatre Operations"
        '
        'mnuFileEditDental
        '
        Me.mnuFileEditDental.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileEditDentalDentalReports, Me.mnuFileEditDentalIPDDentalReports})
        Me.mnuFileEditDental.Image = CType(resources.GetObject("mnuFileEditDental.Image"), System.Drawing.Image)
        Me.mnuFileEditDental.Name = "mnuFileEditDental"
        Me.mnuFileEditDental.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditDental.Tag = ""
        Me.mnuFileEditDental.Text = "Dental"
        '
        'mnuFileEditDentalDentalReports
        '
        Me.mnuFileEditDentalDentalReports.Image = CType(resources.GetObject("mnuFileEditDentalDentalReports.Image"), System.Drawing.Image)
        Me.mnuFileEditDentalDentalReports.Name = "mnuFileEditDentalDentalReports"
        Me.mnuFileEditDentalDentalReports.Size = New System.Drawing.Size(172, 22)
        Me.mnuFileEditDentalDentalReports.Tag = "DentalReports"
        Me.mnuFileEditDentalDentalReports.Text = "Dental Reports"
        '
        'mnuFileEditDentalIPDDentalReports
        '
        Me.mnuFileEditDentalIPDDentalReports.Image = CType(resources.GetObject("mnuFileEditDentalIPDDentalReports.Image"), System.Drawing.Image)
        Me.mnuFileEditDentalIPDDentalReports.Name = "mnuFileEditDentalIPDDentalReports"
        Me.mnuFileEditDentalIPDDentalReports.Size = New System.Drawing.Size(172, 22)
        Me.mnuFileEditDentalIPDDentalReports.Tag = "IPDDentalReports"
        Me.mnuFileEditDentalIPDDentalReports.Text = "IPD Dental Reports"
        '
        'mnuFileEditAppointments
        '
        Me.mnuFileEditAppointments.Image = CType(resources.GetObject("mnuFileEditAppointments.Image"), System.Drawing.Image)
        Me.mnuFileEditAppointments.Name = "mnuFileEditAppointments"
        Me.mnuFileEditAppointments.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditAppointments.Tag = "Appointments"
        Me.mnuFileEditAppointments.Text = "Appointments"
        '
        'mnuFileEditInPatients
        '
        Me.mnuFileEditInPatients.AccessibleName = ""
        Me.mnuFileEditInPatients.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileEditInPatientsAdmissions, Me.mnuFileEditInPatientsExtraBills, Me.mnuFileEditInPatientsDischarges})
        Me.mnuFileEditInPatients.Image = CType(resources.GetObject("mnuFileEditInPatients.Image"), System.Drawing.Image)
        Me.mnuFileEditInPatients.Name = "mnuFileEditInPatients"
        Me.mnuFileEditInPatients.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditInPatients.Tag = ""
        Me.mnuFileEditInPatients.Text = "In Patients"
        '
        'mnuFileEditInPatientsAdmissions
        '
        Me.mnuFileEditInPatientsAdmissions.Image = CType(resources.GetObject("mnuFileEditInPatientsAdmissions.Image"), System.Drawing.Image)
        Me.mnuFileEditInPatientsAdmissions.Name = "mnuFileEditInPatientsAdmissions"
        Me.mnuFileEditInPatientsAdmissions.Size = New System.Drawing.Size(138, 22)
        Me.mnuFileEditInPatientsAdmissions.Tag = "Admissions"
        Me.mnuFileEditInPatientsAdmissions.Text = "Admissions"
        '
        'mnuFileEditInPatientsExtraBills
        '
        Me.mnuFileEditInPatientsExtraBills.Image = CType(resources.GetObject("mnuFileEditInPatientsExtraBills.Image"), System.Drawing.Image)
        Me.mnuFileEditInPatientsExtraBills.Name = "mnuFileEditInPatientsExtraBills"
        Me.mnuFileEditInPatientsExtraBills.Size = New System.Drawing.Size(138, 22)
        Me.mnuFileEditInPatientsExtraBills.Tag = "ExtraBills"
        Me.mnuFileEditInPatientsExtraBills.Text = "Billing Form"
        '
        'mnuFileEditInPatientsDischarges
        '
        Me.mnuFileEditInPatientsDischarges.Image = CType(resources.GetObject("mnuFileEditInPatientsDischarges.Image"), System.Drawing.Image)
        Me.mnuFileEditInPatientsDischarges.Name = "mnuFileEditInPatientsDischarges"
        Me.mnuFileEditInPatientsDischarges.Size = New System.Drawing.Size(138, 22)
        Me.mnuFileEditInPatientsDischarges.Tag = "Discharges"
        Me.mnuFileEditInPatientsDischarges.Text = "Discharges"
        '
        'mnuFileEditManageART
        '
        Me.mnuFileEditManageART.AccessibleName = ""
        Me.mnuFileEditManageART.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileEditManageARTARTRegimen, Me.mnuFileEditManageARTARTStopped, Me.mnuFileEditManageARTExaminations})
        Me.mnuFileEditManageART.Image = CType(resources.GetObject("mnuFileEditManageART.Image"), System.Drawing.Image)
        Me.mnuFileEditManageART.Name = "mnuFileEditManageART"
        Me.mnuFileEditManageART.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditManageART.Tag = ""
        Me.mnuFileEditManageART.Text = "Manage ART"
        '
        'mnuFileEditManageARTARTRegimen
        '
        Me.mnuFileEditManageARTARTRegimen.Image = CType(resources.GetObject("mnuFileEditManageARTARTRegimen.Image"), System.Drawing.Image)
        Me.mnuFileEditManageARTARTRegimen.Name = "mnuFileEditManageARTARTRegimen"
        Me.mnuFileEditManageARTARTRegimen.Size = New System.Drawing.Size(144, 22)
        Me.mnuFileEditManageARTARTRegimen.Tag = "ARTRegimen"
        Me.mnuFileEditManageARTARTRegimen.Text = "ART Regimen"
        '
        'mnuFileEditManageARTARTStopped
        '
        Me.mnuFileEditManageARTARTStopped.Image = CType(resources.GetObject("mnuFileEditManageARTARTStopped.Image"), System.Drawing.Image)
        Me.mnuFileEditManageARTARTStopped.Name = "mnuFileEditManageARTARTStopped"
        Me.mnuFileEditManageARTARTStopped.Size = New System.Drawing.Size(144, 22)
        Me.mnuFileEditManageARTARTStopped.Tag = "ARTStopped"
        Me.mnuFileEditManageARTARTStopped.Text = "ART Stopped"
        '
        'mnuFileEditManageARTExaminations
        '
        Me.mnuFileEditManageARTExaminations.Name = "mnuFileEditManageARTExaminations"
        Me.mnuFileEditManageARTExaminations.Size = New System.Drawing.Size(144, 22)
        Me.mnuFileEditManageARTExaminations.Tag = "Examinations"
        Me.mnuFileEditManageARTExaminations.Text = "Follow-up"
        '
        'mnuFileEditDeaths
        '
        Me.mnuFileEditDeaths.Image = CType(resources.GetObject("mnuFileEditDeaths.Image"), System.Drawing.Image)
        Me.mnuFileEditDeaths.Name = "mnuFileEditDeaths"
        Me.mnuFileEditDeaths.Size = New System.Drawing.Size(150, 22)
        Me.mnuFileEditDeaths.Tag = "Deaths"
        Me.mnuFileEditDeaths.Text = "Deaths"
        '
        'mnuFileSeparator1
        '
        Me.mnuFileSeparator1.Name = "mnuFileSeparator1"
        Me.mnuFileSeparator1.Size = New System.Drawing.Size(172, 6)
        '
        'mnuFileImport
        '
        Me.mnuFileImport.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileImportLabResults, Me.mnuFileImportLabResultsEXT, Me.mnuFileImportPatients, Me.mnuFileImportVisits})
        Me.mnuFileImport.Image = CType(resources.GetObject("mnuFileImport.Image"), System.Drawing.Image)
        Me.mnuFileImport.Name = "mnuFileImport"
        Me.mnuFileImport.Size = New System.Drawing.Size(175, 22)
        Me.mnuFileImport.Text = "Import"
        '
        'mnuFileImportLabResults
        '
        Me.mnuFileImportLabResults.Image = CType(resources.GetObject("mnuFileImportLabResults.Image"), System.Drawing.Image)
        Me.mnuFileImportLabResults.Name = "mnuFileImportLabResults"
        Me.mnuFileImportLabResults.Size = New System.Drawing.Size(170, 22)
        Me.mnuFileImportLabResults.Tag = "LabResults"
        Me.mnuFileImportLabResults.Text = "Lab Results"
        '
        'mnuFileImportLabResultsEXT
        '
        Me.mnuFileImportLabResultsEXT.Image = CType(resources.GetObject("mnuFileImportLabResultsEXT.Image"), System.Drawing.Image)
        Me.mnuFileImportLabResultsEXT.Name = "mnuFileImportLabResultsEXT"
        Me.mnuFileImportLabResultsEXT.Size = New System.Drawing.Size(170, 22)
        Me.mnuFileImportLabResultsEXT.Tag = "LabResultsEXT"
        Me.mnuFileImportLabResultsEXT.Text = "Lab Results EXTRA"
        '
        'mnuFileImportPatients
        '
        Me.mnuFileImportPatients.Image = CType(resources.GetObject("mnuFileImportPatients.Image"), System.Drawing.Image)
        Me.mnuFileImportPatients.Name = "mnuFileImportPatients"
        Me.mnuFileImportPatients.Size = New System.Drawing.Size(170, 22)
        Me.mnuFileImportPatients.Tag = "Patients"
        Me.mnuFileImportPatients.Text = "Patients"
        '
        'mnuFileImportVisits
        '
        Me.mnuFileImportVisits.Image = CType(resources.GetObject("mnuFileImportVisits.Image"), System.Drawing.Image)
        Me.mnuFileImportVisits.Name = "mnuFileImportVisits"
        Me.mnuFileImportVisits.Size = New System.Drawing.Size(170, 22)
        Me.mnuFileImportVisits.Tag = "Visits"
        Me.mnuFileImportVisits.Text = "Visits"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(172, 6)
        '
        'mnuFileClose
        '
        Me.mnuFileClose.Name = "mnuFileClose"
        Me.mnuFileClose.Size = New System.Drawing.Size(175, 22)
        Me.mnuFileClose.Text = "&Close"
        '
        'mnuFileSeparator2
        '
        Me.mnuFileSeparator2.Name = "mnuFileSeparator2"
        Me.mnuFileSeparator2.Size = New System.Drawing.Size(172, 6)
        '
        'mnuFileSwitchUser
        '
        Me.mnuFileSwitchUser.Image = CType(resources.GetObject("mnuFileSwitchUser.Image"), System.Drawing.Image)
        Me.mnuFileSwitchUser.Name = "mnuFileSwitchUser"
        Me.mnuFileSwitchUser.Size = New System.Drawing.Size(175, 22)
        Me.mnuFileSwitchUser.Tag = "Login"
        Me.mnuFileSwitchUser.Text = "S&witch User - None"
        '
        'mnuFileSeparator3
        '
        Me.mnuFileSeparator3.Name = "mnuFileSeparator3"
        Me.mnuFileSeparator3.Size = New System.Drawing.Size(172, 6)
        '
        'mnuFileExit
        '
        Me.mnuFileExit.Image = CType(resources.GetObject("mnuFileExit.Image"), System.Drawing.Image)
        Me.mnuFileExit.Name = "mnuFileExit"
        Me.mnuFileExit.Size = New System.Drawing.Size(175, 22)
        Me.mnuFileExit.Text = "E&xit"
        '
        'mnuEdit
        '
        Me.mnuEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuEditCut, Me.mnuEditCopy, Me.mnuEditPaste, Me.mnuEditDelete, Me.mnuEditSeparator1, Me.mnuEditSelectAll})
        Me.mnuEdit.Name = "mnuEdit"
        Me.mnuEdit.Size = New System.Drawing.Size(39, 20)
        Me.mnuEdit.Text = "&Edit"
        '
        'mnuEditCut
        '
        Me.mnuEditCut.Enabled = False
        Me.mnuEditCut.Image = CType(resources.GetObject("mnuEditCut.Image"), System.Drawing.Image)
        Me.mnuEditCut.Name = "mnuEditCut"
        Me.mnuEditCut.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.mnuEditCut.Size = New System.Drawing.Size(164, 22)
        Me.mnuEditCut.Text = "Cut"
        '
        'mnuEditCopy
        '
        Me.mnuEditCopy.Enabled = False
        Me.mnuEditCopy.Image = CType(resources.GetObject("mnuEditCopy.Image"), System.Drawing.Image)
        Me.mnuEditCopy.Name = "mnuEditCopy"
        Me.mnuEditCopy.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.mnuEditCopy.Size = New System.Drawing.Size(164, 22)
        Me.mnuEditCopy.Text = "Copy"
        '
        'mnuEditPaste
        '
        Me.mnuEditPaste.Enabled = False
        Me.mnuEditPaste.Image = CType(resources.GetObject("mnuEditPaste.Image"), System.Drawing.Image)
        Me.mnuEditPaste.Name = "mnuEditPaste"
        Me.mnuEditPaste.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.mnuEditPaste.Size = New System.Drawing.Size(164, 22)
        Me.mnuEditPaste.Text = "Paste"
        '
        'mnuEditDelete
        '
        Me.mnuEditDelete.Enabled = False
        Me.mnuEditDelete.Image = CType(resources.GetObject("mnuEditDelete.Image"), System.Drawing.Image)
        Me.mnuEditDelete.Name = "mnuEditDelete"
        Me.mnuEditDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete
        Me.mnuEditDelete.Size = New System.Drawing.Size(164, 22)
        Me.mnuEditDelete.Text = "Delete"
        '
        'mnuEditSeparator1
        '
        Me.mnuEditSeparator1.Name = "mnuEditSeparator1"
        Me.mnuEditSeparator1.Size = New System.Drawing.Size(161, 6)
        '
        'mnuEditSelectAll
        '
        Me.mnuEditSelectAll.Enabled = False
        Me.mnuEditSelectAll.Name = "mnuEditSelectAll"
        Me.mnuEditSelectAll.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.mnuEditSelectAll.Size = New System.Drawing.Size(164, 22)
        Me.mnuEditSelectAll.Text = "Select All"
        '
        'mnuView
        '
        Me.mnuView.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuViewToolBar, Me.mnuViewStatusBar, Me.mnuViewSeparator, Me.mnuViewSearch})
        Me.mnuView.Name = "mnuView"
        Me.mnuView.Size = New System.Drawing.Size(44, 20)
        Me.mnuView.Text = "&View"
        '
        'mnuViewToolBar
        '
        Me.mnuViewToolBar.Checked = True
        Me.mnuViewToolBar.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuViewToolBar.Name = "mnuViewToolBar"
        Me.mnuViewToolBar.Size = New System.Drawing.Size(167, 22)
        Me.mnuViewToolBar.Text = "Tool Bar"
        '
        'mnuViewStatusBar
        '
        Me.mnuViewStatusBar.Checked = True
        Me.mnuViewStatusBar.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuViewStatusBar.Name = "mnuViewStatusBar"
        Me.mnuViewStatusBar.Size = New System.Drawing.Size(167, 22)
        Me.mnuViewStatusBar.Text = "Status Bar"
        '
        'mnuViewSeparator
        '
        Me.mnuViewSeparator.Name = "mnuViewSeparator"
        Me.mnuViewSeparator.Size = New System.Drawing.Size(164, 6)
        '
        'mnuViewSearch
        '
        Me.mnuViewSearch.Image = CType(resources.GetObject("mnuViewSearch.Image"), System.Drawing.Image)
        Me.mnuViewSearch.Name = "mnuViewSearch"
        Me.mnuViewSearch.ShortcutKeyDisplayString = ""
        Me.mnuViewSearch.ShortcutKeys = System.Windows.Forms.Keys.F5
        Me.mnuViewSearch.Size = New System.Drawing.Size(167, 22)
        Me.mnuViewSearch.Text = "Search Engine"
        '
        'mnuSetup
        '
        Me.mnuSetup.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetupLookupData, Me.ToolStripSeparator10, Me.mnuSetupNew, Me.mnuSetupEdit, Me.ToolStripSeparator11, Me.mnuSetupExchangeRates, Me.mnuSetupSeparator2, Me.mnuSetupDrugBarcodes, Me.ToolStripMenuItem1, Me.mnuSetupNewConsumableBarCode, Me.mnuSetupIntegrationAgents, Me.ToolStripMenuItem3, Me.mnuSetupImport, Me.ToolStripMenuItem2, Me.ToolStripSeparator6, Me.mnuSetupSecurity})
        Me.mnuSetup.Name = "mnuSetup"
        Me.mnuSetup.Size = New System.Drawing.Size(53, 20)
        Me.mnuSetup.Text = "&Set Up"
        '
        'mnuSetupLookupData
        '
        Me.mnuSetupLookupData.Image = CType(resources.GetObject("mnuSetupLookupData.Image"), System.Drawing.Image)
        Me.mnuSetupLookupData.Name = "mnuSetupLookupData"
        Me.mnuSetupLookupData.Size = New System.Drawing.Size(221, 22)
        Me.mnuSetupLookupData.Tag = "LookupData"
        Me.mnuSetupLookupData.Text = "Lookup Data"
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(218, 6)
        '
        'mnuSetupNew
        '
        Me.mnuSetupNew.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetupNewBillable, Me.ToolStripSeparator1, Me.mnuSetupNewOtherItems, Me.mnuSetupNewDrugCategories, Me.mnuSetupNewStaff, Me.mnuSetupNewDrugCombinations, Me.mnuSetupNewRooms, Me.mnuSetupNewDiseasesEXT, Me.mnuSetupNewDiseases, Me.mnuSetupNewCancerDiseases, Me.mnuSetupNewPhysioDiseases, Me.mnuSetupNewUCITopologySites, Me.mnuSetupNewAllergies, Me.mnuSetupNewBillCustomers, Me.mnuSetupNewBillCustomerMembers, Me.mnuSetupNewMemberBenefits, Me.mnSetUpNewBankAccounts, Me.mnuSetupNewSuppliers, Me.mnuSetupNewLabEXTPossibleResults, Me.ToolStripSeparator14, Me.mnuSetupNewInsurance, Me.mnuSetupNewGeographicalLocation, Me.mnuSetupNewRevenueStreams})
        Me.mnuSetupNew.Image = CType(resources.GetObject("mnuSetupNew.Image"), System.Drawing.Image)
        Me.mnuSetupNew.Name = "mnuSetupNew"
        Me.mnuSetupNew.Size = New System.Drawing.Size(221, 22)
        Me.mnuSetupNew.Text = "New"
        '
        'mnuSetupNewBillable
        '
        Me.mnuSetupNewBillable.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetupNewBillableServices, Me.mnuSetupNewBillableDrugs, Me.mnuSetupNewBillableLabTests, Me.mnuSetupNewBillableCardiologyExaminations, Me.mnuSetupNewBillableRadiologyExaminations, Me.mnuSetupNewPathologyExaminations, Me.mnuSetupNewBillableConsumableItems, Me.mnuSetupNewBillableExtraChargeItems, Me.mnuSetupNewBillableDentalServices, Me.mnuSetupNewBillableTheatreServices, Me.mnuSetupNewBillableEyeServices, Me.mnuSetupNewBillableOpticalServices, Me.mnuSetupNewBillableMaternityServices, Me.mnuSetupNewBillableICUServices, Me.mnuSetupNewBillableProcedures, Me.mnuSetupNewPackages, Me.mnuSetupNewBillableBeds})
        Me.mnuSetupNewBillable.Name = "mnuSetupNewBillable"
        Me.mnuSetupNewBillable.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewBillable.Tag = ""
        Me.mnuSetupNewBillable.Text = "Billable"
        '
        'mnuSetupNewBillableServices
        '
        Me.mnuSetupNewBillableServices.Name = "mnuSetupNewBillableServices"
        Me.mnuSetupNewBillableServices.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewBillableServices.Tag = "Services"
        Me.mnuSetupNewBillableServices.Text = "Services"
        '
        'mnuSetupNewBillableDrugs
        '
        Me.mnuSetupNewBillableDrugs.Name = "mnuSetupNewBillableDrugs"
        Me.mnuSetupNewBillableDrugs.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewBillableDrugs.Tag = "Drugs"
        Me.mnuSetupNewBillableDrugs.Text = "Drugs"
        '
        'mnuSetupNewBillableLabTests
        '
        Me.mnuSetupNewBillableLabTests.Name = "mnuSetupNewBillableLabTests"
        Me.mnuSetupNewBillableLabTests.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewBillableLabTests.Tag = "LabTests"
        Me.mnuSetupNewBillableLabTests.Text = "Lab Tests"
        '
        'mnuSetupNewBillableCardiologyExaminations
        '
        Me.mnuSetupNewBillableCardiologyExaminations.Name = "mnuSetupNewBillableCardiologyExaminations"
        Me.mnuSetupNewBillableCardiologyExaminations.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewBillableCardiologyExaminations.Tag = "RadiologyExaminations"
        Me.mnuSetupNewBillableCardiologyExaminations.Text = "Cardiology Examinations"
        '
        'mnuSetupNewBillableRadiologyExaminations
        '
        Me.mnuSetupNewBillableRadiologyExaminations.Name = "mnuSetupNewBillableRadiologyExaminations"
        Me.mnuSetupNewBillableRadiologyExaminations.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewBillableRadiologyExaminations.Tag = "RadiologyExaminations"
        Me.mnuSetupNewBillableRadiologyExaminations.Text = "Radiology Examinations"
        '
        'mnuSetupNewPathologyExaminations
        '
        Me.mnuSetupNewPathologyExaminations.Name = "mnuSetupNewPathologyExaminations"
        Me.mnuSetupNewPathologyExaminations.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewPathologyExaminations.Tag = "PathologyExaminations"
        Me.mnuSetupNewPathologyExaminations.Text = "Pathology Examinations"
        '
        'mnuSetupNewBillableConsumableItems
        '
        Me.mnuSetupNewBillableConsumableItems.Name = "mnuSetupNewBillableConsumableItems"
        Me.mnuSetupNewBillableConsumableItems.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewBillableConsumableItems.Tag = "ConsumableItems"
        Me.mnuSetupNewBillableConsumableItems.Text = "Consumable Items"
        '
        'mnuSetupNewBillableExtraChargeItems
        '
        Me.mnuSetupNewBillableExtraChargeItems.Name = "mnuSetupNewBillableExtraChargeItems"
        Me.mnuSetupNewBillableExtraChargeItems.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewBillableExtraChargeItems.Tag = "ExtraChargeItems"
        Me.mnuSetupNewBillableExtraChargeItems.Text = "Extra Charge Items"
        '
        'mnuSetupNewBillableDentalServices
        '
        Me.mnuSetupNewBillableDentalServices.Name = "mnuSetupNewBillableDentalServices"
        Me.mnuSetupNewBillableDentalServices.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewBillableDentalServices.Tag = "DentalServices"
        Me.mnuSetupNewBillableDentalServices.Text = "Dental Services"
        '
        'mnuSetupNewBillableTheatreServices
        '
        Me.mnuSetupNewBillableTheatreServices.Name = "mnuSetupNewBillableTheatreServices"
        Me.mnuSetupNewBillableTheatreServices.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewBillableTheatreServices.Tag = "TheatreServices"
        Me.mnuSetupNewBillableTheatreServices.Text = "Theatre Services"
        '
        'mnuSetupNewBillableEyeServices
        '
        Me.mnuSetupNewBillableEyeServices.Name = "mnuSetupNewBillableEyeServices"
        Me.mnuSetupNewBillableEyeServices.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewBillableEyeServices.Tag = "EyeServices"
        Me.mnuSetupNewBillableEyeServices.Text = "Eye Services"
        '
        'mnuSetupNewBillableOpticalServices
        '
        Me.mnuSetupNewBillableOpticalServices.Name = "mnuSetupNewBillableOpticalServices"
        Me.mnuSetupNewBillableOpticalServices.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewBillableOpticalServices.Tag = "OpticalServices"
        Me.mnuSetupNewBillableOpticalServices.Text = "Optical Services"
        '
        'mnuSetupNewBillableMaternityServices
        '
        Me.mnuSetupNewBillableMaternityServices.Name = "mnuSetupNewBillableMaternityServices"
        Me.mnuSetupNewBillableMaternityServices.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewBillableMaternityServices.Tag = "MaternityServices"
        Me.mnuSetupNewBillableMaternityServices.Text = "Maternity Services"
        '
        'mnuSetupNewBillableICUServices
        '
        Me.mnuSetupNewBillableICUServices.Name = "mnuSetupNewBillableICUServices"
        Me.mnuSetupNewBillableICUServices.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewBillableICUServices.Tag = "ICUServices"
        Me.mnuSetupNewBillableICUServices.Text = "ICU Services"
        '
        'mnuSetupNewBillableProcedures
        '
        Me.mnuSetupNewBillableProcedures.Name = "mnuSetupNewBillableProcedures"
        Me.mnuSetupNewBillableProcedures.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewBillableProcedures.Tag = "Procedures"
        Me.mnuSetupNewBillableProcedures.Text = "Procedures"
        '
        'mnuSetupNewPackages
        '
        Me.mnuSetupNewPackages.Name = "mnuSetupNewPackages"
        Me.mnuSetupNewPackages.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewPackages.Tag = "Packages"
        Me.mnuSetupNewPackages.Text = "Packages"
        '
        'mnuSetupNewBillableBeds
        '
        Me.mnuSetupNewBillableBeds.Name = "mnuSetupNewBillableBeds"
        Me.mnuSetupNewBillableBeds.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupNewBillableBeds.Tag = "Beds"
        Me.mnuSetupNewBillableBeds.Text = "Beds"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(230, 6)
        '
        'mnuSetupNewOtherItems
        '
        Me.mnuSetupNewOtherItems.Name = "mnuSetupNewOtherItems"
        Me.mnuSetupNewOtherItems.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewOtherItems.Tag = "OtherItems"
        Me.mnuSetupNewOtherItems.Text = "Non Medical Items"
        '
        'mnuSetupNewDrugCategories
        '
        Me.mnuSetupNewDrugCategories.Name = "mnuSetupNewDrugCategories"
        Me.mnuSetupNewDrugCategories.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewDrugCategories.Tag = "DrugCategories"
        Me.mnuSetupNewDrugCategories.Text = "Drug Categories"
        '
        'mnuSetupNewStaff
        '
        Me.mnuSetupNewStaff.Name = "mnuSetupNewStaff"
        Me.mnuSetupNewStaff.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewStaff.Tag = "Staff"
        Me.mnuSetupNewStaff.Text = "Staff"
        '
        'mnuSetupNewDrugCombinations
        '
        Me.mnuSetupNewDrugCombinations.Name = "mnuSetupNewDrugCombinations"
        Me.mnuSetupNewDrugCombinations.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewDrugCombinations.Tag = "DrugCombinations"
        Me.mnuSetupNewDrugCombinations.Text = "Drug Combinations"
        '
        'mnuSetupNewRooms
        '
        Me.mnuSetupNewRooms.Name = "mnuSetupNewRooms"
        Me.mnuSetupNewRooms.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewRooms.Tag = "Rooms"
        Me.mnuSetupNewRooms.Text = "Rooms"
        '
        'mnuSetupNewDiseasesEXT
        '
        Me.mnuSetupNewDiseasesEXT.Name = "mnuSetupNewDiseasesEXT"
        Me.mnuSetupNewDiseasesEXT.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewDiseasesEXT.Tag = "DiseasesEXT"
        Me.mnuSetupNewDiseasesEXT.Text = "Diseases EXT"
        '
        'mnuSetupNewDiseases
        '
        Me.mnuSetupNewDiseases.Name = "mnuSetupNewDiseases"
        Me.mnuSetupNewDiseases.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewDiseases.Tag = "Diseases"
        Me.mnuSetupNewDiseases.Text = "Diseases"
        '
        'mnuSetupNewCancerDiseases
        '
        Me.mnuSetupNewCancerDiseases.Name = "mnuSetupNewCancerDiseases"
        Me.mnuSetupNewCancerDiseases.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewCancerDiseases.Tag = "CancerDiseases"
        Me.mnuSetupNewCancerDiseases.Text = "Cancer Diseases"
        '
        'mnuSetupNewPhysioDiseases
        '
        Me.mnuSetupNewPhysioDiseases.Name = "mnuSetupNewPhysioDiseases"
        Me.mnuSetupNewPhysioDiseases.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewPhysioDiseases.Tag = "PhysioDiseases"
        Me.mnuSetupNewPhysioDiseases.Text = "PhysioDiseases"
        '
        'mnuSetupNewUCITopologySites
        '
        Me.mnuSetupNewUCITopologySites.Name = "mnuSetupNewUCITopologySites"
        Me.mnuSetupNewUCITopologySites.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewUCITopologySites.Tag = "TopologySites"
        Me.mnuSetupNewUCITopologySites.Text = "Topology Sites"
        '
        'mnuSetupNewAllergies
        '
        Me.mnuSetupNewAllergies.Name = "mnuSetupNewAllergies"
        Me.mnuSetupNewAllergies.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewAllergies.Tag = "Allergies"
        Me.mnuSetupNewAllergies.Text = "Allergies"
        '
        'mnuSetupNewBillCustomers
        '
        Me.mnuSetupNewBillCustomers.Name = "mnuSetupNewBillCustomers"
        Me.mnuSetupNewBillCustomers.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewBillCustomers.Tag = "BillCustomers"
        Me.mnuSetupNewBillCustomers.Text = "To-Bill Customers"
        '
        'mnuSetupNewBillCustomerMembers
        '
        Me.mnuSetupNewBillCustomerMembers.Name = "mnuSetupNewBillCustomerMembers"
        Me.mnuSetupNewBillCustomerMembers.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewBillCustomerMembers.Tag = "BillCustomerMembers"
        Me.mnuSetupNewBillCustomerMembers.Text = "To-Bill Customer Members"
        '
        'mnuSetupNewMemberBenefits
        '
        Me.mnuSetupNewMemberBenefits.Name = "mnuSetupNewMemberBenefits"
        Me.mnuSetupNewMemberBenefits.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewMemberBenefits.Tag = "MemberBenefits"
        Me.mnuSetupNewMemberBenefits.Text = "Member Benefits"
        '
        'mnSetUpNewBankAccounts
        '
        Me.mnSetUpNewBankAccounts.Name = "mnSetUpNewBankAccounts"
        Me.mnSetUpNewBankAccounts.Size = New System.Drawing.Size(233, 22)
        Me.mnSetUpNewBankAccounts.Tag = "BankAccounts"
        Me.mnSetUpNewBankAccounts.Text = "Bank Accounts"
        '
        'mnuSetupNewSuppliers
        '
        Me.mnuSetupNewSuppliers.Name = "mnuSetupNewSuppliers"
        Me.mnuSetupNewSuppliers.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewSuppliers.Tag = "Suppliers"
        Me.mnuSetupNewSuppliers.Text = "Suppliers"
        '
        'mnuSetupNewLabEXTPossibleResults
        '
        Me.mnuSetupNewLabEXTPossibleResults.Name = "mnuSetupNewLabEXTPossibleResults"
        Me.mnuSetupNewLabEXTPossibleResults.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewLabEXTPossibleResults.Tag = "LabTestsEXTPossibleResults"
        Me.mnuSetupNewLabEXTPossibleResults.Text = "Possible Lab Results (Sub Test)"
        '
        'ToolStripSeparator14
        '
        Me.ToolStripSeparator14.Name = "ToolStripSeparator14"
        Me.ToolStripSeparator14.Size = New System.Drawing.Size(230, 6)
        '
        'mnuSetupNewInsurance
        '
        Me.mnuSetupNewInsurance.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetupNewInsuranceInsurances, Me.mnuSetupNewInsuranceCompanies, Me.mnuSetupNewInsuranceInsurancePolicies, Me.mnuSetupNewInsuranceInsuranceSchemes, Me.mnuSetupNewInsuranceSchemeMembers, Me.mnuSetupNewInsuranceHealthUnits})
        Me.mnuSetupNewInsurance.Name = "mnuSetupNewInsurance"
        Me.mnuSetupNewInsurance.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewInsurance.Tag = ""
        Me.mnuSetupNewInsurance.Text = "Insurance"
        '
        'mnuSetupNewInsuranceInsurances
        '
        Me.mnuSetupNewInsuranceInsurances.Name = "mnuSetupNewInsuranceInsurances"
        Me.mnuSetupNewInsuranceInsurances.Size = New System.Drawing.Size(175, 22)
        Me.mnuSetupNewInsuranceInsurances.Tag = "Insurances"
        Me.mnuSetupNewInsuranceInsurances.Text = "Insurances"
        '
        'mnuSetupNewInsuranceCompanies
        '
        Me.mnuSetupNewInsuranceCompanies.Name = "mnuSetupNewInsuranceCompanies"
        Me.mnuSetupNewInsuranceCompanies.Size = New System.Drawing.Size(175, 22)
        Me.mnuSetupNewInsuranceCompanies.Tag = "Companies"
        Me.mnuSetupNewInsuranceCompanies.Text = "Companies"
        '
        'mnuSetupNewInsuranceInsurancePolicies
        '
        Me.mnuSetupNewInsuranceInsurancePolicies.Name = "mnuSetupNewInsuranceInsurancePolicies"
        Me.mnuSetupNewInsuranceInsurancePolicies.Size = New System.Drawing.Size(175, 22)
        Me.mnuSetupNewInsuranceInsurancePolicies.Tag = "InsurancePolicies"
        Me.mnuSetupNewInsuranceInsurancePolicies.Text = "Insurance Policies"
        '
        'mnuSetupNewInsuranceInsuranceSchemes
        '
        Me.mnuSetupNewInsuranceInsuranceSchemes.Name = "mnuSetupNewInsuranceInsuranceSchemes"
        Me.mnuSetupNewInsuranceInsuranceSchemes.Size = New System.Drawing.Size(175, 22)
        Me.mnuSetupNewInsuranceInsuranceSchemes.Tag = "InsuranceSchemes"
        Me.mnuSetupNewInsuranceInsuranceSchemes.Text = "Insurance Schemes"
        '
        'mnuSetupNewInsuranceSchemeMembers
        '
        Me.mnuSetupNewInsuranceSchemeMembers.Name = "mnuSetupNewInsuranceSchemeMembers"
        Me.mnuSetupNewInsuranceSchemeMembers.Size = New System.Drawing.Size(175, 22)
        Me.mnuSetupNewInsuranceSchemeMembers.Tag = "SchemeMembers"
        Me.mnuSetupNewInsuranceSchemeMembers.Text = "Scheme Members"
        '
        'mnuSetupNewInsuranceHealthUnits
        '
        Me.mnuSetupNewInsuranceHealthUnits.Name = "mnuSetupNewInsuranceHealthUnits"
        Me.mnuSetupNewInsuranceHealthUnits.Size = New System.Drawing.Size(175, 22)
        Me.mnuSetupNewInsuranceHealthUnits.Tag = "HealthUnits"
        Me.mnuSetupNewInsuranceHealthUnits.Text = "Health Units"
        '
        'mnuSetupNewGeographicalLocation
        '
        Me.mnuSetupNewGeographicalLocation.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetupNewGeographicalLocationCounties, Me.mnuSetupNewGeographicalLocationSubCounties, Me.mnuSetupNewGeographicalLocationParishes, Me.mnuSetupNewGeographicalLocationVillages})
        Me.mnuSetupNewGeographicalLocation.Name = "mnuSetupNewGeographicalLocation"
        Me.mnuSetupNewGeographicalLocation.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewGeographicalLocation.Tag = ""
        Me.mnuSetupNewGeographicalLocation.Text = "Geographical Location"
        '
        'mnuSetupNewGeographicalLocationCounties
        '
        Me.mnuSetupNewGeographicalLocationCounties.Name = "mnuSetupNewGeographicalLocationCounties"
        Me.mnuSetupNewGeographicalLocationCounties.Size = New System.Drawing.Size(144, 22)
        Me.mnuSetupNewGeographicalLocationCounties.Tag = "Counties"
        Me.mnuSetupNewGeographicalLocationCounties.Text = "Counties"
        '
        'mnuSetupNewGeographicalLocationSubCounties
        '
        Me.mnuSetupNewGeographicalLocationSubCounties.Name = "mnuSetupNewGeographicalLocationSubCounties"
        Me.mnuSetupNewGeographicalLocationSubCounties.Size = New System.Drawing.Size(144, 22)
        Me.mnuSetupNewGeographicalLocationSubCounties.Tag = "SubCounties"
        Me.mnuSetupNewGeographicalLocationSubCounties.Text = "Sub Counties"
        '
        'mnuSetupNewGeographicalLocationParishes
        '
        Me.mnuSetupNewGeographicalLocationParishes.Name = "mnuSetupNewGeographicalLocationParishes"
        Me.mnuSetupNewGeographicalLocationParishes.Size = New System.Drawing.Size(144, 22)
        Me.mnuSetupNewGeographicalLocationParishes.Tag = "Parishes"
        Me.mnuSetupNewGeographicalLocationParishes.Text = "Parishes"
        '
        'mnuSetupNewGeographicalLocationVillages
        '
        Me.mnuSetupNewGeographicalLocationVillages.Name = "mnuSetupNewGeographicalLocationVillages"
        Me.mnuSetupNewGeographicalLocationVillages.Size = New System.Drawing.Size(144, 22)
        Me.mnuSetupNewGeographicalLocationVillages.Tag = "Villages"
        Me.mnuSetupNewGeographicalLocationVillages.Text = "Villages"
        '
        'mnuSetupNewRevenueStreams
        '
        Me.mnuSetupNewRevenueStreams.Name = "mnuSetupNewRevenueStreams"
        Me.mnuSetupNewRevenueStreams.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupNewRevenueStreams.Tag = "ExtraCharge"
        Me.mnuSetupNewRevenueStreams.Text = "Revenue Streams"
        '
        'mnuSetupEdit
        '
        Me.mnuSetupEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetupEditBillable, Me.ToolStripSeparator13, Me.mnuSetupEditChartOfAccounts, Me.ToolStripMenuItem36, Me.mnuSetupEditOtherItems, Me.mnuSetupEditDrugCategories, Me.mnuSetupEditStaff, Me.mnuSetupEditDrugCombinations, Me.mnuSetupEditRooms, Me.mnuSetupEditDiseasesEXT, Me.mnuSetupEditDiseases, Me.mnuSetupEditCancerDiseases, Me.mnuSetupEditTopologySites, Me.mnuSetupEditAllergies, Me.mnuSetupEditBillCustomers, Me.mnuSetupEditBillCustomerMembers, Me.mnuSetupEditMemberBenefits, Me.mnSetupEditBankAccounts, Me.mnuSetupEditSuppliers, Me.mnuSetupEditPossibleLabResultsSubTest, Me.ToolStripSeparator16, Me.mnuSetupEditInsurance, Me.mnuSetupEditGeographicalLocation})
        Me.mnuSetupEdit.Image = CType(resources.GetObject("mnuSetupEdit.Image"), System.Drawing.Image)
        Me.mnuSetupEdit.Name = "mnuSetupEdit"
        Me.mnuSetupEdit.Size = New System.Drawing.Size(221, 22)
        Me.mnuSetupEdit.Tag = ""
        Me.mnuSetupEdit.Text = "Edit"
        '
        'mnuSetupEditBillable
        '
        Me.mnuSetupEditBillable.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetupEditBillableServices, Me.mnuSetupEditBillableDrugs, Me.mnuSetupEditBillableLabTests, Me.mnuSetupEditBillableCardiologyExaminations, Me.mnuSetupEditBillableRadiologyExaminations, Me.mnuSetupEditPathologyExaminations, Me.mnuSetupEditBillableConsumableItems, Me.mnuSetupEditBillableExtraChargeItems, Me.mnuSetupEditBillableDentalServices, Me.mnuSetupEditBillableTheatreServices, Me.mnuSetupEditBillableEyeServices, Me.mnuSetupEditBillableOpticalServices, Me.mnuSetupEditBillableMaternityServices, Me.mnuSetupEditBillableICUServices, Me.mnuSetupEditBillableProcedures, Me.PackagesToolStripMenuItem, Me.mnuSetupEditBillableBeds, Me.mnuSetupEditBillableItemUnitPrice})
        Me.mnuSetupEditBillable.Name = "mnuSetupEditBillable"
        Me.mnuSetupEditBillable.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditBillable.Tag = ""
        Me.mnuSetupEditBillable.Text = "Billable"
        '
        'mnuSetupEditBillableServices
        '
        Me.mnuSetupEditBillableServices.Name = "mnuSetupEditBillableServices"
        Me.mnuSetupEditBillableServices.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableServices.Tag = "Services"
        Me.mnuSetupEditBillableServices.Text = "Services"
        '
        'mnuSetupEditBillableDrugs
        '
        Me.mnuSetupEditBillableDrugs.Name = "mnuSetupEditBillableDrugs"
        Me.mnuSetupEditBillableDrugs.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableDrugs.Tag = "Drugs"
        Me.mnuSetupEditBillableDrugs.Text = "Drugs"
        '
        'mnuSetupEditBillableLabTests
        '
        Me.mnuSetupEditBillableLabTests.Name = "mnuSetupEditBillableLabTests"
        Me.mnuSetupEditBillableLabTests.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableLabTests.Tag = "LabTests"
        Me.mnuSetupEditBillableLabTests.Text = "Lab Tests"
        '
        'mnuSetupEditBillableCardiologyExaminations
        '
        Me.mnuSetupEditBillableCardiologyExaminations.Name = "mnuSetupEditBillableCardiologyExaminations"
        Me.mnuSetupEditBillableCardiologyExaminations.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableCardiologyExaminations.Tag = "RadiologyExaminations"
        Me.mnuSetupEditBillableCardiologyExaminations.Text = "Cardiology Examinations"
        '
        'mnuSetupEditBillableRadiologyExaminations
        '
        Me.mnuSetupEditBillableRadiologyExaminations.Name = "mnuSetupEditBillableRadiologyExaminations"
        Me.mnuSetupEditBillableRadiologyExaminations.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableRadiologyExaminations.Tag = "RadiologyExaminations"
        Me.mnuSetupEditBillableRadiologyExaminations.Text = "Radiology Examinations"
        '
        'mnuSetupEditPathologyExaminations
        '
        Me.mnuSetupEditPathologyExaminations.Name = "mnuSetupEditPathologyExaminations"
        Me.mnuSetupEditPathologyExaminations.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditPathologyExaminations.Tag = "PathologyExaminations"
        Me.mnuSetupEditPathologyExaminations.Text = "Pathology Examinations"
        '
        'mnuSetupEditBillableConsumableItems
        '
        Me.mnuSetupEditBillableConsumableItems.Name = "mnuSetupEditBillableConsumableItems"
        Me.mnuSetupEditBillableConsumableItems.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableConsumableItems.Tag = "ConsumableItems"
        Me.mnuSetupEditBillableConsumableItems.Text = "Consumable Items"
        '
        'mnuSetupEditBillableExtraChargeItems
        '
        Me.mnuSetupEditBillableExtraChargeItems.Name = "mnuSetupEditBillableExtraChargeItems"
        Me.mnuSetupEditBillableExtraChargeItems.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableExtraChargeItems.Tag = "ExtraChargeItems"
        Me.mnuSetupEditBillableExtraChargeItems.Text = "Extra Charge Items"
        '
        'mnuSetupEditBillableDentalServices
        '
        Me.mnuSetupEditBillableDentalServices.Name = "mnuSetupEditBillableDentalServices"
        Me.mnuSetupEditBillableDentalServices.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableDentalServices.Tag = "DentalServices"
        Me.mnuSetupEditBillableDentalServices.Text = "Dental Services"
        '
        'mnuSetupEditBillableTheatreServices
        '
        Me.mnuSetupEditBillableTheatreServices.Name = "mnuSetupEditBillableTheatreServices"
        Me.mnuSetupEditBillableTheatreServices.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableTheatreServices.Tag = "TheatreServices"
        Me.mnuSetupEditBillableTheatreServices.Text = "Theatre Services"
        '
        'mnuSetupEditBillableEyeServices
        '
        Me.mnuSetupEditBillableEyeServices.Name = "mnuSetupEditBillableEyeServices"
        Me.mnuSetupEditBillableEyeServices.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableEyeServices.Tag = "EyeServices"
        Me.mnuSetupEditBillableEyeServices.Text = "Eye Services"
        '
        'mnuSetupEditBillableOpticalServices
        '
        Me.mnuSetupEditBillableOpticalServices.Name = "mnuSetupEditBillableOpticalServices"
        Me.mnuSetupEditBillableOpticalServices.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableOpticalServices.Tag = "OpticalServices"
        Me.mnuSetupEditBillableOpticalServices.Text = "Optical Services"
        '
        'mnuSetupEditBillableMaternityServices
        '
        Me.mnuSetupEditBillableMaternityServices.Name = "mnuSetupEditBillableMaternityServices"
        Me.mnuSetupEditBillableMaternityServices.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableMaternityServices.Tag = "MaternityServices"
        Me.mnuSetupEditBillableMaternityServices.Text = "Maternity Services"
        '
        'mnuSetupEditBillableICUServices
        '
        Me.mnuSetupEditBillableICUServices.Name = "mnuSetupEditBillableICUServices"
        Me.mnuSetupEditBillableICUServices.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableICUServices.Tag = "ICUServices"
        Me.mnuSetupEditBillableICUServices.Text = "ICU Services"
        '
        'mnuSetupEditBillableProcedures
        '
        Me.mnuSetupEditBillableProcedures.Name = "mnuSetupEditBillableProcedures"
        Me.mnuSetupEditBillableProcedures.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableProcedures.Tag = "Procedures"
        Me.mnuSetupEditBillableProcedures.Text = "Procedures"
        '
        'PackagesToolStripMenuItem
        '
        Me.PackagesToolStripMenuItem.Name = "PackagesToolStripMenuItem"
        Me.PackagesToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.PackagesToolStripMenuItem.Tag = "Packages"
        Me.PackagesToolStripMenuItem.Text = "Packages"
        '
        'mnuSetupEditBillableBeds
        '
        Me.mnuSetupEditBillableBeds.Name = "mnuSetupEditBillableBeds"
        Me.mnuSetupEditBillableBeds.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableBeds.Tag = "Beds"
        Me.mnuSetupEditBillableBeds.Text = "Beds"
        '
        'mnuSetupEditBillableItemUnitPrice
        '
        Me.mnuSetupEditBillableItemUnitPrice.Name = "mnuSetupEditBillableItemUnitPrice"
        Me.mnuSetupEditBillableItemUnitPrice.Size = New System.Drawing.Size(206, 22)
        Me.mnuSetupEditBillableItemUnitPrice.Tag = "ItemUnitPrice"
        Me.mnuSetupEditBillableItemUnitPrice.Text = "Item Unit Price"
        '
        'ToolStripSeparator13
        '
        Me.ToolStripSeparator13.Name = "ToolStripSeparator13"
        Me.ToolStripSeparator13.Size = New System.Drawing.Size(230, 6)
        '
        'mnuSetupEditChartOfAccounts
        '
        Me.mnuSetupEditChartOfAccounts.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetupEditChartOfAccountsCategories, Me.ToolStripSeparator20, Me.mnuSetupEditChartOfAccountsSubCategories})
        Me.mnuSetupEditChartOfAccounts.Name = "mnuSetupEditChartOfAccounts"
        Me.mnuSetupEditChartOfAccounts.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditChartOfAccounts.Text = "Chart Of Accounts"
        '
        'mnuSetupEditChartOfAccountsCategories
        '
        Me.mnuSetupEditChartOfAccountsCategories.Name = "mnuSetupEditChartOfAccountsCategories"
        Me.mnuSetupEditChartOfAccountsCategories.Size = New System.Drawing.Size(153, 22)
        Me.mnuSetupEditChartOfAccountsCategories.Text = "Categories"
        '
        'ToolStripSeparator20
        '
        Me.ToolStripSeparator20.Name = "ToolStripSeparator20"
        Me.ToolStripSeparator20.Size = New System.Drawing.Size(150, 6)
        '
        'mnuSetupEditChartOfAccountsSubCategories
        '
        Me.mnuSetupEditChartOfAccountsSubCategories.Name = "mnuSetupEditChartOfAccountsSubCategories"
        Me.mnuSetupEditChartOfAccountsSubCategories.Size = New System.Drawing.Size(153, 22)
        Me.mnuSetupEditChartOfAccountsSubCategories.Text = "Sub Categories"
        '
        'ToolStripMenuItem36
        '
        Me.ToolStripMenuItem36.Name = "ToolStripMenuItem36"
        Me.ToolStripMenuItem36.Size = New System.Drawing.Size(230, 6)
        '
        'mnuSetupEditOtherItems
        '
        Me.mnuSetupEditOtherItems.Name = "mnuSetupEditOtherItems"
        Me.mnuSetupEditOtherItems.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditOtherItems.Tag = "OtherItems"
        Me.mnuSetupEditOtherItems.Text = "Non Medical Items"
        '
        'mnuSetupEditDrugCategories
        '
        Me.mnuSetupEditDrugCategories.Name = "mnuSetupEditDrugCategories"
        Me.mnuSetupEditDrugCategories.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditDrugCategories.Tag = "DrugCategories"
        Me.mnuSetupEditDrugCategories.Text = "Drug Categories"
        '
        'mnuSetupEditStaff
        '
        Me.mnuSetupEditStaff.Name = "mnuSetupEditStaff"
        Me.mnuSetupEditStaff.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditStaff.Tag = "Staff"
        Me.mnuSetupEditStaff.Text = "Staff"
        '
        'mnuSetupEditDrugCombinations
        '
        Me.mnuSetupEditDrugCombinations.Name = "mnuSetupEditDrugCombinations"
        Me.mnuSetupEditDrugCombinations.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditDrugCombinations.Tag = "DrugCombinations"
        Me.mnuSetupEditDrugCombinations.Text = "Drug Combinations"
        '
        'mnuSetupEditRooms
        '
        Me.mnuSetupEditRooms.Name = "mnuSetupEditRooms"
        Me.mnuSetupEditRooms.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditRooms.Tag = "Rooms"
        Me.mnuSetupEditRooms.Text = "Rooms"
        '
        'mnuSetupEditDiseasesEXT
        '
        Me.mnuSetupEditDiseasesEXT.Name = "mnuSetupEditDiseasesEXT"
        Me.mnuSetupEditDiseasesEXT.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditDiseasesEXT.Tag = "DiseasesEXT"
        Me.mnuSetupEditDiseasesEXT.Text = "Diseases EXT"
        '
        'mnuSetupEditDiseases
        '
        Me.mnuSetupEditDiseases.Name = "mnuSetupEditDiseases"
        Me.mnuSetupEditDiseases.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditDiseases.Tag = "Diseases"
        Me.mnuSetupEditDiseases.Text = "Diseases"
        '
        'mnuSetupEditCancerDiseases
        '
        Me.mnuSetupEditCancerDiseases.Name = "mnuSetupEditCancerDiseases"
        Me.mnuSetupEditCancerDiseases.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditCancerDiseases.Tag = "CancerDiseases"
        Me.mnuSetupEditCancerDiseases.Text = "Cancer Diseases"
        '
        'mnuSetupEditTopologySites
        '
        Me.mnuSetupEditTopologySites.Name = "mnuSetupEditTopologySites"
        Me.mnuSetupEditTopologySites.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditTopologySites.Tag = "TopologySites"
        Me.mnuSetupEditTopologySites.Text = "Topology Sites"
        '
        'mnuSetupEditAllergies
        '
        Me.mnuSetupEditAllergies.Name = "mnuSetupEditAllergies"
        Me.mnuSetupEditAllergies.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditAllergies.Tag = "Allergies"
        Me.mnuSetupEditAllergies.Text = "Allergies"
        '
        'mnuSetupEditBillCustomers
        '
        Me.mnuSetupEditBillCustomers.Name = "mnuSetupEditBillCustomers"
        Me.mnuSetupEditBillCustomers.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditBillCustomers.Tag = "BillCustomers"
        Me.mnuSetupEditBillCustomers.Text = "To-Bill Customers"
        '
        'mnuSetupEditBillCustomerMembers
        '
        Me.mnuSetupEditBillCustomerMembers.Name = "mnuSetupEditBillCustomerMembers"
        Me.mnuSetupEditBillCustomerMembers.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditBillCustomerMembers.Tag = "BillCustomerMembers"
        Me.mnuSetupEditBillCustomerMembers.Text = "To-Bill Customer Members"
        '
        'mnuSetupEditMemberBenefits
        '
        Me.mnuSetupEditMemberBenefits.Name = "mnuSetupEditMemberBenefits"
        Me.mnuSetupEditMemberBenefits.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditMemberBenefits.Tag = "MemberBenefits"
        Me.mnuSetupEditMemberBenefits.Text = "Member Benefits"
        '
        'mnSetupEditBankAccounts
        '
        Me.mnSetupEditBankAccounts.Name = "mnSetupEditBankAccounts"
        Me.mnSetupEditBankAccounts.Size = New System.Drawing.Size(233, 22)
        Me.mnSetupEditBankAccounts.Tag = "BankAccounts"
        Me.mnSetupEditBankAccounts.Text = "Bank Accounts"
        '
        'mnuSetupEditSuppliers
        '
        Me.mnuSetupEditSuppliers.Name = "mnuSetupEditSuppliers"
        Me.mnuSetupEditSuppliers.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditSuppliers.Tag = "Suppliers"
        Me.mnuSetupEditSuppliers.Text = "Suppliers"
        '
        'mnuSetupEditPossibleLabResultsSubTest
        '
        Me.mnuSetupEditPossibleLabResultsSubTest.Name = "mnuSetupEditPossibleLabResultsSubTest"
        Me.mnuSetupEditPossibleLabResultsSubTest.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditPossibleLabResultsSubTest.Tag = "LabTestsEXTPossibleResults"
        Me.mnuSetupEditPossibleLabResultsSubTest.Text = "Possible Lab Results (Sub Test)"
        '
        'ToolStripSeparator16
        '
        Me.ToolStripSeparator16.Name = "ToolStripSeparator16"
        Me.ToolStripSeparator16.Size = New System.Drawing.Size(230, 6)
        '
        'mnuSetupEditInsurance
        '
        Me.mnuSetupEditInsurance.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetupEditInsuranceInsurances, Me.mnuSetupEditInsuranceInsurancePolicies, Me.mnuSetupEditInsuranceCompanies, Me.mnuSetupEditInsuranceInsuranceSchemes, Me.mnuSetupEditInsuranceSchemeMembers, Me.mnuSetupEditInsuranceHealthUnits})
        Me.mnuSetupEditInsurance.Name = "mnuSetupEditInsurance"
        Me.mnuSetupEditInsurance.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditInsurance.Tag = ""
        Me.mnuSetupEditInsurance.Text = "Insurance"
        '
        'mnuSetupEditInsuranceInsurances
        '
        Me.mnuSetupEditInsuranceInsurances.Name = "mnuSetupEditInsuranceInsurances"
        Me.mnuSetupEditInsuranceInsurances.Size = New System.Drawing.Size(175, 22)
        Me.mnuSetupEditInsuranceInsurances.Tag = "Insurances"
        Me.mnuSetupEditInsuranceInsurances.Text = "Insurances"
        '
        'mnuSetupEditInsuranceInsurancePolicies
        '
        Me.mnuSetupEditInsuranceInsurancePolicies.Name = "mnuSetupEditInsuranceInsurancePolicies"
        Me.mnuSetupEditInsuranceInsurancePolicies.Size = New System.Drawing.Size(175, 22)
        Me.mnuSetupEditInsuranceInsurancePolicies.Tag = "InsurancePolicies"
        Me.mnuSetupEditInsuranceInsurancePolicies.Text = "Insurance Policies"
        '
        'mnuSetupEditInsuranceCompanies
        '
        Me.mnuSetupEditInsuranceCompanies.Name = "mnuSetupEditInsuranceCompanies"
        Me.mnuSetupEditInsuranceCompanies.Size = New System.Drawing.Size(175, 22)
        Me.mnuSetupEditInsuranceCompanies.Tag = "Companies"
        Me.mnuSetupEditInsuranceCompanies.Text = "Companies"
        '
        'mnuSetupEditInsuranceInsuranceSchemes
        '
        Me.mnuSetupEditInsuranceInsuranceSchemes.Name = "mnuSetupEditInsuranceInsuranceSchemes"
        Me.mnuSetupEditInsuranceInsuranceSchemes.Size = New System.Drawing.Size(175, 22)
        Me.mnuSetupEditInsuranceInsuranceSchemes.Tag = "InsuranceSchemes"
        Me.mnuSetupEditInsuranceInsuranceSchemes.Text = "Insurance Schemes"
        '
        'mnuSetupEditInsuranceSchemeMembers
        '
        Me.mnuSetupEditInsuranceSchemeMembers.Name = "mnuSetupEditInsuranceSchemeMembers"
        Me.mnuSetupEditInsuranceSchemeMembers.Size = New System.Drawing.Size(175, 22)
        Me.mnuSetupEditInsuranceSchemeMembers.Tag = "SchemeMembers"
        Me.mnuSetupEditInsuranceSchemeMembers.Text = "Scheme Members"
        '
        'mnuSetupEditInsuranceHealthUnits
        '
        Me.mnuSetupEditInsuranceHealthUnits.Name = "mnuSetupEditInsuranceHealthUnits"
        Me.mnuSetupEditInsuranceHealthUnits.Size = New System.Drawing.Size(175, 22)
        Me.mnuSetupEditInsuranceHealthUnits.Tag = "HealthUnits"
        Me.mnuSetupEditInsuranceHealthUnits.Text = "Health Units"
        '
        'mnuSetupEditGeographicalLocation
        '
        Me.mnuSetupEditGeographicalLocation.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetupEditGeographicalLocationCounties, Me.mnuSetupEditGeographicalLocationSubCounties, Me.mnuSetupEditGeographicalLocationParishes, Me.mnuSetupEditGeographicalLocationVillages})
        Me.mnuSetupEditGeographicalLocation.Name = "mnuSetupEditGeographicalLocation"
        Me.mnuSetupEditGeographicalLocation.Size = New System.Drawing.Size(233, 22)
        Me.mnuSetupEditGeographicalLocation.Tag = ""
        Me.mnuSetupEditGeographicalLocation.Text = "Geographical Location"
        '
        'mnuSetupEditGeographicalLocationCounties
        '
        Me.mnuSetupEditGeographicalLocationCounties.Name = "mnuSetupEditGeographicalLocationCounties"
        Me.mnuSetupEditGeographicalLocationCounties.Size = New System.Drawing.Size(144, 22)
        Me.mnuSetupEditGeographicalLocationCounties.Tag = "Counties"
        Me.mnuSetupEditGeographicalLocationCounties.Text = "Counties"
        '
        'mnuSetupEditGeographicalLocationSubCounties
        '
        Me.mnuSetupEditGeographicalLocationSubCounties.Name = "mnuSetupEditGeographicalLocationSubCounties"
        Me.mnuSetupEditGeographicalLocationSubCounties.Size = New System.Drawing.Size(144, 22)
        Me.mnuSetupEditGeographicalLocationSubCounties.Tag = "SubCounties"
        Me.mnuSetupEditGeographicalLocationSubCounties.Text = "Sub Counties"
        '
        'mnuSetupEditGeographicalLocationParishes
        '
        Me.mnuSetupEditGeographicalLocationParishes.Name = "mnuSetupEditGeographicalLocationParishes"
        Me.mnuSetupEditGeographicalLocationParishes.Size = New System.Drawing.Size(144, 22)
        Me.mnuSetupEditGeographicalLocationParishes.Tag = "Parishes"
        Me.mnuSetupEditGeographicalLocationParishes.Text = "Parishes"
        '
        'mnuSetupEditGeographicalLocationVillages
        '
        Me.mnuSetupEditGeographicalLocationVillages.Name = "mnuSetupEditGeographicalLocationVillages"
        Me.mnuSetupEditGeographicalLocationVillages.Size = New System.Drawing.Size(144, 22)
        Me.mnuSetupEditGeographicalLocationVillages.Tag = "Villages"
        Me.mnuSetupEditGeographicalLocationVillages.Text = "Villages"
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(218, 6)
        '
        'mnuSetupExchangeRates
        '
        Me.mnuSetupExchangeRates.Image = CType(resources.GetObject("mnuSetupExchangeRates.Image"), System.Drawing.Image)
        Me.mnuSetupExchangeRates.Name = "mnuSetupExchangeRates"
        Me.mnuSetupExchangeRates.Size = New System.Drawing.Size(221, 22)
        Me.mnuSetupExchangeRates.Tag = "ExchangeRates"
        Me.mnuSetupExchangeRates.Text = "Exchange Rates"
        '
        'mnuSetupSeparator2
        '
        Me.mnuSetupSeparator2.Name = "mnuSetupSeparator2"
        Me.mnuSetupSeparator2.Size = New System.Drawing.Size(218, 6)
        '
        'mnuSetupDrugBarcodes
        '
        Me.mnuSetupDrugBarcodes.Image = CType(resources.GetObject("mnuSetupDrugBarcodes.Image"), System.Drawing.Image)
        Me.mnuSetupDrugBarcodes.Name = "mnuSetupDrugBarcodes"
        Me.mnuSetupDrugBarcodes.Size = New System.Drawing.Size(221, 22)
        Me.mnuSetupDrugBarcodes.Tag = "BarCodeDetails"
        Me.mnuSetupDrugBarcodes.Text = "New Drug BarCodes"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(218, 6)
        '
        'mnuSetupNewConsumableBarCode
        '
        Me.mnuSetupNewConsumableBarCode.Image = CType(resources.GetObject("mnuSetupNewConsumableBarCode.Image"), System.Drawing.Image)
        Me.mnuSetupNewConsumableBarCode.Name = "mnuSetupNewConsumableBarCode"
        Me.mnuSetupNewConsumableBarCode.Size = New System.Drawing.Size(221, 22)
        Me.mnuSetupNewConsumableBarCode.Tag = "BarCodeDetails"
        Me.mnuSetupNewConsumableBarCode.Text = "New Consumable BarCodes"
        '
        'mnuSetupIntegrationAgents
        '
        Me.mnuSetupIntegrationAgents.Name = "mnuSetupIntegrationAgents"
        Me.mnuSetupIntegrationAgents.Size = New System.Drawing.Size(221, 22)
        Me.mnuSetupIntegrationAgents.Text = "Integration Agents"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(218, 6)
        '
        'mnuSetupImport
        '
        Me.mnuSetupImport.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetupImportDrugs, Me.mnuSetupImportConsumableItems, Me.mnuSetupImportInventory, Me.mnuSetupImportLabTests, Me.mnuSetupImportLabTestsEXT, Me.mnuSetupImportLabPossibleResults, Me.mnuSetupImportRadiologyExaminations, Me.mnuSetupImportProcedures, Me.mnuSetupImportDentalServices, Me.mnuSetupImportBillCustomers, Me.mnuSetupImportBillCustomFee, Me.mnuSetupImportInsuranceCustomFee, Me.mnuSetupImportBillExcludedItems, Me.mnuSetupImportInsuranceExclusions, Me.mnuSetupImportInsuranceExcludedItems, Me.mnuSetupImportBillCustomerMembers, Me.mnuSetupImportDiseases, Me.mnuSetupImportSchemeMembers})
        Me.mnuSetupImport.Image = CType(resources.GetObject("mnuSetupImport.Image"), System.Drawing.Image)
        Me.mnuSetupImport.Name = "mnuSetupImport"
        Me.mnuSetupImport.Size = New System.Drawing.Size(221, 22)
        Me.mnuSetupImport.Text = "Import"
        '
        'mnuSetupImportDrugs
        '
        Me.mnuSetupImportDrugs.Name = "mnuSetupImportDrugs"
        Me.mnuSetupImportDrugs.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportDrugs.Tag = "Drugs"
        Me.mnuSetupImportDrugs.Text = "Drugs"
        '
        'mnuSetupImportConsumableItems
        '
        Me.mnuSetupImportConsumableItems.Name = "mnuSetupImportConsumableItems"
        Me.mnuSetupImportConsumableItems.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportConsumableItems.Tag = "ConsumableItems"
        Me.mnuSetupImportConsumableItems.Text = "Consumable Items"
        '
        'mnuSetupImportInventory
        '
        Me.mnuSetupImportInventory.Name = "mnuSetupImportInventory"
        Me.mnuSetupImportInventory.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportInventory.Tag = "Inventory"
        Me.mnuSetupImportInventory.Text = "Inventory"
        '
        'mnuSetupImportLabTests
        '
        Me.mnuSetupImportLabTests.Name = "mnuSetupImportLabTests"
        Me.mnuSetupImportLabTests.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportLabTests.Tag = "LabTests"
        Me.mnuSetupImportLabTests.Text = "Lab Tests"
        '
        'mnuSetupImportLabTestsEXT
        '
        Me.mnuSetupImportLabTestsEXT.Name = "mnuSetupImportLabTestsEXT"
        Me.mnuSetupImportLabTestsEXT.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportLabTestsEXT.Tag = "LabTestsEXT"
        Me.mnuSetupImportLabTestsEXT.Text = "Lab Tests EXTRA"
        '
        'mnuSetupImportLabPossibleResults
        '
        Me.mnuSetupImportLabPossibleResults.Name = "mnuSetupImportLabPossibleResults"
        Me.mnuSetupImportLabPossibleResults.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportLabPossibleResults.Tag = "LabPossibleResults"
        Me.mnuSetupImportLabPossibleResults.Text = "Lab Possible Results"
        '
        'mnuSetupImportRadiologyExaminations
        '
        Me.mnuSetupImportRadiologyExaminations.Name = "mnuSetupImportRadiologyExaminations"
        Me.mnuSetupImportRadiologyExaminations.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportRadiologyExaminations.Tag = "RadiologyExaminations"
        Me.mnuSetupImportRadiologyExaminations.Text = "Radiology Examinations"
        '
        'mnuSetupImportProcedures
        '
        Me.mnuSetupImportProcedures.Name = "mnuSetupImportProcedures"
        Me.mnuSetupImportProcedures.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportProcedures.Tag = "Procedures"
        Me.mnuSetupImportProcedures.Text = "Procedures"
        '
        'mnuSetupImportDentalServices
        '
        Me.mnuSetupImportDentalServices.Name = "mnuSetupImportDentalServices"
        Me.mnuSetupImportDentalServices.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportDentalServices.Tag = "DentalServices"
        Me.mnuSetupImportDentalServices.Text = "Dental Services"
        '
        'mnuSetupImportBillCustomers
        '
        Me.mnuSetupImportBillCustomers.Name = "mnuSetupImportBillCustomers"
        Me.mnuSetupImportBillCustomers.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportBillCustomers.Tag = "BillCustomers"
        Me.mnuSetupImportBillCustomers.Text = "To-Bill Customers"
        '
        'mnuSetupImportBillCustomFee
        '
        Me.mnuSetupImportBillCustomFee.Name = "mnuSetupImportBillCustomFee"
        Me.mnuSetupImportBillCustomFee.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportBillCustomFee.Tag = "BillCustomFee"
        Me.mnuSetupImportBillCustomFee.Text = "To-Bill Custom Fee"
        '
        'mnuSetupImportInsuranceCustomFee
        '
        Me.mnuSetupImportInsuranceCustomFee.Name = "mnuSetupImportInsuranceCustomFee"
        Me.mnuSetupImportInsuranceCustomFee.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportInsuranceCustomFee.Tag = "InsuranceCustomFee"
        Me.mnuSetupImportInsuranceCustomFee.Text = "Insurance Custom Fee"
        '
        'mnuSetupImportBillExcludedItems
        '
        Me.mnuSetupImportBillExcludedItems.Name = "mnuSetupImportBillExcludedItems"
        Me.mnuSetupImportBillExcludedItems.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportBillExcludedItems.Tag = "BillExcludedItems"
        Me.mnuSetupImportBillExcludedItems.Text = "To-Bill Excluded Items"
        '
        'mnuSetupImportInsuranceExclusions
        '
        Me.mnuSetupImportInsuranceExclusions.Name = "mnuSetupImportInsuranceExclusions"
        Me.mnuSetupImportInsuranceExclusions.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportInsuranceExclusions.Tag = "InsuranceExclusions"
        Me.mnuSetupImportInsuranceExclusions.Text = "Insurance Exclusions"
        '
        'mnuSetupImportInsuranceExcludedItems
        '
        Me.mnuSetupImportInsuranceExcludedItems.Name = "mnuSetupImportInsuranceExcludedItems"
        Me.mnuSetupImportInsuranceExcludedItems.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportInsuranceExcludedItems.Tag = "InsuranceExcludedItems"
        Me.mnuSetupImportInsuranceExcludedItems.Text = "Insurance Excluded Items"
        '
        'mnuSetupImportBillCustomerMembers
        '
        Me.mnuSetupImportBillCustomerMembers.Name = "mnuSetupImportBillCustomerMembers"
        Me.mnuSetupImportBillCustomerMembers.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportBillCustomerMembers.Tag = "BillCustomerMembers"
        Me.mnuSetupImportBillCustomerMembers.Text = "To-Bill Customer Members"
        '
        'mnuSetupImportDiseases
        '
        Me.mnuSetupImportDiseases.Name = "mnuSetupImportDiseases"
        Me.mnuSetupImportDiseases.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportDiseases.Tag = "Diseases"
        Me.mnuSetupImportDiseases.Text = "Diseases"
        '
        'mnuSetupImportSchemeMembers
        '
        Me.mnuSetupImportSchemeMembers.Name = "mnuSetupImportSchemeMembers"
        Me.mnuSetupImportSchemeMembers.Size = New System.Drawing.Size(215, 22)
        Me.mnuSetupImportSchemeMembers.Tag = "SchemeMembers"
        Me.mnuSetupImportSchemeMembers.Text = "Scheme Members"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(218, 6)
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(218, 6)
        '
        'mnuSetupSecurity
        '
        Me.mnuSetupSecurity.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetupSecurityUsers, Me.mnuSetupSecurityRoles, Me.mnuSetupSecurityChangePassword, Me.ToolStripSeparator4, Me.mnuSetupSecurityLicenses})
        Me.mnuSetupSecurity.Image = CType(resources.GetObject("mnuSetupSecurity.Image"), System.Drawing.Image)
        Me.mnuSetupSecurity.Name = "mnuSetupSecurity"
        Me.mnuSetupSecurity.Size = New System.Drawing.Size(221, 22)
        Me.mnuSetupSecurity.Text = "Security"
        '
        'mnuSetupSecurityUsers
        '
        Me.mnuSetupSecurityUsers.Image = CType(resources.GetObject("mnuSetupSecurityUsers.Image"), System.Drawing.Image)
        Me.mnuSetupSecurityUsers.Name = "mnuSetupSecurityUsers"
        Me.mnuSetupSecurityUsers.Size = New System.Drawing.Size(168, 22)
        Me.mnuSetupSecurityUsers.Tag = "Logins"
        Me.mnuSetupSecurityUsers.Text = "Users"
        '
        'mnuSetupSecurityRoles
        '
        Me.mnuSetupSecurityRoles.Name = "mnuSetupSecurityRoles"
        Me.mnuSetupSecurityRoles.Size = New System.Drawing.Size(168, 22)
        Me.mnuSetupSecurityRoles.Tag = "Roles"
        Me.mnuSetupSecurityRoles.Text = "Roles"
        '
        'mnuSetupSecurityChangePassword
        '
        Me.mnuSetupSecurityChangePassword.Image = CType(resources.GetObject("mnuSetupSecurityChangePassword.Image"), System.Drawing.Image)
        Me.mnuSetupSecurityChangePassword.Name = "mnuSetupSecurityChangePassword"
        Me.mnuSetupSecurityChangePassword.Size = New System.Drawing.Size(168, 22)
        Me.mnuSetupSecurityChangePassword.Text = "Change Password"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(165, 6)
        '
        'mnuSetupSecurityLicenses
        '
        Me.mnuSetupSecurityLicenses.Name = "mnuSetupSecurityLicenses"
        Me.mnuSetupSecurityLicenses.Size = New System.Drawing.Size(168, 22)
        Me.mnuSetupSecurityLicenses.Tag = "Licenses"
        Me.mnuSetupSecurityLicenses.Text = "Licenses..."
        '
        'mnuReports
        '
        Me.mnuReports.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuReportsPayments, Me.mnuReportsInvoices, Me.mnReportsBillingForm, Me.mnuReportsExtras, Me.mnuReportsLabResults, Me.mnuReportsCardiologyReports, Me.mnuReportsIPDCardiologyReports, Me.mnuReportsRadiologyReports, Me.mnuReportsIPDRadiologyReports, Me.mnuReportsClaims, Me.mnuReportsIncomeSummaries, Me.mnuReportsIPDIncomeSummaries, Me.mnuReportsInventory, Me.mnuReportsDiagnosisSummaries, Me.mnuReportsLineGraphs, Me.mnuReportsGeneral, Me.mnReportsBankingRegister})
        Me.mnuReports.Name = "mnuReports"
        Me.mnuReports.Size = New System.Drawing.Size(59, 20)
        Me.mnuReports.Text = "&Reports"
        '
        'mnuReportsPayments
        '
        Me.mnuReportsPayments.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPaymentsCashCollections, Me.mnuReportsPaymentsCashReceipts, Me.mnuReportsPaymentsDailyFinancialReport, Me.mnuReportsPaymentsPatientsAccountStatement, Me.mnuReportsPaymentCategorisation, Me.ToolStripMenuItem49, Me.mnuReportsPaymentsAccountStatement, Me.mniReportdDetailedAccountStatement})
        Me.mnuReportsPayments.Name = "mnuReportsPayments"
        Me.mnuReportsPayments.Size = New System.Drawing.Size(197, 22)
        Me.mnuReportsPayments.Tag = "Payments"
        Me.mnuReportsPayments.Text = "Payments"
        '
        'mnuPaymentsCashCollections
        '
        Me.mnuPaymentsCashCollections.Name = "mnuPaymentsCashCollections"
        Me.mnuPaymentsCashCollections.Size = New System.Drawing.Size(222, 22)
        Me.mnuPaymentsCashCollections.Tag = "CashCollections"
        Me.mnuPaymentsCashCollections.Text = "Cash Collections"
        '
        'mnuReportsPaymentsCashReceipts
        '
        Me.mnuReportsPaymentsCashReceipts.Name = "mnuReportsPaymentsCashReceipts"
        Me.mnuReportsPaymentsCashReceipts.Size = New System.Drawing.Size(222, 22)
        Me.mnuReportsPaymentsCashReceipts.Tag = "CashReceipts"
        Me.mnuReportsPaymentsCashReceipts.Text = "Cash Receipts"
        '
        'mnuReportsPaymentsDailyFinancialReport
        '
        Me.mnuReportsPaymentsDailyFinancialReport.Name = "mnuReportsPaymentsDailyFinancialReport"
        Me.mnuReportsPaymentsDailyFinancialReport.Size = New System.Drawing.Size(222, 22)
        Me.mnuReportsPaymentsDailyFinancialReport.Tag = "DailyFinancialReport"
        Me.mnuReportsPaymentsDailyFinancialReport.Text = "Daily Financial Report"
        '
        'mnuReportsPaymentsPatientsAccountStatement
        '
        Me.mnuReportsPaymentsPatientsAccountStatement.Name = "mnuReportsPaymentsPatientsAccountStatement"
        Me.mnuReportsPaymentsPatientsAccountStatement.Size = New System.Drawing.Size(222, 22)
        Me.mnuReportsPaymentsPatientsAccountStatement.Tag = "CashReceipts"
        Me.mnuReportsPaymentsPatientsAccountStatement.Text = "Account Transactions"
        '
        'mnuReportsPaymentCategorisation
        '
        Me.mnuReportsPaymentCategorisation.Name = "mnuReportsPaymentCategorisation"
        Me.mnuReportsPaymentCategorisation.Size = New System.Drawing.Size(222, 22)
        Me.mnuReportsPaymentCategorisation.Text = "Payment Categorisation"
        '
        'ToolStripMenuItem49
        '
        Me.ToolStripMenuItem49.Name = "ToolStripMenuItem49"
        Me.ToolStripMenuItem49.Size = New System.Drawing.Size(219, 6)
        '
        'mnuReportsPaymentsAccountStatement
        '
        Me.mnuReportsPaymentsAccountStatement.Name = "mnuReportsPaymentsAccountStatement"
        Me.mnuReportsPaymentsAccountStatement.Size = New System.Drawing.Size(222, 22)
        Me.mnuReportsPaymentsAccountStatement.Text = "Account Statement"
        '
        'mniReportdDetailedAccountStatement
        '
        Me.mniReportdDetailedAccountStatement.Name = "mniReportdDetailedAccountStatement"
        Me.mniReportdDetailedAccountStatement.Size = New System.Drawing.Size(222, 22)
        Me.mniReportdDetailedAccountStatement.Tag = "CashReceipts"
        Me.mniReportdDetailedAccountStatement.Text = "Detailed Account Statement"
        '
        'mnuReportsInvoices
        '
        Me.mnuReportsInvoices.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuReportsInvoicesVisits, Me.mnuReportsInvoicesVisitInvoices, Me.mnuReportsInvoicesBillCustomers, Me.mnuReportsInvoicesInsurances, Me.mnuReportInvoicesDueInvoices, Me.mnuReportInvoicesInvoiceCategorisation})
        Me.mnuReportsInvoices.Name = "mnuReportsInvoices"
        Me.mnuReportsInvoices.Size = New System.Drawing.Size(197, 22)
        Me.mnuReportsInvoices.Tag = "Invoices"
        Me.mnuReportsInvoices.Text = "Invoices"
        '
        'mnuReportsInvoicesVisits
        '
        Me.mnuReportsInvoicesVisits.Name = "mnuReportsInvoicesVisits"
        Me.mnuReportsInvoicesVisits.Size = New System.Drawing.Size(192, 22)
        Me.mnuReportsInvoicesVisits.Tag = "Invoices"
        Me.mnuReportsInvoicesVisits.Text = "Visits"
        '
        'mnuReportsInvoicesVisitInvoices
        '
        Me.mnuReportsInvoicesVisitInvoices.Name = "mnuReportsInvoicesVisitInvoices"
        Me.mnuReportsInvoicesVisitInvoices.Size = New System.Drawing.Size(192, 22)
        Me.mnuReportsInvoicesVisitInvoices.Tag = "Invoices"
        Me.mnuReportsInvoicesVisitInvoices.Text = "Visit Invoices"
        '
        'mnuReportsInvoicesBillCustomers
        '
        Me.mnuReportsInvoicesBillCustomers.Name = "mnuReportsInvoicesBillCustomers"
        Me.mnuReportsInvoicesBillCustomers.Size = New System.Drawing.Size(192, 22)
        Me.mnuReportsInvoicesBillCustomers.Tag = "Invoices"
        Me.mnuReportsInvoicesBillCustomers.Text = "To-Bill Customers"
        '
        'mnuReportsInvoicesInsurances
        '
        Me.mnuReportsInvoicesInsurances.Name = "mnuReportsInvoicesInsurances"
        Me.mnuReportsInvoicesInsurances.Size = New System.Drawing.Size(192, 22)
        Me.mnuReportsInvoicesInsurances.Tag = "Invoices"
        Me.mnuReportsInvoicesInsurances.Text = "Insurances"
        '
        'mnuReportInvoicesDueInvoices
        '
        Me.mnuReportInvoicesDueInvoices.Name = "mnuReportInvoicesDueInvoices"
        Me.mnuReportInvoicesDueInvoices.Size = New System.Drawing.Size(192, 22)
        Me.mnuReportInvoicesDueInvoices.Tag = "Invoices"
        Me.mnuReportInvoicesDueInvoices.Text = "Due Invoices"
        '
        'mnuReportInvoicesInvoiceCategorisation
        '
        Me.mnuReportInvoicesInvoiceCategorisation.Name = "mnuReportInvoicesInvoiceCategorisation"
        Me.mnuReportInvoicesInvoiceCategorisation.Size = New System.Drawing.Size(192, 22)
        Me.mnuReportInvoicesInvoiceCategorisation.Tag = "Invoices"
        Me.mnuReportInvoicesInvoiceCategorisation.Text = "Invoice Categorisation"
        '
        'mnReportsBillingForm
        '
        Me.mnReportsBillingForm.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuReportsBillingFormByVisitNo, Me.mnuReportsBillingFormByExtraBillNo})
        Me.mnReportsBillingForm.Name = "mnReportsBillingForm"
        Me.mnReportsBillingForm.Size = New System.Drawing.Size(197, 22)
        Me.mnReportsBillingForm.Tag = "ExtraBills"
        Me.mnReportsBillingForm.Text = "Billing Form"
        '
        'mnuReportsBillingFormByVisitNo
        '
        Me.mnuReportsBillingFormByVisitNo.Name = "mnuReportsBillingFormByVisitNo"
        Me.mnuReportsBillingFormByVisitNo.Size = New System.Drawing.Size(154, 22)
        Me.mnuReportsBillingFormByVisitNo.Tag = "ExtraBills"
        Me.mnuReportsBillingFormByVisitNo.Text = "By Visit No"
        '
        'mnuReportsBillingFormByExtraBillNo
        '
        Me.mnuReportsBillingFormByExtraBillNo.Name = "mnuReportsBillingFormByExtraBillNo"
        Me.mnuReportsBillingFormByExtraBillNo.Size = New System.Drawing.Size(154, 22)
        Me.mnuReportsBillingFormByExtraBillNo.Tag = "ExtraBills"
        Me.mnuReportsBillingFormByExtraBillNo.Text = "By Extra Bill No"
        '
        'mnuReportsExtras
        '
        Me.mnuReportsExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuReportsExtrasInventoryAcknowledges, Me.mnuReportsExtrasGoodsReceivedNote, Me.mniReportsExtrasSupplierHistory})
        Me.mnuReportsExtras.Name = "mnuReportsExtras"
        Me.mnuReportsExtras.Size = New System.Drawing.Size(197, 22)
        Me.mnuReportsExtras.Text = "Extras"
        '
        'mnuReportsExtrasInventoryAcknowledges
        '
        Me.mnuReportsExtrasInventoryAcknowledges.Name = "mnuReportsExtrasInventoryAcknowledges"
        Me.mnuReportsExtrasInventoryAcknowledges.Size = New System.Drawing.Size(204, 22)
        Me.mnuReportsExtrasInventoryAcknowledges.Tag = "InventoryAcknowledges"
        Me.mnuReportsExtrasInventoryAcknowledges.Text = "Inventory Acknowledges"
        '
        'mnuReportsExtrasGoodsReceivedNote
        '
        Me.mnuReportsExtrasGoodsReceivedNote.Name = "mnuReportsExtrasGoodsReceivedNote"
        Me.mnuReportsExtrasGoodsReceivedNote.Size = New System.Drawing.Size(204, 22)
        Me.mnuReportsExtrasGoodsReceivedNote.Tag = "GoodsReceivedNote"
        Me.mnuReportsExtrasGoodsReceivedNote.Text = "Goods Received Note"
        '
        'mniReportsExtrasSupplierHistory
        '
        Me.mniReportsExtrasSupplierHistory.Name = "mniReportsExtrasSupplierHistory"
        Me.mniReportsExtrasSupplierHistory.Size = New System.Drawing.Size(204, 22)
        Me.mniReportsExtrasSupplierHistory.Tag = "GoodsReceivedNote"
        Me.mniReportsExtrasSupplierHistory.Text = "Supplier History"
        '
        'mnuReportsLabResults
        '
        Me.mnuReportsLabResults.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuReportLabResultsLabResultsReport, Me.ToolStripMenuItem6, Me.mnuReportLabResultsLabReport})
        Me.mnuReportsLabResults.Name = "mnuReportsLabResults"
        Me.mnuReportsLabResults.Size = New System.Drawing.Size(197, 22)
        Me.mnuReportsLabResults.Tag = "LabResults"
        Me.mnuReportsLabResults.Text = "Lab Results"
        '
        'mnuReportLabResultsLabResultsReport
        '
        Me.mnuReportLabResultsLabResultsReport.Name = "mnuReportLabResultsLabResultsReport"
        Me.mnuReportLabResultsLabResultsReport.Size = New System.Drawing.Size(171, 22)
        Me.mnuReportLabResultsLabResultsReport.Tag = "LabResults"
        Me.mnuReportLabResultsLabResultsReport.Text = "Lab Results Report"
        '
        'ToolStripMenuItem6
        '
        Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
        Me.ToolStripMenuItem6.Size = New System.Drawing.Size(168, 6)
        '
        'mnuReportLabResultsLabReport
        '
        Me.mnuReportLabResultsLabReport.Name = "mnuReportLabResultsLabReport"
        Me.mnuReportLabResultsLabReport.Size = New System.Drawing.Size(171, 22)
        Me.mnuReportLabResultsLabReport.Tag = "LabResults"
        Me.mnuReportLabResultsLabReport.Text = "Lab Report"
        '
        'mnuReportsCardiologyReports
        '
        Me.mnuReportsCardiologyReports.Name = "mnuReportsCardiologyReports"
        Me.mnuReportsCardiologyReports.Size = New System.Drawing.Size(197, 22)
        Me.mnuReportsCardiologyReports.Tag = "CardiologyReports"
        Me.mnuReportsCardiologyReports.Text = "Cardiology Reports"
        '
        'mnuReportsIPDCardiologyReports
        '
        Me.mnuReportsIPDCardiologyReports.Name = "mnuReportsIPDCardiologyReports"
        Me.mnuReportsIPDCardiologyReports.Size = New System.Drawing.Size(197, 22)
        Me.mnuReportsIPDCardiologyReports.Tag = "IPDCardiologyReports"
        Me.mnuReportsIPDCardiologyReports.Text = "IPD Cardiology Reports"
        '
        'mnuReportsRadiologyReports
        '
        Me.mnuReportsRadiologyReports.Name = "mnuReportsRadiologyReports"
        Me.mnuReportsRadiologyReports.Size = New System.Drawing.Size(197, 22)
        Me.mnuReportsRadiologyReports.Tag = "RadiologyReports"
        Me.mnuReportsRadiologyReports.Text = "Radiology Reports"
        '
        'mnuReportsIPDRadiologyReports
        '
        Me.mnuReportsIPDRadiologyReports.Name = "mnuReportsIPDRadiologyReports"
        Me.mnuReportsIPDRadiologyReports.Size = New System.Drawing.Size(197, 22)
        Me.mnuReportsIPDRadiologyReports.Tag = "IPDRadiologyReports"
        Me.mnuReportsIPDRadiologyReports.Text = "IPD Radiology Reports"
        '
        'mnuReportsClaims
        '
        Me.mnuReportsClaims.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuReportsClaimsBillCustomersClaimForm, Me.mnuReportsClaimsInsuranceClaimForm, Me.mnuReportsClaimsInsuranceClaimSummaries})
        Me.mnuReportsClaims.Name = "mnuReportsClaims"
        Me.mnuReportsClaims.Size = New System.Drawing.Size(197, 22)
        Me.mnuReportsClaims.Tag = ""
        Me.mnuReportsClaims.Text = "Claims"
        '
        'mnuReportsClaimsBillCustomersClaimForm
        '
        Me.mnuReportsClaimsBillCustomersClaimForm.Name = "mnuReportsClaimsBillCustomersClaimForm"
        Me.mnuReportsClaimsBillCustomersClaimForm.Size = New System.Drawing.Size(232, 22)
        Me.mnuReportsClaimsBillCustomersClaimForm.Tag = "Invoices"
        Me.mnuReportsClaimsBillCustomersClaimForm.Text = "To-Bill Customers Claim Form"
        '
        'mnuReportsClaimsInsuranceClaimForm
        '
        Me.mnuReportsClaimsInsuranceClaimForm.Name = "mnuReportsClaimsInsuranceClaimForm"
        Me.mnuReportsClaimsInsuranceClaimForm.Size = New System.Drawing.Size(232, 22)
        Me.mnuReportsClaimsInsuranceClaimForm.Tag = "Claims"
        Me.mnuReportsClaimsInsuranceClaimForm.Text = "Insurance Claim Form"
        '
        'mnuReportsClaimsInsuranceClaimSummaries
        '
        Me.mnuReportsClaimsInsuranceClaimSummaries.Name = "mnuReportsClaimsInsuranceClaimSummaries"
        Me.mnuReportsClaimsInsuranceClaimSummaries.Size = New System.Drawing.Size(232, 22)
        Me.mnuReportsClaimsInsuranceClaimSummaries.Tag = "Claims"
        Me.mnuReportsClaimsInsuranceClaimSummaries.Text = "Insurance Claim Summaries"
        '
        'mnuReportsIncomeSummaries
        '
        Me.mnuReportsIncomeSummaries.Name = "mnuReportsIncomeSummaries"
        Me.mnuReportsIncomeSummaries.Size = New System.Drawing.Size(197, 22)
        Me.mnuReportsIncomeSummaries.Tag = "IncomeSummaries"
        Me.mnuReportsIncomeSummaries.Text = "Income Summaries"
        '
        'mnuReportsIPDIncomeSummaries
        '
        Me.mnuReportsIPDIncomeSummaries.Name = "mnuReportsIPDIncomeSummaries"
        Me.mnuReportsIPDIncomeSummaries.Size = New System.Drawing.Size(197, 22)
        Me.mnuReportsIPDIncomeSummaries.Tag = "IncomeSummaries"
        Me.mnuReportsIPDIncomeSummaries.Text = "IPD Income Summaries"
        '
        'mnuReportsInventory
        '
        Me.mnuReportsInventory.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuReportsInventoryDrugStockCard, Me.mnuReportsInventoryConsumableStockCard, Me.mnuReportsInventoryDrugInventorySummaries, Me.mnuReportsInventoryConsumableInventorySummaries, Me.mnuReportsExtrasPhysicalStockCount})
        Me.mnuReportsInventory.Name = "mnuReportsInventory"
        Me.mnuReportsInventory.Size = New System.Drawing.Size(197, 22)
        Me.mnuReportsInventory.Tag = "Inventory"
        Me.mnuReportsInventory.Text = "Inventory"
        '
        'mnuReportsInventoryDrugStockCard
        '
        Me.mnuReportsInventoryDrugStockCard.Name = "mnuReportsInventoryDrugStockCard"
        Me.mnuReportsInventoryDrugStockCard.Size = New System.Drawing.Size(256, 22)
        Me.mnuReportsInventoryDrugStockCard.Tag = "DrugStockCard"
        Me.mnuReportsInventoryDrugStockCard.Text = "Drug Stock Card"
        '
        'mnuReportsInventoryConsumableStockCard
        '
        Me.mnuReportsInventoryConsumableStockCard.Name = "mnuReportsInventoryConsumableStockCard"
        Me.mnuReportsInventoryConsumableStockCard.Size = New System.Drawing.Size(256, 22)
        Me.mnuReportsInventoryConsumableStockCard.Tag = "ConsumableStockCard"
        Me.mnuReportsInventoryConsumableStockCard.Text = "Consumable Stock Card"
        '
        'mnuReportsInventoryDrugInventorySummaries
        '
        Me.mnuReportsInventoryDrugInventorySummaries.Name = "mnuReportsInventoryDrugInventorySummaries"
        Me.mnuReportsInventoryDrugInventorySummaries.Size = New System.Drawing.Size(256, 22)
        Me.mnuReportsInventoryDrugInventorySummaries.Tag = "DrugInventorySummaries"
        Me.mnuReportsInventoryDrugInventorySummaries.Text = "Drug Inventory Summaries"
        '
        'mnuReportsInventoryConsumableInventorySummaries
        '
        Me.mnuReportsInventoryConsumableInventorySummaries.Name = "mnuReportsInventoryConsumableInventorySummaries"
        Me.mnuReportsInventoryConsumableInventorySummaries.Size = New System.Drawing.Size(256, 22)
        Me.mnuReportsInventoryConsumableInventorySummaries.Tag = "ConsumableInventorySummaries"
        Me.mnuReportsInventoryConsumableInventorySummaries.Text = "Consumable Inventory Summaries"
        '
        'mnuReportsExtrasPhysicalStockCount
        '
        Me.mnuReportsExtrasPhysicalStockCount.Name = "mnuReportsExtrasPhysicalStockCount"
        Me.mnuReportsExtrasPhysicalStockCount.Size = New System.Drawing.Size(256, 22)
        Me.mnuReportsExtrasPhysicalStockCount.Tag = "PhysicalStockCountReport"
        Me.mnuReportsExtrasPhysicalStockCount.Text = "Physical Stock Count"
        '
        'mnuReportsDiagnosisSummaries
        '
        Me.mnuReportsDiagnosisSummaries.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuReportsDiagnosisReportsDiagnosisSummaries, Me.mnuReportsDiagnosisReportsDiagnosisReattendances})
        Me.mnuReportsDiagnosisSummaries.Name = "mnuReportsDiagnosisSummaries"
        Me.mnuReportsDiagnosisSummaries.Size = New System.Drawing.Size(197, 22)
        Me.mnuReportsDiagnosisSummaries.Tag = "DiagnosisSummaries"
        Me.mnuReportsDiagnosisSummaries.Text = "Diagnosis Reports"
        '
        'mnuReportsDiagnosisReportsDiagnosisSummaries
        '
        Me.mnuReportsDiagnosisReportsDiagnosisSummaries.Name = "mnuReportsDiagnosisReportsDiagnosisSummaries"
        Me.mnuReportsDiagnosisReportsDiagnosisSummaries.Size = New System.Drawing.Size(205, 22)
        Me.mnuReportsDiagnosisReportsDiagnosisSummaries.Text = "Diagnosis Summaries"
        '
        'mnuReportsDiagnosisReportsDiagnosisReattendances
        '
        Me.mnuReportsDiagnosisReportsDiagnosisReattendances.Name = "mnuReportsDiagnosisReportsDiagnosisReattendances"
        Me.mnuReportsDiagnosisReportsDiagnosisReattendances.Size = New System.Drawing.Size(205, 22)
        Me.mnuReportsDiagnosisReportsDiagnosisReattendances.Text = "Diagnosis Reattendances"
        '
        'mnuReportsLineGraphs
        '
        Me.mnuReportsLineGraphs.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuReportsLineGraphsLabResults})
        Me.mnuReportsLineGraphs.Name = "mnuReportsLineGraphs"
        Me.mnuReportsLineGraphs.Size = New System.Drawing.Size(197, 22)
        Me.mnuReportsLineGraphs.Text = "Line Graphs"
        '
        'mnuReportsLineGraphsLabResults
        '
        Me.mnuReportsLineGraphsLabResults.Name = "mnuReportsLineGraphsLabResults"
        Me.mnuReportsLineGraphsLabResults.Size = New System.Drawing.Size(133, 22)
        Me.mnuReportsLineGraphsLabResults.Tag = "LabResults"
        Me.mnuReportsLineGraphsLabResults.Text = "Lab Results"
        '
        'mnuReportsGeneral
        '
        Me.mnuReportsGeneral.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuReportsGeneralAppointments, Me.ToolStripMenuItem4, Me.mnuReportsGeneralOperations, Me.ToolStripMenuItem5, Me.mnuReportsGeneraItemStatus, Me.ToolStripMenuItem10, Me.mnuReportsGeneraPatientRecords, Me.ToolStripMenuItem48, Me.mnuReportsGeneraIOPDSpecialistTransactions})
        Me.mnuReportsGeneral.Name = "mnuReportsGeneral"
        Me.mnuReportsGeneral.Size = New System.Drawing.Size(197, 22)
        Me.mnuReportsGeneral.Text = "General"
        '
        'mnuReportsGeneralAppointments
        '
        Me.mnuReportsGeneralAppointments.Name = "mnuReportsGeneralAppointments"
        Me.mnuReportsGeneralAppointments.Size = New System.Drawing.Size(191, 22)
        Me.mnuReportsGeneralAppointments.Tag = "Appointments"
        Me.mnuReportsGeneralAppointments.Text = "Appointments"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(188, 6)
        '
        'mnuReportsGeneralOperations
        '
        Me.mnuReportsGeneralOperations.Name = "mnuReportsGeneralOperations"
        Me.mnuReportsGeneralOperations.Size = New System.Drawing.Size(191, 22)
        Me.mnuReportsGeneralOperations.Tag = "Operations"
        Me.mnuReportsGeneralOperations.Text = "Operations"
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(188, 6)
        '
        'mnuReportsGeneraItemStatus
        '
        Me.mnuReportsGeneraItemStatus.Name = "mnuReportsGeneraItemStatus"
        Me.mnuReportsGeneraItemStatus.Size = New System.Drawing.Size(191, 22)
        Me.mnuReportsGeneraItemStatus.Tag = "ItemStatus"
        Me.mnuReportsGeneraItemStatus.Text = "Item Status"
        '
        'ToolStripMenuItem10
        '
        Me.ToolStripMenuItem10.Name = "ToolStripMenuItem10"
        Me.ToolStripMenuItem10.Size = New System.Drawing.Size(188, 6)
        '
        'mnuReportsGeneraPatientRecords
        '
        Me.mnuReportsGeneraPatientRecords.Name = "mnuReportsGeneraPatientRecords"
        Me.mnuReportsGeneraPatientRecords.Size = New System.Drawing.Size(191, 22)
        Me.mnuReportsGeneraPatientRecords.Tag = "Patients"
        Me.mnuReportsGeneraPatientRecords.Text = "Patient Records"
        '
        'ToolStripMenuItem48
        '
        Me.ToolStripMenuItem48.Name = "ToolStripMenuItem48"
        Me.ToolStripMenuItem48.Size = New System.Drawing.Size(188, 6)
        '
        'mnuReportsGeneraIOPDSpecialistTransactions
        '
        Me.mnuReportsGeneraIOPDSpecialistTransactions.Name = "mnuReportsGeneraIOPDSpecialistTransactions"
        Me.mnuReportsGeneraIOPDSpecialistTransactions.Size = New System.Drawing.Size(191, 22)
        Me.mnuReportsGeneraIOPDSpecialistTransactions.Tag = "CashCollections"
        Me.mnuReportsGeneraIOPDSpecialistTransactions.Text = "Specialist Transactions"
        '
        'mnReportsBankingRegister
        '
        Me.mnReportsBankingRegister.Name = "mnReportsBankingRegister"
        Me.mnReportsBankingRegister.Size = New System.Drawing.Size(197, 22)
        Me.mnReportsBankingRegister.Tag = "BankingRegister"
        Me.mnReportsBankingRegister.Text = "Banking Register"
        '
        'mnuTools
        '
        Me.mnuTools.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuToolsCalculator, Me.mnuToolsSeparator2, Me.mnuToolsDashboard, Me.mnuToolsDatabase, Me.mnuToolsErrorLog, Me.mnToolsQueue, Me.MnuToolsBulkSMS, Me.MnuToolsStaffLocations, Me.mnToolsBankingRegiser, Me.mnuToolsManageSpecialEdits, Me.mnuToolsManageRestrictedKeys, Me.mnuToolsReversals, Me.mnuToolsManageBillUnitPrice, Me.MnuToolsSuspenseAccount, Me.mnuToolsOthers, Me.mnuToolsSeparator1, Me.mnuToolsNew, Me.mnuToolsEdit, Me.mnuToolsSeparator3, Me.mnuToolsOptions})
        Me.mnuTools.Name = "mnuTools"
        Me.mnuTools.Size = New System.Drawing.Size(46, 20)
        Me.mnuTools.Text = "&Tools"
        '
        'mnuToolsCalculator
        '
        Me.mnuToolsCalculator.Image = CType(resources.GetObject("mnuToolsCalculator.Image"), System.Drawing.Image)
        Me.mnuToolsCalculator.Name = "mnuToolsCalculator"
        Me.mnuToolsCalculator.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.mnuToolsCalculator.Size = New System.Drawing.Size(208, 22)
        Me.mnuToolsCalculator.Text = "Calculator"
        '
        'mnuToolsSeparator2
        '
        Me.mnuToolsSeparator2.Name = "mnuToolsSeparator2"
        Me.mnuToolsSeparator2.Size = New System.Drawing.Size(205, 6)
        '
        'mnuToolsDashboard
        '
        Me.mnuToolsDashboard.Image = CType(resources.GetObject("mnuToolsDashboard.Image"), System.Drawing.Image)
        Me.mnuToolsDashboard.Name = "mnuToolsDashboard"
        Me.mnuToolsDashboard.Size = New System.Drawing.Size(208, 22)
        Me.mnuToolsDashboard.Tag = ""
        Me.mnuToolsDashboard.Text = "&Dashboard"
        '
        'mnuToolsDatabase
        '
        Me.mnuToolsDatabase.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuToolsDatabaseBackup, Me.mnuToolsDatabaseRestore})
        Me.mnuToolsDatabase.Image = CType(resources.GetObject("mnuToolsDatabase.Image"), System.Drawing.Image)
        Me.mnuToolsDatabase.Name = "mnuToolsDatabase"
        Me.mnuToolsDatabase.Size = New System.Drawing.Size(208, 22)
        Me.mnuToolsDatabase.Text = "Database"
        '
        'mnuToolsDatabaseBackup
        '
        Me.mnuToolsDatabaseBackup.Name = "mnuToolsDatabaseBackup"
        Me.mnuToolsDatabaseBackup.Size = New System.Drawing.Size(113, 22)
        Me.mnuToolsDatabaseBackup.Tag = "BackupDatabase"
        Me.mnuToolsDatabaseBackup.Text = "Backup"
        '
        'mnuToolsDatabaseRestore
        '
        Me.mnuToolsDatabaseRestore.Name = "mnuToolsDatabaseRestore"
        Me.mnuToolsDatabaseRestore.Size = New System.Drawing.Size(113, 22)
        Me.mnuToolsDatabaseRestore.Tag = "RestoreDatabase"
        Me.mnuToolsDatabaseRestore.Text = "Restore"
        '
        'mnuToolsErrorLog
        '
        Me.mnuToolsErrorLog.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuToolsErrorLogView, Me.mnuToolsErrorLogClear})
        Me.mnuToolsErrorLog.Image = CType(resources.GetObject("mnuToolsErrorLog.Image"), System.Drawing.Image)
        Me.mnuToolsErrorLog.Name = "mnuToolsErrorLog"
        Me.mnuToolsErrorLog.Size = New System.Drawing.Size(208, 22)
        Me.mnuToolsErrorLog.Text = "Error Log"
        '
        'mnuToolsErrorLogView
        '
        Me.mnuToolsErrorLogView.Name = "mnuToolsErrorLogView"
        Me.mnuToolsErrorLogView.Size = New System.Drawing.Size(101, 22)
        Me.mnuToolsErrorLogView.Text = "View"
        '
        'mnuToolsErrorLogClear
        '
        Me.mnuToolsErrorLogClear.Name = "mnuToolsErrorLogClear"
        Me.mnuToolsErrorLogClear.Size = New System.Drawing.Size(101, 22)
        Me.mnuToolsErrorLogClear.Text = "Clear"
        '
        'mnToolsQueue
        '
        Me.mnToolsQueue.Image = CType(resources.GetObject("mnToolsQueue.Image"), System.Drawing.Image)
        Me.mnToolsQueue.Name = "mnToolsQueue"
        Me.mnToolsQueue.Size = New System.Drawing.Size(208, 22)
        Me.mnToolsQueue.Tag = "Queue"
        Me.mnToolsQueue.Text = "Queue"
        '
        'MnuToolsBulkSMS
        '
        Me.MnuToolsBulkSMS.Image = CType(resources.GetObject("MnuToolsBulkSMS.Image"), System.Drawing.Image)
        Me.MnuToolsBulkSMS.Name = "MnuToolsBulkSMS"
        Me.MnuToolsBulkSMS.Size = New System.Drawing.Size(208, 22)
        Me.MnuToolsBulkSMS.Tag = "BulkMessaging"
        Me.MnuToolsBulkSMS.Text = "&Bulk SMS"
        '
        'MnuToolsStaffLocations
        '
        Me.MnuToolsStaffLocations.Image = CType(resources.GetObject("MnuToolsStaffLocations.Image"), System.Drawing.Image)
        Me.MnuToolsStaffLocations.Name = "MnuToolsStaffLocations"
        Me.MnuToolsStaffLocations.Size = New System.Drawing.Size(208, 22)
        Me.MnuToolsStaffLocations.Tag = "StaffLocations"
        Me.MnuToolsStaffLocations.Text = "Staff Locations"
        '
        'mnToolsBankingRegiser
        '
        Me.mnToolsBankingRegiser.Image = CType(resources.GetObject("mnToolsBankingRegiser.Image"), System.Drawing.Image)
        Me.mnToolsBankingRegiser.Name = "mnToolsBankingRegiser"
        Me.mnToolsBankingRegiser.Size = New System.Drawing.Size(208, 22)
        Me.mnToolsBankingRegiser.Tag = "BankingRegister"
        Me.mnToolsBankingRegiser.Text = "Banking Register"
        '
        'mnuToolsManageSpecialEdits
        '
        Me.mnuToolsManageSpecialEdits.Image = CType(resources.GetObject("mnuToolsManageSpecialEdits.Image"), System.Drawing.Image)
        Me.mnuToolsManageSpecialEdits.Name = "mnuToolsManageSpecialEdits"
        Me.mnuToolsManageSpecialEdits.Size = New System.Drawing.Size(208, 22)
        Me.mnuToolsManageSpecialEdits.Tag = "SpecialEdits"
        Me.mnuToolsManageSpecialEdits.Text = "Manage Special Edits..."
        '
        'mnuToolsManageRestrictedKeys
        '
        Me.mnuToolsManageRestrictedKeys.Image = CType(resources.GetObject("mnuToolsManageRestrictedKeys.Image"), System.Drawing.Image)
        Me.mnuToolsManageRestrictedKeys.Name = "mnuToolsManageRestrictedKeys"
        Me.mnuToolsManageRestrictedKeys.Size = New System.Drawing.Size(208, 22)
        Me.mnuToolsManageRestrictedKeys.Tag = "RestrictedKeys"
        Me.mnuToolsManageRestrictedKeys.Text = "Manage Restricted Keys..."
        '
        'mnuToolsReversals
        '
        Me.mnuToolsReversals.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuToolsReversalsReceipts, Me.ToolStripMenuItem41, Me.mnuToolsReveAccounts})
        Me.mnuToolsReversals.Image = CType(resources.GetObject("mnuToolsReversals.Image"), System.Drawing.Image)
        Me.mnuToolsReversals.Name = "mnuToolsReversals"
        Me.mnuToolsReversals.Size = New System.Drawing.Size(208, 22)
        Me.mnuToolsReversals.Text = "Reversals"
        '
        'mnuToolsReversalsReceipts
        '
        Me.mnuToolsReversalsReceipts.Name = "mnuToolsReversalsReceipts"
        Me.mnuToolsReversalsReceipts.Size = New System.Drawing.Size(124, 22)
        Me.mnuToolsReversalsReceipts.Tag = "ReceiptReversals"
        Me.mnuToolsReversalsReceipts.Text = "Receipts"
        '
        'ToolStripMenuItem41
        '
        Me.ToolStripMenuItem41.Name = "ToolStripMenuItem41"
        Me.ToolStripMenuItem41.Size = New System.Drawing.Size(121, 6)
        '
        'mnuToolsReveAccounts
        '
        Me.mnuToolsReveAccounts.Name = "mnuToolsReveAccounts"
        Me.mnuToolsReveAccounts.Size = New System.Drawing.Size(124, 22)
        Me.mnuToolsReveAccounts.Tag = "AccountsEXT"
        Me.mnuToolsReveAccounts.Text = "Accounts"
        '
        'mnuToolsManageBillUnitPrice
        '
        Me.mnuToolsManageBillUnitPrice.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OutPatientToolStripMenuItem, Me.InPatientToolStripMenuItem})
        Me.mnuToolsManageBillUnitPrice.Enabled = False
        Me.mnuToolsManageBillUnitPrice.Image = CType(resources.GetObject("mnuToolsManageBillUnitPrice.Image"), System.Drawing.Image)
        Me.mnuToolsManageBillUnitPrice.Name = "mnuToolsManageBillUnitPrice"
        Me.mnuToolsManageBillUnitPrice.Size = New System.Drawing.Size(208, 22)
        Me.mnuToolsManageBillUnitPrice.Tag = "BillUnitPrice"
        Me.mnuToolsManageBillUnitPrice.Text = "Manage Bill Unit Price..."
        Me.mnuToolsManageBillUnitPrice.Visible = False
        '
        'OutPatientToolStripMenuItem
        '
        Me.OutPatientToolStripMenuItem.Name = "OutPatientToolStripMenuItem"
        Me.OutPatientToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.OutPatientToolStripMenuItem.Text = "Out Patient"
        '
        'InPatientToolStripMenuItem
        '
        Me.InPatientToolStripMenuItem.Name = "InPatientToolStripMenuItem"
        Me.InPatientToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.InPatientToolStripMenuItem.Text = "In Patient"
        '
        'MnuToolsSuspenseAccount
        '
        Me.MnuToolsSuspenseAccount.Image = CType(resources.GetObject("MnuToolsSuspenseAccount.Image"), System.Drawing.Image)
        Me.MnuToolsSuspenseAccount.Name = "MnuToolsSuspenseAccount"
        Me.MnuToolsSuspenseAccount.Size = New System.Drawing.Size(208, 22)
        Me.MnuToolsSuspenseAccount.Tag = "AccountTransferDetails"
        Me.MnuToolsSuspenseAccount.Text = "Suspense Account"
        '
        'mnuToolsOthers
        '
        Me.mnuToolsOthers.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuToolsOthersSagePastel, Me.mnuToolsOthersUnsentTextMessages, Me.SMSRemindersToolStripMenuItem})
        Me.mnuToolsOthers.Image = CType(resources.GetObject("mnuToolsOthers.Image"), System.Drawing.Image)
        Me.mnuToolsOthers.Name = "mnuToolsOthers"
        Me.mnuToolsOthers.Size = New System.Drawing.Size(208, 22)
        Me.mnuToolsOthers.Text = "&Others"
        '
        'mnuToolsOthersSagePastel
        '
        Me.mnuToolsOthersSagePastel.Name = "mnuToolsOthersSagePastel"
        Me.mnuToolsOthersSagePastel.Size = New System.Drawing.Size(189, 22)
        Me.mnuToolsOthersSagePastel.Text = "Sage Pastel"
        '
        'mnuToolsOthersUnsentTextMessages
        '
        Me.mnuToolsOthersUnsentTextMessages.Name = "mnuToolsOthersUnsentTextMessages"
        Me.mnuToolsOthersUnsentTextMessages.Size = New System.Drawing.Size(189, 22)
        Me.mnuToolsOthersUnsentTextMessages.Tag = "BulkMessaging"
        Me.mnuToolsOthersUnsentTextMessages.Text = "Unsent Text Messages"
        '
        'SMSRemindersToolStripMenuItem
        '
        Me.SMSRemindersToolStripMenuItem.Enabled = False
        Me.SMSRemindersToolStripMenuItem.Name = "SMSRemindersToolStripMenuItem"
        Me.SMSRemindersToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.SMSRemindersToolStripMenuItem.Text = "SMS Reminders"
        '
        'mnuToolsSeparator1
        '
        Me.mnuToolsSeparator1.Name = "mnuToolsSeparator1"
        Me.mnuToolsSeparator1.Size = New System.Drawing.Size(205, 6)
        '
        'mnuToolsNew
        '
        Me.mnuToolsNew.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuToolsNewServerCredentials, Me.mnuToolsNewTemplates, Me.mnuToolsNewImportDataInfo})
        Me.mnuToolsNew.Image = CType(resources.GetObject("mnuToolsNew.Image"), System.Drawing.Image)
        Me.mnuToolsNew.Name = "mnuToolsNew"
        Me.mnuToolsNew.Size = New System.Drawing.Size(208, 22)
        Me.mnuToolsNew.Text = "New"
        '
        'mnuToolsNewServerCredentials
        '
        Me.mnuToolsNewServerCredentials.Name = "mnuToolsNewServerCredentials"
        Me.mnuToolsNewServerCredentials.Size = New System.Drawing.Size(168, 22)
        Me.mnuToolsNewServerCredentials.Tag = "ServerCredentials"
        Me.mnuToolsNewServerCredentials.Text = "Server Credentials"
        '
        'mnuToolsNewTemplates
        '
        Me.mnuToolsNewTemplates.Name = "mnuToolsNewTemplates"
        Me.mnuToolsNewTemplates.Size = New System.Drawing.Size(168, 22)
        Me.mnuToolsNewTemplates.Tag = "Templates"
        Me.mnuToolsNewTemplates.Text = "Templates"
        '
        'mnuToolsNewImportDataInfo
        '
        Me.mnuToolsNewImportDataInfo.Name = "mnuToolsNewImportDataInfo"
        Me.mnuToolsNewImportDataInfo.Size = New System.Drawing.Size(168, 22)
        Me.mnuToolsNewImportDataInfo.Tag = "ImportDataInfo"
        Me.mnuToolsNewImportDataInfo.Text = "Import Data Info"
        '
        'mnuToolsEdit
        '
        Me.mnuToolsEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuToolsEditServerCredentials, Me.mnuToolsEditTemplates, Me.mnuToolsEditImportDataInfo})
        Me.mnuToolsEdit.Image = CType(resources.GetObject("mnuToolsEdit.Image"), System.Drawing.Image)
        Me.mnuToolsEdit.Name = "mnuToolsEdit"
        Me.mnuToolsEdit.Size = New System.Drawing.Size(208, 22)
        Me.mnuToolsEdit.Text = "Edit"
        '
        'mnuToolsEditServerCredentials
        '
        Me.mnuToolsEditServerCredentials.Name = "mnuToolsEditServerCredentials"
        Me.mnuToolsEditServerCredentials.Size = New System.Drawing.Size(168, 22)
        Me.mnuToolsEditServerCredentials.Tag = "ServerCredentials"
        Me.mnuToolsEditServerCredentials.Text = "Server Credentials"
        '
        'mnuToolsEditTemplates
        '
        Me.mnuToolsEditTemplates.Name = "mnuToolsEditTemplates"
        Me.mnuToolsEditTemplates.Size = New System.Drawing.Size(168, 22)
        Me.mnuToolsEditTemplates.Tag = "Templates"
        Me.mnuToolsEditTemplates.Text = "Templates"
        '
        'mnuToolsEditImportDataInfo
        '
        Me.mnuToolsEditImportDataInfo.Name = "mnuToolsEditImportDataInfo"
        Me.mnuToolsEditImportDataInfo.Size = New System.Drawing.Size(168, 22)
        Me.mnuToolsEditImportDataInfo.Tag = "ImportDataInfo"
        Me.mnuToolsEditImportDataInfo.Text = "Import Data Info"
        '
        'mnuToolsSeparator3
        '
        Me.mnuToolsSeparator3.Name = "mnuToolsSeparator3"
        Me.mnuToolsSeparator3.Size = New System.Drawing.Size(205, 6)
        '
        'mnuToolsOptions
        '
        Me.mnuToolsOptions.Image = CType(resources.GetObject("mnuToolsOptions.Image"), System.Drawing.Image)
        Me.mnuToolsOptions.Name = "mnuToolsOptions"
        Me.mnuToolsOptions.Size = New System.Drawing.Size(208, 22)
        Me.mnuToolsOptions.Tag = "Options"
        Me.mnuToolsOptions.Text = "&Options..."
        '
        'mnuWindow
        '
        Me.mnuWindow.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuWindowCascade, Me.mnuWindowTile, Me.mnuWindowArrangeIcons, Me.mnuWindowCloseAll})
        Me.mnuWindow.Name = "mnuWindow"
        Me.mnuWindow.Size = New System.Drawing.Size(63, 20)
        Me.mnuWindow.Text = "&Window"
        '
        'mnuWindowCascade
        '
        Me.mnuWindowCascade.Image = CType(resources.GetObject("mnuWindowCascade.Image"), System.Drawing.Image)
        Me.mnuWindowCascade.Name = "mnuWindowCascade"
        Me.mnuWindowCascade.Size = New System.Drawing.Size(147, 22)
        Me.mnuWindowCascade.Text = "Cascade"
        '
        'mnuWindowTile
        '
        Me.mnuWindowTile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuWindowTileHorizontal, Me.mnuWindowTileVertical})
        Me.mnuWindowTile.Name = "mnuWindowTile"
        Me.mnuWindowTile.Size = New System.Drawing.Size(147, 22)
        Me.mnuWindowTile.Text = "Tile"
        '
        'mnuWindowTileHorizontal
        '
        Me.mnuWindowTileHorizontal.Image = CType(resources.GetObject("mnuWindowTileHorizontal.Image"), System.Drawing.Image)
        Me.mnuWindowTileHorizontal.Name = "mnuWindowTileHorizontal"
        Me.mnuWindowTileHorizontal.Size = New System.Drawing.Size(129, 22)
        Me.mnuWindowTileHorizontal.Text = "Horizontal"
        '
        'mnuWindowTileVertical
        '
        Me.mnuWindowTileVertical.Image = CType(resources.GetObject("mnuWindowTileVertical.Image"), System.Drawing.Image)
        Me.mnuWindowTileVertical.Name = "mnuWindowTileVertical"
        Me.mnuWindowTileVertical.Size = New System.Drawing.Size(129, 22)
        Me.mnuWindowTileVertical.Text = "Vertical"
        '
        'mnuWindowArrangeIcons
        '
        Me.mnuWindowArrangeIcons.Name = "mnuWindowArrangeIcons"
        Me.mnuWindowArrangeIcons.Size = New System.Drawing.Size(147, 22)
        Me.mnuWindowArrangeIcons.Text = "Arrange Icons"
        '
        'mnuWindowCloseAll
        '
        Me.mnuWindowCloseAll.Name = "mnuWindowCloseAll"
        Me.mnuWindowCloseAll.Size = New System.Drawing.Size(147, 22)
        Me.mnuWindowCloseAll.Text = "Close All"
        '
        'mnuLocation
        '
        Me.mnuLocation.ForeColor = System.Drawing.Color.Red
        Me.mnuLocation.Name = "mnuLocation"
        Me.mnuLocation.Size = New System.Drawing.Size(65, 20)
        Me.mnuLocation.Text = "Location"
        '
        'mnuMessenger
        '
        Me.mnuMessenger.BackColor = System.Drawing.Color.Transparent
        Me.mnuMessenger.ForeColor = System.Drawing.Color.Red
        Me.mnuMessenger.Image = CType(resources.GetObject("mnuMessenger.Image"), System.Drawing.Image)
        Me.mnuMessenger.Name = "mnuMessenger"
        Me.mnuMessenger.ShortcutKeys = System.Windows.Forms.Keys.F12
        Me.mnuMessenger.Size = New System.Drawing.Size(74, 20)
        Me.mnuMessenger.Text = "&Chat(0)"
        '
        'mnuHelp
        '
        Me.mnuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuHelpHelpTopics, Me.mnuHelpSeparator, Me.mnuHelpAbout, Me.mnuHelpEULA})
        Me.mnuHelp.Name = "mnuHelp"
        Me.mnuHelp.Size = New System.Drawing.Size(44, 20)
        Me.mnuHelp.Text = "&Help"
        '
        'mnuHelpHelpTopics
        '
        Me.mnuHelpHelpTopics.Image = CType(resources.GetObject("mnuHelpHelpTopics.Image"), System.Drawing.Image)
        Me.mnuHelpHelpTopics.Name = "mnuHelpHelpTopics"
        Me.mnuHelpHelpTopics.ShortcutKeys = System.Windows.Forms.Keys.F1
        Me.mnuHelpHelpTopics.Size = New System.Drawing.Size(154, 22)
        Me.mnuHelpHelpTopics.Text = "Help Topics"
        '
        'mnuHelpSeparator
        '
        Me.mnuHelpSeparator.Name = "mnuHelpSeparator"
        Me.mnuHelpSeparator.Size = New System.Drawing.Size(151, 6)
        '
        'mnuHelpAbout
        '
        Me.mnuHelpAbout.Name = "mnuHelpAbout"
        Me.mnuHelpAbout.ShortcutKeys = System.Windows.Forms.Keys.F4
        Me.mnuHelpAbout.Size = New System.Drawing.Size(154, 22)
        Me.mnuHelpAbout.Text = "&About..."
        '
        'mnuHelpEULA
        '
        Me.mnuHelpEULA.Name = "mnuHelpEULA"
        Me.mnuHelpEULA.ShortcutKeys = System.Windows.Forms.Keys.F9
        Me.mnuHelpEULA.Size = New System.Drawing.Size(154, 22)
        Me.mnuHelpEULA.Text = "&EULA..."
        '
        'stbMain
        '
        Me.stbMain.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.stbMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.sbpLogin, Me.sbpLevel, Me.sbpServerName, Me.sbpConnectionMode, Me.sbpUserID, Me.sbpPoweredBy, Me.sbpReady})
        Me.stbMain.Location = New System.Drawing.Point(0, 456)
        Me.stbMain.Name = "stbMain"
        Me.stbMain.Size = New System.Drawing.Size(1155, 22)
        Me.stbMain.TabIndex = 5
        Me.stbMain.Text = "Main"
        '
        'sbpLogin
        '
        Me.sbpLogin.BackColor = System.Drawing.Color.Transparent
        Me.sbpLogin.Name = "sbpLogin"
        Me.sbpLogin.Size = New System.Drawing.Size(37, 17)
        Me.sbpLogin.Text = "Login"
        '
        'sbpLevel
        '
        Me.sbpLevel.BackColor = System.Drawing.Color.Transparent
        Me.sbpLevel.Name = "sbpLevel"
        Me.sbpLevel.Size = New System.Drawing.Size(34, 17)
        Me.sbpLevel.Text = "Level"
        '
        'sbpServerName
        '
        Me.sbpServerName.BackColor = System.Drawing.Color.Transparent
        Me.sbpServerName.Name = "sbpServerName"
        Me.sbpServerName.Size = New System.Drawing.Size(74, 17)
        Me.sbpServerName.Text = "Server Name"
        '
        'sbpConnectionMode
        '
        Me.sbpConnectionMode.BackColor = System.Drawing.Color.Transparent
        Me.sbpConnectionMode.Name = "sbpConnectionMode"
        Me.sbpConnectionMode.Size = New System.Drawing.Size(103, 17)
        Me.sbpConnectionMode.Text = "Connection Mode"
        '
        'sbpUserID
        '
        Me.sbpUserID.BackColor = System.Drawing.Color.Transparent
        Me.sbpUserID.Name = "sbpUserID"
        Me.sbpUserID.Size = New System.Drawing.Size(79, 17)
        Me.sbpUserID.Text = "Server User ID"
        '
        'sbpPoweredBy
        '
        Me.sbpPoweredBy.BackColor = System.Drawing.Color.Transparent
        Me.sbpPoweredBy.Name = "sbpPoweredBy"
        Me.sbpPoweredBy.Size = New System.Drawing.Size(69, 17)
        Me.sbpPoweredBy.Text = "Powered By"
        '
        'sbpReady
        '
        Me.sbpReady.BackColor = System.Drawing.Color.Transparent
        Me.sbpReady.Name = "sbpReady"
        Me.sbpReady.Size = New System.Drawing.Size(48, 17)
        Me.sbpReady.Text = "Ready..."
        '
        'tlbMain
        '
        Me.tlbMain.BackColor = System.Drawing.Color.WhiteSmoke
        Me.tlbMain.BackgroundImage = CType(resources.GetObject("tlbMain.BackgroundImage"), System.Drawing.Image)
        Me.tlbMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbPatients, Me.tbbSeparator1, Me.ddbVisits, Me.tbbSeparator2, Me.ddbExtras, Me.ToolStripSeparator2, Me.ddbTriage, Me.ToolStripSeparator7, Me.tbbCashier, Me.tbbSeparotor3, Me.ddbInvoices, Me.ToolStripSeparator12, Me.tbbDoctor, Me.tbbSeparator4, Me.ddbLaboratory, Me.tbbSeparator5, Me.ddbCardiology, Me.ToolStripSeparator19, Me.ddbRadiology, Me.ToolStripSeparator8, Me.ddbPharmacy, Me.tbbSeparator6, Me.ddbInventory, Me.ToolStripSeparator18, Me.ddbTheatreOperations, Me.tbbSeparator7, Me.ddbDental, Me.ToolStripSeparator9, Me.ddbAppointments, Me.tbbSeparator10, Me.ddbInPatients, Me.ToolStripSeparator17, Me.ToolStripSeparator3, Me.ddbFinances, Me.ddbManageART, Me.ToolStripSeparator15, Me.ddbDeaths})
        Me.tlbMain.Location = New System.Drawing.Point(0, 24)
        Me.tlbMain.Name = "tlbMain"
        Me.tlbMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlbMain.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlbMain.Size = New System.Drawing.Size(1155, 38)
        Me.tlbMain.TabIndex = 7
        Me.tlbMain.Text = "Main"
        '
        'ddbPatients
        '
        Me.ddbPatients.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiPatientsNew, Me.bmiPatientsEdit})
        Me.ddbPatients.Image = CType(resources.GetObject("ddbPatients.Image"), System.Drawing.Image)
        Me.ddbPatients.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbPatients.Name = "ddbPatients"
        Me.ddbPatients.Size = New System.Drawing.Size(62, 35)
        Me.ddbPatients.Tag = ""
        Me.ddbPatients.Text = "Patients"
        Me.ddbPatients.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiPatientsNew
        '
        Me.bmiPatientsNew.Image = CType(resources.GetObject("bmiPatientsNew.Image"), System.Drawing.Image)
        Me.bmiPatientsNew.Name = "bmiPatientsNew"
        Me.bmiPatientsNew.Size = New System.Drawing.Size(98, 22)
        Me.bmiPatientsNew.Tag = "Patients"
        Me.bmiPatientsNew.Text = "New"
        '
        'bmiPatientsEdit
        '
        Me.bmiPatientsEdit.Image = CType(resources.GetObject("bmiPatientsEdit.Image"), System.Drawing.Image)
        Me.bmiPatientsEdit.Name = "bmiPatientsEdit"
        Me.bmiPatientsEdit.Size = New System.Drawing.Size(98, 22)
        Me.bmiPatientsEdit.Tag = "Patients"
        Me.bmiPatientsEdit.Text = "Edit"
        '
        'tbbSeparator1
        '
        Me.tbbSeparator1.Name = "tbbSeparator1"
        Me.tbbSeparator1.Size = New System.Drawing.Size(6, 38)
        '
        'ddbVisits
        '
        Me.ddbVisits.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiVisitsNew, Me.bmiVisitsEdit})
        Me.ddbVisits.Image = CType(resources.GetObject("ddbVisits.Image"), System.Drawing.Image)
        Me.ddbVisits.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbVisits.Name = "ddbVisits"
        Me.ddbVisits.Size = New System.Drawing.Size(47, 35)
        Me.ddbVisits.Tag = ""
        Me.ddbVisits.Text = "Visits"
        Me.ddbVisits.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiVisitsNew
        '
        Me.bmiVisitsNew.Image = CType(resources.GetObject("bmiVisitsNew.Image"), System.Drawing.Image)
        Me.bmiVisitsNew.Name = "bmiVisitsNew"
        Me.bmiVisitsNew.Size = New System.Drawing.Size(98, 22)
        Me.bmiVisitsNew.Tag = "Visits"
        Me.bmiVisitsNew.Text = "New"
        '
        'bmiVisitsEdit
        '
        Me.bmiVisitsEdit.Image = CType(resources.GetObject("bmiVisitsEdit.Image"), System.Drawing.Image)
        Me.bmiVisitsEdit.Name = "bmiVisitsEdit"
        Me.bmiVisitsEdit.Size = New System.Drawing.Size(98, 22)
        Me.bmiVisitsEdit.Tag = "Visits"
        Me.bmiVisitsEdit.Text = "Edit"
        '
        'tbbSeparator2
        '
        Me.tbbSeparator2.Name = "tbbSeparator2"
        Me.tbbSeparator2.Size = New System.Drawing.Size(6, 38)
        '
        'ddbExtras
        '
        Me.ddbExtras.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiExtrasNew, Me.bmiExtrasEdit, Me.bmiExtrasExtraCharge, Me.mnuExtraAttachPackage, Me.bmiExtrasVisitFiles, Me.bmiCancerDiagnosis, Me.bmiHCTClientCard, Me.bmiExtrasAccessCashServices, Me.ClaimPaymentsToolStripMenuItem, Me.StaffPaymentApprovalsToolStripMenuItem, Me.mnuExtrasAntenatal, Me.mnuExtrasRefunds, Me.bmiExtrasOPDBillAdjustments, Me.mnuExtrasCodingMappings})
        Me.ddbExtras.Image = CType(resources.GetObject("ddbExtras.Image"), System.Drawing.Image)
        Me.ddbExtras.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbExtras.Name = "ddbExtras"
        Me.ddbExtras.Size = New System.Drawing.Size(51, 35)
        Me.ddbExtras.Tag = ""
        Me.ddbExtras.Text = "Extras"
        Me.ddbExtras.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiExtrasNew
        '
        Me.bmiExtrasNew.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiNewOPDExtraBills, Me.bmiExtrasNewSelfRequests, Me.bmiNewInventoryQuotation, Me.btnExtraNewEmergencyCase, Me.bmiExtrasNewARTPatient, Me.bmiExtrasNewPhysiotherapy, Me.bmiExtrasNewClients, Me.bmiExtrasNewClaims, Me.bmiExtrasNewOutwardFiles, Me.bmiExtrasNewInwardFiles, Me.bmiExtrasNewSmartCardAuthorisations, Me.bmiExtrasNewResearch, Me.bmiExtrasNewExternalReferralForm, Me.bmiExtrasNewAssetsRegister, Me.bmiExtrasNewAssetMaintainanceLog, Me.bmiExtrasNewSymptomsHistory, Me.IPDStaffPaymentsToolStripMenuItem, Me.OPDStaffPaymentsToolStripMenuItem, Me.btnExtraNewCodeMapping, Me.btnExtraNewImmunisation, Me.bmiExtrasNewServiceInvoices, Me.btnExtraNewOccupationalTherapy, Me.btnExtraNewPatientsEXT, Me.bmiTBIntensifiedCaseFindings, Me.bmiPatientRiskFactors})
        Me.bmiExtrasNew.Image = CType(resources.GetObject("bmiExtrasNew.Image"), System.Drawing.Image)
        Me.bmiExtrasNew.Name = "bmiExtrasNew"
        Me.bmiExtrasNew.Size = New System.Drawing.Size(204, 22)
        Me.bmiExtrasNew.Tag = ""
        Me.bmiExtrasNew.Text = "New"
        '
        'bmiNewOPDExtraBills
        '
        Me.bmiNewOPDExtraBills.Image = CType(resources.GetObject("bmiNewOPDExtraBills.Image"), System.Drawing.Image)
        Me.bmiNewOPDExtraBills.Name = "bmiNewOPDExtraBills"
        Me.bmiNewOPDExtraBills.Size = New System.Drawing.Size(221, 22)
        Me.bmiNewOPDExtraBills.Tag = "ExtraBills"
        Me.bmiNewOPDExtraBills.Text = "OPD Extra Bills"
        Me.bmiNewOPDExtraBills.Visible = False
        '
        'bmiExtrasNewSelfRequests
        '
        Me.bmiExtrasNewSelfRequests.Image = CType(resources.GetObject("bmiExtrasNewSelfRequests.Image"), System.Drawing.Image)
        Me.bmiExtrasNewSelfRequests.Name = "bmiExtrasNewSelfRequests"
        Me.bmiExtrasNewSelfRequests.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasNewSelfRequests.Tag = "SelfRequests"
        Me.bmiExtrasNewSelfRequests.Text = "Self Requests"
        '
        'bmiNewInventoryQuotation
        '
        Me.bmiNewInventoryQuotation.Image = CType(resources.GetObject("bmiNewInventoryQuotation.Image"), System.Drawing.Image)
        Me.bmiNewInventoryQuotation.Name = "bmiNewInventoryQuotation"
        Me.bmiNewInventoryQuotation.Size = New System.Drawing.Size(221, 22)
        Me.bmiNewInventoryQuotation.Tag = "Quotations"
        Me.bmiNewInventoryQuotation.Text = "Quotations"
        '
        'btnExtraNewEmergencyCase
        '
        Me.btnExtraNewEmergencyCase.Image = CType(resources.GetObject("btnExtraNewEmergencyCase.Image"), System.Drawing.Image)
        Me.btnExtraNewEmergencyCase.Name = "btnExtraNewEmergencyCase"
        Me.btnExtraNewEmergencyCase.Size = New System.Drawing.Size(221, 22)
        Me.btnExtraNewEmergencyCase.Tag = "Emergency"
        Me.btnExtraNewEmergencyCase.Text = "Emergency"
        '
        'bmiExtrasNewARTPatient
        '
        Me.bmiExtrasNewARTPatient.Image = CType(resources.GetObject("bmiExtrasNewARTPatient.Image"), System.Drawing.Image)
        Me.bmiExtrasNewARTPatient.Name = "bmiExtrasNewARTPatient"
        Me.bmiExtrasNewARTPatient.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasNewARTPatient.Tag = "HIVCARE"
        Me.bmiExtrasNewARTPatient.Text = "ART Patients"
        '
        'bmiExtrasNewPhysiotherapy
        '
        Me.bmiExtrasNewPhysiotherapy.Image = CType(resources.GetObject("bmiExtrasNewPhysiotherapy.Image"), System.Drawing.Image)
        Me.bmiExtrasNewPhysiotherapy.Name = "bmiExtrasNewPhysiotherapy"
        Me.bmiExtrasNewPhysiotherapy.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasNewPhysiotherapy.Text = "Physiotherapy"
        '
        'bmiExtrasNewClients
        '
        Me.bmiExtrasNewClients.Image = CType(resources.GetObject("bmiExtrasNewClients.Image"), System.Drawing.Image)
        Me.bmiExtrasNewClients.Name = "bmiExtrasNewClients"
        Me.bmiExtrasNewClients.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasNewClients.Tag = "Clients"
        Me.bmiExtrasNewClients.Text = "Clients"
        '
        'bmiExtrasNewClaims
        '
        Me.bmiExtrasNewClaims.Image = CType(resources.GetObject("bmiExtrasNewClaims.Image"), System.Drawing.Image)
        Me.bmiExtrasNewClaims.Name = "bmiExtrasNewClaims"
        Me.bmiExtrasNewClaims.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasNewClaims.Tag = "Claims"
        Me.bmiExtrasNewClaims.Text = "Claims"
        '
        'bmiExtrasNewOutwardFiles
        '
        Me.bmiExtrasNewOutwardFiles.Image = CType(resources.GetObject("bmiExtrasNewOutwardFiles.Image"), System.Drawing.Image)
        Me.bmiExtrasNewOutwardFiles.Name = "bmiExtrasNewOutwardFiles"
        Me.bmiExtrasNewOutwardFiles.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasNewOutwardFiles.Tag = "OutwardFiles"
        Me.bmiExtrasNewOutwardFiles.Text = "Outward Files"
        '
        'bmiExtrasNewInwardFiles
        '
        Me.bmiExtrasNewInwardFiles.Image = CType(resources.GetObject("bmiExtrasNewInwardFiles.Image"), System.Drawing.Image)
        Me.bmiExtrasNewInwardFiles.Name = "bmiExtrasNewInwardFiles"
        Me.bmiExtrasNewInwardFiles.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasNewInwardFiles.Tag = "InwardFiles"
        Me.bmiExtrasNewInwardFiles.Text = "Inward Files"
        '
        'bmiExtrasNewSmartCardAuthorisations
        '
        Me.bmiExtrasNewSmartCardAuthorisations.Image = CType(resources.GetObject("bmiExtrasNewSmartCardAuthorisations.Image"), System.Drawing.Image)
        Me.bmiExtrasNewSmartCardAuthorisations.Name = "bmiExtrasNewSmartCardAuthorisations"
        Me.bmiExtrasNewSmartCardAuthorisations.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasNewSmartCardAuthorisations.Tag = "SmartCardAuthorisations"
        Me.bmiExtrasNewSmartCardAuthorisations.Text = "Smart Card Authorisations"
        '
        'bmiExtrasNewResearch
        '
        Me.bmiExtrasNewResearch.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiExtrasNewResearchResearchRoutingForm, Me.ToolStripMenuItem9, Me.bmiExtrasNewResearchResearchEnrollmentInformation, Me.ToolStripMenuItem42, Me.bmiExtrasNewResearchResearchPatientsEnrollments, Me.ToolStripMenuItem47, Me.bmiExtrasNewResearchResearchPatientsEnd})
        Me.bmiExtrasNewResearch.Name = "bmiExtrasNewResearch"
        Me.bmiExtrasNewResearch.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasNewResearch.Tag = ""
        Me.bmiExtrasNewResearch.Text = "Research"
        '
        'bmiExtrasNewResearchResearchRoutingForm
        '
        Me.bmiExtrasNewResearchResearchRoutingForm.Name = "bmiExtrasNewResearchResearchRoutingForm"
        Me.bmiExtrasNewResearchResearchRoutingForm.Size = New System.Drawing.Size(248, 22)
        Me.bmiExtrasNewResearchResearchRoutingForm.Tag = "ResearchRoutingForm"
        Me.bmiExtrasNewResearchResearchRoutingForm.Text = "Research Routing Form"
        '
        'ToolStripMenuItem9
        '
        Me.ToolStripMenuItem9.Name = "ToolStripMenuItem9"
        Me.ToolStripMenuItem9.Size = New System.Drawing.Size(245, 6)
        '
        'bmiExtrasNewResearchResearchEnrollmentInformation
        '
        Me.bmiExtrasNewResearchResearchEnrollmentInformation.Name = "bmiExtrasNewResearchResearchEnrollmentInformation"
        Me.bmiExtrasNewResearchResearchEnrollmentInformation.Size = New System.Drawing.Size(248, 22)
        Me.bmiExtrasNewResearchResearchEnrollmentInformation.Tag = "EnrollmentInformation"
        Me.bmiExtrasNewResearchResearchEnrollmentInformation.Text = "Research Enrollment Information"
        '
        'ToolStripMenuItem42
        '
        Me.ToolStripMenuItem42.Name = "ToolStripMenuItem42"
        Me.ToolStripMenuItem42.Size = New System.Drawing.Size(245, 6)
        '
        'bmiExtrasNewResearchResearchPatientsEnrollments
        '
        Me.bmiExtrasNewResearchResearchPatientsEnrollments.Name = "bmiExtrasNewResearchResearchPatientsEnrollments"
        Me.bmiExtrasNewResearchResearchPatientsEnrollments.Size = New System.Drawing.Size(248, 22)
        Me.bmiExtrasNewResearchResearchPatientsEnrollments.Tag = "ResearchPatientsEnrollment"
        Me.bmiExtrasNewResearchResearchPatientsEnrollments.Text = "Research Patients Enrollments"
        '
        'ToolStripMenuItem47
        '
        Me.ToolStripMenuItem47.Name = "ToolStripMenuItem47"
        Me.ToolStripMenuItem47.Size = New System.Drawing.Size(245, 6)
        '
        'bmiExtrasNewResearchResearchPatientsEnd
        '
        Me.bmiExtrasNewResearchResearchPatientsEnd.Name = "bmiExtrasNewResearchResearchPatientsEnd"
        Me.bmiExtrasNewResearchResearchPatientsEnd.Size = New System.Drawing.Size(248, 22)
        Me.bmiExtrasNewResearchResearchPatientsEnd.Tag = "ResearchPatientsStop"
        Me.bmiExtrasNewResearchResearchPatientsEnd.Text = "Research Patients End"
        '
        'bmiExtrasNewExternalReferralForm
        '
        Me.bmiExtrasNewExternalReferralForm.Image = CType(resources.GetObject("bmiExtrasNewExternalReferralForm.Image"), System.Drawing.Image)
        Me.bmiExtrasNewExternalReferralForm.Name = "bmiExtrasNewExternalReferralForm"
        Me.bmiExtrasNewExternalReferralForm.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasNewExternalReferralForm.Tag = "ExternalReferrals"
        Me.bmiExtrasNewExternalReferralForm.Text = "External Referral Form "
        '
        'bmiExtrasNewAssetsRegister
        '
        Me.bmiExtrasNewAssetsRegister.Image = CType(resources.GetObject("bmiExtrasNewAssetsRegister.Image"), System.Drawing.Image)
        Me.bmiExtrasNewAssetsRegister.Name = "bmiExtrasNewAssetsRegister"
        Me.bmiExtrasNewAssetsRegister.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasNewAssetsRegister.Tag = "AssetRegister"
        Me.bmiExtrasNewAssetsRegister.Text = "Assets Register"
        '
        'bmiExtrasNewAssetMaintainanceLog
        '
        Me.bmiExtrasNewAssetMaintainanceLog.Image = CType(resources.GetObject("bmiExtrasNewAssetMaintainanceLog.Image"), System.Drawing.Image)
        Me.bmiExtrasNewAssetMaintainanceLog.Name = "bmiExtrasNewAssetMaintainanceLog"
        Me.bmiExtrasNewAssetMaintainanceLog.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasNewAssetMaintainanceLog.Tag = "AssetMaintainanceLog"
        Me.bmiExtrasNewAssetMaintainanceLog.Text = "Asset Maintainance Log"
        '
        'bmiExtrasNewSymptomsHistory
        '
        Me.bmiExtrasNewSymptomsHistory.Image = CType(resources.GetObject("bmiExtrasNewSymptomsHistory.Image"), System.Drawing.Image)
        Me.bmiExtrasNewSymptomsHistory.Name = "bmiExtrasNewSymptomsHistory"
        Me.bmiExtrasNewSymptomsHistory.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasNewSymptomsHistory.Tag = "SymptomsHistory"
        Me.bmiExtrasNewSymptomsHistory.Text = "Symptoms History"
        '
        'IPDStaffPaymentsToolStripMenuItem
        '
        Me.IPDStaffPaymentsToolStripMenuItem.Image = CType(resources.GetObject("IPDStaffPaymentsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.IPDStaffPaymentsToolStripMenuItem.Name = "IPDStaffPaymentsToolStripMenuItem"
        Me.IPDStaffPaymentsToolStripMenuItem.Size = New System.Drawing.Size(221, 22)
        Me.IPDStaffPaymentsToolStripMenuItem.Tag = "StaffPayments"
        Me.IPDStaffPaymentsToolStripMenuItem.Text = "IPD Staff Payments"
        '
        'OPDStaffPaymentsToolStripMenuItem
        '
        Me.OPDStaffPaymentsToolStripMenuItem.Image = CType(resources.GetObject("OPDStaffPaymentsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.OPDStaffPaymentsToolStripMenuItem.Name = "OPDStaffPaymentsToolStripMenuItem"
        Me.OPDStaffPaymentsToolStripMenuItem.Size = New System.Drawing.Size(221, 22)
        Me.OPDStaffPaymentsToolStripMenuItem.Tag = "StaffPayments"
        Me.OPDStaffPaymentsToolStripMenuItem.Text = "OPD Staff Payments"
        '
        'btnExtraNewCodeMapping
        '
        Me.btnExtraNewCodeMapping.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiExtrasNewCodeMappingBillCustomers, Me.bmiExtrasNewCodeMappingFinance})
        Me.btnExtraNewCodeMapping.Image = CType(resources.GetObject("btnExtraNewCodeMapping.Image"), System.Drawing.Image)
        Me.btnExtraNewCodeMapping.Name = "btnExtraNewCodeMapping"
        Me.btnExtraNewCodeMapping.Size = New System.Drawing.Size(221, 22)
        Me.btnExtraNewCodeMapping.Tag = "MappedCodes"
        Me.btnExtraNewCodeMapping.Text = "Code Mapping"
        '
        'bmiExtrasNewCodeMappingBillCustomers
        '
        Me.bmiExtrasNewCodeMappingBillCustomers.Name = "bmiExtrasNewCodeMappingBillCustomers"
        Me.bmiExtrasNewCodeMappingBillCustomers.Size = New System.Drawing.Size(150, 22)
        Me.bmiExtrasNewCodeMappingBillCustomers.Text = "Bill Customers"
        '
        'bmiExtrasNewCodeMappingFinance
        '
        Me.bmiExtrasNewCodeMappingFinance.Name = "bmiExtrasNewCodeMappingFinance"
        Me.bmiExtrasNewCodeMappingFinance.Size = New System.Drawing.Size(150, 22)
        Me.bmiExtrasNewCodeMappingFinance.Text = "Finance"
        '
        'btnExtraNewImmunisation
        '
        Me.btnExtraNewImmunisation.Checked = True
        Me.btnExtraNewImmunisation.CheckState = System.Windows.Forms.CheckState.Checked
        Me.btnExtraNewImmunisation.Image = CType(resources.GetObject("btnExtraNewImmunisation.Image"), System.Drawing.Image)
        Me.btnExtraNewImmunisation.Name = "btnExtraNewImmunisation"
        Me.btnExtraNewImmunisation.Size = New System.Drawing.Size(221, 22)
        Me.btnExtraNewImmunisation.Text = "Immunisation"
        '
        'bmiExtrasNewServiceInvoices
        '
        Me.bmiExtrasNewServiceInvoices.Image = CType(resources.GetObject("bmiExtrasNewServiceInvoices.Image"), System.Drawing.Image)
        Me.bmiExtrasNewServiceInvoices.Name = "bmiExtrasNewServiceInvoices"
        Me.bmiExtrasNewServiceInvoices.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasNewServiceInvoices.Tag = "ServiceInvoices"
        Me.bmiExtrasNewServiceInvoices.Text = "Service Invoices"
        '
        'btnExtraNewOccupationalTherapy
        '
        Me.btnExtraNewOccupationalTherapy.Image = CType(resources.GetObject("btnExtraNewOccupationalTherapy.Image"), System.Drawing.Image)
        Me.btnExtraNewOccupationalTherapy.Name = "btnExtraNewOccupationalTherapy"
        Me.btnExtraNewOccupationalTherapy.Size = New System.Drawing.Size(221, 22)
        Me.btnExtraNewOccupationalTherapy.Text = "Occupational Therapy"
        '
        'btnExtraNewPatientsEXT
        '
        Me.btnExtraNewPatientsEXT.Image = CType(resources.GetObject("btnExtraNewPatientsEXT.Image"), System.Drawing.Image)
        Me.btnExtraNewPatientsEXT.Name = "btnExtraNewPatientsEXT"
        Me.btnExtraNewPatientsEXT.Size = New System.Drawing.Size(221, 22)
        Me.btnExtraNewPatientsEXT.Tag = "PatientsEXT"
        Me.btnExtraNewPatientsEXT.Text = "Patients EXT"
        '
        'bmiTBIntensifiedCaseFindings
        '
        Me.bmiTBIntensifiedCaseFindings.Image = CType(resources.GetObject("bmiTBIntensifiedCaseFindings.Image"), System.Drawing.Image)
        Me.bmiTBIntensifiedCaseFindings.Name = "bmiTBIntensifiedCaseFindings"
        Me.bmiTBIntensifiedCaseFindings.Size = New System.Drawing.Size(221, 22)
        Me.bmiTBIntensifiedCaseFindings.Tag = "TBIntensifiedCaseFinding"
        Me.bmiTBIntensifiedCaseFindings.Text = "TB Intensified Case Findings"
        '
        'bmiPatientRiskFactors
        '
        Me.bmiPatientRiskFactors.Image = CType(resources.GetObject("bmiPatientRiskFactors.Image"), System.Drawing.Image)
        Me.bmiPatientRiskFactors.Name = "bmiPatientRiskFactors"
        Me.bmiPatientRiskFactors.Size = New System.Drawing.Size(221, 22)
        Me.bmiPatientRiskFactors.Tag = "PatientRisks"
        Me.bmiPatientRiskFactors.Text = "Patient Risk Factors"
        '
        'bmiExtrasEdit
        '
        Me.bmiExtrasEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiExtrasEditSelfRequests, Me.bmiEditInventoryQuotation, Me.bmiExtrasEditARTPatient, Me.bmiExtrasEditPhysiotherapy, Me.bmiExtrasEditClients, Me.bmiExtrasEditClaims, Me.bmiExtrasEditOutwardFiles, Me.bmiExtrasEditInwardFiles, Me.bmiExtrasEditSmartCardAuthorisations, Me.bmiExtrasEditResearchRoutingForm, Me.bmiExtrasEditExternalReferralForm, Me.bmiExtrasEditAssetsRegister, Me.bmiExtrasEditAssetMaintainanceLog, Me.bmiExtrasEditSymptomsHistory, Me.IPDStaffPaymentsToolStripMenuItem1, Me.OPDStaffPaymentsToolStripMenuItem1, Me.btnExtraEditCodeMapping, Me.btnExtraEditImmunisation, Me.bmiExtrasEditServiceInvoices, Me.bmiExtrasEditMaternity, Me.bmiExtrasEditTBIntensifiedCaseFindings, Me.bmiEditPatientRiskFactors})
        Me.bmiExtrasEdit.Image = CType(resources.GetObject("bmiExtrasEdit.Image"), System.Drawing.Image)
        Me.bmiExtrasEdit.Name = "bmiExtrasEdit"
        Me.bmiExtrasEdit.Size = New System.Drawing.Size(204, 22)
        Me.bmiExtrasEdit.Tag = ""
        Me.bmiExtrasEdit.Text = "Edit"
        '
        'bmiExtrasEditSelfRequests
        '
        Me.bmiExtrasEditSelfRequests.Image = CType(resources.GetObject("bmiExtrasEditSelfRequests.Image"), System.Drawing.Image)
        Me.bmiExtrasEditSelfRequests.Name = "bmiExtrasEditSelfRequests"
        Me.bmiExtrasEditSelfRequests.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditSelfRequests.Tag = "SelfRequests"
        Me.bmiExtrasEditSelfRequests.Text = "Self Requests"
        '
        'bmiEditInventoryQuotation
        '
        Me.bmiEditInventoryQuotation.Image = CType(resources.GetObject("bmiEditInventoryQuotation.Image"), System.Drawing.Image)
        Me.bmiEditInventoryQuotation.Name = "bmiEditInventoryQuotation"
        Me.bmiEditInventoryQuotation.Size = New System.Drawing.Size(221, 22)
        Me.bmiEditInventoryQuotation.Tag = "Quotations"
        Me.bmiEditInventoryQuotation.Text = "Quotations"
        '
        'bmiExtrasEditARTPatient
        '
        Me.bmiExtrasEditARTPatient.Image = CType(resources.GetObject("bmiExtrasEditARTPatient.Image"), System.Drawing.Image)
        Me.bmiExtrasEditARTPatient.Name = "bmiExtrasEditARTPatient"
        Me.bmiExtrasEditARTPatient.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditARTPatient.Tag = "HIVCARE"
        Me.bmiExtrasEditARTPatient.Text = "ART Patients"
        '
        'bmiExtrasEditPhysiotherapy
        '
        Me.bmiExtrasEditPhysiotherapy.Image = CType(resources.GetObject("bmiExtrasEditPhysiotherapy.Image"), System.Drawing.Image)
        Me.bmiExtrasEditPhysiotherapy.Name = "bmiExtrasEditPhysiotherapy"
        Me.bmiExtrasEditPhysiotherapy.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditPhysiotherapy.Text = "Physiotherapy"
        '
        'bmiExtrasEditClients
        '
        Me.bmiExtrasEditClients.Image = CType(resources.GetObject("bmiExtrasEditClients.Image"), System.Drawing.Image)
        Me.bmiExtrasEditClients.Name = "bmiExtrasEditClients"
        Me.bmiExtrasEditClients.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditClients.Tag = "Clients"
        Me.bmiExtrasEditClients.Text = "Clients"
        '
        'bmiExtrasEditClaims
        '
        Me.bmiExtrasEditClaims.Image = CType(resources.GetObject("bmiExtrasEditClaims.Image"), System.Drawing.Image)
        Me.bmiExtrasEditClaims.Name = "bmiExtrasEditClaims"
        Me.bmiExtrasEditClaims.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditClaims.Tag = "Claims"
        Me.bmiExtrasEditClaims.Text = "Claims"
        '
        'bmiExtrasEditOutwardFiles
        '
        Me.bmiExtrasEditOutwardFiles.Image = CType(resources.GetObject("bmiExtrasEditOutwardFiles.Image"), System.Drawing.Image)
        Me.bmiExtrasEditOutwardFiles.Name = "bmiExtrasEditOutwardFiles"
        Me.bmiExtrasEditOutwardFiles.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditOutwardFiles.Tag = "OutwardFiles"
        Me.bmiExtrasEditOutwardFiles.Text = "Outward Files"
        '
        'bmiExtrasEditInwardFiles
        '
        Me.bmiExtrasEditInwardFiles.Image = CType(resources.GetObject("bmiExtrasEditInwardFiles.Image"), System.Drawing.Image)
        Me.bmiExtrasEditInwardFiles.Name = "bmiExtrasEditInwardFiles"
        Me.bmiExtrasEditInwardFiles.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditInwardFiles.Tag = "InwardFiles"
        Me.bmiExtrasEditInwardFiles.Text = "Inward Files"
        '
        'bmiExtrasEditSmartCardAuthorisations
        '
        Me.bmiExtrasEditSmartCardAuthorisations.Image = CType(resources.GetObject("bmiExtrasEditSmartCardAuthorisations.Image"), System.Drawing.Image)
        Me.bmiExtrasEditSmartCardAuthorisations.Name = "bmiExtrasEditSmartCardAuthorisations"
        Me.bmiExtrasEditSmartCardAuthorisations.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditSmartCardAuthorisations.Tag = "SmartCardAuthorisations"
        Me.bmiExtrasEditSmartCardAuthorisations.Text = "Smart Card Authorisations"
        '
        'bmiExtrasEditResearchRoutingForm
        '
        Me.bmiExtrasEditResearchRoutingForm.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiExtrasEditResearchResearchRoutingForm, Me.ToolStripMenuItem45, Me.bmiExtrasEditResearchResearchEnrollmentInformation, Me.ToolStripMenuItem46, Me.bmiExtraResearchEditResearchPatientsEnrollments})
        Me.bmiExtrasEditResearchRoutingForm.Image = CType(resources.GetObject("bmiExtrasEditResearchRoutingForm.Image"), System.Drawing.Image)
        Me.bmiExtrasEditResearchRoutingForm.Name = "bmiExtrasEditResearchRoutingForm"
        Me.bmiExtrasEditResearchRoutingForm.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditResearchRoutingForm.Text = "Research"
        '
        'bmiExtrasEditResearchResearchRoutingForm
        '
        Me.bmiExtrasEditResearchResearchRoutingForm.Name = "bmiExtrasEditResearchResearchRoutingForm"
        Me.bmiExtrasEditResearchResearchRoutingForm.Size = New System.Drawing.Size(248, 22)
        Me.bmiExtrasEditResearchResearchRoutingForm.Tag = "ResearchRoutingForm"
        Me.bmiExtrasEditResearchResearchRoutingForm.Text = "Research Routing Form"
        '
        'ToolStripMenuItem45
        '
        Me.ToolStripMenuItem45.Name = "ToolStripMenuItem45"
        Me.ToolStripMenuItem45.Size = New System.Drawing.Size(245, 6)
        '
        'bmiExtrasEditResearchResearchEnrollmentInformation
        '
        Me.bmiExtrasEditResearchResearchEnrollmentInformation.Name = "bmiExtrasEditResearchResearchEnrollmentInformation"
        Me.bmiExtrasEditResearchResearchEnrollmentInformation.Size = New System.Drawing.Size(248, 22)
        Me.bmiExtrasEditResearchResearchEnrollmentInformation.Tag = "EnrollmentInformation"
        Me.bmiExtrasEditResearchResearchEnrollmentInformation.Text = "Research Enrollment Information"
        '
        'ToolStripMenuItem46
        '
        Me.ToolStripMenuItem46.Name = "ToolStripMenuItem46"
        Me.ToolStripMenuItem46.Size = New System.Drawing.Size(245, 6)
        '
        'bmiExtraResearchEditResearchPatientsEnrollments
        '
        Me.bmiExtraResearchEditResearchPatientsEnrollments.Name = "bmiExtraResearchEditResearchPatientsEnrollments"
        Me.bmiExtraResearchEditResearchPatientsEnrollments.Size = New System.Drawing.Size(248, 22)
        Me.bmiExtraResearchEditResearchPatientsEnrollments.Tag = "ResearchPatientsEnrollment"
        Me.bmiExtraResearchEditResearchPatientsEnrollments.Text = "Research Patients Enrollments"
        '
        'bmiExtrasEditExternalReferralForm
        '
        Me.bmiExtrasEditExternalReferralForm.Image = CType(resources.GetObject("bmiExtrasEditExternalReferralForm.Image"), System.Drawing.Image)
        Me.bmiExtrasEditExternalReferralForm.Name = "bmiExtrasEditExternalReferralForm"
        Me.bmiExtrasEditExternalReferralForm.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditExternalReferralForm.Tag = "ExternalReferrals"
        Me.bmiExtrasEditExternalReferralForm.Text = "External Referral Form "
        '
        'bmiExtrasEditAssetsRegister
        '
        Me.bmiExtrasEditAssetsRegister.Image = CType(resources.GetObject("bmiExtrasEditAssetsRegister.Image"), System.Drawing.Image)
        Me.bmiExtrasEditAssetsRegister.Name = "bmiExtrasEditAssetsRegister"
        Me.bmiExtrasEditAssetsRegister.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditAssetsRegister.Tag = "AssetRegister"
        Me.bmiExtrasEditAssetsRegister.Text = "Assets Register"
        '
        'bmiExtrasEditAssetMaintainanceLog
        '
        Me.bmiExtrasEditAssetMaintainanceLog.Image = CType(resources.GetObject("bmiExtrasEditAssetMaintainanceLog.Image"), System.Drawing.Image)
        Me.bmiExtrasEditAssetMaintainanceLog.Name = "bmiExtrasEditAssetMaintainanceLog"
        Me.bmiExtrasEditAssetMaintainanceLog.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditAssetMaintainanceLog.Tag = "AssetMaintainanceLog"
        Me.bmiExtrasEditAssetMaintainanceLog.Text = "Asset Maintainance Log"
        '
        'bmiExtrasEditSymptomsHistory
        '
        Me.bmiExtrasEditSymptomsHistory.Image = CType(resources.GetObject("bmiExtrasEditSymptomsHistory.Image"), System.Drawing.Image)
        Me.bmiExtrasEditSymptomsHistory.Name = "bmiExtrasEditSymptomsHistory"
        Me.bmiExtrasEditSymptomsHistory.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditSymptomsHistory.Tag = "SymptomsHistory"
        Me.bmiExtrasEditSymptomsHistory.Text = "Symptoms History"
        '
        'IPDStaffPaymentsToolStripMenuItem1
        '
        Me.IPDStaffPaymentsToolStripMenuItem1.Image = CType(resources.GetObject("IPDStaffPaymentsToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.IPDStaffPaymentsToolStripMenuItem1.Name = "IPDStaffPaymentsToolStripMenuItem1"
        Me.IPDStaffPaymentsToolStripMenuItem1.Size = New System.Drawing.Size(221, 22)
        Me.IPDStaffPaymentsToolStripMenuItem1.Tag = "StaffPayments"
        Me.IPDStaffPaymentsToolStripMenuItem1.Text = "IPD Staff Payments"
        '
        'OPDStaffPaymentsToolStripMenuItem1
        '
        Me.OPDStaffPaymentsToolStripMenuItem1.Image = CType(resources.GetObject("OPDStaffPaymentsToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.OPDStaffPaymentsToolStripMenuItem1.Name = "OPDStaffPaymentsToolStripMenuItem1"
        Me.OPDStaffPaymentsToolStripMenuItem1.Size = New System.Drawing.Size(221, 22)
        Me.OPDStaffPaymentsToolStripMenuItem1.Tag = "StaffPayments"
        Me.OPDStaffPaymentsToolStripMenuItem1.Text = "OPD Staff Payments"
        '
        'btnExtraEditCodeMapping
        '
        Me.btnExtraEditCodeMapping.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnExtraEditCodeMappingBillCustomers, Me.btnExtraEditCodeMappingFinance, Me.btnExtraEditCodeMappingBillableMappings})
        Me.btnExtraEditCodeMapping.Image = CType(resources.GetObject("btnExtraEditCodeMapping.Image"), System.Drawing.Image)
        Me.btnExtraEditCodeMapping.Name = "btnExtraEditCodeMapping"
        Me.btnExtraEditCodeMapping.Size = New System.Drawing.Size(221, 22)
        Me.btnExtraEditCodeMapping.Tag = "MappedCodes"
        Me.btnExtraEditCodeMapping.Text = "Code Mapping"
        '
        'btnExtraEditCodeMappingBillCustomers
        '
        Me.btnExtraEditCodeMappingBillCustomers.Name = "btnExtraEditCodeMappingBillCustomers"
        Me.btnExtraEditCodeMappingBillCustomers.Size = New System.Drawing.Size(168, 22)
        Me.btnExtraEditCodeMappingBillCustomers.Text = "Bill Customers"
        '
        'btnExtraEditCodeMappingFinance
        '
        Me.btnExtraEditCodeMappingFinance.Name = "btnExtraEditCodeMappingFinance"
        Me.btnExtraEditCodeMappingFinance.Size = New System.Drawing.Size(168, 22)
        Me.btnExtraEditCodeMappingFinance.Text = "Finance"
        '
        'btnExtraEditCodeMappingBillableMappings
        '
        Me.btnExtraEditCodeMappingBillableMappings.Name = "btnExtraEditCodeMappingBillableMappings"
        Me.btnExtraEditCodeMappingBillableMappings.Size = New System.Drawing.Size(168, 22)
        Me.btnExtraEditCodeMappingBillableMappings.Text = "Billable Mappings"
        '
        'btnExtraEditImmunisation
        '
        Me.btnExtraEditImmunisation.Image = CType(resources.GetObject("btnExtraEditImmunisation.Image"), System.Drawing.Image)
        Me.btnExtraEditImmunisation.Name = "btnExtraEditImmunisation"
        Me.btnExtraEditImmunisation.Size = New System.Drawing.Size(221, 22)
        Me.btnExtraEditImmunisation.Text = "Immunisation"
        '
        'bmiExtrasEditServiceInvoices
        '
        Me.bmiExtrasEditServiceInvoices.Image = CType(resources.GetObject("bmiExtrasEditServiceInvoices.Image"), System.Drawing.Image)
        Me.bmiExtrasEditServiceInvoices.Name = "bmiExtrasEditServiceInvoices"
        Me.bmiExtrasEditServiceInvoices.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditServiceInvoices.Tag = "ServiceInvoices"
        Me.bmiExtrasEditServiceInvoices.Text = "Service Invoices"
        '
        'bmiExtrasEditMaternity
        '
        Me.bmiExtrasEditMaternity.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiExtraEditMaternityEnrollment, Me.bmiExtraEditMaternityAntenatalVisits})
        Me.bmiExtrasEditMaternity.Image = CType(resources.GetObject("bmiExtrasEditMaternity.Image"), System.Drawing.Image)
        Me.bmiExtrasEditMaternity.Name = "bmiExtrasEditMaternity"
        Me.bmiExtrasEditMaternity.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditMaternity.Text = "Maternity"
        '
        'bmiExtraEditMaternityEnrollment
        '
        Me.bmiExtraEditMaternityEnrollment.Name = "bmiExtraEditMaternityEnrollment"
        Me.bmiExtraEditMaternityEnrollment.Size = New System.Drawing.Size(182, 22)
        Me.bmiExtraEditMaternityEnrollment.Text = "Maternal Enrollment"
        '
        'bmiExtraEditMaternityAntenatalVisits
        '
        Me.bmiExtraEditMaternityAntenatalVisits.Name = "bmiExtraEditMaternityAntenatalVisits"
        Me.bmiExtraEditMaternityAntenatalVisits.Size = New System.Drawing.Size(182, 22)
        Me.bmiExtraEditMaternityAntenatalVisits.Text = "Antenatal Visits"
        '
        'bmiExtrasEditTBIntensifiedCaseFindings
        '
        Me.bmiExtrasEditTBIntensifiedCaseFindings.Image = CType(resources.GetObject("bmiExtrasEditTBIntensifiedCaseFindings.Image"), System.Drawing.Image)
        Me.bmiExtrasEditTBIntensifiedCaseFindings.Name = "bmiExtrasEditTBIntensifiedCaseFindings"
        Me.bmiExtrasEditTBIntensifiedCaseFindings.Size = New System.Drawing.Size(221, 22)
        Me.bmiExtrasEditTBIntensifiedCaseFindings.Tag = "TBIntensifiedCaseFinding"
        Me.bmiExtrasEditTBIntensifiedCaseFindings.Text = "TB Intensified Case Findings"
        '
        'bmiEditPatientRiskFactors
        '
        Me.bmiEditPatientRiskFactors.Image = CType(resources.GetObject("bmiEditPatientRiskFactors.Image"), System.Drawing.Image)
        Me.bmiEditPatientRiskFactors.Name = "bmiEditPatientRiskFactors"
        Me.bmiEditPatientRiskFactors.Size = New System.Drawing.Size(221, 22)
        Me.bmiEditPatientRiskFactors.Tag = "PatientRisks"
        Me.bmiEditPatientRiskFactors.Text = "Patient Risk Factors"
        '
        'bmiExtrasExtraCharge
        '
        Me.bmiExtrasExtraCharge.Image = CType(resources.GetObject("bmiExtrasExtraCharge.Image"), System.Drawing.Image)
        Me.bmiExtrasExtraCharge.Name = "bmiExtrasExtraCharge"
        Me.bmiExtrasExtraCharge.Size = New System.Drawing.Size(204, 22)
        Me.bmiExtrasExtraCharge.Tag = "ExtraCharge"
        Me.bmiExtrasExtraCharge.Text = "Extra Charge"
        '
        'mnuExtraAttachPackage
        '
        Me.mnuExtraAttachPackage.Image = CType(resources.GetObject("mnuExtraAttachPackage.Image"), System.Drawing.Image)
        Me.mnuExtraAttachPackage.Name = "mnuExtraAttachPackage"
        Me.mnuExtraAttachPackage.Size = New System.Drawing.Size(204, 22)
        Me.mnuExtraAttachPackage.Tag = "AttachPackage"
        Me.mnuExtraAttachPackage.Text = "Attach Package"
        '
        'bmiExtrasVisitFiles
        '
        Me.bmiExtrasVisitFiles.Image = CType(resources.GetObject("bmiExtrasVisitFiles.Image"), System.Drawing.Image)
        Me.bmiExtrasVisitFiles.Name = "bmiExtrasVisitFiles"
        Me.bmiExtrasVisitFiles.Size = New System.Drawing.Size(204, 22)
        Me.bmiExtrasVisitFiles.Tag = "VisitFiles"
        Me.bmiExtrasVisitFiles.Text = "Visit Files"
        '
        'bmiCancerDiagnosis
        '
        Me.bmiCancerDiagnosis.Image = CType(resources.GetObject("bmiCancerDiagnosis.Image"), System.Drawing.Image)
        Me.bmiCancerDiagnosis.Name = "bmiCancerDiagnosis"
        Me.bmiCancerDiagnosis.Size = New System.Drawing.Size(204, 22)
        Me.bmiCancerDiagnosis.Tag = "CancerDiagnosis"
        Me.bmiCancerDiagnosis.Text = "Cancer Diagnosis"
        '
        'bmiHCTClientCard
        '
        Me.bmiHCTClientCard.Image = CType(resources.GetObject("bmiHCTClientCard.Image"), System.Drawing.Image)
        Me.bmiHCTClientCard.Name = "bmiHCTClientCard"
        Me.bmiHCTClientCard.Size = New System.Drawing.Size(204, 22)
        Me.bmiHCTClientCard.Tag = "HCTClientCard"
        Me.bmiHCTClientCard.Text = "HCT Client Card"
        '
        'bmiExtrasAccessCashServices
        '
        Me.bmiExtrasAccessCashServices.Image = CType(resources.GetObject("bmiExtrasAccessCashServices.Image"), System.Drawing.Image)
        Me.bmiExtrasAccessCashServices.Name = "bmiExtrasAccessCashServices"
        Me.bmiExtrasAccessCashServices.Size = New System.Drawing.Size(204, 22)
        Me.bmiExtrasAccessCashServices.Tag = "AccessedCashServices"
        Me.bmiExtrasAccessCashServices.Text = "Access Cash Services"
        '
        'ClaimPaymentsToolStripMenuItem
        '
        Me.ClaimPaymentsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClaimPaymentsToolStripMenuItem1, Me.ClaimPaymentDetailedToolStripMenuItem})
        Me.ClaimPaymentsToolStripMenuItem.Image = CType(resources.GetObject("ClaimPaymentsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ClaimPaymentsToolStripMenuItem.Name = "ClaimPaymentsToolStripMenuItem"
        Me.ClaimPaymentsToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.ClaimPaymentsToolStripMenuItem.Tag = ""
        Me.ClaimPaymentsToolStripMenuItem.Text = "Claim Payments"
        '
        'ClaimPaymentsToolStripMenuItem1
        '
        Me.ClaimPaymentsToolStripMenuItem1.Name = "ClaimPaymentsToolStripMenuItem1"
        Me.ClaimPaymentsToolStripMenuItem1.Size = New System.Drawing.Size(201, 22)
        Me.ClaimPaymentsToolStripMenuItem1.Tag = "ClaimPayments"
        Me.ClaimPaymentsToolStripMenuItem1.Text = "Claim Payments"
        '
        'ClaimPaymentDetailedToolStripMenuItem
        '
        Me.ClaimPaymentDetailedToolStripMenuItem.Name = "ClaimPaymentDetailedToolStripMenuItem"
        Me.ClaimPaymentDetailedToolStripMenuItem.Size = New System.Drawing.Size(201, 22)
        Me.ClaimPaymentDetailedToolStripMenuItem.Tag = "ClaimDetails"
        Me.ClaimPaymentDetailedToolStripMenuItem.Text = "Claim Payment Detailed"
        '
        'StaffPaymentApprovalsToolStripMenuItem
        '
        Me.StaffPaymentApprovalsToolStripMenuItem.Image = CType(resources.GetObject("StaffPaymentApprovalsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.StaffPaymentApprovalsToolStripMenuItem.Name = "StaffPaymentApprovalsToolStripMenuItem"
        Me.StaffPaymentApprovalsToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.StaffPaymentApprovalsToolStripMenuItem.Tag = "StaffPayments"
        Me.StaffPaymentApprovalsToolStripMenuItem.Text = "Staff Payment Approvals"
        '
        'mnuExtrasAntenatal
        '
        Me.mnuExtrasAntenatal.Checked = True
        Me.mnuExtrasAntenatal.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuExtrasAntenatal.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExtrasAntenatalnrollment, Me.mnuExtrasAntenatalVisit})
        Me.mnuExtrasAntenatal.Image = CType(resources.GetObject("mnuExtrasAntenatal.Image"), System.Drawing.Image)
        Me.mnuExtrasAntenatal.Name = "mnuExtrasAntenatal"
        Me.mnuExtrasAntenatal.Size = New System.Drawing.Size(204, 22)
        Me.mnuExtrasAntenatal.Tag = "AntenatalEnrollment"
        Me.mnuExtrasAntenatal.Text = "Antenatal"
        '
        'mnuExtrasAntenatalnrollment
        '
        Me.mnuExtrasAntenatalnrollment.Name = "mnuExtrasAntenatalnrollment"
        Me.mnuExtrasAntenatalnrollment.Size = New System.Drawing.Size(155, 22)
        Me.mnuExtrasAntenatalnrollment.Tag = "AntenatalEnrollment"
        Me.mnuExtrasAntenatalnrollment.Text = "Enrollment"
        '
        'mnuExtrasAntenatalVisit
        '
        Me.mnuExtrasAntenatalVisit.Name = "mnuExtrasAntenatalVisit"
        Me.mnuExtrasAntenatalVisit.Size = New System.Drawing.Size(155, 22)
        Me.mnuExtrasAntenatalVisit.Tag = "AntenatalVisits"
        Me.mnuExtrasAntenatalVisit.Text = "Antenatal Visits"
        '
        'mnuExtrasRefunds
        '
        Me.mnuExtrasRefunds.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExtrasRefundsRequests, Me.mnuExtrasRefundApprovals})
        Me.mnuExtrasRefunds.Image = CType(resources.GetObject("mnuExtrasRefunds.Image"), System.Drawing.Image)
        Me.mnuExtrasRefunds.Name = "mnuExtrasRefunds"
        Me.mnuExtrasRefunds.Size = New System.Drawing.Size(204, 22)
        Me.mnuExtrasRefunds.Text = "Refunds"
        '
        'mnuExtrasRefundsRequests
        '
        Me.mnuExtrasRefundsRequests.Name = "mnuExtrasRefundsRequests"
        Me.mnuExtrasRefundsRequests.Size = New System.Drawing.Size(127, 22)
        Me.mnuExtrasRefundsRequests.Tag = "RefundRequests"
        Me.mnuExtrasRefundsRequests.Text = "Requests"
        '
        'mnuExtrasRefundApprovals
        '
        Me.mnuExtrasRefundApprovals.Name = "mnuExtrasRefundApprovals"
        Me.mnuExtrasRefundApprovals.Size = New System.Drawing.Size(127, 22)
        Me.mnuExtrasRefundApprovals.Tag = "RefundApprovals"
        Me.mnuExtrasRefundApprovals.Text = "Approvals"
        '
        'bmiExtrasOPDBillAdjustments
        '
        Me.bmiExtrasOPDBillAdjustments.Image = CType(resources.GetObject("bmiExtrasOPDBillAdjustments.Image"), System.Drawing.Image)
        Me.bmiExtrasOPDBillAdjustments.Name = "bmiExtrasOPDBillAdjustments"
        Me.bmiExtrasOPDBillAdjustments.Size = New System.Drawing.Size(204, 22)
        Me.bmiExtrasOPDBillAdjustments.Tag = "ItemAdjustments"
        Me.bmiExtrasOPDBillAdjustments.Text = "OPD Bill Adjustments"
        '
        'mnuExtrasCodingMappings
        '
        Me.mnuExtrasCodingMappings.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExtrasCodingMappingLookupData, Me.mnuExtrasCodingMappingBillableMappings, Me.mnuExtrasCodingMappingCompanies})
        Me.mnuExtrasCodingMappings.Image = CType(resources.GetObject("mnuExtrasCodingMappings.Image"), System.Drawing.Image)
        Me.mnuExtrasCodingMappings.Name = "mnuExtrasCodingMappings"
        Me.mnuExtrasCodingMappings.Size = New System.Drawing.Size(204, 22)
        Me.mnuExtrasCodingMappings.Tag = "MappedCodes"
        Me.mnuExtrasCodingMappings.Text = "INT Code Mapping"
        '
        'mnuExtrasCodingMappingLookupData
        '
        Me.mnuExtrasCodingMappingLookupData.Name = "mnuExtrasCodingMappingLookupData"
        Me.mnuExtrasCodingMappingLookupData.Size = New System.Drawing.Size(168, 22)
        Me.mnuExtrasCodingMappingLookupData.Text = "Lookup Data"
        '
        'mnuExtrasCodingMappingBillableMappings
        '
        Me.mnuExtrasCodingMappingBillableMappings.Name = "mnuExtrasCodingMappingBillableMappings"
        Me.mnuExtrasCodingMappingBillableMappings.Size = New System.Drawing.Size(168, 22)
        Me.mnuExtrasCodingMappingBillableMappings.Text = "Billable Mappings"
        '
        'mnuExtrasCodingMappingCompanies
        '
        Me.mnuExtrasCodingMappingCompanies.Name = "mnuExtrasCodingMappingCompanies"
        Me.mnuExtrasCodingMappingCompanies.Size = New System.Drawing.Size(168, 22)
        Me.mnuExtrasCodingMappingCompanies.Text = "Companies"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 38)
        '
        'ddbTriage
        '
        Me.ddbTriage.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiTriageNew, Me.bmiTriageEdit})
        Me.ddbTriage.Image = CType(resources.GetObject("ddbTriage.Image"), System.Drawing.Image)
        Me.ddbTriage.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbTriage.Name = "ddbTriage"
        Me.ddbTriage.Size = New System.Drawing.Size(51, 35)
        Me.ddbTriage.Tag = ""
        Me.ddbTriage.Text = "Triage"
        Me.ddbTriage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiTriageNew
        '
        Me.bmiTriageNew.AccessibleDescription = ""
        Me.bmiTriageNew.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiTriageNewTriage, Me.bmiTriageNewVisionAssessment, Me.bmiTriageNewIPDVisionAssessment})
        Me.bmiTriageNew.Image = CType(resources.GetObject("bmiTriageNew.Image"), System.Drawing.Image)
        Me.bmiTriageNew.Name = "bmiTriageNew"
        Me.bmiTriageNew.Size = New System.Drawing.Size(98, 22)
        Me.bmiTriageNew.Tag = ""
        Me.bmiTriageNew.Text = "New"
        '
        'bmiTriageNewTriage
        '
        Me.bmiTriageNewTriage.AccessibleDescription = ""
        Me.bmiTriageNewTriage.Image = CType(resources.GetObject("bmiTriageNewTriage.Image"), System.Drawing.Image)
        Me.bmiTriageNewTriage.Name = "bmiTriageNewTriage"
        Me.bmiTriageNewTriage.Size = New System.Drawing.Size(192, 22)
        Me.bmiTriageNewTriage.Tag = "Triage"
        Me.bmiTriageNewTriage.Text = "Triage"
        '
        'bmiTriageNewVisionAssessment
        '
        Me.bmiTriageNewVisionAssessment.AccessibleDescription = ""
        Me.bmiTriageNewVisionAssessment.Image = CType(resources.GetObject("bmiTriageNewVisionAssessment.Image"), System.Drawing.Image)
        Me.bmiTriageNewVisionAssessment.Name = "bmiTriageNewVisionAssessment"
        Me.bmiTriageNewVisionAssessment.Size = New System.Drawing.Size(192, 22)
        Me.bmiTriageNewVisionAssessment.Tag = "VisionAssessment"
        Me.bmiTriageNewVisionAssessment.Text = "Vision Assessment"
        '
        'bmiTriageNewIPDVisionAssessment
        '
        Me.bmiTriageNewIPDVisionAssessment.Image = CType(resources.GetObject("bmiTriageNewIPDVisionAssessment.Image"), System.Drawing.Image)
        Me.bmiTriageNewIPDVisionAssessment.Name = "bmiTriageNewIPDVisionAssessment"
        Me.bmiTriageNewIPDVisionAssessment.Size = New System.Drawing.Size(192, 22)
        Me.bmiTriageNewIPDVisionAssessment.Tag = "IPDVisionAssessment"
        Me.bmiTriageNewIPDVisionAssessment.Text = "IPD Vision Assessment"
        '
        'bmiTriageEdit
        '
        Me.bmiTriageEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiTriageEditTriage, Me.bmiTriageEditVisionAssessment, Me.bmiTriageEditIPDVisionAssessment})
        Me.bmiTriageEdit.Image = CType(resources.GetObject("bmiTriageEdit.Image"), System.Drawing.Image)
        Me.bmiTriageEdit.Name = "bmiTriageEdit"
        Me.bmiTriageEdit.Size = New System.Drawing.Size(98, 22)
        Me.bmiTriageEdit.Tag = ""
        Me.bmiTriageEdit.Text = "Edit"
        '
        'bmiTriageEditTriage
        '
        Me.bmiTriageEditTriage.Image = CType(resources.GetObject("bmiTriageEditTriage.Image"), System.Drawing.Image)
        Me.bmiTriageEditTriage.Name = "bmiTriageEditTriage"
        Me.bmiTriageEditTriage.Size = New System.Drawing.Size(192, 22)
        Me.bmiTriageEditTriage.Tag = "Triage"
        Me.bmiTriageEditTriage.Text = "Triage"
        '
        'bmiTriageEditVisionAssessment
        '
        Me.bmiTriageEditVisionAssessment.Image = CType(resources.GetObject("bmiTriageEditVisionAssessment.Image"), System.Drawing.Image)
        Me.bmiTriageEditVisionAssessment.Name = "bmiTriageEditVisionAssessment"
        Me.bmiTriageEditVisionAssessment.Size = New System.Drawing.Size(192, 22)
        Me.bmiTriageEditVisionAssessment.Tag = "VisionAssessment"
        Me.bmiTriageEditVisionAssessment.Text = "Vision Assessment"
        '
        'bmiTriageEditIPDVisionAssessment
        '
        Me.bmiTriageEditIPDVisionAssessment.Image = CType(resources.GetObject("bmiTriageEditIPDVisionAssessment.Image"), System.Drawing.Image)
        Me.bmiTriageEditIPDVisionAssessment.Name = "bmiTriageEditIPDVisionAssessment"
        Me.bmiTriageEditIPDVisionAssessment.Size = New System.Drawing.Size(192, 22)
        Me.bmiTriageEditIPDVisionAssessment.Tag = "IPDVisionAssessment"
        Me.bmiTriageEditIPDVisionAssessment.Text = "IPD Vision Assessment"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(6, 38)
        '
        'tbbCashier
        '
        Me.tbbCashier.Image = CType(resources.GetObject("tbbCashier.Image"), System.Drawing.Image)
        Me.tbbCashier.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tbbCashier.Name = "tbbCashier"
        Me.tbbCashier.Size = New System.Drawing.Size(50, 35)
        Me.tbbCashier.Tag = "Payments"
        Me.tbbCashier.Text = "Cashier"
        Me.tbbCashier.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tbbSeparotor3
        '
        Me.tbbSeparotor3.Name = "tbbSeparotor3"
        Me.tbbSeparotor3.Size = New System.Drawing.Size(6, 38)
        '
        'ddbInvoices
        '
        Me.ddbInvoices.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiInvoicesNew, Me.bmiInvoicesInvoiceAdjustments})
        Me.ddbInvoices.Image = CType(resources.GetObject("ddbInvoices.Image"), System.Drawing.Image)
        Me.ddbInvoices.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbInvoices.Name = "ddbInvoices"
        Me.ddbInvoices.Size = New System.Drawing.Size(63, 35)
        Me.ddbInvoices.Tag = ""
        Me.ddbInvoices.Text = "Invoices"
        Me.ddbInvoices.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiInvoicesNew
        '
        Me.bmiInvoicesNew.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiInvoicesNewInvoices, Me.bmiInvoicesNewIBillFormInvoices})
        Me.bmiInvoicesNew.Image = CType(resources.GetObject("bmiInvoicesNew.Image"), System.Drawing.Image)
        Me.bmiInvoicesNew.Name = "bmiInvoicesNew"
        Me.bmiInvoicesNew.Size = New System.Drawing.Size(182, 22)
        Me.bmiInvoicesNew.Tag = "Invoices"
        Me.bmiInvoicesNew.Text = "New"
        '
        'bmiInvoicesNewInvoices
        '
        Me.bmiInvoicesNewInvoices.AccessibleDescription = ""
        Me.bmiInvoicesNewInvoices.AccessibleName = ""
        Me.bmiInvoicesNewInvoices.Image = CType(resources.GetObject("bmiInvoicesNewInvoices.Image"), System.Drawing.Image)
        Me.bmiInvoicesNewInvoices.Name = "bmiInvoicesNewInvoices"
        Me.bmiInvoicesNewInvoices.Size = New System.Drawing.Size(167, 22)
        Me.bmiInvoicesNewInvoices.Tag = "Invoices"
        Me.bmiInvoicesNewInvoices.Text = "Invoices"
        '
        'bmiInvoicesNewIBillFormInvoices
        '
        Me.bmiInvoicesNewIBillFormInvoices.Image = CType(resources.GetObject("bmiInvoicesNewIBillFormInvoices.Image"), System.Drawing.Image)
        Me.bmiInvoicesNewIBillFormInvoices.Name = "bmiInvoicesNewIBillFormInvoices"
        Me.bmiInvoicesNewIBillFormInvoices.Size = New System.Drawing.Size(167, 22)
        Me.bmiInvoicesNewIBillFormInvoices.Tag = "Invoices"
        Me.bmiInvoicesNewIBillFormInvoices.Text = "Bill Form Invoices"
        '
        'bmiInvoicesInvoiceAdjustments
        '
        Me.bmiInvoicesInvoiceAdjustments.Image = CType(resources.GetObject("bmiInvoicesInvoiceAdjustments.Image"), System.Drawing.Image)
        Me.bmiInvoicesInvoiceAdjustments.Name = "bmiInvoicesInvoiceAdjustments"
        Me.bmiInvoicesInvoiceAdjustments.Size = New System.Drawing.Size(182, 22)
        Me.bmiInvoicesInvoiceAdjustments.Tag = "InvoiceAdjustments"
        Me.bmiInvoicesInvoiceAdjustments.Text = "Invoice Adjustments"
        '
        'ToolStripSeparator12
        '
        Me.ToolStripSeparator12.Name = "ToolStripSeparator12"
        Me.ToolStripSeparator12.Size = New System.Drawing.Size(6, 38)
        '
        'tbbDoctor
        '
        Me.tbbDoctor.Image = CType(resources.GetObject("tbbDoctor.Image"), System.Drawing.Image)
        Me.tbbDoctor.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tbbDoctor.Name = "tbbDoctor"
        Me.tbbDoctor.Size = New System.Drawing.Size(47, 35)
        Me.tbbDoctor.Tag = "DoctorVisits"
        Me.tbbDoctor.Text = "Doctor"
        Me.tbbDoctor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tbbSeparator4
        '
        Me.tbbSeparator4.Name = "tbbSeparator4"
        Me.tbbSeparator4.Size = New System.Drawing.Size(6, 38)
        '
        'ddbLaboratory
        '
        Me.ddbLaboratory.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiLaboratoryNew, Me.bmiLaboratoryEdit, Me.bmiLaboratoryApproveLabResults, Me.bmiLaboratoryRefundRequest})
        Me.ddbLaboratory.Image = CType(resources.GetObject("ddbLaboratory.Image"), System.Drawing.Image)
        Me.ddbLaboratory.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbLaboratory.Name = "ddbLaboratory"
        Me.ddbLaboratory.Size = New System.Drawing.Size(77, 35)
        Me.ddbLaboratory.Text = "Laboratory"
        Me.ddbLaboratory.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiLaboratoryNew
        '
        Me.bmiLaboratoryNew.AccessibleDescription = ""
        Me.bmiLaboratoryNew.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiLaboratoryNewLabRequests, Me.bmiLaboratoryNewIPDLabRequests, Me.bmiLaboratoryNewLabResults, Me.ToolStripSeparator21, Me.bmiPathologyNewPathologyRequests, Me.bmiPathologyNewPathologyReports, Me.bmiPathologyNewIPDPathologyRequests, Me.bmiPathologyNewIPDPathologyReports})
        Me.bmiLaboratoryNew.Image = CType(resources.GetObject("bmiLaboratoryNew.Image"), System.Drawing.Image)
        Me.bmiLaboratoryNew.Name = "bmiLaboratoryNew"
        Me.bmiLaboratoryNew.Size = New System.Drawing.Size(181, 22)
        Me.bmiLaboratoryNew.Tag = ""
        Me.bmiLaboratoryNew.Text = "New"
        '
        'bmiLaboratoryNewLabRequests
        '
        Me.bmiLaboratoryNewLabRequests.AccessibleDescription = ""
        Me.bmiLaboratoryNewLabRequests.AccessibleName = ""
        Me.bmiLaboratoryNewLabRequests.Image = CType(resources.GetObject("bmiLaboratoryNewLabRequests.Image"), System.Drawing.Image)
        Me.bmiLaboratoryNewLabRequests.Name = "bmiLaboratoryNewLabRequests"
        Me.bmiLaboratoryNewLabRequests.Size = New System.Drawing.Size(199, 22)
        Me.bmiLaboratoryNewLabRequests.Tag = "LabRequests"
        Me.bmiLaboratoryNewLabRequests.Text = "Lab Requests"
        '
        'bmiLaboratoryNewIPDLabRequests
        '
        Me.bmiLaboratoryNewIPDLabRequests.Image = CType(resources.GetObject("bmiLaboratoryNewIPDLabRequests.Image"), System.Drawing.Image)
        Me.bmiLaboratoryNewIPDLabRequests.Name = "bmiLaboratoryNewIPDLabRequests"
        Me.bmiLaboratoryNewIPDLabRequests.Size = New System.Drawing.Size(199, 22)
        Me.bmiLaboratoryNewIPDLabRequests.Tag = "IPDLabRequests"
        Me.bmiLaboratoryNewIPDLabRequests.Text = "IPD Lab Requests"
        '
        'bmiLaboratoryNewLabResults
        '
        Me.bmiLaboratoryNewLabResults.AccessibleDescription = ""
        Me.bmiLaboratoryNewLabResults.Image = CType(resources.GetObject("bmiLaboratoryNewLabResults.Image"), System.Drawing.Image)
        Me.bmiLaboratoryNewLabResults.Name = "bmiLaboratoryNewLabResults"
        Me.bmiLaboratoryNewLabResults.Size = New System.Drawing.Size(199, 22)
        Me.bmiLaboratoryNewLabResults.Tag = "LabResults"
        Me.bmiLaboratoryNewLabResults.Text = "Lab Results"
        '
        'ToolStripSeparator21
        '
        Me.ToolStripSeparator21.Name = "ToolStripSeparator21"
        Me.ToolStripSeparator21.Size = New System.Drawing.Size(196, 6)
        '
        'bmiPathologyNewPathologyRequests
        '
        Me.bmiPathologyNewPathologyRequests.Image = CType(resources.GetObject("bmiPathologyNewPathologyRequests.Image"), System.Drawing.Image)
        Me.bmiPathologyNewPathologyRequests.Name = "bmiPathologyNewPathologyRequests"
        Me.bmiPathologyNewPathologyRequests.Size = New System.Drawing.Size(199, 22)
        Me.bmiPathologyNewPathologyRequests.Tag = "PathologyRequests"
        Me.bmiPathologyNewPathologyRequests.Text = "Pathology Requests"
        '
        'bmiPathologyNewPathologyReports
        '
        Me.bmiPathologyNewPathologyReports.Image = CType(resources.GetObject("bmiPathologyNewPathologyReports.Image"), System.Drawing.Image)
        Me.bmiPathologyNewPathologyReports.Name = "bmiPathologyNewPathologyReports"
        Me.bmiPathologyNewPathologyReports.Size = New System.Drawing.Size(199, 22)
        Me.bmiPathologyNewPathologyReports.Tag = "PathologyReports"
        Me.bmiPathologyNewPathologyReports.Text = "Pathology Reports"
        '
        'bmiPathologyNewIPDPathologyRequests
        '
        Me.bmiPathologyNewIPDPathologyRequests.Image = CType(resources.GetObject("bmiPathologyNewIPDPathologyRequests.Image"), System.Drawing.Image)
        Me.bmiPathologyNewIPDPathologyRequests.Name = "bmiPathologyNewIPDPathologyRequests"
        Me.bmiPathologyNewIPDPathologyRequests.Size = New System.Drawing.Size(199, 22)
        Me.bmiPathologyNewIPDPathologyRequests.Tag = "PathologyRequests"
        Me.bmiPathologyNewIPDPathologyRequests.Text = "IPD Pathology Requests"
        '
        'bmiPathologyNewIPDPathologyReports
        '
        Me.bmiPathologyNewIPDPathologyReports.Image = CType(resources.GetObject("bmiPathologyNewIPDPathologyReports.Image"), System.Drawing.Image)
        Me.bmiPathologyNewIPDPathologyReports.Name = "bmiPathologyNewIPDPathologyReports"
        Me.bmiPathologyNewIPDPathologyReports.Size = New System.Drawing.Size(199, 22)
        Me.bmiPathologyNewIPDPathologyReports.Tag = "IPDPathologyReports"
        Me.bmiPathologyNewIPDPathologyReports.Text = "IPD Pathology Reports"
        '
        'bmiLaboratoryEdit
        '
        Me.bmiLaboratoryEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiLaboratoryEditLabRequests, Me.bmiLaboratoryEditIPDLabRequests, Me.bmiLaboratoryEditLabResults, Me.ToolStripSeparator22, Me.bmiPathologyEditPathologyReports, Me.bmiPathologyEditIPDPathologyReports})
        Me.bmiLaboratoryEdit.Image = CType(resources.GetObject("bmiLaboratoryEdit.Image"), System.Drawing.Image)
        Me.bmiLaboratoryEdit.Name = "bmiLaboratoryEdit"
        Me.bmiLaboratoryEdit.Size = New System.Drawing.Size(181, 22)
        Me.bmiLaboratoryEdit.Tag = ""
        Me.bmiLaboratoryEdit.Text = "Edit"
        '
        'bmiLaboratoryEditLabRequests
        '
        Me.bmiLaboratoryEditLabRequests.AccessibleDescription = ""
        Me.bmiLaboratoryEditLabRequests.Image = CType(resources.GetObject("bmiLaboratoryEditLabRequests.Image"), System.Drawing.Image)
        Me.bmiLaboratoryEditLabRequests.Name = "bmiLaboratoryEditLabRequests"
        Me.bmiLaboratoryEditLabRequests.Size = New System.Drawing.Size(192, 22)
        Me.bmiLaboratoryEditLabRequests.Tag = "LabRequests"
        Me.bmiLaboratoryEditLabRequests.Text = "Lab Requests"
        '
        'bmiLaboratoryEditIPDLabRequests
        '
        Me.bmiLaboratoryEditIPDLabRequests.Image = CType(resources.GetObject("bmiLaboratoryEditIPDLabRequests.Image"), System.Drawing.Image)
        Me.bmiLaboratoryEditIPDLabRequests.Name = "bmiLaboratoryEditIPDLabRequests"
        Me.bmiLaboratoryEditIPDLabRequests.Size = New System.Drawing.Size(192, 22)
        Me.bmiLaboratoryEditIPDLabRequests.Tag = "IPDLabRequests"
        Me.bmiLaboratoryEditIPDLabRequests.Text = "IPD Lab Requests"
        '
        'bmiLaboratoryEditLabResults
        '
        Me.bmiLaboratoryEditLabResults.AccessibleDescription = ""
        Me.bmiLaboratoryEditLabResults.Image = CType(resources.GetObject("bmiLaboratoryEditLabResults.Image"), System.Drawing.Image)
        Me.bmiLaboratoryEditLabResults.Name = "bmiLaboratoryEditLabResults"
        Me.bmiLaboratoryEditLabResults.Size = New System.Drawing.Size(192, 22)
        Me.bmiLaboratoryEditLabResults.Tag = "LabResults"
        Me.bmiLaboratoryEditLabResults.Text = "Lab Results"
        '
        'ToolStripSeparator22
        '
        Me.ToolStripSeparator22.Name = "ToolStripSeparator22"
        Me.ToolStripSeparator22.Size = New System.Drawing.Size(189, 6)
        '
        'bmiPathologyEditPathologyReports
        '
        Me.bmiPathologyEditPathologyReports.Name = "bmiPathologyEditPathologyReports"
        Me.bmiPathologyEditPathologyReports.Size = New System.Drawing.Size(192, 22)
        Me.bmiPathologyEditPathologyReports.Tag = "PathologyReports"
        Me.bmiPathologyEditPathologyReports.Text = "Pathology Reports"
        '
        'bmiPathologyEditIPDPathologyReports
        '
        Me.bmiPathologyEditIPDPathologyReports.Name = "bmiPathologyEditIPDPathologyReports"
        Me.bmiPathologyEditIPDPathologyReports.Size = New System.Drawing.Size(192, 22)
        Me.bmiPathologyEditIPDPathologyReports.Tag = "PathologyReports"
        Me.bmiPathologyEditIPDPathologyReports.Text = "IPD Pathology Reports"
        '
        'bmiLaboratoryApproveLabResults
        '
        Me.bmiLaboratoryApproveLabResults.Image = CType(resources.GetObject("bmiLaboratoryApproveLabResults.Image"), System.Drawing.Image)
        Me.bmiLaboratoryApproveLabResults.Name = "bmiLaboratoryApproveLabResults"
        Me.bmiLaboratoryApproveLabResults.Size = New System.Drawing.Size(181, 22)
        Me.bmiLaboratoryApproveLabResults.Tag = "ApprovedLabResults"
        Me.bmiLaboratoryApproveLabResults.Text = "Approve Lab Results"
        '
        'bmiLaboratoryRefundRequest
        '
        Me.bmiLaboratoryRefundRequest.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiLaboratoryRefundRequestLaboratory, Me.bmiLaboratoryRefundRequestPathology})
        Me.bmiLaboratoryRefundRequest.Image = CType(resources.GetObject("bmiLaboratoryRefundRequest.Image"), System.Drawing.Image)
        Me.bmiLaboratoryRefundRequest.Name = "bmiLaboratoryRefundRequest"
        Me.bmiLaboratoryRefundRequest.Size = New System.Drawing.Size(181, 22)
        Me.bmiLaboratoryRefundRequest.Tag = "RefundRequests"
        Me.bmiLaboratoryRefundRequest.Text = "Refund Request"
        '
        'bmiLaboratoryRefundRequestLaboratory
        '
        Me.bmiLaboratoryRefundRequestLaboratory.Name = "bmiLaboratoryRefundRequestLaboratory"
        Me.bmiLaboratoryRefundRequestLaboratory.Size = New System.Drawing.Size(131, 22)
        Me.bmiLaboratoryRefundRequestLaboratory.Text = "Laboratory"
        '
        'bmiLaboratoryRefundRequestPathology
        '
        Me.bmiLaboratoryRefundRequestPathology.Name = "bmiLaboratoryRefundRequestPathology"
        Me.bmiLaboratoryRefundRequestPathology.Size = New System.Drawing.Size(131, 22)
        Me.bmiLaboratoryRefundRequestPathology.Text = "Pathology"
        '
        'tbbSeparator5
        '
        Me.tbbSeparator5.Name = "tbbSeparator5"
        Me.tbbSeparator5.Size = New System.Drawing.Size(6, 38)
        '
        'ddbCardiology
        '
        Me.ddbCardiology.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiCardiologyNew, Me.bmiCardiologyEdit, Me.bmiCardiologyRefundRequests})
        Me.ddbCardiology.Image = CType(resources.GetObject("ddbCardiology.Image"), System.Drawing.Image)
        Me.ddbCardiology.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbCardiology.Name = "ddbCardiology"
        Me.ddbCardiology.Size = New System.Drawing.Size(78, 35)
        Me.ddbCardiology.Text = "Cardiology"
        Me.ddbCardiology.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiCardiologyNew
        '
        Me.bmiCardiologyNew.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiCardiologyNewCardiologyRequests, Me.bmiCardiologyNewCardiologyReports, Me.ToolStripMenuItem7, Me.bmiCardiologyNewIPDCardiologyRequests, Me.bmiCardiologyNewIPDCardiologyReports})
        Me.bmiCardiologyNew.Image = CType(resources.GetObject("bmiCardiologyNew.Image"), System.Drawing.Image)
        Me.bmiCardiologyNew.Name = "bmiCardiologyNew"
        Me.bmiCardiologyNew.Size = New System.Drawing.Size(157, 22)
        Me.bmiCardiologyNew.Tag = ""
        Me.bmiCardiologyNew.Text = "New"
        '
        'bmiCardiologyNewCardiologyRequests
        '
        Me.bmiCardiologyNewCardiologyRequests.Image = CType(resources.GetObject("bmiCardiologyNewCardiologyRequests.Image"), System.Drawing.Image)
        Me.bmiCardiologyNewCardiologyRequests.Name = "bmiCardiologyNewCardiologyRequests"
        Me.bmiCardiologyNewCardiologyRequests.Size = New System.Drawing.Size(203, 22)
        Me.bmiCardiologyNewCardiologyRequests.Tag = "CardiologyRequests"
        Me.bmiCardiologyNewCardiologyRequests.Text = "Cardiology Requests"
        '
        'bmiCardiologyNewCardiologyReports
        '
        Me.bmiCardiologyNewCardiologyReports.Image = CType(resources.GetObject("bmiCardiologyNewCardiologyReports.Image"), System.Drawing.Image)
        Me.bmiCardiologyNewCardiologyReports.Name = "bmiCardiologyNewCardiologyReports"
        Me.bmiCardiologyNewCardiologyReports.Size = New System.Drawing.Size(203, 22)
        Me.bmiCardiologyNewCardiologyReports.Tag = "CardiologyReports"
        Me.bmiCardiologyNewCardiologyReports.Text = "Cardiology Reports"
        '
        'ToolStripMenuItem7
        '
        Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
        Me.ToolStripMenuItem7.Size = New System.Drawing.Size(200, 6)
        '
        'bmiCardiologyNewIPDCardiologyRequests
        '
        Me.bmiCardiologyNewIPDCardiologyRequests.Image = CType(resources.GetObject("bmiCardiologyNewIPDCardiologyRequests.Image"), System.Drawing.Image)
        Me.bmiCardiologyNewIPDCardiologyRequests.Name = "bmiCardiologyNewIPDCardiologyRequests"
        Me.bmiCardiologyNewIPDCardiologyRequests.Size = New System.Drawing.Size(203, 22)
        Me.bmiCardiologyNewIPDCardiologyRequests.Tag = "CardiologyRequests"
        Me.bmiCardiologyNewIPDCardiologyRequests.Text = "IPD Cardiology Requests"
        '
        'bmiCardiologyNewIPDCardiologyReports
        '
        Me.bmiCardiologyNewIPDCardiologyReports.Image = CType(resources.GetObject("bmiCardiologyNewIPDCardiologyReports.Image"), System.Drawing.Image)
        Me.bmiCardiologyNewIPDCardiologyReports.Name = "bmiCardiologyNewIPDCardiologyReports"
        Me.bmiCardiologyNewIPDCardiologyReports.Size = New System.Drawing.Size(203, 22)
        Me.bmiCardiologyNewIPDCardiologyReports.Tag = "IPDCardiologyReports"
        Me.bmiCardiologyNewIPDCardiologyReports.Text = "IPD Cardiology Reports"
        '
        'bmiCardiologyEdit
        '
        Me.bmiCardiologyEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiCardiologyEditCardiologyReports, Me.bmiCardiologyEditIPDCardiologyReports})
        Me.bmiCardiologyEdit.Image = CType(resources.GetObject("bmiCardiologyEdit.Image"), System.Drawing.Image)
        Me.bmiCardiologyEdit.Name = "bmiCardiologyEdit"
        Me.bmiCardiologyEdit.Size = New System.Drawing.Size(157, 22)
        Me.bmiCardiologyEdit.Tag = ""
        Me.bmiCardiologyEdit.Text = "Edit"
        '
        'bmiCardiologyEditCardiologyReports
        '
        Me.bmiCardiologyEditCardiologyReports.Image = CType(resources.GetObject("bmiCardiologyEditCardiologyReports.Image"), System.Drawing.Image)
        Me.bmiCardiologyEditCardiologyReports.Name = "bmiCardiologyEditCardiologyReports"
        Me.bmiCardiologyEditCardiologyReports.Size = New System.Drawing.Size(196, 22)
        Me.bmiCardiologyEditCardiologyReports.Tag = "CardiologyReports"
        Me.bmiCardiologyEditCardiologyReports.Text = "Cardiology Reports"
        '
        'bmiCardiologyEditIPDCardiologyReports
        '
        Me.bmiCardiologyEditIPDCardiologyReports.Image = CType(resources.GetObject("bmiCardiologyEditIPDCardiologyReports.Image"), System.Drawing.Image)
        Me.bmiCardiologyEditIPDCardiologyReports.Name = "bmiCardiologyEditIPDCardiologyReports"
        Me.bmiCardiologyEditIPDCardiologyReports.Size = New System.Drawing.Size(196, 22)
        Me.bmiCardiologyEditIPDCardiologyReports.Tag = "IPDCardiologyReports"
        Me.bmiCardiologyEditIPDCardiologyReports.Text = "IPD Cardiology Reports"
        '
        'bmiCardiologyRefundRequests
        '
        Me.bmiCardiologyRefundRequests.Image = CType(resources.GetObject("bmiCardiologyRefundRequests.Image"), System.Drawing.Image)
        Me.bmiCardiologyRefundRequests.Name = "bmiCardiologyRefundRequests"
        Me.bmiCardiologyRefundRequests.Size = New System.Drawing.Size(157, 22)
        Me.bmiCardiologyRefundRequests.Tag = "RefundRequests"
        Me.bmiCardiologyRefundRequests.Text = "Refund Request"
        '
        'ToolStripSeparator19
        '
        Me.ToolStripSeparator19.Name = "ToolStripSeparator19"
        Me.ToolStripSeparator19.Size = New System.Drawing.Size(6, 38)
        '
        'ddbRadiology
        '
        Me.ddbRadiology.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiRadiologyNew, Me.bmiRadiologyEdit, Me.bmiRadiologyRefundRequest, Me.bmiRadiologyReImages})
        Me.ddbRadiology.Image = CType(resources.GetObject("ddbRadiology.Image"), System.Drawing.Image)
        Me.ddbRadiology.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbRadiology.Name = "ddbRadiology"
        Me.ddbRadiology.Size = New System.Drawing.Size(73, 35)
        Me.ddbRadiology.Text = "Radiology"
        Me.ddbRadiology.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiRadiologyNew
        '
        Me.bmiRadiologyNew.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiRadiologyNewRadiologyRequests, Me.bmiRadiologyNewRadiologyReports, Me.bmiRadiologyNewIPDRadiologyRequests, Me.bmiRadiologyNewIPDRadiologyReports})
        Me.bmiRadiologyNew.Image = CType(resources.GetObject("bmiRadiologyNew.Image"), System.Drawing.Image)
        Me.bmiRadiologyNew.Name = "bmiRadiologyNew"
        Me.bmiRadiologyNew.Size = New System.Drawing.Size(157, 22)
        Me.bmiRadiologyNew.Tag = ""
        Me.bmiRadiologyNew.Text = "New"
        '
        'bmiRadiologyNewRadiologyRequests
        '
        Me.bmiRadiologyNewRadiologyRequests.Image = CType(resources.GetObject("bmiRadiologyNewRadiologyRequests.Image"), System.Drawing.Image)
        Me.bmiRadiologyNewRadiologyRequests.Name = "bmiRadiologyNewRadiologyRequests"
        Me.bmiRadiologyNewRadiologyRequests.Size = New System.Drawing.Size(198, 22)
        Me.bmiRadiologyNewRadiologyRequests.Tag = "RadiologyRequests"
        Me.bmiRadiologyNewRadiologyRequests.Text = "Radiology Requests"
        '
        'bmiRadiologyNewRadiologyReports
        '
        Me.bmiRadiologyNewRadiologyReports.Image = CType(resources.GetObject("bmiRadiologyNewRadiologyReports.Image"), System.Drawing.Image)
        Me.bmiRadiologyNewRadiologyReports.Name = "bmiRadiologyNewRadiologyReports"
        Me.bmiRadiologyNewRadiologyReports.Size = New System.Drawing.Size(198, 22)
        Me.bmiRadiologyNewRadiologyReports.Tag = "RadiologyReports"
        Me.bmiRadiologyNewRadiologyReports.Text = "Radiology Reports"
        '
        'bmiRadiologyNewIPDRadiologyRequests
        '
        Me.bmiRadiologyNewIPDRadiologyRequests.Image = CType(resources.GetObject("bmiRadiologyNewIPDRadiologyRequests.Image"), System.Drawing.Image)
        Me.bmiRadiologyNewIPDRadiologyRequests.Name = "bmiRadiologyNewIPDRadiologyRequests"
        Me.bmiRadiologyNewIPDRadiologyRequests.Size = New System.Drawing.Size(198, 22)
        Me.bmiRadiologyNewIPDRadiologyRequests.Tag = "IPDRadiologyRequests"
        Me.bmiRadiologyNewIPDRadiologyRequests.Text = "IPD Radiology Requests"
        '
        'bmiRadiologyNewIPDRadiologyReports
        '
        Me.bmiRadiologyNewIPDRadiologyReports.Image = CType(resources.GetObject("bmiRadiologyNewIPDRadiologyReports.Image"), System.Drawing.Image)
        Me.bmiRadiologyNewIPDRadiologyReports.Name = "bmiRadiologyNewIPDRadiologyReports"
        Me.bmiRadiologyNewIPDRadiologyReports.Size = New System.Drawing.Size(198, 22)
        Me.bmiRadiologyNewIPDRadiologyReports.Tag = "IPDRadiologyReports"
        Me.bmiRadiologyNewIPDRadiologyReports.Text = "IPD Radiology Reports"
        '
        'bmiRadiologyEdit
        '
        Me.bmiRadiologyEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiRadiologyEditRadiologyReports, Me.bmiRadiologyEditIPDRadiologyReports})
        Me.bmiRadiologyEdit.Image = CType(resources.GetObject("bmiRadiologyEdit.Image"), System.Drawing.Image)
        Me.bmiRadiologyEdit.Name = "bmiRadiologyEdit"
        Me.bmiRadiologyEdit.Size = New System.Drawing.Size(157, 22)
        Me.bmiRadiologyEdit.Tag = ""
        Me.bmiRadiologyEdit.Text = "Edit"
        '
        'bmiRadiologyEditRadiologyReports
        '
        Me.bmiRadiologyEditRadiologyReports.Image = CType(resources.GetObject("bmiRadiologyEditRadiologyReports.Image"), System.Drawing.Image)
        Me.bmiRadiologyEditRadiologyReports.Name = "bmiRadiologyEditRadiologyReports"
        Me.bmiRadiologyEditRadiologyReports.Size = New System.Drawing.Size(191, 22)
        Me.bmiRadiologyEditRadiologyReports.Tag = "RadiologyReports"
        Me.bmiRadiologyEditRadiologyReports.Text = "Radiology Reports"
        '
        'bmiRadiologyEditIPDRadiologyReports
        '
        Me.bmiRadiologyEditIPDRadiologyReports.Image = CType(resources.GetObject("bmiRadiologyEditIPDRadiologyReports.Image"), System.Drawing.Image)
        Me.bmiRadiologyEditIPDRadiologyReports.Name = "bmiRadiologyEditIPDRadiologyReports"
        Me.bmiRadiologyEditIPDRadiologyReports.Size = New System.Drawing.Size(191, 22)
        Me.bmiRadiologyEditIPDRadiologyReports.Tag = "IPDRadiologyReports"
        Me.bmiRadiologyEditIPDRadiologyReports.Text = "IPD Radiology Reports"
        '
        'bmiRadiologyRefundRequest
        '
        Me.bmiRadiologyRefundRequest.Image = CType(resources.GetObject("bmiRadiologyRefundRequest.Image"), System.Drawing.Image)
        Me.bmiRadiologyRefundRequest.Name = "bmiRadiologyRefundRequest"
        Me.bmiRadiologyRefundRequest.Size = New System.Drawing.Size(157, 22)
        Me.bmiRadiologyRefundRequest.Tag = "RefundRequests"
        Me.bmiRadiologyRefundRequest.Text = "Refund Request"
        '
        'bmiRadiologyReImages
        '
        Me.bmiRadiologyReImages.Image = CType(resources.GetObject("bmiRadiologyReImages.Image"), System.Drawing.Image)
        Me.bmiRadiologyReImages.Name = "bmiRadiologyReImages"
        Me.bmiRadiologyReImages.Size = New System.Drawing.Size(157, 22)
        Me.bmiRadiologyReImages.Text = "Images"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(6, 38)
        '
        'ddbPharmacy
        '
        Me.ddbPharmacy.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiPharmacyDispense, Me.bmiIPDPharmacyDispense, Me.bmiPharmacyIssueConsumables, Me.bmiPharmacyIssueIPDConsumables, Me.bmiPreviousPrescriptions, Me.bmiDrugAdministration, Me.bmiOPDPharmacyRefundRequests})
        Me.ddbPharmacy.Image = CType(resources.GetObject("ddbPharmacy.Image"), System.Drawing.Image)
        Me.ddbPharmacy.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbPharmacy.Name = "ddbPharmacy"
        Me.ddbPharmacy.Size = New System.Drawing.Size(73, 35)
        Me.ddbPharmacy.Tag = ""
        Me.ddbPharmacy.Text = "Pharmacy"
        Me.ddbPharmacy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiPharmacyDispense
        '
        Me.bmiPharmacyDispense.Image = CType(resources.GetObject("bmiPharmacyDispense.Image"), System.Drawing.Image)
        Me.bmiPharmacyDispense.Name = "bmiPharmacyDispense"
        Me.bmiPharmacyDispense.Size = New System.Drawing.Size(196, 22)
        Me.bmiPharmacyDispense.Tag = "Pharmacy"
        Me.bmiPharmacyDispense.Text = "Dispense"
        '
        'bmiIPDPharmacyDispense
        '
        Me.bmiIPDPharmacyDispense.Image = CType(resources.GetObject("bmiIPDPharmacyDispense.Image"), System.Drawing.Image)
        Me.bmiIPDPharmacyDispense.Name = "bmiIPDPharmacyDispense"
        Me.bmiIPDPharmacyDispense.Size = New System.Drawing.Size(196, 22)
        Me.bmiIPDPharmacyDispense.Tag = "IPDPharmacy"
        Me.bmiIPDPharmacyDispense.Text = "IPD Dispense"
        '
        'bmiPharmacyIssueConsumables
        '
        Me.bmiPharmacyIssueConsumables.Image = CType(resources.GetObject("bmiPharmacyIssueConsumables.Image"), System.Drawing.Image)
        Me.bmiPharmacyIssueConsumables.Name = "bmiPharmacyIssueConsumables"
        Me.bmiPharmacyIssueConsumables.Size = New System.Drawing.Size(196, 22)
        Me.bmiPharmacyIssueConsumables.Tag = "IssueConsumables"
        Me.bmiPharmacyIssueConsumables.Text = "Issue Consumables"
        '
        'bmiPharmacyIssueIPDConsumables
        '
        Me.bmiPharmacyIssueIPDConsumables.Image = CType(resources.GetObject("bmiPharmacyIssueIPDConsumables.Image"), System.Drawing.Image)
        Me.bmiPharmacyIssueIPDConsumables.Name = "bmiPharmacyIssueIPDConsumables"
        Me.bmiPharmacyIssueIPDConsumables.Size = New System.Drawing.Size(196, 22)
        Me.bmiPharmacyIssueIPDConsumables.Tag = "IssueIPDConsumables"
        Me.bmiPharmacyIssueIPDConsumables.Text = "Issue IPD Consumables"
        '
        'bmiPreviousPrescriptions
        '
        Me.bmiPreviousPrescriptions.Image = CType(resources.GetObject("bmiPreviousPrescriptions.Image"), System.Drawing.Image)
        Me.bmiPreviousPrescriptions.Name = "bmiPreviousPrescriptions"
        Me.bmiPreviousPrescriptions.Size = New System.Drawing.Size(196, 22)
        Me.bmiPreviousPrescriptions.Tag = "PreviousPrescriptions"
        Me.bmiPreviousPrescriptions.Text = "Previous Prescriptions"
        '
        'bmiDrugAdministration
        '
        Me.bmiDrugAdministration.Image = CType(resources.GetObject("bmiDrugAdministration.Image"), System.Drawing.Image)
        Me.bmiDrugAdministration.Name = "bmiDrugAdministration"
        Me.bmiDrugAdministration.Size = New System.Drawing.Size(196, 22)
        Me.bmiDrugAdministration.Tag = "DrugAdministration"
        Me.bmiDrugAdministration.Text = "Drug Administration"
        '
        'bmiOPDPharmacyRefundRequests
        '
        Me.bmiOPDPharmacyRefundRequests.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiOPDPharmacyRefundRequestsDrugs, Me.bmiOPDPharmacyRefundRequestsConsumables})
        Me.bmiOPDPharmacyRefundRequests.Image = CType(resources.GetObject("bmiOPDPharmacyRefundRequests.Image"), System.Drawing.Image)
        Me.bmiOPDPharmacyRefundRequests.Name = "bmiOPDPharmacyRefundRequests"
        Me.bmiOPDPharmacyRefundRequests.Size = New System.Drawing.Size(196, 22)
        Me.bmiOPDPharmacyRefundRequests.Tag = "RefundRequests"
        Me.bmiOPDPharmacyRefundRequests.Text = "Refund Request"
        '
        'bmiOPDPharmacyRefundRequestsDrugs
        '
        Me.bmiOPDPharmacyRefundRequestsDrugs.Name = "bmiOPDPharmacyRefundRequestsDrugs"
        Me.bmiOPDPharmacyRefundRequestsDrugs.Size = New System.Drawing.Size(146, 22)
        Me.bmiOPDPharmacyRefundRequestsDrugs.Text = "Drugs"
        '
        'bmiOPDPharmacyRefundRequestsConsumables
        '
        Me.bmiOPDPharmacyRefundRequestsConsumables.Name = "bmiOPDPharmacyRefundRequestsConsumables"
        Me.bmiOPDPharmacyRefundRequestsConsumables.Size = New System.Drawing.Size(146, 22)
        Me.bmiOPDPharmacyRefundRequestsConsumables.Text = "Consumables"
        '
        'tbbSeparator6
        '
        Me.tbbSeparator6.Name = "tbbSeparator6"
        Me.tbbSeparator6.Size = New System.Drawing.Size(6, 38)
        '
        'ddbInventory
        '
        Me.ddbInventory.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiNewInventory, Me.ToolStripMenuItem63, Me.bmiInventoryConsumables, Me.bmiPharmacyConsumableInventory, Me.bmiInventoryDeliveryNote, Me.bmiPharmacyDrugInventory, Me.mnuInventoryOtherItemsInventory, Me.bmiinventoryInventoryAcknowledges, Me.bmiinventoryProcessInventoryOrders, Me.bmiInventoryGoodsReceived, Me.bmiInventoryGoodsReturned, Me.bmiInventoryAcknowledgeReturns, Me.bminventory, Me.bmiInventoryImportInventory})
        Me.ddbInventory.Image = CType(resources.GetObject("ddbInventory.Image"), System.Drawing.Image)
        Me.ddbInventory.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbInventory.Name = "ddbInventory"
        Me.ddbInventory.Size = New System.Drawing.Size(70, 35)
        Me.ddbInventory.Text = "Inventory"
        Me.ddbInventory.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiNewInventory
        '
        Me.bmiNewInventory.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiNewInventoryPurchaseOrders, Me.bmiNewInventoryInventoryOrders, Me.bmiInventoryNewInventoryTransfers, Me.bmiInventoryNewPhysicalStockCount})
        Me.bmiNewInventory.Image = CType(resources.GetObject("bmiNewInventory.Image"), System.Drawing.Image)
        Me.bmiNewInventory.Name = "bmiNewInventory"
        Me.bmiNewInventory.Size = New System.Drawing.Size(227, 22)
        Me.bmiNewInventory.Tag = ""
        Me.bmiNewInventory.Text = "New"
        '
        'bmiNewInventoryPurchaseOrders
        '
        Me.bmiNewInventoryPurchaseOrders.Image = CType(resources.GetObject("bmiNewInventoryPurchaseOrders.Image"), System.Drawing.Image)
        Me.bmiNewInventoryPurchaseOrders.Name = "bmiNewInventoryPurchaseOrders"
        Me.bmiNewInventoryPurchaseOrders.Size = New System.Drawing.Size(185, 22)
        Me.bmiNewInventoryPurchaseOrders.Tag = "PurchaseOrders"
        Me.bmiNewInventoryPurchaseOrders.Text = "Purchase Orders"
        '
        'bmiNewInventoryInventoryOrders
        '
        Me.bmiNewInventoryInventoryOrders.Image = CType(resources.GetObject("bmiNewInventoryInventoryOrders.Image"), System.Drawing.Image)
        Me.bmiNewInventoryInventoryOrders.Name = "bmiNewInventoryInventoryOrders"
        Me.bmiNewInventoryInventoryOrders.Size = New System.Drawing.Size(185, 22)
        Me.bmiNewInventoryInventoryOrders.Tag = "InventoryOrders"
        Me.bmiNewInventoryInventoryOrders.Text = "Inventory Orders"
        '
        'bmiInventoryNewInventoryTransfers
        '
        Me.bmiInventoryNewInventoryTransfers.Image = CType(resources.GetObject("bmiInventoryNewInventoryTransfers.Image"), System.Drawing.Image)
        Me.bmiInventoryNewInventoryTransfers.Name = "bmiInventoryNewInventoryTransfers"
        Me.bmiInventoryNewInventoryTransfers.Size = New System.Drawing.Size(185, 22)
        Me.bmiInventoryNewInventoryTransfers.Tag = "InventoryTransfers"
        Me.bmiInventoryNewInventoryTransfers.Text = "Inventory Transfers"
        '
        'bmiInventoryNewPhysicalStockCount
        '
        Me.bmiInventoryNewPhysicalStockCount.Image = CType(resources.GetObject("bmiInventoryNewPhysicalStockCount.Image"), System.Drawing.Image)
        Me.bmiInventoryNewPhysicalStockCount.Name = "bmiInventoryNewPhysicalStockCount"
        Me.bmiInventoryNewPhysicalStockCount.Size = New System.Drawing.Size(185, 22)
        Me.bmiInventoryNewPhysicalStockCount.Tag = "PhysicalStockCount"
        Me.bmiInventoryNewPhysicalStockCount.Text = "Physical Stock Count"
        '
        'ToolStripMenuItem63
        '
        Me.ToolStripMenuItem63.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiEditInventoryPurchaseOrders, Me.bmiEditInventoryInventoryOrders, Me.bmiInventoryEditInventoryTransfers, Me.bmiInventoryEditPhysicalStockCount})
        Me.ToolStripMenuItem63.Image = CType(resources.GetObject("ToolStripMenuItem63.Image"), System.Drawing.Image)
        Me.ToolStripMenuItem63.Name = "ToolStripMenuItem63"
        Me.ToolStripMenuItem63.Size = New System.Drawing.Size(227, 22)
        Me.ToolStripMenuItem63.Tag = ""
        Me.ToolStripMenuItem63.Text = "Edit"
        '
        'bmiEditInventoryPurchaseOrders
        '
        Me.bmiEditInventoryPurchaseOrders.Image = CType(resources.GetObject("bmiEditInventoryPurchaseOrders.Image"), System.Drawing.Image)
        Me.bmiEditInventoryPurchaseOrders.Name = "bmiEditInventoryPurchaseOrders"
        Me.bmiEditInventoryPurchaseOrders.Size = New System.Drawing.Size(185, 22)
        Me.bmiEditInventoryPurchaseOrders.Tag = "PurchaseOrders"
        Me.bmiEditInventoryPurchaseOrders.Text = "Purchase Orders"
        '
        'bmiEditInventoryInventoryOrders
        '
        Me.bmiEditInventoryInventoryOrders.Image = CType(resources.GetObject("bmiEditInventoryInventoryOrders.Image"), System.Drawing.Image)
        Me.bmiEditInventoryInventoryOrders.Name = "bmiEditInventoryInventoryOrders"
        Me.bmiEditInventoryInventoryOrders.Size = New System.Drawing.Size(185, 22)
        Me.bmiEditInventoryInventoryOrders.Tag = "InventoryOrders"
        Me.bmiEditInventoryInventoryOrders.Text = "Inventory Orders"
        '
        'bmiInventoryEditInventoryTransfers
        '
        Me.bmiInventoryEditInventoryTransfers.Image = CType(resources.GetObject("bmiInventoryEditInventoryTransfers.Image"), System.Drawing.Image)
        Me.bmiInventoryEditInventoryTransfers.Name = "bmiInventoryEditInventoryTransfers"
        Me.bmiInventoryEditInventoryTransfers.Size = New System.Drawing.Size(185, 22)
        Me.bmiInventoryEditInventoryTransfers.Tag = "InventoryTransfers"
        Me.bmiInventoryEditInventoryTransfers.Text = "Inventory Transfers"
        '
        'bmiInventoryEditPhysicalStockCount
        '
        Me.bmiInventoryEditPhysicalStockCount.Name = "bmiInventoryEditPhysicalStockCount"
        Me.bmiInventoryEditPhysicalStockCount.Size = New System.Drawing.Size(185, 22)
        Me.bmiInventoryEditPhysicalStockCount.Tag = "PhysicalStockCount"
        Me.bmiInventoryEditPhysicalStockCount.Text = "Physical Stock Count"
        '
        'bmiInventoryConsumables
        '
        Me.bmiInventoryConsumables.Image = CType(resources.GetObject("bmiInventoryConsumables.Image"), System.Drawing.Image)
        Me.bmiInventoryConsumables.Name = "bmiInventoryConsumables"
        Me.bmiInventoryConsumables.Size = New System.Drawing.Size(227, 22)
        Me.bmiInventoryConsumables.Tag = "Consumables"
        Me.bmiInventoryConsumables.Text = "Consumables"
        '
        'bmiPharmacyConsumableInventory
        '
        Me.bmiPharmacyConsumableInventory.Image = CType(resources.GetObject("bmiPharmacyConsumableInventory.Image"), System.Drawing.Image)
        Me.bmiPharmacyConsumableInventory.Name = "bmiPharmacyConsumableInventory"
        Me.bmiPharmacyConsumableInventory.Size = New System.Drawing.Size(227, 22)
        Me.bmiPharmacyConsumableInventory.Tag = "ConsumableInventory"
        Me.bmiPharmacyConsumableInventory.Text = "Consumable Inventory"
        '
        'bmiInventoryDeliveryNote
        '
        Me.bmiInventoryDeliveryNote.Image = CType(resources.GetObject("bmiInventoryDeliveryNote.Image"), System.Drawing.Image)
        Me.bmiInventoryDeliveryNote.Name = "bmiInventoryDeliveryNote"
        Me.bmiInventoryDeliveryNote.Size = New System.Drawing.Size(227, 22)
        Me.bmiInventoryDeliveryNote.Tag = "DeliveryNoteDetails"
        Me.bmiInventoryDeliveryNote.Text = "Delivery Note"
        '
        'bmiPharmacyDrugInventory
        '
        Me.bmiPharmacyDrugInventory.Image = CType(resources.GetObject("bmiPharmacyDrugInventory.Image"), System.Drawing.Image)
        Me.bmiPharmacyDrugInventory.Name = "bmiPharmacyDrugInventory"
        Me.bmiPharmacyDrugInventory.Size = New System.Drawing.Size(227, 22)
        Me.bmiPharmacyDrugInventory.Tag = "DrugInventory"
        Me.bmiPharmacyDrugInventory.Text = "Drug Inventory"
        '
        'mnuInventoryOtherItemsInventory
        '
        Me.mnuInventoryOtherItemsInventory.Image = CType(resources.GetObject("mnuInventoryOtherItemsInventory.Image"), System.Drawing.Image)
        Me.mnuInventoryOtherItemsInventory.Name = "mnuInventoryOtherItemsInventory"
        Me.mnuInventoryOtherItemsInventory.Size = New System.Drawing.Size(227, 22)
        Me.mnuInventoryOtherItemsInventory.Tag = "OtherItems"
        Me.mnuInventoryOtherItemsInventory.Text = "Non Medical Items Inventory"
        '
        'bmiinventoryInventoryAcknowledges
        '
        Me.bmiinventoryInventoryAcknowledges.Image = CType(resources.GetObject("bmiinventoryInventoryAcknowledges.Image"), System.Drawing.Image)
        Me.bmiinventoryInventoryAcknowledges.Name = "bmiinventoryInventoryAcknowledges"
        Me.bmiinventoryInventoryAcknowledges.Size = New System.Drawing.Size(227, 22)
        Me.bmiinventoryInventoryAcknowledges.Tag = "InventoryAcknowledges"
        Me.bmiinventoryInventoryAcknowledges.Text = "Inventory Acknowledges"
        '
        'bmiinventoryProcessInventoryOrders
        '
        Me.bmiinventoryProcessInventoryOrders.Image = CType(resources.GetObject("bmiinventoryProcessInventoryOrders.Image"), System.Drawing.Image)
        Me.bmiinventoryProcessInventoryOrders.Name = "bmiinventoryProcessInventoryOrders"
        Me.bmiinventoryProcessInventoryOrders.Size = New System.Drawing.Size(227, 22)
        Me.bmiinventoryProcessInventoryOrders.Tag = "InventoryOrders"
        Me.bmiinventoryProcessInventoryOrders.Text = "Process Inventory Orders"
        '
        'bmiInventoryGoodsReceived
        '
        Me.bmiInventoryGoodsReceived.Image = CType(resources.GetObject("bmiInventoryGoodsReceived.Image"), System.Drawing.Image)
        Me.bmiInventoryGoodsReceived.Name = "bmiInventoryGoodsReceived"
        Me.bmiInventoryGoodsReceived.Size = New System.Drawing.Size(227, 22)
        Me.bmiInventoryGoodsReceived.Tag = "GoodsReceivedNote"
        Me.bmiInventoryGoodsReceived.Text = "Goods Received Note"
        '
        'bmiInventoryGoodsReturned
        '
        Me.bmiInventoryGoodsReturned.Image = CType(resources.GetObject("bmiInventoryGoodsReturned.Image"), System.Drawing.Image)
        Me.bmiInventoryGoodsReturned.Name = "bmiInventoryGoodsReturned"
        Me.bmiInventoryGoodsReturned.Size = New System.Drawing.Size(227, 22)
        Me.bmiInventoryGoodsReturned.Tag = "GoodsReturnedNote"
        Me.bmiInventoryGoodsReturned.Text = "Goods Returned Note"
        '
        'bmiInventoryAcknowledgeReturns
        '
        Me.bmiInventoryAcknowledgeReturns.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiInventoryAcknowledgeReturnsAcknowledgeBillFormReturns, Me.bmiInventoryAcknowledgeReturnsAcknowledgeOPDReturns})
        Me.bmiInventoryAcknowledgeReturns.Image = CType(resources.GetObject("bmiInventoryAcknowledgeReturns.Image"), System.Drawing.Image)
        Me.bmiInventoryAcknowledgeReturns.Name = "bmiInventoryAcknowledgeReturns"
        Me.bmiInventoryAcknowledgeReturns.ShowShortcutKeys = False
        Me.bmiInventoryAcknowledgeReturns.Size = New System.Drawing.Size(227, 22)
        Me.bmiInventoryAcknowledgeReturns.Text = "Acknowledge Returns"
        '
        'bmiInventoryAcknowledgeReturnsAcknowledgeBillFormReturns
        '
        Me.bmiInventoryAcknowledgeReturnsAcknowledgeBillFormReturns.Name = "bmiInventoryAcknowledgeReturnsAcknowledgeBillFormReturns"
        Me.bmiInventoryAcknowledgeReturnsAcknowledgeBillFormReturns.Size = New System.Drawing.Size(239, 22)
        Me.bmiInventoryAcknowledgeReturnsAcknowledgeBillFormReturns.Tag = "OPDReturns"
        Me.bmiInventoryAcknowledgeReturnsAcknowledgeBillFormReturns.Text = "Acknowledge Bill Form Returns"
        '
        'bmiInventoryAcknowledgeReturnsAcknowledgeOPDReturns
        '
        Me.bmiInventoryAcknowledgeReturnsAcknowledgeOPDReturns.Name = "bmiInventoryAcknowledgeReturnsAcknowledgeOPDReturns"
        Me.bmiInventoryAcknowledgeReturnsAcknowledgeOPDReturns.Size = New System.Drawing.Size(239, 22)
        Me.bmiInventoryAcknowledgeReturnsAcknowledgeOPDReturns.Tag = "OPDReturns"
        Me.bmiInventoryAcknowledgeReturnsAcknowledgeOPDReturns.Text = "Acknowledge OPD Returns"
        '
        'bminventory
        '
        Me.bminventory.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bminventoryDrugStockCard, Me.bmiInventoryConsumableStockCard, Me.bmiInventoryDrugInventorySummaries, Me.bmiInventoryConsumableInventorySummaries, Me.bmiInventoryPhysicalStockCountReport})
        Me.bminventory.Image = CType(resources.GetObject("bminventory.Image"), System.Drawing.Image)
        Me.bminventory.Name = "bminventory"
        Me.bminventory.Size = New System.Drawing.Size(227, 22)
        Me.bminventory.Text = "Inventory Reports"
        '
        'bminventoryDrugStockCard
        '
        Me.bminventoryDrugStockCard.Name = "bminventoryDrugStockCard"
        Me.bminventoryDrugStockCard.Size = New System.Drawing.Size(256, 22)
        Me.bminventoryDrugStockCard.Tag = "DrugStockCard"
        Me.bminventoryDrugStockCard.Text = "Drug Stock Card"
        '
        'bmiInventoryConsumableStockCard
        '
        Me.bmiInventoryConsumableStockCard.Name = "bmiInventoryConsumableStockCard"
        Me.bmiInventoryConsumableStockCard.Size = New System.Drawing.Size(256, 22)
        Me.bmiInventoryConsumableStockCard.Tag = "ConsumableStockCard"
        Me.bmiInventoryConsumableStockCard.Text = "Consumable Stock Card"
        '
        'bmiInventoryDrugInventorySummaries
        '
        Me.bmiInventoryDrugInventorySummaries.Name = "bmiInventoryDrugInventorySummaries"
        Me.bmiInventoryDrugInventorySummaries.Size = New System.Drawing.Size(256, 22)
        Me.bmiInventoryDrugInventorySummaries.Tag = "DrugInventorySummaries"
        Me.bmiInventoryDrugInventorySummaries.Text = "Drug Inventory Summaries"
        '
        'bmiInventoryConsumableInventorySummaries
        '
        Me.bmiInventoryConsumableInventorySummaries.Name = "bmiInventoryConsumableInventorySummaries"
        Me.bmiInventoryConsumableInventorySummaries.Size = New System.Drawing.Size(256, 22)
        Me.bmiInventoryConsumableInventorySummaries.Tag = "ConsumableInventorySummaries"
        Me.bmiInventoryConsumableInventorySummaries.Text = "Consumable Inventory Summaries"
        '
        'bmiInventoryPhysicalStockCountReport
        '
        Me.bmiInventoryPhysicalStockCountReport.Name = "bmiInventoryPhysicalStockCountReport"
        Me.bmiInventoryPhysicalStockCountReport.Size = New System.Drawing.Size(256, 22)
        Me.bmiInventoryPhysicalStockCountReport.Tag = "PhysicalStockCountReport"
        Me.bmiInventoryPhysicalStockCountReport.Text = "Physical Stock Count"
        '
        'bmiInventoryImportInventory
        '
        Me.bmiInventoryImportInventory.Image = CType(resources.GetObject("bmiInventoryImportInventory.Image"), System.Drawing.Image)
        Me.bmiInventoryImportInventory.Name = "bmiInventoryImportInventory"
        Me.bmiInventoryImportInventory.Size = New System.Drawing.Size(227, 22)
        Me.bmiInventoryImportInventory.Tag = "Inventory"
        Me.bmiInventoryImportInventory.Text = "Import Inventory"
        '
        'ToolStripSeparator18
        '
        Me.ToolStripSeparator18.Name = "ToolStripSeparator18"
        Me.ToolStripSeparator18.Size = New System.Drawing.Size(6, 38)
        '
        'ddbTheatreOperations
        '
        Me.ddbTheatreOperations.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiTheatreNew, Me.bmiTheatreEdit, Me.ddTheatreRefundReques})
        Me.ddbTheatreOperations.Image = CType(resources.GetObject("ddbTheatreOperations.Image"), System.Drawing.Image)
        Me.ddbTheatreOperations.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbTheatreOperations.Name = "ddbTheatreOperations"
        Me.ddbTheatreOperations.Size = New System.Drawing.Size(59, 35)
        Me.ddbTheatreOperations.Text = "Theatre"
        Me.ddbTheatreOperations.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiTheatreNew
        '
        Me.bmiTheatreNew.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiTheatreNewTheatreOperations, Me.bmiTheatreNewIPDTheatreOperations, Me.bmiTheatreNewIPDPreOperativeForm})
        Me.bmiTheatreNew.Image = CType(resources.GetObject("bmiTheatreNew.Image"), System.Drawing.Image)
        Me.bmiTheatreNew.Name = "bmiTheatreNew"
        Me.bmiTheatreNew.Size = New System.Drawing.Size(157, 22)
        Me.bmiTheatreNew.Tag = ""
        Me.bmiTheatreNew.Text = "New"
        '
        'bmiTheatreNewTheatreOperations
        '
        Me.bmiTheatreNewTheatreOperations.Image = CType(resources.GetObject("bmiTheatreNewTheatreOperations.Image"), System.Drawing.Image)
        Me.bmiTheatreNewTheatreOperations.Name = "bmiTheatreNewTheatreOperations"
        Me.bmiTheatreNewTheatreOperations.Size = New System.Drawing.Size(199, 22)
        Me.bmiTheatreNewTheatreOperations.Tag = "TheatreOperations"
        Me.bmiTheatreNewTheatreOperations.Text = "Theatre Operations"
        '
        'bmiTheatreNewIPDTheatreOperations
        '
        Me.bmiTheatreNewIPDTheatreOperations.Image = CType(resources.GetObject("bmiTheatreNewIPDTheatreOperations.Image"), System.Drawing.Image)
        Me.bmiTheatreNewIPDTheatreOperations.Name = "bmiTheatreNewIPDTheatreOperations"
        Me.bmiTheatreNewIPDTheatreOperations.Size = New System.Drawing.Size(199, 22)
        Me.bmiTheatreNewIPDTheatreOperations.Tag = "IPDTheatreOperations"
        Me.bmiTheatreNewIPDTheatreOperations.Text = "IPD Theatre Operations"
        '
        'bmiTheatreNewIPDPreOperativeForm
        '
        Me.bmiTheatreNewIPDPreOperativeForm.Image = CType(resources.GetObject("bmiTheatreNewIPDPreOperativeForm.Image"), System.Drawing.Image)
        Me.bmiTheatreNewIPDPreOperativeForm.Name = "bmiTheatreNewIPDPreOperativeForm"
        Me.bmiTheatreNewIPDPreOperativeForm.Size = New System.Drawing.Size(199, 22)
        Me.bmiTheatreNewIPDPreOperativeForm.Tag = "Preoperative"
        Me.bmiTheatreNewIPDPreOperativeForm.Text = "IPD Pre-Operative Form"
        '
        'bmiTheatreEdit
        '
        Me.bmiTheatreEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiTheatreEditTheatreOperations, Me.bmiTheatreEditIPDTheatreOperations})
        Me.bmiTheatreEdit.Image = CType(resources.GetObject("bmiTheatreEdit.Image"), System.Drawing.Image)
        Me.bmiTheatreEdit.Name = "bmiTheatreEdit"
        Me.bmiTheatreEdit.Size = New System.Drawing.Size(157, 22)
        Me.bmiTheatreEdit.Tag = ""
        Me.bmiTheatreEdit.Text = "Edit"
        '
        'bmiTheatreEditTheatreOperations
        '
        Me.bmiTheatreEditTheatreOperations.Image = CType(resources.GetObject("bmiTheatreEditTheatreOperations.Image"), System.Drawing.Image)
        Me.bmiTheatreEditTheatreOperations.Name = "bmiTheatreEditTheatreOperations"
        Me.bmiTheatreEditTheatreOperations.Size = New System.Drawing.Size(195, 22)
        Me.bmiTheatreEditTheatreOperations.Tag = "TheatreOperations"
        Me.bmiTheatreEditTheatreOperations.Text = "Theatre Operations"
        '
        'bmiTheatreEditIPDTheatreOperations
        '
        Me.bmiTheatreEditIPDTheatreOperations.Image = CType(resources.GetObject("bmiTheatreEditIPDTheatreOperations.Image"), System.Drawing.Image)
        Me.bmiTheatreEditIPDTheatreOperations.Name = "bmiTheatreEditIPDTheatreOperations"
        Me.bmiTheatreEditIPDTheatreOperations.Size = New System.Drawing.Size(195, 22)
        Me.bmiTheatreEditIPDTheatreOperations.Tag = "IPDTheatreOperations"
        Me.bmiTheatreEditIPDTheatreOperations.Text = "IPD Theatre Operations"
        '
        'ddTheatreRefundReques
        '
        Me.ddTheatreRefundReques.Image = CType(resources.GetObject("ddTheatreRefundReques.Image"), System.Drawing.Image)
        Me.ddTheatreRefundReques.Name = "ddTheatreRefundReques"
        Me.ddTheatreRefundReques.Size = New System.Drawing.Size(157, 22)
        Me.ddTheatreRefundReques.Tag = "RefundRequests"
        Me.ddTheatreRefundReques.Text = "Refund Request"
        '
        'tbbSeparator7
        '
        Me.tbbSeparator7.Name = "tbbSeparator7"
        Me.tbbSeparator7.Size = New System.Drawing.Size(6, 38)
        '
        'ddbDental
        '
        Me.ddbDental.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiDentalNew, Me.bmiDentalEdit, Me.ddDentalRefundReques})
        Me.ddbDental.Image = CType(resources.GetObject("ddbDental.Image"), System.Drawing.Image)
        Me.ddbDental.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbDental.Name = "ddbDental"
        Me.ddbDental.Size = New System.Drawing.Size(54, 35)
        Me.ddbDental.Text = "Dental"
        Me.ddbDental.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiDentalNew
        '
        Me.bmiDentalNew.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiDentalNewDentalReports, Me.bmiDentalNewIPDDentalReports})
        Me.bmiDentalNew.Image = CType(resources.GetObject("bmiDentalNew.Image"), System.Drawing.Image)
        Me.bmiDentalNew.Name = "bmiDentalNew"
        Me.bmiDentalNew.Size = New System.Drawing.Size(157, 22)
        Me.bmiDentalNew.Tag = ""
        Me.bmiDentalNew.Text = "New"
        '
        'bmiDentalNewDentalReports
        '
        Me.bmiDentalNewDentalReports.Image = CType(resources.GetObject("bmiDentalNewDentalReports.Image"), System.Drawing.Image)
        Me.bmiDentalNewDentalReports.Name = "bmiDentalNewDentalReports"
        Me.bmiDentalNewDentalReports.Size = New System.Drawing.Size(172, 22)
        Me.bmiDentalNewDentalReports.Tag = "DentalReports"
        Me.bmiDentalNewDentalReports.Text = "Dental Reports"
        '
        'bmiDentalNewIPDDentalReports
        '
        Me.bmiDentalNewIPDDentalReports.Image = CType(resources.GetObject("bmiDentalNewIPDDentalReports.Image"), System.Drawing.Image)
        Me.bmiDentalNewIPDDentalReports.Name = "bmiDentalNewIPDDentalReports"
        Me.bmiDentalNewIPDDentalReports.Size = New System.Drawing.Size(172, 22)
        Me.bmiDentalNewIPDDentalReports.Tag = "IPDDentalReports"
        Me.bmiDentalNewIPDDentalReports.Text = "IPD Dental Reports"
        '
        'bmiDentalEdit
        '
        Me.bmiDentalEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiDentalEditDentalReports, Me.bmiDentalEditIPDDentalReports})
        Me.bmiDentalEdit.Image = CType(resources.GetObject("bmiDentalEdit.Image"), System.Drawing.Image)
        Me.bmiDentalEdit.Name = "bmiDentalEdit"
        Me.bmiDentalEdit.Size = New System.Drawing.Size(157, 22)
        Me.bmiDentalEdit.Tag = ""
        Me.bmiDentalEdit.Text = "Edit"
        '
        'bmiDentalEditDentalReports
        '
        Me.bmiDentalEditDentalReports.Image = CType(resources.GetObject("bmiDentalEditDentalReports.Image"), System.Drawing.Image)
        Me.bmiDentalEditDentalReports.Name = "bmiDentalEditDentalReports"
        Me.bmiDentalEditDentalReports.Size = New System.Drawing.Size(172, 22)
        Me.bmiDentalEditDentalReports.Tag = "DentalReports"
        Me.bmiDentalEditDentalReports.Text = "Dental Reports"
        '
        'bmiDentalEditIPDDentalReports
        '
        Me.bmiDentalEditIPDDentalReports.Image = CType(resources.GetObject("bmiDentalEditIPDDentalReports.Image"), System.Drawing.Image)
        Me.bmiDentalEditIPDDentalReports.Name = "bmiDentalEditIPDDentalReports"
        Me.bmiDentalEditIPDDentalReports.Size = New System.Drawing.Size(172, 22)
        Me.bmiDentalEditIPDDentalReports.Tag = "IPDDentalReports"
        Me.bmiDentalEditIPDDentalReports.Text = "IPD Dental Reports"
        '
        'ddDentalRefundReques
        '
        Me.ddDentalRefundReques.Image = CType(resources.GetObject("ddDentalRefundReques.Image"), System.Drawing.Image)
        Me.ddDentalRefundReques.Name = "ddDentalRefundReques"
        Me.ddDentalRefundReques.Size = New System.Drawing.Size(157, 22)
        Me.ddDentalRefundReques.Tag = "RefundRequests"
        Me.ddDentalRefundReques.Text = "Refund Request"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(6, 38)
        '
        'ddbAppointments
        '
        Me.ddbAppointments.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiAppointmentsNew, Me.bmiAppointmentsEdit})
        Me.ddbAppointments.Image = CType(resources.GetObject("ddbAppointments.Image"), System.Drawing.Image)
        Me.ddbAppointments.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbAppointments.Name = "ddbAppointments"
        Me.ddbAppointments.Size = New System.Drawing.Size(96, 35)
        Me.ddbAppointments.Text = "Appointments"
        Me.ddbAppointments.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiAppointmentsNew
        '
        Me.bmiAppointmentsNew.Image = CType(resources.GetObject("bmiAppointmentsNew.Image"), System.Drawing.Image)
        Me.bmiAppointmentsNew.Name = "bmiAppointmentsNew"
        Me.bmiAppointmentsNew.Size = New System.Drawing.Size(98, 22)
        Me.bmiAppointmentsNew.Tag = "Appointments"
        Me.bmiAppointmentsNew.Text = "New"
        '
        'bmiAppointmentsEdit
        '
        Me.bmiAppointmentsEdit.Image = CType(resources.GetObject("bmiAppointmentsEdit.Image"), System.Drawing.Image)
        Me.bmiAppointmentsEdit.Name = "bmiAppointmentsEdit"
        Me.bmiAppointmentsEdit.Size = New System.Drawing.Size(98, 22)
        Me.bmiAppointmentsEdit.Tag = "Appointments"
        Me.bmiAppointmentsEdit.Text = "Edit"
        '
        'tbbSeparator10
        '
        Me.tbbSeparator10.Name = "tbbSeparator10"
        Me.tbbSeparator10.Size = New System.Drawing.Size(6, 38)
        '
        'ddbInPatients
        '
        Me.ddbInPatients.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiInPatientsNew, Me.bmiInPatientsEdit, Me.bmiInPatientsIPDDoctor, Me.bmiInPatientsIPDConsumables, Me.bmiInPatientsBillAdjustments, Me.bmiInPatientsIPDNurse, Me.bmniInPatientsIPDCancerDiagnosis, Me.bmniInPatientsSmartBilling, Me.bmniInPatientsPara})
        Me.ddbInPatients.Image = CType(resources.GetObject("ddbInPatients.Image"), System.Drawing.Image)
        Me.ddbInPatients.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbInPatients.Name = "ddbInPatients"
        Me.ddbInPatients.Size = New System.Drawing.Size(75, 35)
        Me.ddbInPatients.Text = "In Patients"
        Me.ddbInPatients.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiInPatientsNew
        '
        Me.bmiInPatientsNew.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiInPatientsNewAdmissions, Me.bmiInPatientsNewExtraBills, Me.bmiInPatientsNewDischarges})
        Me.bmiInPatientsNew.Image = CType(resources.GetObject("bmiInPatientsNew.Image"), System.Drawing.Image)
        Me.bmiInPatientsNew.Name = "bmiInPatientsNew"
        Me.bmiInPatientsNew.Size = New System.Drawing.Size(186, 22)
        Me.bmiInPatientsNew.Tag = ""
        Me.bmiInPatientsNew.Text = "New"
        '
        'bmiInPatientsNewAdmissions
        '
        Me.bmiInPatientsNewAdmissions.Image = CType(resources.GetObject("bmiInPatientsNewAdmissions.Image"), System.Drawing.Image)
        Me.bmiInPatientsNewAdmissions.Name = "bmiInPatientsNewAdmissions"
        Me.bmiInPatientsNewAdmissions.Size = New System.Drawing.Size(138, 22)
        Me.bmiInPatientsNewAdmissions.Tag = "Admissions"
        Me.bmiInPatientsNewAdmissions.Text = "Admissions"
        '
        'bmiInPatientsNewExtraBills
        '
        Me.bmiInPatientsNewExtraBills.Image = CType(resources.GetObject("bmiInPatientsNewExtraBills.Image"), System.Drawing.Image)
        Me.bmiInPatientsNewExtraBills.Name = "bmiInPatientsNewExtraBills"
        Me.bmiInPatientsNewExtraBills.Size = New System.Drawing.Size(138, 22)
        Me.bmiInPatientsNewExtraBills.Tag = "ExtraBills"
        Me.bmiInPatientsNewExtraBills.Text = "Billing Form"
        '
        'bmiInPatientsNewDischarges
        '
        Me.bmiInPatientsNewDischarges.Image = CType(resources.GetObject("bmiInPatientsNewDischarges.Image"), System.Drawing.Image)
        Me.bmiInPatientsNewDischarges.Name = "bmiInPatientsNewDischarges"
        Me.bmiInPatientsNewDischarges.Size = New System.Drawing.Size(138, 22)
        Me.bmiInPatientsNewDischarges.Tag = "Discharges"
        Me.bmiInPatientsNewDischarges.Text = "Discharges"
        '
        'bmiInPatientsEdit
        '
        Me.bmiInPatientsEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiInPatientsEditAdmissions, Me.bmiInPatientsEditDischarges})
        Me.bmiInPatientsEdit.Image = CType(resources.GetObject("bmiInPatientsEdit.Image"), System.Drawing.Image)
        Me.bmiInPatientsEdit.Name = "bmiInPatientsEdit"
        Me.bmiInPatientsEdit.Size = New System.Drawing.Size(186, 22)
        Me.bmiInPatientsEdit.Tag = ""
        Me.bmiInPatientsEdit.Text = "Edit"
        '
        'bmiInPatientsEditAdmissions
        '
        Me.bmiInPatientsEditAdmissions.Image = CType(resources.GetObject("bmiInPatientsEditAdmissions.Image"), System.Drawing.Image)
        Me.bmiInPatientsEditAdmissions.Name = "bmiInPatientsEditAdmissions"
        Me.bmiInPatientsEditAdmissions.Size = New System.Drawing.Size(135, 22)
        Me.bmiInPatientsEditAdmissions.Tag = "Admissions"
        Me.bmiInPatientsEditAdmissions.Text = "Admissions"
        '
        'bmiInPatientsEditDischarges
        '
        Me.bmiInPatientsEditDischarges.Image = CType(resources.GetObject("bmiInPatientsEditDischarges.Image"), System.Drawing.Image)
        Me.bmiInPatientsEditDischarges.Name = "bmiInPatientsEditDischarges"
        Me.bmiInPatientsEditDischarges.Size = New System.Drawing.Size(135, 22)
        Me.bmiInPatientsEditDischarges.Tag = "Discharges"
        Me.bmiInPatientsEditDischarges.Text = "Discharges"
        '
        'bmiInPatientsIPDDoctor
        '
        Me.bmiInPatientsIPDDoctor.Image = CType(resources.GetObject("bmiInPatientsIPDDoctor.Image"), System.Drawing.Image)
        Me.bmiInPatientsIPDDoctor.Name = "bmiInPatientsIPDDoctor"
        Me.bmiInPatientsIPDDoctor.Size = New System.Drawing.Size(186, 22)
        Me.bmiInPatientsIPDDoctor.Tag = "IPDDoctor"
        Me.bmiInPatientsIPDDoctor.Text = "IPD Doctor"
        '
        'bmiInPatientsIPDConsumables
        '
        Me.bmiInPatientsIPDConsumables.Image = CType(resources.GetObject("bmiInPatientsIPDConsumables.Image"), System.Drawing.Image)
        Me.bmiInPatientsIPDConsumables.Name = "bmiInPatientsIPDConsumables"
        Me.bmiInPatientsIPDConsumables.Size = New System.Drawing.Size(186, 22)
        Me.bmiInPatientsIPDConsumables.Tag = "IPDConsumables"
        Me.bmiInPatientsIPDConsumables.Text = "IPD Consumables"
        '
        'bmiInPatientsBillAdjustments
        '
        Me.bmiInPatientsBillAdjustments.Image = CType(resources.GetObject("bmiInPatientsBillAdjustments.Image"), System.Drawing.Image)
        Me.bmiInPatientsBillAdjustments.Name = "bmiInPatientsBillAdjustments"
        Me.bmiInPatientsBillAdjustments.Size = New System.Drawing.Size(186, 22)
        Me.bmiInPatientsBillAdjustments.Tag = "BillAdjustments"
        Me.bmiInPatientsBillAdjustments.Text = "Bill Adjustments"
        '
        'bmiInPatientsIPDNurse
        '
        Me.bmiInPatientsIPDNurse.Image = CType(resources.GetObject("bmiInPatientsIPDNurse.Image"), System.Drawing.Image)
        Me.bmiInPatientsIPDNurse.Name = "bmiInPatientsIPDNurse"
        Me.bmiInPatientsIPDNurse.Size = New System.Drawing.Size(186, 22)
        Me.bmiInPatientsIPDNurse.Tag = "IPDNurse"
        Me.bmiInPatientsIPDNurse.Text = "IPD Nurse"
        '
        'bmniInPatientsIPDCancerDiagnosis
        '
        Me.bmniInPatientsIPDCancerDiagnosis.Image = CType(resources.GetObject("bmniInPatientsIPDCancerDiagnosis.Image"), System.Drawing.Image)
        Me.bmniInPatientsIPDCancerDiagnosis.Name = "bmniInPatientsIPDCancerDiagnosis"
        Me.bmniInPatientsIPDCancerDiagnosis.Size = New System.Drawing.Size(186, 22)
        Me.bmniInPatientsIPDCancerDiagnosis.Tag = "IPDCancerDiagnosis"
        Me.bmniInPatientsIPDCancerDiagnosis.Text = "IPD Cancer Diagnosis"
        '
        'bmniInPatientsSmartBilling
        '
        Me.bmniInPatientsSmartBilling.Name = "bmniInPatientsSmartBilling"
        Me.bmniInPatientsSmartBilling.Size = New System.Drawing.Size(186, 22)
        Me.bmniInPatientsSmartBilling.Tag = "SmartBilling"
        Me.bmniInPatientsSmartBilling.Text = "Smart Billing"
        '
        'bmniInPatientsPara
        '
        Me.bmniInPatientsPara.Name = "bmniInPatientsPara"
        Me.bmniInPatientsPara.Size = New System.Drawing.Size(186, 22)
        Me.bmniInPatientsPara.Text = "Para"
        '
        'ToolStripSeparator17
        '
        Me.ToolStripSeparator17.Name = "ToolStripSeparator17"
        Me.ToolStripSeparator17.Size = New System.Drawing.Size(6, 38)
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 38)
        '
        'ddbFinances
        '
        Me.ddbFinances.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbFinancesAccessCashServices, Me.ToolStripMenuItem14, Me.ddbFinancesAccountStatements, Me.ToolStripMenuItem23, Me.mnuFinancesAssets, Me.ToolStripMenuItem38, Me.ddbFinancesInventory, Me.ToolStripMenuItem34, Me.ddbFinancesCashier, Me.ToolStripMenuItem19, Me.ddbFinancesClaims, Me.ddbFinancesBillFormAdjustment, Me.ToolStripMenuItem13, Me.ddbFinancesInvoiceAdjustments, Me.ToolStripMenuItem11, Me.ddbFinancesBanking, Me.ToolStripMenuItem26, Me.ddbFinancesClaimPayments, Me.ToolStripMenuItem17, Me.ddbFinancesIncome, Me.ToolStripMenuItem27, Me.ddbFinancesInvoices, Me.ToolStripMenuItem28, Me.ddbFinancesRefunds, Me.ddbFinancesAccountActivations, Me.ddbFinancesAccountWithdraws, Me.ddbFinancesSuspenseAccount, Me.ToolStripMenuItem15, Me.ddbFinancesStaffPaymentApprovals, Me.ToolStripMenuItem33, Me.ddbFinancesSmartBilling, Me.MobileMoneyPaymentsToolStripMenuItem})
        Me.ddbFinances.Image = CType(resources.GetObject("ddbFinances.Image"), System.Drawing.Image)
        Me.ddbFinances.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbFinances.Name = "ddbFinances"
        Me.ddbFinances.Size = New System.Drawing.Size(66, 35)
        Me.ddbFinances.Text = "Finances"
        Me.ddbFinances.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ddbFinancesAccessCashServices
        '
        Me.ddbFinancesAccessCashServices.Image = CType(resources.GetObject("ddbFinancesAccessCashServices.Image"), System.Drawing.Image)
        Me.ddbFinancesAccessCashServices.Name = "ddbFinancesAccessCashServices"
        Me.ddbFinancesAccessCashServices.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesAccessCashServices.Tag = "AccessedCashServices"
        Me.ddbFinancesAccessCashServices.Text = "Access Cash Services"
        '
        'ToolStripMenuItem14
        '
        Me.ToolStripMenuItem14.Name = "ToolStripMenuItem14"
        Me.ToolStripMenuItem14.Size = New System.Drawing.Size(203, 6)
        '
        'ddbFinancesAccountStatements
        '
        Me.ddbFinancesAccountStatements.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbFinancesAccountStatement, Me.ToolStripMenuItem22, Me.ddbFinancesDetailedAccountStatement})
        Me.ddbFinancesAccountStatements.Image = CType(resources.GetObject("ddbFinancesAccountStatements.Image"), System.Drawing.Image)
        Me.ddbFinancesAccountStatements.Name = "ddbFinancesAccountStatements"
        Me.ddbFinancesAccountStatements.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesAccountStatements.Tag = "Payments"
        Me.ddbFinancesAccountStatements.Text = "Account Statements"
        '
        'ddbFinancesAccountStatement
        '
        Me.ddbFinancesAccountStatement.Name = "ddbFinancesAccountStatement"
        Me.ddbFinancesAccountStatement.Size = New System.Drawing.Size(222, 22)
        Me.ddbFinancesAccountStatement.Tag = "CashReceipts"
        Me.ddbFinancesAccountStatement.Text = "Account Statement"
        '
        'ToolStripMenuItem22
        '
        Me.ToolStripMenuItem22.Name = "ToolStripMenuItem22"
        Me.ToolStripMenuItem22.Size = New System.Drawing.Size(219, 6)
        '
        'ddbFinancesDetailedAccountStatement
        '
        Me.ddbFinancesDetailedAccountStatement.Name = "ddbFinancesDetailedAccountStatement"
        Me.ddbFinancesDetailedAccountStatement.Size = New System.Drawing.Size(222, 22)
        Me.ddbFinancesDetailedAccountStatement.Tag = "CashReceipts"
        Me.ddbFinancesDetailedAccountStatement.Text = "Detailed Account Statement"
        '
        'ToolStripMenuItem23
        '
        Me.ToolStripMenuItem23.Name = "ToolStripMenuItem23"
        Me.ToolStripMenuItem23.Size = New System.Drawing.Size(203, 6)
        '
        'mnuFinancesAssets
        '
        Me.mnuFinancesAssets.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbFinancesNewAsse, Me.ToolStripMenuItem37, Me.ddbFinancesAssetMaintenanceLog})
        Me.mnuFinancesAssets.Image = CType(resources.GetObject("mnuFinancesAssets.Image"), System.Drawing.Image)
        Me.mnuFinancesAssets.Name = "mnuFinancesAssets"
        Me.mnuFinancesAssets.Size = New System.Drawing.Size(206, 22)
        Me.mnuFinancesAssets.Tag = "AssetRegister"
        Me.mnuFinancesAssets.Text = "Assets"
        '
        'ddbFinancesNewAsse
        '
        Me.ddbFinancesNewAsse.Name = "ddbFinancesNewAsse"
        Me.ddbFinancesNewAsse.Size = New System.Drawing.Size(197, 22)
        Me.ddbFinancesNewAsse.Tag = "AssetRegister"
        Me.ddbFinancesNewAsse.Text = "New Asset"
        '
        'ToolStripMenuItem37
        '
        Me.ToolStripMenuItem37.Name = "ToolStripMenuItem37"
        Me.ToolStripMenuItem37.Size = New System.Drawing.Size(194, 6)
        '
        'ddbFinancesAssetMaintenanceLog
        '
        Me.ddbFinancesAssetMaintenanceLog.Name = "ddbFinancesAssetMaintenanceLog"
        Me.ddbFinancesAssetMaintenanceLog.Size = New System.Drawing.Size(197, 22)
        Me.ddbFinancesAssetMaintenanceLog.Tag = "AssetMaintainanceLog"
        Me.ddbFinancesAssetMaintenanceLog.Text = "Asset MaintainanceLog"
        '
        'ToolStripMenuItem38
        '
        Me.ToolStripMenuItem38.Name = "ToolStripMenuItem38"
        Me.ToolStripMenuItem38.Size = New System.Drawing.Size(203, 6)
        '
        'ddbFinancesInventory
        '
        Me.ddbFinancesInventory.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbFinancesInventoryDrugInventorySummaries, Me.ToolStripMenuItem35, Me.ddbFinancesInventoryConsumableInventorySummaries, Me.ToolStripMenuItem39, Me.ddbFinancesInventoryConsumableInventoryPayments, Me.ToolStripMenuItem40, Me.PaymentVoucherBalances})
        Me.ddbFinancesInventory.Image = CType(resources.GetObject("ddbFinancesInventory.Image"), System.Drawing.Image)
        Me.ddbFinancesInventory.Name = "ddbFinancesInventory"
        Me.ddbFinancesInventory.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesInventory.Tag = "Inventory"
        Me.ddbFinancesInventory.Text = "Inventory"
        '
        'ddbFinancesInventoryDrugInventorySummaries
        '
        Me.ddbFinancesInventoryDrugInventorySummaries.Name = "ddbFinancesInventoryDrugInventorySummaries"
        Me.ddbFinancesInventoryDrugInventorySummaries.Size = New System.Drawing.Size(256, 22)
        Me.ddbFinancesInventoryDrugInventorySummaries.Tag = "DrugInventorySummaries"
        Me.ddbFinancesInventoryDrugInventorySummaries.Text = "Drug Inventory Summaries"
        '
        'ToolStripMenuItem35
        '
        Me.ToolStripMenuItem35.Name = "ToolStripMenuItem35"
        Me.ToolStripMenuItem35.Size = New System.Drawing.Size(253, 6)
        '
        'ddbFinancesInventoryConsumableInventorySummaries
        '
        Me.ddbFinancesInventoryConsumableInventorySummaries.Name = "ddbFinancesInventoryConsumableInventorySummaries"
        Me.ddbFinancesInventoryConsumableInventorySummaries.Size = New System.Drawing.Size(256, 22)
        Me.ddbFinancesInventoryConsumableInventorySummaries.Tag = "ConsumableInventorySummaries"
        Me.ddbFinancesInventoryConsumableInventorySummaries.Text = "Consumable Inventory Summaries"
        '
        'ToolStripMenuItem39
        '
        Me.ToolStripMenuItem39.Name = "ToolStripMenuItem39"
        Me.ToolStripMenuItem39.Size = New System.Drawing.Size(253, 6)
        '
        'ddbFinancesInventoryConsumableInventoryPayments
        '
        Me.ddbFinancesInventoryConsumableInventoryPayments.Name = "ddbFinancesInventoryConsumableInventoryPayments"
        Me.ddbFinancesInventoryConsumableInventoryPayments.Size = New System.Drawing.Size(256, 22)
        Me.ddbFinancesInventoryConsumableInventoryPayments.Tag = "PaymentVouchers"
        Me.ddbFinancesInventoryConsumableInventoryPayments.Text = "Payments"
        '
        'ToolStripMenuItem40
        '
        Me.ToolStripMenuItem40.Name = "ToolStripMenuItem40"
        Me.ToolStripMenuItem40.Size = New System.Drawing.Size(253, 6)
        '
        'PaymentVoucherBalances
        '
        Me.PaymentVoucherBalances.Name = "PaymentVoucherBalances"
        Me.PaymentVoucherBalances.Size = New System.Drawing.Size(256, 22)
        Me.PaymentVoucherBalances.Tag = "PaymentVouchers"
        Me.PaymentVoucherBalances.Text = "Payment Voucher Balances"
        '
        'ToolStripMenuItem34
        '
        Me.ToolStripMenuItem34.Name = "ToolStripMenuItem34"
        Me.ToolStripMenuItem34.Size = New System.Drawing.Size(203, 6)
        '
        'ddbFinancesCashier
        '
        Me.ddbFinancesCashier.Image = CType(resources.GetObject("ddbFinancesCashier.Image"), System.Drawing.Image)
        Me.ddbFinancesCashier.Name = "ddbFinancesCashier"
        Me.ddbFinancesCashier.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesCashier.Tag = "Payments"
        Me.ddbFinancesCashier.Text = "Cashier"
        '
        'ToolStripMenuItem19
        '
        Me.ToolStripMenuItem19.Name = "ToolStripMenuItem19"
        Me.ToolStripMenuItem19.Size = New System.Drawing.Size(203, 6)
        '
        'ddbFinancesClaims
        '
        Me.ddbFinancesClaims.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbFinancesClaimsToBillCustomersClaimForm, Me.ToolStripMenuItem12, Me.ddbFinancesClaimsInsuranceClaimForm, Me.ToolStripMenuItem18, Me.ddbFinancesInsuranceClaimSummaries})
        Me.ddbFinancesClaims.Image = CType(resources.GetObject("ddbFinancesClaims.Image"), System.Drawing.Image)
        Me.ddbFinancesClaims.Name = "ddbFinancesClaims"
        Me.ddbFinancesClaims.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesClaims.Tag = "Claims"
        Me.ddbFinancesClaims.Text = "Claims"
        '
        'ddbFinancesClaimsToBillCustomersClaimForm
        '
        Me.ddbFinancesClaimsToBillCustomersClaimForm.Name = "ddbFinancesClaimsToBillCustomersClaimForm"
        Me.ddbFinancesClaimsToBillCustomersClaimForm.Size = New System.Drawing.Size(232, 22)
        Me.ddbFinancesClaimsToBillCustomersClaimForm.Tag = "Invoices"
        Me.ddbFinancesClaimsToBillCustomersClaimForm.Text = "To-Bill Customers Claim Form"
        '
        'ToolStripMenuItem12
        '
        Me.ToolStripMenuItem12.Name = "ToolStripMenuItem12"
        Me.ToolStripMenuItem12.Size = New System.Drawing.Size(229, 6)
        '
        'ddbFinancesClaimsInsuranceClaimForm
        '
        Me.ddbFinancesClaimsInsuranceClaimForm.Name = "ddbFinancesClaimsInsuranceClaimForm"
        Me.ddbFinancesClaimsInsuranceClaimForm.Size = New System.Drawing.Size(232, 22)
        Me.ddbFinancesClaimsInsuranceClaimForm.Tag = "Claims"
        Me.ddbFinancesClaimsInsuranceClaimForm.Text = "Insurance Claim Form"
        '
        'ToolStripMenuItem18
        '
        Me.ToolStripMenuItem18.Name = "ToolStripMenuItem18"
        Me.ToolStripMenuItem18.Size = New System.Drawing.Size(229, 6)
        '
        'ddbFinancesInsuranceClaimSummaries
        '
        Me.ddbFinancesInsuranceClaimSummaries.Name = "ddbFinancesInsuranceClaimSummaries"
        Me.ddbFinancesInsuranceClaimSummaries.Size = New System.Drawing.Size(232, 22)
        Me.ddbFinancesInsuranceClaimSummaries.Tag = "Claims"
        Me.ddbFinancesInsuranceClaimSummaries.Text = "Insurance Claim Summaries"
        '
        'ddbFinancesBillFormAdjustment
        '
        Me.ddbFinancesBillFormAdjustment.Image = CType(resources.GetObject("ddbFinancesBillFormAdjustment.Image"), System.Drawing.Image)
        Me.ddbFinancesBillFormAdjustment.Name = "ddbFinancesBillFormAdjustment"
        Me.ddbFinancesBillFormAdjustment.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesBillFormAdjustment.Tag = "BillAdjustments"
        Me.ddbFinancesBillFormAdjustment.Text = "Bill Form Adjustments"
        '
        'ToolStripMenuItem13
        '
        Me.ToolStripMenuItem13.Name = "ToolStripMenuItem13"
        Me.ToolStripMenuItem13.Size = New System.Drawing.Size(203, 6)
        '
        'ddbFinancesInvoiceAdjustments
        '
        Me.ddbFinancesInvoiceAdjustments.Image = CType(resources.GetObject("ddbFinancesInvoiceAdjustments.Image"), System.Drawing.Image)
        Me.ddbFinancesInvoiceAdjustments.Name = "ddbFinancesInvoiceAdjustments"
        Me.ddbFinancesInvoiceAdjustments.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesInvoiceAdjustments.Tag = "InvoiceAdjustments"
        Me.ddbFinancesInvoiceAdjustments.Text = "Invoice Adjustments"
        '
        'ToolStripMenuItem11
        '
        Me.ToolStripMenuItem11.Name = "ToolStripMenuItem11"
        Me.ToolStripMenuItem11.Size = New System.Drawing.Size(203, 6)
        '
        'ddbFinancesBanking
        '
        Me.ddbFinancesBanking.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbFinancesBankingNewBankAccount, Me.ToolStripMenuItem24, Me.ddbFinancesBankingRegister, Me.ToolStripMenuItem25, Me.ddbFinancesBankingReport})
        Me.ddbFinancesBanking.Image = CType(resources.GetObject("ddbFinancesBanking.Image"), System.Drawing.Image)
        Me.ddbFinancesBanking.Name = "ddbFinancesBanking"
        Me.ddbFinancesBanking.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesBanking.Tag = "BankingRegister"
        Me.ddbFinancesBanking.Text = "Banking"
        '
        'ddbFinancesBankingNewBankAccount
        '
        Me.ddbFinancesBankingNewBankAccount.Name = "ddbFinancesBankingNewBankAccount"
        Me.ddbFinancesBankingNewBankAccount.Size = New System.Drawing.Size(175, 22)
        Me.ddbFinancesBankingNewBankAccount.Tag = "BankAccounts"
        Me.ddbFinancesBankingNewBankAccount.Text = "New Bank Account"
        '
        'ToolStripMenuItem24
        '
        Me.ToolStripMenuItem24.Name = "ToolStripMenuItem24"
        Me.ToolStripMenuItem24.Size = New System.Drawing.Size(172, 6)
        '
        'ddbFinancesBankingRegister
        '
        Me.ddbFinancesBankingRegister.Name = "ddbFinancesBankingRegister"
        Me.ddbFinancesBankingRegister.Size = New System.Drawing.Size(175, 22)
        Me.ddbFinancesBankingRegister.Tag = "BankingRegister"
        Me.ddbFinancesBankingRegister.Text = "Banking Register"
        '
        'ToolStripMenuItem25
        '
        Me.ToolStripMenuItem25.Name = "ToolStripMenuItem25"
        Me.ToolStripMenuItem25.Size = New System.Drawing.Size(172, 6)
        '
        'ddbFinancesBankingReport
        '
        Me.ddbFinancesBankingReport.Name = "ddbFinancesBankingReport"
        Me.ddbFinancesBankingReport.Size = New System.Drawing.Size(175, 22)
        Me.ddbFinancesBankingReport.Tag = "BankingRegister"
        Me.ddbFinancesBankingReport.Text = "Banking Report"
        '
        'ToolStripMenuItem26
        '
        Me.ToolStripMenuItem26.Name = "ToolStripMenuItem26"
        Me.ToolStripMenuItem26.Size = New System.Drawing.Size(203, 6)
        '
        'ddbFinancesClaimPayments
        '
        Me.ddbFinancesClaimPayments.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbFinancesClaimPaymentsClaimPayments, Me.ToolStripMenuItem16, Me.ddbFinancesClaimPaymentsClaimPaymentDetailed})
        Me.ddbFinancesClaimPayments.Image = CType(resources.GetObject("ddbFinancesClaimPayments.Image"), System.Drawing.Image)
        Me.ddbFinancesClaimPayments.Name = "ddbFinancesClaimPayments"
        Me.ddbFinancesClaimPayments.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesClaimPayments.Tag = "Payments"
        Me.ddbFinancesClaimPayments.Text = "Claim Payments"
        '
        'ddbFinancesClaimPaymentsClaimPayments
        '
        Me.ddbFinancesClaimPaymentsClaimPayments.Name = "ddbFinancesClaimPaymentsClaimPayments"
        Me.ddbFinancesClaimPaymentsClaimPayments.Size = New System.Drawing.Size(201, 22)
        Me.ddbFinancesClaimPaymentsClaimPayments.Tag = "ClaimPayments"
        Me.ddbFinancesClaimPaymentsClaimPayments.Text = "Claim Payments"
        '
        'ToolStripMenuItem16
        '
        Me.ToolStripMenuItem16.Name = "ToolStripMenuItem16"
        Me.ToolStripMenuItem16.Size = New System.Drawing.Size(198, 6)
        '
        'ddbFinancesClaimPaymentsClaimPaymentDetailed
        '
        Me.ddbFinancesClaimPaymentsClaimPaymentDetailed.Name = "ddbFinancesClaimPaymentsClaimPaymentDetailed"
        Me.ddbFinancesClaimPaymentsClaimPaymentDetailed.Size = New System.Drawing.Size(201, 22)
        Me.ddbFinancesClaimPaymentsClaimPaymentDetailed.Tag = "ClaimDetails"
        Me.ddbFinancesClaimPaymentsClaimPaymentDetailed.Text = "Claim Payment Detailed"
        '
        'ToolStripMenuItem17
        '
        Me.ToolStripMenuItem17.Name = "ToolStripMenuItem17"
        Me.ToolStripMenuItem17.Size = New System.Drawing.Size(203, 6)
        '
        'ddbFinancesIncome
        '
        Me.ddbFinancesIncome.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbFinancesIncomeCashCollections, Me.ddbFinancesOPDIncomeSummaries, Me.ddbFinancesIPDIncomeSummaries, Me.btnFinCollectionBreakDownIncomeCollectionBreakdown})
        Me.ddbFinancesIncome.Image = CType(resources.GetObject("ddbFinancesIncome.Image"), System.Drawing.Image)
        Me.ddbFinancesIncome.Name = "ddbFinancesIncome"
        Me.ddbFinancesIncome.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesIncome.Tag = "Payments"
        Me.ddbFinancesIncome.Text = "Income"
        '
        'ddbFinancesIncomeCashCollections
        '
        Me.ddbFinancesIncomeCashCollections.Name = "ddbFinancesIncomeCashCollections"
        Me.ddbFinancesIncomeCashCollections.Size = New System.Drawing.Size(203, 22)
        Me.ddbFinancesIncomeCashCollections.Tag = "CashCollections"
        Me.ddbFinancesIncomeCashCollections.Text = "Cash Collections"
        '
        'ddbFinancesOPDIncomeSummaries
        '
        Me.ddbFinancesOPDIncomeSummaries.Name = "ddbFinancesOPDIncomeSummaries"
        Me.ddbFinancesOPDIncomeSummaries.Size = New System.Drawing.Size(203, 22)
        Me.ddbFinancesOPDIncomeSummaries.Tag = "IncomeSummaries"
        Me.ddbFinancesOPDIncomeSummaries.Text = "OPD Income Summaries"
        '
        'ddbFinancesIPDIncomeSummaries
        '
        Me.ddbFinancesIPDIncomeSummaries.Name = "ddbFinancesIPDIncomeSummaries"
        Me.ddbFinancesIPDIncomeSummaries.Size = New System.Drawing.Size(203, 22)
        Me.ddbFinancesIPDIncomeSummaries.Tag = "IncomeSummaries"
        Me.ddbFinancesIPDIncomeSummaries.Text = "IPD Income Summaries"
        '
        'btnFinCollectionBreakDownIncomeCollectionBreakdown
        '
        Me.btnFinCollectionBreakDownIncomeCollectionBreakdown.Name = "btnFinCollectionBreakDownIncomeCollectionBreakdown"
        Me.btnFinCollectionBreakDownIncomeCollectionBreakdown.Size = New System.Drawing.Size(203, 22)
        Me.btnFinCollectionBreakDownIncomeCollectionBreakdown.Tag = "IncomeSummaries"
        Me.btnFinCollectionBreakDownIncomeCollectionBreakdown.Text = "Collection Break Down"
        '
        'ToolStripMenuItem27
        '
        Me.ToolStripMenuItem27.Name = "ToolStripMenuItem27"
        Me.ToolStripMenuItem27.Size = New System.Drawing.Size(203, 6)
        '
        'ddbFinancesInvoices
        '
        Me.ddbFinancesInvoices.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbFinancesInvoicesVisits, Me.ToolStripMenuItem29, Me.ddbFinancesInvoicesToBillCustomers, Me.ToolStripMenuItem30, Me.ddbFinancesInvoicesInsurances, Me.ToolStripMenuItem31, Me.ddbFinancesInvoicesBillingForm, Me.ToolStripMenuItem32, Me.ddbFinancesInvoicesInvoiceCategorisation})
        Me.ddbFinancesInvoices.Image = CType(resources.GetObject("ddbFinancesInvoices.Image"), System.Drawing.Image)
        Me.ddbFinancesInvoices.Name = "ddbFinancesInvoices"
        Me.ddbFinancesInvoices.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesInvoices.Tag = "Invoices"
        Me.ddbFinancesInvoices.Text = "Invoices"
        '
        'ddbFinancesInvoicesVisits
        '
        Me.ddbFinancesInvoicesVisits.Name = "ddbFinancesInvoicesVisits"
        Me.ddbFinancesInvoicesVisits.Size = New System.Drawing.Size(192, 22)
        Me.ddbFinancesInvoicesVisits.Tag = "Invoices"
        Me.ddbFinancesInvoicesVisits.Text = "Visits"
        '
        'ToolStripMenuItem29
        '
        Me.ToolStripMenuItem29.Name = "ToolStripMenuItem29"
        Me.ToolStripMenuItem29.Size = New System.Drawing.Size(189, 6)
        '
        'ddbFinancesInvoicesToBillCustomers
        '
        Me.ddbFinancesInvoicesToBillCustomers.Name = "ddbFinancesInvoicesToBillCustomers"
        Me.ddbFinancesInvoicesToBillCustomers.Size = New System.Drawing.Size(192, 22)
        Me.ddbFinancesInvoicesToBillCustomers.Tag = "Invoices"
        Me.ddbFinancesInvoicesToBillCustomers.Text = "To-Bill Customers"
        '
        'ToolStripMenuItem30
        '
        Me.ToolStripMenuItem30.Name = "ToolStripMenuItem30"
        Me.ToolStripMenuItem30.Size = New System.Drawing.Size(189, 6)
        '
        'ddbFinancesInvoicesInsurances
        '
        Me.ddbFinancesInvoicesInsurances.Name = "ddbFinancesInvoicesInsurances"
        Me.ddbFinancesInvoicesInsurances.Size = New System.Drawing.Size(192, 22)
        Me.ddbFinancesInvoicesInsurances.Tag = "Invoices"
        Me.ddbFinancesInvoicesInsurances.Text = "Insurances"
        '
        'ToolStripMenuItem31
        '
        Me.ToolStripMenuItem31.Name = "ToolStripMenuItem31"
        Me.ToolStripMenuItem31.Size = New System.Drawing.Size(189, 6)
        '
        'ddbFinancesInvoicesBillingForm
        '
        Me.ddbFinancesInvoicesBillingForm.Name = "ddbFinancesInvoicesBillingForm"
        Me.ddbFinancesInvoicesBillingForm.Size = New System.Drawing.Size(192, 22)
        Me.ddbFinancesInvoicesBillingForm.Tag = "Invoices"
        Me.ddbFinancesInvoicesBillingForm.Text = "Billing Form"
        '
        'ToolStripMenuItem32
        '
        Me.ToolStripMenuItem32.Name = "ToolStripMenuItem32"
        Me.ToolStripMenuItem32.Size = New System.Drawing.Size(189, 6)
        '
        'ddbFinancesInvoicesInvoiceCategorisation
        '
        Me.ddbFinancesInvoicesInvoiceCategorisation.Name = "ddbFinancesInvoicesInvoiceCategorisation"
        Me.ddbFinancesInvoicesInvoiceCategorisation.Size = New System.Drawing.Size(192, 22)
        Me.ddbFinancesInvoicesInvoiceCategorisation.Tag = "Invoices"
        Me.ddbFinancesInvoicesInvoiceCategorisation.Text = "Invoice Categorisation"
        '
        'ToolStripMenuItem28
        '
        Me.ToolStripMenuItem28.Name = "ToolStripMenuItem28"
        Me.ToolStripMenuItem28.Size = New System.Drawing.Size(203, 6)
        '
        'ddbFinancesRefunds
        '
        Me.ddbFinancesRefunds.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbFinancesRefundsRequest, Me.ddbFinancesRefundsApproval})
        Me.ddbFinancesRefunds.Image = CType(resources.GetObject("ddbFinancesRefunds.Image"), System.Drawing.Image)
        Me.ddbFinancesRefunds.Name = "ddbFinancesRefunds"
        Me.ddbFinancesRefunds.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesRefunds.Tag = "Refunds"
        Me.ddbFinancesRefunds.Text = "Refunds"
        '
        'ddbFinancesRefundsRequest
        '
        Me.ddbFinancesRefundsRequest.Name = "ddbFinancesRefundsRequest"
        Me.ddbFinancesRefundsRequest.Size = New System.Drawing.Size(122, 22)
        Me.ddbFinancesRefundsRequest.Tag = "RefundRequests"
        Me.ddbFinancesRefundsRequest.Text = "Request"
        '
        'ddbFinancesRefundsApproval
        '
        Me.ddbFinancesRefundsApproval.Name = "ddbFinancesRefundsApproval"
        Me.ddbFinancesRefundsApproval.Size = New System.Drawing.Size(122, 22)
        Me.ddbFinancesRefundsApproval.Tag = "RefundApprovals"
        Me.ddbFinancesRefundsApproval.Text = "Approval"
        '
        'ddbFinancesAccountActivations
        '
        Me.ddbFinancesAccountActivations.Image = CType(resources.GetObject("ddbFinancesAccountActivations.Image"), System.Drawing.Image)
        Me.ddbFinancesAccountActivations.Name = "ddbFinancesAccountActivations"
        Me.ddbFinancesAccountActivations.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesAccountActivations.Tag = "AccountActivations"
        Me.ddbFinancesAccountActivations.Text = "Account Activations"
        '
        'ddbFinancesAccountWithdraws
        '
        Me.ddbFinancesAccountWithdraws.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbFinancesAccountWithdrawsRequest, Me.ddFinancesAccountWithdrawsApprovals})
        Me.ddbFinancesAccountWithdraws.Image = Global.ClinicMaster.My.Resources.Resources.Money
        Me.ddbFinancesAccountWithdraws.Name = "ddbFinancesAccountWithdraws"
        Me.ddbFinancesAccountWithdraws.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesAccountWithdraws.Text = "Account Withdraws"
        '
        'ddbFinancesAccountWithdrawsRequest
        '
        Me.ddbFinancesAccountWithdrawsRequest.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbFinancesAccountWithdrawsRequestNew, Me.ddbFinancesAccountWithdrawsRequestEdit})
        Me.ddbFinancesAccountWithdrawsRequest.Name = "ddbFinancesAccountWithdrawsRequest"
        Me.ddbFinancesAccountWithdrawsRequest.Size = New System.Drawing.Size(127, 22)
        Me.ddbFinancesAccountWithdrawsRequest.Tag = "AccountWithdrawRequests"
        Me.ddbFinancesAccountWithdrawsRequest.Text = "Requests"
        '
        'ddbFinancesAccountWithdrawsRequestNew
        '
        Me.ddbFinancesAccountWithdrawsRequestNew.Image = CType(resources.GetObject("ddbFinancesAccountWithdrawsRequestNew.Image"), System.Drawing.Image)
        Me.ddbFinancesAccountWithdrawsRequestNew.Name = "ddbFinancesAccountWithdrawsRequestNew"
        Me.ddbFinancesAccountWithdrawsRequestNew.Size = New System.Drawing.Size(98, 22)
        Me.ddbFinancesAccountWithdrawsRequestNew.Text = "New"
        '
        'ddbFinancesAccountWithdrawsRequestEdit
        '
        Me.ddbFinancesAccountWithdrawsRequestEdit.Image = CType(resources.GetObject("ddbFinancesAccountWithdrawsRequestEdit.Image"), System.Drawing.Image)
        Me.ddbFinancesAccountWithdrawsRequestEdit.Name = "ddbFinancesAccountWithdrawsRequestEdit"
        Me.ddbFinancesAccountWithdrawsRequestEdit.Size = New System.Drawing.Size(98, 22)
        Me.ddbFinancesAccountWithdrawsRequestEdit.Text = "Edit"
        '
        'ddFinancesAccountWithdrawsApprovals
        '
        Me.ddFinancesAccountWithdrawsApprovals.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddFinancesAccountWithdrawsApprovalsNew, Me.ddFinancesAccountWithdrawsApprovalsEdit})
        Me.ddFinancesAccountWithdrawsApprovals.Name = "ddFinancesAccountWithdrawsApprovals"
        Me.ddFinancesAccountWithdrawsApprovals.Size = New System.Drawing.Size(127, 22)
        Me.ddFinancesAccountWithdrawsApprovals.Tag = "AccountWithdrawApprovals"
        Me.ddFinancesAccountWithdrawsApprovals.Text = "Approvals"
        '
        'ddFinancesAccountWithdrawsApprovalsNew
        '
        Me.ddFinancesAccountWithdrawsApprovalsNew.Image = CType(resources.GetObject("ddFinancesAccountWithdrawsApprovalsNew.Image"), System.Drawing.Image)
        Me.ddFinancesAccountWithdrawsApprovalsNew.Name = "ddFinancesAccountWithdrawsApprovalsNew"
        Me.ddFinancesAccountWithdrawsApprovalsNew.Size = New System.Drawing.Size(98, 22)
        Me.ddFinancesAccountWithdrawsApprovalsNew.Text = "New"
        '
        'ddFinancesAccountWithdrawsApprovalsEdit
        '
        Me.ddFinancesAccountWithdrawsApprovalsEdit.Image = CType(resources.GetObject("ddFinancesAccountWithdrawsApprovalsEdit.Image"), System.Drawing.Image)
        Me.ddFinancesAccountWithdrawsApprovalsEdit.Name = "ddFinancesAccountWithdrawsApprovalsEdit"
        Me.ddFinancesAccountWithdrawsApprovalsEdit.Size = New System.Drawing.Size(98, 22)
        Me.ddFinancesAccountWithdrawsApprovalsEdit.Text = "Edit"
        '
        'ddbFinancesSuspenseAccount
        '
        Me.ddbFinancesSuspenseAccount.Image = CType(resources.GetObject("ddbFinancesSuspenseAccount.Image"), System.Drawing.Image)
        Me.ddbFinancesSuspenseAccount.Name = "ddbFinancesSuspenseAccount"
        Me.ddbFinancesSuspenseAccount.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesSuspenseAccount.Tag = "AccountTransferDetails"
        Me.ddbFinancesSuspenseAccount.Text = "Suspense Account"
        '
        'ToolStripMenuItem15
        '
        Me.ToolStripMenuItem15.Name = "ToolStripMenuItem15"
        Me.ToolStripMenuItem15.Size = New System.Drawing.Size(203, 6)
        '
        'ddbFinancesStaffPaymentApprovals
        '
        Me.ddbFinancesStaffPaymentApprovals.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbFinancesStaffPaymentsOPDStaffPayments, Me.ToolStripMenuItem20, Me.ddbFinancesStaffPaymentsIPDStaffPayments, Me.ToolStripMenuItem21, Me.ddbFinancesStaffPaymentsPaymentApprovalsToolStripMenuItem})
        Me.ddbFinancesStaffPaymentApprovals.Image = CType(resources.GetObject("ddbFinancesStaffPaymentApprovals.Image"), System.Drawing.Image)
        Me.ddbFinancesStaffPaymentApprovals.Name = "ddbFinancesStaffPaymentApprovals"
        Me.ddbFinancesStaffPaymentApprovals.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesStaffPaymentApprovals.Tag = "StaffPayments"
        Me.ddbFinancesStaffPaymentApprovals.Text = "Staff Payments"
        '
        'ddbFinancesStaffPaymentsOPDStaffPayments
        '
        Me.ddbFinancesStaffPaymentsOPDStaffPayments.Name = "ddbFinancesStaffPaymentsOPDStaffPayments"
        Me.ddbFinancesStaffPaymentsOPDStaffPayments.Size = New System.Drawing.Size(180, 22)
        Me.ddbFinancesStaffPaymentsOPDStaffPayments.Tag = "StaffPayments"
        Me.ddbFinancesStaffPaymentsOPDStaffPayments.Text = "OPD Staff Payments"
        '
        'ToolStripMenuItem20
        '
        Me.ToolStripMenuItem20.Name = "ToolStripMenuItem20"
        Me.ToolStripMenuItem20.Size = New System.Drawing.Size(177, 6)
        '
        'ddbFinancesStaffPaymentsIPDStaffPayments
        '
        Me.ddbFinancesStaffPaymentsIPDStaffPayments.Name = "ddbFinancesStaffPaymentsIPDStaffPayments"
        Me.ddbFinancesStaffPaymentsIPDStaffPayments.Size = New System.Drawing.Size(180, 22)
        Me.ddbFinancesStaffPaymentsIPDStaffPayments.Tag = "StaffPayments"
        Me.ddbFinancesStaffPaymentsIPDStaffPayments.Text = "IPD Staff Payments"
        '
        'ToolStripMenuItem21
        '
        Me.ToolStripMenuItem21.Name = "ToolStripMenuItem21"
        Me.ToolStripMenuItem21.Size = New System.Drawing.Size(177, 6)
        '
        'ddbFinancesStaffPaymentsPaymentApprovalsToolStripMenuItem
        '
        Me.ddbFinancesStaffPaymentsPaymentApprovalsToolStripMenuItem.Name = "ddbFinancesStaffPaymentsPaymentApprovalsToolStripMenuItem"
        Me.ddbFinancesStaffPaymentsPaymentApprovalsToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ddbFinancesStaffPaymentsPaymentApprovalsToolStripMenuItem.Tag = "StaffPayments"
        Me.ddbFinancesStaffPaymentsPaymentApprovalsToolStripMenuItem.Text = "Payment Approvals"
        '
        'ToolStripMenuItem33
        '
        Me.ToolStripMenuItem33.Name = "ToolStripMenuItem33"
        Me.ToolStripMenuItem33.Size = New System.Drawing.Size(203, 6)
        '
        'ddbFinancesSmartBilling
        '
        Me.ddbFinancesSmartBilling.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbFinancesSmartBillingOPD, Me.ddbFinancesSmartBillingIPD})
        Me.ddbFinancesSmartBilling.Image = Global.ClinicMaster.My.Resources.Resources.Invoices
        Me.ddbFinancesSmartBilling.Name = "ddbFinancesSmartBilling"
        Me.ddbFinancesSmartBilling.Size = New System.Drawing.Size(206, 22)
        Me.ddbFinancesSmartBilling.Text = "Smart Billing"
        '
        'ddbFinancesSmartBillingOPD
        '
        Me.ddbFinancesSmartBillingOPD.Name = "ddbFinancesSmartBillingOPD"
        Me.ddbFinancesSmartBillingOPD.Size = New System.Drawing.Size(121, 22)
        Me.ddbFinancesSmartBillingOPD.Text = "OPD"
        '
        'ddbFinancesSmartBillingIPD
        '
        Me.ddbFinancesSmartBillingIPD.Name = "ddbFinancesSmartBillingIPD"
        Me.ddbFinancesSmartBillingIPD.Size = New System.Drawing.Size(121, 22)
        Me.ddbFinancesSmartBillingIPD.Text = "Bill Form"
        '
        'MobileMoneyPaymentsToolStripMenuItem
        '
        Me.MobileMoneyPaymentsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ddbMakePayment, Me.ddbCheckPayment})
        Me.MobileMoneyPaymentsToolStripMenuItem.Name = "MobileMoneyPaymentsToolStripMenuItem"
        Me.MobileMoneyPaymentsToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.MobileMoneyPaymentsToolStripMenuItem.Text = "Mobile Money Payments"
        '
        'ddbMakePayment
        '
        Me.ddbMakePayment.Name = "ddbMakePayment"
        Me.ddbMakePayment.Size = New System.Drawing.Size(157, 22)
        Me.ddbMakePayment.Text = "Make Payment"
        '
        'ddbCheckPayment
        '
        Me.ddbCheckPayment.Name = "ddbCheckPayment"
        Me.ddbCheckPayment.Size = New System.Drawing.Size(157, 22)
        Me.ddbCheckPayment.Text = "Check Payment"
        '
        'ddbManageART
        '
        Me.ddbManageART.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiManageARTNew, Me.bmiManageARTEdit})
        Me.ddbManageART.Image = CType(resources.GetObject("ddbManageART.Image"), System.Drawing.Image)
        Me.ddbManageART.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbManageART.Name = "ddbManageART"
        Me.ddbManageART.Size = New System.Drawing.Size(86, 35)
        Me.ddbManageART.Text = "Manage ART"
        Me.ddbManageART.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiManageARTNew
        '
        Me.bmiManageARTNew.AccessibleDescription = ""
        Me.bmiManageARTNew.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiManageARTNewARTRegimen, Me.bmiManageARTNewARTStopped})
        Me.bmiManageARTNew.Image = CType(resources.GetObject("bmiManageARTNew.Image"), System.Drawing.Image)
        Me.bmiManageARTNew.Name = "bmiManageARTNew"
        Me.bmiManageARTNew.Size = New System.Drawing.Size(98, 22)
        Me.bmiManageARTNew.Tag = ""
        Me.bmiManageARTNew.Text = "New"
        '
        'bmiManageARTNewARTRegimen
        '
        Me.bmiManageARTNewARTRegimen.AccessibleDescription = ""
        Me.bmiManageARTNewARTRegimen.AccessibleName = ""
        Me.bmiManageARTNewARTRegimen.Image = CType(resources.GetObject("bmiManageARTNewARTRegimen.Image"), System.Drawing.Image)
        Me.bmiManageARTNewARTRegimen.Name = "bmiManageARTNewARTRegimen"
        Me.bmiManageARTNewARTRegimen.Size = New System.Drawing.Size(144, 22)
        Me.bmiManageARTNewARTRegimen.Tag = "ARTRegimen"
        Me.bmiManageARTNewARTRegimen.Text = "ART Regimen"
        '
        'bmiManageARTNewARTStopped
        '
        Me.bmiManageARTNewARTStopped.AccessibleDescription = ""
        Me.bmiManageARTNewARTStopped.Image = CType(resources.GetObject("bmiManageARTNewARTStopped.Image"), System.Drawing.Image)
        Me.bmiManageARTNewARTStopped.Name = "bmiManageARTNewARTStopped"
        Me.bmiManageARTNewARTStopped.Size = New System.Drawing.Size(144, 22)
        Me.bmiManageARTNewARTStopped.Tag = "ARTStopped"
        Me.bmiManageARTNewARTStopped.Text = "ART Stopped"
        '
        'bmiManageARTEdit
        '
        Me.bmiManageARTEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiManageARTEditARTRegimen, Me.bmiManageARTEditARTStopped})
        Me.bmiManageARTEdit.Image = CType(resources.GetObject("bmiManageARTEdit.Image"), System.Drawing.Image)
        Me.bmiManageARTEdit.Name = "bmiManageARTEdit"
        Me.bmiManageARTEdit.Size = New System.Drawing.Size(98, 22)
        Me.bmiManageARTEdit.Tag = ""
        Me.bmiManageARTEdit.Text = "Edit"
        '
        'bmiManageARTEditARTRegimen
        '
        Me.bmiManageARTEditARTRegimen.AccessibleDescription = ""
        Me.bmiManageARTEditARTRegimen.Image = CType(resources.GetObject("bmiManageARTEditARTRegimen.Image"), System.Drawing.Image)
        Me.bmiManageARTEditARTRegimen.Name = "bmiManageARTEditARTRegimen"
        Me.bmiManageARTEditARTRegimen.Size = New System.Drawing.Size(144, 22)
        Me.bmiManageARTEditARTRegimen.Tag = "ARTRegimen"
        Me.bmiManageARTEditARTRegimen.Text = "ART Regimen"
        '
        'bmiManageARTEditARTStopped
        '
        Me.bmiManageARTEditARTStopped.AccessibleDescription = ""
        Me.bmiManageARTEditARTStopped.Image = CType(resources.GetObject("bmiManageARTEditARTStopped.Image"), System.Drawing.Image)
        Me.bmiManageARTEditARTStopped.Name = "bmiManageARTEditARTStopped"
        Me.bmiManageARTEditARTStopped.Size = New System.Drawing.Size(144, 22)
        Me.bmiManageARTEditARTStopped.Tag = "ARTStopped"
        Me.bmiManageARTEditARTStopped.Text = "ART Stopped"
        '
        'ToolStripSeparator15
        '
        Me.ToolStripSeparator15.Name = "ToolStripSeparator15"
        Me.ToolStripSeparator15.Size = New System.Drawing.Size(6, 43)
        '
        'ddbDeaths
        '
        Me.ddbDeaths.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bmiDeathsNew, Me.bmiDeathsEdit})
        Me.ddbDeaths.Image = CType(resources.GetObject("ddbDeaths.Image"), System.Drawing.Image)
        Me.ddbDeaths.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ddbDeaths.Name = "ddbDeaths"
        Me.ddbDeaths.Size = New System.Drawing.Size(56, 35)
        Me.ddbDeaths.Text = "Deaths"
        Me.ddbDeaths.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'bmiDeathsNew
        '
        Me.bmiDeathsNew.Image = CType(resources.GetObject("bmiDeathsNew.Image"), System.Drawing.Image)
        Me.bmiDeathsNew.Name = "bmiDeathsNew"
        Me.bmiDeathsNew.Size = New System.Drawing.Size(98, 22)
        Me.bmiDeathsNew.Tag = "Deaths"
        Me.bmiDeathsNew.Text = "New"
        '
        'bmiDeathsEdit
        '
        Me.bmiDeathsEdit.Image = CType(resources.GetObject("bmiDeathsEdit.Image"), System.Drawing.Image)
        Me.bmiDeathsEdit.Name = "bmiDeathsEdit"
        Me.bmiDeathsEdit.Size = New System.Drawing.Size(98, 22)
        Me.bmiDeathsEdit.Tag = "Deaths"
        Me.bmiDeathsEdit.Text = "Edit"
        '
        'tmrUserIdleDuration
        '
        Me.tmrUserIdleDuration.Enabled = True
        Me.tmrUserIdleDuration.Interval = 300000
        '
        'tmrSMSNotifications
        '
        Me.tmrSMSNotifications.Enabled = True
        Me.tmrSMSNotifications.Interval = 30000
        '
        'ntIMessages
        '
        Me.ntIMessages.Icon = CType(resources.GetObject("ntIMessages.Icon"), System.Drawing.Icon)
        Me.ntIMessages.Text = "Messages"
        Me.ntIMessages.Visible = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1155, 478)
        Me.Controls.Add(Me.tlbMain)
        Me.Controls.Add(Me.stbMain)
        Me.Controls.Add(Me.mnuMain)
        Me.Controls.Add(Me.mdiClient)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = ""
        Me.Text = "Clinic Master"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mnuMain.ResumeLayout(False)
        Me.mnuMain.PerformLayout()
        Me.stbMain.ResumeLayout(False)
        Me.stbMain.PerformLayout()
        Me.tlbMain.ResumeLayout(False)
        Me.tlbMain.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mdiClient As System.Windows.Forms.MdiClient
    Friend WithEvents mnuMain As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuFileSwitchUser As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuFileExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuView As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuViewToolBar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuViewStatusBar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuViewSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuViewSearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupLookupData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTools As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsCalculator As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuToolsOptions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuWindow As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuWindowCascade As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuWindowTile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuWindowTileHorizontal As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuWindowTileVertical As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuWindowArrangeIcons As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuWindowCloseAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHelpHelpTopics As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHelpSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuHelpAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents stbMain As System.Windows.Forms.StatusStrip
    Friend WithEvents sbpLogin As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents sbpLevel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents sbpReady As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tlbMain As System.Windows.Forms.ToolStrip
    Friend WithEvents mnuEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEditCopy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEditPaste As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEditSelectAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEditSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuEditCut As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEditDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuSetupNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsErrorLog As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsErrorLogClear As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsErrorLogView As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsDatabase As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsDatabaseBackup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsDatabaseRestore As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuSetupSecurity As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupSecurityUsers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupSecurityRoles As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupSecurityChangePassword As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tbbSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tbbSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tbbCashier As System.Windows.Forms.ToolStripButton
    Friend WithEvents tbbSeparotor3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tbbDoctor As System.Windows.Forms.ToolStripButton
    Friend WithEvents tbbSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tbbSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tbbSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuSetupNewStaff As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillCustomers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillCustomers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditStaff As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditPatients As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewPatients As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewVisits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewCashier As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewDoctor As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbPatients As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiPatientsNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiPatientsEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewAppointments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditAppointments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbAppointments As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiAppointmentsNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiAppointmentsEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbInPatients As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiInPatientsNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInPatientsEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tbbSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuFileEditVisits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbVisits As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiVisitsNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiVisitsEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewDrugCombinations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditDrugCombinations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsPayments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsPaymentsCashReceipts As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsLabResults As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewDeaths As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditDeaths As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tbbSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbDeaths As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiDeathsNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiDeathsEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuSetupSecurityLicenses As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents sbpServerName As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents sbpConnectionMode As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents sbpUserID As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuFileImport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileImportPatients As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileImportLabResults As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportDrugs As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportLabTests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportBillCustomers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuToolsManageRestrictedKeys As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileImportVisits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewTriage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditTriage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbTriage As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiTriageNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiTriageEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuSetupNewDrugCategories As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditDrugCategories As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInPatientsNewAdmissions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInPatientsEditAdmissions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInPatientsNewDischarges As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInPatientsEditDischarges As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbRadiology As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiRadiologyNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiRadiologyEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbPharmacy As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiPharmacyDispense As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewRooms As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditRooms As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsLineGraphs As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsLineGraphsLabResults As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tmrUserIdleDuration As System.Windows.Forms.Timer
    Friend WithEvents bmiInPatientsIPDDoctor As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbLaboratory As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiLaboratoryNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiLaboratoryNewLabRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiLaboratoryNewIPDLabRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiLaboratoryNewLabResults As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiLaboratoryEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiLaboratoryEditLabRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiLaboratoryEditIPDLabRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiLaboratoryEditLabResults As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbManageART As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiManageARTNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiManageARTNewARTRegimen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiManageARTNewARTStopped As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiManageARTEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiManageARTEditARTRegimen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiManageARTEditARTStopped As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiIPDPharmacyDispense As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsNewTemplates As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsEditTemplates As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuToolsSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuSetupNewDiseases As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditDiseases As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportProcedures As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportDiseases As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportRadiologyExaminations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportLabTestsEXT As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportLabPossibleResults As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents sbpPoweredBy As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents mnuSetupImportSchemeMembers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiRadiologyNewRadiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiRadiologyNewIPDRadiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiRadiologyEditRadiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiRadiologyEditIPDRadiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsRadiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsIPDRadiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportDentalServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsClaims As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsClaimsInsuranceClaimForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsInvoices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsInvoicesVisits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsInvoicesBillCustomers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsInvoicesInsurances As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbDental As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiDentalNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiDentalNewDentalReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiDentalNewIPDDentalReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiDentalEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiDentalEditDentalReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiDentalEditIPDDentalReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuPaymentsCashCollections As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInPatientsNewExtraBills As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsManageSpecialEdits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewAllergies As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditAllergies As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsIncomeSummaries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportBillCustomFee As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportInsuranceCustomFee As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsClaimsInsuranceClaimSummaries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillCustomerMembers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillCustomerMembers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportBillCustomerMembers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbTheatreOperations As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiTheatreNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiTheatreNewTheatreOperations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiTheatreNewIPDTheatreOperations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiTheatreEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiTheatreEditTheatreOperations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiTheatreEditIPDTheatreOperations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsClaimsBillCustomersClaimForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbInvoices As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiInvoicesNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuFileImportLabResultsEXT As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInvoicesNewInvoices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInvoicesNewIBillFormInvoices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuSetupExchangeRates As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator14 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator16 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuSetupNewGeographicalLocation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewGeographicalLocationCounties As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewGeographicalLocationSubCounties As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewGeographicalLocationParishes As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewGeographicalLocationVillages As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditGeographicalLocation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditGeographicalLocationCounties As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditGeographicalLocationSubCounties As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditGeographicalLocationParishes As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditGeographicalLocationVillages As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewInsurance As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewInsuranceInsurances As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewInsuranceCompanies As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewInsuranceInsurancePolicies As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewInsuranceInsuranceSchemes As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewInsuranceSchemeMembers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewInsuranceHealthUnits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditInsurance As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditInsuranceInsurances As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditInsuranceInsurancePolicies As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditInsuranceCompanies As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditInsuranceInsuranceSchemes As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditInsuranceSchemeMembers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditInsuranceHealthUnits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportConsumableItems As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsNewImportDataInfo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsEditImportDataInfo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInPatientsIPDConsumables As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditExtras As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditExtrasSelfRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditExtrasQuotations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditExtrasClaims As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtras As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasSelfRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasClaims As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasQuotations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasConsumables As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasExtraCharge As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasConsumableInventory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasVisitFiles As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewInPatients As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditInPatients As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewInPatientsAdmissions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewInPatientsExtraBills As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewInPatientsDischarges As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewInPatientsIPDDoctor As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewInPatientsIPDConsumables As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditInPatientsAdmissions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditInPatientsExtraBills As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditInPatientsDischarges As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewPharmacy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewPharmacyDispense As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewPharmacyIPDDispense As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewPharmacyDrugInventory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewLaboratory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditLaboratory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewLaboratoryLabRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewLaboratoryLabResults As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewLaboratoryIPDLabRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditLaboratoryLabRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditLaboratoryLabResults As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditLaboratoryIPDLabRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewManageART As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditManageART As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewManageARTARTRegimen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewManageARTARTStopped As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditManageARTARTRegimen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditManageARTARTStopped As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewInvoices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditInvoices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewInvoicesInvoices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewInvoicesIPDInvoices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditInvoicesInvoices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditInvoicesIPDInvoices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewDental As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditDental As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewDentalDentalReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewDentalIPDDentalReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditDentalDentalReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditDentalIPDDentalReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewTheatre As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditTheatre As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewTheatreTheatreOperations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewTheatreIPDTheatreOperations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditTheatreTheatreOperations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditTheatreIPDTheatreOperations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewRadiology As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditRadiology As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewRadiologyRadiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewRadiologyIPDRadiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditRadiologyRadiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditRadiologyIPDRadiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasOutwardFiles As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasInwardFiles As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditExtrasOutwardFiles As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditExtrasInwardFiles As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewInPatientsReturnedExtraBillItems As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInPatientsBillAdjustments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewRadiologyRadiologyRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiRadiologyNewRadiologyRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewRadiologyIPDRadiologyRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiRadiologyNewIPDRadiologyRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsNewServerCredentials As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsEditServerCredentials As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportInventory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsDiagnosisSummaries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditPharmacy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasInventoryTransfers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasInventoryAcknowledges As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditExtrasInventoryTransfers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasInventoryOrders As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditExtrasInventoryOrders As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasProcessInventoryOrders As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillable As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillable As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillableServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillableDrugs As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillableConsumableItems As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillableLabTests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillableRadiologyExaminations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillableBeds As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillableExtraChargeItems As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillableProcedures As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillableDentalServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillableTheatreServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillableEyeServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillableOpticalServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillableMaternityServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewBillableICUServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableDrugs As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableConsumableItems As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableLabTests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableRadiologyExaminations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableBeds As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableExtraChargeItems As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableProcedures As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableDentalServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableTheatreServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableEyeServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableOpticalServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableMaternityServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableICUServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuSetupNewMemberBenefits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditMemberBenefits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiTriageNewTriage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiTriageNewVisionAssessment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiTriageEditTriage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiTriageEditVisionAssessment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewTriageTriage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewTriageVisionAssessment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditTriageTriage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditTriageVisionAssessment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewPharmacyIssueConsumables As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiPharmacyIssueConsumables As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewPharmacyIssueIPDConsumables As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiPharmacyIssueIPDConsumables As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsInventory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsInventoryDrugStockCard As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsInventoryConsumableStockCard As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsInventoryDrugInventorySummaries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsInventoryConsumableInventorySummaries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewSuppliers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditSuppliers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasPurchaseOrders As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditExtrasPurchaseOrders As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiTriageNewIPDVisionAssessment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiTriageEditIPDVisionAssessment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewTriageIPDVisionAssessment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditTriageIPDVisionAssessment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewPathologyExaminations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditPathologyExaminations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbCardiology As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiCardiologyNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiCardiologyNewCardiologyRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiCardiologyNewCardiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiCardiologyEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiCardiologyEditCardiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator15 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuFileNewExtrasGoodsReceivedNote As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewCardiology As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewCardiologyCardiologyRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewCardiologyCardiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditCardiology As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditCardiologyCardiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasResearchRoutingForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditExtrasResearchRoutingForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditExtrasResearchEnrollmentInformation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsDashboard As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsExtras As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsExtrasGoodsReceivedNote As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsExtrasInventoryAcknowledges As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasCancerDiagnosis As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewCancerDiseases As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewUCITopologySites As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditCancerDiseases As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditTopologySites As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewHCTClientCard As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportBillExcludedItems As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportInsuranceExclusions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupImportInsuranceExcludedItems As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableItemUnitPrice As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsOthers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsOthersSagePastel As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsPaymentsDailyFinancialReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsGeneral As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsGeneralAppointments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsManageBillUnitPrice As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasSmartCardAuthorisations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditExtrasSmartCardAuthorisations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExternalReferralForm As ToolStripMenuItem
    Friend WithEvents mnuFileEditExtrasExternalReferralForm As ToolStripMenuItem
    Friend WithEvents bmiPreviousPrescriptions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewManageARTExaminations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditManageARTExaminations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiDrugAdministration As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InPatientToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OutPatientToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsIPDIncomeSummaries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsPaymentsPatientsAccountStatement As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator17 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MnuToolsBulkSMS As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMessenger As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupDrugBarcodes As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuSetupNewConsumableBarCode As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuReportsGeneralOperations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInPatientsIPDNurse As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHelpEULA As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsExtrasPhysicalStockCount As ToolStripMenuItem
    Friend WithEvents tmrSMSNotifications As System.Windows.Forms.Timer
    Friend WithEvents mnToolsBankingRegiser As ToolStripMenuItem
    Friend WithEvents mnSetUpNewBankAccounts As ToolStripMenuItem
    Friend WithEvents mnSetupEditBankAccounts As ToolStripMenuItem
    Friend WithEvents mnReportsBankingRegister As ToolStripMenuItem
    Friend WithEvents MnuToolsStaffLocations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLocation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsGeneraItemStatus As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnToolsQueue As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuToolsOthersUnsentTextMessages As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SMSRemindersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewOtherItems As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditOtherItems As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mniReportsExtrasSupplierHistory As ToolStripMenuItem
    Friend WithEvents mniReportdDetailedAccountStatement As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewRevenueStreams As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsDiagnosisReportsDiagnosisSummaries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsDiagnosisReportsDiagnosisReattendances As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewPackages As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PackagesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewLabEXTPossibleResults As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditPossibleLabResultsSubTest As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportLabResultsLabResultsReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuReportLabResultsLabReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmniInPatientsIPDCancerDiagnosis As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MnuToolsSuspenseAccount As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiCardiologyNewIPDCardiologyRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuFileNewCardiologyIPDCardiologyRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiCardiologyNewIPDCardiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiCardiologyEditIPDCardiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiOPDPharmacyRefundRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiOPDPharmacyRefundRequestsDrugs As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiOPDPharmacyRefundRequestsConsumables As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiLaboratoryRefundRequest As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiRadiologyRefundRequest As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddTheatreRefundReques As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddDentalRefundReques As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInvoicesInvoiceAdjustments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiCardiologyRefundRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuReportsGeneraPatientRecords As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportInvoicesInvoiceCategorisation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinances As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ddbFinancesAccessCashServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem14 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesCashier As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem15 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesRefunds As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesRefundsRequest As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesRefundsApproval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesClaimPayments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesClaimPaymentsClaimPayments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem16 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesClaimPaymentsClaimPaymentDetailed As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem17 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesStaffPaymentApprovals As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem19 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesStaffPaymentsOPDStaffPayments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem20 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesStaffPaymentsIPDStaffPayments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesStaffPaymentsPaymentApprovalsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem21 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesInvoiceAdjustments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesAccountStatements As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesAccountStatement As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem22 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesDetailedAccountStatement As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem23 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesBanking As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesBankingNewBankAccount As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem24 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesBankingRegister As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem25 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesBankingReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem26 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesIncome As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesIncomeCashCollections As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesOPDIncomeSummaries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesIPDIncomeSummaries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem27 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesClaims As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesClaimsToBillCustomersClaimForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesClaimsInsuranceClaimForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem18 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesInsuranceClaimSummaries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesInvoices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesInvoicesVisits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem29 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesInvoicesToBillCustomers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem30 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesInvoicesInsurances As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem31 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesInvoicesBillingForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem32 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesInvoicesInvoiceCategorisation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem28 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem33 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesSuspenseAccount As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem34 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesInventory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesInventoryDrugInventorySummaries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem35 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesInventoryConsumableInventorySummaries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFinancesAssets As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesNewAsse As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem37 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesAssetMaintenanceLog As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem38 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator19 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbInventory As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiNewInventory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiNewInventoryPurchaseOrders As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiNewInventoryInventoryOrders As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryNewInventoryTransfers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryConsumables As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem63 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiEditInventoryPurchaseOrders As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiEditInventoryInventoryOrders As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryEditInventoryTransfers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryDeliveryNote As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiinventoryInventoryAcknowledges As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiinventoryProcessInventoryOrders As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryGoodsReturned As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryGoodsReceived As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryAcknowledgeReturns As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryAcknowledgeReturnsAcknowledgeBillFormReturns As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryAcknowledgeReturnsAcknowledgeOPDReturns As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator18 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bmiPharmacyConsumableInventory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiPharmacyDrugInventory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bminventory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bminventoryDrugStockCard As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryConsumableStockCard As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryDrugInventorySummaries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryConsumableInventorySummaries As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryPhysicalStockCountReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbExtras As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents bmiExtrasNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewSelfRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnExtraNewEmergencyCase As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewARTPatient As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewClaims As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewOutwardFiles As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewInwardFiles As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewSmartCardAuthorisations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewResearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewExternalReferralForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewAssetsRegister As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewAssetMaintainanceLog As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewSymptomsHistory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IPDStaffPaymentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OPDStaffPaymentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditSelfRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditARTPatient As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditClaims As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditOutwardFiles As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditInwardFiles As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditSmartCardAuthorisations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditResearchRoutingForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditExternalReferralForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditAssetsRegister As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditAssetMaintainanceLog As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditSymptomsHistory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IPDStaffPaymentsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OPDStaffPaymentsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasExtraCharge As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtraAttachPackage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasVisitFiles As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiCancerDiagnosis As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiHCTClientCard As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasAccessCashServices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClaimPaymentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClaimPaymentsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClaimPaymentDetailedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StaffPaymentApprovalsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtrasRefunds As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtrasRefundsRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtrasRefundApprovals As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnExtraNewCodeMapping As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnExtraEditCodeMapping As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnFinCollectionBreakDownIncomeCollectionBreakdown As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuInventoryOtherItemsInventory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditChartOfAccounts As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditChartOfAccountsCategories As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator20 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuSetupEditChartOfAccountsSubCategories As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem36 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuSetupNewBillableCardiologyExaminations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditBillableCardiologyExaminations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsCardiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsIPDCardiologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator21 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bmiPathologyNewPathologyRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiPathologyNewPathologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiPathologyNewIPDPathologyRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiPathologyNewIPDPathologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator22 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bmiPathologyEditPathologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiPathologyEditIPDPathologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiLaboratoryRefundRequestLaboratory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiLaboratoryRefundRequestPathology As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewPathology As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewPathologyPathologyRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewPathologyPathologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator23 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuFileNewPathologyIPDPathologyRequests As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditPathology As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileEditPathologyPathologyReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnExtraNewImmunisation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnExtraEditImmunisation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewServiceInvoices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditServiceInvoices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem39 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ddbFinancesInventoryConsumableInventoryPayments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem40 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PaymentVoucherBalances As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewPhysiotherapy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditPhysiotherapy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewPhysioDiseases As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewClients As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditClients As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiNewInventoryQuotation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiEditInventoryQuotation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewCodeMappingBillCustomers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasNewCodeMappingFinance As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnExtraEditCodeMappingBillCustomers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnExtraEditCodeMappingFinance As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsInvoicesVisitInvoices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsReversals As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsReversalsReceipts As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem41 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnExtraNewOccupationalTherapy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryNewPhysicalStockCount As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryEditPhysicalStockCount As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiInventoryImportInventory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditMaternity As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtraEditMaternityEnrollment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtraEditMaternityAntenatalVisits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bmiRadiologyReImages As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsPaymentCategorisation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiLaboratoryApproveLabResults As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasOPDBillAdjustments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuToolsReveAccounts As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmniInPatientsSmartBilling As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnExtraEditCodeMappingBillableMappings As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupIntegrationAgents As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmniInPatientsPara As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiNewOPDExtraBills As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ntIMessages As System.Windows.Forms.NotifyIcon
    Friend WithEvents bmiExtrasNewResearchResearchRoutingForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bmiExtrasNewResearchResearchEnrollmentInformation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem42 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bmiExtrasNewResearchResearchPatientsEnrollments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNewExtrasResearchResearchRoutingForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem43 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuFileNewExtrasResearchEnrollmentInformation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem44 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuResearchPatientsEnrollment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditResearchResearchRoutingForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem45 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bmiExtrasEditResearchResearchEnrollmentInformation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem46 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bmiExtraResearchEditResearchPatientsEnrollments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem47 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bmiExtrasNewResearchResearchPatientsEnd As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesBillFormAdjustment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem48 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuReportsGeneraIOPDSpecialistTransactions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtrasCodingMappings As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtrasCodingMappingLookupData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtrasCodingMappingBillableMappings As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtrasCodingMappingCompanies As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupNewDiseasesEXT As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetupEditDiseasesEXT As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiTheatreNewIPDPreOperativeForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnExtraNewPatientsEXT As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiTBIntensifiedCaseFindings As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiPatientRiskFactors As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiEditPatientRiskFactors As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtrasMaternityNewAntenatalVisits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bmiExtrasEditTBIntensifiedCaseFindings As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportInvoicesDueInvoices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesSmartBilling As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesSmartBillingOPD As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesSmartBillingIPD As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtrasAntenatal As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtrasAntenatalnrollment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtrasAntenatalVisit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnReportsBillingForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsBillingFormByVisitNo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportsBillingFormByExtraBillNo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem49 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuReportsPaymentsAccountStatement As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesAccountWithdraws As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesAccountWithdrawsRequest As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddFinancesAccountWithdrawsApprovals As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesAccountActivations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesAccountWithdrawsRequestNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbFinancesAccountWithdrawsRequestEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddFinancesAccountWithdrawsApprovalsNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddFinancesAccountWithdrawsApprovalsEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MobileMoneyPaymentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbMakePayment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ddbCheckPayment As System.Windows.Forms.ToolStripMenuItem
End Class
