﻿Option Strict On

Imports SyncSoft.SQLDb
Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Common.Structures
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Enumerations
Imports SyncSoft.Common.SQL.Enumerations
Imports LookupDataID = SyncSoft.SQLDb.Lookup.LookupDataID
Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects
Imports System.Collections.Generic
Imports SyncSoft.Common.SQL.Classes
Imports SyncSoft.SQLDb.Lookup.LookupDataID

Public Class frmAcknowledgeBillReturns

#Region "Fields"
    Private oItemCategoryID As New LookupDataID.ItemCategoryID()
    Private _VisitState As Boolean = True
#End Region

    Private Sub frmAcknowledgeFormReturns_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Cursor = Cursors.WaitCursor

            Me.SetDefaultLocation()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Select Case _VisitState
                Case True
                    Me.Text = "Acknowledge IPD Returns"
                    Me.lblRoundNo.Visible = True
                    Me.lblVisitNo.Visible = True
                    Me.stbRoundNo.Visible = True
                    Me.stbVisitNo.Visible = True
                    Me.lblExtraBillDate.Visible = True
                    Me.stbExtraBillDate.Visible = True


                Case False
                    Me.Text = "Acknowledge OPD Returns"
                    Me.lblRoundNo.Visible = False
                    Me.lblVisitNo.Visible = False
                    Me.stbRoundNo.Visible = False
                    Me.stbVisitNo.Visible = False
                    Me.lblExtraBillDate.Visible = False
                    Me.lblExtraBillNo.Text = "Visit No"
                    Me.stbExtraBillDate.Visible = False


            End Select
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.ShowUnProcessedExtraBillItemAdjustments()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default()
        End Try

    End Sub

    Private Sub SetDefaultLocation()

        Try
            Me.Cursor = Cursors.WaitCursor

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.LoadInternalInventoryLocations()

            If Not String.IsNullOrEmpty(InitOptions.Location) Then
                Me.cboLocationID.SelectedValue = GetLookupDataID(LookupObjects.Location, InitOptions.Location)
                Me.EnableSetInventoryLocation()
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try
        'Try
        '    Me.Cursor = Cursors.WaitCursor

        '    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '    LoadLookupDataCombo(Me.cboLocationID, LookupObjects.Location, True)
        '    If Not String.IsNullOrEmpty(InitOptions.Location) Then
        '        Me.cboLocationID.SelectedValue = GetLookupDataID(LookupObjects.Location, InitOptions.Location)
        '        Me.EnableSetInventoryLocation()
        '    End If
        '    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        'Catch ex As Exception
        '    ErrorMessage(ex)

        'Finally
        '    Me.Cursor = Cursors.Default

        'End Try


    End Sub

    Private Sub EnableSetInventoryLocation()

        Dim oVariousOptions As New VariousOptions()

        Try

            Dim location As String = StringMayBeEnteredIn(Me.cboLocationID)
            If Not oVariousOptions.EnableSetInventoryLocation AndAlso Not String.IsNullOrEmpty(location) Then
                Me.cboLocationID.Enabled = False
            Else : Me.cboLocationID.Enabled = True
            End If

        Catch ex As Exception
            Me.cboLocationID.Enabled = True

        End Try

    End Sub

    Private Sub stbExtraBillNo_TextChanged(sender As Object, e As EventArgs) Handles stbExtraBillNo.TextChanged
        Me.ClearControls()
    End Sub

    Private Sub ClearControls()

        ' Me.stbExtraBillNo.Clear()
        Me.stbExtraBillDate.Clear()
        Me.stbStaffNo.Clear()
        Me.stbVisitNo.Clear()

        Me.stbVisitDate.Clear()
        Me.stbPatientNo.Clear()
        Me.stbFullName.Clear()

        Me.stbBillCustomerName.Clear()
        Me.stbRoundNo.Clear()
        Me.stbJoinDate.Clear()
        Me.stbAge.Clear()
        Me.stbVisitStatus.Clear()
        Me.stbGender.Clear()
        Me.stbBillMode.Clear()
        Me.stbInsuranceName.Clear()

        Me.dgvReturnedItems.Rows.Clear()
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' ResetControlsIn(Me.pnlAcknowledgeBiiFormReturnsDetails)

    End Sub

    Private Sub stbExtraBillNo_Leave(sender As Object, e As EventArgs) Handles stbExtraBillNo.Leave

        Try
            Me.Cursor = Cursors.WaitCursor()

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.ClearControls()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Select Case _VisitState
                Case True
                    Dim extraBillNo As String = RevertText(StringMayBeEnteredIn(Me.stbExtraBillNo))
                    Me.ShowExtraBillDetails(extraBillNo)
                Case False
                    Dim VisitNo As String = RevertText(StringMayBeEnteredIn(Me.stbExtraBillNo))
                    Me.ShowExtraBillDetails(VisitNo)
            End Select

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            Me.ClearControls()
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default()

        End Try
    End Sub

    Private Sub ShowExtraBillDetails(ByVal extraBillNo As String)

        Dim oExtraBills As New SyncSoft.SQLDb.ExtraBills()
        Dim oVisits As New SyncSoft.SQLDb.Visits()

        Try
            Me.Cursor = Cursors.WaitCursor

            Me.ClearControls()

            If String.IsNullOrEmpty(extraBillNo) Then Return

            Select Case _VisitState
                Case True
                    Dim ExtraBillsDetails As DataTable = oExtraBills.GetExtraBills(extraBillNo).Tables("ExtraBills")
                    Dim row As DataRow = ExtraBillsDetails.Rows(0)

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.stbExtraBillNo.Text = FormatText(extraBillNo, "ExtraBills", "ExtraBillNo")
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim patientNo As String = StringEnteredIn(row, "PatientNo")
                    Dim visitNo As String = StringEnteredIn(row, "VisitNo")

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.stbExtraBillDate.Text = FormatDate(DateEnteredIn(row, "ExtraBillDate"))

                    Me.stbStaffNo.Text = StringMayBeEnteredIn(row, "StaffName")


                    Me.stbVisitNo.Text = FormatText(visitNo, "Visits", "VisitNo")

                    Me.stbVisitDate.Text = FormatDate(DateEnteredIn(row, "VisitDate"))
                    Me.stbPatientNo.Text = FormatText(patientNo, "Patients", "PatientNo")
                    Me.stbFullName.Text = StringEnteredIn(row, "FullName")

                    Me.stbBillCustomerName.Text = StringMayBeEnteredIn(row, "BillCustomerName")
                    Me.stbRoundNo.Text = StringMayBeEnteredIn(row, "RoundNo")
                    Me.stbJoinDate.Text = FormatDate(DateEnteredIn(row, "JoinDate"))
                    Me.stbAge.Text = StringEnteredIn(row, "Age")
                    Me.stbVisitStatus.Text = StringEnteredIn(row, "VisitStatus")
                    Me.stbGender.Text = StringEnteredIn(row, "Gender")
                    Me.stbBillMode.Text = StringEnteredIn(row, "BillMode")
                    Me.stbInsuranceName.Text = StringMayBeEnteredIn(row, "InsuranceName")

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.LoadReturnedItems(extraBillNo)

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Case False
                    Dim visits As DataTable = oVisits.GetVisits(extraBillNo).Tables("Visits")
                    Dim row As DataRow = visits.Rows(0)

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    'Me.stbExtraBillNo.Text = FormatText(extraBillNo, "ExtraBills", "ExtraBillNo")
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim patientNo As String = StringEnteredIn(row, "PatientNo")
                    'Dim visitNo As String = StringEnteredIn(row, "VisitNo")

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    'Me.stbExtraBillDate.Text = FormatDate(DateEnteredIn(row, "ExtraBillDate"))
                    'Me.stbStaffNo.Text = StringMayBeEnteredIn(row, "StaffName")
                    Me.stbStaffNo.Text = StringMayBeEnteredIn(row, "DoctorStaffNo")
                    'Me.stbVisitNo.Text = FormatText(visitNo, "Visits", "VisitNo")

                    Me.stbVisitDate.Text = FormatDate(DateEnteredIn(row, "VisitDate"))
                    Me.stbPatientNo.Text = FormatText(patientNo, "Patients", "PatientNo")
                    Me.stbFullName.Text = StringEnteredIn(row, "FullName")

                    Me.stbBillCustomerName.Text = StringMayBeEnteredIn(row, "BillCustomerName")
                    'Me.stbRoundNo.Text = StringMayBeEnteredIn(row, "RoundNo")
                    Me.stbJoinDate.Text = FormatDate(DateEnteredIn(row, "JoinDate"))
                    Me.stbAge.Text = StringEnteredIn(row, "Age")
                    Me.stbVisitStatus.Text = StringEnteredIn(row, "VisitStatus")
                    Me.stbGender.Text = StringEnteredIn(row, "Gender")
                    Me.stbBillMode.Text = StringEnteredIn(row, "BillMode")
                    Me.stbInsuranceName.Text = StringMayBeEnteredIn(row, "InsuranceName")

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Me.LoadReturnedItems(extraBillNo)
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            End Select
        Catch eX As Exception
            ErrorMessage(eX)
            Me.ClearControls()
        Finally
            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub fbnClose_Click(sender As Object, e As EventArgs) Handles fbnClose.Click
        Me.Close()
    End Sub

    Private Sub btnFindExtraBillNo_Click(sender As Object, e As EventArgs) Handles btnFindExtraBillNo.Click
        Select Case _VisitState
            Case True
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim fFindExtraBillNo As New frmFindAutoNo(Me.stbExtraBillNo, AutoNumber.ExtraBillNo)
                fFindExtraBillNo.ShowDialog(Me)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim extraBillNo As String = RevertText(StringEnteredIn(Me.stbExtraBillNo, "ExtraBillNo!"))
                Me.ShowExtraBillDetails(extraBillNo)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Case False
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim fFindVisitNo As New frmFindAutoNo(Me.stbExtraBillNo, AutoNumber.VisitNo)
                fFindVisitNo.ShowDialog(Me)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim visitNo As String = RevertText(StringEnteredIn(Me.stbExtraBillNo, "visitNo!"))
                Me.ShowExtraBillDetails(visitNo)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        End Select
    End Sub

    Private Sub ShowUnProcessedExtraBillItemAdjustments()

        Dim oVariousOptions As New VariousOptions()
        Dim InventoryAlertDays As Integer = oVariousOptions.InventoryAlertDays
        Dim oExtraBillItemAdjustments As New SyncSoft.SQLDb.ExtraBillItemAdjustments()
        Dim oItemAdjustments As New SyncSoft.SQLDb.ItemAdjustments()

        Dim recordsNo As Integer
        Try
            Dim startDate As Date = Today.AddDays(-InventoryAlertDays)
            Dim endDate As Date = Today
            Me.Cursor = Cursors.WaitCursor

            Dim pendingBillAdjustments As New DataTable


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Select Case _VisitState
                Case True
                  
                    pendingBillAdjustments = oExtraBillItemAdjustments.GetPeriodicToAckwnoledgeExtraBillItems(startDate, endDate).Tables("ExtraBillItemAdjustments")
                    
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    
                Case False

                    pendingBillAdjustments = oItemAdjustments.GetPeriodicToAcknowledgeItemAdjustments(startDate, endDate).Tables("ItemAdjustments")
                   
            End Select

            recordsNo = pendingBillAdjustments.Rows.Count

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.lblPendingIventoryAcknowledgements.Text = Me.Text + " " + recordsNo.ToString()

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub frmAcknowledgeBillFormReturns_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Try
            Me.Cursor = Cursors.WaitCursor

            Me.ShowUnProcessedExtraBillItemAdjustments()
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub tbcExtraBillItemAdjustments_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Me.Cursor = Cursors.WaitCursor
            ' Me.lblPendingIventoryAcknowledgements.Text = "Returns Acknowledgements: " + Me.ShowUnProcessedExtraBillItemAdjustments.ToString()
            Me.ShowUnProcessedExtraBillItemAdjustments()
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub



#Region "Drug Grid"

    Private Sub dgvReturnedDrugs_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvReturnedItems.CellClick

        Try

            Me.Cursor = Cursors.WaitCursor

            If e.RowIndex < 0 Then Return

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim expiryDate As Date = DateMayBeEnteredIn(Me.dgvReturnedItems.Rows(e.RowIndex).Cells, Me.ColDrugExpiryDate)

            Dim fSelectDateTime As New SyncSoft.SQL.Win.Forms.SelectDateTime(expiryDate, "Expiry Date", Today, AppData.MaximumDate,
                                                                             Me.dgvReturnedItems, Me.ColDrugExpiryDate, e.RowIndex)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If Me.ColDrugExpiryDate.Index.Equals(e.ColumnIndex) AndAlso Me.dgvReturnedItems.Rows(e.RowIndex).IsNewRow Then

                Me.dgvReturnedItems.Rows.Add()
                fSelectDateTime.ShowDialog(Me)

                Dim enteredDate As Date = DateMayBeEnteredIn(Me.dgvReturnedItems.Rows(e.RowIndex).Cells, Me.ColDrugExpiryDate)
                If enteredDate = AppData.NullDateValue Then Me.dgvReturnedItems.Rows.RemoveAt(e.RowIndex)

            ElseIf Me.ColDrugExpiryDate.Index.Equals(e.ColumnIndex) Then

                fSelectDateTime.ShowDialog(Me)

            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default

        End Try


    End Sub

    Private Sub LoadReturnedItems(ByVal extraBillNo As String)

        Dim oExtraBillItemAdjustments As New SyncSoft.SQLDb.ExtraBillItemAdjustments()
        Dim oItemAdjustments As New SyncSoft.SQLDb.ItemAdjustments
        Dim dataTable As New DataTable()
        Dim objectName As String = String.Empty

        Try
            Select Case _VisitState
                Case True
                    dataTable = oExtraBillItemAdjustments.GetExtraBillToAckwnoledgeExtraBillItems(extraBillNo).Tables("ExtraBillItemAdjustments")
                    ObjectName = "ExtraBills"

                Case False
                    dataTable = oItemAdjustments.GetVisitToAcknowkedgeItemAdjustments(extraBillNo).Tables("ItemAdjustments")
                    objectName = "Visits"


            End Select

            LoadGridData(Me.dgvReturnedItems, dataTable)

            If Me.dgvReturnedItems.RowCount <= 1 Then Return

            Dim oInventoryEXT As New InventoryEXT()

            Dim uniqueItem As String = String.Empty
            Dim uniqueAdjustmentNo As String = String.Empty

            For Each row As DataGridViewRow In Me.dgvReturnedItems.Rows
                If row.IsNewRow Then Exit For

                Dim itemCode As String = StringEnteredIn(Me.dgvReturnedItems.Rows(row.Index).Cells, Me.colItemCode)
                Dim adjustmentNo As String = StringEnteredIn(Me.dgvReturnedItems.Rows(row.Index).Cells, Me.colAdjustmentNo)

                If (uniqueItem = itemCode) And (uniqueAdjustmentNo = adjustmentNo) Then
                    Continue For
                Else
                    uniqueItem = itemCode
                    uniqueAdjustmentNo = adjustmentNo

                    Dim batchInfo = oInventoryEXT.GetIssuedBatches(itemCode, extraBillNo, objectName).Tables("InventoryEXT")
                    If batchInfo.Rows.Count > 0 Then
                        Me.setItemBatchInfo(row.Index, batchInfo)
                    End If
                End If

            Next

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub setItemBatchInfo(ByVal selectedRow As Integer, ByVal batchInfo As DataTable)

        For rowNo As Integer = 0 To batchInfo.Rows.Count - 1
            Me.dgvReturnedItems.Item(Me.colDrugBatchNo.Name, selectedRow).Value = batchInfo.Rows(rowNo).Item("BatchNo")
            Me.dgvReturnedItems.Item(Me.colMaxBatchQty.Name, selectedRow).Value = batchInfo.Rows(rowNo).Item("MaxReturnQty")
            Me.dgvReturnedItems.Item(Me.ColDrugExpiryDate.Name, selectedRow).Value = batchInfo.Rows(rowNo).Item("ExpiryDate")

            If batchInfo.Rows.Count > 1 And rowNo < batchInfo.Rows.Count - 1 Then
                Me.dgvReturnedItems.Rows.Insert((selectedRow + 1), Me.CloneWithValues(dgvReturnedItems.Rows(selectedRow)))
            End If
            selectedRow = selectedRow + 1
        Next
    End Sub

    Public Function CloneWithValues(ByVal row As DataGridViewRow) As DataGridViewRow
        Dim clonedRow As DataGridViewRow = CType(row.Clone(), DataGridViewRow)

        For index As Int32 = 0 To row.Cells.Count - 1
            clonedRow.Cells(index).Value = row.Cells(index).Value
        Next

        Return clonedRow
    End Function

#End Region

    Private Sub fbnSave_Click(sender As Object, e As EventArgs) Handles fbnSave.Click

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim LocationID As String = StringValueEnteredIn(Me.cboLocationID, "Location!")
            If Not (String.IsNullOrEmpty(LocationID)) Then
                If Me.dgvReturnedItems.RowCount > 1 Then Me.SaveReturnedPrescriptions(LocationID)

            End If

        Catch ex As Exception
            ErrorMessage(ex)

        Finally

            Me.Cursor = Cursors.Default

        End Try

    End Sub

    Private Sub SaveReturnedPrescriptions(ByVal LocationID As String)
        
        Dim oStockTypeID As New StockTypeID()
        Dim oEntryModeID As New EntryModeID
        Dim transactions As New List(Of TransactionList(Of DBConnect))
        Dim lToAcknowledgeTeturns As New List(Of DBConnect)
        Dim lInventory As New List(Of DBConnect)

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim oItemTransferStatusID As New LookupDataID.ItemStatusID

            Dim ItemTransferStatus As String = oItemTransferStatusID.Pending
            Dim extraBillDate As Date = DateMayBeEnteredIn(Me.stbExtraBillDate)


            If Me.dgvReturnedItems.RowCount <= 1 Then Throw New ArgumentException("Must Register At least one entry for returned drug!")

            For Each row As DataGridViewRow In Me.dgvReturnedItems.Rows

                If row.IsNewRow Then Exit For

                Dim expiryDate As Date = DateEnteredIn(row.Cells, Me.ColDrugExpiryDate, "Expiry Date!")
                Dim returnedQuantity As Integer = IntegerEnteredIn(row.Cells, Me.colDrugReturnedQuantity, "Returned Quantity!")
                Dim batchNo As String = StringEnteredIn(row.Cells, Me.colDrugBatchNo, "BatchNo")
                Dim batchQuantity As Integer = IntegerEnteredIn(row.Cells, Me.colBatchQuantity, "Batch Return Quantity !")

                If expiryDate < extraBillDate Then Throw New ArgumentException("Expiry date can’t be before extra bill date!")
                If returnedQuantity < 1 Then Throw New ArgumentException("Returned quantity can’t be less than one!")

            Next

            Dim lItem As New List(Of String)
            Dim uniqueItem As String = String.Empty
            Dim uniqueAdjustmentNo As String = String.Empty
            Dim sumBatchQuantity As Integer = 0
            Dim sumQuantity As Integer = 0
            Dim previousRow As Integer = 0
            Dim previousAdjustmentNo As String = String.Empty
            Dim previousItemCode As String = String.Empty

            For Each row As DataGridViewRow In Me.dgvReturnedItems.Rows
                If row.IsNewRow Then Exit For

                Dim itemCode As String = StringEnteredIn(Me.dgvReturnedItems.Rows(row.Index).Cells, Me.colItemCode)
                Dim adjustmentNo As String = StringEnteredIn(Me.dgvReturnedItems.Rows(row.Index).Cells, Me.colAdjustmentNo)

                If (uniqueItem = itemCode) And (uniqueAdjustmentNo = adjustmentNo) Then

                Else
                    uniqueItem = itemCode
                    lItem.Add(itemCode)
                    uniqueAdjustmentNo = adjustmentNo

                    If (sumBatchQuantity > sumQuantity Or sumBatchQuantity < sumQuantity) Then
                        previousRow = ((row.Index) - 1)
                        previousAdjustmentNo = StringEnteredIn(Me.dgvReturnedItems.Rows(previousRow).Cells, Me.colAdjustmentNo)
                        previousItemCode = StringEnteredIn(Me.dgvReturnedItems.Rows(previousRow).Cells, Me.colItemCode)

                        Throw New ArgumentException("The Item Code " + previousItemCode + " with Adjustment No: " + previousAdjustmentNo + " has a total return Quantity: " + sumBatchQuantity.ToString + " that is not equal to the Return Quantity: " + sumQuantity.ToString)
                    Else
                        sumBatchQuantity = 0
                        sumQuantity = 0
                    End If
                End If

                If (lItem.Contains(StringMayBeEnteredIn(Me.dgvReturnedItems.Rows(row.Index).Cells, Me.colItemCode))) Then
                    sumBatchQuantity = sumBatchQuantity + IntegerMayBeEnteredIn(Me.dgvReturnedItems.Rows(row.Index).Cells, Me.colBatchQuantity)
                    sumQuantity = IntegerMayBeEnteredIn(Me.dgvReturnedItems.Rows(row.Index).Cells, Me.colDrugReturnedQuantity)
                End If

            Next

            previousRow = (dgvReturnedItems.RowCount - 2)
            previousAdjustmentNo = StringEnteredIn(Me.dgvReturnedItems.Rows(previousRow).Cells, Me.colAdjustmentNo)
            previousItemCode = StringEnteredIn(Me.dgvReturnedItems.Rows(previousRow).Cells, Me.colItemCode)

            If (sumBatchQuantity > sumQuantity Or sumBatchQuantity < sumQuantity) Then
                Throw New ArgumentException("The Item Code " + previousItemCode + " with Adjustment No: " + previousAdjustmentNo + " has a total batch return Quantity: " + sumBatchQuantity.ToString + " that is not equal to Return Quantity: " + sumQuantity.ToString)
            End If

            Select Case _VisitState
                Case True

                    Dim extraBillNo As String = RevertText(StringEnteredIn(Me.stbExtraBillNo, "Extra Bill No"))
                    Dim roundNo As String = RevertText(StringEnteredIn(Me.stbExtraBillNo, "Round No!"))
                    Dim oAccessObjectName As New AccessObjectNames()

                    Dim lUniqueItems As New List(Of String)
                    For Each row As DataGridViewRow In Me.dgvReturnedItems.Rows
                        If row.IsNewRow Then Exit For

                        Dim cells As DataGridViewCellCollection = Me.dgvReturnedItems.Rows(row.Index).Cells
                        Dim adjustmentNo As String = RevertText(StringEnteredIn(cells, Me.colAdjustmentNo))
                        Dim itemCode As String = StringEnteredIn(cells, Me.colItemCode)
                        Dim itemCategoryID As String = StringEnteredIn(cells, Me.colItemCategoryID)
                        Dim expiryDate As Date = DateEnteredIn(cells, Me.ColDrugExpiryDate)
                        Dim batchNo As String = StringEnteredIn(cells, Me.colDrugBatchNo)
                        Dim Quantity As Integer = IntegerEnteredIn(cells, Me.colDrugReturnedQuantity)
                        Dim batchQuantity As Integer = IntegerEnteredIn(row.Cells, Me.colBatchQuantity)

                        If Not lUniqueItems.Contains(itemCode) Then

                            lUniqueItems.Add(itemCode)
                            Using oExtraBillItemAdjustments As New ExtraBillItemAdjustments
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                With oExtraBillItemAdjustments
                                    .AdjustmentNo = adjustmentNo
                                    .ExtraBillNo = extraBillNo
                                    .ItemCode = itemCode
                                    .ItemCategoryID = itemCategoryID

                                End With
                                lToAcknowledgeTeturns.Add(oExtraBillItemAdjustments)


                            End Using
                        End If

                        Using oInventory As New Inventory
                            With oInventory

                                .LocationID = LocationID
                                .ItemCode = itemCode
                                .ItemCategoryID = itemCategoryID
                                .TranDate = Today
                                .StockTypeID = oStockTypeID.Received
                                .Quantity = batchQuantity
                                .EntryModeID = oEntryModeID.Returned
                                .LoginID = CurrentUser.LoginID
                                .BatchNo = batchNo
                                .ExpiryDate = expiryDate
                                .Details = GetLookupDataDes(itemCategoryID) + "(s) Returned from Extra Bill No: " + extraBillNo
                                .ReferenceNo = extraBillNo
                                .ReferenceObjectName = oAccessObjectName.ExtraBills
                                .SourceNo = adjustmentNo
                                .ObjectName = oAccessObjectName.BillAdjustments
                            End With
                            lInventory.Add(oInventory)
                        End Using

                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Next

                    transactions.Add(New TransactionList(Of DBConnect)(lToAcknowledgeTeturns, Action.Update))
                    transactions.Add(New TransactionList(Of DBConnect)(lInventory, Action.Save))
                    DoTransactions(transactions)

                Case False
                    Dim visitNo As String = RevertText(StringEnteredIn(Me.stbExtraBillNo, "Visit No!"))
                    Dim oAccessObjectName As New AccessObjectNames()

                    Dim lUniqueItems As New List(Of String)
                    For Each row As DataGridViewRow In Me.dgvReturnedItems.Rows
                        If row.IsNewRow Then Exit For

                        Dim lReturnedItems As New List(Of DBConnect)

                        Dim cells As DataGridViewCellCollection = Me.dgvReturnedItems.Rows(row.Index).Cells
                        Dim adjustmentNo As String = RevertText(StringEnteredIn(cells, Me.colAdjustmentNo))
                        Dim itemCode As String = StringEnteredIn(cells, Me.colItemCode)
                        Dim itemCategoryID As String = StringEnteredIn(cells, Me.colItemCategoryID)
                        Dim expiryDate As Date = DateEnteredIn(cells, Me.ColDrugExpiryDate)
                        Dim batchNo As String = StringEnteredIn(cells, Me.colDrugBatchNo)
                        Dim Quantity As Integer = IntegerEnteredIn(cells, Me.colDrugReturnedQuantity)
                        Dim batchQuantity As Integer = IntegerEnteredIn(row.Cells, Me.colBatchQuantity)


                        Using oInventory As New Inventory
                            With oInventory

                                .LocationID = LocationID
                                .ItemCode = itemCode
                                .ItemCategoryID = itemCategoryID
                                .TranDate = Today
                                .StockTypeID = oStockTypeID.Received
                                .Quantity = batchQuantity
                                .EntryModeID = oEntryModeID.Returned
                                .LoginID = CurrentUser.LoginID
                                .BatchNo = batchNo
                                .ExpiryDate = expiryDate
                                .Details = GetLookupDataDes(itemCategoryID) + "(s) Returned from Visit No: " + visitNo
                                .ReferenceNo = visitNo
                                .ReferenceObjectName = oAccessObjectName.Visits
                                .SourceNo = adjustmentNo
                                .ObjectName = oAccessObjectName.BillAdjustments
                            End With
                            lInventory.Add(oInventory)
                        End Using

                        If Not lUniqueItems.Contains(itemCode) Then

                            lUniqueItems.Add(itemCode)
                            Using oItemAdjustments As New ItemAdjustments
                                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                With oItemAdjustments
                                    .AdjustmentNo = adjustmentNo
                                    .VisitNo = visitNo
                                    .ItemCode = itemCode
                                    .ItemCategoryID = itemCategoryID

                                End With
                                lToAcknowledgeTeturns.Add(oItemAdjustments)
                            End Using
                        End If


                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Next

                    transactions.Add(New TransactionList(Of DBConnect)(lToAcknowledgeTeturns, Action.Update))
                    transactions.Add(New TransactionList(Of DBConnect)(lInventory, Action.Save))
                    DoTransactions(transactions)
            End Select

            ResetControlsIn(Me)

        Catch ex As Exception
            ErrorMessage(ex)

        Finally
            Me.Cursor = Cursors.Default
            Me.ShowUnProcessedExtraBillItemAdjustments()
        End Try

    End Sub


    Private Sub cboLocationID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboLocationID.SelectedIndexChanged
        Try
            Cursor.Current = Cursors.WaitCursor
            If String.IsNullOrEmpty(cboLocationID.Text) Then Return
        Catch ex As Exception
            ErrorMessage(ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try


    End Sub


    Private Sub BtnLoad_Click(sender As System.Object, e As System.EventArgs) Handles BtnLoad.Click

        Select Case _VisitState

            Case True
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim fPeriodicExtraBillItemAdjustments As New frmPeriodicReturnedExtraBillItems(True, Me.stbExtraBillNo)
                fPeriodicExtraBillItemAdjustments.ShowDialog(Me)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim extraBillNo As String = RevertText(StringMayBeEnteredIn(Me.stbExtraBillNo))
                Me.ShowExtraBillDetails(extraBillNo)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Case False
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim fPeriodicExtraBillItemAdjustments As New frmPeriodicReturnedExtraBillItems(False, Me.stbExtraBillNo)
                fPeriodicExtraBillItemAdjustments.ShowDialog(Me)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim VisitNo As String = RevertText(StringMayBeEnteredIn(Me.stbExtraBillNo))
                Me.ShowExtraBillDetails(VisitNo)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        End Select
    End Sub

    Private Sub dgvReturnedItems_CellEndEdit(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvReturnedItems.CellEndEdit
        Try
            If e.ColumnIndex.Equals(Me.colBatchQuantity.Index) Then
                Dim selectedRow As Integer = Me.dgvReturnedItems.CurrentCell.RowIndex
                Dim itemCode As String = StringEnteredIn(Me.dgvReturnedItems.Rows(selectedRow).Cells, Me.colItemCode)
                Dim batchNo As String = StringEnteredIn(Me.dgvReturnedItems.Rows(selectedRow).Cells, Me.colDrugBatchNo)

                Dim batchQuantity As Integer = Me.getTotalEnteredBatchQuantity(itemCode, batchNo)

                Dim maxBatchQuantity As Integer = IntegerMayBeEnteredIn(Me.dgvReturnedItems.Rows(selectedRow).Cells, Me.colMaxBatchQty)

                If batchQuantity > maxBatchQuantity Then
                    DisplayMessage("The ItemCode: " + itemCode + " has total Batch Return Quantity " + batchQuantity.ToString + " that is greater than the maximum batch units : " + maxBatchQuantity.ToString + " issued for Batch No: " + batchNo)
                    Me.dgvReturnedItems.Item(Me.colBatchQuantity.Name, selectedRow).Value = String.Empty
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Function getTotalEnteredBatchQuantity(ByVal itemCode As String, ByVal batchNo As String) As Integer
        Dim totalEnteredBatchQuantity As Integer = 0
        Try

            For Each row As DataGridViewRow In Me.dgvReturnedItems.Rows
                If row.IsNewRow Then Exit For

                If (itemCode = StringEnteredIn(Me.dgvReturnedItems.Rows(row.Index).Cells, Me.colItemCode)) And
                    (batchNo = StringEnteredIn(Me.dgvReturnedItems.Rows(row.Index).Cells, Me.colDrugBatchNo)) Then
                    totalEnteredBatchQuantity = totalEnteredBatchQuantity + IntegerMayBeEnteredIn(Me.dgvReturnedItems.Rows(row.Index).Cells, Me.colBatchQuantity)
                End If
            Next

        Catch ex As Exception
            ErrorMessage(ex)
        End Try

        Return totalEnteredBatchQuantity
    End Function

    Private Sub LoadInternalInventoryLocations()

        cboLocationID.DataSource = Nothing

        Dim oInventoryLocation As New InventoryLocation()
        Dim allInventoryLocation As DataTable = oInventoryLocation.GetAllInternalInventoryLocations().Tables("AllLocations")

        If allInventoryLocation.Rows.Count() < 1 Then Return
        Me.cboLocationID.DataSource = allInventoryLocation
        cboLocationID.DisplayMember = "DataDes"
        cboLocationID.ValueMember = "DataID"
        Me.cboLocationID.SelectedIndex = -1

    End Sub

End Class