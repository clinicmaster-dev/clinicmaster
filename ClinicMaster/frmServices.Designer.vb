<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class frmServices : Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmServices))
        Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton()
        Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton()
        Me.fcbServiceBillAtID = New SyncSoft.Common.Win.Controls.FlatComboBox()
        Me.nbxStandardFee = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.stbServiceName = New SyncSoft.Common.Win.Controls.SmartTextBox()
        Me.fcbServicePointID = New SyncSoft.Common.Win.Controls.FlatComboBox()
        Me.chkHidden = New System.Windows.Forms.CheckBox()
        Me.nbxUnitCost = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.nbxVATPercentage = New SyncSoft.Common.Win.Controls.NumericBox()
        Me.cboRevenueStream = New System.Windows.Forms.ComboBox()
        Me.lblServiceBillAtID = New System.Windows.Forms.Label()
        Me.lblStandardFee = New System.Windows.Forms.Label()
        Me.lblServiceName = New System.Windows.Forms.Label()
        Me.lblServiceCode = New System.Windows.Forms.Label()
        Me.lblServicePointID = New System.Windows.Forms.Label()
        Me.tbcServicesOtherFees = New System.Windows.Forms.TabControl()
        Me.tpgBillCustomFee = New System.Windows.Forms.TabPage()
        Me.dgvBillCustomFee = New System.Windows.Forms.DataGridView()
        Me.colBillCustomerName = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colAccountNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBillCustomFee = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBillCurrenciesID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.ColRequiresPayment = New SyncSoft.Common.Win.Controls.GridComboBoxColumn()
        Me.colBillCustomFeeSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.tpgInsuranceCustomFee = New System.Windows.Forms.TabPage()
        Me.dgvInsuranceCustomFee = New System.Windows.Forms.DataGridView()
        Me.tpgServicesDrSpecialtyFee = New System.Windows.Forms.TabPage()
        Me.dgvServicesDrSpecialtyFee = New System.Windows.Forms.DataGridView()
        Me.colDoctorSpecialtyID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colSpecialtyFee = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDrSpecialtyFeeCurrenciesID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colServicesDrSpecialtyFeeSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.tpgServicesStaffFee = New System.Windows.Forms.TabPage()
        Me.dgvServicesStaffFee = New System.Windows.Forms.DataGridView()
        Me.colStaffNo = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colStaffFee = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colStaffFeeCurrenciesID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colServicesStaffFeeSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.tpgServicesSpecialtyBillCustomFee = New System.Windows.Forms.TabPage()
        Me.dgvServicesSpecialtyBillCustomFee = New System.Windows.Forms.DataGridView()
        Me.colDoctorSpecialtyBillID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colSpecialtyBillCustomerName = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colSpecialtyBillAccountNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSpecialtyBillCustomFee = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSpecialtyBillCurrenciesID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colSpecialtyBillCustomFeeSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.tpgServicesStaffBillCustomFee = New System.Windows.Forms.TabPage()
        Me.dgvServicesStaffBillCustomFee = New System.Windows.Forms.DataGridView()
        Me.colStaffBillID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colStaffBillCustomerName = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colStaffBillAccountNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colStaffBillCustomFee = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colStaffBillCurrenciesID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colStaffBillCustomFeeSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.tpgServicesSpecialtyCustomCode = New System.Windows.Forms.TabPage()
        Me.dgvServicesSpecialtyCustomCode = New System.Windows.Forms.DataGridView()
        Me.colDoctorSpecialtyCustomCodeID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colSpecialtyCustomCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colServicesSpecialtyCustomCodeSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.cboServiceCode = New System.Windows.Forms.ComboBox()
        Me.lblUnitCost = New System.Windows.Forms.Label()
        Me.lblVATPercentage = New System.Windows.Forms.Label()
        Me.lblRevenueStream = New System.Windows.Forms.Label()
        Me.colInsuranceName = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colInsuranceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInsuranceCustomFee = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colInsuranceCurrenciesID = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.ColInsuranceRequiresPayment = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colInsuranceCustomFeeSaved = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.tbcServicesOtherFees.SuspendLayout()
        Me.tpgBillCustomFee.SuspendLayout()
        CType(Me.dgvBillCustomFee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgInsuranceCustomFee.SuspendLayout()
        CType(Me.dgvInsuranceCustomFee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgServicesDrSpecialtyFee.SuspendLayout()
        CType(Me.dgvServicesDrSpecialtyFee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgServicesStaffFee.SuspendLayout()
        CType(Me.dgvServicesStaffFee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgServicesSpecialtyBillCustomFee.SuspendLayout()
        CType(Me.dgvServicesSpecialtyBillCustomFee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgServicesStaffBillCustomFee.SuspendLayout()
        CType(Me.dgvServicesStaffBillCustomFee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgServicesSpecialtyCustomCode.SuspendLayout()
        CType(Me.dgvServicesSpecialtyCustomCode, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'fbnClose
        '
        Me.fbnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnClose.Location = New System.Drawing.Point(665, 372)
        Me.fbnClose.Name = "fbnClose"
        Me.fbnClose.Size = New System.Drawing.Size(72, 24)
        Me.fbnClose.TabIndex = 21
        Me.fbnClose.Text = "&Close"
        Me.fbnClose.UseVisualStyleBackColor = False
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSearch.Location = New System.Drawing.Point(15, 342)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(77, 23)
        Me.btnSearch.TabIndex = 18
        Me.btnSearch.Text = "S&earch"
        Me.btnSearch.UseVisualStyleBackColor = True
        Me.btnSearch.Visible = False
        '
        'fbnDelete
        '
        Me.fbnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fbnDelete.Location = New System.Drawing.Point(665, 342)
        Me.fbnDelete.Name = "fbnDelete"
        Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
        Me.fbnDelete.TabIndex = 20
        Me.fbnDelete.Tag = "Services"
        Me.fbnDelete.Text = "&Delete"
        Me.fbnDelete.UseVisualStyleBackColor = False
        Me.fbnDelete.Visible = False
        '
        'ebnSaveUpdate
        '
        Me.ebnSaveUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ebnSaveUpdate.DataSource = Nothing
        Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
        Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ebnSaveUpdate.Location = New System.Drawing.Point(15, 371)
        Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
        Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
        Me.ebnSaveUpdate.TabIndex = 19
        Me.ebnSaveUpdate.Tag = "Services"
        Me.ebnSaveUpdate.Text = "&Save"
        Me.ebnSaveUpdate.UseVisualStyleBackColor = False
        '
        'fcbServiceBillAtID
        '
        Me.fcbServiceBillAtID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.fcbServiceBillAtID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ebnSaveUpdate.SetDataMember(Me.fcbServiceBillAtID, "ServiceBillAt,ServiceBillAtID")
        Me.fcbServiceBillAtID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.fcbServiceBillAtID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fcbServiceBillAtID.FormattingEnabled = True
        Me.fcbServiceBillAtID.Location = New System.Drawing.Point(133, 69)
        Me.fcbServiceBillAtID.Name = "fcbServiceBillAtID"
        Me.fcbServiceBillAtID.ReadOnly = True
        Me.fcbServiceBillAtID.Size = New System.Drawing.Size(172, 21)
        Me.fcbServiceBillAtID.TabIndex = 7
        '
        'nbxStandardFee
        '
        Me.nbxStandardFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxStandardFee.ControlCaption = "Standard Fee"
        Me.nbxStandardFee.DataFormat = SyncSoft.Common.Win.Controls.DisplayFormat.Standard
        Me.ebnSaveUpdate.SetDataMember(Me.nbxStandardFee, "StandardFee")
        Me.nbxStandardFee.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxStandardFee.DecimalPlaces = 2
        Me.nbxStandardFee.Location = New System.Drawing.Point(437, 67)
        Me.nbxStandardFee.MaxLength = 12
        Me.nbxStandardFee.MaxValue = 0.0R
        Me.nbxStandardFee.MinValue = 0.0R
        Me.nbxStandardFee.MustEnterNumeric = True
        Me.nbxStandardFee.Name = "nbxStandardFee"
        Me.nbxStandardFee.Size = New System.Drawing.Size(172, 20)
        Me.nbxStandardFee.TabIndex = 15
        Me.nbxStandardFee.Tag = "ServicesPrices"
        Me.nbxStandardFee.Value = ""
        '
        'stbServiceName
        '
        Me.stbServiceName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.stbServiceName.CapitalizeFirstLetter = True
        Me.ebnSaveUpdate.SetDataMember(Me.stbServiceName, "ServiceName")
        Me.stbServiceName.EntryErrorMSG = ""
        Me.stbServiceName.Location = New System.Drawing.Point(133, 26)
        Me.stbServiceName.MaxLength = 100
        Me.stbServiceName.Name = "stbServiceName"
        Me.stbServiceName.RegularExpression = ""
        Me.stbServiceName.Size = New System.Drawing.Size(172, 20)
        Me.stbServiceName.TabIndex = 3
        '
        'fcbServicePointID
        '
        Me.fcbServicePointID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.fcbServicePointID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ebnSaveUpdate.SetDataMember(Me.fcbServicePointID, "ServicePoint,ServicePointID")
        Me.fcbServicePointID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.fcbServicePointID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fcbServicePointID.FormattingEnabled = True
        Me.fcbServicePointID.Location = New System.Drawing.Point(133, 46)
        Me.fcbServicePointID.Name = "fcbServicePointID"
        Me.fcbServicePointID.ReadOnly = True
        Me.fcbServicePointID.Size = New System.Drawing.Size(172, 21)
        Me.fcbServicePointID.TabIndex = 5
        '
        'chkHidden
        '
        Me.chkHidden.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ebnSaveUpdate.SetDataMember(Me.chkHidden, "Hidden")
        Me.chkHidden.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.chkHidden.Location = New System.Drawing.Point(471, 91)
        Me.chkHidden.Name = "chkHidden"
        Me.chkHidden.Size = New System.Drawing.Size(138, 20)
        Me.chkHidden.TabIndex = 16
        Me.chkHidden.Text = "Hidden"
        '
        'nbxUnitCost
        '
        Me.nbxUnitCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxUnitCost.ControlCaption = "Unit Cost"
        Me.nbxUnitCost.DataFormat = SyncSoft.Common.Win.Controls.DisplayFormat.Standard
        Me.ebnSaveUpdate.SetDataMember(Me.nbxUnitCost, "UnitCost")
        Me.nbxUnitCost.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxUnitCost.DecimalPlaces = 2
        Me.nbxUnitCost.Location = New System.Drawing.Point(437, 28)
        Me.nbxUnitCost.MaxLength = 12
        Me.nbxUnitCost.MaxValue = 0.0R
        Me.nbxUnitCost.MinValue = 0.0R
        Me.nbxUnitCost.MustEnterNumeric = True
        Me.nbxUnitCost.Name = "nbxUnitCost"
        Me.nbxUnitCost.Size = New System.Drawing.Size(172, 20)
        Me.nbxUnitCost.TabIndex = 11
        Me.nbxUnitCost.Tag = "ServicesPrices"
        Me.nbxUnitCost.Value = ""
        '
        'nbxVATPercentage
        '
        Me.nbxVATPercentage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbxVATPercentage.ControlCaption = "VATPercentage"
        Me.nbxVATPercentage.DataFormat = SyncSoft.Common.Win.Controls.DisplayFormat.Standard
        Me.ebnSaveUpdate.SetDataMember(Me.nbxVATPercentage, "VATPercentage")
        Me.nbxVATPercentage.DataType = SyncSoft.Common.Win.Controls.Number.[Decimal]
        Me.nbxVATPercentage.DecimalPlaces = 2
        Me.nbxVATPercentage.Location = New System.Drawing.Point(437, 47)
        Me.nbxVATPercentage.MaxLength = 12
        Me.nbxVATPercentage.MaxValue = 0.0R
        Me.nbxVATPercentage.MinValue = 0.0R
        Me.nbxVATPercentage.MustEnterNumeric = True
        Me.nbxVATPercentage.Name = "nbxVATPercentage"
        Me.nbxVATPercentage.Size = New System.Drawing.Size(172, 20)
        Me.nbxVATPercentage.TabIndex = 13
        Me.nbxVATPercentage.Tag = "ServicesPrices"
        Me.nbxVATPercentage.Value = ""
        '
        'cboRevenueStream
        '
        Me.ebnSaveUpdate.SetDataMember(Me.cboRevenueStream, "Name,RevenueStreamCode")
        Me.cboRevenueStream.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRevenueStream.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboRevenueStream.Location = New System.Drawing.Point(437, 5)
        Me.cboRevenueStream.Name = "cboRevenueStream"
        Me.cboRevenueStream.Size = New System.Drawing.Size(172, 21)
        Me.cboRevenueStream.TabIndex = 9
        '
        'lblServiceBillAtID
        '
        Me.lblServiceBillAtID.Location = New System.Drawing.Point(9, 68)
        Me.lblServiceBillAtID.Name = "lblServiceBillAtID"
        Me.lblServiceBillAtID.Size = New System.Drawing.Size(106, 20)
        Me.lblServiceBillAtID.TabIndex = 6
        Me.lblServiceBillAtID.Text = "Service Bill At"
        '
        'lblStandardFee
        '
        Me.lblStandardFee.Location = New System.Drawing.Point(313, 65)
        Me.lblStandardFee.Name = "lblStandardFee"
        Me.lblStandardFee.Size = New System.Drawing.Size(106, 20)
        Me.lblStandardFee.TabIndex = 14
        Me.lblStandardFee.Text = "Standard Fee"
        '
        'lblServiceName
        '
        Me.lblServiceName.Location = New System.Drawing.Point(9, 27)
        Me.lblServiceName.Name = "lblServiceName"
        Me.lblServiceName.Size = New System.Drawing.Size(106, 20)
        Me.lblServiceName.TabIndex = 2
        Me.lblServiceName.Text = "Service Name"
        '
        'lblServiceCode
        '
        Me.lblServiceCode.Location = New System.Drawing.Point(9, 7)
        Me.lblServiceCode.Name = "lblServiceCode"
        Me.lblServiceCode.Size = New System.Drawing.Size(106, 20)
        Me.lblServiceCode.TabIndex = 0
        Me.lblServiceCode.Text = "Service Code"
        '
        'lblServicePointID
        '
        Me.lblServicePointID.Location = New System.Drawing.Point(9, 46)
        Me.lblServicePointID.Name = "lblServicePointID"
        Me.lblServicePointID.Size = New System.Drawing.Size(106, 20)
        Me.lblServicePointID.TabIndex = 4
        Me.lblServicePointID.Text = "Service Point"
        '
        'tbcServicesOtherFees
        '
        Me.tbcServicesOtherFees.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbcServicesOtherFees.Controls.Add(Me.tpgBillCustomFee)
        Me.tbcServicesOtherFees.Controls.Add(Me.tpgInsuranceCustomFee)
        Me.tbcServicesOtherFees.Controls.Add(Me.tpgServicesDrSpecialtyFee)
        Me.tbcServicesOtherFees.Controls.Add(Me.tpgServicesStaffFee)
        Me.tbcServicesOtherFees.Controls.Add(Me.tpgServicesSpecialtyBillCustomFee)
        Me.tbcServicesOtherFees.Controls.Add(Me.tpgServicesStaffBillCustomFee)
        Me.tbcServicesOtherFees.Controls.Add(Me.tpgServicesSpecialtyCustomCode)
        Me.tbcServicesOtherFees.HotTrack = True
        Me.tbcServicesOtherFees.Location = New System.Drawing.Point(14, 117)
        Me.tbcServicesOtherFees.Name = "tbcServicesOtherFees"
        Me.tbcServicesOtherFees.SelectedIndex = 0
        Me.tbcServicesOtherFees.Size = New System.Drawing.Size(735, 219)
        Me.tbcServicesOtherFees.TabIndex = 17
        '
        'tpgBillCustomFee
        '
        Me.tpgBillCustomFee.Controls.Add(Me.dgvBillCustomFee)
        Me.tpgBillCustomFee.Location = New System.Drawing.Point(4, 22)
        Me.tpgBillCustomFee.Name = "tpgBillCustomFee"
        Me.tpgBillCustomFee.Size = New System.Drawing.Size(727, 193)
        Me.tpgBillCustomFee.TabIndex = 4
        Me.tpgBillCustomFee.Tag = "BillCustomFee"
        Me.tpgBillCustomFee.Text = "Bill Custom Fee"
        Me.tpgBillCustomFee.UseVisualStyleBackColor = True
        '
        'dgvBillCustomFee
        '
        Me.dgvBillCustomFee.AllowUserToOrderColumns = True
        Me.dgvBillCustomFee.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBillCustomFee.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvBillCustomFee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colBillCustomerName, Me.colAccountNo, Me.colBillCustomFee, Me.colBillCurrenciesID, Me.ColRequiresPayment, Me.colBillCustomFeeSaved})
        Me.dgvBillCustomFee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvBillCustomFee.EnableHeadersVisualStyles = False
        Me.dgvBillCustomFee.GridColor = System.Drawing.Color.Khaki
        Me.dgvBillCustomFee.Location = New System.Drawing.Point(0, 0)
        Me.dgvBillCustomFee.Name = "dgvBillCustomFee"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBillCustomFee.RowHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvBillCustomFee.Size = New System.Drawing.Size(727, 193)
        Me.dgvBillCustomFee.TabIndex = 0
        Me.dgvBillCustomFee.Tag = "ServicesPrices"
        Me.dgvBillCustomFee.Text = "DataGridView1"
        '
        'colBillCustomerName
        '
        Me.colBillCustomerName.DataPropertyName = "AccountNo"
        Me.colBillCustomerName.DisplayStyleForCurrentCellOnly = True
        Me.colBillCustomerName.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colBillCustomerName.HeaderText = "To-Bill Account Name"
        Me.colBillCustomerName.Name = "colBillCustomerName"
        Me.colBillCustomerName.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colBillCustomerName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colBillCustomerName.Width = 200
        '
        'colAccountNo
        '
        Me.colAccountNo.DataPropertyName = "AccountNo"
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Info
        Me.colAccountNo.DefaultCellStyle = DataGridViewCellStyle2
        Me.colAccountNo.HeaderText = "Account No"
        Me.colAccountNo.Name = "colAccountNo"
        Me.colAccountNo.ReadOnly = True
        '
        'colBillCustomFee
        '
        Me.colBillCustomFee.DataPropertyName = "CustomFee"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.colBillCustomFee.DefaultCellStyle = DataGridViewCellStyle3
        Me.colBillCustomFee.HeaderText = "Custom Fee"
        Me.colBillCustomFee.MaxInputLength = 12
        Me.colBillCustomFee.Name = "colBillCustomFee"
        '
        'colBillCurrenciesID
        '
        Me.colBillCurrenciesID.DataPropertyName = "CurrenciesID"
        Me.colBillCurrenciesID.DisplayStyleForCurrentCellOnly = True
        Me.colBillCurrenciesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colBillCurrenciesID.HeaderText = "Currency"
        Me.colBillCurrenciesID.Name = "colBillCurrenciesID"
        Me.colBillCurrenciesID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'ColRequiresPayment
        '
        Me.ColRequiresPayment.ControlCaption = Nothing
        Me.ColRequiresPayment.DataPropertyName = "RequiresPayment"
        Me.ColRequiresPayment.DisplayStyleForCurrentCellOnly = True
        Me.ColRequiresPayment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ColRequiresPayment.HeaderText = "Requires Payment"
        Me.ColRequiresPayment.Name = "ColRequiresPayment"
        Me.ColRequiresPayment.SourceColumn = Nothing
        '
        'colBillCustomFeeSaved
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle4.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle4.NullValue = False
        Me.colBillCustomFeeSaved.DefaultCellStyle = DataGridViewCellStyle4
        Me.colBillCustomFeeSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colBillCustomFeeSaved.HeaderText = "Saved"
        Me.colBillCustomFeeSaved.Name = "colBillCustomFeeSaved"
        Me.colBillCustomFeeSaved.ReadOnly = True
        Me.colBillCustomFeeSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colBillCustomFeeSaved.Width = 50
        '
        'tpgInsuranceCustomFee
        '
        Me.tpgInsuranceCustomFee.Controls.Add(Me.dgvInsuranceCustomFee)
        Me.tpgInsuranceCustomFee.Location = New System.Drawing.Point(4, 22)
        Me.tpgInsuranceCustomFee.Name = "tpgInsuranceCustomFee"
        Me.tpgInsuranceCustomFee.Size = New System.Drawing.Size(727, 193)
        Me.tpgInsuranceCustomFee.TabIndex = 5
        Me.tpgInsuranceCustomFee.Tag = "InsuranceCustomFee"
        Me.tpgInsuranceCustomFee.Text = "Insurance Custom Fee"
        Me.tpgInsuranceCustomFee.UseVisualStyleBackColor = True
        '
        'dgvInsuranceCustomFee
        '
        Me.dgvInsuranceCustomFee.AllowUserToOrderColumns = True
        Me.dgvInsuranceCustomFee.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInsuranceCustomFee.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvInsuranceCustomFee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colInsuranceName, Me.colInsuranceNo, Me.colInsuranceCustomFee, Me.colInsuranceCurrenciesID, Me.ColInsuranceRequiresPayment, Me.colInsuranceCustomFeeSaved})
        Me.dgvInsuranceCustomFee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvInsuranceCustomFee.EnableHeadersVisualStyles = False
        Me.dgvInsuranceCustomFee.GridColor = System.Drawing.Color.Khaki
        Me.dgvInsuranceCustomFee.Location = New System.Drawing.Point(0, 0)
        Me.dgvInsuranceCustomFee.Name = "dgvInsuranceCustomFee"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvInsuranceCustomFee.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvInsuranceCustomFee.Size = New System.Drawing.Size(727, 193)
        Me.dgvInsuranceCustomFee.TabIndex = 22
        Me.dgvInsuranceCustomFee.Tag = "ServicesPrices"
        Me.dgvInsuranceCustomFee.Text = "DataGridView1"
        '
        'tpgServicesDrSpecialtyFee
        '
        Me.tpgServicesDrSpecialtyFee.Controls.Add(Me.dgvServicesDrSpecialtyFee)
        Me.tpgServicesDrSpecialtyFee.Location = New System.Drawing.Point(4, 22)
        Me.tpgServicesDrSpecialtyFee.Name = "tpgServicesDrSpecialtyFee"
        Me.tpgServicesDrSpecialtyFee.Size = New System.Drawing.Size(727, 193)
        Me.tpgServicesDrSpecialtyFee.TabIndex = 1
        Me.tpgServicesDrSpecialtyFee.Tag = "ServicesDrSpecialtyFee"
        Me.tpgServicesDrSpecialtyFee.Text = "Doctor Specialty"
        Me.tpgServicesDrSpecialtyFee.UseVisualStyleBackColor = True
        '
        'dgvServicesDrSpecialtyFee
        '
        Me.dgvServicesDrSpecialtyFee.AllowUserToOrderColumns = True
        Me.dgvServicesDrSpecialtyFee.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvServicesDrSpecialtyFee.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgvServicesDrSpecialtyFee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colDoctorSpecialtyID, Me.colSpecialtyFee, Me.colDrSpecialtyFeeCurrenciesID, Me.colServicesDrSpecialtyFeeSaved})
        Me.dgvServicesDrSpecialtyFee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvServicesDrSpecialtyFee.EnableHeadersVisualStyles = False
        Me.dgvServicesDrSpecialtyFee.GridColor = System.Drawing.Color.Khaki
        Me.dgvServicesDrSpecialtyFee.Location = New System.Drawing.Point(0, 0)
        Me.dgvServicesDrSpecialtyFee.Name = "dgvServicesDrSpecialtyFee"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvServicesDrSpecialtyFee.RowHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.dgvServicesDrSpecialtyFee.Size = New System.Drawing.Size(727, 193)
        Me.dgvServicesDrSpecialtyFee.TabIndex = 21
        Me.dgvServicesDrSpecialtyFee.Tag = "ServicesPrices"
        Me.dgvServicesDrSpecialtyFee.Text = "DataGridView1"
        '
        'colDoctorSpecialtyID
        '
        Me.colDoctorSpecialtyID.DataPropertyName = "DoctorSpecialtyID"
        Me.colDoctorSpecialtyID.DisplayStyleForCurrentCellOnly = True
        Me.colDoctorSpecialtyID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colDoctorSpecialtyID.HeaderText = "Doctor Specialty"
        Me.colDoctorSpecialtyID.Name = "colDoctorSpecialtyID"
        Me.colDoctorSpecialtyID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colDoctorSpecialtyID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colDoctorSpecialtyID.Width = 200
        '
        'colSpecialtyFee
        '
        Me.colSpecialtyFee.DataPropertyName = "SpecialtyFee"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.Format = "N2"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.colSpecialtyFee.DefaultCellStyle = DataGridViewCellStyle12
        Me.colSpecialtyFee.HeaderText = "Specialty Fee"
        Me.colSpecialtyFee.MaxInputLength = 12
        Me.colSpecialtyFee.Name = "colSpecialtyFee"
        '
        'colDrSpecialtyFeeCurrenciesID
        '
        Me.colDrSpecialtyFeeCurrenciesID.DataPropertyName = "CurrenciesID"
        Me.colDrSpecialtyFeeCurrenciesID.DisplayStyleForCurrentCellOnly = True
        Me.colDrSpecialtyFeeCurrenciesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colDrSpecialtyFeeCurrenciesID.HeaderText = "Currency"
        Me.colDrSpecialtyFeeCurrenciesID.Name = "colDrSpecialtyFeeCurrenciesID"
        Me.colDrSpecialtyFeeCurrenciesID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'colServicesDrSpecialtyFeeSaved
        '
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle13.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle13.NullValue = False
        Me.colServicesDrSpecialtyFeeSaved.DefaultCellStyle = DataGridViewCellStyle13
        Me.colServicesDrSpecialtyFeeSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colServicesDrSpecialtyFeeSaved.HeaderText = "Saved"
        Me.colServicesDrSpecialtyFeeSaved.Name = "colServicesDrSpecialtyFeeSaved"
        Me.colServicesDrSpecialtyFeeSaved.ReadOnly = True
        Me.colServicesDrSpecialtyFeeSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colServicesDrSpecialtyFeeSaved.Width = 50
        '
        'tpgServicesStaffFee
        '
        Me.tpgServicesStaffFee.Controls.Add(Me.dgvServicesStaffFee)
        Me.tpgServicesStaffFee.Location = New System.Drawing.Point(4, 22)
        Me.tpgServicesStaffFee.Name = "tpgServicesStaffFee"
        Me.tpgServicesStaffFee.Size = New System.Drawing.Size(727, 193)
        Me.tpgServicesStaffFee.TabIndex = 3
        Me.tpgServicesStaffFee.Tag = "ServicesStaffFee"
        Me.tpgServicesStaffFee.Text = "Staff"
        Me.tpgServicesStaffFee.UseVisualStyleBackColor = True
        '
        'dgvServicesStaffFee
        '
        Me.dgvServicesStaffFee.AllowUserToOrderColumns = True
        Me.dgvServicesStaffFee.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvServicesStaffFee.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.dgvServicesStaffFee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colStaffNo, Me.colStaffFee, Me.colStaffFeeCurrenciesID, Me.colServicesStaffFeeSaved})
        Me.dgvServicesStaffFee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvServicesStaffFee.EnableHeadersVisualStyles = False
        Me.dgvServicesStaffFee.GridColor = System.Drawing.Color.Khaki
        Me.dgvServicesStaffFee.Location = New System.Drawing.Point(0, 0)
        Me.dgvServicesStaffFee.Name = "dgvServicesStaffFee"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvServicesStaffFee.RowHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.dgvServicesStaffFee.Size = New System.Drawing.Size(727, 193)
        Me.dgvServicesStaffFee.TabIndex = 21
        Me.dgvServicesStaffFee.Tag = "ServicesPrices"
        Me.dgvServicesStaffFee.Text = "DataGridView1"
        '
        'colStaffNo
        '
        Me.colStaffNo.DataPropertyName = "StaffFullName"
        Me.colStaffNo.DisplayStyleForCurrentCellOnly = True
        Me.colStaffNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colStaffNo.HeaderText = "Staff No"
        Me.colStaffNo.Name = "colStaffNo"
        Me.colStaffNo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colStaffNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colStaffNo.Width = 200
        '
        'colStaffFee
        '
        Me.colStaffFee.DataPropertyName = "StaffFee"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle16.Format = "N2"
        DataGridViewCellStyle16.NullValue = Nothing
        Me.colStaffFee.DefaultCellStyle = DataGridViewCellStyle16
        Me.colStaffFee.HeaderText = "Staff Fee"
        Me.colStaffFee.MaxInputLength = 12
        Me.colStaffFee.Name = "colStaffFee"
        '
        'colStaffFeeCurrenciesID
        '
        Me.colStaffFeeCurrenciesID.DataPropertyName = "CurrenciesID"
        Me.colStaffFeeCurrenciesID.DisplayStyleForCurrentCellOnly = True
        Me.colStaffFeeCurrenciesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colStaffFeeCurrenciesID.HeaderText = "Currency"
        Me.colStaffFeeCurrenciesID.Name = "colStaffFeeCurrenciesID"
        Me.colStaffFeeCurrenciesID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'colServicesStaffFeeSaved
        '
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle17.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle17.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle17.NullValue = False
        Me.colServicesStaffFeeSaved.DefaultCellStyle = DataGridViewCellStyle17
        Me.colServicesStaffFeeSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colServicesStaffFeeSaved.HeaderText = "Saved"
        Me.colServicesStaffFeeSaved.Name = "colServicesStaffFeeSaved"
        Me.colServicesStaffFeeSaved.ReadOnly = True
        Me.colServicesStaffFeeSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colServicesStaffFeeSaved.Width = 50
        '
        'tpgServicesSpecialtyBillCustomFee
        '
        Me.tpgServicesSpecialtyBillCustomFee.Controls.Add(Me.dgvServicesSpecialtyBillCustomFee)
        Me.tpgServicesSpecialtyBillCustomFee.Location = New System.Drawing.Point(4, 22)
        Me.tpgServicesSpecialtyBillCustomFee.Margin = New System.Windows.Forms.Padding(2)
        Me.tpgServicesSpecialtyBillCustomFee.Name = "tpgServicesSpecialtyBillCustomFee"
        Me.tpgServicesSpecialtyBillCustomFee.Size = New System.Drawing.Size(727, 193)
        Me.tpgServicesSpecialtyBillCustomFee.TabIndex = 6
        Me.tpgServicesSpecialtyBillCustomFee.Tag = "ServicesSpecialtyBillCustomFee"
        Me.tpgServicesSpecialtyBillCustomFee.Text = "Specialty Bill Custom Fee"
        Me.tpgServicesSpecialtyBillCustomFee.UseVisualStyleBackColor = True
        '
        'dgvServicesSpecialtyBillCustomFee
        '
        Me.dgvServicesSpecialtyBillCustomFee.AllowUserToOrderColumns = True
        Me.dgvServicesSpecialtyBillCustomFee.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle19.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvServicesSpecialtyBillCustomFee.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle19
        Me.dgvServicesSpecialtyBillCustomFee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colDoctorSpecialtyBillID, Me.colSpecialtyBillCustomerName, Me.colSpecialtyBillAccountNo, Me.colSpecialtyBillCustomFee, Me.colSpecialtyBillCurrenciesID, Me.colSpecialtyBillCustomFeeSaved})
        Me.dgvServicesSpecialtyBillCustomFee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvServicesSpecialtyBillCustomFee.EnableHeadersVisualStyles = False
        Me.dgvServicesSpecialtyBillCustomFee.GridColor = System.Drawing.Color.Khaki
        Me.dgvServicesSpecialtyBillCustomFee.Location = New System.Drawing.Point(0, 0)
        Me.dgvServicesSpecialtyBillCustomFee.Name = "dgvServicesSpecialtyBillCustomFee"
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle23.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvServicesSpecialtyBillCustomFee.RowHeadersDefaultCellStyle = DataGridViewCellStyle23
        Me.dgvServicesSpecialtyBillCustomFee.Size = New System.Drawing.Size(727, 193)
        Me.dgvServicesSpecialtyBillCustomFee.TabIndex = 0
        Me.dgvServicesSpecialtyBillCustomFee.Tag = "ServicesPrices"
        Me.dgvServicesSpecialtyBillCustomFee.Text = "DataGridView1"
        '
        'colDoctorSpecialtyBillID
        '
        Me.colDoctorSpecialtyBillID.DataPropertyName = "DoctorSpecialtyID"
        Me.colDoctorSpecialtyBillID.DisplayStyleForCurrentCellOnly = True
        Me.colDoctorSpecialtyBillID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colDoctorSpecialtyBillID.HeaderText = "Doctor Specialty"
        Me.colDoctorSpecialtyBillID.Name = "colDoctorSpecialtyBillID"
        Me.colDoctorSpecialtyBillID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colDoctorSpecialtyBillID.Width = 160
        '
        'colSpecialtyBillCustomerName
        '
        Me.colSpecialtyBillCustomerName.DataPropertyName = "AccountNo"
        Me.colSpecialtyBillCustomerName.DisplayStyleForCurrentCellOnly = True
        Me.colSpecialtyBillCustomerName.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colSpecialtyBillCustomerName.HeaderText = "To-Bill Account Name"
        Me.colSpecialtyBillCustomerName.Name = "colSpecialtyBillCustomerName"
        Me.colSpecialtyBillCustomerName.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colSpecialtyBillCustomerName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colSpecialtyBillCustomerName.Width = 160
        '
        'colSpecialtyBillAccountNo
        '
        Me.colSpecialtyBillAccountNo.DataPropertyName = "AccountNo"
        DataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Info
        Me.colSpecialtyBillAccountNo.DefaultCellStyle = DataGridViewCellStyle20
        Me.colSpecialtyBillAccountNo.HeaderText = "Account No"
        Me.colSpecialtyBillAccountNo.Name = "colSpecialtyBillAccountNo"
        Me.colSpecialtyBillAccountNo.ReadOnly = True
        '
        'colSpecialtyBillCustomFee
        '
        Me.colSpecialtyBillCustomFee.DataPropertyName = "CustomFee"
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle21.Format = "N2"
        DataGridViewCellStyle21.NullValue = Nothing
        Me.colSpecialtyBillCustomFee.DefaultCellStyle = DataGridViewCellStyle21
        Me.colSpecialtyBillCustomFee.HeaderText = "Custom Fee"
        Me.colSpecialtyBillCustomFee.MaxInputLength = 12
        Me.colSpecialtyBillCustomFee.Name = "colSpecialtyBillCustomFee"
        '
        'colSpecialtyBillCurrenciesID
        '
        Me.colSpecialtyBillCurrenciesID.DataPropertyName = "CurrenciesID"
        Me.colSpecialtyBillCurrenciesID.DisplayStyleForCurrentCellOnly = True
        Me.colSpecialtyBillCurrenciesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colSpecialtyBillCurrenciesID.HeaderText = "Currency"
        Me.colSpecialtyBillCurrenciesID.Name = "colSpecialtyBillCurrenciesID"
        Me.colSpecialtyBillCurrenciesID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'colSpecialtyBillCustomFeeSaved
        '
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle22.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle22.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle22.NullValue = False
        Me.colSpecialtyBillCustomFeeSaved.DefaultCellStyle = DataGridViewCellStyle22
        Me.colSpecialtyBillCustomFeeSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colSpecialtyBillCustomFeeSaved.HeaderText = "Saved"
        Me.colSpecialtyBillCustomFeeSaved.Name = "colSpecialtyBillCustomFeeSaved"
        Me.colSpecialtyBillCustomFeeSaved.ReadOnly = True
        Me.colSpecialtyBillCustomFeeSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colSpecialtyBillCustomFeeSaved.Width = 50
        '
        'tpgServicesStaffBillCustomFee
        '
        Me.tpgServicesStaffBillCustomFee.Controls.Add(Me.dgvServicesStaffBillCustomFee)
        Me.tpgServicesStaffBillCustomFee.Location = New System.Drawing.Point(4, 22)
        Me.tpgServicesStaffBillCustomFee.Margin = New System.Windows.Forms.Padding(2)
        Me.tpgServicesStaffBillCustomFee.Name = "tpgServicesStaffBillCustomFee"
        Me.tpgServicesStaffBillCustomFee.Size = New System.Drawing.Size(727, 193)
        Me.tpgServicesStaffBillCustomFee.TabIndex = 7
        Me.tpgServicesStaffBillCustomFee.Tag = "ServicesStaffBillCustomFee"
        Me.tpgServicesStaffBillCustomFee.Text = "Staff Bill Custom Fee"
        Me.tpgServicesStaffBillCustomFee.UseVisualStyleBackColor = True
        '
        'dgvServicesStaffBillCustomFee
        '
        Me.dgvServicesStaffBillCustomFee.AccessibleDescription = ""
        Me.dgvServicesStaffBillCustomFee.AllowUserToOrderColumns = True
        Me.dgvServicesStaffBillCustomFee.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle24.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvServicesStaffBillCustomFee.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle24
        Me.dgvServicesStaffBillCustomFee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colStaffBillID, Me.colStaffBillCustomerName, Me.colStaffBillAccountNo, Me.colStaffBillCustomFee, Me.colStaffBillCurrenciesID, Me.colStaffBillCustomFeeSaved})
        Me.dgvServicesStaffBillCustomFee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvServicesStaffBillCustomFee.EnableHeadersVisualStyles = False
        Me.dgvServicesStaffBillCustomFee.GridColor = System.Drawing.Color.Khaki
        Me.dgvServicesStaffBillCustomFee.Location = New System.Drawing.Point(0, 0)
        Me.dgvServicesStaffBillCustomFee.Name = "dgvServicesStaffBillCustomFee"
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle28.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvServicesStaffBillCustomFee.RowHeadersDefaultCellStyle = DataGridViewCellStyle28
        Me.dgvServicesStaffBillCustomFee.Size = New System.Drawing.Size(727, 193)
        Me.dgvServicesStaffBillCustomFee.TabIndex = 2
        Me.dgvServicesStaffBillCustomFee.Tag = "ServicesPrices"
        Me.dgvServicesStaffBillCustomFee.Text = "DataGridView1"
        '
        'colStaffBillID
        '
        Me.colStaffBillID.DataPropertyName = "StaffFullName"
        Me.colStaffBillID.DisplayStyleForCurrentCellOnly = True
        Me.colStaffBillID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colStaffBillID.HeaderText = "Staff No"
        Me.colStaffBillID.Name = "colStaffBillID"
        Me.colStaffBillID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colStaffBillID.Width = 160
        '
        'colStaffBillCustomerName
        '
        Me.colStaffBillCustomerName.DataPropertyName = "AccountNo"
        Me.colStaffBillCustomerName.DisplayStyleForCurrentCellOnly = True
        Me.colStaffBillCustomerName.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colStaffBillCustomerName.HeaderText = "To-Bill Account Name"
        Me.colStaffBillCustomerName.Name = "colStaffBillCustomerName"
        Me.colStaffBillCustomerName.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colStaffBillCustomerName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colStaffBillCustomerName.Width = 160
        '
        'colStaffBillAccountNo
        '
        Me.colStaffBillAccountNo.DataPropertyName = "AccountNo"
        DataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Info
        Me.colStaffBillAccountNo.DefaultCellStyle = DataGridViewCellStyle25
        Me.colStaffBillAccountNo.HeaderText = "Account No"
        Me.colStaffBillAccountNo.Name = "colStaffBillAccountNo"
        Me.colStaffBillAccountNo.ReadOnly = True
        '
        'colStaffBillCustomFee
        '
        Me.colStaffBillCustomFee.DataPropertyName = "CustomFee"
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle26.Format = "N2"
        DataGridViewCellStyle26.NullValue = Nothing
        Me.colStaffBillCustomFee.DefaultCellStyle = DataGridViewCellStyle26
        Me.colStaffBillCustomFee.HeaderText = "Custom Fee"
        Me.colStaffBillCustomFee.MaxInputLength = 12
        Me.colStaffBillCustomFee.Name = "colStaffBillCustomFee"
        '
        'colStaffBillCurrenciesID
        '
        Me.colStaffBillCurrenciesID.DataPropertyName = "CurrenciesID"
        Me.colStaffBillCurrenciesID.DisplayStyleForCurrentCellOnly = True
        Me.colStaffBillCurrenciesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colStaffBillCurrenciesID.HeaderText = "Currency"
        Me.colStaffBillCurrenciesID.Name = "colStaffBillCurrenciesID"
        Me.colStaffBillCurrenciesID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'colStaffBillCustomFeeSaved
        '
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle27.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle27.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle27.NullValue = False
        Me.colStaffBillCustomFeeSaved.DefaultCellStyle = DataGridViewCellStyle27
        Me.colStaffBillCustomFeeSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colStaffBillCustomFeeSaved.HeaderText = "Saved"
        Me.colStaffBillCustomFeeSaved.Name = "colStaffBillCustomFeeSaved"
        Me.colStaffBillCustomFeeSaved.ReadOnly = True
        Me.colStaffBillCustomFeeSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colStaffBillCustomFeeSaved.Width = 50
        '
        'tpgServicesSpecialtyCustomCode
        '
        Me.tpgServicesSpecialtyCustomCode.Controls.Add(Me.dgvServicesSpecialtyCustomCode)
        Me.tpgServicesSpecialtyCustomCode.Location = New System.Drawing.Point(4, 22)
        Me.tpgServicesSpecialtyCustomCode.Margin = New System.Windows.Forms.Padding(2)
        Me.tpgServicesSpecialtyCustomCode.Name = "tpgServicesSpecialtyCustomCode"
        Me.tpgServicesSpecialtyCustomCode.Size = New System.Drawing.Size(727, 193)
        Me.tpgServicesSpecialtyCustomCode.TabIndex = 8
        Me.tpgServicesSpecialtyCustomCode.Tag = "ServicesSpecialtyCustomCode"
        Me.tpgServicesSpecialtyCustomCode.Text = "Specialty Custom Code"
        Me.tpgServicesSpecialtyCustomCode.UseVisualStyleBackColor = True
        '
        'dgvServicesSpecialtyCustomCode
        '
        Me.dgvServicesSpecialtyCustomCode.AllowUserToOrderColumns = True
        Me.dgvServicesSpecialtyCustomCode.BackgroundColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle29.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvServicesSpecialtyCustomCode.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle29
        Me.dgvServicesSpecialtyCustomCode.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colDoctorSpecialtyCustomCodeID, Me.colSpecialtyCustomCode, Me.colServicesSpecialtyCustomCodeSaved})
        Me.dgvServicesSpecialtyCustomCode.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvServicesSpecialtyCustomCode.EnableHeadersVisualStyles = False
        Me.dgvServicesSpecialtyCustomCode.GridColor = System.Drawing.Color.Khaki
        Me.dgvServicesSpecialtyCustomCode.Location = New System.Drawing.Point(0, 0)
        Me.dgvServicesSpecialtyCustomCode.Name = "dgvServicesSpecialtyCustomCode"
        DataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle32.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvServicesSpecialtyCustomCode.RowHeadersDefaultCellStyle = DataGridViewCellStyle32
        Me.dgvServicesSpecialtyCustomCode.Size = New System.Drawing.Size(727, 193)
        Me.dgvServicesSpecialtyCustomCode.TabIndex = 2
        Me.dgvServicesSpecialtyCustomCode.Text = "DataGridView1"
        '
        'colDoctorSpecialtyCustomCodeID
        '
        Me.colDoctorSpecialtyCustomCodeID.DataPropertyName = "DoctorSpecialtyID"
        Me.colDoctorSpecialtyCustomCodeID.DisplayStyleForCurrentCellOnly = True
        Me.colDoctorSpecialtyCustomCodeID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colDoctorSpecialtyCustomCodeID.HeaderText = "Doctor Specialty"
        Me.colDoctorSpecialtyCustomCodeID.Name = "colDoctorSpecialtyCustomCodeID"
        Me.colDoctorSpecialtyCustomCodeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colDoctorSpecialtyCustomCodeID.Width = 200
        '
        'colSpecialtyCustomCode
        '
        Me.colSpecialtyCustomCode.DataPropertyName = "CustomCode"
        DataGridViewCellStyle30.NullValue = Nothing
        Me.colSpecialtyCustomCode.DefaultCellStyle = DataGridViewCellStyle30
        Me.colSpecialtyCustomCode.HeaderText = "Custom Code"
        Me.colSpecialtyCustomCode.MaxInputLength = 20
        Me.colSpecialtyCustomCode.Name = "colSpecialtyCustomCode"
        Me.colSpecialtyCustomCode.Width = 120
        '
        'colServicesSpecialtyCustomCodeSaved
        '
        DataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle31.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle31.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle31.NullValue = False
        Me.colServicesSpecialtyCustomCodeSaved.DefaultCellStyle = DataGridViewCellStyle31
        Me.colServicesSpecialtyCustomCodeSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colServicesSpecialtyCustomCodeSaved.HeaderText = "Saved"
        Me.colServicesSpecialtyCustomCodeSaved.Name = "colServicesSpecialtyCustomCodeSaved"
        Me.colServicesSpecialtyCustomCodeSaved.ReadOnly = True
        Me.colServicesSpecialtyCustomCodeSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colServicesSpecialtyCustomCodeSaved.Width = 50
        '
        'cboServiceCode
        '
        Me.cboServiceCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboServiceCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboServiceCode.DropDownWidth = 300
        Me.cboServiceCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboServiceCode.FormattingEnabled = True
        Me.cboServiceCode.Location = New System.Drawing.Point(133, 4)
        Me.cboServiceCode.MaxLength = 20
        Me.cboServiceCode.Name = "cboServiceCode"
        Me.cboServiceCode.Size = New System.Drawing.Size(172, 21)
        Me.cboServiceCode.TabIndex = 1
        '
        'lblUnitCost
        '
        Me.lblUnitCost.Location = New System.Drawing.Point(313, 28)
        Me.lblUnitCost.Name = "lblUnitCost"
        Me.lblUnitCost.Size = New System.Drawing.Size(106, 20)
        Me.lblUnitCost.TabIndex = 10
        Me.lblUnitCost.Text = "Unit Cost"
        '
        'lblVATPercentage
        '
        Me.lblVATPercentage.Location = New System.Drawing.Point(313, 47)
        Me.lblVATPercentage.Name = "lblVATPercentage"
        Me.lblVATPercentage.Size = New System.Drawing.Size(106, 20)
        Me.lblVATPercentage.TabIndex = 12
        Me.lblVATPercentage.Text = "VAT Percentage"
        '
        'lblRevenueStream
        '
        Me.lblRevenueStream.Location = New System.Drawing.Point(313, 5)
        Me.lblRevenueStream.Name = "lblRevenueStream"
        Me.lblRevenueStream.Size = New System.Drawing.Size(106, 20)
        Me.lblRevenueStream.TabIndex = 8
        Me.lblRevenueStream.Text = "Revenue Stream"
        '
        'colInsuranceName
        '
        Me.colInsuranceName.DataPropertyName = "InsuranceNo"
        Me.colInsuranceName.DisplayStyleForCurrentCellOnly = True
        Me.colInsuranceName.DropDownWidth = 200
        Me.colInsuranceName.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colInsuranceName.HeaderText = "Insurance Name"
        Me.colInsuranceName.Name = "colInsuranceName"
        Me.colInsuranceName.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colInsuranceName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colInsuranceName.Width = 200
        '
        'colInsuranceNo
        '
        Me.colInsuranceNo.DataPropertyName = "InsuranceNo"
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Info
        Me.colInsuranceNo.DefaultCellStyle = DataGridViewCellStyle7
        Me.colInsuranceNo.HeaderText = "Insurance No"
        Me.colInsuranceNo.Name = "colInsuranceNo"
        Me.colInsuranceNo.ReadOnly = True
        '
        'colInsuranceCustomFee
        '
        Me.colInsuranceCustomFee.DataPropertyName = "CustomFee"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.colInsuranceCustomFee.DefaultCellStyle = DataGridViewCellStyle8
        Me.colInsuranceCustomFee.HeaderText = "Custom Fee"
        Me.colInsuranceCustomFee.MaxInputLength = 12
        Me.colInsuranceCustomFee.Name = "colInsuranceCustomFee"
        '
        'colInsuranceCurrenciesID
        '
        Me.colInsuranceCurrenciesID.DataPropertyName = "CurrenciesID"
        Me.colInsuranceCurrenciesID.DisplayStyleForCurrentCellOnly = True
        Me.colInsuranceCurrenciesID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colInsuranceCurrenciesID.HeaderText = "Currency"
        Me.colInsuranceCurrenciesID.Name = "colInsuranceCurrenciesID"
        Me.colInsuranceCurrenciesID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'ColInsuranceRequiresPayment
        '
        Me.ColInsuranceRequiresPayment.DataPropertyName = "RequiresPayment"
        Me.ColInsuranceRequiresPayment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ColInsuranceRequiresPayment.HeaderText = "Requires Payment"
        Me.ColInsuranceRequiresPayment.Name = "ColInsuranceRequiresPayment"
        Me.ColInsuranceRequiresPayment.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ColInsuranceRequiresPayment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'colInsuranceCustomFeeSaved
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle9.FormatProvider = New System.Globalization.CultureInfo("en-GB")
        DataGridViewCellStyle9.NullValue = False
        Me.colInsuranceCustomFeeSaved.DefaultCellStyle = DataGridViewCellStyle9
        Me.colInsuranceCustomFeeSaved.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.colInsuranceCustomFeeSaved.HeaderText = "Saved"
        Me.colInsuranceCustomFeeSaved.Name = "colInsuranceCustomFeeSaved"
        Me.colInsuranceCustomFeeSaved.ReadOnly = True
        Me.colInsuranceCustomFeeSaved.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colInsuranceCustomFeeSaved.Width = 50
        '
        'frmServices
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.fbnClose
        Me.ClientSize = New System.Drawing.Size(754, 403)
        Me.Controls.Add(Me.cboRevenueStream)
        Me.Controls.Add(Me.lblRevenueStream)
        Me.Controls.Add(Me.nbxVATPercentage)
        Me.Controls.Add(Me.lblVATPercentage)
        Me.Controls.Add(Me.nbxUnitCost)
        Me.Controls.Add(Me.lblUnitCost)
        Me.Controls.Add(Me.cboServiceCode)
        Me.Controls.Add(Me.chkHidden)
        Me.Controls.Add(Me.tbcServicesOtherFees)
        Me.Controls.Add(Me.fcbServicePointID)
        Me.Controls.Add(Me.lblServicePointID)
        Me.Controls.Add(Me.fcbServiceBillAtID)
        Me.Controls.Add(Me.lblServiceBillAtID)
        Me.Controls.Add(Me.nbxStandardFee)
        Me.Controls.Add(Me.lblStandardFee)
        Me.Controls.Add(Me.stbServiceName)
        Me.Controls.Add(Me.lblServiceName)
        Me.Controls.Add(Me.lblServiceCode)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.fbnDelete)
        Me.Controls.Add(Me.ebnSaveUpdate)
        Me.Controls.Add(Me.fbnClose)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmServices"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Services"
        Me.tbcServicesOtherFees.ResumeLayout(False)
        Me.tpgBillCustomFee.ResumeLayout(False)
        CType(Me.dgvBillCustomFee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgInsuranceCustomFee.ResumeLayout(False)
        CType(Me.dgvInsuranceCustomFee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgServicesDrSpecialtyFee.ResumeLayout(False)
        CType(Me.dgvServicesDrSpecialtyFee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgServicesStaffFee.ResumeLayout(False)
        CType(Me.dgvServicesStaffFee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgServicesSpecialtyBillCustomFee.ResumeLayout(False)
        CType(Me.dgvServicesSpecialtyBillCustomFee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgServicesStaffBillCustomFee.ResumeLayout(False)
        CType(Me.dgvServicesStaffBillCustomFee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgServicesSpecialtyCustomCode.ResumeLayout(False)
        CType(Me.dgvServicesSpecialtyCustomCode, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
    Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
    Friend WithEvents fcbServiceBillAtID As SyncSoft.Common.Win.Controls.FlatComboBox
    Friend WithEvents lblServiceBillAtID As System.Windows.Forms.Label
    Friend WithEvents nbxStandardFee As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblStandardFee As System.Windows.Forms.Label
    Friend WithEvents stbServiceName As SyncSoft.Common.Win.Controls.SmartTextBox
    Friend WithEvents lblServiceName As System.Windows.Forms.Label
    Friend WithEvents lblServiceCode As System.Windows.Forms.Label
    Friend WithEvents fcbServicePointID As SyncSoft.Common.Win.Controls.FlatComboBox
    Friend WithEvents lblServicePointID As System.Windows.Forms.Label
    Friend WithEvents tbcServicesOtherFees As System.Windows.Forms.TabControl
    Friend WithEvents tpgServicesDrSpecialtyFee As System.Windows.Forms.TabPage
    Friend WithEvents tpgServicesStaffFee As System.Windows.Forms.TabPage
    Friend WithEvents dgvServicesDrSpecialtyFee As System.Windows.Forms.DataGridView
    Friend WithEvents dgvServicesStaffFee As System.Windows.Forms.DataGridView
    Friend WithEvents chkHidden As System.Windows.Forms.CheckBox
    Friend WithEvents tpgBillCustomFee As System.Windows.Forms.TabPage
    Friend WithEvents tpgInsuranceCustomFee As System.Windows.Forms.TabPage
    Friend WithEvents dgvBillCustomFee As System.Windows.Forms.DataGridView
    Friend WithEvents dgvInsuranceCustomFee As System.Windows.Forms.DataGridView
    Friend WithEvents cboServiceCode As System.Windows.Forms.ComboBox
    Friend WithEvents nbxUnitCost As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblUnitCost As System.Windows.Forms.Label
    Friend WithEvents colDoctorSpecialtyID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colSpecialtyFee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDrSpecialtyFeeCurrenciesID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colServicesDrSpecialtyFeeSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colStaffNo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colStaffFee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colStaffFeeCurrenciesID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colServicesStaffFeeSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents nbxVATPercentage As SyncSoft.Common.Win.Controls.NumericBox
    Friend WithEvents lblVATPercentage As Label
    Friend WithEvents cboRevenueStream As ComboBox
    Friend WithEvents lblRevenueStream As Label
    Friend WithEvents tpgServicesSpecialtyBillCustomFee As System.Windows.Forms.TabPage
    Friend WithEvents dgvServicesSpecialtyBillCustomFee As System.Windows.Forms.DataGridView
    Friend WithEvents colDoctorSpecialtyBillID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colSpecialtyBillCustomerName As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colSpecialtyBillAccountNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSpecialtyBillCustomFee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSpecialtyBillCurrenciesID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colSpecialtyBillCustomFeeSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents tpgServicesStaffBillCustomFee As System.Windows.Forms.TabPage
    Friend WithEvents tpgServicesSpecialtyCustomCode As System.Windows.Forms.TabPage
    Friend WithEvents dgvServicesStaffBillCustomFee As System.Windows.Forms.DataGridView
    Friend WithEvents colStaffBillID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colStaffBillCustomerName As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colStaffBillAccountNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colStaffBillCustomFee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colStaffBillCurrenciesID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colStaffBillCustomFeeSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgvServicesSpecialtyCustomCode As System.Windows.Forms.DataGridView
    Friend WithEvents colDoctorSpecialtyCustomCodeID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colSpecialtyCustomCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colServicesSpecialtyCustomCodeSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colBillCustomerName As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colAccountNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBillCustomFee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBillCurrenciesID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents ColRequiresPayment As SyncSoft.Common.Win.Controls.GridComboBoxColumn
    Friend WithEvents colBillCustomFeeSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colInsuranceName As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colInsuranceNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInsuranceCustomFee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInsuranceCurrenciesID As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents ColInsuranceRequiresPayment As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colInsuranceCustomFeeSaved As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
