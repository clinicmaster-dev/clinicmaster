
Public Class TheatreRecordChart : Inherits DBConnect

#Region " Fields "

#End Region

#Region " Auto-Implemented Properties "

Public Property RecordChartID As Integer
Public Property RecordChartNo As String
Public Property TheatreNo As String
Public Property Anesthetist As String
Public Property RecordChartDateTime As Date
Public Property RecordDateTime As Date

#End Region

#Region " Constructors "

Public Sub New()
	MyBase.New()
End Sub

Public Sub New(ByVal serverName As String, ByVal databaseName As String)
	MyClass.New()
	Me.ServerName = serverName
	Me.DatabaseName = databaseName
End Sub

#End Region

#Region " Methods "

Protected Overrides Function SaveData() As ArrayList

	Me.SetCommand("uspInsertTheatreRecordChart")

	With Parameters
		.Add(New ParameterSQL("RecordChartID", Me.RecordChartID))
		.Add(New ParameterSQL("RecordChartNo", Me.RecordChartNo))
		.Add(New ParameterSQL("TheatreNo", Me.TheatreNo))
		.Add(New ParameterSQL("Anesthetist", Me.Anesthetist))
		.Add(New ParameterSQL("RecordChartDateTime", Me.RecordChartDateTime))
		.Add(New ParameterSQL("LoginID", Me.LoginID))
		.Add(New ParameterSQL("ClientMachine", My.Computer.Name))
		.Add(New ParameterSQL("RecordDateTime", Me.RecordDateTime))
	End With

	Return Parameters

End Function

Protected Overrides Function UpdateData() As ArrayList

	Me.SetCommand("uspUpdateTheatreRecordChart")

	With Parameters
		.Add(New ParameterSQL("RecordChartID", Me.RecordChartID))
		.Add(New ParameterSQL("RecordChartNo", Me.RecordChartNo))
		.Add(New ParameterSQL("TheatreNo", Me.TheatreNo))
		.Add(New ParameterSQL("Anesthetist", Me.Anesthetist))
		.Add(New ParameterSQL("RecordChartDateTime", Me.RecordChartDateTime))
		.Add(New ParameterSQL("LoginID", Me.LoginID))
		.Add(New ParameterSQL("ClientMachine", My.Computer.Name))
		.Add(New ParameterSQL("RecordDateTime", Me.RecordDateTime))
	End With

	'For Audit Trail
	Me.SetLogObject("TheatreRecordChart")

	Return Parameters

End Function

Protected Overrides Function DeleteData() As ArrayList

	Dim where As String = "RecordChartNo = '" + Me.RecordChartNo + "' and TheatreNo = '" + Me.TheatreNo + "'
	Dim errorPart As String = "Record Chart No: " + Me.RecordChartNo + " and Theatre No: " + Me.TheatreNo

	Me.SetCommand("uspDeleteObject")

	With Parameters
		.Add(New ParameterSQL("ObjectName", "TheatreRecordChart"))
		.Add(New ParameterSQL("Where", where))
		.Add(New ParameterSQL("ErrorPart", errorPart))
	End With

	'For Audit Trail
	Me.SetLogObject("TheatreRecordChart")

	Return Parameters

End Function

Public Function GetTheatreRecordChart(ByVal recordChartNo As String, ByVal theatreNo As String) As DataSet

	With Parameters
		.Add(New ParameterSQL("RecordChartNo", recordChartNo))
		.Add(New ParameterSQL("TheatreNo", theatreNo))
	End With

	Return Me.Load("uspGetTheatreRecordChart", "TheatreRecordChart", Parameters)

End Function

#End Region



End Class