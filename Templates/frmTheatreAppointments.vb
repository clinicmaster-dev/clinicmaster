
Option Strict On

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls

Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects

Public Class frmTheatreAppointments

#Region " Fields "

#End Region

Private Sub frmTheatreAppointments_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

	Try
		Me.Cursor = Cursors.WaitCursor()



	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

Private Sub frmTheatreAppointments_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
	If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
End Sub

Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
	Me.Close()
End Sub

Private Sub fbnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnDelete.Click

Dim oTheatreAppointments As New SyncSoft.SQLDb.TheatreAppointments()

	Try
		Me.Cursor = Cursors.WaitCursor()

		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		If DeleteMessage() = Windows.Forms.DialogResult.No Then Return

		oTheatreAppointments.VisitNo = StringEnteredIn(Me.stbVisitNo, "Visit No!")
		oTheatreAppointments.ProcedureCode = StringEnteredIn(Me.stbProcedureCode, "Procedure Code!")

		DisplayMessage(oTheatreAppointments.Delete())
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		ResetControlsIn(Me)
		Me.CallOnKeyEdit()

	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

Private Sub fbnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnSearch.Click

Dim visitNo As String
Dim procedureCode As String

Dim oTheatreAppointments As New SyncSoft.SQLDb.TheatreAppointments()

	Try
		Me.Cursor = Cursors.WaitCursor()

		visitNo = StringEnteredIn(Me.stbVisitNo, "Visit No!")
		procedureCode = StringEnteredIn(Me.stbProcedureCode, "Procedure Code!")

		Dim dataSource As DataTable = oTheatreAppointments.GetTheatreAppointments(visitNo, procedureCode).Tables("TheatreAppointments")
		Me.DisplayData(dataSource)

	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

Private Sub ebnSaveUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ebnSaveUpdate.Click

Dim oTheatreAppointments As New SyncSoft.SQLDb.TheatreAppointments()

	Try
		Me.Cursor = Cursors.WaitCursor()

		With oTheatreAppointments

		.VisitNo = StringEnteredIn(Me.stbVisitNo, "Visit No!")
		.ProcedureCode = StringEnteredIn(Me.stbProcedureCode, "Procedure Code!")
		.StartDateTime = DateEnteredIn(Me.dtpStartDateTime, "Start Date!")
		.Duration = Me.nbxDuration.GetInteger()
		.EndDate = DateEnteredIn(Me.dtpEndDate, "End Date!")
		.StaffNo = StringEnteredIn(Me.stbStaffNo, "Staff No!")
		.AppointmentDes = StringEnteredIn(Me.stbAppointmentDes, "Appointment Description!")
		.AppointmentStatusID = StringEnteredIn(Me.stbAppointmentStatusID, "Appointment Status!")
		.LoginID = StringEnteredIn(Me.stbLoginID, "LoginID!")
		.RecordDateTime = DateEnteredIn(Me.dtpRecordDateTime, "RecordDateTime!")

		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		ValidateEntriesIn(Me)
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		End With

		Select Case Me.ebnSaveUpdate.ButtonText

		Case ButtonCaption.Save

		oTheatreAppointments.Save()

		''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		ResetControlsIn(Me)
		''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		Case ButtonCaption.Update

		DisplayMessage(oTheatreAppointments.Update())
		Me.CallOnKeyEdit()

	End Select

	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

#Region " Edit Methods "

Public Sub Edit()

	Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update
	Me.ebnSaveUpdate.Enabled = False
	Me.fbnDelete.Visible = True
	Me.fbnDelete.Enabled = False
	Me.fbnSearch.Visible = True

	ResetControlsIn(Me)

End Sub

Public Sub Save()

	Me.ebnSaveUpdate.ButtonText = ButtonCaption.Save
	Me.ebnSaveUpdate.Enabled = True
	Me.fbnDelete.Visible = False
	Me.fbnDelete.Enabled = True
	Me.fbnSearch.Visible = False

	ResetControlsIn(Me)

End Sub

Private Sub DisplayData(ByVal dataSource As DataTable)

Try

	Me.ebnSaveUpdate.DataSource = dataSource
	Me.ebnSaveUpdate.LoadData(Me)

	Me.ebnSaveUpdate.Enabled = dataSource.Rows.Count > 0
	Me.fbnDelete.Enabled = dataSource.Rows.Count > 0

	Security.Apply(Me.ebnSaveUpdate, AccessRights.Update)
	Security.Apply(Me.fbnDelete, AccessRights.Delete)

Catch ex As Exception
	Throw ex
End Try

End Sub

Private Sub CallOnKeyEdit()
If Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update Then
	Me.ebnSaveUpdate.Enabled = False
	Me.fbnDelete.Enabled = False
End If
End Sub

#End Region

End Class