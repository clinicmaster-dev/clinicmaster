
------------------------------------------------------------------------------------------------------
-------------- Create Table: PreTheatreChecklist ------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'PreTheatreChecklist')
	drop table PreTheatreChecklist
go

create table PreTheatreChecklist
(TheatreNo varchar(20) not null
constraint fkTheatreNoPreTheatreChecklist references TheatreAdmissions (TheatreNo)
constraint pkTheatreNo primary key,
Anesthetist varchar(20)
constraint fkAnesthetistPreTheatreChecklist references Staff (Anesthetist),
ConfirmedPatientIdentity varchar(10)
constraint fkConfirmedPatientIdentityPreTheatreChecklist references LookupData (DataID),
PatientConsented varchar(10)
constraint fkPatientConsentedPreTheatreChecklist references LookupData (DataID),
SiteMarked varchar(10)
constraint fkSiteMarkedPreTheatreChecklist references LookupData (DataID),
AnesthesiaMachineChecked varchar(10)
constraint fkAnesthesiaMachineCheckedPreTheatreChecklist references LookupData (DataID),
AnesthesiaMedicationChecked varchar(10)
constraint fkAnesthesiaMedicationCheckedPreTheatreChecklist references LookupData (DataID),
PulseOximeterOnFunctional varchar(10)
constraint fkPulseOximeterOnFunctionalPreTheatreChecklist references LookupData (DataID),
KnownAllergies varchar(10)
constraint fkKnownAllergiesPreTheatreChecklist references LookupData (DataID),
AirWayAspirationRisk varchar(10)
constraint fkAirWayAspirationRiskPreTheatreChecklist references LookupData (DataID),
HighBloodLossRisk varchar(10)
constraint fkHighBloodLossRiskPreTheatreChecklist references LookupData (DataID),
LoginID varchar(10)
constraint fkLoginIDPreTheatreChecklist references Logins (LoginID),
ClientMachine varchar(40) constraint dfClientMachinePreTheatreChecklist default host_name(),
RecordDateTime smalldatetime constraint dfRecordDateTimePreTheatreChecklist default getDate()
)
go


------------------------------------------------------------------------------------------------------
-------------- PreTheatreChecklist --------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-------------- Insert PreTheatreChecklist -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspInsertPreTheatreChecklist')
	drop proc uspInsertPreTheatreChecklist
go

create proc uspInsertPreTheatreChecklist(
@TheatreNo varchar(20),
@Anesthetist varchar(20),
@ConfirmedPatientIdentity varchar(10),
@PatientConsented varchar(10),
@SiteMarked varchar(10),
@AnesthesiaMachineChecked varchar(10),
@AnesthesiaMedicationChecked varchar(10),
@PulseOximeterOnFunctional varchar(10),
@KnownAllergies varchar(10),
@AirWayAspirationRisk varchar(10),
@HighBloodLossRisk varchar(10),
@LoginID varchar(10),
@ClientMachine varchar(40),
@RecordDateTime smalldatetime
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select TheatreNo from TheatreAdmissions where TheatreNo  = @TheatreNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'TheatreAdmissions')
		return 1
	end

if exists(select TheatreNo from PreTheatreChecklist where TheatreNo = @TheatreNo)
	begin
		set @ErrorMSG = 'The record with %s: %s, you are trying to enter already exists'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo)
		return 1
	end



begin
insert into PreTheatreChecklist
(TheatreNo, Anesthetist, ConfirmedPatientIdentity, PatientConsented, SiteMarked, AnesthesiaMachineChecked, AnesthesiaMedicationChecked, PulseOximeterOnFunctional, KnownAllergies, AirWayAspirationRisk, HighBloodLossRisk, LoginID, ClientMachine, RecordDateTime)
values
(@TheatreNo, @Anesthetist, @ConfirmedPatientIdentity, @PatientConsented, @SiteMarked, @AnesthesiaMachineChecked, @AnesthesiaMedicationChecked, @PulseOximeterOnFunctional, @KnownAllergies, @AirWayAspirationRisk, @HighBloodLossRisk, @LoginID, @ClientMachine, @RecordDateTime)
return 0
end
go

/******************************************************************************************************
exec uspInsertPreTheatreChecklist
******************************************************************************************************/
-- select * from PreTheatreChecklist
-- delete from PreTheatreChecklist


-------------- Update PreTheatreChecklist -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspUpdatePreTheatreChecklist')
	drop proc uspUpdatePreTheatreChecklist
go

create proc uspUpdatePreTheatreChecklist(
@TheatreNo varchar(20),
@Anesthetist varchar(20),
@ConfirmedPatientIdentity varchar(10),
@PatientConsented varchar(10),
@SiteMarked varchar(10),
@AnesthesiaMachineChecked varchar(10),
@AnesthesiaMedicationChecked varchar(10),
@PulseOximeterOnFunctional varchar(10),
@KnownAllergies varchar(10),
@AirWayAspirationRisk varchar(10),
@HighBloodLossRisk varchar(10),
@LoginID varchar(10),
@ClientMachine varchar(40),
@RecordDateTime smalldatetime
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select TheatreNo from PreTheatreChecklist where TheatreNo = @TheatreNo)
	begin
		set @ErrorMSG = 'The record with %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'PreTheatreChecklist')
		return 1
	end



begin
update PreTheatreChecklist set
Anesthetist = @Anesthetist, ConfirmedPatientIdentity = @ConfirmedPatientIdentity, PatientConsented = @PatientConsented, SiteMarked = @SiteMarked, AnesthesiaMachineChecked = @AnesthesiaMachineChecked, AnesthesiaMedicationChecked = @AnesthesiaMedicationChecked, PulseOximeterOnFunctional = @PulseOximeterOnFunctional, KnownAllergies = @KnownAllergies, AirWayAspirationRisk = @AirWayAspirationRisk, HighBloodLossRisk = @HighBloodLossRisk, LoginID = @LoginID, ClientMachine = @ClientMachine, RecordDateTime = @RecordDateTime
where TheatreNo = @TheatreNo
return 0
end
go

/******************************************************************************************************
exec uspUpdatePreTheatreChecklist
******************************************************************************************************/
-- select * from PreTheatreChecklist
-- delete from PreTheatreChecklist


-------------- Get PreTheatreChecklist -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspGetPreTheatreChecklist')
	drop proc uspGetPreTheatreChecklist
go

create proc uspGetPreTheatreChecklist(
@TheatreNo varchar(20)
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select TheatreNo from PreTheatreChecklist where TheatreNo = @TheatreNo)
	begin
		set @ErrorMSG = 'The record with %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'PreTheatreChecklist')
		return 1
	end
else
begin
	select TheatreNo, Anesthetist, ConfirmedPatientIdentity, PatientConsented, SiteMarked, AnesthesiaMachineChecked, AnesthesiaMedicationChecked, PulseOximeterOnFunctional, KnownAllergies, AirWayAspirationRisk, HighBloodLossRisk, LoginID, ClientMachine, RecordDateTime
	from PreTheatreChecklist

	where TheatreNo = @TheatreNo
return 0
end
go

/******************************************************************************************************
exec uspGetPreTheatreChecklist
******************************************************************************************************/
-- select * from PreTheatreChecklist
-- delete from PreTheatreChecklist


------------------------------------------------------------------------------------------------------
-------------- Update: PreTheatreChecklist ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrUpdatePreTheatreChecklist')
	drop trigger utrUpdatePreTheatreChecklist
go

create trigger utrUpdatePreTheatreChecklist
on PreTheatreChecklist
for update
as
declare @ErrorMSG varchar(200)
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Updates ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'U')
if @ObjectName is null return
if @ObjectName <> 'PreTheatreChecklist' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return



------------------- Key-TheatreNo -----------------------------------------------------

begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'TheatreNo', Deleted.TheatreNo, Inserted.TheatreNo
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
end

-------------------  Anesthetist  -----------------------------------------------------

if update(Anesthetist)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'Anesthetist', Deleted.Anesthetist, Inserted.Anesthetist
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.Anesthetist <> Deleted.Anesthetist
end

-------------------  ConfirmedPatientIdentity  -----------------------------------------------------

if update(ConfirmedPatientIdentity)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'ConfirmedPatientIdentity', Deleted.ConfirmedPatientIdentity, Inserted.ConfirmedPatientIdentity
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.ConfirmedPatientIdentity <> Deleted.ConfirmedPatientIdentity
end

-------------------  PatientConsented  -----------------------------------------------------

if update(PatientConsented)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'PatientConsented', Deleted.PatientConsented, Inserted.PatientConsented
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.PatientConsented <> Deleted.PatientConsented
end

-------------------  SiteMarked  -----------------------------------------------------

if update(SiteMarked)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'SiteMarked', Deleted.SiteMarked, Inserted.SiteMarked
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.SiteMarked <> Deleted.SiteMarked
end

-------------------  AnesthesiaMachineChecked  -----------------------------------------------------

if update(AnesthesiaMachineChecked)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'AnesthesiaMachineChecked', Deleted.AnesthesiaMachineChecked, Inserted.AnesthesiaMachineChecked
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.AnesthesiaMachineChecked <> Deleted.AnesthesiaMachineChecked
end

-------------------  AnesthesiaMedicationChecked  -----------------------------------------------------

if update(AnesthesiaMedicationChecked)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'AnesthesiaMedicationChecked', Deleted.AnesthesiaMedicationChecked, Inserted.AnesthesiaMedicationChecked
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.AnesthesiaMedicationChecked <> Deleted.AnesthesiaMedicationChecked
end

-------------------  PulseOximeterOnFunctional  -----------------------------------------------------

if update(PulseOximeterOnFunctional)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'PulseOximeterOnFunctional', Deleted.PulseOximeterOnFunctional, Inserted.PulseOximeterOnFunctional
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.PulseOximeterOnFunctional <> Deleted.PulseOximeterOnFunctional
end

-------------------  KnownAllergies  -----------------------------------------------------

if update(KnownAllergies)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'KnownAllergies', Deleted.KnownAllergies, Inserted.KnownAllergies
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.KnownAllergies <> Deleted.KnownAllergies
end

-------------------  AirWayAspirationRisk  -----------------------------------------------------

if update(AirWayAspirationRisk)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'AirWayAspirationRisk', Deleted.AirWayAspirationRisk, Inserted.AirWayAspirationRisk
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.AirWayAspirationRisk <> Deleted.AirWayAspirationRisk
end

-------------------  HighBloodLossRisk  -----------------------------------------------------

if update(HighBloodLossRisk)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'HighBloodLossRisk', Deleted.HighBloodLossRisk, Inserted.HighBloodLossRisk
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.HighBloodLossRisk <> Deleted.HighBloodLossRisk
end

-------------------  LoginID  -----------------------------------------------------

if update(LoginID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'LoginID', Deleted.LoginID, Inserted.LoginID
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.LoginID <> Deleted.LoginID
end

-------------------  ClientMachine  -----------------------------------------------------

if update(ClientMachine)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'ClientMachine', Deleted.ClientMachine, Inserted.ClientMachine
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.ClientMachine <> Deleted.ClientMachine
end

-------------------  RecordDateTime  -----------------------------------------------------

if update(RecordDateTime)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'RecordDateTime', dbo.FormatDateTime(Deleted.RecordDateTime), dbo.FormatDateTime(Inserted.RecordDateTime)
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.RecordDateTime <> Deleted.RecordDateTime
end
go


------------------------------------------------------------------------------------------------------
-------------- Delete: PreTheatreChecklist ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrDeletePreTheatreChecklist')
	drop trigger utrDeletePreTheatreChecklist
go

create trigger utrDeletePreTheatreChecklist
on PreTheatreChecklist
for delete
as
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Deletes ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'D')
if @ObjectName is null return
if @ObjectName <> 'PreTheatreChecklist' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return

-------------------  TheatreNo  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'TheatreNo', Deleted.TheatreNo, null from Deleted

-------------------  Anesthetist  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'Anesthetist', Deleted.Anesthetist, null from Deleted

-------------------  ConfirmedPatientIdentity  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'ConfirmedPatientIdentity', Deleted.ConfirmedPatientIdentity, null from Deleted

-------------------  PatientConsented  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'PatientConsented', Deleted.PatientConsented, null from Deleted

-------------------  SiteMarked  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'SiteMarked', Deleted.SiteMarked, null from Deleted

-------------------  AnesthesiaMachineChecked  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'AnesthesiaMachineChecked', Deleted.AnesthesiaMachineChecked, null from Deleted

-------------------  AnesthesiaMedicationChecked  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'AnesthesiaMedicationChecked', Deleted.AnesthesiaMedicationChecked, null from Deleted

-------------------  PulseOximeterOnFunctional  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'PulseOximeterOnFunctional', Deleted.PulseOximeterOnFunctional, null from Deleted

-------------------  KnownAllergies  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'KnownAllergies', Deleted.KnownAllergies, null from Deleted

-------------------  AirWayAspirationRisk  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'AirWayAspirationRisk', Deleted.AirWayAspirationRisk, null from Deleted

-------------------  HighBloodLossRisk  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'HighBloodLossRisk', Deleted.HighBloodLossRisk, null from Deleted

-------------------  LoginID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'LoginID', Deleted.LoginID, null from Deleted

-------------------  ClientMachine  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'ClientMachine', Deleted.ClientMachine, null from Deleted

-------------------  RecordDateTime  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'RecordDateTime', dbo.FormatDateTime(Deleted.RecordDateTime), null from Deleted
go

