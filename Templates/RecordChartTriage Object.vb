
Public Class RecordChartTriage : Inherits DBConnect

#Region " Fields "

#End Region

#Region " Auto-Implemented Properties "

Public Property RecordChartNo As String
Public Property VitalID As String
Public Property Value As String
Public Property RecordDateTime As Date

#End Region

#Region " Constructors "

Public Sub New()
	MyBase.New()
End Sub

Public Sub New(ByVal serverName As String, ByVal databaseName As String)
	MyClass.New()
	Me.ServerName = serverName
	Me.DatabaseName = databaseName
End Sub

#End Region

#Region " Methods "

Protected Overrides Function SaveData() As ArrayList

	Me.SetCommand("uspInsertRecordChartTriage")

	With Parameters
		.Add(New ParameterSQL("RecordChartNo", Me.RecordChartNo))
		.Add(New ParameterSQL("VitalID", Me.VitalID))
		.Add(New ParameterSQL("Value", Me.Value))
		.Add(New ParameterSQL("LoginID", Me.LoginID))
		.Add(New ParameterSQL("ClientMachine", My.Computer.Name))
		.Add(New ParameterSQL("RecordDateTime", Me.RecordDateTime))
	End With

	Return Parameters

End Function

Protected Overrides Function UpdateData() As ArrayList

	Me.SetCommand("uspUpdateRecordChartTriage")

	With Parameters
		.Add(New ParameterSQL("RecordChartNo", Me.RecordChartNo))
		.Add(New ParameterSQL("VitalID", Me.VitalID))
		.Add(New ParameterSQL("Value", Me.Value))
		.Add(New ParameterSQL("LoginID", Me.LoginID))
		.Add(New ParameterSQL("ClientMachine", My.Computer.Name))
		.Add(New ParameterSQL("RecordDateTime", Me.RecordDateTime))
	End With

	'For Audit Trail
	Me.SetLogObject("RecordChartTriage")

	Return Parameters

End Function

Protected Overrides Function DeleteData() As ArrayList

	Dim where As String = "RecordChartNo = '" + Me.RecordChartNo + "'
	Dim errorPart As String = "Record Chart No: " + Me.RecordChartNo

	Me.SetCommand("uspDeleteObject")

	With Parameters
		.Add(New ParameterSQL("ObjectName", "RecordChartTriage"))
		.Add(New ParameterSQL("Where", where))
		.Add(New ParameterSQL("ErrorPart", errorPart))
	End With

	'For Audit Trail
	Me.SetLogObject("RecordChartTriage")

	Return Parameters

End Function

Public Function GetRecordChartTriage(ByVal recordChartNo As String) As DataSet

	With Parameters
		.Add(New ParameterSQL("RecordChartNo", recordChartNo))
	End With

	Return Me.Load("uspGetRecordChartTriage", "RecordChartTriage", Parameters)

End Function

#End Region



End Class