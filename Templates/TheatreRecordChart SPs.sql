
------------------------------------------------------------------------------------------------------
-------------- Create Table: TheatreRecordChart ------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'TheatreRecordChart')
	drop table TheatreRecordChart
go

create table TheatreRecordChart
(RecordChartID int not null,
RecordChartNo varchar(20) not null
constraint fkRecordChartNoTheatreRecordChart references TheatreAdmissions (RecordChartNo),
TheatreNo varchar(20) not null
constraint fkTheatreNoTheatreRecordChart references TheatreAdmissions (TheatreNo),
constraint pkRecordChartNoTheatreNo primary key(RecordChartNo, TheatreNo),
Anesthetist varchar(20)
constraint fkAnesthetistTheatreRecordChart references Staff (Anesthetist),
RecordChartDateTime SmallDateTime,
LoginID varchar(20)
constraint fkLoginIDTheatreRecordChart references Logins (LoginID),
ClientMachine varchar(40) constraint dfClientMachineTheatreRecordChart default host_name(),
RecordDateTime smalldatetime constraint dfRecordDateTimeTheatreRecordChart default getdate()
)
go


------------------------------------------------------------------------------------------------------
-------------- TheatreRecordChart --------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-------------- Insert TheatreRecordChart -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspInsertTheatreRecordChart')
	drop proc uspInsertTheatreRecordChart
go

create proc uspInsertTheatreRecordChart(
@RecordChartID int,
@RecordChartNo varchar(20),
@TheatreNo varchar(20),
@Anesthetist varchar(20),
@RecordChartDateTime SmallDateTime,
@LoginID varchar(20),
@ClientMachine varchar(40),
@RecordDateTime smalldatetime
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select RecordChartNo from TheatreAdmissions where RecordChartNo  = @RecordChartNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Record Chart No', @RecordChartNo, 'TheatreAdmissions')
		return 1
	end

if not exists(select TheatreNo from TheatreAdmissions where TheatreNo  = @TheatreNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'TheatreAdmissions')
		return 1
	end

if exists(select RecordChartNo from TheatreRecordChart where RecordChartNo = @RecordChartNo and TheatreNo = @TheatreNo)
	begin
		set @ErrorMSG = 'The record with %s: %s and %s: %s, you are trying to enter already exists'
		raiserror(@ErrorMSG, 16, 1, 'Record Chart No', @RecordChartNo, 'Theatre No', @TheatreNo)
		return 1
	end



begin
insert into TheatreRecordChart
(RecordChartID, RecordChartNo, TheatreNo, Anesthetist, RecordChartDateTime, LoginID, ClientMachine, RecordDateTime)
values
(@RecordChartID, @RecordChartNo, @TheatreNo, @Anesthetist, @RecordChartDateTime, @LoginID, @ClientMachine, @RecordDateTime)
return 0
end
go

/******************************************************************************************************
exec uspInsertTheatreRecordChart
******************************************************************************************************/
-- select * from TheatreRecordChart
-- delete from TheatreRecordChart


-------------- Update TheatreRecordChart -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspUpdateTheatreRecordChart')
	drop proc uspUpdateTheatreRecordChart
go

create proc uspUpdateTheatreRecordChart(
@RecordChartID int,
@RecordChartNo varchar(20),
@TheatreNo varchar(20),
@Anesthetist varchar(20),
@RecordChartDateTime SmallDateTime,
@LoginID varchar(20),
@ClientMachine varchar(40),
@RecordDateTime smalldatetime
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select RecordChartNo from TheatreRecordChart where RecordChartNo = @RecordChartNo and TheatreNo = @TheatreNo)
	begin
		set @ErrorMSG = 'The record with %s: %s and %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Record Chart No', @RecordChartNo, 'Theatre No', @TheatreNo, 'TheatreRecordChart')
		return 1
	end



begin
update TheatreRecordChart set
RecordChartID = @RecordChartID, Anesthetist = @Anesthetist, RecordChartDateTime = @RecordChartDateTime, LoginID = @LoginID, ClientMachine = @ClientMachine, RecordDateTime = @RecordDateTime
where RecordChartNo = @RecordChartNo and TheatreNo = @TheatreNo
return 0
end
go

/******************************************************************************************************
exec uspUpdateTheatreRecordChart
******************************************************************************************************/
-- select * from TheatreRecordChart
-- delete from TheatreRecordChart


-------------- Get TheatreRecordChart -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspGetTheatreRecordChart')
	drop proc uspGetTheatreRecordChart
go

create proc uspGetTheatreRecordChart(
@RecordChartNo varchar(20),
@TheatreNo varchar(20)
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select RecordChartNo from TheatreRecordChart where RecordChartNo = @RecordChartNo and TheatreNo = @TheatreNo)
	begin
		set @ErrorMSG = 'The record with %s: %s and %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Record Chart No', @RecordChartNo, 'Theatre No', @TheatreNo, 'TheatreRecordChart')
		return 1
	end
else
begin
	select RecordChartID, RecordChartNo, TheatreNo, Anesthetist, RecordChartDateTime, LoginID, ClientMachine, RecordDateTime
	from TheatreRecordChart

	where RecordChartNo = @RecordChartNo and TheatreNo = @TheatreNo
return 0
end
go

/******************************************************************************************************
exec uspGetTheatreRecordChart
******************************************************************************************************/
-- select * from TheatreRecordChart
-- delete from TheatreRecordChart


------------------------------------------------------------------------------------------------------
-------------- Update: TheatreRecordChart ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrUpdateTheatreRecordChart')
	drop trigger utrUpdateTheatreRecordChart
go

create trigger utrUpdateTheatreRecordChart
on TheatreRecordChart
for update
as
declare @ErrorMSG varchar(200)
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Updates ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'U')
if @ObjectName is null return
if @ObjectName <> 'TheatreRecordChart' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return



------------------- Key-RecordChartNo -----------------------------------------------------

begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'RecordChartNo', Deleted.RecordChartNo, Inserted.RecordChartNo
	from Inserted inner join Deleted
	on Inserted.RecordChartNo = Deleted.RecordChartNo
	and Inserted.TheatreNo = Deleted.TheatreNo
end

------------------- Key-TheatreNo -----------------------------------------------------

begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'TheatreNo', Deleted.TheatreNo, Inserted.TheatreNo
	from Inserted inner join Deleted
	on Inserted.RecordChartNo = Deleted.RecordChartNo
	and Inserted.TheatreNo = Deleted.TheatreNo
end

-------------------  RecordChartID  -----------------------------------------------------

if update(RecordChartID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'RecordChartID', Deleted.RecordChartID, Inserted.RecordChartID
	from Inserted inner join Deleted
	on Inserted.RecordChartNo = Deleted.RecordChartNo
	and Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.RecordChartID <> Deleted.RecordChartID
end

-------------------  Anesthetist  -----------------------------------------------------

if update(Anesthetist)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'Anesthetist', Deleted.Anesthetist, Inserted.Anesthetist
	from Inserted inner join Deleted
	on Inserted.RecordChartNo = Deleted.RecordChartNo
	and Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.Anesthetist <> Deleted.Anesthetist
end

-------------------  RecordChartDateTime  -----------------------------------------------------

if update(RecordChartDateTime)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'RecordChartDateTime', dbo.FormatDate(Deleted.RecordChartDateTime), dbo.FormatDate(Inserted.RecordChartDateTime)
	from Inserted inner join Deleted
	on Inserted.RecordChartNo = Deleted.RecordChartNo
	and Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.RecordChartDateTime <> Deleted.RecordChartDateTime
end

-------------------  LoginID  -----------------------------------------------------

if update(LoginID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'LoginID', Deleted.LoginID, Inserted.LoginID
	from Inserted inner join Deleted
	on Inserted.RecordChartNo = Deleted.RecordChartNo
	and Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.LoginID <> Deleted.LoginID
end

-------------------  ClientMachine  -----------------------------------------------------

if update(ClientMachine)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'ClientMachine', Deleted.ClientMachine, Inserted.ClientMachine
	from Inserted inner join Deleted
	on Inserted.RecordChartNo = Deleted.RecordChartNo
	and Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.ClientMachine <> Deleted.ClientMachine
end

-------------------  RecordDateTime  -----------------------------------------------------

if update(RecordDateTime)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'RecordDateTime', dbo.FormatDateTime(Deleted.RecordDateTime), dbo.FormatDateTime(Inserted.RecordDateTime)
	from Inserted inner join Deleted
	on Inserted.RecordChartNo = Deleted.RecordChartNo
	and Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.RecordDateTime <> Deleted.RecordDateTime
end
go


------------------------------------------------------------------------------------------------------
-------------- Delete: TheatreRecordChart ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrDeleteTheatreRecordChart')
	drop trigger utrDeleteTheatreRecordChart
go

create trigger utrDeleteTheatreRecordChart
on TheatreRecordChart
for delete
as
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Deletes ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'D')
if @ObjectName is null return
if @ObjectName <> 'TheatreRecordChart' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return

-------------------  RecordChartID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'RecordChartID', Deleted.RecordChartID, null from Deleted

-------------------  RecordChartNo  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'RecordChartNo', Deleted.RecordChartNo, null from Deleted

-------------------  TheatreNo  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'TheatreNo', Deleted.TheatreNo, null from Deleted

-------------------  Anesthetist  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'Anesthetist', Deleted.Anesthetist, null from Deleted

-------------------  RecordChartDateTime  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'RecordChartDateTime', dbo.FormatDate(Deleted.RecordChartDateTime), null from Deleted

-------------------  LoginID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'LoginID', Deleted.LoginID, null from Deleted

-------------------  ClientMachine  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'ClientMachine', Deleted.ClientMachine, null from Deleted

-------------------  RecordDateTime  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'RecordDateTime', dbo.FormatDateTime(Deleted.RecordDateTime), null from Deleted
go

