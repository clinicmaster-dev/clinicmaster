
Public Class PreTheatreChecklist : Inherits DBConnect

#Region " Fields "

#End Region

#Region " Auto-Implemented Properties "

Public Property TheatreNo As String
Public Property Anesthetist As String
Public Property ConfirmedPatientIdentity As String
Public Property PatientConsented As String
Public Property SiteMarked As String
Public Property AnesthesiaMachineChecked As String
Public Property AnesthesiaMedicationChecked As String
Public Property PulseOximeterOnFunctional As String
Public Property KnownAllergies As String
Public Property AirWayAspirationRisk As String
Public Property HighBloodLossRisk As String
Public Property RecordDateTime As Date

#End Region

#Region " Constructors "

Public Sub New()
	MyBase.New()
End Sub

Public Sub New(ByVal serverName As String, ByVal databaseName As String)
	MyClass.New()
	Me.ServerName = serverName
	Me.DatabaseName = databaseName
End Sub

#End Region

#Region " Methods "

Protected Overrides Function SaveData() As ArrayList

	Me.SetCommand("uspInsertPreTheatreChecklist")

	With Parameters
		.Add(New ParameterSQL("TheatreNo", Me.TheatreNo))
		.Add(New ParameterSQL("Anesthetist", Me.Anesthetist))
		.Add(New ParameterSQL("ConfirmedPatientIdentity", Me.ConfirmedPatientIdentity))
		.Add(New ParameterSQL("PatientConsented", Me.PatientConsented))
		.Add(New ParameterSQL("SiteMarked", Me.SiteMarked))
		.Add(New ParameterSQL("AnesthesiaMachineChecked", Me.AnesthesiaMachineChecked))
		.Add(New ParameterSQL("AnesthesiaMedicationChecked", Me.AnesthesiaMedicationChecked))
		.Add(New ParameterSQL("PulseOximeterOnFunctional", Me.PulseOximeterOnFunctional))
		.Add(New ParameterSQL("KnownAllergies", Me.KnownAllergies))
		.Add(New ParameterSQL("AirWayAspirationRisk", Me.AirWayAspirationRisk))
		.Add(New ParameterSQL("HighBloodLossRisk", Me.HighBloodLossRisk))
		.Add(New ParameterSQL("LoginID", Me.LoginID))
		.Add(New ParameterSQL("ClientMachine", My.Computer.Name))
		.Add(New ParameterSQL("RecordDateTime", Me.RecordDateTime))
	End With

	Return Parameters

End Function

Protected Overrides Function UpdateData() As ArrayList

	Me.SetCommand("uspUpdatePreTheatreChecklist")

	With Parameters
		.Add(New ParameterSQL("TheatreNo", Me.TheatreNo))
		.Add(New ParameterSQL("Anesthetist", Me.Anesthetist))
		.Add(New ParameterSQL("ConfirmedPatientIdentity", Me.ConfirmedPatientIdentity))
		.Add(New ParameterSQL("PatientConsented", Me.PatientConsented))
		.Add(New ParameterSQL("SiteMarked", Me.SiteMarked))
		.Add(New ParameterSQL("AnesthesiaMachineChecked", Me.AnesthesiaMachineChecked))
		.Add(New ParameterSQL("AnesthesiaMedicationChecked", Me.AnesthesiaMedicationChecked))
		.Add(New ParameterSQL("PulseOximeterOnFunctional", Me.PulseOximeterOnFunctional))
		.Add(New ParameterSQL("KnownAllergies", Me.KnownAllergies))
		.Add(New ParameterSQL("AirWayAspirationRisk", Me.AirWayAspirationRisk))
		.Add(New ParameterSQL("HighBloodLossRisk", Me.HighBloodLossRisk))
		.Add(New ParameterSQL("LoginID", Me.LoginID))
		.Add(New ParameterSQL("ClientMachine", My.Computer.Name))
		.Add(New ParameterSQL("RecordDateTime", Me.RecordDateTime))
	End With

	'For Audit Trail
	Me.SetLogObject("PreTheatreChecklist")

	Return Parameters

End Function

Protected Overrides Function DeleteData() As ArrayList

	Dim where As String = "TheatreNo = '" + Me.TheatreNo + "'
	Dim errorPart As String = "Theatre No: " + Me.TheatreNo

	Me.SetCommand("uspDeleteObject")

	With Parameters
		.Add(New ParameterSQL("ObjectName", "PreTheatreChecklist"))
		.Add(New ParameterSQL("Where", where))
		.Add(New ParameterSQL("ErrorPart", errorPart))
	End With

	'For Audit Trail
	Me.SetLogObject("PreTheatreChecklist")

	Return Parameters

End Function

Public Function GetPreTheatreChecklist(ByVal theatreNo As String) As DataSet

	With Parameters
		.Add(New ParameterSQL("TheatreNo", theatreNo))
	End With

	Return Me.Load("uspGetPreTheatreChecklist", "PreTheatreChecklist", Parameters)

End Function

#End Region



End Class