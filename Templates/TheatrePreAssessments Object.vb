
Public Class TheatrePreAssessments : Inherits DBConnect

#Region " Fields "

#End Region

#Region " Auto-Implemented Properties "

Public Property TheatreNo As String
Public Property LeadAnesthetist As String
Public Property OtherAnesthetist As String
Public Property Height As Single
Public Property Weight As Single
Public Property HeartRate As Integer
Public Property OxygenSaturation As Single
Public Property Hypertensive As String
Public Property Diabetic As String
Public Property HIVStatus As String
Public Property Bathed As String
Public Property CleanTheatreGown As String
Public Property Smoker As String
Public Property AlcoholUse As String
Public Property DrugUse As String
Public Property ASAClassID As String
Public Property LastSolids As Date
Public Property LastLiquids As Date
Public Property PastHistory As String
Public Property GeneralCondition As String
Public Property PreoperativeInstructions  As String
Public Property AnesthesiaConsentID As String
Public Property AnesthesiaConsentNotes As String
Public Property RecordDateTime As Date

#End Region

#Region " Constructors "

Public Sub New()
	MyBase.New()
End Sub

Public Sub New(ByVal serverName As String, ByVal databaseName As String)
	MyClass.New()
	Me.ServerName = serverName
	Me.DatabaseName = databaseName
End Sub

#End Region

#Region " Methods "

Protected Overrides Function SaveData() As ArrayList

	Me.SetCommand("uspInsertTheatrePreAssessments")

	With Parameters
		.Add(New ParameterSQL("TheatreNo", Me.TheatreNo))
		.Add(New ParameterSQL("LeadAnesthetist", Me.LeadAnesthetist))
		.Add(New ParameterSQL("OtherAnesthetist", Me.OtherAnesthetist))
		.Add(New ParameterSQL("Height", Me.Height))
		.Add(New ParameterSQL("Weight", Me.Weight))
		.Add(New ParameterSQL("HeartRate", Me.HeartRate))
		.Add(New ParameterSQL("OxygenSaturation", Me.OxygenSaturation))
		.Add(New ParameterSQL("Hypertensive", Me.Hypertensive))
		.Add(New ParameterSQL("Diabetic", Me.Diabetic))
		.Add(New ParameterSQL("HIVStatus", Me.HIVStatus))
		.Add(New ParameterSQL("Bathed", Me.Bathed))
		.Add(New ParameterSQL("CleanTheatreGown", Me.CleanTheatreGown))
		.Add(New ParameterSQL("Smoker", Me.Smoker))
		.Add(New ParameterSQL("AlcoholUse", Me.AlcoholUse))
		.Add(New ParameterSQL("DrugUse", Me.DrugUse))
		.Add(New ParameterSQL("ASAClassID", Me.ASAClassID))
		.Add(New ParameterSQL("LastSolids", Me.LastSolids))
		.Add(New ParameterSQL("LastLiquids", Me.LastLiquids))
		.Add(New ParameterSQL("PastHistory", Me.PastHistory))
		.Add(New ParameterSQL("GeneralCondition", Me.GeneralCondition))
		.Add(New ParameterSQL("PreoperativeInstructions ", Me.PreoperativeInstructions ))
		.Add(New ParameterSQL("AnesthesiaConsentID", Me.AnesthesiaConsentID))
		.Add(New ParameterSQL("AnesthesiaConsentNotes", Me.AnesthesiaConsentNotes))
		.Add(New ParameterSQL("LoginID", Me.LoginID))
		.Add(New ParameterSQL("ClientMachine", My.Computer.Name))
		.Add(New ParameterSQL("RecordDateTime", Me.RecordDateTime))
	End With

	Return Parameters

End Function

Protected Overrides Function UpdateData() As ArrayList

	Me.SetCommand("uspUpdateTheatrePreAssessments")

	With Parameters
		.Add(New ParameterSQL("TheatreNo", Me.TheatreNo))
		.Add(New ParameterSQL("LeadAnesthetist", Me.LeadAnesthetist))
		.Add(New ParameterSQL("OtherAnesthetist", Me.OtherAnesthetist))
		.Add(New ParameterSQL("Height", Me.Height))
		.Add(New ParameterSQL("Weight", Me.Weight))
		.Add(New ParameterSQL("HeartRate", Me.HeartRate))
		.Add(New ParameterSQL("OxygenSaturation", Me.OxygenSaturation))
		.Add(New ParameterSQL("Hypertensive", Me.Hypertensive))
		.Add(New ParameterSQL("Diabetic", Me.Diabetic))
		.Add(New ParameterSQL("HIVStatus", Me.HIVStatus))
		.Add(New ParameterSQL("Bathed", Me.Bathed))
		.Add(New ParameterSQL("CleanTheatreGown", Me.CleanTheatreGown))
		.Add(New ParameterSQL("Smoker", Me.Smoker))
		.Add(New ParameterSQL("AlcoholUse", Me.AlcoholUse))
		.Add(New ParameterSQL("DrugUse", Me.DrugUse))
		.Add(New ParameterSQL("ASAClassID", Me.ASAClassID))
		.Add(New ParameterSQL("LastSolids", Me.LastSolids))
		.Add(New ParameterSQL("LastLiquids", Me.LastLiquids))
		.Add(New ParameterSQL("PastHistory", Me.PastHistory))
		.Add(New ParameterSQL("GeneralCondition", Me.GeneralCondition))
		.Add(New ParameterSQL("PreoperativeInstructions ", Me.PreoperativeInstructions ))
		.Add(New ParameterSQL("AnesthesiaConsentID", Me.AnesthesiaConsentID))
		.Add(New ParameterSQL("AnesthesiaConsentNotes", Me.AnesthesiaConsentNotes))
		.Add(New ParameterSQL("LoginID", Me.LoginID))
		.Add(New ParameterSQL("ClientMachine", My.Computer.Name))
		.Add(New ParameterSQL("RecordDateTime", Me.RecordDateTime))
	End With

	'For Audit Trail
	Me.SetLogObject("TheatrePreAssessments")

	Return Parameters

End Function

Protected Overrides Function DeleteData() As ArrayList

	Dim where As String = "TheatreNo = '" + Me.TheatreNo + "'
	Dim errorPart As String = "Theatre No: " + Me.TheatreNo

	Me.SetCommand("uspDeleteObject")

	With Parameters
		.Add(New ParameterSQL("ObjectName", "TheatrePreAssessments"))
		.Add(New ParameterSQL("Where", where))
		.Add(New ParameterSQL("ErrorPart", errorPart))
	End With

	'For Audit Trail
	Me.SetLogObject("TheatrePreAssessments")

	Return Parameters

End Function

Public Function GetTheatrePreAssessments(ByVal theatreNo As String) As DataSet

	With Parameters
		.Add(New ParameterSQL("TheatreNo", theatreNo))
	End With

	Return Me.Load("uspGetTheatrePreAssessments", "TheatrePreAssessments", Parameters)

End Function

#End Region



End Class