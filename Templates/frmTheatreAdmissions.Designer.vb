
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTheatreAdmissions : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton
Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton
Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton
Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton
Me.nbxTheatreID = New SyncSoft.Common.Win.Controls.NumericBox
Me.lblTheatreID = New System.Windows.Forms.Label
Me.stbTheatreNo = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblTheatreNo = New System.Windows.Forms.Label
Me.stbVisitNo = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblVisitNo = New System.Windows.Forms.Label
Me.stbProcedureCode = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblProcedureCode = New System.Windows.Forms.Label
Me.dtpAdmissionDateTime = New System.Windows.Forms.DateTimePicker
Me.lblAdmissionDateTime = New System.Windows.Forms.Label
Me.stbOperationTypeID = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblOperationTypeID = New System.Windows.Forms.Label
Me.stbOperationScheduleID = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblOperationScheduleID = New System.Windows.Forms.Label
Me.stbInformedConsentID = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblInformedConsentID = New System.Windows.Forms.Label
Me.stbInformedConsentNotes = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblInformedConsentNotes = New System.Windows.Forms.Label
Me.stbAdmissionStatusID = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblAdmissionStatusID = New System.Windows.Forms.Label
Me.cboStaffNo = New System.Windows.Forms.ComboBox
Me.lblStaffNo = New System.Windows.Forms.Label
Me.dtpRecordDateTime = New System.Windows.Forms.DateTimePicker
Me.lblRecordDateTime = New System.Windows.Forms.Label
Me.SuspendLayout()
'
'nbxTheatreID
'
Me.nbxTheatreID.Location = New System.Drawing.Point(218, 12)
Me.nbxTheatreID.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.nbxTheatreID, "TheatreID")
Me.nbxTheatreID.Name = "nbxTheatreID"
Me.nbxTheatreID.ControlCaption = "Theatre ID"
Me.nbxTheatreID.DataType = SyncSoft.Common.Win.Controls.Number.[Integer]
Me.nbxTheatreID.MustEnterNumeric = True
'
'lblTheatreID
'
Me.lblTheatreID.Location = New System.Drawing.Point(12, 12)
Me.lblTheatreID.Size = New System.Drawing.Size(200, 20)
Me.lblTheatreID.Name = "lblTheatreID"
Me.lblTheatreID.Text = "Theatre ID"
'
'stbTheatreNo
'
Me.stbTheatreNo.Location = New System.Drawing.Point(218, 35)
Me.stbTheatreNo.Size = New System.Drawing.Size(170, 20)
Me.stbTheatreNo.Name = "stbTheatreNo"
'
'lblTheatreNo
'
Me.lblTheatreNo.Location = New System.Drawing.Point(12, 35)
Me.lblTheatreNo.Size = New System.Drawing.Size(200, 20)
Me.lblTheatreNo.Name = "lblTheatreNo"
Me.lblTheatreNo.Text = "Theatre No"
'
'stbVisitNo
'
Me.stbVisitNo.Location = New System.Drawing.Point(218, 58)
Me.stbVisitNo.Size = New System.Drawing.Size(170, 20)
Me.stbVisitNo.Name = "stbVisitNo"
'
'lblVisitNo
'
Me.lblVisitNo.Location = New System.Drawing.Point(12, 58)
Me.lblVisitNo.Size = New System.Drawing.Size(200, 20)
Me.lblVisitNo.Name = "lblVisitNo"
Me.lblVisitNo.Text = "Visit No"
'
'stbProcedureCode
'
Me.stbProcedureCode.Location = New System.Drawing.Point(218, 81)
Me.stbProcedureCode.Size = New System.Drawing.Size(170, 20)
Me.stbProcedureCode.Name = "stbProcedureCode"
'
'lblProcedureCode
'
Me.lblProcedureCode.Location = New System.Drawing.Point(12, 81)
Me.lblProcedureCode.Size = New System.Drawing.Size(200, 20)
Me.lblProcedureCode.Name = "lblProcedureCode"
Me.lblProcedureCode.Text = "Procedure Code"
'
'dtpAdmissionDateTime
'
Me.dtpAdmissionDateTime.Location = New System.Drawing.Point(218, 104)
Me.dtpAdmissionDateTime.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.dtpAdmissionDateTime, "AdmissionDateTime")
Me.dtpAdmissionDateTime.Name = "dtpAdmissionDateTime"
Me.dtpAdmissionDateTime.ShowCheckBox = True
Me.dtpAdmissionDateTime.Checked = False
'
'lblAdmissionDateTime
'
Me.lblAdmissionDateTime.Location = New System.Drawing.Point(12, 104)
Me.lblAdmissionDateTime.Size = New System.Drawing.Size(200, 20)
Me.lblAdmissionDateTime.Name = "lblAdmissionDateTime"
Me.lblAdmissionDateTime.Text = "Admission Date Time"
'
'stbOperationTypeID
'
Me.stbOperationTypeID.Location = New System.Drawing.Point(218, 127)
Me.stbOperationTypeID.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbOperationTypeID, "OperationTypeID")
Me.stbOperationTypeID.Name = "stbOperationTypeID"
'
'lblOperationTypeID
'
Me.lblOperationTypeID.Location = New System.Drawing.Point(12, 127)
Me.lblOperationTypeID.Size = New System.Drawing.Size(200, 20)
Me.lblOperationTypeID.Name = "lblOperationTypeID"
Me.lblOperationTypeID.Text = "Operation Type"
'
'stbOperationScheduleID
'
Me.stbOperationScheduleID.Location = New System.Drawing.Point(218, 150)
Me.stbOperationScheduleID.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbOperationScheduleID, "OperationScheduleID")
Me.stbOperationScheduleID.Name = "stbOperationScheduleID"
'
'lblOperationScheduleID
'
Me.lblOperationScheduleID.Location = New System.Drawing.Point(12, 150)
Me.lblOperationScheduleID.Size = New System.Drawing.Size(200, 20)
Me.lblOperationScheduleID.Name = "lblOperationScheduleID"
Me.lblOperationScheduleID.Text = "Operation Schedule"
'
'stbInformedConsentID
'
Me.stbInformedConsentID.Location = New System.Drawing.Point(218, 173)
Me.stbInformedConsentID.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbInformedConsentID, "InformedConsentID")
Me.stbInformedConsentID.Name = "stbInformedConsentID"
'
'lblInformedConsentID
'
Me.lblInformedConsentID.Location = New System.Drawing.Point(12, 173)
Me.lblInformedConsentID.Size = New System.Drawing.Size(200, 20)
Me.lblInformedConsentID.Name = "lblInformedConsentID"
Me.lblInformedConsentID.Text = "Informed Consent"
'
'stbInformedConsentNotes
'
Me.stbInformedConsentNotes.Location = New System.Drawing.Point(218, 196)
Me.stbInformedConsentNotes.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbInformedConsentNotes, "InformedConsentNotes")
Me.stbInformedConsentNotes.Name = "stbInformedConsentNotes"
'
'lblInformedConsentNotes
'
Me.lblInformedConsentNotes.Location = New System.Drawing.Point(12, 196)
Me.lblInformedConsentNotes.Size = New System.Drawing.Size(200, 20)
Me.lblInformedConsentNotes.Name = "lblInformedConsentNotes"
Me.lblInformedConsentNotes.Text = "Informed Consent Notes"
'
'stbAdmissionStatusID
'
Me.stbAdmissionStatusID.Location = New System.Drawing.Point(218, 219)
Me.stbAdmissionStatusID.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbAdmissionStatusID, "AdmissionStatusID")
Me.stbAdmissionStatusID.Name = "stbAdmissionStatusID"
'
'lblAdmissionStatusID
'
Me.lblAdmissionStatusID.Location = New System.Drawing.Point(12, 219)
Me.lblAdmissionStatusID.Size = New System.Drawing.Size(200, 20)
Me.lblAdmissionStatusID.Name = "lblAdmissionStatusID"
Me.lblAdmissionStatusID.Text = "Admission Status"
'
'cboStaffNo
'
Me.cboStaffNo.Location = New System.Drawing.Point(218, 242)
Me.cboStaffNo.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.cboStaffNo, "StaffNo")
Me.cboStaffNo.Name = "cboStaffNo"
Me.cboStaffNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboStaffNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
'
'lblStaffNo
'
Me.lblStaffNo.Location = New System.Drawing.Point(12, 242)
Me.lblStaffNo.Size = New System.Drawing.Size(200, 20)
Me.lblStaffNo.Name = "lblStaffNo"
Me.lblStaffNo.Text = "Staff No"
'
'dtpRecordDateTime
'
Me.dtpRecordDateTime.Location = New System.Drawing.Point(218, 265)
Me.dtpRecordDateTime.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.dtpRecordDateTime, "RecordDateTime")
Me.dtpRecordDateTime.Name = "dtpRecordDateTime"
Me.dtpRecordDateTime.ShowCheckBox = True
Me.dtpRecordDateTime.Checked = False
'
'lblRecordDateTime
'
Me.lblRecordDateTime.Location = New System.Drawing.Point(12, 265)
Me.lblRecordDateTime.Size = New System.Drawing.Size(200, 20)
Me.lblRecordDateTime.Name = "lblRecordDateTime"
Me.lblRecordDateTime.Text = "RecordDateTime"
'
'fbnSearch
'
Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnSearch.Location = New System.Drawing.Point(17, 295)
Me.fbnSearch.Name = "fbnSearch"
Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
Me.fbnSearch.Text = "S&earch"
Me.fbnSearch.UseVisualStyleBackColor = True
Me.fbnSearch.Visible = False
'
'fbnDelete
'
Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnDelete.Location = New System.Drawing.Point(316, 295)
Me.fbnDelete.Name = "fbnDelete"
Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
Me.fbnDelete.Tag = "TheatreAdmissions"
Me.fbnDelete.Text = "&Delete"
Me.fbnDelete.UseVisualStyleBackColor = False
Me.fbnDelete.Visible = False
'
'ebnSaveUpdate
'
Me.ebnSaveUpdate.ButtonText = SyncSoft.Common.Win.Controls.ButtonCaption.Save
Me.ebnSaveUpdate.DataSource = Nothing
Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.ebnSaveUpdate.Location = New System.Drawing.Point(17, 322)
Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
Me.ebnSaveUpdate.Tag = "TheatreAdmissions"
Me.ebnSaveUpdate.Text = "&Save"
Me.ebnSaveUpdate.UseVisualStyleBackColor = False
'
'fbnClose
'
Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnClose.Location = New System.Drawing.Point(316, 322)
Me.fbnClose.Name = "fbnClose"
Me.fbnClose.Size = New System.Drawing.Size(72, 24)
Me.fbnClose.Text = "&Close"
Me.fbnClose.UseVisualStyleBackColor = False
'
'frmTheatreAdmissions
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.CancelButton = Me.fbnClose
Me.ClientSize = New System.Drawing.Size(415,  372)
Me.Controls.Add(Me.fbnSearch)
Me.Controls.Add(Me.fbnDelete)
Me.Controls.Add(Me.ebnSaveUpdate)
Me.Controls.Add(Me.fbnClose)
Me.Controls.Add(Me.nbxTheatreID)
Me.Controls.Add(Me.lblTheatreID)
Me.Controls.Add(Me.stbTheatreNo)
Me.Controls.Add(Me.lblTheatreNo)
Me.Controls.Add(Me.stbVisitNo)
Me.Controls.Add(Me.lblVisitNo)
Me.Controls.Add(Me.stbProcedureCode)
Me.Controls.Add(Me.lblProcedureCode)
Me.Controls.Add(Me.dtpAdmissionDateTime)
Me.Controls.Add(Me.lblAdmissionDateTime)
Me.Controls.Add(Me.stbOperationTypeID)
Me.Controls.Add(Me.lblOperationTypeID)
Me.Controls.Add(Me.stbOperationScheduleID)
Me.Controls.Add(Me.lblOperationScheduleID)
Me.Controls.Add(Me.stbInformedConsentID)
Me.Controls.Add(Me.lblInformedConsentID)
Me.Controls.Add(Me.stbInformedConsentNotes)
Me.Controls.Add(Me.lblInformedConsentNotes)
Me.Controls.Add(Me.stbAdmissionStatusID)
Me.Controls.Add(Me.lblAdmissionStatusID)
Me.Controls.Add(Me.cboStaffNo)
Me.Controls.Add(Me.lblStaffNo)
Me.Controls.Add(Me.dtpRecordDateTime)
Me.Controls.Add(Me.lblRecordDateTime)
Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
Me.KeyPreview = True
Me.MaximizeBox = False
Me.Name = "frmTheatreAdmissions"
Me.Text = "TheatreAdmissions"
Me.ResumeLayout(False)
Me.PerformLayout()

End Sub

Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents nbxTheatreID As SyncSoft.Common.Win.Controls.NumericBox
Friend WithEvents lblTheatreID As System.Windows.Forms.Label
Friend WithEvents stbTheatreNo As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblTheatreNo As System.Windows.Forms.Label
Friend WithEvents stbVisitNo As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblVisitNo As System.Windows.Forms.Label
Friend WithEvents stbProcedureCode As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblProcedureCode As System.Windows.Forms.Label
Friend WithEvents dtpAdmissionDateTime As System.Windows.Forms.DateTimePicker
Friend WithEvents lblAdmissionDateTime As System.Windows.Forms.Label
Friend WithEvents stbOperationTypeID As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblOperationTypeID As System.Windows.Forms.Label
Friend WithEvents stbOperationScheduleID As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblOperationScheduleID As System.Windows.Forms.Label
Friend WithEvents stbInformedConsentID As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblInformedConsentID As System.Windows.Forms.Label
Friend WithEvents stbInformedConsentNotes As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblInformedConsentNotes As System.Windows.Forms.Label
Friend WithEvents stbAdmissionStatusID As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblAdmissionStatusID As System.Windows.Forms.Label
Friend WithEvents cboStaffNo As System.Windows.Forms.ComboBox
Friend WithEvents lblStaffNo As System.Windows.Forms.Label
Friend WithEvents dtpRecordDateTime As System.Windows.Forms.DateTimePicker
Friend WithEvents lblRecordDateTime As System.Windows.Forms.Label

End Class