
------------------------------------------------------------------------------------------------------
-------------- Create Table: TheatreAppointments ------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'TheatreAppointments')
	drop table TheatreAppointments
go

create table TheatreAppointments
(VisitNo varchar(20)
constraint fkVisitNoTheatreAppointments references Visits (VisitNo),
ProcedureCode varchar(20)
constraint fkProcedureCodeTheatreAppointments references Procedures (ProcedureCode),
constraint pkVisitNoProcedureCode primary key(VisitNo, ProcedureCode),
StartDateTime SmallDateTime,
Duration int,
EndDate SmallDateTime,
StaffNo varchar(10)
constraint fkStaffNoTheatreAppointments references Staff (StaffNo),
AppointmentDes varchar(200),
AppointmentStatusID varchar(10)
constraint fkAppointmentStatusIDTheatreAppointments references LookupData (DataID),
LoginID varchar(20)
constraint fkLoginIDTheatreAppointments references Logins (LoginID),
RecordDateTime SmallDateTime constraint dfRecordDateTimeTheatreAppointments default getDate()
)
go


------------------------------------------------------------------------------------------------------
-------------- TheatreAppointments --------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-------------- Insert TheatreAppointments -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspInsertTheatreAppointments')
	drop proc uspInsertTheatreAppointments
go

create proc uspInsertTheatreAppointments(
@VisitNo varchar(20),
@ProcedureCode varchar(20),
@StartDateTime SmallDateTime,
@Duration int,
@EndDate SmallDateTime,
@StaffNo varchar(10),
@AppointmentDes varchar(200),
@AppointmentStatusID varchar(10),
@LoginID varchar(20),
@RecordDateTime SmallDateTime
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select VisitNo from Visits where VisitNo  = @VisitNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Visit No', @VisitNo, 'Visits')
		return 1
	end

if not exists(select ProcedureCode from Procedures where ProcedureCode  = @ProcedureCode)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Procedure Code', @ProcedureCode, 'Procedures')
		return 1
	end

if exists(select VisitNo from TheatreAppointments where VisitNo = @VisitNo and ProcedureCode = @ProcedureCode)
	begin
		set @ErrorMSG = 'The record with %s: %s and %s: %s, you are trying to enter already exists'
		raiserror(@ErrorMSG, 16, 1, 'Visit No', @VisitNo, 'Procedure Code', @ProcedureCode)
		return 1
	end



begin
insert into TheatreAppointments
(VisitNo, ProcedureCode, StartDateTime, Duration, EndDate, StaffNo, AppointmentDes, AppointmentStatusID, LoginID, RecordDateTime)
values
(@VisitNo, @ProcedureCode, @StartDateTime, @Duration, @EndDate, @StaffNo, @AppointmentDes, @AppointmentStatusID, @LoginID, @RecordDateTime)
return 0
end
go

/******************************************************************************************************
exec uspInsertTheatreAppointments
******************************************************************************************************/
-- select * from TheatreAppointments
-- delete from TheatreAppointments


-------------- Update TheatreAppointments -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspUpdateTheatreAppointments')
	drop proc uspUpdateTheatreAppointments
go

create proc uspUpdateTheatreAppointments(
@VisitNo varchar(20),
@ProcedureCode varchar(20),
@StartDateTime SmallDateTime,
@Duration int,
@EndDate SmallDateTime,
@StaffNo varchar(10),
@AppointmentDes varchar(200),
@AppointmentStatusID varchar(10),
@LoginID varchar(20),
@RecordDateTime SmallDateTime
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select VisitNo from TheatreAppointments where VisitNo = @VisitNo and ProcedureCode = @ProcedureCode)
	begin
		set @ErrorMSG = 'The record with %s: %s and %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Visit No', @VisitNo, 'Procedure Code', @ProcedureCode, 'TheatreAppointments')
		return 1
	end



begin
update TheatreAppointments set
StartDateTime = @StartDateTime, Duration = @Duration, EndDate = @EndDate, StaffNo = @StaffNo, AppointmentDes = @AppointmentDes, AppointmentStatusID = @AppointmentStatusID, LoginID = @LoginID, RecordDateTime = @RecordDateTime
where VisitNo = @VisitNo and ProcedureCode = @ProcedureCode
return 0
end
go

/******************************************************************************************************
exec uspUpdateTheatreAppointments
******************************************************************************************************/
-- select * from TheatreAppointments
-- delete from TheatreAppointments


-------------- Get TheatreAppointments -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspGetTheatreAppointments')
	drop proc uspGetTheatreAppointments
go

create proc uspGetTheatreAppointments(
@VisitNo varchar(20),
@ProcedureCode varchar(20)
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select VisitNo from TheatreAppointments where VisitNo = @VisitNo and ProcedureCode = @ProcedureCode)
	begin
		set @ErrorMSG = 'The record with %s: %s and %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Visit No', @VisitNo, 'Procedure Code', @ProcedureCode, 'TheatreAppointments')
		return 1
	end
else
begin
	select VisitNo, ProcedureCode, StartDateTime, Duration, EndDate, StaffNo, AppointmentDes, AppointmentStatusID, LoginID, RecordDateTime
	from TheatreAppointments

	where VisitNo = @VisitNo and ProcedureCode = @ProcedureCode
return 0
end
go

/******************************************************************************************************
exec uspGetTheatreAppointments
******************************************************************************************************/
-- select * from TheatreAppointments
-- delete from TheatreAppointments


------------------------------------------------------------------------------------------------------
-------------- Update: TheatreAppointments ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrUpdateTheatreAppointments')
	drop trigger utrUpdateTheatreAppointments
go

create trigger utrUpdateTheatreAppointments
on TheatreAppointments
for update
as
declare @ErrorMSG varchar(200)
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Updates ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'U')
if @ObjectName is null return
if @ObjectName <> 'TheatreAppointments' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return



------------------- Key-VisitNo -----------------------------------------------------

begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'VisitNo', Deleted.VisitNo, Inserted.VisitNo
	from Inserted inner join Deleted
	on Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
end

------------------- Key-ProcedureCode -----------------------------------------------------

begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'ProcedureCode', Deleted.ProcedureCode, Inserted.ProcedureCode
	from Inserted inner join Deleted
	on Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
end

-------------------  StartDateTime  -----------------------------------------------------

if update(StartDateTime)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'StartDateTime', dbo.FormatDate(Deleted.StartDateTime), dbo.FormatDate(Inserted.StartDateTime)
	from Inserted inner join Deleted
	on Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.StartDateTime <> Deleted.StartDateTime
end

-------------------  Duration  -----------------------------------------------------

if update(Duration)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'Duration', Deleted.Duration, Inserted.Duration
	from Inserted inner join Deleted
	on Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.Duration <> Deleted.Duration
end

-------------------  EndDate  -----------------------------------------------------

if update(EndDate)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'EndDate', dbo.FormatDate(Deleted.EndDate), dbo.FormatDate(Inserted.EndDate)
	from Inserted inner join Deleted
	on Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.EndDate <> Deleted.EndDate
end

-------------------  StaffNo  -----------------------------------------------------

if update(StaffNo)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'StaffNo', Deleted.StaffNo, Inserted.StaffNo
	from Inserted inner join Deleted
	on Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.StaffNo <> Deleted.StaffNo
end

-------------------  AppointmentDes  -----------------------------------------------------

if update(AppointmentDes)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'AppointmentDes', Deleted.AppointmentDes, Inserted.AppointmentDes
	from Inserted inner join Deleted
	on Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.AppointmentDes <> Deleted.AppointmentDes
end

-------------------  AppointmentStatusID  -----------------------------------------------------

if update(AppointmentStatusID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'AppointmentStatusID', Deleted.AppointmentStatusID, Inserted.AppointmentStatusID
	from Inserted inner join Deleted
	on Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.AppointmentStatusID <> Deleted.AppointmentStatusID
end

-------------------  LoginID  -----------------------------------------------------

if update(LoginID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'LoginID', Deleted.LoginID, Inserted.LoginID
	from Inserted inner join Deleted
	on Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.LoginID <> Deleted.LoginID
end

-------------------  RecordDateTime  -----------------------------------------------------

if update(RecordDateTime)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'RecordDateTime', dbo.FormatDateTime(Deleted.RecordDateTime), dbo.FormatDateTime(Inserted.RecordDateTime)
	from Inserted inner join Deleted
	on Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.RecordDateTime <> Deleted.RecordDateTime
end
go


------------------------------------------------------------------------------------------------------
-------------- Delete: TheatreAppointments ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrDeleteTheatreAppointments')
	drop trigger utrDeleteTheatreAppointments
go

create trigger utrDeleteTheatreAppointments
on TheatreAppointments
for delete
as
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Deletes ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'D')
if @ObjectName is null return
if @ObjectName <> 'TheatreAppointments' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return

-------------------  VisitNo  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'VisitNo', Deleted.VisitNo, null from Deleted

-------------------  ProcedureCode  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'ProcedureCode', Deleted.ProcedureCode, null from Deleted

-------------------  StartDateTime  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'StartDateTime', dbo.FormatDate(Deleted.StartDateTime), null from Deleted

-------------------  Duration  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'Duration', Deleted.Duration, null from Deleted

-------------------  EndDate  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'EndDate', dbo.FormatDate(Deleted.EndDate), null from Deleted

-------------------  StaffNo  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'StaffNo', Deleted.StaffNo, null from Deleted

-------------------  AppointmentDes  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'AppointmentDes', Deleted.AppointmentDes, null from Deleted

-------------------  AppointmentStatusID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'AppointmentStatusID', Deleted.AppointmentStatusID, null from Deleted

-------------------  LoginID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'LoginID', Deleted.LoginID, null from Deleted

-------------------  RecordDateTime  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'RecordDateTime', dbo.FormatDateTime(Deleted.RecordDateTime), null from Deleted
go

