
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTheatrePreAssessments : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton
Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton
Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton
Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton
Me.stbTheatreNo = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblTheatreNo = New System.Windows.Forms.Label
Me.stbLeadAnesthetist = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblLeadAnesthetist = New System.Windows.Forms.Label
Me.stbOtherAnesthetist = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblOtherAnesthetist = New System.Windows.Forms.Label
Me.nbxHeight = New SyncSoft.Common.Win.Controls.NumericBox
Me.lblHeight = New System.Windows.Forms.Label
Me.nbxWeight = New SyncSoft.Common.Win.Controls.NumericBox
Me.lblWeight = New System.Windows.Forms.Label
Me.nbxHeartRate = New SyncSoft.Common.Win.Controls.NumericBox
Me.lblHeartRate = New System.Windows.Forms.Label
Me.nbxOxygenSaturation = New SyncSoft.Common.Win.Controls.NumericBox
Me.lblOxygenSaturation = New System.Windows.Forms.Label
Me.stbHypertensive = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblHypertensive = New System.Windows.Forms.Label
Me.stbDiabetic = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblDiabetic = New System.Windows.Forms.Label
Me.stbHIVStatus = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblHIVStatus = New System.Windows.Forms.Label
Me.stbBathed = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblBathed = New System.Windows.Forms.Label
Me.stbCleanTheatreGown = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblCleanTheatreGown = New System.Windows.Forms.Label
Me.stbSmoker = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblSmoker = New System.Windows.Forms.Label
Me.stbAlcoholUse = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblAlcoholUse = New System.Windows.Forms.Label
Me.stbDrugUse = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblDrugUse = New System.Windows.Forms.Label
Me.stbASAClassID = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblASAClassID = New System.Windows.Forms.Label
Me.dtpLastSolids = New System.Windows.Forms.DateTimePicker
Me.lblLastSolids = New System.Windows.Forms.Label
Me.dtpLastLiquids = New System.Windows.Forms.DateTimePicker
Me.lblLastLiquids = New System.Windows.Forms.Label
Me.stbPastHistory = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblPastHistory = New System.Windows.Forms.Label
Me.stbGeneralCondition = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblGeneralCondition = New System.Windows.Forms.Label
Me.stbPreoperativeInstructions  = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblPreoperativeInstructions  = New System.Windows.Forms.Label
Me.stbAnesthesiaConsentID = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblAnesthesiaConsentID = New System.Windows.Forms.Label
Me.stbAnesthesiaConsentNotes = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblAnesthesiaConsentNotes = New System.Windows.Forms.Label
Me.dtpRecordDateTime = New System.Windows.Forms.DateTimePicker
Me.lblRecordDateTime = New System.Windows.Forms.Label
Me.SuspendLayout()
'
'stbTheatreNo
'
Me.stbTheatreNo.Location = New System.Drawing.Point(218, 12)
Me.stbTheatreNo.Size = New System.Drawing.Size(170, 20)
Me.stbTheatreNo.Name = "stbTheatreNo"
'
'lblTheatreNo
'
Me.lblTheatreNo.Location = New System.Drawing.Point(12, 12)
Me.lblTheatreNo.Size = New System.Drawing.Size(200, 20)
Me.lblTheatreNo.Name = "lblTheatreNo"
Me.lblTheatreNo.Text = "Theatre No"
'
'stbLeadAnesthetist
'
Me.stbLeadAnesthetist.Location = New System.Drawing.Point(218, 35)
Me.stbLeadAnesthetist.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbLeadAnesthetist, "LeadAnesthetist")
Me.stbLeadAnesthetist.Name = "stbLeadAnesthetist"
'
'lblLeadAnesthetist
'
Me.lblLeadAnesthetist.Location = New System.Drawing.Point(12, 35)
Me.lblLeadAnesthetist.Size = New System.Drawing.Size(200, 20)
Me.lblLeadAnesthetist.Name = "lblLeadAnesthetist"
Me.lblLeadAnesthetist.Text = "Lead Anesthetist"
'
'stbOtherAnesthetist
'
Me.stbOtherAnesthetist.Location = New System.Drawing.Point(218, 58)
Me.stbOtherAnesthetist.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbOtherAnesthetist, "OtherAnesthetist")
Me.stbOtherAnesthetist.Name = "stbOtherAnesthetist"
'
'lblOtherAnesthetist
'
Me.lblOtherAnesthetist.Location = New System.Drawing.Point(12, 58)
Me.lblOtherAnesthetist.Size = New System.Drawing.Size(200, 20)
Me.lblOtherAnesthetist.Name = "lblOtherAnesthetist"
Me.lblOtherAnesthetist.Text = "Other Anesthetist"
'
'nbxHeight
'
Me.nbxHeight.Location = New System.Drawing.Point(218, 81)
Me.nbxHeight.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.nbxHeight, "Height")
Me.nbxHeight.Name = "nbxHeight"
Me.nbxHeight.ControlCaption = "Height"
Me.nbxHeight.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
Me.nbxHeight.MustEnterNumeric = True
'
'lblHeight
'
Me.lblHeight.Location = New System.Drawing.Point(12, 81)
Me.lblHeight.Size = New System.Drawing.Size(200, 20)
Me.lblHeight.Name = "lblHeight"
Me.lblHeight.Text = "Height"
'
'nbxWeight
'
Me.nbxWeight.Location = New System.Drawing.Point(218, 104)
Me.nbxWeight.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.nbxWeight, "Weight")
Me.nbxWeight.Name = "nbxWeight"
Me.nbxWeight.ControlCaption = "Weight"
Me.nbxWeight.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
Me.nbxWeight.MustEnterNumeric = True
'
'lblWeight
'
Me.lblWeight.Location = New System.Drawing.Point(12, 104)
Me.lblWeight.Size = New System.Drawing.Size(200, 20)
Me.lblWeight.Name = "lblWeight"
Me.lblWeight.Text = "Weight"
'
'nbxHeartRate
'
Me.nbxHeartRate.Location = New System.Drawing.Point(218, 127)
Me.nbxHeartRate.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.nbxHeartRate, "HeartRate")
Me.nbxHeartRate.Name = "nbxHeartRate"
Me.nbxHeartRate.ControlCaption = "Heart Rate"
Me.nbxHeartRate.DataType = SyncSoft.Common.Win.Controls.Number.[Integer]
Me.nbxHeartRate.MustEnterNumeric = True
'
'lblHeartRate
'
Me.lblHeartRate.Location = New System.Drawing.Point(12, 127)
Me.lblHeartRate.Size = New System.Drawing.Size(200, 20)
Me.lblHeartRate.Name = "lblHeartRate"
Me.lblHeartRate.Text = "Heart Rate"
'
'nbxOxygenSaturation
'
Me.nbxOxygenSaturation.Location = New System.Drawing.Point(218, 150)
Me.nbxOxygenSaturation.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.nbxOxygenSaturation, "OxygenSaturation")
Me.nbxOxygenSaturation.Name = "nbxOxygenSaturation"
Me.nbxOxygenSaturation.ControlCaption = "Oxygen Saturation"
Me.nbxOxygenSaturation.DataType = SyncSoft.Common.Win.Controls.Number.[Single]
Me.nbxOxygenSaturation.MustEnterNumeric = True
'
'lblOxygenSaturation
'
Me.lblOxygenSaturation.Location = New System.Drawing.Point(12, 150)
Me.lblOxygenSaturation.Size = New System.Drawing.Size(200, 20)
Me.lblOxygenSaturation.Name = "lblOxygenSaturation"
Me.lblOxygenSaturation.Text = "Oxygen Saturation"
'
'stbHypertensive
'
Me.stbHypertensive.Location = New System.Drawing.Point(218, 173)
Me.stbHypertensive.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbHypertensive, "Hypertensive")
Me.stbHypertensive.Name = "stbHypertensive"
'
'lblHypertensive
'
Me.lblHypertensive.Location = New System.Drawing.Point(12, 173)
Me.lblHypertensive.Size = New System.Drawing.Size(200, 20)
Me.lblHypertensive.Name = "lblHypertensive"
Me.lblHypertensive.Text = "Hypertensive"
'
'stbDiabetic
'
Me.stbDiabetic.Location = New System.Drawing.Point(218, 196)
Me.stbDiabetic.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbDiabetic, "Diabetic")
Me.stbDiabetic.Name = "stbDiabetic"
'
'lblDiabetic
'
Me.lblDiabetic.Location = New System.Drawing.Point(12, 196)
Me.lblDiabetic.Size = New System.Drawing.Size(200, 20)
Me.lblDiabetic.Name = "lblDiabetic"
Me.lblDiabetic.Text = "Diabetic"
'
'stbHIVStatus
'
Me.stbHIVStatus.Location = New System.Drawing.Point(218, 219)
Me.stbHIVStatus.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbHIVStatus, "HIVStatus")
Me.stbHIVStatus.Name = "stbHIVStatus"
'
'lblHIVStatus
'
Me.lblHIVStatus.Location = New System.Drawing.Point(12, 219)
Me.lblHIVStatus.Size = New System.Drawing.Size(200, 20)
Me.lblHIVStatus.Name = "lblHIVStatus"
Me.lblHIVStatus.Text = "HIV Status"
'
'stbBathed
'
Me.stbBathed.Location = New System.Drawing.Point(218, 242)
Me.stbBathed.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbBathed, "Bathed")
Me.stbBathed.Name = "stbBathed"
'
'lblBathed
'
Me.lblBathed.Location = New System.Drawing.Point(12, 242)
Me.lblBathed.Size = New System.Drawing.Size(200, 20)
Me.lblBathed.Name = "lblBathed"
Me.lblBathed.Text = "Bathed"
'
'stbCleanTheatreGown
'
Me.stbCleanTheatreGown.Location = New System.Drawing.Point(218, 265)
Me.stbCleanTheatreGown.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbCleanTheatreGown, "CleanTheatreGown")
Me.stbCleanTheatreGown.Name = "stbCleanTheatreGown"
'
'lblCleanTheatreGown
'
Me.lblCleanTheatreGown.Location = New System.Drawing.Point(12, 265)
Me.lblCleanTheatreGown.Size = New System.Drawing.Size(200, 20)
Me.lblCleanTheatreGown.Name = "lblCleanTheatreGown"
Me.lblCleanTheatreGown.Text = "Clean Theatre Gown"
'
'stbSmoker
'
Me.stbSmoker.Location = New System.Drawing.Point(218, 288)
Me.stbSmoker.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbSmoker, "Smoker")
Me.stbSmoker.Name = "stbSmoker"
'
'lblSmoker
'
Me.lblSmoker.Location = New System.Drawing.Point(12, 288)
Me.lblSmoker.Size = New System.Drawing.Size(200, 20)
Me.lblSmoker.Name = "lblSmoker"
Me.lblSmoker.Text = "Smoker"
'
'stbAlcoholUse
'
Me.stbAlcoholUse.Location = New System.Drawing.Point(218, 311)
Me.stbAlcoholUse.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbAlcoholUse, "AlcoholUse")
Me.stbAlcoholUse.Name = "stbAlcoholUse"
'
'lblAlcoholUse
'
Me.lblAlcoholUse.Location = New System.Drawing.Point(12, 311)
Me.lblAlcoholUse.Size = New System.Drawing.Size(200, 20)
Me.lblAlcoholUse.Name = "lblAlcoholUse"
Me.lblAlcoholUse.Text = "Alcohol Use"
'
'stbDrugUse
'
Me.stbDrugUse.Location = New System.Drawing.Point(218, 334)
Me.stbDrugUse.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbDrugUse, "DrugUse")
Me.stbDrugUse.Name = "stbDrugUse"
'
'lblDrugUse
'
Me.lblDrugUse.Location = New System.Drawing.Point(12, 334)
Me.lblDrugUse.Size = New System.Drawing.Size(200, 20)
Me.lblDrugUse.Name = "lblDrugUse"
Me.lblDrugUse.Text = "Drug Use"
'
'stbASAClassID
'
Me.stbASAClassID.Location = New System.Drawing.Point(218, 357)
Me.stbASAClassID.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbASAClassID, "ASAClassID")
Me.stbASAClassID.Name = "stbASAClassID"
'
'lblASAClassID
'
Me.lblASAClassID.Location = New System.Drawing.Point(12, 357)
Me.lblASAClassID.Size = New System.Drawing.Size(200, 20)
Me.lblASAClassID.Name = "lblASAClassID"
Me.lblASAClassID.Text = "ASA Class"
'
'dtpLastSolids
'
Me.dtpLastSolids.Location = New System.Drawing.Point(218, 380)
Me.dtpLastSolids.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.dtpLastSolids, "LastSolids")
Me.dtpLastSolids.Name = "dtpLastSolids"
Me.dtpLastSolids.ShowCheckBox = True
Me.dtpLastSolids.Checked = False
'
'lblLastSolids
'
Me.lblLastSolids.Location = New System.Drawing.Point(12, 380)
Me.lblLastSolids.Size = New System.Drawing.Size(200, 20)
Me.lblLastSolids.Name = "lblLastSolids"
Me.lblLastSolids.Text = "Last Solids"
'
'dtpLastLiquids
'
Me.dtpLastLiquids.Location = New System.Drawing.Point(218, 403)
Me.dtpLastLiquids.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.dtpLastLiquids, "LastLiquids")
Me.dtpLastLiquids.Name = "dtpLastLiquids"
Me.dtpLastLiquids.ShowCheckBox = True
Me.dtpLastLiquids.Checked = False
'
'lblLastLiquids
'
Me.lblLastLiquids.Location = New System.Drawing.Point(12, 403)
Me.lblLastLiquids.Size = New System.Drawing.Size(200, 20)
Me.lblLastLiquids.Name = "lblLastLiquids"
Me.lblLastLiquids.Text = "Last Liquids"
'
'stbPastHistory
'
Me.stbPastHistory.Location = New System.Drawing.Point(218, 426)
Me.stbPastHistory.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbPastHistory, "PastHistory")
Me.stbPastHistory.Name = "stbPastHistory"
'
'lblPastHistory
'
Me.lblPastHistory.Location = New System.Drawing.Point(12, 426)
Me.lblPastHistory.Size = New System.Drawing.Size(200, 20)
Me.lblPastHistory.Name = "lblPastHistory"
Me.lblPastHistory.Text = "Past History"
'
'stbGeneralCondition
'
Me.stbGeneralCondition.Location = New System.Drawing.Point(218, 449)
Me.stbGeneralCondition.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbGeneralCondition, "GeneralCondition")
Me.stbGeneralCondition.Name = "stbGeneralCondition"
'
'lblGeneralCondition
'
Me.lblGeneralCondition.Location = New System.Drawing.Point(12, 449)
Me.lblGeneralCondition.Size = New System.Drawing.Size(200, 20)
Me.lblGeneralCondition.Name = "lblGeneralCondition"
Me.lblGeneralCondition.Text = "General Condition"
'
'stbPreoperativeInstructions 
'
Me.stbPreoperativeInstructions .Location = New System.Drawing.Point(218, 472)
Me.stbPreoperativeInstructions .Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbPreoperativeInstructions , "PreoperativeInstructions ")
Me.stbPreoperativeInstructions .Name = "stbPreoperativeInstructions "
'
'lblPreoperativeInstructions 
'
Me.lblPreoperativeInstructions .Location = New System.Drawing.Point(12, 472)
Me.lblPreoperativeInstructions .Size = New System.Drawing.Size(200, 20)
Me.lblPreoperativeInstructions .Name = "lblPreoperativeInstructions "
Me.lblPreoperativeInstructions .Text = "Preoperative Instructions "
'
'stbAnesthesiaConsentID
'
Me.stbAnesthesiaConsentID.Location = New System.Drawing.Point(218, 495)
Me.stbAnesthesiaConsentID.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbAnesthesiaConsentID, "AnesthesiaConsentID")
Me.stbAnesthesiaConsentID.Name = "stbAnesthesiaConsentID"
'
'lblAnesthesiaConsentID
'
Me.lblAnesthesiaConsentID.Location = New System.Drawing.Point(12, 495)
Me.lblAnesthesiaConsentID.Size = New System.Drawing.Size(200, 20)
Me.lblAnesthesiaConsentID.Name = "lblAnesthesiaConsentID"
Me.lblAnesthesiaConsentID.Text = "Anesthesia Consent"
'
'stbAnesthesiaConsentNotes
'
Me.stbAnesthesiaConsentNotes.Location = New System.Drawing.Point(218, 518)
Me.stbAnesthesiaConsentNotes.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbAnesthesiaConsentNotes, "AnesthesiaConsentNotes")
Me.stbAnesthesiaConsentNotes.Name = "stbAnesthesiaConsentNotes"
'
'lblAnesthesiaConsentNotes
'
Me.lblAnesthesiaConsentNotes.Location = New System.Drawing.Point(12, 518)
Me.lblAnesthesiaConsentNotes.Size = New System.Drawing.Size(200, 20)
Me.lblAnesthesiaConsentNotes.Name = "lblAnesthesiaConsentNotes"
Me.lblAnesthesiaConsentNotes.Text = "Anesthesia Consent Notes"
'
'dtpRecordDateTime
'
Me.dtpRecordDateTime.Location = New System.Drawing.Point(218, 541)
Me.dtpRecordDateTime.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.dtpRecordDateTime, "RecordDateTime")
Me.dtpRecordDateTime.Name = "dtpRecordDateTime"
Me.dtpRecordDateTime.ShowCheckBox = True
Me.dtpRecordDateTime.Checked = False
'
'lblRecordDateTime
'
Me.lblRecordDateTime.Location = New System.Drawing.Point(12, 541)
Me.lblRecordDateTime.Size = New System.Drawing.Size(200, 20)
Me.lblRecordDateTime.Name = "lblRecordDateTime"
Me.lblRecordDateTime.Text = "RecordDateTime"
'
'fbnSearch
'
Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnSearch.Location = New System.Drawing.Point(17, 571)
Me.fbnSearch.Name = "fbnSearch"
Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
Me.fbnSearch.Text = "S&earch"
Me.fbnSearch.UseVisualStyleBackColor = True
Me.fbnSearch.Visible = False
'
'fbnDelete
'
Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnDelete.Location = New System.Drawing.Point(316, 571)
Me.fbnDelete.Name = "fbnDelete"
Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
Me.fbnDelete.Tag = "TheatrePreAssessments"
Me.fbnDelete.Text = "&Delete"
Me.fbnDelete.UseVisualStyleBackColor = False
Me.fbnDelete.Visible = False
'
'ebnSaveUpdate
'
Me.ebnSaveUpdate.ButtonText = SyncSoft.Common.Win.Controls.ButtonCaption.Save
Me.ebnSaveUpdate.DataSource = Nothing
Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.ebnSaveUpdate.Location = New System.Drawing.Point(17, 598)
Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
Me.ebnSaveUpdate.Tag = "TheatrePreAssessments"
Me.ebnSaveUpdate.Text = "&Save"
Me.ebnSaveUpdate.UseVisualStyleBackColor = False
'
'fbnClose
'
Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnClose.Location = New System.Drawing.Point(316, 598)
Me.fbnClose.Name = "fbnClose"
Me.fbnClose.Size = New System.Drawing.Size(72, 24)
Me.fbnClose.Text = "&Close"
Me.fbnClose.UseVisualStyleBackColor = False
'
'frmTheatrePreAssessments
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.CancelButton = Me.fbnClose
Me.ClientSize = New System.Drawing.Size(415,  648)
Me.Controls.Add(Me.fbnSearch)
Me.Controls.Add(Me.fbnDelete)
Me.Controls.Add(Me.ebnSaveUpdate)
Me.Controls.Add(Me.fbnClose)
Me.Controls.Add(Me.stbTheatreNo)
Me.Controls.Add(Me.lblTheatreNo)
Me.Controls.Add(Me.stbLeadAnesthetist)
Me.Controls.Add(Me.lblLeadAnesthetist)
Me.Controls.Add(Me.stbOtherAnesthetist)
Me.Controls.Add(Me.lblOtherAnesthetist)
Me.Controls.Add(Me.nbxHeight)
Me.Controls.Add(Me.lblHeight)
Me.Controls.Add(Me.nbxWeight)
Me.Controls.Add(Me.lblWeight)
Me.Controls.Add(Me.nbxHeartRate)
Me.Controls.Add(Me.lblHeartRate)
Me.Controls.Add(Me.nbxOxygenSaturation)
Me.Controls.Add(Me.lblOxygenSaturation)
Me.Controls.Add(Me.stbHypertensive)
Me.Controls.Add(Me.lblHypertensive)
Me.Controls.Add(Me.stbDiabetic)
Me.Controls.Add(Me.lblDiabetic)
Me.Controls.Add(Me.stbHIVStatus)
Me.Controls.Add(Me.lblHIVStatus)
Me.Controls.Add(Me.stbBathed)
Me.Controls.Add(Me.lblBathed)
Me.Controls.Add(Me.stbCleanTheatreGown)
Me.Controls.Add(Me.lblCleanTheatreGown)
Me.Controls.Add(Me.stbSmoker)
Me.Controls.Add(Me.lblSmoker)
Me.Controls.Add(Me.stbAlcoholUse)
Me.Controls.Add(Me.lblAlcoholUse)
Me.Controls.Add(Me.stbDrugUse)
Me.Controls.Add(Me.lblDrugUse)
Me.Controls.Add(Me.stbASAClassID)
Me.Controls.Add(Me.lblASAClassID)
Me.Controls.Add(Me.dtpLastSolids)
Me.Controls.Add(Me.lblLastSolids)
Me.Controls.Add(Me.dtpLastLiquids)
Me.Controls.Add(Me.lblLastLiquids)
Me.Controls.Add(Me.stbPastHistory)
Me.Controls.Add(Me.lblPastHistory)
Me.Controls.Add(Me.stbGeneralCondition)
Me.Controls.Add(Me.lblGeneralCondition)
Me.Controls.Add(Me.stbPreoperativeInstructions )
Me.Controls.Add(Me.lblPreoperativeInstructions )
Me.Controls.Add(Me.stbAnesthesiaConsentID)
Me.Controls.Add(Me.lblAnesthesiaConsentID)
Me.Controls.Add(Me.stbAnesthesiaConsentNotes)
Me.Controls.Add(Me.lblAnesthesiaConsentNotes)
Me.Controls.Add(Me.dtpRecordDateTime)
Me.Controls.Add(Me.lblRecordDateTime)
Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
Me.KeyPreview = True
Me.MaximizeBox = False
Me.Name = "frmTheatrePreAssessments"
Me.Text = "TheatrePreAssessments"
Me.ResumeLayout(False)
Me.PerformLayout()

End Sub

Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents stbTheatreNo As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblTheatreNo As System.Windows.Forms.Label
Friend WithEvents stbLeadAnesthetist As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblLeadAnesthetist As System.Windows.Forms.Label
Friend WithEvents stbOtherAnesthetist As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblOtherAnesthetist As System.Windows.Forms.Label
Friend WithEvents nbxHeight As SyncSoft.Common.Win.Controls.NumericBox
Friend WithEvents lblHeight As System.Windows.Forms.Label
Friend WithEvents nbxWeight As SyncSoft.Common.Win.Controls.NumericBox
Friend WithEvents lblWeight As System.Windows.Forms.Label
Friend WithEvents nbxHeartRate As SyncSoft.Common.Win.Controls.NumericBox
Friend WithEvents lblHeartRate As System.Windows.Forms.Label
Friend WithEvents nbxOxygenSaturation As SyncSoft.Common.Win.Controls.NumericBox
Friend WithEvents lblOxygenSaturation As System.Windows.Forms.Label
Friend WithEvents stbHypertensive As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblHypertensive As System.Windows.Forms.Label
Friend WithEvents stbDiabetic As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblDiabetic As System.Windows.Forms.Label
Friend WithEvents stbHIVStatus As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblHIVStatus As System.Windows.Forms.Label
Friend WithEvents stbBathed As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblBathed As System.Windows.Forms.Label
Friend WithEvents stbCleanTheatreGown As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblCleanTheatreGown As System.Windows.Forms.Label
Friend WithEvents stbSmoker As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblSmoker As System.Windows.Forms.Label
Friend WithEvents stbAlcoholUse As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblAlcoholUse As System.Windows.Forms.Label
Friend WithEvents stbDrugUse As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblDrugUse As System.Windows.Forms.Label
Friend WithEvents stbASAClassID As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblASAClassID As System.Windows.Forms.Label
Friend WithEvents dtpLastSolids As System.Windows.Forms.DateTimePicker
Friend WithEvents lblLastSolids As System.Windows.Forms.Label
Friend WithEvents dtpLastLiquids As System.Windows.Forms.DateTimePicker
Friend WithEvents lblLastLiquids As System.Windows.Forms.Label
Friend WithEvents stbPastHistory As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblPastHistory As System.Windows.Forms.Label
Friend WithEvents stbGeneralCondition As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblGeneralCondition As System.Windows.Forms.Label
Friend WithEvents stbPreoperativeInstructions  As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblPreoperativeInstructions  As System.Windows.Forms.Label
Friend WithEvents stbAnesthesiaConsentID As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblAnesthesiaConsentID As System.Windows.Forms.Label
Friend WithEvents stbAnesthesiaConsentNotes As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblAnesthesiaConsentNotes As System.Windows.Forms.Label
Friend WithEvents dtpRecordDateTime As System.Windows.Forms.DateTimePicker
Friend WithEvents lblRecordDateTime As System.Windows.Forms.Label

End Class