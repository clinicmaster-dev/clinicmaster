
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPreSurgicalCheckList : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton
Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton
Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton
Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton
Me.stbTheatreNo = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblTheatreNo = New System.Windows.Forms.Label
Me.stbPatientIdentified = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblPatientIdentified = New System.Windows.Forms.Label
Me.stbIncisionSiteIdentified = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblIncisionSiteIdentified = New System.Windows.Forms.Label
Me.stbTeamMembersIntroduced = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblTeamMembersIntroduced = New System.Windows.Forms.Label
Me.stbProphylaxisMedicationGiven  = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblProphylaxisMedicationGiven  = New System.Windows.Forms.Label
Me.stbCriticalNonRoutineSteps = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblCriticalNonRoutineSteps = New System.Windows.Forms.Label
Me.stbAnticipatedDuration = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblAnticipatedDuration = New System.Windows.Forms.Label
Me.stbAnticipatedBloodLoss = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblAnticipatedBloodLoss = New System.Windows.Forms.Label
Me.stbPatientConcerns = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblPatientConcerns = New System.Windows.Forms.Label
Me.stbSterilityConfirmed = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblSterilityConfirmed = New System.Windows.Forms.Label
Me.stbOtherConcerns = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblOtherConcerns = New System.Windows.Forms.Label
Me.stbIsEssentialImageDisplayed = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblIsEssentialImageDisplayed = New System.Windows.Forms.Label
Me.dtpRecordDateTime = New System.Windows.Forms.DateTimePicker
Me.lblRecordDateTime = New System.Windows.Forms.Label
Me.SuspendLayout()
'
'stbTheatreNo
'
Me.stbTheatreNo.Location = New System.Drawing.Point(218, 12)
Me.stbTheatreNo.Size = New System.Drawing.Size(170, 20)
Me.stbTheatreNo.Name = "stbTheatreNo"
'
'lblTheatreNo
'
Me.lblTheatreNo.Location = New System.Drawing.Point(12, 12)
Me.lblTheatreNo.Size = New System.Drawing.Size(200, 20)
Me.lblTheatreNo.Name = "lblTheatreNo"
Me.lblTheatreNo.Text = "Theatre No"
'
'stbPatientIdentified
'
Me.stbPatientIdentified.Location = New System.Drawing.Point(218, 35)
Me.stbPatientIdentified.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbPatientIdentified, "PatientIdentified")
Me.stbPatientIdentified.Name = "stbPatientIdentified"
'
'lblPatientIdentified
'
Me.lblPatientIdentified.Location = New System.Drawing.Point(12, 35)
Me.lblPatientIdentified.Size = New System.Drawing.Size(200, 20)
Me.lblPatientIdentified.Name = "lblPatientIdentified"
Me.lblPatientIdentified.Text = "Patient Identified"
'
'stbIncisionSiteIdentified
'
Me.stbIncisionSiteIdentified.Location = New System.Drawing.Point(218, 58)
Me.stbIncisionSiteIdentified.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbIncisionSiteIdentified, "IncisionSiteIdentified")
Me.stbIncisionSiteIdentified.Name = "stbIncisionSiteIdentified"
'
'lblIncisionSiteIdentified
'
Me.lblIncisionSiteIdentified.Location = New System.Drawing.Point(12, 58)
Me.lblIncisionSiteIdentified.Size = New System.Drawing.Size(200, 20)
Me.lblIncisionSiteIdentified.Name = "lblIncisionSiteIdentified"
Me.lblIncisionSiteIdentified.Text = "Incision Site Identified"
'
'stbTeamMembersIntroduced
'
Me.stbTeamMembersIntroduced.Location = New System.Drawing.Point(218, 81)
Me.stbTeamMembersIntroduced.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbTeamMembersIntroduced, "TeamMembersIntroduced")
Me.stbTeamMembersIntroduced.Name = "stbTeamMembersIntroduced"
'
'lblTeamMembersIntroduced
'
Me.lblTeamMembersIntroduced.Location = New System.Drawing.Point(12, 81)
Me.lblTeamMembersIntroduced.Size = New System.Drawing.Size(200, 20)
Me.lblTeamMembersIntroduced.Name = "lblTeamMembersIntroduced"
Me.lblTeamMembersIntroduced.Text = "Team Members Introduced"
'
'stbProphylaxisMedicationGiven 
'
Me.stbProphylaxisMedicationGiven .Location = New System.Drawing.Point(218, 104)
Me.stbProphylaxisMedicationGiven .Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbProphylaxisMedicationGiven , "ProphylaxisMedicationGiven ")
Me.stbProphylaxisMedicationGiven .Name = "stbProphylaxisMedicationGiven "
'
'lblProphylaxisMedicationGiven 
'
Me.lblProphylaxisMedicationGiven .Location = New System.Drawing.Point(12, 104)
Me.lblProphylaxisMedicationGiven .Size = New System.Drawing.Size(200, 20)
Me.lblProphylaxisMedicationGiven .Name = "lblProphylaxisMedicationGiven "
Me.lblProphylaxisMedicationGiven .Text = "Prophylaxis Medication Given "
'
'stbCriticalNonRoutineSteps
'
Me.stbCriticalNonRoutineSteps.Location = New System.Drawing.Point(218, 127)
Me.stbCriticalNonRoutineSteps.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbCriticalNonRoutineSteps, "CriticalNonRoutineSteps")
Me.stbCriticalNonRoutineSteps.Name = "stbCriticalNonRoutineSteps"
'
'lblCriticalNonRoutineSteps
'
Me.lblCriticalNonRoutineSteps.Location = New System.Drawing.Point(12, 127)
Me.lblCriticalNonRoutineSteps.Size = New System.Drawing.Size(200, 20)
Me.lblCriticalNonRoutineSteps.Name = "lblCriticalNonRoutineSteps"
Me.lblCriticalNonRoutineSteps.Text = "Critical Non Routine Steps"
'
'stbAnticipatedDuration
'
Me.stbAnticipatedDuration.Location = New System.Drawing.Point(218, 150)
Me.stbAnticipatedDuration.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbAnticipatedDuration, "AnticipatedDuration")
Me.stbAnticipatedDuration.Name = "stbAnticipatedDuration"
'
'lblAnticipatedDuration
'
Me.lblAnticipatedDuration.Location = New System.Drawing.Point(12, 150)
Me.lblAnticipatedDuration.Size = New System.Drawing.Size(200, 20)
Me.lblAnticipatedDuration.Name = "lblAnticipatedDuration"
Me.lblAnticipatedDuration.Text = "Anticipated Duration"
'
'stbAnticipatedBloodLoss
'
Me.stbAnticipatedBloodLoss.Location = New System.Drawing.Point(218, 173)
Me.stbAnticipatedBloodLoss.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbAnticipatedBloodLoss, "AnticipatedBloodLoss")
Me.stbAnticipatedBloodLoss.Name = "stbAnticipatedBloodLoss"
'
'lblAnticipatedBloodLoss
'
Me.lblAnticipatedBloodLoss.Location = New System.Drawing.Point(12, 173)
Me.lblAnticipatedBloodLoss.Size = New System.Drawing.Size(200, 20)
Me.lblAnticipatedBloodLoss.Name = "lblAnticipatedBloodLoss"
Me.lblAnticipatedBloodLoss.Text = "Anticipated Blood Loss"
'
'stbPatientConcerns
'
Me.stbPatientConcerns.Location = New System.Drawing.Point(218, 196)
Me.stbPatientConcerns.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbPatientConcerns, "PatientConcerns")
Me.stbPatientConcerns.Name = "stbPatientConcerns"
'
'lblPatientConcerns
'
Me.lblPatientConcerns.Location = New System.Drawing.Point(12, 196)
Me.lblPatientConcerns.Size = New System.Drawing.Size(200, 20)
Me.lblPatientConcerns.Name = "lblPatientConcerns"
Me.lblPatientConcerns.Text = "PatientConcerns"
'
'stbSterilityConfirmed
'
Me.stbSterilityConfirmed.Location = New System.Drawing.Point(218, 219)
Me.stbSterilityConfirmed.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbSterilityConfirmed, "SterilityConfirmed")
Me.stbSterilityConfirmed.Name = "stbSterilityConfirmed"
'
'lblSterilityConfirmed
'
Me.lblSterilityConfirmed.Location = New System.Drawing.Point(12, 219)
Me.lblSterilityConfirmed.Size = New System.Drawing.Size(200, 20)
Me.lblSterilityConfirmed.Name = "lblSterilityConfirmed"
Me.lblSterilityConfirmed.Text = "Sterility Confirmed"
'
'stbOtherConcerns
'
Me.stbOtherConcerns.Location = New System.Drawing.Point(218, 242)
Me.stbOtherConcerns.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbOtherConcerns, "OtherConcerns")
Me.stbOtherConcerns.Name = "stbOtherConcerns"
'
'lblOtherConcerns
'
Me.lblOtherConcerns.Location = New System.Drawing.Point(12, 242)
Me.lblOtherConcerns.Size = New System.Drawing.Size(200, 20)
Me.lblOtherConcerns.Name = "lblOtherConcerns"
Me.lblOtherConcerns.Text = "Other Concerns"
'
'stbIsEssentialImageDisplayed
'
Me.stbIsEssentialImageDisplayed.Location = New System.Drawing.Point(218, 265)
Me.stbIsEssentialImageDisplayed.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbIsEssentialImageDisplayed, "IsEssentialImageDisplayed")
Me.stbIsEssentialImageDisplayed.Name = "stbIsEssentialImageDisplayed"
'
'lblIsEssentialImageDisplayed
'
Me.lblIsEssentialImageDisplayed.Location = New System.Drawing.Point(12, 265)
Me.lblIsEssentialImageDisplayed.Size = New System.Drawing.Size(200, 20)
Me.lblIsEssentialImageDisplayed.Name = "lblIsEssentialImageDisplayed"
Me.lblIsEssentialImageDisplayed.Text = "Is Essential Image Displayed"
'
'dtpRecordDateTime
'
Me.dtpRecordDateTime.Location = New System.Drawing.Point(218, 288)
Me.dtpRecordDateTime.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.dtpRecordDateTime, "RecordDateTime")
Me.dtpRecordDateTime.Name = "dtpRecordDateTime"
Me.dtpRecordDateTime.ShowCheckBox = True
Me.dtpRecordDateTime.Checked = False
'
'lblRecordDateTime
'
Me.lblRecordDateTime.Location = New System.Drawing.Point(12, 288)
Me.lblRecordDateTime.Size = New System.Drawing.Size(200, 20)
Me.lblRecordDateTime.Name = "lblRecordDateTime"
Me.lblRecordDateTime.Text = "Record Date Time"
'
'fbnSearch
'
Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnSearch.Location = New System.Drawing.Point(17, 318)
Me.fbnSearch.Name = "fbnSearch"
Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
Me.fbnSearch.Text = "S&earch"
Me.fbnSearch.UseVisualStyleBackColor = True
Me.fbnSearch.Visible = False
'
'fbnDelete
'
Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnDelete.Location = New System.Drawing.Point(316, 318)
Me.fbnDelete.Name = "fbnDelete"
Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
Me.fbnDelete.Tag = "PreSurgicalCheckList"
Me.fbnDelete.Text = "&Delete"
Me.fbnDelete.UseVisualStyleBackColor = False
Me.fbnDelete.Visible = False
'
'ebnSaveUpdate
'
Me.ebnSaveUpdate.ButtonText = SyncSoft.Common.Win.Controls.ButtonCaption.Save
Me.ebnSaveUpdate.DataSource = Nothing
Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.ebnSaveUpdate.Location = New System.Drawing.Point(17, 345)
Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
Me.ebnSaveUpdate.Tag = "PreSurgicalCheckList"
Me.ebnSaveUpdate.Text = "&Save"
Me.ebnSaveUpdate.UseVisualStyleBackColor = False
'
'fbnClose
'
Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnClose.Location = New System.Drawing.Point(316, 345)
Me.fbnClose.Name = "fbnClose"
Me.fbnClose.Size = New System.Drawing.Size(72, 24)
Me.fbnClose.Text = "&Close"
Me.fbnClose.UseVisualStyleBackColor = False
'
'frmPreSurgicalCheckList
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.CancelButton = Me.fbnClose
Me.ClientSize = New System.Drawing.Size(415,  395)
Me.Controls.Add(Me.fbnSearch)
Me.Controls.Add(Me.fbnDelete)
Me.Controls.Add(Me.ebnSaveUpdate)
Me.Controls.Add(Me.fbnClose)
Me.Controls.Add(Me.stbTheatreNo)
Me.Controls.Add(Me.lblTheatreNo)
Me.Controls.Add(Me.stbPatientIdentified)
Me.Controls.Add(Me.lblPatientIdentified)
Me.Controls.Add(Me.stbIncisionSiteIdentified)
Me.Controls.Add(Me.lblIncisionSiteIdentified)
Me.Controls.Add(Me.stbTeamMembersIntroduced)
Me.Controls.Add(Me.lblTeamMembersIntroduced)
Me.Controls.Add(Me.stbProphylaxisMedicationGiven )
Me.Controls.Add(Me.lblProphylaxisMedicationGiven )
Me.Controls.Add(Me.stbCriticalNonRoutineSteps)
Me.Controls.Add(Me.lblCriticalNonRoutineSteps)
Me.Controls.Add(Me.stbAnticipatedDuration)
Me.Controls.Add(Me.lblAnticipatedDuration)
Me.Controls.Add(Me.stbAnticipatedBloodLoss)
Me.Controls.Add(Me.lblAnticipatedBloodLoss)
Me.Controls.Add(Me.stbPatientConcerns)
Me.Controls.Add(Me.lblPatientConcerns)
Me.Controls.Add(Me.stbSterilityConfirmed)
Me.Controls.Add(Me.lblSterilityConfirmed)
Me.Controls.Add(Me.stbOtherConcerns)
Me.Controls.Add(Me.lblOtherConcerns)
Me.Controls.Add(Me.stbIsEssentialImageDisplayed)
Me.Controls.Add(Me.lblIsEssentialImageDisplayed)
Me.Controls.Add(Me.dtpRecordDateTime)
Me.Controls.Add(Me.lblRecordDateTime)
Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
Me.KeyPreview = True
Me.MaximizeBox = False
Me.Name = "frmPreSurgicalCheckList"
Me.Text = "PreSurgicalCheckList"
Me.ResumeLayout(False)
Me.PerformLayout()

End Sub

Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents stbTheatreNo As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblTheatreNo As System.Windows.Forms.Label
Friend WithEvents stbPatientIdentified As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblPatientIdentified As System.Windows.Forms.Label
Friend WithEvents stbIncisionSiteIdentified As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblIncisionSiteIdentified As System.Windows.Forms.Label
Friend WithEvents stbTeamMembersIntroduced As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblTeamMembersIntroduced As System.Windows.Forms.Label
Friend WithEvents stbProphylaxisMedicationGiven  As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblProphylaxisMedicationGiven  As System.Windows.Forms.Label
Friend WithEvents stbCriticalNonRoutineSteps As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblCriticalNonRoutineSteps As System.Windows.Forms.Label
Friend WithEvents stbAnticipatedDuration As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblAnticipatedDuration As System.Windows.Forms.Label
Friend WithEvents stbAnticipatedBloodLoss As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblAnticipatedBloodLoss As System.Windows.Forms.Label
Friend WithEvents stbPatientConcerns As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblPatientConcerns As System.Windows.Forms.Label
Friend WithEvents stbSterilityConfirmed As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblSterilityConfirmed As System.Windows.Forms.Label
Friend WithEvents stbOtherConcerns As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblOtherConcerns As System.Windows.Forms.Label
Friend WithEvents stbIsEssentialImageDisplayed As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblIsEssentialImageDisplayed As System.Windows.Forms.Label
Friend WithEvents dtpRecordDateTime As System.Windows.Forms.DateTimePicker
Friend WithEvents lblRecordDateTime As System.Windows.Forms.Label

End Class