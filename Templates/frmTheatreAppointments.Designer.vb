
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTheatreAppointments : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton
Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton
Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton
Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton
Me.stbVisitNo = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblVisitNo = New System.Windows.Forms.Label
Me.stbProcedureCode = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblProcedureCode = New System.Windows.Forms.Label
Me.dtpStartDateTime = New System.Windows.Forms.DateTimePicker
Me.lblStartDateTime = New System.Windows.Forms.Label
Me.nbxDuration = New SyncSoft.Common.Win.Controls.NumericBox
Me.lblDuration = New System.Windows.Forms.Label
Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
Me.lblEndDate = New System.Windows.Forms.Label
Me.stbStaffNo = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblStaffNo = New System.Windows.Forms.Label
Me.stbAppointmentDes = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblAppointmentDes = New System.Windows.Forms.Label
Me.stbAppointmentStatusID = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblAppointmentStatusID = New System.Windows.Forms.Label
Me.dtpRecordDateTime = New System.Windows.Forms.DateTimePicker
Me.lblRecordDateTime = New System.Windows.Forms.Label
Me.SuspendLayout()
'
'stbVisitNo
'
Me.stbVisitNo.Location = New System.Drawing.Point(218, 12)
Me.stbVisitNo.Size = New System.Drawing.Size(170, 20)
Me.stbVisitNo.Name = "stbVisitNo"
'
'lblVisitNo
'
Me.lblVisitNo.Location = New System.Drawing.Point(12, 12)
Me.lblVisitNo.Size = New System.Drawing.Size(200, 20)
Me.lblVisitNo.Name = "lblVisitNo"
Me.lblVisitNo.Text = "Visit No"
'
'stbProcedureCode
'
Me.stbProcedureCode.Location = New System.Drawing.Point(218, 35)
Me.stbProcedureCode.Size = New System.Drawing.Size(170, 20)
Me.stbProcedureCode.Name = "stbProcedureCode"
'
'lblProcedureCode
'
Me.lblProcedureCode.Location = New System.Drawing.Point(12, 35)
Me.lblProcedureCode.Size = New System.Drawing.Size(200, 20)
Me.lblProcedureCode.Name = "lblProcedureCode"
Me.lblProcedureCode.Text = "Procedure Code"
'
'dtpStartDateTime
'
Me.dtpStartDateTime.Location = New System.Drawing.Point(218, 58)
Me.dtpStartDateTime.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.dtpStartDateTime, "StartDateTime")
Me.dtpStartDateTime.Name = "dtpStartDateTime"
Me.dtpStartDateTime.ShowCheckBox = True
Me.dtpStartDateTime.Checked = False
'
'lblStartDateTime
'
Me.lblStartDateTime.Location = New System.Drawing.Point(12, 58)
Me.lblStartDateTime.Size = New System.Drawing.Size(200, 20)
Me.lblStartDateTime.Name = "lblStartDateTime"
Me.lblStartDateTime.Text = "Start Date"
'
'nbxDuration
'
Me.nbxDuration.Location = New System.Drawing.Point(218, 81)
Me.nbxDuration.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.nbxDuration, "Duration")
Me.nbxDuration.Name = "nbxDuration"
Me.nbxDuration.ControlCaption = "Duration"
Me.nbxDuration.DataType = SyncSoft.Common.Win.Controls.Number.[Integer]
Me.nbxDuration.MustEnterNumeric = True
'
'lblDuration
'
Me.lblDuration.Location = New System.Drawing.Point(12, 81)
Me.lblDuration.Size = New System.Drawing.Size(200, 20)
Me.lblDuration.Name = "lblDuration"
Me.lblDuration.Text = "Duration"
'
'dtpEndDate
'
Me.dtpEndDate.Location = New System.Drawing.Point(218, 104)
Me.dtpEndDate.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.dtpEndDate, "EndDate")
Me.dtpEndDate.Name = "dtpEndDate"
Me.dtpEndDate.ShowCheckBox = True
Me.dtpEndDate.Checked = False
'
'lblEndDate
'
Me.lblEndDate.Location = New System.Drawing.Point(12, 104)
Me.lblEndDate.Size = New System.Drawing.Size(200, 20)
Me.lblEndDate.Name = "lblEndDate"
Me.lblEndDate.Text = "End Date"
'
'stbStaffNo
'
Me.stbStaffNo.Location = New System.Drawing.Point(218, 127)
Me.stbStaffNo.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbStaffNo, "StaffNo")
Me.stbStaffNo.Name = "stbStaffNo"
'
'lblStaffNo
'
Me.lblStaffNo.Location = New System.Drawing.Point(12, 127)
Me.lblStaffNo.Size = New System.Drawing.Size(200, 20)
Me.lblStaffNo.Name = "lblStaffNo"
Me.lblStaffNo.Text = "Staff No"
'
'stbAppointmentDes
'
Me.stbAppointmentDes.Location = New System.Drawing.Point(218, 150)
Me.stbAppointmentDes.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbAppointmentDes, "AppointmentDes")
Me.stbAppointmentDes.Name = "stbAppointmentDes"
'
'lblAppointmentDes
'
Me.lblAppointmentDes.Location = New System.Drawing.Point(12, 150)
Me.lblAppointmentDes.Size = New System.Drawing.Size(200, 20)
Me.lblAppointmentDes.Name = "lblAppointmentDes"
Me.lblAppointmentDes.Text = "Appointment Description"
'
'stbAppointmentStatusID
'
Me.stbAppointmentStatusID.Location = New System.Drawing.Point(218, 173)
Me.stbAppointmentStatusID.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbAppointmentStatusID, "AppointmentStatusID")
Me.stbAppointmentStatusID.Name = "stbAppointmentStatusID"
'
'lblAppointmentStatusID
'
Me.lblAppointmentStatusID.Location = New System.Drawing.Point(12, 173)
Me.lblAppointmentStatusID.Size = New System.Drawing.Size(200, 20)
Me.lblAppointmentStatusID.Name = "lblAppointmentStatusID"
Me.lblAppointmentStatusID.Text = "Appointment Status"
'
'dtpRecordDateTime
'
Me.dtpRecordDateTime.Location = New System.Drawing.Point(218, 196)
Me.dtpRecordDateTime.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.dtpRecordDateTime, "RecordDateTime")
Me.dtpRecordDateTime.Name = "dtpRecordDateTime"
Me.dtpRecordDateTime.ShowCheckBox = True
Me.dtpRecordDateTime.Checked = False
'
'lblRecordDateTime
'
Me.lblRecordDateTime.Location = New System.Drawing.Point(12, 196)
Me.lblRecordDateTime.Size = New System.Drawing.Size(200, 20)
Me.lblRecordDateTime.Name = "lblRecordDateTime"
Me.lblRecordDateTime.Text = "RecordDateTime"
'
'fbnSearch
'
Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnSearch.Location = New System.Drawing.Point(17, 226)
Me.fbnSearch.Name = "fbnSearch"
Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
Me.fbnSearch.Text = "S&earch"
Me.fbnSearch.UseVisualStyleBackColor = True
Me.fbnSearch.Visible = False
'
'fbnDelete
'
Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnDelete.Location = New System.Drawing.Point(316, 226)
Me.fbnDelete.Name = "fbnDelete"
Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
Me.fbnDelete.Tag = "TheatreAppointments"
Me.fbnDelete.Text = "&Delete"
Me.fbnDelete.UseVisualStyleBackColor = False
Me.fbnDelete.Visible = False
'
'ebnSaveUpdate
'
Me.ebnSaveUpdate.ButtonText = SyncSoft.Common.Win.Controls.ButtonCaption.Save
Me.ebnSaveUpdate.DataSource = Nothing
Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.ebnSaveUpdate.Location = New System.Drawing.Point(17, 253)
Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
Me.ebnSaveUpdate.Tag = "TheatreAppointments"
Me.ebnSaveUpdate.Text = "&Save"
Me.ebnSaveUpdate.UseVisualStyleBackColor = False
'
'fbnClose
'
Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnClose.Location = New System.Drawing.Point(316, 253)
Me.fbnClose.Name = "fbnClose"
Me.fbnClose.Size = New System.Drawing.Size(72, 24)
Me.fbnClose.Text = "&Close"
Me.fbnClose.UseVisualStyleBackColor = False
'
'frmTheatreAppointments
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.CancelButton = Me.fbnClose
Me.ClientSize = New System.Drawing.Size(415,  303)
Me.Controls.Add(Me.fbnSearch)
Me.Controls.Add(Me.fbnDelete)
Me.Controls.Add(Me.ebnSaveUpdate)
Me.Controls.Add(Me.fbnClose)
Me.Controls.Add(Me.stbVisitNo)
Me.Controls.Add(Me.lblVisitNo)
Me.Controls.Add(Me.stbProcedureCode)
Me.Controls.Add(Me.lblProcedureCode)
Me.Controls.Add(Me.dtpStartDateTime)
Me.Controls.Add(Me.lblStartDateTime)
Me.Controls.Add(Me.nbxDuration)
Me.Controls.Add(Me.lblDuration)
Me.Controls.Add(Me.dtpEndDate)
Me.Controls.Add(Me.lblEndDate)
Me.Controls.Add(Me.stbStaffNo)
Me.Controls.Add(Me.lblStaffNo)
Me.Controls.Add(Me.stbAppointmentDes)
Me.Controls.Add(Me.lblAppointmentDes)
Me.Controls.Add(Me.stbAppointmentStatusID)
Me.Controls.Add(Me.lblAppointmentStatusID)
Me.Controls.Add(Me.dtpRecordDateTime)
Me.Controls.Add(Me.lblRecordDateTime)
Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
Me.KeyPreview = True
Me.MaximizeBox = False
Me.Name = "frmTheatreAppointments"
Me.Text = "TheatreAppointments"
Me.ResumeLayout(False)
Me.PerformLayout()

End Sub

Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents stbVisitNo As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblVisitNo As System.Windows.Forms.Label
Friend WithEvents stbProcedureCode As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblProcedureCode As System.Windows.Forms.Label
Friend WithEvents dtpStartDateTime As System.Windows.Forms.DateTimePicker
Friend WithEvents lblStartDateTime As System.Windows.Forms.Label
Friend WithEvents nbxDuration As SyncSoft.Common.Win.Controls.NumericBox
Friend WithEvents lblDuration As System.Windows.Forms.Label
Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
Friend WithEvents lblEndDate As System.Windows.Forms.Label
Friend WithEvents stbStaffNo As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblStaffNo As System.Windows.Forms.Label
Friend WithEvents stbAppointmentDes As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblAppointmentDes As System.Windows.Forms.Label
Friend WithEvents stbAppointmentStatusID As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblAppointmentStatusID As System.Windows.Forms.Label
Friend WithEvents dtpRecordDateTime As System.Windows.Forms.DateTimePicker
Friend WithEvents lblRecordDateTime As System.Windows.Forms.Label

End Class