
Option Strict On

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls

Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects

Public Class frmTheatreAdmissions

#Region " Fields "

#End Region

Private Sub frmTheatreAdmissions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

	Try
		Me.Cursor = Cursors.WaitCursor()



	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

Private Sub frmTheatreAdmissions_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
	If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
End Sub

Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
	Me.Close()
End Sub

Private Sub fbnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnDelete.Click

Dim oTheatreAdmissions As New SyncSoft.SQLDb.TheatreAdmissions()

	Try
		Me.Cursor = Cursors.WaitCursor()

		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		If DeleteMessage() = Windows.Forms.DialogResult.No Then Return

		oTheatreAdmissions.TheatreNo = StringEnteredIn(Me.stbTheatreNo, "Theatre No!")
		oTheatreAdmissions.VisitNo = StringEnteredIn(Me.stbVisitNo, "Visit No!")
		oTheatreAdmissions.ProcedureCode = StringEnteredIn(Me.stbProcedureCode, "Procedure Code!")

		DisplayMessage(oTheatreAdmissions.Delete())
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		ResetControlsIn(Me)
		Me.CallOnKeyEdit()

	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

Private Sub fbnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnSearch.Click

Dim theatreNo As String
Dim visitNo As String
Dim procedureCode As String

Dim oTheatreAdmissions As New SyncSoft.SQLDb.TheatreAdmissions()

	Try
		Me.Cursor = Cursors.WaitCursor()

		theatreNo = StringEnteredIn(Me.stbTheatreNo, "Theatre No!")
		visitNo = StringEnteredIn(Me.stbVisitNo, "Visit No!")
		procedureCode = StringEnteredIn(Me.stbProcedureCode, "Procedure Code!")

		Dim dataSource As DataTable = oTheatreAdmissions.GetTheatreAdmissions(theatreNo, visitNo, procedureCode).Tables("TheatreAdmissions")
		Me.DisplayData(dataSource)

	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

Private Sub ebnSaveUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ebnSaveUpdate.Click

Dim oTheatreAdmissions As New SyncSoft.SQLDb.TheatreAdmissions()

	Try
		Me.Cursor = Cursors.WaitCursor()

		With oTheatreAdmissions

		.TheatreID = Me.nbxTheatreID.GetInteger()
		.TheatreNo = StringEnteredIn(Me.stbTheatreNo, "Theatre No!")
		.VisitNo = StringEnteredIn(Me.stbVisitNo, "Visit No!")
		.ProcedureCode = StringEnteredIn(Me.stbProcedureCode, "Procedure Code!")
		.AdmissionDateTime = DateEnteredIn(Me.dtpAdmissionDateTime, "Admission Date Time!")
		.OperationTypeID = StringEnteredIn(Me.stbOperationTypeID, "Operation Type!")
		.OperationScheduleID = StringEnteredIn(Me.stbOperationScheduleID, "Operation Schedule!")
		.InformedConsentID = StringEnteredIn(Me.stbInformedConsentID, "Informed Consent!")
		.InformedConsentNotes = StringEnteredIn(Me.stbInformedConsentNotes, "Informed Consent Notes!")
		.AdmissionStatusID = StringEnteredIn(Me.stbAdmissionStatusID, "Admission Status!")
		.StaffNo = StringValueEnteredIn(Me.cboStaffNo, "Staff No!")
		.LoginID = CurrentUser.LoginID
		
		.RecordDateTime = DateEnteredIn(Me.dtpRecordDateTime, "RecordDateTime!")

		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		ValidateEntriesIn(Me)
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		End With

		Select Case Me.ebnSaveUpdate.ButtonText

		Case ButtonCaption.Save

		oTheatreAdmissions.Save()

		''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		ResetControlsIn(Me)
		''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		Case ButtonCaption.Update

		DisplayMessage(oTheatreAdmissions.Update())
		Me.CallOnKeyEdit()

	End Select

	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

#Region " Edit Methods "

Public Sub Edit()

	Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update
	Me.ebnSaveUpdate.Enabled = False
	Me.fbnDelete.Visible = True
	Me.fbnDelete.Enabled = False
	Me.fbnSearch.Visible = True

	ResetControlsIn(Me)

End Sub

Public Sub Save()

	Me.ebnSaveUpdate.ButtonText = ButtonCaption.Save
	Me.ebnSaveUpdate.Enabled = True
	Me.fbnDelete.Visible = False
	Me.fbnDelete.Enabled = True
	Me.fbnSearch.Visible = False

	ResetControlsIn(Me)

End Sub

Private Sub DisplayData(ByVal dataSource As DataTable)

Try

	Me.ebnSaveUpdate.DataSource = dataSource
	Me.ebnSaveUpdate.LoadData(Me)

	Me.ebnSaveUpdate.Enabled = dataSource.Rows.Count > 0
	Me.fbnDelete.Enabled = dataSource.Rows.Count > 0

	Security.Apply(Me.ebnSaveUpdate, AccessRights.Update)
	Security.Apply(Me.fbnDelete, AccessRights.Delete)

Catch ex As Exception
	Throw ex
End Try

End Sub

Private Sub CallOnKeyEdit()
If Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update Then
	Me.ebnSaveUpdate.Enabled = False
	Me.fbnDelete.Enabled = False
End If
End Sub

#End Region

End Class