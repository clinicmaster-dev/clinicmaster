
Public Class TheatreAppointments : Inherits DBConnect

#Region " Fields "

#End Region

#Region " Auto-Implemented Properties "

Public Property VisitNo As String
Public Property ProcedureCode As String
Public Property StartDateTime As Date
Public Property Duration As Integer
Public Property EndDate As Date
Public Property StaffNo As String
Public Property AppointmentDes As String
Public Property AppointmentStatusID As String
Public Property RecordDateTime As Date

#End Region

#Region " Constructors "

Public Sub New()
	MyBase.New()
End Sub

Public Sub New(ByVal serverName As String, ByVal databaseName As String)
	MyClass.New()
	Me.ServerName = serverName
	Me.DatabaseName = databaseName
End Sub

#End Region

#Region " Methods "

Protected Overrides Function SaveData() As ArrayList

	Me.SetCommand("uspInsertTheatreAppointments")

	With Parameters
		.Add(New ParameterSQL("VisitNo", Me.VisitNo))
		.Add(New ParameterSQL("ProcedureCode", Me.ProcedureCode))
		.Add(New ParameterSQL("StartDateTime", Me.StartDateTime))
		.Add(New ParameterSQL("Duration", Me.Duration))
		.Add(New ParameterSQL("EndDate", Me.EndDate))
		.Add(New ParameterSQL("StaffNo", Me.StaffNo))
		.Add(New ParameterSQL("AppointmentDes", Me.AppointmentDes))
		.Add(New ParameterSQL("AppointmentStatusID", Me.AppointmentStatusID))
		.Add(New ParameterSQL("LoginID", Me.LoginID))
		.Add(New ParameterSQL("RecordDateTime", Me.RecordDateTime))
	End With

	Return Parameters

End Function

Protected Overrides Function UpdateData() As ArrayList

	Me.SetCommand("uspUpdateTheatreAppointments")

	With Parameters
		.Add(New ParameterSQL("VisitNo", Me.VisitNo))
		.Add(New ParameterSQL("ProcedureCode", Me.ProcedureCode))
		.Add(New ParameterSQL("StartDateTime", Me.StartDateTime))
		.Add(New ParameterSQL("Duration", Me.Duration))
		.Add(New ParameterSQL("EndDate", Me.EndDate))
		.Add(New ParameterSQL("StaffNo", Me.StaffNo))
		.Add(New ParameterSQL("AppointmentDes", Me.AppointmentDes))
		.Add(New ParameterSQL("AppointmentStatusID", Me.AppointmentStatusID))
		.Add(New ParameterSQL("LoginID", Me.LoginID))
		.Add(New ParameterSQL("RecordDateTime", Me.RecordDateTime))
	End With

	'For Audit Trail
	Me.SetLogObject("TheatreAppointments")

	Return Parameters

End Function

Protected Overrides Function DeleteData() As ArrayList

	Dim where As String = "VisitNo = '" + Me.VisitNo + "' and ProcedureCode = '" + Me.ProcedureCode + "'
	Dim errorPart As String = "Visit No: " + Me.VisitNo + " and Procedure Code: " + Me.ProcedureCode

	Me.SetCommand("uspDeleteObject")

	With Parameters
		.Add(New ParameterSQL("ObjectName", "TheatreAppointments"))
		.Add(New ParameterSQL("Where", where))
		.Add(New ParameterSQL("ErrorPart", errorPart))
	End With

	'For Audit Trail
	Me.SetLogObject("TheatreAppointments")

	Return Parameters

End Function

Public Function GetTheatreAppointments(ByVal visitNo As String, ByVal procedureCode As String) As DataSet

	With Parameters
		.Add(New ParameterSQL("VisitNo", visitNo))
		.Add(New ParameterSQL("ProcedureCode", procedureCode))
	End With

	Return Me.Load("uspGetTheatreAppointments", "TheatreAppointments", Parameters)

End Function

#End Region



End Class