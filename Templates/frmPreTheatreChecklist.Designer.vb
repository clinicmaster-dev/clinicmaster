
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPreTheatreChecklist : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton
Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton
Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton
Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton
Me.stbTheatreNo = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblTheatreNo = New System.Windows.Forms.Label
Me.stbAnesthetist = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblAnesthetist = New System.Windows.Forms.Label
Me.stbConfirmedPatientIdentity = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblConfirmedPatientIdentity = New System.Windows.Forms.Label
Me.stbPatientConsented = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblPatientConsented = New System.Windows.Forms.Label
Me.stbSiteMarked = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblSiteMarked = New System.Windows.Forms.Label
Me.stbAnesthesiaMachineChecked = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblAnesthesiaMachineChecked = New System.Windows.Forms.Label
Me.stbAnesthesiaMedicationChecked = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblAnesthesiaMedicationChecked = New System.Windows.Forms.Label
Me.stbPulseOximeterOnFunctional = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblPulseOximeterOnFunctional = New System.Windows.Forms.Label
Me.stbKnownAllergies = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblKnownAllergies = New System.Windows.Forms.Label
Me.stbAirWayAspirationRisk = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblAirWayAspirationRisk = New System.Windows.Forms.Label
Me.stbHighBloodLossRisk = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblHighBloodLossRisk = New System.Windows.Forms.Label
Me.dtpRecordDateTime = New System.Windows.Forms.DateTimePicker
Me.lblRecordDateTime = New System.Windows.Forms.Label
Me.SuspendLayout()
'
'stbTheatreNo
'
Me.stbTheatreNo.Location = New System.Drawing.Point(218, 12)
Me.stbTheatreNo.Size = New System.Drawing.Size(170, 20)
Me.stbTheatreNo.Name = "stbTheatreNo"
'
'lblTheatreNo
'
Me.lblTheatreNo.Location = New System.Drawing.Point(12, 12)
Me.lblTheatreNo.Size = New System.Drawing.Size(200, 20)
Me.lblTheatreNo.Name = "lblTheatreNo"
Me.lblTheatreNo.Text = "Theatre No"
'
'stbAnesthetist
'
Me.stbAnesthetist.Location = New System.Drawing.Point(218, 35)
Me.stbAnesthetist.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbAnesthetist, "Anesthetist")
Me.stbAnesthetist.Name = "stbAnesthetist"
'
'lblAnesthetist
'
Me.lblAnesthetist.Location = New System.Drawing.Point(12, 35)
Me.lblAnesthetist.Size = New System.Drawing.Size(200, 20)
Me.lblAnesthetist.Name = "lblAnesthetist"
Me.lblAnesthetist.Text = "Anesthetist"
'
'stbConfirmedPatientIdentity
'
Me.stbConfirmedPatientIdentity.Location = New System.Drawing.Point(218, 58)
Me.stbConfirmedPatientIdentity.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbConfirmedPatientIdentity, "ConfirmedPatientIdentity")
Me.stbConfirmedPatientIdentity.Name = "stbConfirmedPatientIdentity"
'
'lblConfirmedPatientIdentity
'
Me.lblConfirmedPatientIdentity.Location = New System.Drawing.Point(12, 58)
Me.lblConfirmedPatientIdentity.Size = New System.Drawing.Size(200, 20)
Me.lblConfirmedPatientIdentity.Name = "lblConfirmedPatientIdentity"
Me.lblConfirmedPatientIdentity.Text = "Confirmed Patient Identity"
'
'stbPatientConsented
'
Me.stbPatientConsented.Location = New System.Drawing.Point(218, 81)
Me.stbPatientConsented.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbPatientConsented, "PatientConsented")
Me.stbPatientConsented.Name = "stbPatientConsented"
'
'lblPatientConsented
'
Me.lblPatientConsented.Location = New System.Drawing.Point(12, 81)
Me.lblPatientConsented.Size = New System.Drawing.Size(200, 20)
Me.lblPatientConsented.Name = "lblPatientConsented"
Me.lblPatientConsented.Text = "Patient Consented"
'
'stbSiteMarked
'
Me.stbSiteMarked.Location = New System.Drawing.Point(218, 104)
Me.stbSiteMarked.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbSiteMarked, "SiteMarked")
Me.stbSiteMarked.Name = "stbSiteMarked"
'
'lblSiteMarked
'
Me.lblSiteMarked.Location = New System.Drawing.Point(12, 104)
Me.lblSiteMarked.Size = New System.Drawing.Size(200, 20)
Me.lblSiteMarked.Name = "lblSiteMarked"
Me.lblSiteMarked.Text = "Site Marked"
'
'stbAnesthesiaMachineChecked
'
Me.stbAnesthesiaMachineChecked.Location = New System.Drawing.Point(218, 127)
Me.stbAnesthesiaMachineChecked.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbAnesthesiaMachineChecked, "AnesthesiaMachineChecked")
Me.stbAnesthesiaMachineChecked.Name = "stbAnesthesiaMachineChecked"
'
'lblAnesthesiaMachineChecked
'
Me.lblAnesthesiaMachineChecked.Location = New System.Drawing.Point(12, 127)
Me.lblAnesthesiaMachineChecked.Size = New System.Drawing.Size(200, 20)
Me.lblAnesthesiaMachineChecked.Name = "lblAnesthesiaMachineChecked"
Me.lblAnesthesiaMachineChecked.Text = "Anesthesia Machine Checked"
'
'stbAnesthesiaMedicationChecked
'
Me.stbAnesthesiaMedicationChecked.Location = New System.Drawing.Point(218, 150)
Me.stbAnesthesiaMedicationChecked.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbAnesthesiaMedicationChecked, "AnesthesiaMedicationChecked")
Me.stbAnesthesiaMedicationChecked.Name = "stbAnesthesiaMedicationChecked"
'
'lblAnesthesiaMedicationChecked
'
Me.lblAnesthesiaMedicationChecked.Location = New System.Drawing.Point(12, 150)
Me.lblAnesthesiaMedicationChecked.Size = New System.Drawing.Size(200, 20)
Me.lblAnesthesiaMedicationChecked.Name = "lblAnesthesiaMedicationChecked"
Me.lblAnesthesiaMedicationChecked.Text = "Anesthesia Medication Checked"
'
'stbPulseOximeterOnFunctional
'
Me.stbPulseOximeterOnFunctional.Location = New System.Drawing.Point(218, 173)
Me.stbPulseOximeterOnFunctional.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbPulseOximeterOnFunctional, "PulseOximeterOnFunctional")
Me.stbPulseOximeterOnFunctional.Name = "stbPulseOximeterOnFunctional"
'
'lblPulseOximeterOnFunctional
'
Me.lblPulseOximeterOnFunctional.Location = New System.Drawing.Point(12, 173)
Me.lblPulseOximeterOnFunctional.Size = New System.Drawing.Size(200, 20)
Me.lblPulseOximeterOnFunctional.Name = "lblPulseOximeterOnFunctional"
Me.lblPulseOximeterOnFunctional.Text = "Pulse Oximeter On Functional"
'
'stbKnownAllergies
'
Me.stbKnownAllergies.Location = New System.Drawing.Point(218, 196)
Me.stbKnownAllergies.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbKnownAllergies, "KnownAllergies")
Me.stbKnownAllergies.Name = "stbKnownAllergies"
'
'lblKnownAllergies
'
Me.lblKnownAllergies.Location = New System.Drawing.Point(12, 196)
Me.lblKnownAllergies.Size = New System.Drawing.Size(200, 20)
Me.lblKnownAllergies.Name = "lblKnownAllergies"
Me.lblKnownAllergies.Text = "Known Allergies"
'
'stbAirWayAspirationRisk
'
Me.stbAirWayAspirationRisk.Location = New System.Drawing.Point(218, 219)
Me.stbAirWayAspirationRisk.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbAirWayAspirationRisk, "AirWayAspirationRisk")
Me.stbAirWayAspirationRisk.Name = "stbAirWayAspirationRisk"
'
'lblAirWayAspirationRisk
'
Me.lblAirWayAspirationRisk.Location = New System.Drawing.Point(12, 219)
Me.lblAirWayAspirationRisk.Size = New System.Drawing.Size(200, 20)
Me.lblAirWayAspirationRisk.Name = "lblAirWayAspirationRisk"
Me.lblAirWayAspirationRisk.Text = "Air Way Aspiration Risk"
'
'stbHighBloodLossRisk
'
Me.stbHighBloodLossRisk.Location = New System.Drawing.Point(218, 242)
Me.stbHighBloodLossRisk.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbHighBloodLossRisk, "HighBloodLossRisk")
Me.stbHighBloodLossRisk.Name = "stbHighBloodLossRisk"
'
'lblHighBloodLossRisk
'
Me.lblHighBloodLossRisk.Location = New System.Drawing.Point(12, 242)
Me.lblHighBloodLossRisk.Size = New System.Drawing.Size(200, 20)
Me.lblHighBloodLossRisk.Name = "lblHighBloodLossRisk"
Me.lblHighBloodLossRisk.Text = "High Blood Loss Risk"
'
'dtpRecordDateTime
'
Me.dtpRecordDateTime.Location = New System.Drawing.Point(218, 265)
Me.dtpRecordDateTime.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.dtpRecordDateTime, "RecordDateTime")
Me.dtpRecordDateTime.Name = "dtpRecordDateTime"
Me.dtpRecordDateTime.ShowCheckBox = True
Me.dtpRecordDateTime.Checked = False
'
'lblRecordDateTime
'
Me.lblRecordDateTime.Location = New System.Drawing.Point(12, 265)
Me.lblRecordDateTime.Size = New System.Drawing.Size(200, 20)
Me.lblRecordDateTime.Name = "lblRecordDateTime"
Me.lblRecordDateTime.Text = "Record Date Time"
'
'fbnSearch
'
Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnSearch.Location = New System.Drawing.Point(17, 295)
Me.fbnSearch.Name = "fbnSearch"
Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
Me.fbnSearch.Text = "S&earch"
Me.fbnSearch.UseVisualStyleBackColor = True
Me.fbnSearch.Visible = False
'
'fbnDelete
'
Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnDelete.Location = New System.Drawing.Point(316, 295)
Me.fbnDelete.Name = "fbnDelete"
Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
Me.fbnDelete.Tag = "PreTheatreChecklist"
Me.fbnDelete.Text = "&Delete"
Me.fbnDelete.UseVisualStyleBackColor = False
Me.fbnDelete.Visible = False
'
'ebnSaveUpdate
'
Me.ebnSaveUpdate.ButtonText = SyncSoft.Common.Win.Controls.ButtonCaption.Save
Me.ebnSaveUpdate.DataSource = Nothing
Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.ebnSaveUpdate.Location = New System.Drawing.Point(17, 322)
Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
Me.ebnSaveUpdate.Tag = "PreTheatreChecklist"
Me.ebnSaveUpdate.Text = "&Save"
Me.ebnSaveUpdate.UseVisualStyleBackColor = False
'
'fbnClose
'
Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnClose.Location = New System.Drawing.Point(316, 322)
Me.fbnClose.Name = "fbnClose"
Me.fbnClose.Size = New System.Drawing.Size(72, 24)
Me.fbnClose.Text = "&Close"
Me.fbnClose.UseVisualStyleBackColor = False
'
'frmPreTheatreChecklist
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.CancelButton = Me.fbnClose
Me.ClientSize = New System.Drawing.Size(415,  372)
Me.Controls.Add(Me.fbnSearch)
Me.Controls.Add(Me.fbnDelete)
Me.Controls.Add(Me.ebnSaveUpdate)
Me.Controls.Add(Me.fbnClose)
Me.Controls.Add(Me.stbTheatreNo)
Me.Controls.Add(Me.lblTheatreNo)
Me.Controls.Add(Me.stbAnesthetist)
Me.Controls.Add(Me.lblAnesthetist)
Me.Controls.Add(Me.stbConfirmedPatientIdentity)
Me.Controls.Add(Me.lblConfirmedPatientIdentity)
Me.Controls.Add(Me.stbPatientConsented)
Me.Controls.Add(Me.lblPatientConsented)
Me.Controls.Add(Me.stbSiteMarked)
Me.Controls.Add(Me.lblSiteMarked)
Me.Controls.Add(Me.stbAnesthesiaMachineChecked)
Me.Controls.Add(Me.lblAnesthesiaMachineChecked)
Me.Controls.Add(Me.stbAnesthesiaMedicationChecked)
Me.Controls.Add(Me.lblAnesthesiaMedicationChecked)
Me.Controls.Add(Me.stbPulseOximeterOnFunctional)
Me.Controls.Add(Me.lblPulseOximeterOnFunctional)
Me.Controls.Add(Me.stbKnownAllergies)
Me.Controls.Add(Me.lblKnownAllergies)
Me.Controls.Add(Me.stbAirWayAspirationRisk)
Me.Controls.Add(Me.lblAirWayAspirationRisk)
Me.Controls.Add(Me.stbHighBloodLossRisk)
Me.Controls.Add(Me.lblHighBloodLossRisk)
Me.Controls.Add(Me.dtpRecordDateTime)
Me.Controls.Add(Me.lblRecordDateTime)
Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
Me.KeyPreview = True
Me.MaximizeBox = False
Me.Name = "frmPreTheatreChecklist"
Me.Text = "PreTheatreChecklist"
Me.ResumeLayout(False)
Me.PerformLayout()

End Sub

Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents stbTheatreNo As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblTheatreNo As System.Windows.Forms.Label
Friend WithEvents stbAnesthetist As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblAnesthetist As System.Windows.Forms.Label
Friend WithEvents stbConfirmedPatientIdentity As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblConfirmedPatientIdentity As System.Windows.Forms.Label
Friend WithEvents stbPatientConsented As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblPatientConsented As System.Windows.Forms.Label
Friend WithEvents stbSiteMarked As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblSiteMarked As System.Windows.Forms.Label
Friend WithEvents stbAnesthesiaMachineChecked As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblAnesthesiaMachineChecked As System.Windows.Forms.Label
Friend WithEvents stbAnesthesiaMedicationChecked As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblAnesthesiaMedicationChecked As System.Windows.Forms.Label
Friend WithEvents stbPulseOximeterOnFunctional As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblPulseOximeterOnFunctional As System.Windows.Forms.Label
Friend WithEvents stbKnownAllergies As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblKnownAllergies As System.Windows.Forms.Label
Friend WithEvents stbAirWayAspirationRisk As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblAirWayAspirationRisk As System.Windows.Forms.Label
Friend WithEvents stbHighBloodLossRisk As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblHighBloodLossRisk As System.Windows.Forms.Label
Friend WithEvents dtpRecordDateTime As System.Windows.Forms.DateTimePicker
Friend WithEvents lblRecordDateTime As System.Windows.Forms.Label

End Class