
------------------------------------------------------------------------------------------------------
-------------- Create Table: TheatreAdmissionsIPD ------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'TheatreAdmissionsIPD')
	drop table TheatreAdmissionsIPD
go

create table TheatreAdmissionsIPD
(TheatreNo varchar(20)
constraint fkTheatreNoTheatreAdmissionsIPD references TheatreAdmissions (TheatreNo),
RoundNo varchar(20)
constraint fkRoundNoTheatreAdmissionsIPD references IPDDoctor (RoundNo),
constraint pkTheatreNoRoundNo primary key(TheatreNo, RoundNo)
)
go


------------------------------------------------------------------------------------------------------
-------------- TheatreAdmissionsIPD --------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-------------- Insert TheatreAdmissionsIPD -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspInsertTheatreAdmissionsIPD')
	drop proc uspInsertTheatreAdmissionsIPD
go

create proc uspInsertTheatreAdmissionsIPD(
@TheatreNo varchar(20),
@RoundNo varchar(20)
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select TheatreNo from TheatreAdmissions where TheatreNo  = @TheatreNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'TheatreAdmissions')
		return 1
	end

if not exists(select RoundNo from IPDDoctor where RoundNo  = @RoundNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Round No', @RoundNo, 'IPDDoctor')
		return 1
	end

if exists(select TheatreNo from TheatreAdmissionsIPD where TheatreNo = @TheatreNo and RoundNo = @RoundNo)
	begin
		set @ErrorMSG = 'The record with %s: %s and %s: %s, you are trying to enter already exists'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'Round No', @RoundNo)
		return 1
	end



begin
insert into TheatreAdmissionsIPD
(TheatreNo, RoundNo)
values
(@TheatreNo, @RoundNo)
return 0
end
go

/******************************************************************************************************
exec uspInsertTheatreAdmissionsIPD
******************************************************************************************************/
-- select * from TheatreAdmissionsIPD
-- delete from TheatreAdmissionsIPD


-------------- Update TheatreAdmissionsIPD -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspUpdateTheatreAdmissionsIPD')
	drop proc uspUpdateTheatreAdmissionsIPD
go

create proc uspUpdateTheatreAdmissionsIPD(
@TheatreNo varchar(20),
@RoundNo varchar(20)
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select TheatreNo from TheatreAdmissionsIPD where TheatreNo = @TheatreNo and RoundNo = @RoundNo)
	begin
		set @ErrorMSG = 'The record with %s: %s and %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'Round No', @RoundNo, 'TheatreAdmissionsIPD')
		return 1
	end



begin
update TheatreAdmissionsIPD set

where TheatreNo = @TheatreNo and RoundNo = @RoundNo
return 0
end
go

/******************************************************************************************************
exec uspUpdateTheatreAdmissionsIPD
******************************************************************************************************/
-- select * from TheatreAdmissionsIPD
-- delete from TheatreAdmissionsIPD


-------------- Get TheatreAdmissionsIPD -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspGetTheatreAdmissionsIPD')
	drop proc uspGetTheatreAdmissionsIPD
go

create proc uspGetTheatreAdmissionsIPD(
@TheatreNo varchar(20),
@RoundNo varchar(20)
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select TheatreNo from TheatreAdmissionsIPD where TheatreNo = @TheatreNo and RoundNo = @RoundNo)
	begin
		set @ErrorMSG = 'The record with %s: %s and %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'Round No', @RoundNo, 'TheatreAdmissionsIPD')
		return 1
	end
else
begin
	select TheatreNo, RoundNo
	from TheatreAdmissionsIPD

	where TheatreNo = @TheatreNo and RoundNo = @RoundNo
return 0
end
go

/******************************************************************************************************
exec uspGetTheatreAdmissionsIPD
******************************************************************************************************/
-- select * from TheatreAdmissionsIPD
-- delete from TheatreAdmissionsIPD


------------------------------------------------------------------------------------------------------
-------------- Update: TheatreAdmissionsIPD ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrUpdateTheatreAdmissionsIPD')
	drop trigger utrUpdateTheatreAdmissionsIPD
go

create trigger utrUpdateTheatreAdmissionsIPD
on TheatreAdmissionsIPD
for update
as
declare @ErrorMSG varchar(200)
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Updates ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'U')
if @ObjectName is null return
if @ObjectName <> 'TheatreAdmissionsIPD' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return



------------------- Key-TheatreNo -----------------------------------------------------

begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'TheatreNo', Deleted.TheatreNo, Inserted.TheatreNo
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.RoundNo = Deleted.RoundNo
end

------------------- Key-RoundNo -----------------------------------------------------

begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'RoundNo', Deleted.RoundNo, Inserted.RoundNo
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.RoundNo = Deleted.RoundNo
end


go


------------------------------------------------------------------------------------------------------
-------------- Delete: TheatreAdmissionsIPD ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrDeleteTheatreAdmissionsIPD')
	drop trigger utrDeleteTheatreAdmissionsIPD
go

create trigger utrDeleteTheatreAdmissionsIPD
on TheatreAdmissionsIPD
for delete
as
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Deletes ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'D')
if @ObjectName is null return
if @ObjectName <> 'TheatreAdmissionsIPD' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return

-------------------  TheatreNo  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'TheatreNo', Deleted.TheatreNo, null from Deleted

-------------------  RoundNo  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'RoundNo', Deleted.RoundNo, null from Deleted
go

