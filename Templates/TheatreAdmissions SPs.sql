
------------------------------------------------------------------------------------------------------
-------------- Create Table: TheatreAdmissions ------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'TheatreAdmissions')
	drop table TheatreAdmissions
go

create table TheatreAdmissions
(TheatreID int,
TheatreNo varchar(20),
VisitNo varchar(20)
constraint fkVisitNoTheatreAdmissions references Visits (VisitNo),
ProcedureCode varchar(20)
constraint fkProcedureCodeTheatreAdmissions references Procedures (ProcedureCode),
constraint pkTheatreNoVisitNoProcedureCode primary key(TheatreNo, VisitNo, ProcedureCode),
AdmissionDateTime SmallDateTime,
OperationTypeID varchar(10)
constraint fkOperationTypeIDTheatreAdmissions references LookupData (DataID),
OperationScheduleID varchar(10)
constraint fkOperationScheduleIDTheatreAdmissions references LookupData (DataID),
InformedConsentID varchar(10)
constraint fkInformedConsentIDTheatreAdmissions references LookupData (DataID),
InformedConsentNotes varchar(200),
AdmissionStatusID varchar(10)
constraint fkAdmissionStatusIDTheatreAdmissions references LookupData (DataID),
StaffNo varchar(20)
constraint fkStaffNoTheatreAdmissions references Staff (StaffNo),
LoginID varchar(20)
constraint fkLoginIDTheatreAdmissions references Logins (LoginID),
ClientMachine varchar(40) constraint dfClientMachineTheatreAdmissions default host_name(),
RecordDateTime SmallDateTime constraint dfRecordDateTimeTheatreAdmissions default getDate()
)
go


------------------------------------------------------------------------------------------------------
-------------- TheatreAdmissions --------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-------------- Insert TheatreAdmissions -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspInsertTheatreAdmissions')
	drop proc uspInsertTheatreAdmissions
go

create proc uspInsertTheatreAdmissions(
@TheatreID int,
@TheatreNo varchar(20),
@VisitNo varchar(20),
@ProcedureCode varchar(20),
@AdmissionDateTime SmallDateTime,
@OperationTypeID varchar(10),
@OperationScheduleID varchar(10),
@InformedConsentID varchar(10),
@InformedConsentNotes varchar(200),
@AdmissionStatusID varchar(10),
@StaffNo varchar(20),
@LoginID varchar(20),
@ClientMachine varchar(40),
@RecordDateTime SmallDateTime
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select VisitNo from Visits where VisitNo  = @VisitNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Visit No', @VisitNo, 'Visits')
		return 1
	end

if not exists(select ProcedureCode from Procedures where ProcedureCode  = @ProcedureCode)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Procedure Code', @ProcedureCode, 'Procedures')
		return 1
	end

if exists(select TheatreNo from TheatreAdmissions where TheatreNo = @TheatreNo and VisitNo = @VisitNo and ProcedureCode = @ProcedureCode)
	begin
		set @ErrorMSG = 'The record with %s: %s and %s: %s and %s: %s, you are trying to enter already exists'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'Visit No', @VisitNo, 'Procedure Code', @ProcedureCode)
		return 1
	end



begin
insert into TheatreAdmissions
(TheatreID, TheatreNo, VisitNo, ProcedureCode, AdmissionDateTime, OperationTypeID, OperationScheduleID, InformedConsentID, InformedConsentNotes, AdmissionStatusID, StaffNo, LoginID, ClientMachine, RecordDateTime)
values
(@TheatreID, @TheatreNo, @VisitNo, @ProcedureCode, @AdmissionDateTime, @OperationTypeID, @OperationScheduleID, @InformedConsentID, @InformedConsentNotes, @AdmissionStatusID, @StaffNo, @LoginID, @ClientMachine, @RecordDateTime)
return 0
end
go

/******************************************************************************************************
exec uspInsertTheatreAdmissions
******************************************************************************************************/
-- select * from TheatreAdmissions
-- delete from TheatreAdmissions


-------------- Update TheatreAdmissions -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspUpdateTheatreAdmissions')
	drop proc uspUpdateTheatreAdmissions
go

create proc uspUpdateTheatreAdmissions(
@TheatreID int,
@TheatreNo varchar(20),
@VisitNo varchar(20),
@ProcedureCode varchar(20),
@AdmissionDateTime SmallDateTime,
@OperationTypeID varchar(10),
@OperationScheduleID varchar(10),
@InformedConsentID varchar(10),
@InformedConsentNotes varchar(200),
@AdmissionStatusID varchar(10),
@StaffNo varchar(20),
@LoginID varchar(20),
@ClientMachine varchar(40),
@RecordDateTime SmallDateTime
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select TheatreNo from TheatreAdmissions where TheatreNo = @TheatreNo and VisitNo = @VisitNo and ProcedureCode = @ProcedureCode)
	begin
		set @ErrorMSG = 'The record with %s: %s and %s: %s and %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'Visit No', @VisitNo, 'Procedure Code', @ProcedureCode, 'TheatreAdmissions')
		return 1
	end



begin
update TheatreAdmissions set
TheatreID = @TheatreID, AdmissionDateTime = @AdmissionDateTime, OperationTypeID = @OperationTypeID, OperationScheduleID = @OperationScheduleID, InformedConsentID = @InformedConsentID, InformedConsentNotes = @InformedConsentNotes, AdmissionStatusID = @AdmissionStatusID, StaffNo = @StaffNo, LoginID = @LoginID, ClientMachine = @ClientMachine, RecordDateTime = @RecordDateTime
where TheatreNo = @TheatreNo and VisitNo = @VisitNo and ProcedureCode = @ProcedureCode
return 0
end
go

/******************************************************************************************************
exec uspUpdateTheatreAdmissions
******************************************************************************************************/
-- select * from TheatreAdmissions
-- delete from TheatreAdmissions


-------------- Get TheatreAdmissions -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspGetTheatreAdmissions')
	drop proc uspGetTheatreAdmissions
go

create proc uspGetTheatreAdmissions(
@TheatreNo varchar(20),
@VisitNo varchar(20),
@ProcedureCode varchar(20)
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select TheatreNo from TheatreAdmissions where TheatreNo = @TheatreNo and VisitNo = @VisitNo and ProcedureCode = @ProcedureCode)
	begin
		set @ErrorMSG = 'The record with %s: %s and %s: %s and %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'Visit No', @VisitNo, 'Procedure Code', @ProcedureCode, 'TheatreAdmissions')
		return 1
	end
else
begin
	select TheatreID, TheatreNo, VisitNo, ProcedureCode, AdmissionDateTime, OperationTypeID, OperationScheduleID, InformedConsentID, InformedConsentNotes, AdmissionStatusID, StaffNo, LoginID, ClientMachine, RecordDateTime
	from TheatreAdmissions

	where TheatreNo = @TheatreNo and VisitNo = @VisitNo and ProcedureCode = @ProcedureCode
return 0
end
go

/******************************************************************************************************
exec uspGetTheatreAdmissions
******************************************************************************************************/
-- select * from TheatreAdmissions
-- delete from TheatreAdmissions


------------------------------------------------------------------------------------------------------
-------------- Update: TheatreAdmissions ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrUpdateTheatreAdmissions')
	drop trigger utrUpdateTheatreAdmissions
go

create trigger utrUpdateTheatreAdmissions
on TheatreAdmissions
for update
as
declare @ErrorMSG varchar(200)
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Updates ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'U')
if @ObjectName is null return
if @ObjectName <> 'TheatreAdmissions' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return



------------------- Key-TheatreNo -----------------------------------------------------

begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'TheatreNo', Deleted.TheatreNo, Inserted.TheatreNo
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
end

------------------- Key-VisitNo -----------------------------------------------------

begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'VisitNo', Deleted.VisitNo, Inserted.VisitNo
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
end

------------------- Key-ProcedureCode -----------------------------------------------------

begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'ProcedureCode', Deleted.ProcedureCode, Inserted.ProcedureCode
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
end

-------------------  TheatreID  -----------------------------------------------------

if update(TheatreID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'TheatreID', Deleted.TheatreID, Inserted.TheatreID
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.TheatreID <> Deleted.TheatreID
end

-------------------  AdmissionDateTime  -----------------------------------------------------

if update(AdmissionDateTime)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'AdmissionDateTime', dbo.FormatDate(Deleted.AdmissionDateTime), dbo.FormatDate(Inserted.AdmissionDateTime)
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.AdmissionDateTime <> Deleted.AdmissionDateTime
end

-------------------  OperationTypeID  -----------------------------------------------------

if update(OperationTypeID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'OperationTypeID', Deleted.OperationTypeID, Inserted.OperationTypeID
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.OperationTypeID <> Deleted.OperationTypeID
end

-------------------  OperationScheduleID  -----------------------------------------------------

if update(OperationScheduleID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'OperationScheduleID', Deleted.OperationScheduleID, Inserted.OperationScheduleID
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.OperationScheduleID <> Deleted.OperationScheduleID
end

-------------------  InformedConsentID  -----------------------------------------------------

if update(InformedConsentID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'InformedConsentID', Deleted.InformedConsentID, Inserted.InformedConsentID
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.InformedConsentID <> Deleted.InformedConsentID
end

-------------------  InformedConsentNotes  -----------------------------------------------------

if update(InformedConsentNotes)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'InformedConsentNotes', Deleted.InformedConsentNotes, Inserted.InformedConsentNotes
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.InformedConsentNotes <> Deleted.InformedConsentNotes
end

-------------------  AdmissionStatusID  -----------------------------------------------------

if update(AdmissionStatusID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'AdmissionStatusID', Deleted.AdmissionStatusID, Inserted.AdmissionStatusID
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.AdmissionStatusID <> Deleted.AdmissionStatusID
end

-------------------  StaffNo  -----------------------------------------------------

if update(StaffNo)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'StaffNo', Deleted.StaffNo, Inserted.StaffNo
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.StaffNo <> Deleted.StaffNo
end

-------------------  LoginID  -----------------------------------------------------

if update(LoginID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'LoginID', Deleted.LoginID, Inserted.LoginID
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.LoginID <> Deleted.LoginID
end

-------------------  ClientMachine  -----------------------------------------------------

if update(ClientMachine)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'ClientMachine', Deleted.ClientMachine, Inserted.ClientMachine
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.ClientMachine <> Deleted.ClientMachine
end

-------------------  RecordDateTime  -----------------------------------------------------

if update(RecordDateTime)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'RecordDateTime', dbo.FormatDateTime(Deleted.RecordDateTime), dbo.FormatDateTime(Inserted.RecordDateTime)
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.VisitNo = Deleted.VisitNo
	and Inserted.ProcedureCode = Deleted.ProcedureCode
	and Inserted.RecordDateTime <> Deleted.RecordDateTime
end
go


------------------------------------------------------------------------------------------------------
-------------- Delete: TheatreAdmissions ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrDeleteTheatreAdmissions')
	drop trigger utrDeleteTheatreAdmissions
go

create trigger utrDeleteTheatreAdmissions
on TheatreAdmissions
for delete
as
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Deletes ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'D')
if @ObjectName is null return
if @ObjectName <> 'TheatreAdmissions' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return

-------------------  TheatreID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'TheatreID', Deleted.TheatreID, null from Deleted

-------------------  TheatreNo  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'TheatreNo', Deleted.TheatreNo, null from Deleted

-------------------  VisitNo  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'VisitNo', Deleted.VisitNo, null from Deleted

-------------------  ProcedureCode  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'ProcedureCode', Deleted.ProcedureCode, null from Deleted

-------------------  AdmissionDateTime  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'AdmissionDateTime', dbo.FormatDate(Deleted.AdmissionDateTime), null from Deleted

-------------------  OperationTypeID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'OperationTypeID', Deleted.OperationTypeID, null from Deleted

-------------------  OperationScheduleID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'OperationScheduleID', Deleted.OperationScheduleID, null from Deleted

-------------------  InformedConsentID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'InformedConsentID', Deleted.InformedConsentID, null from Deleted

-------------------  InformedConsentNotes  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'InformedConsentNotes', Deleted.InformedConsentNotes, null from Deleted

-------------------  AdmissionStatusID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'AdmissionStatusID', Deleted.AdmissionStatusID, null from Deleted

-------------------  StaffNo  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'StaffNo', Deleted.StaffNo, null from Deleted

-------------------  LoginID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'LoginID', Deleted.LoginID, null from Deleted

-------------------  ClientMachine  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'ClientMachine', Deleted.ClientMachine, null from Deleted

-------------------  RecordDateTime  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'RecordDateTime', dbo.FormatDateTime(Deleted.RecordDateTime), null from Deleted
go

