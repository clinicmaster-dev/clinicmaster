
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTheatreRecordChart : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton
Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton
Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton
Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton
Me.nbxRecordChartID = New SyncSoft.Common.Win.Controls.NumericBox
Me.lblRecordChartID = New System.Windows.Forms.Label
Me.stbRecordChartNo = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblRecordChartNo = New System.Windows.Forms.Label
Me.stbTheatreNo = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblTheatreNo = New System.Windows.Forms.Label
Me.stbAnesthetist = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblAnesthetist = New System.Windows.Forms.Label
Me.dtpRecordChartDateTime = New System.Windows.Forms.DateTimePicker
Me.lblRecordChartDateTime = New System.Windows.Forms.Label
Me.dtpRecordDateTime = New System.Windows.Forms.DateTimePicker
Me.lblRecordDateTime = New System.Windows.Forms.Label
Me.SuspendLayout()
'
'nbxRecordChartID
'
Me.nbxRecordChartID.Location = New System.Drawing.Point(218, 12)
Me.nbxRecordChartID.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.nbxRecordChartID, "RecordChartID")
Me.nbxRecordChartID.Name = "nbxRecordChartID"
Me.nbxRecordChartID.ControlCaption = "Record Chart ID"
Me.nbxRecordChartID.DataType = SyncSoft.Common.Win.Controls.Number.[Integer]
Me.nbxRecordChartID.MustEnterNumeric = True
'
'lblRecordChartID
'
Me.lblRecordChartID.Location = New System.Drawing.Point(12, 12)
Me.lblRecordChartID.Size = New System.Drawing.Size(200, 20)
Me.lblRecordChartID.Name = "lblRecordChartID"
Me.lblRecordChartID.Text = "Record Chart ID"
'
'stbRecordChartNo
'
Me.stbRecordChartNo.Location = New System.Drawing.Point(218, 35)
Me.stbRecordChartNo.Size = New System.Drawing.Size(170, 20)
Me.stbRecordChartNo.Name = "stbRecordChartNo"
'
'lblRecordChartNo
'
Me.lblRecordChartNo.Location = New System.Drawing.Point(12, 35)
Me.lblRecordChartNo.Size = New System.Drawing.Size(200, 20)
Me.lblRecordChartNo.Name = "lblRecordChartNo"
Me.lblRecordChartNo.Text = "Record Chart No"
'
'stbTheatreNo
'
Me.stbTheatreNo.Location = New System.Drawing.Point(218, 58)
Me.stbTheatreNo.Size = New System.Drawing.Size(170, 20)
Me.stbTheatreNo.Name = "stbTheatreNo"
'
'lblTheatreNo
'
Me.lblTheatreNo.Location = New System.Drawing.Point(12, 58)
Me.lblTheatreNo.Size = New System.Drawing.Size(200, 20)
Me.lblTheatreNo.Name = "lblTheatreNo"
Me.lblTheatreNo.Text = "Theatre No"
'
'stbAnesthetist
'
Me.stbAnesthetist.Location = New System.Drawing.Point(218, 81)
Me.stbAnesthetist.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbAnesthetist, "Anesthetist")
Me.stbAnesthetist.Name = "stbAnesthetist"
'
'lblAnesthetist
'
Me.lblAnesthetist.Location = New System.Drawing.Point(12, 81)
Me.lblAnesthetist.Size = New System.Drawing.Size(200, 20)
Me.lblAnesthetist.Name = "lblAnesthetist"
Me.lblAnesthetist.Text = "Anesthetist"
'
'dtpRecordChartDateTime
'
Me.dtpRecordChartDateTime.Location = New System.Drawing.Point(218, 104)
Me.dtpRecordChartDateTime.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.dtpRecordChartDateTime, "RecordChartDateTime")
Me.dtpRecordChartDateTime.Name = "dtpRecordChartDateTime"
Me.dtpRecordChartDateTime.ShowCheckBox = True
Me.dtpRecordChartDateTime.Checked = False
'
'lblRecordChartDateTime
'
Me.lblRecordChartDateTime.Location = New System.Drawing.Point(12, 104)
Me.lblRecordChartDateTime.Size = New System.Drawing.Size(200, 20)
Me.lblRecordChartDateTime.Name = "lblRecordChartDateTime"
Me.lblRecordChartDateTime.Text = "Record Chart Date Time"
'
'dtpRecordDateTime
'
Me.dtpRecordDateTime.Location = New System.Drawing.Point(218, 127)
Me.dtpRecordDateTime.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.dtpRecordDateTime, "RecordDateTime")
Me.dtpRecordDateTime.Name = "dtpRecordDateTime"
Me.dtpRecordDateTime.ShowCheckBox = True
Me.dtpRecordDateTime.Checked = False
'
'lblRecordDateTime
'
Me.lblRecordDateTime.Location = New System.Drawing.Point(12, 127)
Me.lblRecordDateTime.Size = New System.Drawing.Size(200, 20)
Me.lblRecordDateTime.Name = "lblRecordDateTime"
Me.lblRecordDateTime.Text = "Record Date Time"
'
'fbnSearch
'
Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnSearch.Location = New System.Drawing.Point(17, 157)
Me.fbnSearch.Name = "fbnSearch"
Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
Me.fbnSearch.Text = "S&earch"
Me.fbnSearch.UseVisualStyleBackColor = True
Me.fbnSearch.Visible = False
'
'fbnDelete
'
Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnDelete.Location = New System.Drawing.Point(316, 157)
Me.fbnDelete.Name = "fbnDelete"
Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
Me.fbnDelete.Tag = "TheatreRecordChart"
Me.fbnDelete.Text = "&Delete"
Me.fbnDelete.UseVisualStyleBackColor = False
Me.fbnDelete.Visible = False
'
'ebnSaveUpdate
'
Me.ebnSaveUpdate.ButtonText = SyncSoft.Common.Win.Controls.ButtonCaption.Save
Me.ebnSaveUpdate.DataSource = Nothing
Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.ebnSaveUpdate.Location = New System.Drawing.Point(17, 184)
Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
Me.ebnSaveUpdate.Tag = "TheatreRecordChart"
Me.ebnSaveUpdate.Text = "&Save"
Me.ebnSaveUpdate.UseVisualStyleBackColor = False
'
'fbnClose
'
Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnClose.Location = New System.Drawing.Point(316, 184)
Me.fbnClose.Name = "fbnClose"
Me.fbnClose.Size = New System.Drawing.Size(72, 24)
Me.fbnClose.Text = "&Close"
Me.fbnClose.UseVisualStyleBackColor = False
'
'frmTheatreRecordChart
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.CancelButton = Me.fbnClose
Me.ClientSize = New System.Drawing.Size(415,  234)
Me.Controls.Add(Me.fbnSearch)
Me.Controls.Add(Me.fbnDelete)
Me.Controls.Add(Me.ebnSaveUpdate)
Me.Controls.Add(Me.fbnClose)
Me.Controls.Add(Me.nbxRecordChartID)
Me.Controls.Add(Me.lblRecordChartID)
Me.Controls.Add(Me.stbRecordChartNo)
Me.Controls.Add(Me.lblRecordChartNo)
Me.Controls.Add(Me.stbTheatreNo)
Me.Controls.Add(Me.lblTheatreNo)
Me.Controls.Add(Me.stbAnesthetist)
Me.Controls.Add(Me.lblAnesthetist)
Me.Controls.Add(Me.dtpRecordChartDateTime)
Me.Controls.Add(Me.lblRecordChartDateTime)
Me.Controls.Add(Me.dtpRecordDateTime)
Me.Controls.Add(Me.lblRecordDateTime)
Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
Me.KeyPreview = True
Me.MaximizeBox = False
Me.Name = "frmTheatreRecordChart"
Me.Text = "TheatreRecordChart"
Me.ResumeLayout(False)
Me.PerformLayout()

End Sub

Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents nbxRecordChartID As SyncSoft.Common.Win.Controls.NumericBox
Friend WithEvents lblRecordChartID As System.Windows.Forms.Label
Friend WithEvents stbRecordChartNo As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblRecordChartNo As System.Windows.Forms.Label
Friend WithEvents stbTheatreNo As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblTheatreNo As System.Windows.Forms.Label
Friend WithEvents stbAnesthetist As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblAnesthetist As System.Windows.Forms.Label
Friend WithEvents dtpRecordChartDateTime As System.Windows.Forms.DateTimePicker
Friend WithEvents lblRecordChartDateTime As System.Windows.Forms.Label
Friend WithEvents dtpRecordDateTime As System.Windows.Forms.DateTimePicker
Friend WithEvents lblRecordDateTime As System.Windows.Forms.Label

End Class