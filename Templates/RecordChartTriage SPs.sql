
------------------------------------------------------------------------------------------------------
-------------- Create Table: RecordChartTriage ------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'RecordChartTriage')
	drop table RecordChartTriage
go

create table RecordChartTriage
(RecordChartNo varchar(20)
constraint fkRecordChartNoRecordChartTriage references TheatreRecordChart (RecordChartNo)
constraint pkRecordChartNo primary key,
VitalID varchar(20),
Value varchar(20),
LoginID varchar(20)
constraint fkLoginIDRecordChartTriage references Logins (LoginID),
ClientMachine varchar(40) constraint dfClientMachineRecordChartTriage default host_name(),
RecordDateTime smalldatetime constraint dfRecordDateTimeRecordChartTriage default getdate()
)
go


------------------------------------------------------------------------------------------------------
-------------- RecordChartTriage --------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-------------- Insert RecordChartTriage -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspInsertRecordChartTriage')
	drop proc uspInsertRecordChartTriage
go

create proc uspInsertRecordChartTriage(
@RecordChartNo varchar(20),
@VitalID varchar(20),
@Value varchar(20),
@LoginID varchar(20),
@ClientMachine varchar(40),
@RecordDateTime smalldatetime
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select RecordChartNo from TheatreRecordChart where RecordChartNo  = @RecordChartNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Record Chart No', @RecordChartNo, 'TheatreRecordChart')
		return 1
	end

if exists(select RecordChartNo from RecordChartTriage where RecordChartNo = @RecordChartNo)
	begin
		set @ErrorMSG = 'The record with %s: %s, you are trying to enter already exists'
		raiserror(@ErrorMSG, 16, 1, 'Record Chart No', @RecordChartNo)
		return 1
	end



begin
insert into RecordChartTriage
(RecordChartNo, VitalID, Value, LoginID, ClientMachine, RecordDateTime)
values
(@RecordChartNo, @VitalID, @Value, @LoginID, @ClientMachine, @RecordDateTime)
return 0
end
go

/******************************************************************************************************
exec uspInsertRecordChartTriage
******************************************************************************************************/
-- select * from RecordChartTriage
-- delete from RecordChartTriage


-------------- Update RecordChartTriage -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspUpdateRecordChartTriage')
	drop proc uspUpdateRecordChartTriage
go

create proc uspUpdateRecordChartTriage(
@RecordChartNo varchar(20),
@VitalID varchar(20),
@Value varchar(20),
@LoginID varchar(20),
@ClientMachine varchar(40),
@RecordDateTime smalldatetime
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select RecordChartNo from RecordChartTriage where RecordChartNo = @RecordChartNo)
	begin
		set @ErrorMSG = 'The record with %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Record Chart No', @RecordChartNo, 'RecordChartTriage')
		return 1
	end



begin
update RecordChartTriage set
VitalID = @VitalID, Value = @Value, LoginID = @LoginID, ClientMachine = @ClientMachine, RecordDateTime = @RecordDateTime
where RecordChartNo = @RecordChartNo
return 0
end
go

/******************************************************************************************************
exec uspUpdateRecordChartTriage
******************************************************************************************************/
-- select * from RecordChartTriage
-- delete from RecordChartTriage


-------------- Get RecordChartTriage -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspGetRecordChartTriage')
	drop proc uspGetRecordChartTriage
go

create proc uspGetRecordChartTriage(
@RecordChartNo varchar(20)
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select RecordChartNo from RecordChartTriage where RecordChartNo = @RecordChartNo)
	begin
		set @ErrorMSG = 'The record with %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Record Chart No', @RecordChartNo, 'RecordChartTriage')
		return 1
	end
else
begin
	select RecordChartNo, VitalID, Value, LoginID, ClientMachine, RecordDateTime
	from RecordChartTriage

	where RecordChartNo = @RecordChartNo
return 0
end
go

/******************************************************************************************************
exec uspGetRecordChartTriage
******************************************************************************************************/
-- select * from RecordChartTriage
-- delete from RecordChartTriage


------------------------------------------------------------------------------------------------------
-------------- Update: RecordChartTriage ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrUpdateRecordChartTriage')
	drop trigger utrUpdateRecordChartTriage
go

create trigger utrUpdateRecordChartTriage
on RecordChartTriage
for update
as
declare @ErrorMSG varchar(200)
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Updates ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'U')
if @ObjectName is null return
if @ObjectName <> 'RecordChartTriage' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return



------------------- Key-RecordChartNo -----------------------------------------------------

begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'RecordChartNo', Deleted.RecordChartNo, Inserted.RecordChartNo
	from Inserted inner join Deleted
	on Inserted.RecordChartNo = Deleted.RecordChartNo
end

-------------------  VitalID  -----------------------------------------------------

if update(VitalID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'VitalID', Deleted.VitalID, Inserted.VitalID
	from Inserted inner join Deleted
	on Inserted.RecordChartNo = Deleted.RecordChartNo
	and Inserted.VitalID <> Deleted.VitalID
end

-------------------  Value  -----------------------------------------------------

if update(Value)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'Value', Deleted.Value, Inserted.Value
	from Inserted inner join Deleted
	on Inserted.RecordChartNo = Deleted.RecordChartNo
	and Inserted.Value <> Deleted.Value
end

-------------------  LoginID  -----------------------------------------------------

if update(LoginID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'LoginID', Deleted.LoginID, Inserted.LoginID
	from Inserted inner join Deleted
	on Inserted.RecordChartNo = Deleted.RecordChartNo
	and Inserted.LoginID <> Deleted.LoginID
end

-------------------  ClientMachine  -----------------------------------------------------

if update(ClientMachine)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'ClientMachine', Deleted.ClientMachine, Inserted.ClientMachine
	from Inserted inner join Deleted
	on Inserted.RecordChartNo = Deleted.RecordChartNo
	and Inserted.ClientMachine <> Deleted.ClientMachine
end

-------------------  RecordDateTime  -----------------------------------------------------

if update(RecordDateTime)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'RecordDateTime', dbo.FormatDateTime(Deleted.RecordDateTime), dbo.FormatDateTime(Inserted.RecordDateTime)
	from Inserted inner join Deleted
	on Inserted.RecordChartNo = Deleted.RecordChartNo
	and Inserted.RecordDateTime <> Deleted.RecordDateTime
end
go


------------------------------------------------------------------------------------------------------
-------------- Delete: RecordChartTriage ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrDeleteRecordChartTriage')
	drop trigger utrDeleteRecordChartTriage
go

create trigger utrDeleteRecordChartTriage
on RecordChartTriage
for delete
as
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Deletes ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'D')
if @ObjectName is null return
if @ObjectName <> 'RecordChartTriage' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return

-------------------  RecordChartNo  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'RecordChartNo', Deleted.RecordChartNo, null from Deleted

-------------------  VitalID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'VitalID', Deleted.VitalID, null from Deleted

-------------------  Value  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'Value', Deleted.Value, null from Deleted

-------------------  LoginID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'LoginID', Deleted.LoginID, null from Deleted

-------------------  ClientMachine  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'ClientMachine', Deleted.ClientMachine, null from Deleted

-------------------  RecordDateTime  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'RecordDateTime', dbo.FormatDateTime(Deleted.RecordDateTime), null from Deleted
go

