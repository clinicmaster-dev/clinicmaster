
Public Class TheatreAdmissionsIPD : Inherits DBConnect

#Region " Fields "

#End Region

#Region " Auto-Implemented Properties "

Public Property TheatreNo As String
Public Property RoundNo As String

#End Region

#Region " Constructors "

Public Sub New()
	MyBase.New()
End Sub

Public Sub New(ByVal serverName As String, ByVal databaseName As String)
	MyClass.New()
	Me.ServerName = serverName
	Me.DatabaseName = databaseName
End Sub

#End Region

#Region " Methods "

Protected Overrides Function SaveData() As ArrayList

	Me.SetCommand("uspInsertTheatreAdmissionsIPD")

	With Parameters
		.Add(New ParameterSQL("TheatreNo", Me.TheatreNo))
		.Add(New ParameterSQL("RoundNo", Me.RoundNo))
	End With

	Return Parameters

End Function

Protected Overrides Function UpdateData() As ArrayList

	Me.SetCommand("uspUpdateTheatreAdmissionsIPD")

	With Parameters
		.Add(New ParameterSQL("TheatreNo", Me.TheatreNo))
		.Add(New ParameterSQL("RoundNo", Me.RoundNo))
	End With

	'For Audit Trail
	Me.SetLogObject("TheatreAdmissionsIPD")

	Return Parameters

End Function

Protected Overrides Function DeleteData() As ArrayList

	
	

	Me.SetCommand("uspDeleteObject")

	With Parameters
		.Add(New ParameterSQL("ObjectName", "TheatreAdmissionsIPD"))
		.Add(New ParameterSQL("Where", where))
		.Add(New ParameterSQL("ErrorPart", errorPart))
	End With

	'For Audit Trail
	Me.SetLogObject("TheatreAdmissionsIPD")

	Return Parameters

End Function

Public Function GetTheatreAdmissionsIPD() As DataSet

	With Parameters
	End With

	Return Me.Load("uspGetTheatreAdmissionsIPD", "TheatreAdmissionsIPD", Parameters)

End Function

#End Region



End Class