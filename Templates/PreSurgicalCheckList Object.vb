
Public Class PreSurgicalCheckList : Inherits DBConnect

#Region " Fields "

#End Region

#Region " Auto-Implemented Properties "

Public Property TheatreNo As String
Public Property PatientIdentified As String
Public Property IncisionSiteIdentified As String
Public Property TeamMembersIntroduced As String
Public Property ProphylaxisMedicationGiven  As String
Public Property CriticalNonRoutineSteps As String
Public Property AnticipatedDuration As String
Public Property AnticipatedBloodLoss As String
Public Property PatientConcerns As String
Public Property SterilityConfirmed As String
Public Property OtherConcerns As String
Public Property IsEssentialImageDisplayed As String
Public Property RecordDateTime As Date

#End Region

#Region " Constructors "

Public Sub New()
	MyBase.New()
End Sub

Public Sub New(ByVal serverName As String, ByVal databaseName As String)
	MyClass.New()
	Me.ServerName = serverName
	Me.DatabaseName = databaseName
End Sub

#End Region

#Region " Methods "

Protected Overrides Function SaveData() As ArrayList

	Me.SetCommand("uspInsertPreSurgicalCheckList")

	With Parameters
		.Add(New ParameterSQL("TheatreNo", Me.TheatreNo))
		.Add(New ParameterSQL("PatientIdentified", Me.PatientIdentified))
		.Add(New ParameterSQL("IncisionSiteIdentified", Me.IncisionSiteIdentified))
		.Add(New ParameterSQL("TeamMembersIntroduced", Me.TeamMembersIntroduced))
		.Add(New ParameterSQL("ProphylaxisMedicationGiven ", Me.ProphylaxisMedicationGiven ))
		.Add(New ParameterSQL("CriticalNonRoutineSteps", Me.CriticalNonRoutineSteps))
		.Add(New ParameterSQL("AnticipatedDuration", Me.AnticipatedDuration))
		.Add(New ParameterSQL("AnticipatedBloodLoss", Me.AnticipatedBloodLoss))
		.Add(New ParameterSQL("PatientConcerns", Me.PatientConcerns))
		.Add(New ParameterSQL("SterilityConfirmed", Me.SterilityConfirmed))
		.Add(New ParameterSQL("OtherConcerns", Me.OtherConcerns))
		.Add(New ParameterSQL("IsEssentialImageDisplayed", Me.IsEssentialImageDisplayed))
		.Add(New ParameterSQL("LoginID", Me.LoginID))
		.Add(New ParameterSQL("ClientMachine", My.Computer.Name))
		.Add(New ParameterSQL("RecordDateTime", Me.RecordDateTime))
	End With

	Return Parameters

End Function

Protected Overrides Function UpdateData() As ArrayList

	Me.SetCommand("uspUpdatePreSurgicalCheckList")

	With Parameters
		.Add(New ParameterSQL("TheatreNo", Me.TheatreNo))
		.Add(New ParameterSQL("PatientIdentified", Me.PatientIdentified))
		.Add(New ParameterSQL("IncisionSiteIdentified", Me.IncisionSiteIdentified))
		.Add(New ParameterSQL("TeamMembersIntroduced", Me.TeamMembersIntroduced))
		.Add(New ParameterSQL("ProphylaxisMedicationGiven ", Me.ProphylaxisMedicationGiven ))
		.Add(New ParameterSQL("CriticalNonRoutineSteps", Me.CriticalNonRoutineSteps))
		.Add(New ParameterSQL("AnticipatedDuration", Me.AnticipatedDuration))
		.Add(New ParameterSQL("AnticipatedBloodLoss", Me.AnticipatedBloodLoss))
		.Add(New ParameterSQL("PatientConcerns", Me.PatientConcerns))
		.Add(New ParameterSQL("SterilityConfirmed", Me.SterilityConfirmed))
		.Add(New ParameterSQL("OtherConcerns", Me.OtherConcerns))
		.Add(New ParameterSQL("IsEssentialImageDisplayed", Me.IsEssentialImageDisplayed))
		.Add(New ParameterSQL("LoginID", Me.LoginID))
		.Add(New ParameterSQL("ClientMachine", My.Computer.Name))
		.Add(New ParameterSQL("RecordDateTime", Me.RecordDateTime))
	End With

	'For Audit Trail
	Me.SetLogObject("PreSurgicalCheckList")

	Return Parameters

End Function

Protected Overrides Function DeleteData() As ArrayList

	Dim where As String = "TheatreNo = '" + Me.TheatreNo + "'
	Dim errorPart As String = "Theatre No: " + Me.TheatreNo

	Me.SetCommand("uspDeleteObject")

	With Parameters
		.Add(New ParameterSQL("ObjectName", "PreSurgicalCheckList"))
		.Add(New ParameterSQL("Where", where))
		.Add(New ParameterSQL("ErrorPart", errorPart))
	End With

	'For Audit Trail
	Me.SetLogObject("PreSurgicalCheckList")

	Return Parameters

End Function

Public Function GetPreSurgicalCheckList(ByVal theatreNo As String) As DataSet

	With Parameters
		.Add(New ParameterSQL("TheatreNo", theatreNo))
	End With

	Return Me.Load("uspGetPreSurgicalCheckList", "PreSurgicalCheckList", Parameters)

End Function

#End Region



End Class