
Option Strict On

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls

Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects

Public Class frmPreSurgicalCheckList

#Region " Fields "

#End Region

Private Sub frmPreSurgicalCheckList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

	Try
		Me.Cursor = Cursors.WaitCursor()



	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

Private Sub frmPreSurgicalCheckList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
	If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
End Sub

Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
	Me.Close()
End Sub

Private Sub fbnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnDelete.Click

Dim oPreSurgicalCheckList As New SyncSoft.SQLDb.PreSurgicalCheckList()

	Try
		Me.Cursor = Cursors.WaitCursor()

		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		If DeleteMessage() = Windows.Forms.DialogResult.No Then Return

		oPreSurgicalCheckList.TheatreNo = StringEnteredIn(Me.stbTheatreNo, "Theatre No!")

		DisplayMessage(oPreSurgicalCheckList.Delete())
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		ResetControlsIn(Me)
		Me.CallOnKeyEdit()

	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

Private Sub fbnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnSearch.Click

Dim theatreNo As String

Dim oPreSurgicalCheckList As New SyncSoft.SQLDb.PreSurgicalCheckList()

	Try
		Me.Cursor = Cursors.WaitCursor()

		theatreNo = StringEnteredIn(Me.stbTheatreNo, "Theatre No!")

		Dim dataSource As DataTable = oPreSurgicalCheckList.GetPreSurgicalCheckList(theatreNo).Tables("PreSurgicalCheckList")
		Me.DisplayData(dataSource)

	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

Private Sub ebnSaveUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ebnSaveUpdate.Click

Dim oPreSurgicalCheckList As New SyncSoft.SQLDb.PreSurgicalCheckList()

	Try
		Me.Cursor = Cursors.WaitCursor()

		With oPreSurgicalCheckList

		.TheatreNo = StringEnteredIn(Me.stbTheatreNo, "Theatre No!")
		.PatientIdentified = StringEnteredIn(Me.stbPatientIdentified, "Patient Identified!")
		.IncisionSiteIdentified = StringEnteredIn(Me.stbIncisionSiteIdentified, "Incision Site Identified!")
		.TeamMembersIntroduced = StringEnteredIn(Me.stbTeamMembersIntroduced, "Team Members Introduced!")
		.ProphylaxisMedicationGiven  = StringEnteredIn(Me.stbProphylaxisMedicationGiven , "Prophylaxis Medication Given !")
		.CriticalNonRoutineSteps = StringEnteredIn(Me.stbCriticalNonRoutineSteps, "Critical Non Routine Steps!")
		.AnticipatedDuration = StringEnteredIn(Me.stbAnticipatedDuration, "Anticipated Duration!")
		.AnticipatedBloodLoss = StringEnteredIn(Me.stbAnticipatedBloodLoss, "Anticipated Blood Loss!")
		.PatientConcerns = StringEnteredIn(Me.stbPatientConcerns, "PatientConcerns!")
		.SterilityConfirmed = StringEnteredIn(Me.stbSterilityConfirmed, "Sterility Confirmed!")
		.OtherConcerns = StringEnteredIn(Me.stbOtherConcerns, "Other Concerns!")
		.IsEssentialImageDisplayed = StringEnteredIn(Me.stbIsEssentialImageDisplayed, "Is Essential Image Displayed!")
		.LoginID = StringEnteredIn(Me.stbLoginID, "LoginID!")
		
		.RecordDateTime = DateEnteredIn(Me.dtpRecordDateTime, "Record Date Time!")

		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		ValidateEntriesIn(Me)
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		End With

		Select Case Me.ebnSaveUpdate.ButtonText

		Case ButtonCaption.Save

		oPreSurgicalCheckList.Save()

		''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		ResetControlsIn(Me)
		''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		Case ButtonCaption.Update

		DisplayMessage(oPreSurgicalCheckList.Update())
		Me.CallOnKeyEdit()

	End Select

	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

#Region " Edit Methods "

Public Sub Edit()

	Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update
	Me.ebnSaveUpdate.Enabled = False
	Me.fbnDelete.Visible = True
	Me.fbnDelete.Enabled = False
	Me.fbnSearch.Visible = True

	ResetControlsIn(Me)

End Sub

Public Sub Save()

	Me.ebnSaveUpdate.ButtonText = ButtonCaption.Save
	Me.ebnSaveUpdate.Enabled = True
	Me.fbnDelete.Visible = False
	Me.fbnDelete.Enabled = True
	Me.fbnSearch.Visible = False

	ResetControlsIn(Me)

End Sub

Private Sub DisplayData(ByVal dataSource As DataTable)

Try

	Me.ebnSaveUpdate.DataSource = dataSource
	Me.ebnSaveUpdate.LoadData(Me)

	Me.ebnSaveUpdate.Enabled = dataSource.Rows.Count > 0
	Me.fbnDelete.Enabled = dataSource.Rows.Count > 0

	Security.Apply(Me.ebnSaveUpdate, AccessRights.Update)
	Security.Apply(Me.fbnDelete, AccessRights.Delete)

Catch ex As Exception
	Throw ex
End Try

End Sub

Private Sub CallOnKeyEdit()
If Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update Then
	Me.ebnSaveUpdate.Enabled = False
	Me.fbnDelete.Enabled = False
End If
End Sub

#End Region

End Class