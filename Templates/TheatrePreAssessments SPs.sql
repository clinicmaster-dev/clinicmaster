
------------------------------------------------------------------------------------------------------
-------------- Create Table: TheatrePreAssessments ------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'TheatrePreAssessments')
	drop table TheatrePreAssessments
go

create table TheatrePreAssessments
(TheatreNo Varchar(20)
constraint fkTheatreNoTheatrePreAssessments references TheatreAdmissions (TheatreNo)
constraint pkTheatreNo primary key,
LeadAnesthetist varchar(20)
constraint fkLeadAnesthetistTheatrePreAssessments references Staff (LeadAnesthetist),
OtherAnesthetist varchar(200),
Height Decimal5,2,
Weight Decimal5,2,
HeartRate Tinyint,
OxygenSaturation Decimal5,2,
Hypertensive varchar(10)
constraint fkHypertensiveTheatrePreAssessments references LookupData (DataID),
Diabetic varchar(10)
constraint fkDiabeticTheatrePreAssessments references LookupData (DataID),
HIVStatus varchar(10)
constraint fkHIVStatusTheatrePreAssessments references LookupData (DataID),
Bathed varchar(10)
constraint fkBathedTheatrePreAssessments references LookupData (DataID),
CleanTheatreGown varchar(10)
constraint fkCleanTheatreGownTheatrePreAssessments references LookupData (DataID),
Smoker varchar(10)
constraint fkSmokerTheatrePreAssessments references LookupData (DataID),
AlcoholUse varchar(10)
constraint fkAlcoholUseTheatrePreAssessments references LookupData (DataID),
DrugUse varchar(10)
constraint fkDrugUseTheatrePreAssessments references LookupData (DataID),
ASAClassID varchar(10)
constraint fkASAClassIDTheatrePreAssessments references LookupData (DataID),
LastSolids SmallDateTime,
LastLiquids SmallDateTime,
PastHistory varchar(800),
GeneralCondition varchar(800),
PreoperativeInstructions  varchar(800),
AnesthesiaConsentID varchar(10)
constraint fkAnesthesiaConsentIDTheatrePreAssessments references LookupData (DataID),
AnesthesiaConsentNotes varchar(200),
LoginID varchar(20)
constraint fkLoginIDTheatrePreAssessments references Logins (LoginID),
ClientMachine varchar(40) constraint dfClientMachineTheatrePreAssessments default host_name(),
RecordDateTime SmallDateTime constraint dfRecordDateTimeTheatrePreAssessments default getDate()
)
go


------------------------------------------------------------------------------------------------------
-------------- TheatrePreAssessments --------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-------------- Insert TheatrePreAssessments -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspInsertTheatrePreAssessments')
	drop proc uspInsertTheatrePreAssessments
go

create proc uspInsertTheatrePreAssessments(
@TheatreNo Varchar(20),
@LeadAnesthetist varchar(20),
@OtherAnesthetist varchar(200),
@Height Decimal5,2,
@Weight Decimal5,2,
@HeartRate Tinyint,
@OxygenSaturation Decimal5,2,
@Hypertensive varchar(10),
@Diabetic varchar(10),
@HIVStatus varchar(10),
@Bathed varchar(10),
@CleanTheatreGown varchar(10),
@Smoker varchar(10),
@AlcoholUse varchar(10),
@DrugUse varchar(10),
@ASAClassID varchar(10),
@LastSolids SmallDateTime,
@LastLiquids SmallDateTime,
@PastHistory varchar(800),
@GeneralCondition varchar(800),
@PreoperativeInstructions  varchar(800),
@AnesthesiaConsentID varchar(10),
@AnesthesiaConsentNotes varchar(200),
@LoginID varchar(20),
@ClientMachine varchar(40),
@RecordDateTime SmallDateTime
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select TheatreNo from TheatreAdmissions where TheatreNo  = @TheatreNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'TheatreAdmissions')
		return 1
	end

if exists(select TheatreNo from TheatrePreAssessments where TheatreNo = @TheatreNo)
	begin
		set @ErrorMSG = 'The record with %s: %s, you are trying to enter already exists'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo)
		return 1
	end



begin
insert into TheatrePreAssessments
(TheatreNo, LeadAnesthetist, OtherAnesthetist, Height, Weight, HeartRate, OxygenSaturation, Hypertensive, Diabetic, HIVStatus, Bathed, CleanTheatreGown, Smoker, AlcoholUse, DrugUse, ASAClassID, LastSolids, LastLiquids, PastHistory, GeneralCondition, PreoperativeInstructions , AnesthesiaConsentID, AnesthesiaConsentNotes, LoginID, ClientMachine, RecordDateTime)
values
(@TheatreNo, @LeadAnesthetist, @OtherAnesthetist, @Height, @Weight, @HeartRate, @OxygenSaturation, @Hypertensive, @Diabetic, @HIVStatus, @Bathed, @CleanTheatreGown, @Smoker, @AlcoholUse, @DrugUse, @ASAClassID, @LastSolids, @LastLiquids, @PastHistory, @GeneralCondition, @PreoperativeInstructions , @AnesthesiaConsentID, @AnesthesiaConsentNotes, @LoginID, @ClientMachine, @RecordDateTime)
return 0
end
go

/******************************************************************************************************
exec uspInsertTheatrePreAssessments
******************************************************************************************************/
-- select * from TheatrePreAssessments
-- delete from TheatrePreAssessments


-------------- Update TheatrePreAssessments -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspUpdateTheatrePreAssessments')
	drop proc uspUpdateTheatrePreAssessments
go

create proc uspUpdateTheatrePreAssessments(
@TheatreNo Varchar(20),
@LeadAnesthetist varchar(20),
@OtherAnesthetist varchar(200),
@Height Decimal5,2,
@Weight Decimal5,2,
@HeartRate Tinyint,
@OxygenSaturation Decimal5,2,
@Hypertensive varchar(10),
@Diabetic varchar(10),
@HIVStatus varchar(10),
@Bathed varchar(10),
@CleanTheatreGown varchar(10),
@Smoker varchar(10),
@AlcoholUse varchar(10),
@DrugUse varchar(10),
@ASAClassID varchar(10),
@LastSolids SmallDateTime,
@LastLiquids SmallDateTime,
@PastHistory varchar(800),
@GeneralCondition varchar(800),
@PreoperativeInstructions  varchar(800),
@AnesthesiaConsentID varchar(10),
@AnesthesiaConsentNotes varchar(200),
@LoginID varchar(20),
@ClientMachine varchar(40),
@RecordDateTime SmallDateTime
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select TheatreNo from TheatrePreAssessments where TheatreNo = @TheatreNo)
	begin
		set @ErrorMSG = 'The record with %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'TheatrePreAssessments')
		return 1
	end



begin
update TheatrePreAssessments set
LeadAnesthetist = @LeadAnesthetist, OtherAnesthetist = @OtherAnesthetist, Height = @Height, Weight = @Weight, HeartRate = @HeartRate, OxygenSaturation = @OxygenSaturation, Hypertensive = @Hypertensive, Diabetic = @Diabetic, HIVStatus = @HIVStatus, Bathed = @Bathed, CleanTheatreGown = @CleanTheatreGown, Smoker = @Smoker, AlcoholUse = @AlcoholUse, DrugUse = @DrugUse, ASAClassID = @ASAClassID, LastSolids = @LastSolids, LastLiquids = @LastLiquids, PastHistory = @PastHistory, GeneralCondition = @GeneralCondition, PreoperativeInstructions  = @PreoperativeInstructions , AnesthesiaConsentID = @AnesthesiaConsentID, AnesthesiaConsentNotes = @AnesthesiaConsentNotes, LoginID = @LoginID, ClientMachine = @ClientMachine, RecordDateTime = @RecordDateTime
where TheatreNo = @TheatreNo
return 0
end
go

/******************************************************************************************************
exec uspUpdateTheatrePreAssessments
******************************************************************************************************/
-- select * from TheatrePreAssessments
-- delete from TheatrePreAssessments


-------------- Get TheatrePreAssessments -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspGetTheatrePreAssessments')
	drop proc uspGetTheatrePreAssessments
go

create proc uspGetTheatrePreAssessments(
@TheatreNo Varchar(20)
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select TheatreNo from TheatrePreAssessments where TheatreNo = @TheatreNo)
	begin
		set @ErrorMSG = 'The record with %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'TheatrePreAssessments')
		return 1
	end
else
begin
	select TheatreNo, LeadAnesthetist, OtherAnesthetist, Height, Weight, HeartRate, OxygenSaturation, Hypertensive, Diabetic, HIVStatus, Bathed, CleanTheatreGown, Smoker, AlcoholUse, DrugUse, ASAClassID, LastSolids, LastLiquids, PastHistory, GeneralCondition, PreoperativeInstructions , AnesthesiaConsentID, AnesthesiaConsentNotes, LoginID, ClientMachine, RecordDateTime
	from TheatrePreAssessments

	where TheatreNo = @TheatreNo
return 0
end
go

/******************************************************************************************************
exec uspGetTheatrePreAssessments
******************************************************************************************************/
-- select * from TheatrePreAssessments
-- delete from TheatrePreAssessments


------------------------------------------------------------------------------------------------------
-------------- Update: TheatrePreAssessments ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrUpdateTheatrePreAssessments')
	drop trigger utrUpdateTheatrePreAssessments
go

create trigger utrUpdateTheatrePreAssessments
on TheatrePreAssessments
for update
as
declare @ErrorMSG varchar(200)
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Updates ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'U')
if @ObjectName is null return
if @ObjectName <> 'TheatrePreAssessments' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return



------------------- Key-TheatreNo -----------------------------------------------------

begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'TheatreNo', Deleted.TheatreNo, Inserted.TheatreNo
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
end

-------------------  LeadAnesthetist  -----------------------------------------------------

if update(LeadAnesthetist)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'LeadAnesthetist', Deleted.LeadAnesthetist, Inserted.LeadAnesthetist
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.LeadAnesthetist <> Deleted.LeadAnesthetist
end

-------------------  OtherAnesthetist  -----------------------------------------------------

if update(OtherAnesthetist)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'OtherAnesthetist', Deleted.OtherAnesthetist, Inserted.OtherAnesthetist
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.OtherAnesthetist <> Deleted.OtherAnesthetist
end

-------------------  Height  -----------------------------------------------------

if update(Height)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'Height', Deleted.Height, Inserted.Height
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.Height <> Deleted.Height
end

-------------------  Weight  -----------------------------------------------------

if update(Weight)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'Weight', Deleted.Weight, Inserted.Weight
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.Weight <> Deleted.Weight
end

-------------------  HeartRate  -----------------------------------------------------

if update(HeartRate)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'HeartRate', Deleted.HeartRate, Inserted.HeartRate
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.HeartRate <> Deleted.HeartRate
end

-------------------  OxygenSaturation  -----------------------------------------------------

if update(OxygenSaturation)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'OxygenSaturation', Deleted.OxygenSaturation, Inserted.OxygenSaturation
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.OxygenSaturation <> Deleted.OxygenSaturation
end

-------------------  Hypertensive  -----------------------------------------------------

if update(Hypertensive)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'Hypertensive', Deleted.Hypertensive, Inserted.Hypertensive
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.Hypertensive <> Deleted.Hypertensive
end

-------------------  Diabetic  -----------------------------------------------------

if update(Diabetic)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'Diabetic', Deleted.Diabetic, Inserted.Diabetic
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.Diabetic <> Deleted.Diabetic
end

-------------------  HIVStatus  -----------------------------------------------------

if update(HIVStatus)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'HIVStatus', Deleted.HIVStatus, Inserted.HIVStatus
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.HIVStatus <> Deleted.HIVStatus
end

-------------------  Bathed  -----------------------------------------------------

if update(Bathed)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'Bathed', Deleted.Bathed, Inserted.Bathed
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.Bathed <> Deleted.Bathed
end

-------------------  CleanTheatreGown  -----------------------------------------------------

if update(CleanTheatreGown)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'CleanTheatreGown', Deleted.CleanTheatreGown, Inserted.CleanTheatreGown
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.CleanTheatreGown <> Deleted.CleanTheatreGown
end

-------------------  Smoker  -----------------------------------------------------

if update(Smoker)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'Smoker', Deleted.Smoker, Inserted.Smoker
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.Smoker <> Deleted.Smoker
end

-------------------  AlcoholUse  -----------------------------------------------------

if update(AlcoholUse)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'AlcoholUse', Deleted.AlcoholUse, Inserted.AlcoholUse
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.AlcoholUse <> Deleted.AlcoholUse
end

-------------------  DrugUse  -----------------------------------------------------

if update(DrugUse)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'DrugUse', Deleted.DrugUse, Inserted.DrugUse
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.DrugUse <> Deleted.DrugUse
end

-------------------  ASAClassID  -----------------------------------------------------

if update(ASAClassID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'ASAClassID', Deleted.ASAClassID, Inserted.ASAClassID
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.ASAClassID <> Deleted.ASAClassID
end

-------------------  LastSolids  -----------------------------------------------------

if update(LastSolids)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'LastSolids', dbo.FormatDate(Deleted.LastSolids), dbo.FormatDate(Inserted.LastSolids)
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.LastSolids <> Deleted.LastSolids
end

-------------------  LastLiquids  -----------------------------------------------------

if update(LastLiquids)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'LastLiquids', dbo.FormatDate(Deleted.LastLiquids), dbo.FormatDate(Inserted.LastLiquids)
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.LastLiquids <> Deleted.LastLiquids
end

-------------------  PastHistory  -----------------------------------------------------

if update(PastHistory)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'PastHistory', Deleted.PastHistory, Inserted.PastHistory
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.PastHistory <> Deleted.PastHistory
end

-------------------  GeneralCondition  -----------------------------------------------------

if update(GeneralCondition)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'GeneralCondition', Deleted.GeneralCondition, Inserted.GeneralCondition
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.GeneralCondition <> Deleted.GeneralCondition
end

-------------------  PreoperativeInstructions   -----------------------------------------------------

if update(PreoperativeInstructions )
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'PreoperativeInstructions ', Deleted.PreoperativeInstructions , Inserted.PreoperativeInstructions 
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.PreoperativeInstructions  <> Deleted.PreoperativeInstructions 
end

-------------------  AnesthesiaConsentID  -----------------------------------------------------

if update(AnesthesiaConsentID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'AnesthesiaConsentID', Deleted.AnesthesiaConsentID, Inserted.AnesthesiaConsentID
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.AnesthesiaConsentID <> Deleted.AnesthesiaConsentID
end

-------------------  AnesthesiaConsentNotes  -----------------------------------------------------

if update(AnesthesiaConsentNotes)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'AnesthesiaConsentNotes', Deleted.AnesthesiaConsentNotes, Inserted.AnesthesiaConsentNotes
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.AnesthesiaConsentNotes <> Deleted.AnesthesiaConsentNotes
end

-------------------  LoginID  -----------------------------------------------------

if update(LoginID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'LoginID', Deleted.LoginID, Inserted.LoginID
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.LoginID <> Deleted.LoginID
end

-------------------  ClientMachine  -----------------------------------------------------

if update(ClientMachine)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'ClientMachine', Deleted.ClientMachine, Inserted.ClientMachine
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.ClientMachine <> Deleted.ClientMachine
end

-------------------  RecordDateTime  -----------------------------------------------------

if update(RecordDateTime)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'RecordDateTime', dbo.FormatDateTime(Deleted.RecordDateTime), dbo.FormatDateTime(Inserted.RecordDateTime)
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.RecordDateTime <> Deleted.RecordDateTime
end
go


------------------------------------------------------------------------------------------------------
-------------- Delete: TheatrePreAssessments ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrDeleteTheatrePreAssessments')
	drop trigger utrDeleteTheatrePreAssessments
go

create trigger utrDeleteTheatrePreAssessments
on TheatrePreAssessments
for delete
as
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Deletes ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'D')
if @ObjectName is null return
if @ObjectName <> 'TheatrePreAssessments' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return

-------------------  TheatreNo  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'TheatreNo', Deleted.TheatreNo, null from Deleted

-------------------  LeadAnesthetist  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'LeadAnesthetist', Deleted.LeadAnesthetist, null from Deleted

-------------------  OtherAnesthetist  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'OtherAnesthetist', Deleted.OtherAnesthetist, null from Deleted

-------------------  Height  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'Height', Deleted.Height, null from Deleted

-------------------  Weight  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'Weight', Deleted.Weight, null from Deleted

-------------------  HeartRate  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'HeartRate', Deleted.HeartRate, null from Deleted

-------------------  OxygenSaturation  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'OxygenSaturation', Deleted.OxygenSaturation, null from Deleted

-------------------  Hypertensive  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'Hypertensive', Deleted.Hypertensive, null from Deleted

-------------------  Diabetic  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'Diabetic', Deleted.Diabetic, null from Deleted

-------------------  HIVStatus  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'HIVStatus', Deleted.HIVStatus, null from Deleted

-------------------  Bathed  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'Bathed', Deleted.Bathed, null from Deleted

-------------------  CleanTheatreGown  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'CleanTheatreGown', Deleted.CleanTheatreGown, null from Deleted

-------------------  Smoker  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'Smoker', Deleted.Smoker, null from Deleted

-------------------  AlcoholUse  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'AlcoholUse', Deleted.AlcoholUse, null from Deleted

-------------------  DrugUse  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'DrugUse', Deleted.DrugUse, null from Deleted

-------------------  ASAClassID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'ASAClassID', Deleted.ASAClassID, null from Deleted

-------------------  LastSolids  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'LastSolids', dbo.FormatDate(Deleted.LastSolids), null from Deleted

-------------------  LastLiquids  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'LastLiquids', dbo.FormatDate(Deleted.LastLiquids), null from Deleted

-------------------  PastHistory  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'PastHistory', Deleted.PastHistory, null from Deleted

-------------------  GeneralCondition  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'GeneralCondition', Deleted.GeneralCondition, null from Deleted

-------------------  PreoperativeInstructions   -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'PreoperativeInstructions ', Deleted.PreoperativeInstructions , null from Deleted

-------------------  AnesthesiaConsentID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'AnesthesiaConsentID', Deleted.AnesthesiaConsentID, null from Deleted

-------------------  AnesthesiaConsentNotes  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'AnesthesiaConsentNotes', Deleted.AnesthesiaConsentNotes, null from Deleted

-------------------  LoginID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'LoginID', Deleted.LoginID, null from Deleted

-------------------  ClientMachine  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'ClientMachine', Deleted.ClientMachine, null from Deleted

-------------------  RecordDateTime  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'RecordDateTime', dbo.FormatDateTime(Deleted.RecordDateTime), null from Deleted
go

