
Option Strict On

Imports SyncSoft.Security
Imports SyncSoft.Common.Methods
Imports SyncSoft.Lookup.SQL.Methods
Imports SyncSoft.Common.SQL.Methods
Imports SyncSoft.Common.Win.Controls

Imports LookupObjects = SyncSoft.SQLDb.Lookup.LookupObjects

Public Class frmTheatrePreAssessments

#Region " Fields "

#End Region

Private Sub frmTheatrePreAssessments_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

	Try
		Me.Cursor = Cursors.WaitCursor()



	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

Private Sub frmTheatrePreAssessments_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
	If e.KeyCode = Keys.Enter Then Me.ProcessTabKey(True)
End Sub

Private Sub fbnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnClose.Click
	Me.Close()
End Sub

Private Sub fbnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnDelete.Click

Dim oTheatrePreAssessments As New SyncSoft.SQLDb.TheatrePreAssessments()

	Try
		Me.Cursor = Cursors.WaitCursor()

		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		If DeleteMessage() = Windows.Forms.DialogResult.No Then Return

		oTheatrePreAssessments.TheatreNo = StringEnteredIn(Me.stbTheatreNo, "Theatre No!")

		DisplayMessage(oTheatrePreAssessments.Delete())
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		ResetControlsIn(Me)
		Me.CallOnKeyEdit()

	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

Private Sub fbnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fbnSearch.Click

Dim theatreNo As String

Dim oTheatrePreAssessments As New SyncSoft.SQLDb.TheatrePreAssessments()

	Try
		Me.Cursor = Cursors.WaitCursor()

		theatreNo = StringEnteredIn(Me.stbTheatreNo, "Theatre No!")

		Dim dataSource As DataTable = oTheatrePreAssessments.GetTheatrePreAssessments(theatreNo).Tables("TheatrePreAssessments")
		Me.DisplayData(dataSource)

	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

Private Sub ebnSaveUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ebnSaveUpdate.Click

Dim oTheatrePreAssessments As New SyncSoft.SQLDb.TheatrePreAssessments()

	Try
		Me.Cursor = Cursors.WaitCursor()

		With oTheatrePreAssessments

		.TheatreNo = StringEnteredIn(Me.stbTheatreNo, "Theatre No!")
		.LeadAnesthetist = StringEnteredIn(Me.stbLeadAnesthetist, "Lead Anesthetist!")
		.OtherAnesthetist = StringEnteredIn(Me.stbOtherAnesthetist, "Other Anesthetist!")
		.Height = Me.nbxHeight.GetSingle()
		.Weight = Me.nbxWeight.GetSingle()
		.HeartRate = Me.nbxHeartRate.GetInteger()
		.OxygenSaturation = Me.nbxOxygenSaturation.GetSingle()
		.Hypertensive = StringEnteredIn(Me.stbHypertensive, "Hypertensive!")
		.Diabetic = StringEnteredIn(Me.stbDiabetic, "Diabetic!")
		.HIVStatus = StringEnteredIn(Me.stbHIVStatus, "HIV Status!")
		.Bathed = StringEnteredIn(Me.stbBathed, "Bathed!")
		.CleanTheatreGown = StringEnteredIn(Me.stbCleanTheatreGown, "Clean Theatre Gown!")
		.Smoker = StringEnteredIn(Me.stbSmoker, "Smoker!")
		.AlcoholUse = StringEnteredIn(Me.stbAlcoholUse, "Alcohol Use!")
		.DrugUse = StringEnteredIn(Me.stbDrugUse, "Drug Use!")
		.ASAClassID = StringEnteredIn(Me.stbASAClassID, "ASA Class!")
		.LastSolids = DateEnteredIn(Me.dtpLastSolids, "Last Solids!")
		.LastLiquids = DateEnteredIn(Me.dtpLastLiquids, "Last Liquids!")
		.PastHistory = StringEnteredIn(Me.stbPastHistory, "Past History!")
		.GeneralCondition = StringEnteredIn(Me.stbGeneralCondition, "General Condition!")
		.PreoperativeInstructions  = StringEnteredIn(Me.stbPreoperativeInstructions , "Preoperative Instructions !")
		.AnesthesiaConsentID = StringEnteredIn(Me.stbAnesthesiaConsentID, "Anesthesia Consent!")
		.AnesthesiaConsentNotes = StringEnteredIn(Me.stbAnesthesiaConsentNotes, "Anesthesia Consent Notes!")
		.LoginID = StringEnteredIn(Me.stbLoginID, "Login ID!")
		
		.RecordDateTime = DateEnteredIn(Me.dtpRecordDateTime, "RecordDateTime!")

		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		ValidateEntriesIn(Me)
		'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		End With

		Select Case Me.ebnSaveUpdate.ButtonText

		Case ButtonCaption.Save

		oTheatrePreAssessments.Save()

		''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		ResetControlsIn(Me)
		''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		Case ButtonCaption.Update

		DisplayMessage(oTheatrePreAssessments.Update())
		Me.CallOnKeyEdit()

	End Select

	Catch ex As Exception
		ErrorMessage(ex)

	Finally
		Me.Cursor = Cursors.Default()

	End Try

End Sub

#Region " Edit Methods "

Public Sub Edit()

	Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update
	Me.ebnSaveUpdate.Enabled = False
	Me.fbnDelete.Visible = True
	Me.fbnDelete.Enabled = False
	Me.fbnSearch.Visible = True

	ResetControlsIn(Me)

End Sub

Public Sub Save()

	Me.ebnSaveUpdate.ButtonText = ButtonCaption.Save
	Me.ebnSaveUpdate.Enabled = True
	Me.fbnDelete.Visible = False
	Me.fbnDelete.Enabled = True
	Me.fbnSearch.Visible = False

	ResetControlsIn(Me)

End Sub

Private Sub DisplayData(ByVal dataSource As DataTable)

Try

	Me.ebnSaveUpdate.DataSource = dataSource
	Me.ebnSaveUpdate.LoadData(Me)

	Me.ebnSaveUpdate.Enabled = dataSource.Rows.Count > 0
	Me.fbnDelete.Enabled = dataSource.Rows.Count > 0

	Security.Apply(Me.ebnSaveUpdate, AccessRights.Update)
	Security.Apply(Me.fbnDelete, AccessRights.Delete)

Catch ex As Exception
	Throw ex
End Try

End Sub

Private Sub CallOnKeyEdit()
If Me.ebnSaveUpdate.ButtonText = ButtonCaption.Update Then
	Me.ebnSaveUpdate.Enabled = False
	Me.fbnDelete.Enabled = False
End If
End Sub

#End Region

End Class