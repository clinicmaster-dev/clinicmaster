
------------------------------------------------------------------------------------------------------
-------------- Create Table: PreSurgicalCheckList ------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'PreSurgicalCheckList')
	drop table PreSurgicalCheckList
go

create table PreSurgicalCheckList
(TheatreNo varchar(20) not null
constraint fkTheatreNoPreSurgicalCheckList references TheatreAdmissions (TheatreNo)
constraint pkTheatreNo primary key,
PatientIdentified Varchar(10)
constraint fkPatientIdentifiedPreSurgicalCheckList references LookupData (DataID),
IncisionSiteIdentified Varchar(10)
constraint fkIncisionSiteIdentifiedPreSurgicalCheckList references LookupData (DataID),
TeamMembersIntroduced Varchar(10)
constraint fkTeamMembersIntroducedPreSurgicalCheckList references LookupData (DataID),
ProphylaxisMedicationGiven  Varchar(10)
constraint fkProphylaxisMedicationGiven PreSurgicalCheckList references LookupData (DataID),
CriticalNonRoutineSteps Varchar(800),
AnticipatedDuration Varchar(100),
AnticipatedBloodLoss Varchar(100),
PatientConcerns Varchar(800),
SterilityConfirmed Varchar(10)
constraint fkSterilityConfirmedPreSurgicalCheckList references LookupData (DataID),
OtherConcerns Varchar(800),
IsEssentialImageDisplayed Varchar(10)
constraint fkIsEssentialImageDisplayedPreSurgicalCheckList references LookupData (DataID),
LoginID varchar(20)
constraint fkLoginIDPreSurgicalCheckList references Logins (LoginID),
ClientMachine varchar(40) constraint dfClientMachinePreSurgicalCheckList default host_name(),
RecordDateTime smalldatetime constraint dfRecordDateTimePreSurgicalCheckList default getDate()
)
go


------------------------------------------------------------------------------------------------------
-------------- PreSurgicalCheckList --------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-------------- Insert PreSurgicalCheckList -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspInsertPreSurgicalCheckList')
	drop proc uspInsertPreSurgicalCheckList
go

create proc uspInsertPreSurgicalCheckList(
@TheatreNo varchar(20),
@PatientIdentified Varchar(10),
@IncisionSiteIdentified Varchar(10),
@TeamMembersIntroduced Varchar(10),
@ProphylaxisMedicationGiven  Varchar(10),
@CriticalNonRoutineSteps Varchar(800),
@AnticipatedDuration Varchar(100),
@AnticipatedBloodLoss Varchar(100),
@PatientConcerns Varchar(800),
@SterilityConfirmed Varchar(10),
@OtherConcerns Varchar(800),
@IsEssentialImageDisplayed Varchar(10),
@LoginID varchar(20),
@ClientMachine varchar(40),
@RecordDateTime smalldatetime
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select TheatreNo from TheatreAdmissions where TheatreNo  = @TheatreNo)
	begin
		set @ErrorMSG = 'The %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'TheatreAdmissions')
		return 1
	end

if exists(select TheatreNo from PreSurgicalCheckList where TheatreNo = @TheatreNo)
	begin
		set @ErrorMSG = 'The record with %s: %s, you are trying to enter already exists'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo)
		return 1
	end



begin
insert into PreSurgicalCheckList
(TheatreNo, PatientIdentified, IncisionSiteIdentified, TeamMembersIntroduced, ProphylaxisMedicationGiven , CriticalNonRoutineSteps, AnticipatedDuration, AnticipatedBloodLoss, PatientConcerns, SterilityConfirmed, OtherConcerns, IsEssentialImageDisplayed, LoginID, ClientMachine, RecordDateTime)
values
(@TheatreNo, @PatientIdentified, @IncisionSiteIdentified, @TeamMembersIntroduced, @ProphylaxisMedicationGiven , @CriticalNonRoutineSteps, @AnticipatedDuration, @AnticipatedBloodLoss, @PatientConcerns, @SterilityConfirmed, @OtherConcerns, @IsEssentialImageDisplayed, @LoginID, @ClientMachine, @RecordDateTime)
return 0
end
go

/******************************************************************************************************
exec uspInsertPreSurgicalCheckList
******************************************************************************************************/
-- select * from PreSurgicalCheckList
-- delete from PreSurgicalCheckList


-------------- Update PreSurgicalCheckList -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspUpdatePreSurgicalCheckList')
	drop proc uspUpdatePreSurgicalCheckList
go

create proc uspUpdatePreSurgicalCheckList(
@TheatreNo varchar(20),
@PatientIdentified Varchar(10),
@IncisionSiteIdentified Varchar(10),
@TeamMembersIntroduced Varchar(10),
@ProphylaxisMedicationGiven  Varchar(10),
@CriticalNonRoutineSteps Varchar(800),
@AnticipatedDuration Varchar(100),
@AnticipatedBloodLoss Varchar(100),
@PatientConcerns Varchar(800),
@SterilityConfirmed Varchar(10),
@OtherConcerns Varchar(800),
@IsEssentialImageDisplayed Varchar(10),
@LoginID varchar(20),
@ClientMachine varchar(40),
@RecordDateTime smalldatetime
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select TheatreNo from PreSurgicalCheckList where TheatreNo = @TheatreNo)
	begin
		set @ErrorMSG = 'The record with %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'PreSurgicalCheckList')
		return 1
	end



begin
update PreSurgicalCheckList set
PatientIdentified = @PatientIdentified, IncisionSiteIdentified = @IncisionSiteIdentified, TeamMembersIntroduced = @TeamMembersIntroduced, ProphylaxisMedicationGiven  = @ProphylaxisMedicationGiven , CriticalNonRoutineSteps = @CriticalNonRoutineSteps, AnticipatedDuration = @AnticipatedDuration, AnticipatedBloodLoss = @AnticipatedBloodLoss, PatientConcerns = @PatientConcerns, SterilityConfirmed = @SterilityConfirmed, OtherConcerns = @OtherConcerns, IsEssentialImageDisplayed = @IsEssentialImageDisplayed, LoginID = @LoginID, ClientMachine = @ClientMachine, RecordDateTime = @RecordDateTime
where TheatreNo = @TheatreNo
return 0
end
go

/******************************************************************************************************
exec uspUpdatePreSurgicalCheckList
******************************************************************************************************/
-- select * from PreSurgicalCheckList
-- delete from PreSurgicalCheckList


-------------- Get PreSurgicalCheckList -------------------------------------------------------------

if exists (select * from sysobjects where name = 'uspGetPreSurgicalCheckList')
	drop proc uspGetPreSurgicalCheckList
go

create proc uspGetPreSurgicalCheckList(
@TheatreNo varchar(20)
)with encryption as

declare @ErrorMSG varchar(200)

if not exists(select TheatreNo from PreSurgicalCheckList where TheatreNo = @TheatreNo)
	begin
		set @ErrorMSG = 'The record with %s: %s, you are trying to enter does not exist in the registered %s'
		raiserror(@ErrorMSG, 16, 1, 'Theatre No', @TheatreNo, 'PreSurgicalCheckList')
		return 1
	end
else
begin
	select TheatreNo, PatientIdentified, IncisionSiteIdentified, TeamMembersIntroduced, ProphylaxisMedicationGiven , CriticalNonRoutineSteps, AnticipatedDuration, AnticipatedBloodLoss, PatientConcerns, SterilityConfirmed, OtherConcerns, IsEssentialImageDisplayed, LoginID, ClientMachine, RecordDateTime
	from PreSurgicalCheckList

	where TheatreNo = @TheatreNo
return 0
end
go

/******************************************************************************************************
exec uspGetPreSurgicalCheckList
******************************************************************************************************/
-- select * from PreSurgicalCheckList
-- delete from PreSurgicalCheckList


------------------------------------------------------------------------------------------------------
-------------- Update: PreSurgicalCheckList ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrUpdatePreSurgicalCheckList')
	drop trigger utrUpdatePreSurgicalCheckList
go

create trigger utrUpdatePreSurgicalCheckList
on PreSurgicalCheckList
for update
as
declare @ErrorMSG varchar(200)
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Updates ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'U')
if @ObjectName is null return
if @ObjectName <> 'PreSurgicalCheckList' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return



------------------- Key-TheatreNo -----------------------------------------------------

begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'TheatreNo', Deleted.TheatreNo, Inserted.TheatreNo
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
end

-------------------  PatientIdentified  -----------------------------------------------------

if update(PatientIdentified)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'PatientIdentified', Deleted.PatientIdentified, Inserted.PatientIdentified
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.PatientIdentified <> Deleted.PatientIdentified
end

-------------------  IncisionSiteIdentified  -----------------------------------------------------

if update(IncisionSiteIdentified)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'IncisionSiteIdentified', Deleted.IncisionSiteIdentified, Inserted.IncisionSiteIdentified
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.IncisionSiteIdentified <> Deleted.IncisionSiteIdentified
end

-------------------  TeamMembersIntroduced  -----------------------------------------------------

if update(TeamMembersIntroduced)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'TeamMembersIntroduced', Deleted.TeamMembersIntroduced, Inserted.TeamMembersIntroduced
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.TeamMembersIntroduced <> Deleted.TeamMembersIntroduced
end

-------------------  ProphylaxisMedicationGiven   -----------------------------------------------------

if update(ProphylaxisMedicationGiven )
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'ProphylaxisMedicationGiven ', Deleted.ProphylaxisMedicationGiven , Inserted.ProphylaxisMedicationGiven 
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.ProphylaxisMedicationGiven  <> Deleted.ProphylaxisMedicationGiven 
end

-------------------  CriticalNonRoutineSteps  -----------------------------------------------------

if update(CriticalNonRoutineSteps)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'CriticalNonRoutineSteps', Deleted.CriticalNonRoutineSteps, Inserted.CriticalNonRoutineSteps
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.CriticalNonRoutineSteps <> Deleted.CriticalNonRoutineSteps
end

-------------------  AnticipatedDuration  -----------------------------------------------------

if update(AnticipatedDuration)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'AnticipatedDuration', Deleted.AnticipatedDuration, Inserted.AnticipatedDuration
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.AnticipatedDuration <> Deleted.AnticipatedDuration
end

-------------------  AnticipatedBloodLoss  -----------------------------------------------------

if update(AnticipatedBloodLoss)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'AnticipatedBloodLoss', Deleted.AnticipatedBloodLoss, Inserted.AnticipatedBloodLoss
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.AnticipatedBloodLoss <> Deleted.AnticipatedBloodLoss
end

-------------------  PatientConcerns  -----------------------------------------------------

if update(PatientConcerns)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'PatientConcerns', Deleted.PatientConcerns, Inserted.PatientConcerns
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.PatientConcerns <> Deleted.PatientConcerns
end

-------------------  SterilityConfirmed  -----------------------------------------------------

if update(SterilityConfirmed)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'SterilityConfirmed', Deleted.SterilityConfirmed, Inserted.SterilityConfirmed
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.SterilityConfirmed <> Deleted.SterilityConfirmed
end

-------------------  OtherConcerns  -----------------------------------------------------

if update(OtherConcerns)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'OtherConcerns', Deleted.OtherConcerns, Inserted.OtherConcerns
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.OtherConcerns <> Deleted.OtherConcerns
end

-------------------  IsEssentialImageDisplayed  -----------------------------------------------------

if update(IsEssentialImageDisplayed)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'IsEssentialImageDisplayed', Deleted.IsEssentialImageDisplayed, Inserted.IsEssentialImageDisplayed
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.IsEssentialImageDisplayed <> Deleted.IsEssentialImageDisplayed
end

-------------------  LoginID  -----------------------------------------------------

if update(LoginID)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'LoginID', Deleted.LoginID, Inserted.LoginID
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.LoginID <> Deleted.LoginID
end

-------------------  ClientMachine  -----------------------------------------------------

if update(ClientMachine)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'ClientMachine', Deleted.ClientMachine, Inserted.ClientMachine
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.ClientMachine <> Deleted.ClientMachine
end

-------------------  RecordDateTime  -----------------------------------------------------

if update(RecordDateTime)
begin
	insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
	select @Identity, 'RecordDateTime', dbo.FormatDateTime(Deleted.RecordDateTime), dbo.FormatDateTime(Inserted.RecordDateTime)
	from Inserted inner join Deleted
	on Inserted.TheatreNo = Deleted.TheatreNo
	and Inserted.RecordDateTime <> Deleted.RecordDateTime
end
go


------------------------------------------------------------------------------------------------------
-------------- Delete: PreSurgicalCheckList ----------------------------------------------------------
------------------------------------------------------------------------------------------------------

if exists (select * from sysobjects where name = 'utrDeletePreSurgicalCheckList')
	drop trigger utrDeletePreSurgicalCheckList
go

create trigger utrDeletePreSurgicalCheckList
on PreSurgicalCheckList
for delete
as
declare @Identity int
declare @ObjectName varchar(40)
declare @FullDate smalldatetime

if @@rowcount = 0 return

------------------- Log Deletes ----------------------------------------------

if ident_current('AuditTrail') <>  @@identity return
set @Identity = ident_current('AuditTrail')

if @Identity is null return

set @ObjectName = (select ObjectName from AuditTrail where AuditID = @Identity and AuditAction = 'D')
if @ObjectName is null return
if @ObjectName <> 'PreSurgicalCheckList' return

set @FullDate = (select FullDate from AuditTrail where AuditID = @Identity)
if @FullDate is null return
if datediff(second, @FullDate, getdate()) > 60 return

-------------------  TheatreNo  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'TheatreNo', Deleted.TheatreNo, null from Deleted

-------------------  PatientIdentified  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'PatientIdentified', Deleted.PatientIdentified, null from Deleted

-------------------  IncisionSiteIdentified  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'IncisionSiteIdentified', Deleted.IncisionSiteIdentified, null from Deleted

-------------------  TeamMembersIntroduced  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'TeamMembersIntroduced', Deleted.TeamMembersIntroduced, null from Deleted

-------------------  ProphylaxisMedicationGiven   -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'ProphylaxisMedicationGiven ', Deleted.ProphylaxisMedicationGiven , null from Deleted

-------------------  CriticalNonRoutineSteps  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'CriticalNonRoutineSteps', Deleted.CriticalNonRoutineSteps, null from Deleted

-------------------  AnticipatedDuration  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'AnticipatedDuration', Deleted.AnticipatedDuration, null from Deleted

-------------------  AnticipatedBloodLoss  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'AnticipatedBloodLoss', Deleted.AnticipatedBloodLoss, null from Deleted

-------------------  PatientConcerns  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'PatientConcerns', Deleted.PatientConcerns, null from Deleted

-------------------  SterilityConfirmed  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'SterilityConfirmed', Deleted.SterilityConfirmed, null from Deleted

-------------------  OtherConcerns  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'OtherConcerns', Deleted.OtherConcerns, null from Deleted

-------------------  IsEssentialImageDisplayed  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'IsEssentialImageDisplayed', Deleted.IsEssentialImageDisplayed, null from Deleted

-------------------  LoginID  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'LoginID', Deleted.LoginID, null from Deleted

-------------------  ClientMachine  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'ClientMachine', Deleted.ClientMachine, null from Deleted

-------------------  RecordDateTime  -----------------------------------------------------
insert AuditTrailDetails (AuditID, ColumnName, OriginalValue, NewValue)
select @Identity, 'RecordDateTime', dbo.FormatDateTime(Deleted.RecordDateTime), null from Deleted
go

