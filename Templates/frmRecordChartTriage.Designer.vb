
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRecordChartTriage : Inherits System.Windows.Forms.Form

'Form overrides dispose to clean up the component list.
<System.Diagnostics.DebuggerNonUserCode()> _
Protected Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing AndAlso components IsNot Nothing Then
		components.Dispose()
	End If
	MyBase.Dispose(disposing)
End Sub

'Required by the Windows Form Designer
Private components As System.ComponentModel.IContainer

'NOTE: The following procedure is required by the Windows Form Designer
'It can be modified using the Windows Form Designer.
'Do not modify it using the code editor.
<System.Diagnostics.DebuggerStepThrough()> _
Private Sub InitializeComponent()
Me.fbnSearch = New SyncSoft.Common.Win.Controls.FlatButton
Me.fbnDelete = New SyncSoft.Common.Win.Controls.FlatButton
Me.ebnSaveUpdate = New SyncSoft.Common.Win.Controls.EditButton
Me.fbnClose = New SyncSoft.Common.Win.Controls.FlatButton
Me.stbRecordChartNo = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblRecordChartNo = New System.Windows.Forms.Label
Me.stbVitalID = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblVitalID = New System.Windows.Forms.Label
Me.stbValue = New SyncSoft.Common.Win.Controls.SmartTextBox
Me.lblValue = New System.Windows.Forms.Label
Me.dtpRecordDateTime = New System.Windows.Forms.DateTimePicker
Me.lblRecordDateTime = New System.Windows.Forms.Label
Me.SuspendLayout()
'
'stbRecordChartNo
'
Me.stbRecordChartNo.Location = New System.Drawing.Point(218, 12)
Me.stbRecordChartNo.Size = New System.Drawing.Size(170, 20)
Me.stbRecordChartNo.Name = "stbRecordChartNo"
'
'lblRecordChartNo
'
Me.lblRecordChartNo.Location = New System.Drawing.Point(12, 12)
Me.lblRecordChartNo.Size = New System.Drawing.Size(200, 20)
Me.lblRecordChartNo.Name = "lblRecordChartNo"
Me.lblRecordChartNo.Text = "Record Chart No"
'
'stbVitalID
'
Me.stbVitalID.Location = New System.Drawing.Point(218, 35)
Me.stbVitalID.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbVitalID, "VitalID")
Me.stbVitalID.Name = "stbVitalID"
'
'lblVitalID
'
Me.lblVitalID.Location = New System.Drawing.Point(12, 35)
Me.lblVitalID.Size = New System.Drawing.Size(200, 20)
Me.lblVitalID.Name = "lblVitalID"
Me.lblVitalID.Text = "Vital ID"
'
'stbValue
'
Me.stbValue.Location = New System.Drawing.Point(218, 58)
Me.stbValue.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.stbValue, "Value")
Me.stbValue.Name = "stbValue"
'
'lblValue
'
Me.lblValue.Location = New System.Drawing.Point(12, 58)
Me.lblValue.Size = New System.Drawing.Size(200, 20)
Me.lblValue.Name = "lblValue"
Me.lblValue.Text = "Value"
'
'dtpRecordDateTime
'
Me.dtpRecordDateTime.Location = New System.Drawing.Point(218, 81)
Me.dtpRecordDateTime.Size = New System.Drawing.Size(170, 20)
Me.ebnSaveUpdate.SetDataMember(Me.dtpRecordDateTime, "RecordDateTime")
Me.dtpRecordDateTime.Name = "dtpRecordDateTime"
Me.dtpRecordDateTime.ShowCheckBox = True
Me.dtpRecordDateTime.Checked = False
'
'lblRecordDateTime
'
Me.lblRecordDateTime.Location = New System.Drawing.Point(12, 81)
Me.lblRecordDateTime.Size = New System.Drawing.Size(200, 20)
Me.lblRecordDateTime.Name = "lblRecordDateTime"
Me.lblRecordDateTime.Text = "Record Date Time"
'
'fbnSearch
'
Me.fbnSearch.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnSearch.Location = New System.Drawing.Point(17, 111)
Me.fbnSearch.Name = "fbnSearch"
Me.fbnSearch.Size = New System.Drawing.Size(77, 23)
Me.fbnSearch.Text = "S&earch"
Me.fbnSearch.UseVisualStyleBackColor = True
Me.fbnSearch.Visible = False
'
'fbnDelete
'
Me.fbnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnDelete.Location = New System.Drawing.Point(316, 111)
Me.fbnDelete.Name = "fbnDelete"
Me.fbnDelete.Size = New System.Drawing.Size(72, 24)
Me.fbnDelete.Tag = "RecordChartTriage"
Me.fbnDelete.Text = "&Delete"
Me.fbnDelete.UseVisualStyleBackColor = False
Me.fbnDelete.Visible = False
'
'ebnSaveUpdate
'
Me.ebnSaveUpdate.ButtonText = SyncSoft.Common.Win.Controls.ButtonCaption.Save
Me.ebnSaveUpdate.DataSource = Nothing
Me.ebnSaveUpdate.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.ebnSaveUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.ebnSaveUpdate.Location = New System.Drawing.Point(17, 138)
Me.ebnSaveUpdate.Name = "ebnSaveUpdate"
Me.ebnSaveUpdate.Size = New System.Drawing.Size(77, 23)
Me.ebnSaveUpdate.Tag = "RecordChartTriage"
Me.ebnSaveUpdate.Text = "&Save"
Me.ebnSaveUpdate.UseVisualStyleBackColor = False
'
'fbnClose
'
Me.fbnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
Me.fbnClose.FlatAppearance.BorderColor = System.Drawing.Color.DarkBlue
Me.fbnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.fbnClose.Location = New System.Drawing.Point(316, 138)
Me.fbnClose.Name = "fbnClose"
Me.fbnClose.Size = New System.Drawing.Size(72, 24)
Me.fbnClose.Text = "&Close"
Me.fbnClose.UseVisualStyleBackColor = False
'
'frmRecordChartTriage
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.CancelButton = Me.fbnClose
Me.ClientSize = New System.Drawing.Size(415,  188)
Me.Controls.Add(Me.fbnSearch)
Me.Controls.Add(Me.fbnDelete)
Me.Controls.Add(Me.ebnSaveUpdate)
Me.Controls.Add(Me.fbnClose)
Me.Controls.Add(Me.stbRecordChartNo)
Me.Controls.Add(Me.lblRecordChartNo)
Me.Controls.Add(Me.stbVitalID)
Me.Controls.Add(Me.lblVitalID)
Me.Controls.Add(Me.stbValue)
Me.Controls.Add(Me.lblValue)
Me.Controls.Add(Me.dtpRecordDateTime)
Me.Controls.Add(Me.lblRecordDateTime)
Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
Me.KeyPreview = True
Me.MaximizeBox = False
Me.Name = "frmRecordChartTriage"
Me.Text = "RecordChartTriage"
Me.ResumeLayout(False)
Me.PerformLayout()

End Sub

Friend WithEvents fbnSearch As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents fbnDelete As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents ebnSaveUpdate As SyncSoft.Common.Win.Controls.EditButton
Friend WithEvents fbnClose As SyncSoft.Common.Win.Controls.FlatButton
Friend WithEvents stbRecordChartNo As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblRecordChartNo As System.Windows.Forms.Label
Friend WithEvents stbVitalID As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblVitalID As System.Windows.Forms.Label
Friend WithEvents stbValue As SyncSoft.Common.Win.Controls.SmartTextBox
Friend WithEvents lblValue As System.Windows.Forms.Label
Friend WithEvents dtpRecordDateTime As System.Windows.Forms.DateTimePicker
Friend WithEvents lblRecordDateTime As System.Windows.Forms.Label

End Class