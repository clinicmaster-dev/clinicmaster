
Public Class TheatreAdmissions : Inherits DBConnect

#Region " Fields "

#End Region

#Region " Auto-Implemented Properties "

Public Property TheatreID As Integer
Public Property TheatreNo As String
Public Property VisitNo As String
Public Property ProcedureCode As String
Public Property AdmissionDateTime As Date
Public Property OperationTypeID As String
Public Property OperationScheduleID As String
Public Property InformedConsentID As String
Public Property InformedConsentNotes As String
Public Property AdmissionStatusID As String
Public Property StaffNo As String
Public Property RecordDateTime As Date

#End Region

#Region " Constructors "

Public Sub New()
	MyBase.New()
End Sub

Public Sub New(ByVal serverName As String, ByVal databaseName As String)
	MyClass.New()
	Me.ServerName = serverName
	Me.DatabaseName = databaseName
End Sub

#End Region

#Region " Methods "

Protected Overrides Function SaveData() As ArrayList

	Me.SetCommand("uspInsertTheatreAdmissions")

	With Parameters
		.Add(New ParameterSQL("TheatreID", Me.TheatreID))
		.Add(New ParameterSQL("TheatreNo", Me.TheatreNo))
		.Add(New ParameterSQL("VisitNo", Me.VisitNo))
		.Add(New ParameterSQL("ProcedureCode", Me.ProcedureCode))
		.Add(New ParameterSQL("AdmissionDateTime", Me.AdmissionDateTime))
		.Add(New ParameterSQL("OperationTypeID", Me.OperationTypeID))
		.Add(New ParameterSQL("OperationScheduleID", Me.OperationScheduleID))
		.Add(New ParameterSQL("InformedConsentID", Me.InformedConsentID))
		.Add(New ParameterSQL("InformedConsentNotes", Me.InformedConsentNotes))
		.Add(New ParameterSQL("AdmissionStatusID", Me.AdmissionStatusID))
		.Add(New ParameterSQL("StaffNo", Me.StaffNo))
		.Add(New ParameterSQL("LoginID", Me.LoginID))
		.Add(New ParameterSQL("ClientMachine", My.Computer.Name))
		.Add(New ParameterSQL("RecordDateTime", Me.RecordDateTime))
	End With

	Return Parameters

End Function

Protected Overrides Function UpdateData() As ArrayList

	Me.SetCommand("uspUpdateTheatreAdmissions")

	With Parameters
		.Add(New ParameterSQL("TheatreID", Me.TheatreID))
		.Add(New ParameterSQL("TheatreNo", Me.TheatreNo))
		.Add(New ParameterSQL("VisitNo", Me.VisitNo))
		.Add(New ParameterSQL("ProcedureCode", Me.ProcedureCode))
		.Add(New ParameterSQL("AdmissionDateTime", Me.AdmissionDateTime))
		.Add(New ParameterSQL("OperationTypeID", Me.OperationTypeID))
		.Add(New ParameterSQL("OperationScheduleID", Me.OperationScheduleID))
		.Add(New ParameterSQL("InformedConsentID", Me.InformedConsentID))
		.Add(New ParameterSQL("InformedConsentNotes", Me.InformedConsentNotes))
		.Add(New ParameterSQL("AdmissionStatusID", Me.AdmissionStatusID))
		.Add(New ParameterSQL("StaffNo", Me.StaffNo))
		.Add(New ParameterSQL("LoginID", Me.LoginID))
		.Add(New ParameterSQL("ClientMachine", My.Computer.Name))
		.Add(New ParameterSQL("RecordDateTime", Me.RecordDateTime))
	End With

	'For Audit Trail
	Me.SetLogObject("TheatreAdmissions")

	Return Parameters

End Function

Protected Overrides Function DeleteData() As ArrayList

	Dim where As String = "TheatreNo = '" + Me.TheatreNo + "' and VisitNo = '" + Me.VisitNo + "' and ProcedureCode = '" + Me.ProcedureCode + "'
	Dim errorPart As String = "Theatre No: " + Me.TheatreNo + " and Visit No: " + Me.VisitNo + " and Procedure Code: " + Me.ProcedureCode

	Me.SetCommand("uspDeleteObject")

	With Parameters
		.Add(New ParameterSQL("ObjectName", "TheatreAdmissions"))
		.Add(New ParameterSQL("Where", where))
		.Add(New ParameterSQL("ErrorPart", errorPart))
	End With

	'For Audit Trail
	Me.SetLogObject("TheatreAdmissions")

	Return Parameters

End Function

Public Function GetTheatreAdmissions(ByVal theatreNo As String, ByVal visitNo As String, ByVal procedureCode As String) As DataSet

	With Parameters
		.Add(New ParameterSQL("TheatreNo", theatreNo))
		.Add(New ParameterSQL("VisitNo", visitNo))
		.Add(New ParameterSQL("ProcedureCode", procedureCode))
	End With

	Return Me.Load("uspGetTheatreAdmissions", "TheatreAdmissions", Parameters)

End Function

#End Region



End Class