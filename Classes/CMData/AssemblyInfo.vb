Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Clinic Master Data")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("ClinicMaster INTERNATIONAL")> 
<Assembly: AssemblyProduct("")> 
<Assembly: AssemblyCopyright("Copyright Clinic Master �  2019")> 
<Assembly: AssemblyTrademark("ClinicMaster INTERNATIONAL, All Rights Reserved")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("356BED40-7241-47FB-8BB4-9290E54EE1CB")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("2.0.0.0")> 
