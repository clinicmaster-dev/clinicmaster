if exists (select * from sysobjects where name = 'INTExtraBillItems')
	drop table INTExtraBillItems
go

if exists (select * from sysobjects where name = 'INTItems')
	drop table INTItems
go

if exists (select * from sysobjects where name = 'INTAdmissions')
	drop table INTAdmissions
go

if exists (select * from sysobjects where name = 'INTVisits')
	drop table INTVisits
go


create table INTVisits
(AgentNo varchar(20) not null
constraint fkAgentNoINTVisits references INTAgents (AgentNo),
VisitNo varchar(20) not null
constraint fkVisitNoINTVisits references Visits (VisitNo),
constraint pkAgentNoVisitNo primary key(AgentNo, VisitNo),
MemberLimit money,
BillMode varchar(100),
BillNo varchar(20),
InsuranceNo varchar(20),
MemberCardNo varchar(30),
MainMemberName varchar(41),
ClaimReferenceNo varchar(30),
VisitDate smalldatetime,
SyncStatus bit constraint dfSyncStatusINTVisits default 1,
UserID varchar(100) constraint dfUserIDINTVisits default System_User,
ClientMachine varchar(40) constraint dfClientMachineINTVisits default host_name(),
RecordDateTime smalldatetime constraint dfRecordDateTimeINTVisits default getdate()
)
go

if exists (select * from sysobjects where name = 'INTAdmissions')
	drop table INTAdmissions
go

create table INTAdmissions
(AgentNo varchar(20) not null
constraint fkAgentNoINTAdmissions references INTAgents (AgentNo),
AdmissionNo varchar(20) not null
constraint fkAdmissionNoINTAdmissions references Admissions (AdmissionNo),
constraint pkAgentNoAdmissionNo primary key(AgentNo, AdmissionNo),
MemberLimit money,
AdmissionDateTime smalldatetime,
BillMode varchar(100),
BillNo varchar(20),
InsuranceNo varchar(20),
MemberCardNo varchar(30),
MainMemberName varchar(41),
ClaimReferenceNo varchar(30),
SyncStatus bit constraint dfSyncStatusINTAdmissions default 1,
UserID varchar(20) constraint dfUserIDINTAdmissions default System_User,
ClientMachine varchar(41) constraint dfClientMachineINTAdmissions default host_name(),
RecordDateTime smalldatetime constraint dfRecordDateTimeINTAdmissions default getdate()
)
go

create table INTItems
(AgentNo varchar(20) not null,
VisitNo varchar(20) not null,
ItemCode varchar(20) not null,
ItemCategoryID varchar(10) not null,
constraint fkItemCategoryIDINTItemsVisitNoItemCodeItemCategoryID foreign key(VisitNo, ItemCode, ItemCategoryID) references Items (VisitNo, ItemCode, ItemCategoryID),
constraint pkAgentNoVisitNoItemCodeItemCategoryID primary key(AgentNo, VisitNo, ItemCode, ItemCategoryID),
ItemCategory varchar(100),
ItemName varchar(800),
OriginalQuantity int,
OriginalPrice money,
InvoiceNo varchar(20),
SyncStatus bit constraint dfSyncStatusINTItems default 1,
UserID varchar(100) constraint dfUserIDINTItems default System_User,
ClientMachine varchar(40) constraint dfClientMachineINTItems default host_name(),
RecordDateTime smalldatetime constraint dfRecordDateTimeINTItems default getdate()
)
go

create table INTExtraBillItems
(AgentNo varchar(20) not null
constraint fkAgentNoINTExtraBillItems references INTAgents (AgentNo),
ExtraBillNo varchar(20) not null,
ItemCode varchar(20) not null,
ItemCategoryID varchar(10) not null,
constraint fkExtraBillNoItemCodeItemCategoryIDINTExtraBillItems foreign key (ExtraBillNo, ItemCode, ItemCategoryID) references ExtraBillItems (ExtraBillNo, ItemCode, ItemCategoryID),
constraint pkAgentNoExtraBillNoItemCodeItemCategoryID primary key(AgentNo, ExtraBillNo, ItemCode, ItemCategoryID),
ItemName varchar(800),
BillMode varchar(100),
BillNo varchar(20),
BillToCustomerNo varchar(20),
BillToCustomerName varchar(41),
OriginalQuantity int,
OriginalPrice money,
ExtraBillRecordDateTime smalldatetime,
SyncStatus bit constraint dfSyncStatusINTExtraBillItems default 1,
UserID varchar(20) constraint dfUserIDINTExtraBillItems default System_User,
ClientMachine varchar(41) constraint dfClientMachineINTExtraBillItems default host_name(),
RecordDateTime smalldatetime constraint dfRecordDateTimeINTExtraBillItems default getdate()
)
go
