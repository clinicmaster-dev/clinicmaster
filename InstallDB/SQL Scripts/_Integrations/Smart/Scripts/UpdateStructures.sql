use XXMaster
go
create table INTVisits
(AgentNo varchar(20) not null
constraint fkAgentNoINTVisits references INTAgents (AgentNo),
VisitNo varchar(20) not null
constraint fkVisitNoINTVisits references Visits (VisitNo),
constraint pkAgentNoVisitNo primary key(AgentNo, VisitNo),
MemberLimit money,
BillMode varchar(100),
BillNo varchar(20),
InsuranceNo varchar(20),
MemberCardNo varchar(30),
MainMemberName varchar(41),
ClaimReferenceNo varchar(30),
VisitDate smalldatetime,
SyncStatus bit constraint dfSyncStatusINTVisits default 1,
UserID varchar(100) constraint dfUserIDINTVisits default System_User,
ClientMachine varchar(40) constraint dfClientMachineINTVisits default host_name(),
RecordDateTime smalldatetime constraint dfRecordDateTimeINTVisits default getdate()
)
go

alter table INTAdmissions add
BillMode varchar(100),
BillNo varchar(20),
InsuranceNo varchar(20),
MemberCardNo varchar(30),
MainMemberName varchar(41),
ClaimReferenceNo varchar(30)
go


create table INTItems
(AgentNo varchar(20) not null,
VisitNo varchar(20) not null,
ItemCode varchar(20) not null,
ItemCategoryID varchar(10) not null,
constraint fkItemCategoryIDINTItemsVisitNoItemCodeItemCategoryID foreign key(VisitNo, ItemCode, ItemCategoryID) 
references Items (VisitNo, ItemCode, ItemCategoryID),
constraint pkAgentNoVisitNoItemCodeItemCategoryID primary key(AgentNo, VisitNo, ItemCode, ItemCategoryID),
ItemCategory varchar(100),
ItemName varchar(800),
OriginalQuantity int,
OriginalPrice money,
InvoiceNo varchar(20),
SyncStatus bit constraint dfSyncStatusINTItems default 1,
UserID varchar(100) constraint dfUserIDINTItems default System_User,
ClientMachine varchar(40) constraint dfClientMachineINTItems default host_name(),
RecordDateTime smalldatetime constraint dfRecordDateTimeINTItems default getdate()
)
go

alter table INTExtraBillItems add
ItemName varchar(800),
BillMode varchar(100),
BillNo varchar(20),
BillToCustomerNo varchar(20),
BillToCustomerName varchar(41),
OriginalQuantity int,
OriginalPrice money,
ExtraBillRecordDateTime smalldatetime
go

exec sp_rename 'INTAdmissions.BillToCustomer', 'BillNo', 'COLUMN'
go