﻿/*****************************************************************************************************
This Script is a property of ClinicMaster INTERNATIONAL
Un authorised use or ammendment is not permitted
-- Cancel Processing and Pending Inventory Order Details
******************************************************************************************************/

use ClinicMaster
go


declare @PendingItemStatusID varchar(10)
declare @ProcessingItemStatusID varchar(10)
declare @CancelledItemStatusID varchar(10)

set @PendingItemStatusID = dbo.GetLookupDataID('ItemStatus', 'P')
set @ProcessingItemStatusID = dbo.GetLookupDataID('ItemStatus', 'R')
set @CancelledItemStatusID = dbo.GetLookupDataID('ItemStatus', 'C')

Update InventoryOrderDetails set ItemStatusID = @CancelledItemStatusID 
where ItemStatusID = @PendingItemStatusID

Update InventoryOrderDetails set ItemStatusID = @CancelledItemStatusID 
where ItemStatusID = @ProcessingItemStatusID


--------run cursor below, but be very carefull--------------------------------------------------------------
declare @TransferNo varchar(20)
declare @IssuedID varchar(10)
declare @ItemCategoryID varchar(10)
declare @ItemCode varchar(20)

set @IssuedID = dbo.GetLookupDataID('StockType', 'I')

DECLARE InventoryTransferDetails_Cursor INSENSITIVE CURSOR FOR

SELECT TransferNo, ItemCategoryID, ItemCode FROM InventoryTransferDetails where StockStatusID = @IssuedID

OPEN InventoryTransferDetails_Cursor
FETCH NEXT FROM InventoryTransferDetails_Cursor INTO @TransferNo, @ItemCategoryID, @ItemCode
WHILE (@@FETCH_STATUS <> -1)
	BEGIN
		delete from InventoryTransferDetails where TransferNo = @TransferNo and ItemCategoryID =@ItemCategoryID and ItemCode =@ItemCode
  FETCH NEXT FROM InventoryTransferDetails_Cursor INTO @TransferNo, @ItemCategoryID, @ItemCode
	END
CLOSE InventoryTransferDetails_Cursor
deallocate InventoryTransferDetails_Cursor

----------------------------------------------------------------------------------
delete from InventoryTransfers where (select count(TransferNo) from ​InventoryTransferDetails) = 0
go


