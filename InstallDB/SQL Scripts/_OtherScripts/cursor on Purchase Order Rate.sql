
DECLARE @Rate money
DECLARE @VATValue money
DECLARE @PackID VARCHAR(10)
DECLARE @PackSize int
DECLARE @Quantity int
DECLARE @PurchaseOrderNo varchar(20)
DECLARE @ItemCategoryID VARCHAR(10)
DECLARE @ItemCode VARCHAR(20)
declare @Amount money
DECLARE @ConsumableID VARCHAR(10)
DECLARE @NAPackID VARCHAR(10)
declare @NewRate money


set @ConsumableID = dbo.GetLookupDataID('ItemCategory', 'C')
set @NAPackID = dbo.GetLookupDataID('Pack', '004')


DECLARE PurchaseOrderRateCalculation INSENSITIVE CURSOR FOR

SELECT PurchaseOrderNo, ItemCategoryID, ItemCode, Rate, Amount, VATValue,PackID, PackSize, Quantity from PurchaseOrderDetails

OPEN PurchaseOrderRateCalculation
	FETCH NEXT FROM PurchaseOrderRateCalculation INTO @PurchaseOrderNo, @ItemCategoryID, @ItemCode, @Rate,@Amount, @VATValue, @PackID, @PackSize , @Quantity

WHILE (@@FETCH_STATUS <> -1)
	BEGIN
	
	if ((@ItemCategoryID = @ConsumableID))
		begin

		set	@NewRate = (@Amount - @VATValue)/ (@PackSize * @Quantity)

		 if  not (@NewRate = @Rate)
			 begin
				 UPDATE PurchaseOrderDetails SET Rate = @NewRate
				 WHERE PurchaseOrderNo = @PurchaseOrderNo AND ItemCategoryID = @ItemCategoryID AND ItemCode = @ItemCode
			 end
	
		end
	
		FETCH NEXT FROM PurchaseOrderRateCalculation INTO @PurchaseOrderNo, @ItemCategoryID, @ItemCode, @Rate,@Amount, @VATValue, @PackID, @PackSize , @Quantity
	END

CLOSE PurchaseOrderRateCalculation
DEALLOCATE PurchaseOrderRateCalculation

