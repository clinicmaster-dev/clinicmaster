
/***************************************************************
This Script is a property of ClinicMaster INTERNATIONAL
Un authorised use or ammendment is not permitted
-- Last updated 10/02/2016 by Wilson Kutegeka
***************************************************************/

use ClinicMaster
go

------------------------------------ Options ---------------------------------------------------------
-------------------- Option Starts with A ------------------------------------------------------------	
exec uspEditOptions 'ActivePatientMonths', 24, '3NUM', 2, 0
exec uspEditOptions 'AdmissionMaxDays', 30, '3NUM', 2, 0
exec uspEditOptions 'AllowAccessCashConsultation', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowAccessCashDischarges', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowAccessCashServices', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowAccessCoPayServices', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowAccessOPDTheatre', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowCreateInvoicesAtCashPayments', 0, '3BIT', 1, 0
exec uspEditOptions 'AccountIdleMonthDuration', 6, '3NUM', 2, 0
exec uspEditOptions 'AllowCreateMultipleExtraBills', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowCreateMultipleSpecialityVisits', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowCreateMultipleVisitInvoices', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowCreateMultipleVisits', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowCreditSelfRequests', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowCustomAdmissionNoFormat', 0, '3BIT', 1, 1
exec uspEditOptions 'AllowDirectDebitBalanceEntry', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowDirectDiscountCashPayment', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowDispensingExpiredConsumables', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowDispensingExpiredDrugs', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowDispensingToNegative', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowDrugsServiceFee', 0, '3BIT', 1, 1
exec uspEditOptions 'AllowExtendedVisitEdits', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowExtraBillInventoryIssuing', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowGenerateSelfRequestsNo', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowInsuranceDirectLinkedMember', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowInventoryManualIssuing', 1, '3BIT', 1, 0
exec uspEditOptions 'AllowIssueStockOnLabRequest', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowLocationIssuingToNegative', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowManualAccountDebitEntry', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowManualIssuingToNegative', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowNetworkMultipleLogins', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowPartialCashPayment', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowPrescriptionExpiredConsumables', 1, '3BIT', 1, 0
exec uspEditOptions 'AllowPrescriptionExpiredDrugs', 1, '3BIT', 1, 0
exec uspEditOptions 'AllowPrescriptionToNegative', 1, '3BIT', 1, 0
exec uspEditOptions 'AllowPrintingAdmissionFaceSheet', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowPrintingAtDischarge',1, '3BIT', 1, 0
exec uspEditOptions 'AllowPrintingBeforeDispensing', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowPrintingPatientBioData', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowPrintingPatientFaceSheet', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowPrintingPatientFaceSheet', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowProcessingPendingItems', 1, '3BIT', 1, 0
exec uspEditOptions 'AllowProvisionalPrinting', 0, '3BIT', 1, 0
exec uspEditOptions 'AllowSmartCardApplicableVisit', 1, '3BIT', 1, 0
exec uspEditOptions 'ANCNoPrefix', 'ANC', '3STR', 20, 1
exec uspEditOptions 'AutoRenewSchemeMembers', 0, '3BIT', 1, 0
go

-------------------- Option Starts with B ------------------------------------------------------------	
exec uspEditOptions 'BedNoPrefix', 'BED', '3STR', 20, 1
go

-------------------- Option Starts with c ------------------------------------------------------------	
exec uspEditOptions 'CaptureAndUseBarCodes', 0, '3BIT', 1, 0
exec uspEditOptions 'CardiologyExamCodePrefix', 'CAR', '3STR', 20, 1
exec uspEditOptions 'CashPaymentPercentBeforeAdmission', 0, '3NUM', 3, 0
exec uspEditOptions 'CategorizeVisitPaymentDetails', 0, '3BIT', 1, 0
exec uspEditOptions 'ConsumableNoPrefix', 'CON', '3STR', 20, 1
exec uspEditOptions 'Currency', 'UGX', '3STR', 10, 1
go

-------------------- Option Starts with D ------------------------------------------------------------	
exec uspEditOptions 'DecimalPlaces', 2, '3NUM', 1, 0
exec uspEditOptions 'DentalCodePrefix', 'DEN', '3STR', 20, 1
exec uspEditOptions 'DisableAppointments', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableCaradiology', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableCashier', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableConstraints', 0, '3BIT', 1, 1
exec uspEditOptions 'DisableDeaths', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableDental', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableDoctor', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableDosageAndDurationAtSelfRequest', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableExtraBillsBedBilling',0, '3BIT', 1, 1
exec uspEditOptions 'DisableExtras', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableFinance', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableInPatients', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableInvoices', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableLaboratory', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableManageART', 0, '3BIT', 1, 0
exec uspEditOptions 'DisablePathology', 0, '3BIT', 1, 0
exec uspEditOptions 'DisablePatientRegistration', 0, '3BIT', 1, 0
exec uspEditOptions 'DisablePatientSignOnInvoices', 0, '3BIT', 1, 0
exec uspEditOptions 'DisablePharmacy', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableRadiology', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableTheatre', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableTriagePoint', 0, '3BIT', 1, 0
exec uspEditOptions 'DisableVisitsCreation', 0, '3BIT', 1, 0
exec uspEditOptions 'DisallowPaymentOfOutStockDrugs', 0, '3BIT', 1, 0
exec uspEditOptions 'DoctorVisitUpdateDays', 14, '3NUM', 2, 0
exec uspEditOptions 'DrugNoPrefix', 'DRG', '3STR', 20, 1
go

-------------------- Option Starts with E ------------------------------------------------------------	
exec uspEditOptions 'EnableAccessCashServices', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableAuditTrail', 1, '3BIT', 1, 1
exec uspEditOptions 'EnableCentralisedSmartCardProcessing', 0, '3BIT', 1, 1
exec uspEditOptions 'EnableChartNotifications', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableCommentsAtPrintingLabReport', 1, '3BIT', 1, 0
exec uspEditOptions 'EnableInventoryPhysicalStockEntry', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableInvoiceDate',0, '3BIT', 1, 0
exec uspEditOptions 'EnableKwiksyPayments', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableMemberLimitBalanceTracking',0, '3BIT', 1, 0
exec uspEditOptions 'EnableOPDExtraBills', 0, '3BIT', 1, 1
exec uspEditOptions 'EnablePayDate',0, '3BIT', 1, 0
exec uspEditOptions 'EnablePriceAdjustments', 0, '3BIT', 1
exec uspEditOptions 'EnablePrintingInvoicesWithCompanyName', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableQueue', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableSecondPatientForm', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableSetAssociatedBillCustomer', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableSetInventoryLocation', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableUseOfInventoryPackSizes', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableSMSNotificationAtAppointments', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableSMSNotificationAtCardiology', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableSMSNotificationAtCashPayment', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableSMSNotificationAtLab', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableSMSNotificationAtManageAccounts', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableSMSNotificationAtPatientReg', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableSMSNotificationAtRadiology', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableSMSNotificationAtVisits', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableSMSNotificationForBirthdays', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableSMSNotificationForIncomeSummaries', 0, '3BIT', 1, 0
exec uspEditOptions 'EnableVisitDate',0, '3BIT', 1, 0
exec uspEditOptions 'EnableVisitToSeeDoctorSelection', 1, '3BIT', 1, 0
exec uspEditOptions 'EnforceCreditLimitOnBothOPDandIPD', 0, '3BIT', 1, 1
exec uspEditOptions 'ExpiryWarningDays', 60, '3NUM', 3, 0
exec uspEditOptions 'ExtraItemCodePrefix', 'EXT', '3STR', 20, 1
exec uspEditOptions 'EyeCodePrefix', 'EYE', '3STR', 20, 1
go

-------------------- Option Starts with F ------------------------------------------------------------	
exec uspEditOptions 'FingerprintCaptureAgeLimit', 3, '3NUM', 1, 0
exec uspEditOptions 'FingerprintDevice', '105DP', '3STR', 5, 1
exec uspEditOptions 'ForceAccountMainMemberName', 0, '3BIT', 1, 0
exec uspEditOptions 'ForceBillableMappings', 0, '3BIT', 1, 0
exec uspEditOptions 'ForceDiagnosisAtDoctor', 0, '3BIT', 1, 0
exec uspEditOptions 'ForceDiagnosisOnAdmission',1, '3BIT', 1, 0
exec uspEditOptions 'ForceDiagnosisOnPrescription', 0, '3BIT', 1, 0
exec uspEditOptions 'ForceDispensingPreviousPrescription', 0, '3BIT', 1, 0
exec uspEditOptions 'ForceFingerPrintOnSelfRequestLabReport', 0, '3BIT', 1, 0
exec uspEditOptions 'ForceInsuranceFingerprintVerification', 0, '3BIT', 1, 0
exec uspEditOptions 'ForceInventoryAcknowledgementBeforeOrdering', 0, '3BIT', 1, 0
exec uspEditOptions 'ForceLabResultsVerification', 0, '3BIT', 1, 0
exec uspEditOptions 'ForcePatientGeographicalLocation', 0, '3BIT', 1, 0
exec uspEditOptions 'ForceRefundsApproval', 1, '3BIT', 1, 0
exec uspEditOptions 'ForceTBAssessmentAtTriage', 1, '3BIT', 1, 0
go

-------------------- Option Starts with G ------------------------------------------------------------	
exec uspEditOptions 'GenerateInventoryInvoiceOnDispensingOnly', 0, '3BIT', 1, 0
go
exec uspEditOptions 'InventoryTransferReservePeriod', 24, '3NUM', 2, 0

-------------------- Option Starts with H ------------------------------------------------------------	
exec uspEditOptions 'HideAccessCashServicesAtVisits', 0, '3BIT', 1, 0
exec uspEditOptions 'HideBillFormDrugDetails', 0, '3BIT', 1, 0
exec uspEditOptions 'HideBillFormItemsPresentAtIPDDoctor', 0, '3BIT', 1, 0
exec uspEditOptions 'HideBillFormPaymentReceiptDetails', 0, '3BIT', 1, 0
exec uspEditOptions 'HideCashPaymentReceiptDetails', 0, '3BIT', 1, 0
exec uspEditOptions 'HideCashReceiptHeader', 0, '3BIT', 1, 0
exec uspEditOptions 'HideCreditBillFormPaymentReceiptDetails', 0, '3BIT', 1, 0
exec uspEditOptions 'HideCreditBillsPaymentReceiptDetails', 0, '3BIT', 1, 0
exec uspEditOptions 'HideDoctorBillServiceFee', 0, '3BIT', 1, 0
exec uspEditOptions 'HideInvoiceHeader', 0, '3BIT', 1, 0
go

-------------------- Option Starts with I ------------------------------------------------------------	
exec uspEditOptions 'ICUCodePrefix', 'ICU', '3STR', 20, 1
exec uspEditOptions 'ImmunisationNoPrefix', 'I', '3STR', 1, 1
exec uspEditOptions 'IncludeMonthsForAgesBelow', 3, '3NUM', 2, 0
exec uspEditOptions 'IncomeSummariesSMSTime(2)', '8:31:00 AM', '3STR', 20, 0
exec uspEditOptions 'IncomeSummariesSMSTime', '7:31:00 PM', '3STR', 20, 0
exec uspEditOptions 'IncorporateFingerprintCapture', 0, '3BIT', 1, 0
exec uspEditOptions 'InventoryAlertDays', 7, '3NUM', 2, 0
go
-------------------- Option Starts with L ------------------------------------------------------------
exec uspEditOptions 'LockItemsUnitPrices', 1, '3BIT', 1, 0
exec uspEditOptions 'LockVisitUponInvoiceCreation', 0, '3BIT', 1, 0
go
-------------------- Option Starts with M ------------------------------------------------------------	
exec uspEditOptions 'MaternityCodePrefix', 'MAT', '3STR', 20, 1
exec uspEditOptions 'MaximumConsumableQtyToGive', 200, '3NUM', 4, 0
exec uspEditOptions 'MaximumDrugQtyToGive', 100, '3NUM', 4, 0
exec uspEditOptions 'MaximumExtraChargeQtyToIssue', 10, '3NUM', 4, 0
exec uspEditOptions 'MedicalCardNoPrefix', '', '3STR', 10, 1
go
-------------------- Option Starts with N ------------------------------------------------------------	
exec uspEditOptions 'NullDateValue', '1 Jan 1900', '3DTE', 18, 1
go

-------------------- Option Starts with O ------------------------------------------------------------	
exec uspEditOptions 'OpenIPDDispenseAfterPrescription', 0, '3BIT', 1, 0
exec uspEditOptions 'OpenIPDTheatreIssueConsumablesAfterPrescription', 0, '3BIT', 1, 0
exec uspEditOptions 'OpenIssueConsumablesAfterPrescription', 0, '3BIT', 1, 0
exec uspEditOptions 'OpticalCodePrefix', 'OPT', '3STR', 20, 1
exec uspEditOptions 'OtherItemsPrefix', 'OTH', '3STR', 20, 1
go
-------------------- Option Starts with P ------------------------------------------------------------	
exec uspEditOptions 'PackageNoPrefix', 'PAC', '3STR', 20, 1
exec uspEditOptions 'PathologyExamCodePrefix', 'PAT', '3STR', 20, 1
exec uspEditOptions 'PatientNoPrefix', '', '3STR', 10, 1
exec uspEditOptions 'ProcedureCodePrefix', 'PRO', '3STR', 20, 1
go
-------------------- Option Starts with Q ------------------------------------------------------------	
exec uspEditOptions 'QueueReadCount', 3, '3NUM', 1, 0
go
-------------------- Option Starts with R ------------------------------------------------------------	
exec uspEditOptions 'RadiologyExamCodePrefix', 'RAD', '3STR', 20, 1
exec uspEditOptions 'ReceiptNoPrefix', '', '3STR', 10, 1
exec uspEditOptions 'RequisitionApprovalNoPrefix', 'APP', '3STR', 20, 1
exec uspEditOptions 'RequisitionNoPrefix', 'REQ', '3STR', 20, 1
exec uspEditOptions 'RequisitionPaymentNoPrefix', 'PAY', '3STR', 20, 1
exec uspEditOptions 'RestrictDoctorLoginID', 0, '3BIT', 1, 0
exec uspEditOptions 'RestrictDrawnByLoginID', 0, '3BIT', 1, 0
exec uspEditOptions 'RestrictLabTechnologistLoginID', 0, '3BIT', 1, 0
exec uspEditOptions 'RestrictPathologistLoginID', 0, '3BIT', 1, 0
exec uspEditOptions 'RestrictPharmacistLoginID', 0, '3BIT', 1, 0
exec uspEditOptions 'RestrictRadiologistLoginID', 0, '3BIT', 1, 0
go

-------------------- Option Starts with S ------------------------------------------------------------	
exec uspEditOptions 'SchemeMembersMaxDependants', 2, '3NUM', 2, 0
exec uspEditOptions 'SelfRequestNoPrefix', 'SR', '3STR', 10, 1
exec uspEditOptions 'SendSMSUsingAPI001', 0, '3BIT', 1, 0
exec uspEditOptions 'SendSMSUsingAPI002', 0, '3BIT', 1, 0
exec uspEditOptions 'SendSMSUsingAPI003', 0, '3BIT', 1, 0
exec uspEditOptions 'ServiceCodePrefix', 'SER', '3STR', 20, 1
exec uspEditOptions 'SmartCardConnectionAttemptNo', 10, '3NUM', 3, 0
exec uspEditOptions 'SmartCardServiceProviderNo', '1234567890ABC', '3STR', 20, 0
exec uspEditOptions 'SMSLifeSpanAppointments', '30', '3NUM', 2, 0
exec uspEditOptions 'SMSLifeSpanBirthDays', '5', '3NUM', 2, 0
exec uspEditOptions 'SMSLifeSpanCardiology', '30', '3NUM', 2, 0
exec uspEditOptions 'SMSLifeSpanCashier', '30', '3NUM', 2, 0
exec uspEditOptions 'SMSLifeSpanIncomeSummaries', '30', '3NUM', 2, 0
exec uspEditOptions 'SMSLifeSpanLab', '3', '3NUM', 2, 0
exec uspEditOptions 'SMSLifeSpanManageACCs', '30', '3NUM', 2, 0
exec uspEditOptions 'SMSLifeSpanPatientReg', '30', '3NUM', 2, 0
exec uspEditOptions 'UseCentralisedPhysicalStockCount', 0, '3BIT', 1, 0
exec uspEditOptions 'SMSLifeSpanPharmacy', '3', '3NUM', 2, 0
exec uspEditOptions 'SMSLifeSpanRadiology', '30', '3NUM', 2, 0
exec uspEditOptions 'SMSLifeSpanVisits', '30', '3NUM', 2, 0
exec uspEditOptions 'StaffNoPrefix', 'S', '3STR', 1, 1
go
-------------------- Option Starts with T ------------------------------------------------------------	
exec uspEditOptions 'TestCodePrefix', 'LAB', '3STR', 20, 1
exec uspEditOptions 'TheatreCodePrefix', 'THE', '3STR', 20, 1
go
-------------------- Option Starts with U ------------------------------------------------------------	
exec uspEditOptions 'UserIdleDuration', 30, '3NUM', 2, 0
go
-------------------- Option Starts with V ------------------------------------------------------------	
exec uspEditOptions 'VisitReviewDays', 0, '3NUM', 2, 0
go
-------------------- Option Starts with W ------------------------------------------------------------	
exec uspEditOptions 'WithdrawRequestExpiryHours', 12, '3NUM', 2, 0
go
-- select * from Options

------Services--------------------------------------------------------
exec uspInsertServices 'NA', 'NA','16VIS','30DOC', 0, 0,0,0
exec uspInsertServices '10C', 'Consultation','16VIS','30DOC', 0,0,20000,0
exec uspInsertServices 'HOS', 'Hospitalization','16DIS', '30DIS', 0,0,30000,0
exec uspInsertServices 'REV', 'Review','16VIS','30DOC', 0,0,10000,0
exec uspInsertServices 'PHY', 'Physiotherapy','16VIS','30VIS', 0, 0,20000,0
exec uspInsertServices 'MEX', 'Medical Examination','16VIS','30VIS', 0,0,20000,0
exec uspInsertServices 'NAADM', 'NA.','16ADM','30ADM', 0, 0,0,0
go
-- select * from services
-- delete from Services 

-------------- ExtraChargeItems ---------------------------------------------------------------

exec uspInsertExtraChargeItems 'CPV', 'CO-PAY VALUE', '145GEN', 0, 0,0,0
go

-- select * from ExtraChargeItems
-- delete from ExtraChargeItems

-------------- Locations ---------------------------------------------------------------
exec uspInsertLocations '117GR', '700GR', 'Administrator', host_name
go

-------------- MemberBenefits -----------------------------------------------------------------

exec uspInsertMemberBenefits '7S', 'Service', '7S'
exec uspInsertMemberBenefits '7D', 'Drug', '7D'
exec uspInsertMemberBenefits '7C', 'Consumable', '7C'
exec uspInsertMemberBenefits '7P', 'Procedure', '7P'
exec uspInsertMemberBenefits '7T', 'Test', '7T'
exec uspInsertMemberBenefits '7CA', 'Cardiology', '7CA'

exec uspInsertMemberBenefits '7R', 'Radiology', '7R'
exec uspInsertMemberBenefits '7N', 'Dental', '7N'
exec uspInsertMemberBenefits '7H', 'Theatre', '7H'
exec uspInsertMemberBenefits '7O', 'Optical', '7O'
exec uspInsertMemberBenefits '7M', 'Maternity', '7M'
exec uspInsertMemberBenefits '7I', 'ICU', '7I'
exec uspInsertMemberBenefits '7E', 'Extras', '7E'
exec uspInsertMemberBenefits '7Y', 'Eye', '7Y'
exec uspInsertMemberBenefits '7A', 'Admission', '7A'
exec uspInsertMemberBenefits '7OP', 'Out Patient'
exec uspInsertMemberBenefits '7IP', 'In Patient'
go

--------Bill Customers------------------------------------------------

exec uspInsertBillCustomers 'CASH', 'Cash Customer', '18IND', '', '', '', '', '', '', '', null, '', '', '44NA', 0, 0, 0, 0, 0, 0, 0, 0, 0, '1A'
exec uspInsertBillCustomers 'SUSP', 'SUSPENSE ACCOUNT', '18IND', '', '', '', '', '', '', '', null, '', '', '44NA', 0, 0, 0, 0, 0, 0, 0, 0, 1, '1A'
go

-- select * from BillCustomers where AccountNo = 'SIL'
-- delete from BillCustomers where AccountNo = 'SIL'
-- CDC
-- PEPFA
-- MOH Ministry of Health

--------DrugCategories------------------------------------------------

exec uspInsertDrugCategories '4ARVB','ARV BRAND',0,0,'X','31M',''
exec uspInsertDrugCategories '4ARVG','ARV GENERIC',0,0,'X','31M',''
exec uspInsertDrugCategories '4CRE','CREAMS',1,2,';','31A',''
exec uspInsertDrugCategories '4DRO','DROPS',0,0,'X','31M',''
exec uspInsertDrugCategories '4EYE','EYE OITMENT',0,0,'X','31M',''
exec uspInsertDrugCategories '4GEL','GEL',0,0,'X','31M',''
exec uspInsertDrugCategories '4INJ','INJECTABLES',0,0,'X','31M',''
exec uspInsertDrugCategories '4IV','I.V',0,0,'X','31M',''
exec uspInsertDrugCategories '4LOT','LOTION',0,0,'X','31M',''
exec uspInsertDrugCategories '4OIT','OITMENT',0,0,'X','31M',''
exec uspInsertDrugCategories '4PEN','PENCIL',0,0,'X','31M',''
exec uspInsertDrugCategories '4SHA','SHAMPOO',0,0,'X','31M',''
exec uspInsertDrugCategories '4SYR','SYRUPS',1,1,'X','31M',''
exec uspInsertDrugCategories '4TAB','TABLETS MULTIPLY',0,0,'X','31M',''
exec uspInsertDrugCategories '4TABA','TABLETS ADD',0,0,';','31A',''
go


-- select * from DrugCategories
-- delete from DrugCategories

--------ARV Drugs---------------------------------------------------------------------------------------------------------------

exec uspInsertDrugs 'A001','NEVIMUNE ORAL SOLUTION','','4ARVG','4ARVG','8NA',0,0,0,0,2000,'Jan 22 2010'
exec uspInsertDrugs 'A002','NEVIMUNE TAB 200MG BRAND','','4ARVG','4ARVG','8NA',0,0,0,0,14000,'Jan 22 2010'
exec uspInsertDrugs 'A002B','NEVIMUNE TAB 200MG','','4ARVG','4ARVG','8NA',0,0,0,0,6000,'Jan 22 2010'
exec uspInsertDrugs 'A003','DOUVIR','','4ARVG','4ARVG','8NA',0,0,0,0,20000,'Jan 22 2010'
exec uspInsertDrugs 'A003B','DUOVIR-N','','4ARVG','4ARVG','8NA',0,0,0,0,45000,'Jan 22 2010'
exec uspInsertDrugs 'A004','MAXIVIR TAB','','4ARVG','4ARVG','8NA',0,0,0,0,38000,'Jan 22 2010'
exec uspInsertDrugs 'A004B','TRIOMUNE TAB 40','','4ARVG','4ARVG','8NA',0,0,0,0,40000,'Jan 22 2010'
exec uspInsertDrugs 'A005','TRIOMUNE TAB 30','','4ARVG','4ARVG','8NA',0,0,0,0,42000,'Jan 22 2010'
exec uspInsertDrugs 'A005B','LAMIVUDINE TAB 150MG','','4ARVB','4ARVB','8NA',0,0,0,0,9000,'Jan 22 2010'
go
--------Other Drugs---------------------------------------------------------------------------------------------------------------
exec uspInsertDrugs 'M129', 'ACENAC- TAB100mg', '', '4TAB', '4TAB','8NA', 0, 0, 0,0, 500, 'Jan 22 2010'
exec uspInsertDrugs 'M429', 'ACTIFED DRY- SYP', '', '4SYR', '4SYR','8NA', 0, 0, 0,0, 10000, 'Jan 22 2010'
exec uspInsertDrugs 'M327', 'ACYCLOVIR- CRM15mg', '', '4CRE','4CRE', '8NA', 0, 0, 0,0, 10000, 'Jan 22 2010'
exec uspInsertDrugs 'M317', 'ADRENALIN- INJ', '', '4INJ','4INJ', '8NA', 0, 0, 0,0, 5000, 'Jan 22 2010'
exec uspInsertDrugs 'M609', 'B.B.E. LOTION- LOT', '', '4LOT','4LOT', '8NA', 0, 0, 0,0, 3000, 'Jan 22 2010'
exec uspInsertDrugs 'M341', 'BENZOX- GEL', '', '4GEL','4GEL', '8NA', 0, 0, 0,0, 10000, 'Jan 22 2010'
exec uspInsertDrugs 'M267', 'BETHAMETHASONE-DRP', '', '4DRO','4DRO', '8NA', 0, 0, 0,0, 3000, 'Jan 22 2010'
go

-- select * from drugs 
-- delete from drugs 

-------- ConsumableItems ---------------------------------------------------------------------------------------------------------------
exec uspInsertConsumableItems 'C001', 'Catheter foley CH 16 2 way', '', '8PC', 0, 0, 0,0, 3000, '27 Oct 2020',0,0,'324NA'
exec uspInsertConsumableItems 'C002', 'Catheter foley CH 18 2 way', '', '8PC', 0, 0, 0,0, 3000, '27 Oct 2020',0,0,'324NA'
exec uspInsertConsumableItems 'C003', 'Cotton wool 500g', '', '8PC', 0, 0, 0,0, 15000, '27 Oct 2020',0,0,'324NA'
exec uspInsertConsumableItems 'C004', 'Derivery kits( mama kit)', '', '8PC', 0, 0, 0,0, 30000, '27 Oct 2020',0,0,'324NA'
exec uspInsertConsumableItems 'C005', 'Surgical gloves 7.5', '', '8PC', 0, 0, 0,0, 1500, '27 Oct 2020',0,0,'324NA'
exec uspInsertConsumableItems 'C006', 'Insuline syringes', '', '8PC', 0, 0, 0,0, 500, '27 Oct 2020',0,0,'324NA'
go

--------Lab Tests-------------------------------------------------------------------------------------------------------------
exec uspInsertLabTests 'T002', 'ESR', '14BLD', '5H', '', '8NA', 0, 5000,0, '3STR',0,'','',''
exec uspInsertLabTests 'T001', 'FULL HAEMOGRAM(CBC)', '14BLD', '5H', '', '8NA', 0, 15000,0, '3DEC',0,'','',''
exec uspInsertLabTests 'T058', '24HR URINALYSIS', '14BLD', '5B', '', '8NA', 0, 45000,0, '3STR',0,'','',''
exec uspInsertLabTests 'T015', 'ABO AND RH BLOOD GROUP', '14BLD', '5H', '', '8NA', 0, 10000,0,'3STR',0,'','',''
exec uspInsertLabTests 'T019', 'MALARIA PARASITES-B/S', '14BLD', '5H', '', '8NA', 0, 5000,0, '3STR',0,'','',''
exec uspInsertLabTests 'T190', 'GRAM STAIN', '14PUS', '5M', '', '8NA', 0, 5000,0, '3STR',0,'','',''
go

---- select * from labTests
---- delete from labTests
--

-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------LabPossibleResults------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------

exec uspEditLabPossibleResults 'T019','MPS +'
exec uspEditLabPossibleResults 'T019','MPS ++'
exec uspEditLabPossibleResults 'T019','MPS +++'
exec uspEditLabPossibleResults 'T019','MPS ++++'
exec uspEditLabPossibleResults 'T019','NO MPS SEEN'
exec uspEditLabPossibleResults 'T190','Gram Positive Cocci (GPC)'
exec uspEditLabPossibleResults 'T190','Gram Positive Rods (GPR)'
exec uspEditLabPossibleResults 'T190','Gram Negative Cocci (GNC)'
exec uspEditLabPossibleResults 'T190','Gram Negative Rods (GNR)'
go

-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------LabTestsEXT-------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------
exec uspEditLabTestsEXT 'T001', 'GR%', 'GR%', '50-75', '8PCT', '3DEC'
exec uspEditLabTestsEXT 'T001', 'GRA', 'GRA', '2.5-7.5', '8103UL', '3DEC'
exec uspEditLabTestsEXT 'T001', 'HB', 'HB', '12-17.5', '8NA', '3DEC'
exec uspEditLabTestsEXT 'T001', 'HCT', 'HCT', '36-52', '8PCT', '3DEC'
exec uspEditLabTestsEXT 'T001', 'LY%', 'LY%', '25-40', '8PCT', '3DEC'
exec uspEditLabTestsEXT 'T001', 'LYM', 'LYM', '1.3-4.0', '8103UL', '3DEC'
exec uspEditLabTestsEXT 'T001', 'MCH', 'MCH', '27-32', '8NA', '3DEC'
exec uspEditLabTestsEXT 'T001', 'MCHC', 'MCHC', '30-35', '8NA', '3DEC'
exec uspEditLabTestsEXT 'T001', 'MCV', 'MCV', '76-96', '8NA', '3NUM'
exec uspEditLabTestsEXT 'T001', 'MON', 'MON', '0.15-0.70', '8103UL', '3DEC'
exec uspEditLabTestsEXT 'T001', 'MON%', 'MON%', '3.0-7.0', '8PCT', '3DEC'
exec uspEditLabTestsEXT 'T001', 'MPV', 'MPV', '8-15', '8NA', '3DEC'
exec uspEditLabTestsEXT 'T001', 'PCT', 'PCT', '', '8PCT', '3DEC'
exec uspEditLabTestsEXT 'T001', 'PDWC', 'PDWC', '', '8PCT', '3DEC'
exec uspEditLabTestsEXT 'T001', 'PLT', 'PLT', '150-400', '8103UL', '3NUM'
exec uspEditLabTestsEXT 'T001', 'RBC', 'RBC', '4.0-5.5', '8106UL', '3DEC'
exec uspEditLabTestsEXT 'T001', 'RDWC', 'RDWC', '', '8PCT', '3DEC'
exec uspEditLabTestsEXT 'T001', 'WBC', 'WBC', '05-10', '8103UL', '3DEC'
exec uspEditLabTestsEXT 'T002', '20YRS', '0-20', 'M 0-10, F 0-15', '8NA', '3STR'
exec uspEditLabTestsEXT 'T002', '55YRS', '20-55YEARS', 'M 0-27.5, F 0-32.5', '8NA', '3STR'
exec uspEditLabTestsEXT 'T002', '90YRS', '55-90', 'M 0-45, F 0-50', '8NA', '3STR'
go
---- select * from LabTestsEXT
---- delete from LabTestsEXT 

---------------------------------------------------------------------------------------------------------------------------------------------------
---------staff-------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------

exec uspInsertStaff	100, 'Private', 'Doctor', '15F', '20DR', '', '', '', '05 Jan 2013',  '', '', '39GEN', ''
exec uspInsertStaff	101, 'Private', 'Technologist', '15M', '20TEC', '', '', '', '05 Jan 2013',  '', ''
exec uspInsertStaff	102, 'Private', 'Pharmacist', '15F', '20PHA', '', '', '', '05 Jan 2013',  '', ''
exec uspInsertStaff	103, 'Private', 'Radiologist', '15M', '20RAD', '', '', '', '05 Jan 2013',  '', ''
exec uspInsertStaff	104, 'Private', 'Anaesthetist', '15M', '20ANA', '', '', '', '05 Jan 2013',  '', ''
exec uspInsertStaff	105, 'Private', 'Nurse', '15F', '20NUR', '', '', '', '05 Jan 2013',  '', ''
exec uspInsertStaff	106, 'Private', 'Anesthesiologist', '15F', '20ANE', '', '', '', '05 Sep 2018',  '', ''
exec uspInsertStaff	107, 'Private', 'Pathologist', '15F', '20PAT', '', '', '', '05 Sep 2018',  '', ''
exec uspInsertStaff	108, 'Private', 'Cardiologist', '15F', '20CAR', '', '', '', '05 Sep 2018',  '', ''
exec uspInsertStaff	109, 'Private', 'Therapist', '15F', '20THE', '', '', '', '05 Sep 2018',  '', ''
go

-- delete from staff

---------------------------------------------------------------------------------------------------------------------------------------------------
---------DrugCombinations--------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------

exec uspInsertDrugCombinations 'D4T+3TC+NVP', 'Stavudine, Lamivudine (Epivir), Nevirapine (Viramune)'
exec uspInsertDrugCombinations 'D4T+3TC+EFV', 'Stavudine, Lamivudine (Epivir), Efavirenz'
exec uspInsertDrugCombinations 'ZDV+3TC+NVP', 'Zidovudine (AZT), Lamivudine (Epivir), Nevirapine (Viramune)'
exec uspInsertDrugCombinations 'ZDV+3TC+EFV', 'Zidovudine (AZT), Lamivudine (Epivir), Efavirenz'
exec uspInsertDrugCombinations 'ZDV+DDI+NVP', 'Zidovudine (AZT), Didanosine, Nevirapine (Viramune)'
exec uspInsertDrugCombinations 'ZDV+DDI+EFV', 'Zidovudine (AZT), Didanosine, Efavirenz'
exec uspInsertDrugCombinations 'ABC+DDI+KLT', 'Abacavir, Didanosine, Kaletra (LPV/r, Lopinavir/Ritonavir)'
exec uspInsertDrugCombinations 'D4T+DDI+EFV', 'Stavudine, Didanosine, Efavirenz'
exec uspInsertDrugCombinations 'ZDV+DDI+KLT', 'Zidovudine (AZT), Didanosine, Kaletra (LPV/r, Lopinavir/Ritonavir)'
exec uspInsertDrugCombinations 'D4T+DDI+KLT', 'Stavudine, Didanosine, Kaletra (LPV/r, Lopinavir/Ritonovir)'
exec uspInsertDrugCombinations 'CDV+CXV+RTV', 'Combivir (Lamivudin, Zidovudine), Indinavir, Kaletra (LPV/r, Lopinavir/Ritonavir)'
exec uspInsertDrugCombinations 'D4T+DDI+NVP', 'Stavudine, Didanosine, Nevirapine (Viramune)'
exec uspInsertDrugCombinations 'LPV/r+CBV', 'Kaletra (LPV/r, Lopinavir/Ritonavir), '
go
-- select * from DrugCombinations
-- delete from DrugCombinations

---------------------------------------------------------------------------------------------------------------------------------------------------
---------DrugCombinationDetails--------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------

exec uspEditDrugCombinationDetails 'CDV+CXV+RTV', 'A005B'
exec uspEditDrugCombinationDetails 'D4T+3TC+NVP', 'A001'
exec uspEditDrugCombinationDetails 'D4T+3TC+NVP', 'A002'
exec uspEditDrugCombinationDetails 'D4T+3TC+NVP', 'A002B'
exec uspEditDrugCombinationDetails 'D4T+3TC+NVP', 'A005B'
exec uspEditDrugCombinationDetails 'D4T+DDI+NVP', 'A001'
exec uspEditDrugCombinationDetails 'D4T+DDI+NVP', 'A002'
exec uspEditDrugCombinationDetails 'D4T+DDI+NVP', 'A002B'
exec uspEditDrugCombinationDetails 'LPV/R+CBV', 'A005B'
exec uspEditDrugCombinationDetails 'ZDV+3TC+NVP', 'A001'
exec uspEditDrugCombinationDetails 'ZDV+3TC+NVP', 'A002'
exec uspEditDrugCombinationDetails 'ZDV+3TC+NVP', 'A002B'
exec uspEditDrugCombinationDetails 'ZDV+3TC+NVP', 'A005B'
exec uspEditDrugCombinationDetails 'ZDV+DDI+NVP', 'A001'
exec uspEditDrugCombinationDetails 'ZDV+DDI+NVP', 'A002'
exec uspEditDrugCombinationDetails 'ZDV+DDI+NVP', 'A002B'
go

-- select * from DrugCombinationDetails
-- delete from DrugCombinationDetails


---------------------------------------------------------------------------------------------------------------------------------------------------
---------CardiologyExaminations---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
--exec uspInsertCardiologyExaminations 'CE001', 'Transmyocardial Laser Revascularization', '23701', '23801', '250000',  0, 0, '', '', ''
--exec uspInsertCardiologyExaminations 'CE002','Heart Valve Repair or Replacement', '23702', '23801', '200000', 0, 0, '', '', ''
--exec uspInsertCardiologyExaminations 'CE003', 'Arrhythmia Treatment', '23703', '23802', '350000', 0, 0, '', '', ''
--exec uspInsertCardiologyExaminations 'CE004','Aneurysm Repair', '23704', '23801', '150000', 0, 0, '', '', ''
--exec uspInsertCardiologyExaminations 'CE005', 'Heart Transplant','23705', '23802', '1550000', 0, 0, '', '', ''
--go

-- select * from CardiologyExaminations
-- delete from CardiologyExaminations



---------------------------------------------------------------------------------------------------------------------------------------------------
---------RadiologyExaminations---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
exec uspInsertRadiologyExaminations 'R001', 'OBSETRICObstetric', '37US','19209', 40000,0,0
exec uspInsertRadiologyExaminations 'R002', 'Biophysical profile SCAN', '37US','19209', 60000,0,0
exec uspInsertRadiologyExaminations 'R003', 'Gender determination (fetal sex) SCAN', '37US','19209', 50000,0,0
exec uspInsertRadiologyExaminations 'R004', 'Breast SCAN', '37US','19209', 50000,0,0
exec uspInsertRadiologyExaminations 'R005', 'Doppler ultrasound', '37US','19209', 100000,0,0
go

-- select * from RadiologyExaminations
-- delete from RadiologyExaminations

---------------------------------------------------------------------------------------------------------------------------------------------------
---------DentalServices----------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------

exec uspInsertDentalServices 'D001', 'Acrylic', '102S', 400000,0
exec uspInsertDentalServices 'D002', 'Amalgam Filling', '102S', 50000,0
exec uspInsertDentalServices 'D003', 'Biopsy', '102S', 35000,0
exec uspInsertDentalServices 'D004', 'Bridge/Unit - Porcelein', '102S', 350000,0
exec uspInsertDentalServices 'D005', 'Composite - Splinting', '102S', 55000,0
go
-- select * from DentalServices
-- delete from DentalServices

-----------------------------------------------------------------------------------------------
-------------- Rooms --------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------

exec uspInsertRooms 'R0101', 'Private', '4001'
exec uspInsertRooms 'R0201', 'Private', '4002'
go
-----------------------------------------------------------------------------------------------
-------------- Beds ---------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------

exec uspInsertBeds 'B010101', 'Bed: 1', 'R0101', 0,0, 25000
exec uspInsertBeds 'B010102', 'Bed: 2', 'R0101', 0,0, 26000
go

exec uspInsertBeds 'B020101', 'Bed: 1', 'R0201', 0,0, 35000
exec uspInsertBeds 'B020102', 'Bed: 2', 'R0201', 0,0, 36000
exec uspInsertBeds 'B020103', 'Bed: 3', 'R0201', 0,0, 68000
go